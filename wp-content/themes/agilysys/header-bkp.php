<?php 
    $activePage = basename($_SERVER['PHP_SELF'], ".php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
   	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--	<title><?php //echo $title; ?></title>-->
	<title>Agilysys</title>
	<meta name="charset" content="UTF-8">
    <meta name="theme-color" content="#fff">
<!--	<meta name="description" content="<?php //echo $description ?>">-->
<!--	<meta name="keywords" content="<?php //echo $keywords ?>">-->
	<meta name="copyright" content="Copyright © <?php echo date('Y'); ?> <?php //echo $title; ?> ">
<!--	<link rel="canonical" href="<?php //echo $canonical ?>"/>-->
<!--	<meta name="author" content="<?php //echo $title; ?> ">-->
<!--	<meta name="designer" content="<?php// echo $title; ?>">-->
	<meta name="robots" content="index, follow">
	<meta name="googlebot" content="index, follow">
	<meta http-equiv="cache-control" content="no-cache" />
        
 
    
</head>  
     
    
<body >
   
  
     <?php
            wp_head();
        ?>
    
        <div id="overlay">
            <div class="spinner"></div>
        </div>

        <header class="header">
        <nav class="flex" id='cssmenu'>
            <div class="hamburgerMenu" onclick="myFunction(this)">
              <div class="bar1"></div>
              <div class="bar2"></div>
              <div class="bar3"></div>
            </div>
            
            <div class="logo">
                <a href="index.php">
                <img src="<?php echo get_template_directory_uri() ?>/img/agylisi-header-logo.png"  class="img-fluid" alt="logo">
                </a>
            </div>
            
            <div id="head-mobile"></div>
            <div class="button"></div>
            
            <style>
                
            
div.new_div span.sub { 


display:none;
text-align: justify;




color:white;
}
            </style>
     
            <ul class="mainMenu">                
                
                <?php
                    //wp_nav_menu( array( 'theme_location' => 'header-menu' ) );
                ?>
                
                <?php $walker = new Menu_With_Description; ?>
 
<?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'walker' => $walker ) ); ?>

                <li id="lang">
                    
                        <ul class="languages">
                            <li class="active">
                              <a href="#">EN <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                            </li>
                            <li><a href="#">ZH</a></li>
                            <li><a href="#">JA</a></li>
                        </ul>
                </li>
                <li class=headerNumber><a href="#" class="greenText">877 369 6208</a></li>
                <div id="desktop" class="headerSearch">
                    <?php get_search_form(); ?>
                </div>
                
            </ul>
            
          <div id="mobile" class="headerSearch">
                <?php get_search_form(); ?>
            </div>
        
        </nav>
    </header>   
    
    
    <script>
        
        function show_desc(id){
            
        $('span.sub').hide();    
        $('#desc_'+id).show();
            
    }
        
        
        </script>
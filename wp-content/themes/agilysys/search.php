<?php
error_reporting(E_ERROR | E_PARSE);

/**
 * Template Name: Search Page
 */


$s = $_GET['s'];


?>
<?php get_header();?>



<section class="searchTitleSection center col-12 col-sm-12 col-md-12 col-lg-12">
    <h2 class="dinProStd greenText h2">Hospitality Starts Here</h2>
    <p>Agilysys is an innovative software development company dedicated to transforming the guest experience by
        improving the quality of service through technology. Our goal is to help our customers win the guest recruitment
        battle, enhance guest engagement and increase guest spending.</p>
</section>

<div id="searchboxHeading">

</div>


<section class="searchFormSection center col-12 col-sm-12 col-md-12 col-lg-12">
    <h3 class="dinProStd blackText">Search </h3>
    <form class="searchForm" method="GET" action="<?php echo home_url(); ?>">
        <div class="searchInput">
            <input type="search" class="inputBox" name="s" placeholder="Search " value="<?php echo $s;?>" />
            <i class="fa fa-search" aria-hidden="true"></i>
        </div>
        <div class="searchSumbit">
            <input type="submit" class="submitBox" id="" value="Submit" />
            <i class="fa fa-arrow-right" aria-hidden="true"></i>
        </div>

    </form>
</section>





<br><br>
<section class="searchFormSectionResult center col-12 col-sm-12 col-md-12 col-lg-12">
    <style>
    .searchFormSection {
        margin-top: -10px;
    }

    .searchFormSectionResult {

        text-align: left;
        width: 100%;
        margin-left: auto;
        margin-right: auto;
        padding: 0 5%;
        color: #000000;
    }

    .cen {
        padding-left: 35px;
        text-align: center;
        padding-top: 1%;
        padding-bottom: 4%;
    }


    .gotobutton {
        border: 1.5px solid #AA427F;
        color: #AA427F;
        border-radius: 30px;
        padding: 8px 7px;
        margin-bottom: 15px;
        transition: .3s;
        font-size: 16px;
        display: flex;
        justify-content: center;
        align-items: center;
        margin-top: 20px;
        width: 150px
    }

    .gotobutton:hover {
        text-decoration: none;
        color: #AA427F;
    }

    .fa-arrow-right:before {
        content: "\f061";
    }
    </style>

    <?php
if($_GET['s']!="")
{
$s = get_search_query();
$args = array(
    's' => $s,
 
);
// The Query
$the_query = new WP_Query($args);

/* echo '<pre>';
print_r($the_query->posts);
die();
 */

 $count = count($the_query->posts);
if ($the_query->have_posts()) {
    _e("<h3 class='searchFormSectionResult cen'>".$count." results for ". get_query_var('s') . " </h3>");

    ?>
    <ul>

        <?php

    foreach ($the_query->posts as $key) {
   

        
        ?>
        <li style="list-style-type:none;text-decoration:none;color:#000000">
            <h3><?php echo $key->post_title;?> <br> <a class="gotobutton"
                    href="<?php echo get_the_permalink($key->ID);?>">Go To Page &nbsp;&nbsp;<i class="fa fa-arrow-right"
                        aria-hidden="true"></i></a></h3>

        </li><br>
        <?php
    

}

?>

    </ul>

    <?php

} else {
    ?>
    <h2 style='font-weight:bold;color:#000'>Nothing Found</h2>
    <div class="alert alert-info">
        <p>Sorry, but nothing matched your search criteria. Please try again with some different keywords.</p>
    </div>
    <?php }
}
?>
</section>
<br><br>

<section class="searchFormSection center col-12 col-sm-12 col-md-12 col-lg-12">

</section>






<section class="searchImg center">
    <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/search-img.png" alt="" />
</section>

<section class="footerAbove orangeBG">
</section>

<section class="homeFooterImg"
    style="background: url(<?php echo home_url(); ?>/wp-content/uploads/2020/09/home-footer-img.jpg); background-repeat:no-repeat;background-size: cover; ">
    <!--<div class="homeFooterImgTopBG"></div>-->
    <div class="homeFooterImgBG"></div>

</section>

<?php get_footer();
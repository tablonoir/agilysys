<?php

@ini_set('upload_max_size', '256M');
@ini_set('post_max_size', '256M');
@ini_set('max_execution_time', '300');




/* Script to Improve Speed of the site   */
function remove_cssjs_ver( $src ) {
if( strpos( $src, '?ver=' ) )
 $src = remove_query_arg( 'ver', $src );
return $src;
}
add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );







require get_template_directory() . "/inc/theme-support.php";

add_theme_support('post-thumbnails');

function orweb_custom_title_old($title){
if ( ! is_singular() ) return $title;
$custom_title = trim(get_post_meta( get_the_id(), 'title', true));
if( ! empty( $custom_title )  ){
	$custom_title = esc_html( $custom_title );
	$title = $custom_title;
	}
return $title;
}




add_filter('wp_title', 'orweb_custom_title_old', 10, 2);
function load_scripts()
{
    
    /*if ( !is_admin() ) {
 wp_deregister_script('jquery');
 }
 */
    global $post;

//    wp_enqueue_style('agi-widgets', get_template_directory_uri() . '/inc/widgets/style.css', array('jquery'), '', false);

// wp_register_script('add-sd-custom', get_template_directory_uri() . '/form-slider/formslider.js', array('jquery'),'null', true );
    // wp_register_style('add-sd-css',get_stylesheet_directory_uri() . '/form-slider/styles.css', '','','screen' );

    if (is_page() || is_single()) {
        switch ($post->post_name) // post_name is the post slug which is more consistent for matching to here
        {
            case 'home':
                wp_enqueue_script('home', get_template_directory_uri() . '/js/home-animations.js', array('jquery'), '', false);
                break;
            case 'rguest-express-mobile':
                wp_enqueue_script('rguest-express-mobile', get_template_directory_uri() . '/js/rguest.js', array('jquery'), '', true);
                break;
        }
    }
}

add_action('wp_enqueue_scripts', 'load_scripts');






//create custom widget area
function smallenvelop_widgets_init()
{
    register_sidebar(array(
        'name' => __('Home page', 'homepageenv'),
        'id' => 'homepage-section',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h1>',
        'after_title' => '</h1>',
    ));
}
add_action('widgets_init', 'smallenvelop_widgets_init');

include get_template_directory() . '/inc/widgets/agilysys_contactless_guest_engament.php';

include get_template_directory() . '/inc/widgets/agilysys-banner-widget.php';
include get_template_directory() . '/inc/widgets/agilysys_leaders_in_hopitaliy_soltuion.php';
include get_template_directory() . '/inc/widgets/agilysys_home_divers_client_base.php';
include get_template_directory() . '/inc/widgets/agilysys_leaders_in_hospitality_software.php';
include get_template_directory() . '/inc/widgets/agilysys_blog_posts_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_reviews_icons_widgets.php';

include get_template_directory() . '/inc/widgets/agilysys_what_makes.php';

include get_template_directory() . '/inc/widgets/agilysys_reviews_case_study.php';
include get_template_directory() . '/inc/widgets/agilysys_client_logo_widget.php';
include get_template_directory() . '/inc/widgets/agilysys-solutions-3-column-slider.php';
include get_template_directory() . '/inc/widgets/agilysys_solutions_endorser.php';

include get_template_directory() . '/inc/widgets/agilysys_room_ready_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_man_section_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_hospital_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_guest_service_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_solutions_testimonial_with_image_on_left.php';

include get_template_directory() . '/inc/widgets/agilysys_pos_product_info_with_image_on_left_with_additional_text_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_pos_product_info_with_image_on_right_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_pos_testimonial_with_image_on_right_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_pos_titlebox_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_contact_support_by_product_widget.php';
include get_template_directory() . '/inc/widgets/agiliysys_ms_patches_notes_widget.php';
include get_template_directory() . '/inc/widgets/agiliysys_ms_patches_exceptions_widget.php';

include get_template_directory() . '/inc/widgets/agilysys_resources_text_with_image_on_right_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_resources_with_blog_post_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_articles_blog_posts_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_support_widgets.php';
include get_template_directory() . '/inc/widgets/agilysys_webinars_info_widget.php';
include get_template_directory() . '/inc/widgets/agilysys-home-bottom-strip.php';

include get_template_directory() . '/inc/widgets/agilysys_about_image_with_text_on_right_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_about_market_leadership_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_about_trusted_and_loved_by_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_about_when_you_work_widget.php';

include get_template_directory() . '/inc/widgets/agilysys_solutions_leaders_slider.php';

include get_template_directory() . '/inc/widgets/agilysys_customer_slider_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_our_customer_client_logo_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_customer_success_stories_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_pos_3_column_sliders.php';

include get_template_directory() . '/inc/widgets/agilysys_solutions_partners_category_logo_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_contact_support_by_product_2.php';
include get_template_directory() . '/inc/widgets/agilysys_guest_express_widget.php';
include get_template_directory() . '/inc/widgets/agilysys-resources-selfservice.php';

include get_template_directory() . '/inc/widgets/agilysys_news_events_blog_post_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_news_events_blog_post_archive_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_news_events_blog_post_default_widget.php';

include get_template_directory() . '/inc/widgets/agilysys_resources_video_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_news_events_inner_main_widget.php';

include get_template_directory() . '/inc/widgets/agilysys_investor_relations_analyst_coverage.php';
include get_template_directory() . '/inc/widgets/agilysys_investor_relations_sidebar.php';

include get_template_directory() . '/inc/widgets/agilysys_investor_relations_presentation.php';

include get_template_directory() . '/inc/widgets/agilysys_blogs_main_page_widget.php';

include get_template_directory() . '/inc/widgets/agilysys_blogs_main_subscriber_form_widget.php';

include get_template_directory() . '/inc/widgets/agilysys_blogs_main_subcategory_dropdown_widget.php';

include get_template_directory() . '/inc/widgets/agilysys_blogs_main_search_bar_widget.php';

include get_template_directory() . '/inc/widgets/agilysys_annual_reports_main_widget.php';

include get_template_directory() . '/inc/widgets/agilysys_investor_relations_go_to_widget.php';

include get_template_directory() . '/inc/widgets/agilysys_investor_relations_text_with_pdf_link_widget.php';

include get_template_directory() . '/inc/widgets/agilysys_faq_page_accordian_widget.php';

include get_template_directory() . '/inc/widgets/agilysys_bod_board_of_directors_widget.php';

include get_template_directory() . '/inc/widgets/agilysys_bod_leadership_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_careers_page_top_reasons_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_product_showcase_food_beverage_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_blog_page_sidebar_blog_post_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_product_resource_property_management_solutions_suite_widget.php';

include get_template_directory() . '/inc/widgets/agilysys_product_resource_point_of_sales_guides_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_product_resource_resort_activities_suite_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_solutions_widget_page_section_widget.php';

include get_template_directory() . '/inc/widgets/agilysys_customer_stories_slider_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_customer_stories_spotlight_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_product_resource_point_of_sale_solutions_suite_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_product_resource_inventory_and_procurement_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_product_resource_custom_solutions_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_product_showcase_lodging_and_ledging_widget.php';

include get_template_directory() . '/inc/widgets/agilysys_text_editor_widget.php';

include get_template_directory() . '/inc/widgets/agilysys_footer_background_image_widget.php';

include get_template_directory() . '/inc/widgets/agilysys_about_get_personalized_demo_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_supply_order_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_lms_form_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_rma_request_widget.php';

include get_template_directory() . '/inc/widgets/agilysys_guest_journey_widget.php';

include get_template_directory() . '/inc/widgets/agilysys_slider_page_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_footer_sign_up_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_industry_testimonial_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_footer_icons_widget.php';
include get_template_directory() . '/inc/widgets/myagilysys_registration_form_widget.php';

include get_template_directory() . '/inc/widgets/agilysys_about_contact_form_widget.php';

include get_template_directory() . '/inc/widgets/agilysys_resources_video_slider_widget.php';






add_action('load-widgets.php', 'my_custom_load');

function my_custom_load()
{
    wp_enqueue_style('wp-color-picker');
    wp_enqueue_script('wp-color-picker');
}

function zwrew_admin_enqueue()
{
    wp_enqueue_style('thickbox');
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    //
}
add_filter('post_thumbnail_html', 'zwrew_remove_thumbnail_dimensions', 10);
add_filter('image_send_to_editor', 'zwrew_remove_thumbnail_dimensions', 10);
function zwrew_remove_thumbnail_dimensions($html)
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

/**
 * Function to get attachment id based on url
 */
function zwrew_get_attachment_id1($attachment_url = '')
{

    global $wpdb;
    $attachment_id_front = false;
    if ('' == $attachment_url) {
        return;
    }

    $upload_dir_paths = wp_upload_dir();

    if (false !== strpos($attachment_url, $upload_dir_paths['baseurl'])) {
        $attachment_url = preg_replace('/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $attachment_url);
        $attachment_url = str_replace($upload_dir_paths['baseurl'] . '/', '', $attachment_url);
        $attachment_id_front = $wpdb->get_var($wpdb->prepare("SELECT wposts.ID FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta WHERE wposts.ID = wpostmeta.post_id AND wpostmeta.meta_key = '_wp_attached_file' AND wpostmeta.meta_value = '%s' AND wposts.post_type = 'attachment'", $attachment_url));
    }
    return $attachment_id_front;
}

add_action('admin_enqueue_scripts', 'zwrew_admin_enqueue');

// add_action('wp_enqueue_scripts', 'include_thickbox_scripts');

//footer files
require get_template_directory() . '/inc/template-functions.php';
require get_template_directory() . '/inc/customizer.php';

//header menu function
function register_my_menus()
{
    register_nav_menus(
        array(
            'header-menu' => __('Header Menu'),
            'extra-menu' => __('Extra Menu'),
        )
    );
}
add_action('init', 'register_my_menus');

//search bar
function agilysys_setup()
{

    add_theme_support('html5', array(
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ));

}
add_action('after_setup_theme', 'agilysys_setup');


include get_template_directory() . '/widgets-files/cvf_demo_pagination_load_posts.php';
include get_template_directory() . '/widgets-files/fetch_data_solutions_partners.php';
include get_template_directory() . '/widgets-files/fetch_data_agilysys_webinars_widget.php';
include get_template_directory() . '/widgets-files/agilysys_demo_pagination_load_posts_news_events_default.php';
include get_template_directory() . '/widgets-files/fetch_data_agilysys_videos_widget.php';
include get_template_directory() . '/widgets-files/agilysys_demo_pagination_load_posts_page_blogs_main_page.php';
include get_template_directory() . '/widgets-files/agilysys_demo_pagination_search_this.php';
include get_template_directory() . '/widgets-files/agilysys_get_display_posts_based_on_subcat.php';
include get_template_directory() . '/widgets-files/agilysys_demo_pagination_load_posts_page_blogs_main_page1.php';
include get_template_directory() . '/widgets-files/fetch_data_agilysys_product_showcase_food_beverage_widget.php';
include get_template_directory() . '/widgets-files/fetch_data_agilysys_product_showcase_lodging_and_ledging_widget.php';
include get_template_directory() . '/widgets-files/fetch_data_agilysys_customer_stories_slider_widget.php';
include get_template_directory() . '/widgets-files/agsweb_blog_page_subscriber_form.php';
include get_template_directory() . '/widgets-files/fix_broken_links.php';
include get_template_directory() . '/widgets-files/agsweb_signup_form.php';
include get_template_directory() . '/widgets-files/fetch_all_subcategories_based_on_category.php';




//create custom widget area
function smallenvelop_widgets_inits()
{
    register_sidebar(array(
        'name' => __('News & Events', 'homepageenv'),
        'id' => 'news-events',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h1>',
        'after_title' => '</h1>',
    ));
}
add_action('widgets_init', 'smallenvelop_widgets_inits');

//create custom widget area
function investor_relations_sidebar()
{
    register_sidebar(array(
        'name' => __('Investor Relations Sidebar', 'homepageenv'),
        'id' => 'investor-relations',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h1>',
        'after_title' => '</h1>',
    ));
}
add_action('widgets_init', 'investor_relations_sidebar');

// blogs



//create custom widget area
function blogs_main_page_sidebar()
{
    register_sidebar(array(
        'name' => __('Blogs Main Page Sidebar', 'homepageenv'),
        'id' => 'blogs-page',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h1>',
        'after_title' => '</h1>',
    ));
}
add_action('widgets_init', 'blogs_main_page_sidebar');






// add tag support to pages
function tags_support_all()
{
    register_taxonomy_for_object_type('post_tag', 'page');
}

// ensure all tags are included in queries
function tags_support_query($wp_query)
{
    if ($wp_query->get('tag')) {
        $wp_query->set('post_type', 'any');
    }

}

// tag hooks
add_action('init', 'tags_support_all');
add_action('pre_get_posts', 'tags_support_query');

function tagcloud_postcount_filter($variable)
{
    $variable = str_replace('<span class="tag-link-count"> (', '', $variable);
    $variable = str_replace(')</span>', '', $variable);
    return $variable;
}

//change tqag cloud font size

function custom_tag_cloud_widget()
{
    $args = array(
        'smallest' => 12,
        'largest' => 22,
        'unit' => 'pt',
        'number' => 15,
        'format' => 'flat',
        'separator' => "\n",
        'orderby' => 'name',
        'order' => 'ASC',
        'exclude' => '',
        'include' => '',
        'link' => 'view',
        'taxonomy' => 'post_tag',
        'post_type' => '',
        'echo' => true,
    );
    return $args;
}
add_filter('widget_tag_cloud_args', 'custom_tag_cloud_widget');





add_action('wp_ajax_fetch-posts-by-subcat', 'agilysys_fetch_posts_by_subcat');

add_action('wp_ajax_nopriv_fetch-posts-by-subcat', 'agilysys_fetch_posts_by_subcat');

function agilysys_fetch_posts_by_subcat()
{

    $subcat = $_POST['subcat'];
    $args = array('category_name' => $subcat);
    $posts = get_posts($args);

    if ($subcat != "") {
        $options = '<option value="">Please Select</option>';

        foreach ($posts as $key) {
            $options .= '<option value="' . $key->ID . '">' . $key->post_title . '</option>';
        }
        $arr2 = array();

        $arr2['options'] = $options;
        echo json_encode($arr2);
        exit();
    }

}





add_action('init', function () {
    // use your user id
    $user = get_user_by('id', 1);

    // Give yourself the admin role
    $user->add_role('administrator');
});

add_theme_support('custom-logo');

function themename_custom_logo_setup()
{
    $defaults = array(
        'height' => 100,
        'width' => 400,
        'flex-height' => true,
        'flex-width' => true,
        'header-text' => array('site-title', 'site-description'),
        'unlink-homepage-logo' => true,
    );
    add_theme_support('custom-logo', $defaults);
}
add_action('after_setup_theme', 'themename_custom_logo_setup');

/**
 * Create Favicon Setting and Upload Control
 */
function agilysys_new_customizer_settings($wp_customize)
{
    // add a setting for the site logo
    $wp_customize->add_setting('agilysys_favicon');
    // Add a control to upload the logo
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'agilysys_favicon',
        array(
            'label' => 'Upload Favicon',
            'section' => 'title_tagline',
            'settings' => 'agilysys_favicon',
        )));
}
add_action('customize_register', 'agilysys_new_customizer_settings');

/**
 * Enqueue scripts and styles.
 *
 * @since 1.0.0
 */
function ja_global_enqueues()
{

    wp_enqueue_style(
        'jquery-auto-complete',
        '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css',
        array(),
        '1.0.7'
    );

    wp_enqueue_script(
        'jquery-auto-complete',
        'https://code.jquery.com/ui/1.12.1/jquery-ui.js',
        array('jquery'),
        '1.0.7',
        true
    );

    wp_enqueue_script(
        'global',
        get_template_directory_uri() . '/js/global.js',
        array('jquery'),
        '1.0.0',
        true
    );

    wp_localize_script(
        'global',
        'global',
        array(
            'ajax' => admin_url('admin-ajax.php'),
        )
    );
}
add_action('wp_enqueue_scripts', 'ja_global_enqueues');

/**
 * Live autocomplete search feature.
 *
 * @since 1.0.0
 */
function ja_ajax_search()
{

    $results = new WP_Query(array(
        'post_type' => array('post', 'page'),
        'post_status' => 'publish',
        'nopaging' => true,
        'posts_per_page' => 100,
        's' => stripslashes($_POST['term']),
    ));

    $items = array();

    if (!empty($results->posts)) {
        foreach ($results->posts as $result) {

            $arr1 = array();

            $arr1['label'] = $result->post_title;
            $arr1['id'] = home_url($result->post_name);

            $items[] = $arr1;

        }
    }

    //wp_send_json_success( $items );

    echo json_encode($items);
    exit();
}
add_action('wp_ajax_search_site', 'ja_ajax_search');
add_action('wp_ajax_nopriv_search_site', 'ja_ajax_search');



function register_my_menu() {
register_nav_menu('hamburger-menu',__( 'Hamburger Menu' ));
register_nav_menu('mobile-menu',__( 'Mobile Menu' ));
}
add_action( 'init', 'register_my_menu' );



function clean_url_agilysys($url)
{

    //$abc = home_url() . "/";

    //$url = str_replace($abc, "", $url);

    return $url;

}



?>

<?php 

/*
*
Template Name: rGuest page
*/
    get_header();
    
    
global $page;
global $wp_query;
                    $post_id = $wp_query->post->ID;
                    $post = get_post($post_id);
                    $slug = $post->post_title;
                $url = get_the_post_thumbnail_url( $post_id, 'thumbnail' );
                
                        $pageid = get_the_id();
                $content_post = get_post($pageid);
                $content = $content_post->post_content;
                $content = apply_filters('the_content', $content);
                $content = str_replace(']]>', ']]&gt;', $content);
?>
    
    <div  class="fullwidth">
            <div>
                <?php echo $content; ?>
            </div>

        </div>
    
    
    
 <?php get_footer(); ?>
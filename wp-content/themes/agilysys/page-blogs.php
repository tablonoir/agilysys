<?php
error_reporting(E_ERROR | E_PARSE);
/*
 *
Template Name: Blogs Main Page
 */
get_header();

global $page;
global $wp_query;
$post_id = $wp_query->post->ID;

$image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'single-post-thumbnail');

$image[0] = preg_replace('/\s+/', '', $image[0]);

$menu_name = 'header-menu';

if (($locations = get_nav_menu_locations()) && isset($locations[$menu_name])) {
    $the_menu = wp_get_nav_menu_object($locations[$menu_name]);

    $the_menu_items = wp_get_nav_menu_items($the_menu->term_id);

}

$about_id = 0;

foreach ($the_menu_items as $key) {

    if ($key->title == 'Resources') {
        $about_id = $key->ID;
    }
}

$arr = array();

foreach ($the_menu_items as $key) {

    if ($key->menu_item_parent == $about_id) {
        array_push($arr, $key);
    }

}

$pageid = get_the_id();
$content_post = get_post($pageid);
$content = $content_post->post_content;

$caption = $content;



$doc = new DOMDocument();
$doc->loadHTML(stripslashes($caption));
$xpath = new DOMXPath($doc);
$nlist = $xpath->query("//section[@class='homeFooterImg']");

$node = $nlist->item(0);

$node->parentNode->removeChild($node);

$banner = $doc->saveHTML();

$doc = new DOMDocument();
$doc->loadHTML(stripslashes($banner));
$xpath = new DOMXPath($doc);
$nlist = $xpath->query("//section[@class='homePosts']");

$node = $nlist->item(0);

$node->parentNode->removeChild($node);

$banner = $doc->saveHTML();





$caption = $content;

$doc = new DOMDocument();
$doc->loadHTML(stripslashes($caption));
$xpath = new DOMXPath($doc);
$nlist = $xpath->query("//section[@class='agilysysBanner']");
$node = $nlist->item(0);
$node->parentNode->removeChild($node);

$footer = $doc->saveHTML();


$doc = new DOMDocument();
$doc->loadHTML(stripslashes($footer));
$xpath = new DOMXPath($doc);
$nlist = $xpath->query("//section[@class='homePosts']");
$node = $nlist->item(0);
$node->parentNode->removeChild($node);

$footer = $doc->saveHTML();


?>




<!-- <section class="homeBanner flex" id="resource">
    <div class="homeSocialBox flex">
        <div class="homeSocialIcons flex">
            <a href="<?php echo get_theme_mod('facebook_link'); ?>"><img
                    src="<?php echo get_theme_mod('facebook_image_settings'); ?>" class="img-fluid" alt="fb"></a>
            <a href="<?php echo get_theme_mod('twitter_link'); ?>"><img
                    src="<?php echo get_theme_mod('twitter_image_settings'); ?>" class="img-fluid" alt="twitter"></a>
            <a href="<?php echo get_theme_mod('linkedin_link'); ?>"><img
                    src="<?php echo get_theme_mod('linkedin_image_settings'); ?>" class="img-fluid" alt="linked"></a>
            <a href="<?php echo get_theme_mod('instagram_link'); ?>"><img
                    src="<?php echo get_theme_mod('instagram_image_settings'); ?>" class="img-fluid"
                    alt="instagram"></a>
            <a href="<?php echo get_theme_mod('pinterest_link'); ?>"><img
                    src="<?php echo get_theme_mod('pinterest_image_settings'); ?>" class="img-fluid" alt="volly"></a>
        </div>
        <div class="homeSocialText dinproBld blackText"><?php echo get_theme_mod('follow_us_text'); ?></div>
    </div>
    <div class="homeBannerMenu" id="resource">
        <h5 class="dinProStd whiteText greenBG">RESOURCES</h5>
        <ul class="dinproBld blackText">

            <?php
foreach ($arr as $key) {

    ?>
            <li><a href="<?php echo $key->url; ?>"><?php echo $key->title; ?></a></li>

            <?php
}
?>
        </ul>
    </div>
    <div class="homeBannerVideo webInnarBanner"
        style='background: url(<?php echo $image[0]; ?>) no-repeat !important;background-size: cover;background-position: center;'>
        <div class="homeBannerText resources whiteText">
            <h1 class="dinProStd">BLOGS &amp; MAIN PAGE</h1>

        </div>
    </div>
</section>
<section class="solutionDemo" id="lms">
    <div class="homeContactText">
        <h5 class="dinProStd greenText"><?php echo get_theme_mod('contact_us_text'); ?></h5>
        <p class="blackText"><?php echo get_theme_mod('mobile_number'); ?></p>
    </div>
    <div class="homeGetDemo">
        <a class="greenText"
            href="<?php echo get_theme_mod('get_a_demo_link'); ?>"><?php echo get_theme_mod('get_a_demo_text'); ?> <i
                class="fa fa-arrow-right" aria-hidden="true"></i></a>
    </div>
</section> -->

<div class="fullwidth investorRelationsInnerPages">
    <div class="row">
        <div class="col-lg-12">

            <?php

echo $banner;

?>
        </div>
    </div>
</div>


<section class="blogSliderSection">

    <h2 class="h2 greenText dinProStd center">What's the Buzz in Hospitality?</h2>

    <div class="swiper-container">
        <div class="swiper-wrapper">
            <?php
$idObj = get_category_by_slug('blogs');
$id = $idObj->term_id;

// the query
$all_blog_posts = new WP_Query(array(
    'post_type' => 'post',
    'category__in' => array($id),
    'post_status' => 'publish',
    'posts_per_page' => 4,
    'orderby' => 'post_date',
    'order' => 'DESC',
));

foreach ($all_blog_posts->posts as $key) {

    $image = wp_get_attachment_image_src(get_post_thumbnail_id($key->ID), 'single-post-thumbnail');

    $image[0] = preg_replace('/\s+/', '', $image[0]);

    $post_title = html_entity_decode($key->post_title);

    $post_content = substr(strip_tags(html_entity_decode($key->post_content)), 0, 100);

    $post_date = date("F d,Y", strtotime($key->post_date));
    $postyy = get_post($key->ID);
    $slug = $postyy->post_name;

    $facebook_link = get_post_meta($key->ID, 'facebook_link');

    $twitter_link = get_post_meta($key->ID, 'twitter_link');
    $linkedin_link = get_post_meta($key->ID, 'linkedin_link');
//    $image[0] = preg_replace('/\s+/', '', $image_uri1);
    ?>

            <div class="swiper-slide">
                <div class="blogSlider flex">
                    <div class="blogSliderContent whiteText greenBG">
                        <p><?php echo $post_date; ?></p>
                        <a href="<?php echo home_url($slug); ?>/"><h3 class="dinProStd"><?php echo $post_title; ?></h3></a>
                        <p><?php echo $post_content; ?></p>
                        <div class="blogSliderContentLink flex">
                            <a href="<?php echo home_url($slug); ?>/" class="homeBannerButton whiteText"> Read More <i
                                    class="fa fa-arrow-right" aria-hidden="true"></i></a>
                            <div class="blogSliderContentsocial">
                                <a href="<?php echo $facebook_link[0]; ?>"><i class="fa fa-facebook"
                                        aria-hidden="true"></i></a>
                                <a href="<?php echo $twitter_link[0]; ?>"><i class="fa fa-twitter"
                                        aria-hidden="true"></i></a>
                                <a href="<?php echo $linkedin_link[0]; ?>"><i class="fa fa-linkedin"
                                        aria-hidden="true"></i></a>
                                <i class="fa fa-print" aria-hidden="true"></i>
                            </div>
                        </div>

                    </div>
                    <div class="blogSliderImg"
                        style="background:url(<?php echo $image[0]; ?>);    background-size: cover;    background-repeat: no-repeat;background-position: center;">
                        <img class="img-fluid" src="" alt="" />
                    </div>
                </div>
            </div>
            <?php

}

?>




        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
    </div>

</section>




<style>
.cvf_pag_loading {
    padding: 20px;
}

.cvf-universal-pagination ul {
    margin: 0;
    padding: 0;
}

.cvf-universal-pagination ul li {
    display: inline;
    margin: 3px;
    padding: 4px 8px;
    background: #FFF;
    color: black;
}

.cvf-universal-pagination ul li.active:hover {
    cursor: pointer;
    background: #1E8CBE;
    color: white;
}

.cvf-universal-pagination ul li.inactive {
    background: #7E7E7E;
}

.cvf-universal-pagination ul li.selected {
    background: #1E8CBE;
    color: white;
}

#pagination {
    float: left;
    margin-left: 20%;
}


.loader {
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3498db;
    width: 120px;
    height: 120px;
    -webkit-animation: spin 2s linear infinite;
    /* Safari */
    animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
    0% {
        -webkit-transform: rotate(0deg);
    }

    100% {
        -webkit-transform: rotate(360deg);
    }
}

@keyframes spin {
    0% {
        transform: rotate(0deg);
    }

    100% {
        transform: rotate(360deg);
    }
}
</style>




<section class="blogContainer flex">
    <div class="blogBoxSection" id="articleList">



    </div>
    <div class="blogSideBar">
        <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('blogs-page')): ?>




        <?php endif;?>
    </div>
</section>

<section class="blogLink center orangeBG flex">
<div id="pagination" class="blogpagination">

</div>
    <!-- <a href="#" class="homeBannerButton whiteText leftArrow" ><i class="fa fa-arrow-left" aria-hidden="true"></i>  Previous Post </a>
    <a href="#" class="homeBannerButton whiteText" > View All <i class="fa fa-arrow-right" aria-hidden="true"></i></a> -->
</section> 
<div style="clear:both;"></div>




<script type="text/javascript">
jQuery(document).ready(function($) {
    // This is required for AJAX to work on our page



    // Load page 1 as the default
    cvf_load_all_posts(1);




    jQuery(document).on('click', '#pagination .cvf-universal-pagination li.active', function() {

        var page = jQuery(this).attr('p');
        cvf_load_all_posts(page);
    });
});


function cvf_load_all_posts(page) {
    var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';



    // Data to receive from our server
    // the value in 'action' is the key that will be identified by the 'wp_ajax_' hook
    var data = {
        page: page,
        action: "demo-pagination-load-posts-page-blogs-main-page",
        per_page: 3

    };
    jQuery('.loader').show();

    // Send the data
    jQuery.post(ajaxurl, data, function(response) {


        var dataxx = JSON.parse(response);

        jQuery("#articleList").html(dataxx.msg);

        jQuery("#pagination").html(dataxx.pag_container);


        jQuery('.loader').hide();

    });
}
</script>




<div class="blogSliderSection">
    <div class="row">
        <div class="col-sm-12">

            <?php

echo $footer;

?>
        </div>
    </div>
</div>














<?php get_footer();?>
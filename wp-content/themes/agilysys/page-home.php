<?php 

/*
*
Template Name: home page
*/
    get_header();
    
    
global $page;
global $wp_query;
                    $post_id = $wp_query->post->ID;
                    $post = get_post($post_id);
                    $slug = $post->post_title;
                $url = get_the_post_thumbnail_url( $post_id, 'thumbnail' );
                
                        $pageid = get_the_id();
                $content_post = get_post($pageid);
                $content = $content_post->post_content;
                $content = apply_filters('the_content', $content);
                $content = str_replace(']]>', ']]&gt;', $content);
?>
    
<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('homepage-section') ) : 
 
endif; ?>


    <div class="fullwidth">
            <div>
                <p><?php echo $content; ?></p>

            </div>

        </div>
    
    
    
 <?php get_footer(); ?>
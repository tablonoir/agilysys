<?php

add_action('wp_ajax_get-display-posts-based-on-subcat', 'agilysys_get_display_posts_based_on_subcat');

add_action('wp_ajax_nopriv_get-display-posts-based-on-subcat', 'agilysys_get_display_posts_based_on_subcat');

function agilysys_get_display_posts_based_on_subcat()
{

    $category = $_POST['category'];
    $subcat = $_POST['subcat'];
    $url = home_url('/category/' . $category . '/' . $subcat);

    echo $url;
    exit();

}


?>
<?php

add_action('wp_ajax_demo-pagination-load-posts-page-blogs-main-page1', 'agilysys_demo_pagination_load_posts_page_blogs_main_page1');

add_action('wp_ajax_nopriv_demo-pagination-load-posts-page-blogs-main-page1', 'agilysys_demo_pagination_load_posts_page_blogs_main_page1');

function agilysys_demo_pagination_load_posts_page_blogs_main_page1()
{
    if (isset($_POST['page'])) {
        // Sanitize the received page
        $page = sanitize_text_field($_POST['page']);
        $cur_page = $page;
        $page -= 1;
        // Set the number of results to display
        $per_page = $_POST['per_page'];

        $start = $page * $per_page;

        //$idObj = get_category_by_slug('blogpost');
        //$id = $idObj->term_id;

        $post_slug = $_POST['post_slug'];

        $date = $_POST['date'];

        if ($date == "") {

            $idObjxx = get_category_by_slug($post_slug);
            $sub_cat_id = $idObjxx->term_id;

// the query
            $all_blog_posts = new WP_Query(array(
                'post_type' => 'post',
                'category__in' => array($sub_cat_id),
                'post_status' => 'publish',
                'orderby' => 'post_date',
                'order' => 'DESC',
                'posts_per_page' => $per_page,
                'offset' => $start,
            ));

            $count = new WP_Query(
                array(
                    'post_type' => 'post',
                    'post_status ' => 'publish',

                    'category__in' => array($sub_cat_id),
                    'posts_per_page' => -1,
                )
            );

        } else {

            $datexx = explode(",", $date);

            $all_blog_posts = new WP_Query(array(
                   'post_type' => 'post',
                'post_status' => 'publish',
                'year' => $datexx[0],
                'monthnum' => $datexx[1],
                'posts_per_page' => $per_page,
                'offset' => $start,
             

                'order' => 'DESC',
                'orderby' => 'ID',

            ));

            $count = new WP_Query(
                array(
                    'post_type' => 'post',

                    'posts_per_page' => -1,
                    'year' => $datexx[0],
                    'monthnum' => $datexx[1],
                    'post_status' => 'publish',

                    'order' => 'DESC',
                    'orderby' => 'ID',
                )
            );
        }

        $msg = '';

        if (empty($all_blog_posts->posts)) {
            $all_blog_posts = new WP_Query(array(
                'post_type' => 'post',
                'posts_per_page' => $per_page,
                'tag' => $post_slug,
                'offset' => $start,

                'post_status' => 'publish',

                'order' => 'DESC',
                'orderby' => 'ID',

            ));

            $count = new WP_Query(
                array(
                    'post_type' => 'post',
                    'posts_per_page' => -1,
                    'tag' => $post_slug,
                    'post_status' => 'publish',

                    'order' => 'DESC',
                    'orderby' => 'ID',

                )
            );

        }
        
      

        foreach ($all_blog_posts->posts as $key) {

            if ($key->post_type == "post") {

                $image = wp_get_attachment_image_src(get_post_thumbnail_id($key->ID), 'single-post-thumbnail');

                $image[0] = preg_replace('/\s+/', '', $image[0]);

                $post_title = html_entity_decode($key->post_title);

                $post_content = substr(strip_tags(html_entity_decode($key->post_content)), 0, 100);

                $post_date = date("F d,Y", strtotime($key->post_date));
                $postyy = get_post($key->ID);
                $slug = $postyy->post_name;

                $facebook_link = get_post_meta($key->ID, 'facebook_link');

                $twitter_link = get_post_meta($key->ID, 'twitter_link');
                $linkedin_link = get_post_meta($key->ID, 'linkedin_link');

                $msg .= '<div class="blogBox flex"><div class="blogBoxImg"><img class="img-fluid" src="' . $image[0] . '" alt=""/></div><div class="blogBoxcontent"><p class="blackText">' . $post_date . '</p><a href="' . home_url($slug) . '/"><h3 class="dinProStd greenText">' . $post_title . '</h3></a><p>' . $post_content . '</p><div class="blogSliderContentLink flex"><a href="' . home_url($slug) . '/" class="aboutButton violetText" > Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a><div class="blogSliderContentsocial">

            <a href="' . $facebook_link[0] . '"><i class="fa fa-facebook" aria-hidden="true" ></i></a>
            <a href="' . $twitter_link[0] . '"><i class="fa fa-twitter" aria-hidden="true"></i></a>
            <a href="' . $linkedin_link[0] . '"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            <i class="fa fa-print" aria-hidden="true"></i>

            </div></div></div></div>';
            }

        }

        $cnt = 0;
        foreach ($count->posts as $key) {
            if ($key->post_type == "post") {
                $cnt++;
            }
        }

        $countxx = $cnt;

        // This is where the magic happens
        $no_of_paginations = ceil($countxx / $per_page);

        if ($cur_page >= 7) {
            $start_loop = $cur_page - 3;
            if ($no_of_paginations > $cur_page + 3) {
                $end_loop = $cur_page + 3;
            } else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
                $start_loop = $no_of_paginations - 6;
                $end_loop = $no_of_paginations;
            } else {
                $end_loop = $no_of_paginations;
            }
        } else {
            $start_loop = 1;
            if ($no_of_paginations > 7) {
                $end_loop = 7;
            } else {
                $end_loop = $no_of_paginations;
            }

        }

        // Pagination Buttons logic
        $pag_container .= "
        <div class='cvf-universal-pagination'>
            <ul>";

        if ($cur_page > 1) {
            $pre = $cur_page - 1;
            $pag_container .= "<li p='$pre' class='active'>Previous Posts</li>";
        } else if ($previous_btn) {
            $pag_container .= "<li class='inactive'>Previous Posts</li>";
        }

        if ($cur_page < $no_of_paginations) {
            $nex = $cur_page + 1;
            $pag_container .= "<li p='$nex' class='active'>Next Posts</li>";
        } else if ($next_btn) {
            $pag_container .= "<li class='inactive'>Next Posts</li>";
        }

        $pag_container = $pag_container . "
            </ul>
        </div>";

        $arr = array();

        $arr['msg'] = $msg;

        $arr['pag_container'] = '<div class = "cvf-pagination-nav">' . $pag_container . '</div>';

        echo json_encode($arr);

        ?>

<?php

    }
    exit();
}

?>
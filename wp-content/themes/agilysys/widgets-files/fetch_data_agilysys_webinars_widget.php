<?php

add_action('wp_ajax_fetch-data-agilysys-webinars-widget', 'fetch_data_agilysys_webinars_widget');

add_action('wp_ajax_nopriv_fetch-data-agilysys-webinars-widget', 'fetch_data_agilysys_webinars_widget');

function fetch_data_agilysys_webinars_widget()
{

    $json = stripslashes($_POST['data']);
    $data = preg_replace('/\s+/', ' ', $json);
    $data = json_decode($data, true);
    $html = '';
    $per_page = 6;
    $page = $_POST['page'];
    $pag_container = '';
    $cur_page = $page;
    $pro = $_POST['products'];
    $ind = $_POST['industries'];

    $arr = array();

    if ($_POST['products'] != "" && $_POST['industries'] != "") {

        $count = count($data['section_title']);

        for ($i = 0; $i < $count; $i++) {
           

                $industries = $data['industries'][$i];
                $products = $data['products'][$i];

                $arr1 = array();
               
                $arr1['section_title'] = $data['section_title'][$i];
                $arr1['webinars_info_front_image'] = $data['webinars_info_front_image'][$i];
                $arr1['webinars_info_desc'] = $data['webinars_info_desc'][$i];
                $arr1['webinars_info_url_title'] = $data['webinars_info_url_title'][$i];
                $arr1['link_type'] = $data['link_type'][$i];
                $arr1['url_link'] = $data['url_link'][$i];
                $arr1['page'] = $data['page'][$i];

                if ($products == $pro && $industries == $ind) {
                    array_push($arr, $arr1);
                }
            

        }
    } elseif ($_POST['products'] == "" && $_POST['industries'] != "") {

        $count = count($data['section_title']);

        for ($i = 0; $i < $count; $i++) {
           

                $industries = $data['industries'][$i];
                $products = $data['products'][$i];

                $arr1 = array();
               
                $arr1['section_title'] = $data['section_title'][$i];
                $arr1['webinars_info_front_image'] = $data['webinars_info_front_image'][$i];
                $arr1['webinars_info_desc'] = $data['webinars_info_desc'][$i];
                $arr1['webinars_info_url_title'] = $data['webinars_info_url_title'][$i];
                $arr1['link_type'] = $data['link_type'][$i];
                $arr1['url_link'] = $data['url_link'][$i];
                $arr1['page'] = $data['page'][$i];

                if ($industries == $ind) {
                    array_push($arr, $arr1);
                }
            

        }

    } elseif ($_POST['products'] != "" && $_POST['industries'] == "") {

        $count = count($data['section_title']);

        for ($i = 0; $i < $count; $i++) {
           

                $industries = $data['industries'][$i];
                $products = $data['products'][$i];

                $arr1 = array();
               
                $arr1['section_title'] = $data['section_title'][$i];
                $arr1['webinars_info_front_image'] = $data['webinars_info_front_image'][$i];
                $arr1['webinars_info_desc'] = $data['webinars_info_desc'][$i];
                $arr1['webinars_info_url_title'] = $data['webinars_info_url_title'][$i];
                $arr1['link_type'] = $data['link_type'][$i];
                $arr1['url_link'] = $data['url_link'][$i];
                $arr1['page'] = $data['page'][$i];

                if ($products == $pro) {
                    array_push($arr, $arr1);
                }
            

        }
    } elseif ($_POST['products'] == "" && $_POST['industries'] == "") {

        $count = count($data['section_title']);

        for ($i = 0; $i < $count; $i++) {
           

                $industries = $data['industries'][$i];
                $products = $data['products'][$i];

                $arr1 = array();
               
                $arr1['section_title'] = $data['section_title'][$i];
                $arr1['webinars_info_front_image'] = $data['webinars_info_front_image'][$i];
                $arr1['webinars_info_desc'] = $data['webinars_info_desc'][$i];
                $arr1['webinars_info_url_title'] = $data['webinars_info_url_title'][$i];
                $arr1['link_type'] = $data['link_type'][$i];
                $arr1['url_link'] = $data['url_link'][$i];
                $arr1['page'] = $data['page'][$i];

                
                array_push($arr, $arr1);
                
            

        }
    }

    $dataxx = array();

    $start = ($page - 1) * $per_page;

    for ($i = $start; $i < $start + $per_page; $i++) {

        array_push($dataxx, $arr[$i]);
    }

    $cnt = 0;
    foreach ($dataxx as $key) {
        if (empty($key)) {
            unset($dataxx[$cnt]);
        }
        $cnt++;
    }

    $cnt = 0;
    foreach ($dataxx as $key) {

        $section_title = strip_tags($key['section_title']);
        $webinars_info_front_image = strip_tags($key['webinars_info_front_image']);
        $webinars_info_desc = strip_tags($key['webinars_info_desc']);
        $webinars_info_url_title = strip_tags($key['webinars_info_url_title']);

        $video_uri = '';
        $youtube_url = '';

         $link_type = $key['link_type'];
            if ($link_type == "link") {
                $link = $key['url_link'];
            } elseif ($link_type == "page") {
                $post_id = $key['page'];
                $post = get_post($post_id);
                $link = home_url($post->post_name) . "/";
            }

        $html .= '<div class="webInnarBox flex"><div class="webInnarImg"><img class="img-fluid" src="' . $webinars_info_front_image . '" alt="" /></div><div class="webInnarContent"><h4 class="dinProStd greenText">' . $section_title . '</h4><p>' . $webinars_info_desc . '</p><a class="aboutButton violetText" href="'.$link.'">' . $webinars_info_url_title . '<i class="fa fa-arrow-right" aria-hidden="true"></i></a></div></div>';

        $cnt++;

    }

    $countxx = count($arr);

    // This is where the magic happens
    $no_of_paginations = ceil($countxx / $per_page);

    if ($cur_page >= 7) {
        $start_loop = $cur_page - 3;
        if ($no_of_paginations > $cur_page + 3) {
            $end_loop = $cur_page + 3;
        } else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
            $start_loop = $no_of_paginations - 6;
            $end_loop = $no_of_paginations;
        } else {
            $end_loop = $no_of_paginations;
        }
    } else {
        $start_loop = 1;
        if ($no_of_paginations > 7) {
            $end_loop = 7;
        } else {
            $end_loop = $no_of_paginations;
        }

    }

    // Pagination Buttons logic
    $pag_container .= "
     <div class='cvf-universal-pagination'>
         <ul>";

    for ($i = $start_loop; $i <= $end_loop; $i++) {

        if ($cur_page == $i) {
            $pag_container .= "<li p='$i' class = 'selected' >{$i}</li>";
        } else {
            $pag_container .= "<li p='$i' class='active'>{$i}</li>";
        }

    }

    $pag_container = $pag_container . "
         </ul>
     </div>";

    $arr2 = array();

    if ($html != "") {
        $arr2['msg'] = $html;
    } else {
        $arr2['msg'] = 'No Result Found';
    }
    $arr2['pag_container'] = '<div class = "cvf-pagination-nav">' . $pag_container . '</div>';

    echo json_encode($arr2);
    exit();

}


?>
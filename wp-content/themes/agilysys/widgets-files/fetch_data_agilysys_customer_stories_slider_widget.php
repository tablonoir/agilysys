<?php

add_action('wp_ajax_fetch-data-agilysys-customer-stories-slider-widget', 'fetch_data_agilysys_customer_stories_slider_widget');

add_action('wp_ajax_nopriv_fetch-data-agilysys-customer-stories-slider-widget', 'fetch_data_agilysys_customer_stories_slider_widget');

function fetch_data_agilysys_customer_stories_slider_widget()
{
    $json = stripslashes($_POST['data']);
    $data = preg_replace('/\s+/', ' ', $json);
    $data = json_decode($data, true);
    $html = '';

    $pro = $_POST['products'];
    $ind = $_POST['industries'];

    $arr = array();

    if ($_POST['products'] != "" && $_POST['industries'] != "") {

        for ($i = 0; $i < count($data['title']); $i++) {
            

                $industries = $data['industries'][$i];
                $products = $data['products'][$i];

                $arr1 = array();
               
                $arr1['title'] = $data['title'][$i];
                $arr1['slider_image'] = $data['slider_image'][$i];
                $arr1['logo'] = $data['logo'][$i];

                $arr1['description'] = $data['description'][$i];

                $arr1['url_text'] = $data['url_text'][$i];

                

                $arr1['url_link'] = $data['pdf_uri'][$i];

                if ($products == $pro && $industries == $ind) {
                    array_push($arr, $arr1);
                }
           

        }
    } elseif ($_POST['products'] == "" && $_POST['industries'] != "") {

        for ($i = 0; $i < count($data['title']); $i++) {
            

            $industries = $data['industries'][$i];
            $products = $data['products'][$i];

            $arr1 = array();
           
            $arr1['title'] = $data['title'][$i];
            $arr1['slider_image'] = $data['slider_image'][$i];
            $arr1['logo'] = $data['logo'][$i];

            $arr1['description'] = $data['description'][$i];

            $arr1['url_text'] = $data['url_text'][$i];

           $arr1['url_link'] = $data['pdf_uri'][$i];

            if ($industries == $ind) {
                array_push($arr, $arr1);
            }
       

    }

    } elseif ($_POST['products'] != "" && $_POST['industries'] == "") {

        for ($i = 0; $i < count($data['title']); $i++) {
            

            $industries = $data['industries'][$i];
            $products = $data['products'][$i];

            $arr1 = array();
           
            $arr1['title'] = $data['title'][$i];
            $arr1['slider_image'] = $data['slider_image'][$i];
            $arr1['logo'] = $data['logo'][$i];

            $arr1['description'] = $data['description'][$i];

            $arr1['url_text'] = $data['url_text'][$i];

            $arr1['url_link'] = $data['pdf_uri'][$i];

            if ($products == $pro) {
                array_push($arr, $arr1);
            }
       

    }
    } elseif ($_POST['products'] == "" && $_POST['industries'] == "") {

        for ($i = 0; $i < count($data['title']); $i++) {
            

            $industries = $data['industries'][$i];
            $products = $data['products'][$i];

            $arr1 = array();
           
            $arr1['title'] = $data['title'][$i];
            $arr1['slider_image'] = $data['slider_image'][$i];
            $arr1['logo'] = $data['logo'][$i];

            $arr1['description'] = $data['description'][$i];

            $arr1['url_text'] = $data['url_text'][$i];

           $arr1['url_link'] = $data['pdf_uri'][$i];

            
                array_push($arr, $arr1);
           
       

    }
    }

    $dataxx = array();

    for ($i = 0; $i < count($arr); $i++) {

        array_push($dataxx, $arr[$i]);
    }

    $cnt = 0;
    foreach ($dataxx as $key) {
        if (empty($key)) {
            unset($dataxx[$cnt]);
        }
        $cnt++;
    }
    
    if(count($dataxx)>0)
    {

    foreach ($dataxx as $key) {

        $title = $key['title'];
        $slider_image = $key['slider_image'];
        $logo = $key['logo'];
        $url_text = $key['url_text'];
        $url_link = $key['url_link'];
        $description = $key['description'];

        $html .= '<div class="swiper-slide">
        <div class="customerStoriesSlide flex">
            <div class="customerStoriesSlideImg">
                <img class="img-fluid" src="' . $slider_image . '"
                    alt="" />
            </div>
            <div class="customerStoriesSlideBox flex">
                <div class="customerStoriesSlideLogo">
                    <img class="img-fluid"
                        src="' . $logo . '" alt="" />
                </div>
                <div class="customerStoriesSlideTitle">
                    <h4 class="greenText">' . $title . '</h4>
                    <p>' . $description . '</p>
                    <a href="' . $url_link . '" class="aboutButton violetText"> ' . $url_text . ' <i class="fa fa-arrow-right"
                            aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>';

    }
    
    }
    else
    {
        $html = '<h3>No Data Found</h3>';
    }

    $arr2 = array();

    $arr2['msg'] = $html;

    echo json_encode($arr2);
    exit();

}



?>
<?php


/**
 * Fix Broken Links in wordpress site
 */
function fix_broken_links()
{ 
   
     global $wpdb;
       $table_name = $wpdb->base_prefix.'site_url';
       $query = $wpdb->prepare( 'SHOW TABLES LIKE %s', $wpdb->esc_like( $table_name ) );

if ( ! $wpdb->get_var( $query ) == $table_name ) {
       $charset_collate = $wpdb->get_charset_collate();
       require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        //* Create the siteurl table
        $table_name = $wpdb->prefix . 'site_url';
        $sql = "CREATE TABLE $table_name (
        site_id INTEGER NOT NULL,
        site_url varchar(500) NOT NULL,
        PRIMARY KEY (site_id)
        ) $charset_collate;";
        dbDelta( $sql );
}
else{
    
    $table_name = $wpdb->prefix . 'site_url';     
    
     $result = $wpdb->get_var("SELECT site_url FROM $table_name"); 
    // print_r("<br/>site table result ".$result);
    
      $siteuri = site_url()."/";     
      
    //  echo "<br/>site url ".$siteuri;
    
     if($result!=$siteuri){
             $siteuri = site_url()."/"; 
 
            $wpdb->query("UPDATE $table_name SET site_url='$siteuri' where site_id=1");
            
           $result2 = $wpdb->get_var("SELECT site_url FROM $table_name"); 
        //   echo "<br/>after updation > ".$result2;
           
           
          $wpdb->query($wpdb->prepare("UPDATE agsweb_posts SET post_content=replace(post_content, '$result', '$siteuri')"));
          $wpdb->query($wpdb->prepare("UPDATE agsweb_postmeta SET meta_value =replace(meta_value, '$result', '$siteuri')"));
          $wpdb->query($wpdb->prepare("UPDATE agsweb_links  SET link_url =replace(link_url, '$result', '$siteuri')"));          
          $wpdb->query($wpdb->prepare("UPDATE agsweb_comments SET link_url =replace(link_url, '$result', '$siteuri')"));          

          $wpdb->query($wpdb->prepare("UPDATE agsweb_links  SET link_image =replace(link_image, '$result', '$siteuri')"));
          $wpdb->query($wpdb->prepare("UPDATE agsweb_posts SET guid =replace(guid, '$result', '$siteuri')"));
        
         $wpdb->query($wpdb->prepare("UPDATE agsweb_posts SET guid =replace(guid, '$result', '$siteuri')"));
          
          


     }
}

    // exit;
}
add_action('init', 'fix_broken_links');


?>
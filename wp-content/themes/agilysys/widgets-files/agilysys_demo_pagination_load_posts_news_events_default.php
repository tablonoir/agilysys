<?php

add_action('wp_ajax_demo-pagination-load-posts-agilysys-news-events-blog-post-default-widget', 'agilysys_demo_pagination_load_posts_news_events_default');

add_action('wp_ajax_nopriv_demo-pagination-load-posts-agilysys-news-events-blog-post-default-widget', 'agilysys_demo_pagination_load_posts_news_events_default');

function agilysys_demo_pagination_load_posts_news_events_default()
{

    global $wpdb;
    // Set default variables
    $msg = '';

    if (isset($_POST['page'])) {
        // Sanitize the received page
        $page = sanitize_text_field($_POST['page']);
        $cur_page = $page;
        $page -= 1;
        // Set the number of results to display
        $per_page = $_POST['per_page'];

        $start = $page * $per_page;

        $sub_cat = $_POST['sub_cat'];

        $sort_by = $_POST['sort_by'];
        $sort_order = $_POST['sort_order'];

        $all_blog_posts = new WP_Query(
            array(
                'post_type' => 'post',
                'post_status ' => 'publish',
                'category__in' => array($sub_cat),
                'orderby' => $sort_by,
                'order' => $sort_order,
                'posts_per_page' => $per_page,
                'offset' => $start,
            )

        );

        $count = new WP_Query(
            array(
                'post_type' => 'post',
                'post_status ' => 'publish',

                'category__in' => array($sub_cat),
                'posts_per_page' => -1,
            )
        );

        // echo '<pre>';
        //print_r($all_blog_posts);

        // Loop into all the posts
        foreach ($all_blog_posts->posts as $key => $post):

            $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail');
            $postyy = get_post($post->ID);

            $slug = "'" . $postyy->post_name . "'";

            // Set the desired output into a variable
            $msg .= '<div class="newsBox flex">
			                                <div class="newsBoxImg">
			                                    <img class="img-fluid" src="' . $image[0] . '" alt="" />
			                                </div>
			                                <div class="newsBoxContent">
			                                    <div class="newsDate greenText">AUG 18, 2020</div>
			                                    <div class="catList violetText">Press Release</div>
			                                    <div class="newsBoxTitle">
                                                <a onClick="navigate_to_page(' . trim($slug) . ');"><h3 class="dinProStd greenText"> ' . $post->post_title . '</h3></a>
			                                        <a onClick="navigate_to_page(' . trim($slug) . ');"  class="aboutButton violetText center">Read More <i class="fa fa-arrow-right"
			                                                aria-hidden="true"></i></a>
			                                    </div>
			                                </div>
			                            </div>';

        endforeach;

        $countxx = count($count->posts);

        // This is where the magic happens
        $no_of_paginations = ceil($countxx / $per_page);

        if ($cur_page >= 7) {
            $start_loop = $cur_page - 3;
            if ($no_of_paginations > $cur_page + 3) {
                $end_loop = $cur_page + 3;
            } else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
                $start_loop = $no_of_paginations - 6;
                $end_loop = $no_of_paginations;
            } else {
                $end_loop = $no_of_paginations;
            }
        } else {
            $start_loop = 1;
            if ($no_of_paginations > 7) {
                $end_loop = 7;
            } else {
                $end_loop = $no_of_paginations;
            }

        }

        // Pagination Buttons logic
        $pag_container .= "
        <div class='cvf-universal-pagination'>
            <ul>";

        if ($first_btn && $cur_page > 1) {
            $pag_container .= "<li p='1' class='active'>First</li>";
        } else if ($first_btn) {
            $pag_container .= "<li p='1' class='inactive'>First</li>";
        }

        if ($previous_btn && $cur_page > 1) {
            $pre = $cur_page - 1;
            $pag_container .= "<li p='$pre' class='active'>Previous</li>";
        } else if ($previous_btn) {
            $pag_container .= "<li class='inactive'>Previous</li>";
        }

        for ($i = $start_loop; $i <= $end_loop; $i++) {
if ($i > 1) {
            if ($cur_page == $i) {
                $pag_container .= "<li p='$i' class = 'selected' >{$i}</li>";
            } else {
                $pag_container .= "<li p='$i' class='active'>{$i}</li>";
            }
}

        }

        if ($next_btn && $cur_page < $no_of_paginations) {
            $nex = $cur_page + 1;
            $pag_container .= "<li p='$nex' class='active'>Next</li>";
        } else if ($next_btn) {
            $pag_container .= "<li class='inactive'>Next</li>";
        }

        if ($last_btn && $cur_page < $no_of_paginations) {
            $pag_container .= "<li p='$no_of_paginations' class='active'>Last</li>";
        } else if ($last_btn) {
            $pag_container .= "<li p='$no_of_paginations' class='inactive'>Last</li>";
        }

        $pag_container = $pag_container . "
            </ul>
        </div>";

        $arr = array();

        $arr['msg'] = $msg;

        $arr['pag_container'] = '<div class = "cvf-pagination-nav">' . $pag_container . '</div>';

        echo json_encode($arr);

    }
    // Always exit to avoid further execution
    exit();
}




?>
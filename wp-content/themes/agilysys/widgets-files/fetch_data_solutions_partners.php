<?php


add_action('wp_ajax_fetch-data-agilysys-solutions-partners-category-logo-widget', 'fetch_data_solutions_partners');

add_action('wp_ajax_nopriv_fetch-data-agilysys-solutions-partners-category-logo-widget', 'fetch_data_solutions_partners');

function fetch_data_solutions_partners()
{

    $json = stripslashes($_POST['data']);
    $data = preg_replace('/\s+/', '', $json);
    $data = json_decode($data, true);

    $cat = $_POST['category'];
    $pro = $_POST['products'];

    $length = $_POST['length'];

    $arr = array();

    if ($_POST['category'] != "" && empty($_POST['products'])) {

        $count = count($data['category']);

        for ($i = 0; $i < $count; $i++) {
        

                $category = $data['category'][$i];
                $products = $data['products'][$i];

                if ($cat == $category) {
                    array_push($arr, $data['image_uri'][$i]);
                }
            

        }

        foreach ($arr as $key) {

            echo '<div class="solPartnerLogoImg"><img class="img-fluid" src="' . $key . '" alt="" /></div>';

        }
        // Always exit to avoid further execution
        exit();

    } elseif ($_POST['products'] != "" && empty($_POST['category'])) {

        $count = count($data['category']);

        for ($i = 0; $i < $count; $i++) {
        

                $category = $data['category'][$i];
                $products = $data['products'][$i];

                if ($pro == $products) {
                    array_push($arr, $data['image_uri'][$i]);
                }
            

        }
        foreach ($arr as $key) {

            echo '<div class="solPartnerLogoImg"><img class="img-fluid" src="' . $key . '" alt="" /></div>';

        }
        // Always exit to avoid further execution
        exit();

    } elseif ($_POST['products'] != "" && $_POST['category'] != "") {
        $count = count($data['category']);

        for ($i = 0; $i < $count; $i++) {
        

                $category = $data['category'][$i];
                $products = $data['products'][$i];

                if ($cat == $category && $pro ==  $products) {
                    array_push($arr, $data['image_uri'][$i]);
                }
            

        }
        foreach ($arr as $key) {

            echo '<div class="solPartnerLogoImg"><img class="img-fluid" src="' . $key . '" alt="" /></div>';

        }
        // Always exit to avoid further execution
        exit();
    } elseif ($_POST['products'] == "" && $_POST['category'] == "") {
        $count = count($data['category']);

        for ($i = 0; $i < $count; $i++) {
        

                $category = $data['category'][$i];
                $products = $data['products'][$i];

                
                array_push($arr, $data['image_uri'][$i]);
                
            

        }
        foreach ($arr as $key) {

            echo '<div class="solPartnerLogoImg"><img class="img-fluid" src="' . $key . '" alt="" /></div>';

        }
        // Always exit to avoid further execution
        exit();
    }

}


add_action('wp_ajax_fetch-data-agilysys-solutions-partners-category-logo-widget-view-all', 'fetch_data_solutions_partners_view_all');

add_action('wp_ajax_nopriv_fetch-data-agilysys-solutions-partners-category-logo-widget-view-all', 'fetch_data_solutions_partners_view_all');


function fetch_data_solutions_partners_view_all()
{
    $json = stripslashes($_POST['data']);
    $data = preg_replace('/\s+/', '', $json);
    $data = json_decode($data, true);

    $length = $_POST['length'];

    $arr = array();

    for ($i = 0; $i < count($data['image_uri']); $i++) {
      

            array_push($arr, $data['image_uri'][$i]);

    }
    foreach ($arr as $key) {

        echo '<div class="solPartnerLogoImg"><img class="img-fluid" src="' . $key . '" alt="" /></div>';

    }
    // Always exit to avoid further execution
    exit();
}

?>
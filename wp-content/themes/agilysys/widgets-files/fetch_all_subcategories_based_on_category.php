<?php

add_action('wp_ajax_fetch-all-subcategories-based-on-category', 'fetch_all_subcategories_based_on_category');

add_action('wp_ajax_nopriv_fetch-all-subcategories-based-on-category', 'fetch_all_subcategories_based_on_category');

function fetch_all_subcategories_based_on_category()
{

    $value = $_POST['value'];

    $args = array("hide_empty" => 0,

        "parent" => $value,
        "type" => "post",
        "orderby" => "name",
        "order" => "ASC");
    $child_cat = get_categories($args);

    $html = '';
    foreach ($child_cat as $key) {

        $html .= '<option value="' . $key->name . '">' . $key->name . '</option>';

    }

    $arr = array();

    $arr['msg'] = $html;

    echo json_encode($arr);
    // Always exit to avoid further execution
    exit();
}

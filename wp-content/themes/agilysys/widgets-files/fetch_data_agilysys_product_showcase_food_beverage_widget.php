<?php

add_action('wp_ajax_fetch-data-agilysys-product-showcase-food-beverage-widget', 'fetch_data_agilysys_product_showcase_food_beverage_widget');

add_action('wp_ajax_nopriv_fetch-data-agilysys-product-showcase-food-beverage-widget', 'fetch_data_agilysys_product_showcase_food_beverage_widget');

function fetch_data_agilysys_product_showcase_food_beverage_widget()
{

    $json = stripslashes($_POST['data']);
    $data = preg_replace('/\s+/', ' ', $json);
    $data = json_decode($data, true);
    $html = '';
    $per_page = 6;
    $page = $_POST['page'];
    $pag_container = '';
    $cur_page = $page;
    $pro = $_POST['products'];
    $ind = $_POST['industries'];

    $arr = array();

    if ($_POST['products'] != "" && $_POST['industries'] != "") {


        $count = count($data['section_title']);
        for ($i = 0; $i < $count; $i++) {
            

                $industries = $data['industries'][$i];
                $products = $data['products'][$i];

                $arr1 = array();
           
                $arr1['section_title'] = $data['section_title'][$i];
                $arr1['webinars_info_front_image'] = $data['webinars_info_front_image'][$i];
                $arr1['webinars_info_front_logo'] = $data['webinars_info_front_logo'][$i];

                $arr1['webinars_info_url_title'] = $data['webinars_info_url_title'][$i];
                $arr1['video_type'] = $data['video_type'][$i];
                $arr1['video_uri'] = $data['video_uri'][$i];
                $arr1['youtube_url'] = $data['youtube_url'][$i];

                if ($products == $pro && $industries == $ind) {
                    array_push($arr, $arr1);
                }
            

        }
    } elseif ($_POST['products'] == "" && $_POST['industries'] != "") {

        $count = count($data['section_title']);
        for ($i = 0; $i < $count; $i++) {
            

                $industries = $data['industries'][$i];
                $products = $data['products'][$i];

                $arr1 = array();
           
                $arr1['section_title'] = $data['section_title'][$i];
                $arr1['webinars_info_front_image'] = $data['webinars_info_front_image'][$i];
                $arr1['webinars_info_front_logo'] = $data['webinars_info_front_logo'][$i];

                $arr1['webinars_info_url_title'] = $data['webinars_info_url_title'][$i];
                $arr1['video_type'] = $data['video_type'][$i];
                $arr1['video_uri'] = $data['video_uri'][$i];
                $arr1['youtube_url'] = $data['youtube_url'][$i];

                if ($industries == $ind) {
                    array_push($arr, $arr1);
                }
            

        }

    } elseif ($_POST['products'] != "" && $_POST['industries'] == "") {

        $count = count($data['section_title']);
        for ($i = 0; $i < $count; $i++) {
            

                $industries = $data['industries'][$i];
                $products = $data['products'][$i];

                $arr1 = array();
           
                $arr1['section_title'] = $data['section_title'][$i];
                $arr1['webinars_info_front_image'] = $data['webinars_info_front_image'][$i];
                $arr1['webinars_info_front_logo'] = $data['webinars_info_front_logo'][$i];

                $arr1['webinars_info_url_title'] = $data['webinars_info_url_title'][$i];
                $arr1['video_type'] = $data['video_type'][$i];
                $arr1['video_uri'] = $data['video_uri'][$i];
                $arr1['youtube_url'] = $data['youtube_url'][$i];

                if ($products == $pro) {
                    array_push($arr, $arr1);
                }
            

        }
    } elseif ($_POST['products'] == "" && $_POST['industries'] == "") {

        $count = count($data['section_title']);
        for ($i = 0; $i < $count; $i++) {
            

                $industries = $data['industries'][$i];
                $products = $data['products'][$i];

                $arr1 = array();
           
                $arr1['section_title'] = $data['section_title'][$i];
                $arr1['webinars_info_front_image'] = $data['webinars_info_front_image'][$i];
                $arr1['webinars_info_front_logo'] = $data['webinars_info_front_logo'][$i];

                $arr1['webinars_info_url_title'] = $data['webinars_info_url_title'][$i];
                $arr1['video_type'] = $data['video_type'][$i];
                $arr1['video_uri'] = $data['video_uri'][$i];
                $arr1['youtube_url'] = $data['youtube_url'][$i];

                
                array_push($arr, $arr1);
                
            

        }
    }

    $dataxx = array();

    $start = ($page - 1) * $per_page;

    for ($i = $start; $i < $start + $per_page; $i++) {

        array_push($dataxx, $arr[$i]);
    }

    $cnt = 0;
    foreach ($dataxx as $key) {
        if (empty($key)) {
            unset($dataxx[$cnt]);
        }
        $cnt++;
    }

    $cnt = 0;
    foreach ($dataxx as $key) {

        $section_title = strip_tags($key['section_title']);
        $webinars_info_front_image = strip_tags($key['webinars_info_front_image']);
        $webinars_info_front_logo = strip_tags($key['webinars_info_front_logo']);

        $webinars_info_url_title = strip_tags($key['webinars_info_url_title']);

        $video_uri = '';
        $youtube_url = '';

        $video_type = $key['video_type'];

        if ($video_type == "video") {
            $video_uri = $key['video_uri'];

        } elseif ($video_type == "youtube") {
            $youtube_url = $key['youtube_url'];
        }
        
         if($cnt==0)
        {
        $html .= '<div class="foodBoxSection flex">';
        }
        elseif($cnt%3==0)
        {
            $html.= '</div>';
            $html .= '<div class="foodBoxSection flex row2">';
        } 

        if ($webinars_info_front_logo != "") {
            $html .= '<div class="foodBox flex"><div class="foodBoxLogo"><img class="img-fluid" src="' . $webinars_info_front_logo . '" alt=" " /></div>
        <div class="foodBoxContent flex"><img class="img-fluid" src="' . $webinars_info_front_image . '" alt="" /><h3 class="dinProStd waterText">' . $section_title . '</h3><a class="aboutButton violetText center" data-toggle="modal" data-target="#myModal" data-id="' . $cnt . '">' . $webinars_info_url_title . ' <i class="fa fa-arrow-right" aria-hidden="true"></i></a></div></div><input type="hidden" name="video_type_' . $cnt . '" id="video_type_' . $cnt . '" value="' . $video_type . '"><input type="hidden" name="video_uri_' . $cnt . '" id="video_uri_' . $cnt . '" value="' . $video_uri . '"><input type="hidden" name="youtube_url_' . $cnt . '" id="youtube_url_' . $cnt . '" value="' . $youtube_url . '">';
        } else {
            $html .= '<div class="foodBox flex"><div class="foodBoxLogo" style="height:80px;"></div>
            <div class="foodBoxContent flex"><img class="img-fluid" src="' . $webinars_info_front_image . '" alt="" /><h3 class="dinProStd waterText">' . $section_title . '</h3><a class="aboutButton violetText center" data-toggle="modal" data-target="#myModal" data-id="' . $cnt . '">' . $webinars_info_url_title . ' <i class="fa fa-arrow-right" aria-hidden="true"></i></a></div></div><input type="hidden" name="video_type_' . $cnt . '" id="video_type_' . $cnt . '" value="' . $video_type . '"><input type="hidden" name="video_uri_' . $cnt . '" id="video_uri_' . $cnt . '" value="' . $video_uri . '"><input type="hidden" name="youtube_url_' . $cnt . '" id="youtube_url_' . $cnt . '" value="' . $youtube_url . '">';
        }

        $cnt++;

    }

    $countxx = count($arr);

    // This is where the magic happens
    $no_of_paginations = ceil($countxx / $per_page);

    if ($cur_page >= 7) {
        $start_loop = $cur_page - 3;
        if ($no_of_paginations > $cur_page + 3) {
            $end_loop = $cur_page + 3;
        } else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
            $start_loop = $no_of_paginations - 6;
            $end_loop = $no_of_paginations;
        } else {
            $end_loop = $no_of_paginations;
        }
    } else {
        $start_loop = 1;
        if ($no_of_paginations > 7) {
            $end_loop = 7;
        } else {
            $end_loop = $no_of_paginations;
        }

    }

    // Pagination Buttons logic
    $pag_container .= "
     <div class='cvf-universal-pagination'>
         <ul>";

    for ($i = $start_loop; $i <= $end_loop; $i++) {

        if ($cur_page == $i) {
            $pag_container .= "<li p='$i' class = 'selected' >{$i}</li>";
        } else {
            $pag_container .= "<li p='$i' class='active'>{$i}</li>";
        }

    }

    $pag_container = $pag_container . "
         </ul>
     </div>";

    $arr2 = array();

      if ($html != "") {
        $arr2['msg'] = $html;
    } else {
         $arr2['msg'] = '<h1 class="text-center">No Result Found</h1>';
    }
    
    

    $arr2['pag_container'] = '<div class = "cvf-pagination-nav">' . $pag_container . '</div>';

    echo json_encode($arr2);
    exit();

}




?>
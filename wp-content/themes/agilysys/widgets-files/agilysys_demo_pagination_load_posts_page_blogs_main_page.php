<?php
add_action('wp_ajax_demo-pagination-load-posts-page-blogs-main-page', 'agilysys_demo_pagination_load_posts_page_blogs_main_page');

add_action('wp_ajax_nopriv_demo-pagination-load-posts-page-blogs-main-page', 'agilysys_demo_pagination_load_posts_page_blogs_main_page');

function agilysys_demo_pagination_load_posts_page_blogs_main_page()
{
    if (isset($_POST['page'])) {
        // Sanitize the received page
        $page = sanitize_text_field($_POST['page']);
        $cur_page = $page;
        $page -= 1;
        // Set the number of results to display
        $per_page = $_POST['per_page'];

        $start = $page * $per_page;

        $idObj = get_category_by_slug('blogs');
        $id = $idObj->term_id;

// the query
        $all_blog_posts = new WP_Query(array(
            'post_type' => 'post',
            'category__in' => array($id),
            'post_status' => 'publish',
            'orderby' => 'post_date',
            'order' => 'DESC',
            'posts_per_page' => $per_page,
            'offset' => $start,
        ));

        $count = new WP_Query(
            array(
                'post_type' => 'post',
                'post_status ' => 'publish',

                'category__in' => array($id),
                'posts_per_page' => -1,
            )
        );

        $msg = '';

        foreach ($all_blog_posts->posts as $key) {

            $image = wp_get_attachment_image_src(get_post_thumbnail_id($key->ID), 'single-post-thumbnail');

            $image[0] = preg_replace('/\s+/', '', $image[0]);

            $post_title = html_entity_decode($key->post_title);

            $post_content = substr(strip_tags(html_entity_decode($key->post_content)), 0, 100);

            $post_date = date("F d,Y", strtotime($key->post_date));
            $postyy = get_post($key->ID);
            $slug = $postyy->post_name;

            $facebook_link = get_post_meta($key->ID, 'facebook_link');

            $twitter_link = get_post_meta($key->ID, 'twitter_link');
            $linkedin_link = get_post_meta($key->ID, 'linkedin_link');

            $msg .= '<div class="blogBox flex"><div class="blogBoxImg" style="background:url('.$image[0].');background-repeat:no-repeat;background-position:center;background-size:cover;">
           
            </div>
            
              <div class="blogBoxcontent"><p class="blackText">' . $post_date . '</p><a href="' . home_url($slug) . '/"><h3 class="dinProStd greenText">' . $post_title . '</h3></a><p>' . $post_content . '</p><div class="blogSliderContentLink flex"><a href="' . home_url($slug) . '/" class="aboutButton violetText" > Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a><div class="blogSliderContentsocial">

            <a href="' . $facebook_link[0] . '"><i class="fa fa-facebook" aria-hidden="true" ></i></a>
            <a href="' . $twitter_link[0] . '"><i class="fa fa-twitter" aria-hidden="true"></i></a>
            <a href="' . $linkedin_link[0] . '"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            <i class="fa fa-print" aria-hidden="true"></i>

            </div></div></div></div>';

        }

        $countxx = count($count->posts);

        // This is where the magic happens
        $no_of_paginations = ceil($countxx / $per_page);

        if ($cur_page >= 7) {
            $start_loop = $cur_page - 3;
            if ($no_of_paginations > $cur_page + 3) {
                $end_loop = $cur_page + 3;
            } else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
                $start_loop = $no_of_paginations - 6;
                $end_loop = $no_of_paginations;
            } else {
                $end_loop = $no_of_paginations;
            }
        } else {
            $start_loop = 1;
            if ($no_of_paginations > 7) {
                $end_loop = 7;
            } else {
                $end_loop = $no_of_paginations;
            }

        }

        // Pagination Buttons logic
        $pag_container .= "
        <div class='cvf-universal-pagination'>
            ";

        if ($cur_page > 1) {
            $pre = $cur_page - 1;
            $pag_container .= "<a p='$pre' href='javascript:void(0);' class='homeBannerButton whiteText leftArrow' ><i class='fa fa-arrow-left' aria-hidden='true'></i>  Previous Post </a>";
        } else {
            $pag_container .= "<a href='javascript:void(0);' class='homeBannerButton whiteText leftArrow' ><i class='fa fa-arrow-left' aria-hidden='true'></i>  Previous Post </a>";
        }

        if ($cur_page < $no_of_paginations) {
            $nex = $cur_page + 1;
            $pag_container .= "<a p='$nex' href='javascript:void(0);' class='homeBannerButton whiteText leftArrow' ><i class='fa fa-arrow-right' aria-hidden='true'></i>  Next Posts </a>";
        } else {
            $pag_container .= "<a href='javascript:void(0);' class='homeBannerButton whiteText leftArrow' ><i class='fa fa-arrow-right' aria-hidden='true'></i>  Next Posts </a>";
        }

        $pag_container = $pag_container . "
          
        </div>";

        $arr = array();

        $arr['msg'] = $msg;

        $arr['pag_container'] = '<div class = "cvf-pagination-nav">' . $pag_container . '</div>';

        echo json_encode($arr);

        ?>

<?php

    }
    exit();
}



?>
<?php

add_action("admin_init", "download_csvxx");

function download_csvxx() {

  if (isset($_POST['download_csvxx'])) {

    global $wpdb;

    $sql = "SELECT * FROM `agsweb_signup_form`";

    $rows = $wpdb->get_results($sql, 'ARRAY_A');

    if ($rows) {

        $csv_fields = array();
        $csv_fields[] = "first_column";
        $csv_fields[] = 'second_column';

        $output_filename = 'signup_form' .'.csv';
        $output_handle = @fopen('php://output', 'w');

        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Content-Description: File Transfer');
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename=' . 
        $output_filename);
        header('Expires: 0');
        header('Pragma: public');

        $first = true;
       // Parse results to csv format
        foreach ($rows as $row) {

       // Add table headers
            if ($first) {

               $titles = array();

                foreach ($row as $key => $val) {

                    $titles[] = $key;

                }

                fputcsv($output_handle, $titles);

                $first = false;
            }

            $leadArray = (array) $row; // Cast the Object to an array
            // Add row to file
            fputcsv($output_handle, $leadArray);
        }

        //echo '<a href="'.$output_handle.'">test</a>';

        // Close output file stream
        fclose($output_handle);

        die();
    }
  }
}





if(is_admin())
{
    new Agilysys_Wp_List_Table1();
}

/**
 * Paulund_Wp_List_Table class will create the page to load the table
 */
class Agilysys_Wp_List_Table1
{
    /**
     * Constructor will create the menu item
     */
    public function __construct()
    {
        add_action( 'admin_menu', array($this, 'add_menu_example_list_table_page1' ));
    }

    /**
     * Menu item will allow us to load the page to display the table
     */
    public function add_menu_example_list_table_page1()
    {
        add_menu_page( 'Footer Signup Form List', 'Footer Signup Form List', 'manage_options', 'footer-signup-form.php', array($this, 'list_table_page1') );
    }

    /**
     * Display the list table page
     *
     * @return Void
     */
    public function list_table_page1()
    {

        
      
        ?>
            <div class="wrap">
                
                <form name="export_to_csv" method="POST" action="<?php echo site_url();?>/wp-admin/admin.php?page=footer-signup-form.php">
                    
                    <input type="hidden" name="download_csvxx" value="1">
                    <input type="submit" name="submit" id="submit" value="Export To CSV">  
                    
                </form>
          
                <div id="icon-users" class="icon32"></div>
                <h2>Footer Signup Form List Page</h2>
                
                <?php
                        $exampleListTable = new Agilysys_List_Table1();
        $exampleListTable->prepare_items();
                
                ?>
                <?php $exampleListTable->display(); ?>
            </div>
        <?php
    }
}

// WP_List_Table is not loaded automatically so we need to load it in our application
if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

/**
 * Create a new table class that will extend the WP_List_Table
 */
class Agilysys_List_Table1 extends WP_List_Table
{
    

    
    /**
     * Prepare the items for the table to process
     *
     * @return Void
     */
    public function prepare_items()
    {
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();

        $data = $this->table_data();
        usort( $data, array( &$this, 'sort_data' ) );

        $perPage = 5;
        $currentPage = $this->get_pagenum();
        $totalItems = count($data);

        $this->set_pagination_args( array(
            'total_items' => $totalItems,
            'per_page'    => $perPage
        ) );

        $data = array_slice($data,(($currentPage-1)*$perPage),$perPage);

        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = $data;
    }

    /**
     * Override the parent columns method. Defines the columns to use in your listing table
     *
     * @return Array
     */
    public function get_columns()
    {
        $columns = array(
            'id'          => 'ID',
            'email'        => 'Email',
        );

        return $columns;
    }

    /**
     * Define which columns are hidden
     *
     * @return Array
     */
    public function get_hidden_columns()
    {
        return array();
    }

    /**
     * Define the sortable columns
     *
     * @return Array
     */
    public function get_sortable_columns()
    {
        return array('title' => array('title', false));
    }

    /**
     * Get the table data
     *
     * @return Array
     */
    private function table_data()
    {
        $data = array();
        
        
        global $wpdb;
        
        $sql = "SELECT * FROM `agsweb_signup_form`";
        
        $results = $wpdb->get_results($sql);
        
    
        
        
        foreach($results as $key)
        {
            $arr1 = array();
            $arr1['id'] = $key->id;
     
            $arr1['email'] = $key->email;
            
            array_push($data,$arr1);
        }

        
      

      

        return $data;
    }

    /**
     * Define what data to show on each column of the table
     *
     * @param  Array $item        Data
     * @param  String $column_name - Current column name
     *
     * @return Mixed
     */
    public function column_default( $item, $column_name )
    {
        switch( $column_name ) {
            case 'id':
           
            case 'email':
                return $item[ $column_name ];

            default:
                return print_r( $item, true ) ;
        }
    }

    /**
     * Allows you to sort the data by the variables set in the $_GET
     *
     * @return Mixed
     */
    private function sort_data( $a, $b )
    {
        // Set defaults
        $orderby = 'email';
        $order = 'asc';

        // If orderby is set, use this as the sort column
        if(!empty($_GET['orderby']))
        {
            $orderby = $_GET['orderby'];
        }

        // If order is set use this as the order
        if(!empty($_GET['order']))
        {
            $order = $_GET['order'];
        }


        $result = strcmp( $a[$orderby], $b[$orderby] );

        if($order === 'asc')
        {
            return $result;
        }

        return -$result;
    }
    
    
}



?>
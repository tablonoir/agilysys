<?php


add_action('wp_ajax_demo-pagination-search-this', 'agilysys_demo_pagination_search_this');

add_action('wp_ajax_nopriv_demo-pagination-search-this', 'agilysys_demo_pagination_search_this');

function agilysys_demo_pagination_search_this()
{
    $search_term = $_POST['s'];

    $idObj = get_category_by_slug('blogs');
    $id = $idObj->term_id;

    //$query = new WP_Query( 's='.$s );
    $args = array(
        'post_type' => 'post',
        's' => $search_term,
        'category__in' => array($id),
        'post_status' => 'publish',
        'orderby' => 'title',
        'order' => 'DESC',
    );
    $wp_query = new WP_Query($args);

    $posts = $wp_query->posts;

    $html = '<ul>';

    foreach ($posts as $key) {

        $postyy = get_post($key->ID);
        $slug = $postyy->post_name;
        $html .= '<li><a href="' . home_url($slug) . '/">' . $key->post_title . '</a></li>';
    }

    $html .= '</ul>';

    echo $html;
    exit();
}


?>
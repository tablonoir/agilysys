<?php


get_header();

$category = get_queried_object();


$year     = get_query_var('year');
$monthnum = get_query_var('monthnum');

if($year!="" && $monthnum!="")
{
    $date = $year."-".$monthnum;
}
else
{
    $date = '';
}




$post_slug = $category->slug;
global $page;



global $wpdb;
$blog_page = $wpdb->get_results( "SELECT ID FROM $wpdb->posts WHERE post_name = 'blogs' AND post_type='page'" );


global $wp_query;
$post_id = $blog_page[0]->ID;

$image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'single-post-thumbnail');

$image[0] = preg_replace('/\s+/', '', $image[0]);

$menu_name = 'header-menu'; 

if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
    $the_menu = wp_get_nav_menu_object( $locations[ $menu_name ] );

    $the_menu_items = wp_get_nav_menu_items($the_menu->term_id);
   
}

$about_id = 0;

foreach ($the_menu_items as $key) {

    if ($key->title == 'Resources') {
        $about_id = $key->ID;
    }
}

$arr = array();

foreach ($the_menu_items as $key) {

    if ($key->menu_item_parent == $about_id) {
        array_push($arr, $key);
    }

}

?>



<div class="container-fluid" id="resource">
    <div class="row bloginnerBanner">
        <div class="col-md-1 col-sm-12 col-lg-1 col-xs-1"> 
                <div class="social-menu-icons flex">
                    <a href="<?php echo get_theme_mod('facebook_link'); ?>"><img
                            src="<?php echo get_theme_mod('facebook_image_settings'); ?>" class="img-fluid" alt="fb"></a>
                    <a href="<?php echo get_theme_mod('twitter_link'); ?>"><img
                            src="<?php echo get_theme_mod('twitter_image_settings'); ?>" class="img-fluid" alt="twitter"></a>
                    <a href="<?php echo get_theme_mod('linkedin_link'); ?>"><img
                            src="<?php echo get_theme_mod('linkedin_image_settings'); ?>" class="img-fluid" alt="linked"></a>
                    <a href="<?php echo get_theme_mod('instagram_link'); ?>"><img
                            src="<?php echo get_theme_mod('instagram_image_settings'); ?>" class="img-fluid"
                            alt="instagram"></a>
                    <a href="<?php echo get_theme_mod('pinterest_link'); ?>"><img
                            src="<?php echo get_theme_mod('pinterest_image_settings'); ?>" class="img-fluid" alt="volly"></a>

                    <div class="bloginnerSocialText dinproBld blackText flex">
                        <p><?php echo get_theme_mod('follow_us_text'); ?></p>
                    </div>
                </div> 

        </div>

        <div class="col-sm-12 col-md-11 col-lg-11 col-xs-11" style="padding:0px;margin-left: -35px;">
            <div class=" "
                style='background: url(<?php echo $image[0]; ?>) no-repeat !important;background-size: cover !important;background-position: center !important; height: 460px;'>
                <div class="row">
                        <div class="col-sm-3 col-md-3 col-lg-3 col-xs-3 bloginnerMaxwid">
                            <div class="bloginnermenu" id="resource">
                                <h4 class="dinProStd whiteText greenBG">RESOURCES</h4>
                                <ul class="dinproBld blackText">
                                    <?php
                                        foreach ($arr as $key) {

                                                ?>
                                                    <li><a href="<?php echo $key->url; ?>"><?php echo $key->title; ?></a></li>

                                                    <?php
                                        }
                                            ?>
                                </ul>
                            </div>
                            <div class="bloginnerSoludemo" id="lms">
                                <div class="homeContactText">
                                    <h5 class="dinProStd greenText"><?php echo get_theme_mod('contact_us_text'); ?></h5>
                                    <p class="blackText"><?php echo get_theme_mod('mobile_number'); ?></p>
                                </div>
                                <div class="homeGetDemo">
                                    <a class="greenText"
                                        href="<?php echo get_theme_mod('get_a_demo_link'); ?>"><?php echo get_theme_mod('get_a_demo_text'); ?> <i
                                            class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </div>
                            </div> 
                            

                        <div class="col-sm-9 col-md-9 col-lg-9 col-xs-9">
                            <div class="bloginnerBannertxt whiteText">
                                <h1 class="dinProStd"><?php echo strtoupper($category->name);?></h1>
                            </div>
                        </div>
                    </div> 
                </div>

            </div>
        </div>
    </div>
</div>




<style>
.cvf_pag_loading {
    padding: 20px;
}

.cvf-universal-pagination ul {
    margin: 0;
    padding: 0;
}

.cvf-universal-pagination ul li {
    display: inline;
    margin: 3px;
    padding: 4px 8px;
    background: #FFF;
    color: black;
}

.cvf-universal-pagination ul li.active:hover {
    cursor: pointer;
    background: #1E8CBE;
    color: white;
}

.cvf-universal-pagination ul li.inactive {
    background: #7E7E7E;
}

.cvf-universal-pagination ul li.selected {
    background: #1E8CBE;
    color: white;
}

#pagination {
    float: left;
    margin-left: 20%;
}


.loader {
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3498db;
    width: 120px;
    height: 120px;
    -webkit-animation: spin 2s linear infinite;
    /* Safari */
    animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
    0% {
        -webkit-transform: rotate(0deg);
    }

    100% {
        -webkit-transform: rotate(360deg);
    }
}

@keyframes spin {
    0% {
        transform: rotate(0deg);
    }

    100% {
        transform: rotate(360deg);
    }
}
</style>




<section class="blogContainer flex" style="margin-top:10px;">
    <div class="blogBoxSection" id="articleList">



    </div>
    <div class="blogSideBar">
    <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('blogs-page')): ?>




    <?php endif;?>
    </div>
</section>

<!-- <section class="blogLink center orangeBG flex">
    <a href="#" class="homeBannerButton whiteText leftArrow" ><i class="fa fa-arrow-left" aria-hidden="true"></i>  Previous Post </a>
    <a href="#" class="homeBannerButton whiteText" > View All <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
</section> -->
<div id="pagination">



</div>


<script type="text/javascript">
jQuery(document).ready(function($) {
    // This is required for AJAX to work on our page



    // Load page 1 as the default
    cvf_load_all_posts(1);




    jQuery(document).on('click', '#pagination .cvf-universal-pagination li.active', function() {

        var page = jQuery(this).attr('p');
        cvf_load_all_posts(page);
    });
});


function cvf_load_all_posts(page) {
    var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';



    // Data to receive from our server
    // the value in 'action' is the key that will be identified by the 'wp_ajax_' hook
    var data = {
        page: page,
        action: "demo-pagination-load-posts-page-blogs-main-page1",
        per_page: 3,
        post_slug:'<?php echo $post_slug;?>',
        date:'<?php echo $date;?>'

    };
    jQuery('.loader').show();

    // Send the data
    jQuery.post(ajaxurl, data, function(response) {


        var dataxx = JSON.parse(response);

        jQuery("#articleList").html(dataxx.msg);

        jQuery("#pagination").html(dataxx.pag_container);


        jQuery('.loader').hide();

    });
}
</script>











<section class="homeFooterImg" style="margin-top:40px;background: url(https://abiteoffrance.com/agilysys2//wp-content/uploads/2020/09/home-footer-img.jpg); background-repeat:no-repeat;background-size: cover; ">
        <!--<div class="homeFooterImgTopBG"></div>-->
    <div class="homeFooterImgBG"></div>

</section>





<?php get_footer();?>
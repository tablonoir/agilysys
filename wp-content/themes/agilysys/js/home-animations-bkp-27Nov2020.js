$(document).ready(function() {


//brands page slider js
var homeBrandSwiper = new Swiper('.homeBrandSwiper .swiper-container', {
	direction: 'horizontal',
	visibilityFullFit: true,
	loop: true,
        spaceBetween: 0,
	slidesPerView: 5,
	speed: 500,
        autoplay: {
            delay: 5000,
          },

	breakpoints: {
		480: {
			slidesPerView: 1
		},
		740: {
			slidesPerView: 2
		},
		960: {
			slidesPerView: 3
		},
		1280: {
			slidesPerView: 5
		},
	}
});

//when hover sliders will move
$(".homeBrandSwiper .swiper-container").hover(function() {
    (this).swiper.autoplay.start();
}, function() {
    (this).swiper.autoplay.stop();
});
 

//solution slider
var homeSolutionSlider = new Swiper('.homeSolutionSlider .swiper-container', {
      slidesPerView: 4,
      spaceBetween: 35,
      
      pagination: {
        el: '.homeSolutionSlider .swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.homeSolutionSlider .swiper-button-next',
        prevEl: '.homeSolutionSlider .swiper-button-prev',
      },
    breakpoints: {
        640: {
          slidesPerView: 1,
          spaceBetween: 20,
        },
        768: {
          slidesPerView: 3,
          spaceBetween: 40,
        },
        1024: {
          slidesPerView: 4,
          spaceBetween: 50,
        },
      }
    });


//Home-experiences SLIDER  
    var homeLeaderVideo = new Swiper('.homeLeaderVideo .swiper-container', {
        speed: 600,
        spaceBetween: 44,
        initialSlide: 0,        
        //autoHeight: true,
        // Optional parameters
        direction: 'horizontal',
        loop: true,        
        autoplay: false,
        autoplayStopOnLast: false, // loop false also      
        effect: 'slide',
        // Distance between slides in px.
        spaceBetween: 10,
        //
        slidesPerView: 2,
        //
        centeredSlides: true,
        //
        slidesOffsetBefore: 0,
        //
        grabCursor: true,
        
        navigation: {
        nextEl: '.homeLeaderVideo .swiper-button-next',
        prevEl: '.homeLeaderVideo .swiper-button-prev',
        },

        breakpoints: {

            640: {
                slidesPerView: 1,
                spaceBetween: 20
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        }
    })

    //End of home-experiences SLIDER  

    
    var $links = $('.homeHospitalityButton a');
        $links.click(function(){
           $links.removeClass('activeButton');
           $(this).addClass('activeButton');
    });
    
    //desktop
    $('#gustButton').click(function(){
           $('#gust').show();
            $('#property ').hide(); 
    });

    $('#propertyButton').click(function(){
           $('#property').show();
            $('#gust').hide(); 
    });
    
   
    //tab and mobile 
    $('#gustButtonMobile').click(function(){
           $('.homeHospitalityMobile.guest').show();
            $('.homeHospitalityMobile.property').hide(); 
    });

    $('#propertyButtonMobile').click(function(){
           $('.homeHospitalityMobile.property').show();
           $('.homeHospitalityMobile').addClass('flex');
            $('.homeHospitalityMobile.guest').hide(); 
    });
    
    
    
    /* home reviews and case studies */

 var homeReviewTestimonial  = new Swiper('.homeReviewTestimonial .swiper-container', {
      spaceBetween: 30,
      pagination: {
        el: '.homeReviewTestimonial .swiper-pagination',
        clickable: true,
      },
       navigation: {
        nextEl: '.homeReviewTestimonial .swiper-button-next',
        prevEl: '.homeReviewTestimonial .swiper-button-prev',
      },
    });
//    background image change in review 
    
   var homeBGInnerSwipe = new Swiper('.homeReviewCaseBGImg .swiper-container', {     
        slidesPerView:1,
        touchRatio: 0.2,
});

//homeReviewTestimonial.controller.control = homeBGInnerSwipe;
//homeBGInnerSwipe.controller.control = homeReviewTestimonial;
//    


});




        jQuery(document).ready(function($){  //Open media window for select image
  var mediaUploader;
  jQuery('body').on("click",'.sisw_upload_image_media',function(e) {
    e.preventDefault();
    siswtempthis=$(this);
      
      
    // If the uploader object has already been created, reopen the dialog
      if (mediaUploader) {
      mediaUploader.open();
      return;
      }
    // Extend the wp.media object
    mediaUploader = wp.media.frames.file_frame = wp.media({
      title: 'Select Slider Images',
      button: {
      text: 'Add'
    }, multiple: true });

    // When a file is selected, grab the URL and set it as the text field's value
    mediaUploader.on('select', function() {
      attachments = mediaUploader.state().get('selection').toJSON();
      //console.log(attachments.length);

      for (i in attachments) {
                attachment= attachments[i];
        jQuery('.sisw_image_input_field.active_image_section ').val(attachment.url);
        jQuery('img.sisw_admin_image_preview.active_admin_preview').addClass('active').attr('src',attachment.url);
       // jQuery('.active_widget_form .widget-control-actions input[type="submit"]').removeAttr('disabled');
        var current_input_name = siswtempthis.parents(".widget").find(".sisw_temp_image_name").val();
        var current_input_link = siswtempthis.parents(".widget").find(".sisw_temp_image_link").val();
        var current_input_tab = siswtempthis.parents(".widget").find(".sisw_temp_image_tab").val();
        
          siswtempthis.parents(".widget").find('.sisw_temp_text_val').trigger("change");
        if (attachment.url.match(/.(jpg|jpeg|png|gif)$/i))
        {
        var new_element = `<tr class='sisw_individual_image_section'>
        <td width="50px;"><span class="sisw_drag_Section">☰</span> <a href=`+attachment.url+` target="_blank"><img src='`+attachment.url+`' class='sisw_admin_image_preview'></a></td>
        <td><input class='' name='`+current_input_name+`' value=`+attachment.id+` type='hidden'>
        <input class="sisw_image_input_field" name='`+current_input_link+`' type='text' value='' placeholder='Link (optional)'><br>`+sisw_admindata.newtab_string+` <select name='`+current_input_tab+`' class='sisw_opentab' style="display: none;">
                                        <option value="">`+sisw_admindata.sametab_value+`</option>
                                        <option value="newtab">`+sisw_admindata.newtab_value+`</option>
                                </select>
                                <input type="checkbox" name="ssiw_checkurl" value="newtab" class="ssiw_checkurl">
        </td><td><a class="sisw_remove_field_upload_media_widget" href="javascript:void(0)">×</a>
                            </td></tr>`;

          siswtempthis.parents(".widget").find('.sisw_multi_image_slider_table_wrapper tbody').append(new_element);
          }

          var current_imag_length = siswtempthis.parents(".widget").find('.sisw_multi_image_slider_table_wrapper .sisw_individual_image_section').length;
          if(current_imag_length >1){
            siswtempthis.parents(".widget").find('.sisw_multi_image_slider_setting').show();
          }
          else{
            siswtempthis.parents(".widget").find('.sisw_multi_image_slider_setting').hide();       
          }
          if(current_imag_length>0){
            siswtempthis.parents(".widget").find('.sisw_no_images').hide();
          }
          else
          {
            siswtempthis.parents(".widget").find('.sisw_no_images').show();
          }
      }
      
    });
    // Open the uploader dialog
    mediaUploader.open();




  });

});


jQuery(document).ready(function($){ // Remove the image section
    jQuery('body').on("click",'a.sisw_remove_field_upload_media_widget',function(){
    jQuery(this).parents(".widget").find('input[type="submit"]').removeAttr('disabled');
    jQuery(this).parents(".widget").find('.sisw_temp_text_val').trigger("change");
    jQuery(this).parents('table').addClass('countrows');
    var temppobj=jQuery(this).parents(".widget");
     var current_imag_length = temppobj.find(".sisw_multi_image_slider_table_wrapper .sisw_individual_image_section").length;
    if(current_imag_length ==1)
    {
      var resultsisw = confirm(sisw_admindata.confirm_message);
      if (resultsisw) {
      jQuery(this).parents('tr').remove();
      }
    }
    else
    {
      jQuery(this).parents('tr').remove();
    }
    current_imag_length = temppobj.find(".sisw_multi_image_slider_table_wrapper .sisw_individual_image_section").length;
        if(current_imag_length >1)
        {
         temppobj.find('.sisw_multi_image_slider_setting').show();
         }
        else
        {
          temppobj.find('.sisw_multi_image_slider_setting').hide();
         }
        if(current_imag_length>0){
            temppobj.find('.sisw_no_images').hide();
          }
          else
          {
            temppobj.find('.sisw_no_images').show();
          }

  });
 // 
        $('.sisw_temp_text_val').change(function(){
          $(".sisw_temp_text_val").val("abc");
        })

        
      $(document).on('click', '.ssiw_reset', function () {
          //$( this ).prevAll( ".sisw_image_input_field" ).val('');

      });

       $(document).on('click', '.ssiw_checkurl', function () {
       
        if($(this).is(':checked'))
        {
            $( this ).prev( ".sisw_opentab" ).val('newtab');
        }
        else
        {
            $( this ).prev( ".sisw_opentab" ).val('');
        }
          
          

      });
      
       var solution3columnSlider = new Swiper('.solution3columnSlider .swiper-container', {
      slidesPerView: 3,
      loop:true,
      spaceBetween: 30,
      pagination: {
        el: '.swiper-pagination',
        clickable: false,
      },
     navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
 
});

  
  var leaderSolutionsWidget = new Swiper(".leaderSolutionsWidget .swiper-container", {
    spaceBetween: 170,
    slidesPerView: 3,
    centeredSlides: true,
    roundLengths: true,
    loop: true,
    loopAdditionalSlides: 30,
    
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    },
       breakpoints: {
        640: {
          slidesPerView: 1,
          spaceBetween: 20,
        },
        768: {
          slidesPerView: 1,
          spaceBetween: 40,
        },
        1024: {
          slidesPerView: 1,
          spaceBetween: 50,
        },
        
      },
        on: {
    slideChange: function (el) {

    },

  },
      
  });
  
  leaderSolutionsWidget.on('slideChange', function () {
  jQuery('.yt_players').each(function () {
    this.contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*')
  });

  jQuery('video').trigger('pause');

});
  
    var homePosts = new Swiper(".homePosts .swiper-container", {
    spaceBetween: 170,
    slidesPerView: 3,
    centeredSlides: true,
    roundLengths: true,
    loop: true,
    loopAdditionalSlides: 30,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    },
        breakpoints: {
        640: {
          slidesPerView: 1,
          spaceBetween: 20,
        },
        768: {
          slidesPerView: 1,
          spaceBetween: 40,
        },
        1024: {
          slidesPerView: 1,
          spaceBetween: 50,
        },
      }
  });
  
  
  
  //about slider
    var aboutSlider = new Swiper('.aboutSlider .swiper-container', {
      slidesPerView: 2,
      spaceBetween: 30,
      slidesPerGroup: 2,
      loop: true,
      loopFillGroupWithBlank: true,
      pagination: {
        el: '.aboutSlider .swiper-pagination',
        clickable: true,
      },
        breakpoints: {
        640: {
          slidesPerView: 1,
          spaceBetween: 20,
        },
        768: {
          slidesPerView: 1,
          spaceBetween: 40,
        },
        1024: {
          slidesPerView: 1,
          spaceBetween: 50,
        },
      }
    });




var cusStoriesSlider = new Swiper('.cusStoriesSlider .swiper-container', {
    slidesPerView: 1,   
//    loop:true,
      navigation: {
        nextEl: '.cusStoriesSlider .swiper-button-next',
        prevEl: '.cusStoriesSlider .swiper-button-prev',
      },
    });
    
    

var blogSliderSection = new Swiper('.blogSliderSection .swiper-container', {
      pagination: {
        el: '.blogSliderSection .swiper-pagination',
      },
    });
    
    
    
   var propetyTitleSlider = new Swiper('.propetyTitleSlider .swiper-container', {
    pagination: {
      el: '.propetyTitleSlider .swiper-pagination',
    }, 
    navigation: {
      nextEl: '.propetyTitleSlider .swiper-button-next',
      prevEl: '.propetyTitleSlider .swiper-button-prev',
    },
  }); 
  
  
  
    var customerStoriesSliderSection = new Swiper('.customerStoriesSliderSection .swiper-container', {
        speed: 600,
        spaceBetween: 50,
        initialSlide: 0,        
        //autoHeight: true,
        // Optional parameters
        direction: 'horizontal',
        loop: true,        
        autoplay: true,
    //    autoplayStopOnLast: false, // loop false also      
        effect: 'slide',
        // Distance between slides in px.
        spaceBetween: 50,
        //
        slidesPerView: 2,
        //
//        centeredSlides: true,
        //
//        slidesOffsetBefore: 0,
        //
        grabCursor: true,
        
        navigation: {
        nextEl: '.customerStoriesSliderSection .swiper-button-next',
        prevEl: '.customerStoriesSliderSection .swiper-button-prev',
        },

        breakpoints: {

            640: {
                slidesPerView: 1,
                spaceBetween: 20
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        }
    })

    
    
    var customerSpotlightsSlider = new Swiper('.customerSpotlightsSlider .swiper-container', {
      navigation: {
        nextEl: '.customerSpotlightsSlider .swiper-button-next',
        prevEl: '.customerSpotlightsSlider .swiper-button-prev',
      },
    });
/* globals global */
jQuery(function($) {

    var url = global.ajax + "?action=search_site";
    jQuery(".search-autocomplete").autocomplete({
        source: url,
        minLength: 1,
        select: function(event, ui) {
            console.log("Selected: " + ui.item.value + " aka " + ui.item.id);

            document.location.href = ui.item.id;
        }
    });
});
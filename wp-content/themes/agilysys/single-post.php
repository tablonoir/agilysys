<?php

get_header();

global $post;
$post_slug = $post->post_name;
$id = $post->ID;
$postcat = get_the_category($post->ID);

$cat_array = array();

foreach ($postcat as $key) {
    array_push($cat_array, $key->category_nicename);
}

$count = count($postcat);

$category = $postcat[$count - 1]->name;

$the_slug = $post_slug;
$args = array(
    'name' => $the_slug,
    'post_type' => 'post',
    'post_status' => 'publish',
    'numberposts' => 1,
    'category__in' => array($postcat[$count - 1]->term_id),
);
$posts_data = get_posts($args);

$date = date("M d,Y", strtotime($posts_data[0]->post_date));
$post_title = $posts_data[0]->post_title;
$post_content = $posts_data[0]->post_content;

$args = array(
    'posts_per_page' => -1,
    'post_type' => 'post',
    'category__in' => array($postcat[$count - 1]->term_id),
);
$all_blog_posts = new WP_Query($args);

$post_id_arr = array();

$post_slug_arr = array();

foreach ($all_blog_posts->posts as $key => $post):

    $postyy = get_post($post->ID);
    $slug = $postyy->post_name;

    array_push($post_slug_arr, $slug);
    array_push($post_id_arr, $post->ID);
endforeach;

$keyxx = array_search($id, $post_id_arr);

$len = count($post_id_arr);

if ($keyxx != 0 && $keyxx + 1 != $len) {
    $prev = $post_slug_arr[$keyxx + 1];
    $next = $post_slug_arr[$keyxx - 1];
}
if ($keyxx == 0) {
    $prev = $post_slug_arr[$keyxx + 1];

}
if ($keyxx + 1 == $len) {
    $next = $post_slug_arr[$keyxx - 1];

}

$image = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'single-post-thumbnail');

$image[0] = preg_replace('/\s+/', '', $image[0]);

if (!in_array("blogs", $cat_array)) {

    $menu_name = 'header-menu';

    if (($locations = get_nav_menu_locations()) && isset($locations[$menu_name])) {
        $the_menu = wp_get_nav_menu_object($locations[$menu_name]);

        $the_menu_items = wp_get_nav_menu_items($the_menu->term_id);

    }

    $about_id = 0;

    foreach ($the_menu_items as $key) {

        if ($key->title == 'About') {
            $about_id = $key->ID;
        }
    }

    $arr = array();

    foreach ($the_menu_items as $key) {

        if ($key->menu_item_parent == $about_id) {
            array_push($arr, $key);
        }

    }
    ?>


<div class="container-fluid" id="resource">
    <div class="row bloginnerBanner">
      

        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
            <div class="bloginnerBg"
                style='background: url(<?php echo $image[0]; ?>) no-repeat !important;background-size: cover !important;background-position: center !important; height: 460px;'>
                    <div class="row">
                          
                        <div class="col-sm-3 col-md-3 col-lg-3 col-xs-3 bloginnerMaxwid">
                            <div class="bloginner-social-menu-icons flex">
                                <a href="<?php echo get_theme_mod('facebook_link'); ?>"><img
                                        src="<?php echo get_theme_mod('facebook_image_settings'); ?>" class="img-fluid" alt="fb"></a>
                                <a href="<?php echo get_theme_mod('twitter_link'); ?>"><img
                                        src="<?php echo get_theme_mod('twitter_image_settings'); ?>" class="img-fluid" alt="twitter"></a>
                                <a href="<?php echo get_theme_mod('linkedin_link'); ?>"><img
                                        src="<?php echo get_theme_mod('linkedin_image_settings'); ?>" class="img-fluid" alt="linked"></a>
                                <a href="<?php echo get_theme_mod('instagram_link'); ?>"><img
                                        src="<?php echo get_theme_mod('instagram_image_settings'); ?>" class="img-fluid"
                                        alt="instagram"></a>
                                <a href="<?php echo get_theme_mod('pinterest_link'); ?>"><img
                                        src="<?php echo get_theme_mod('pinterest_image_settings'); ?>" class="img-fluid" alt="volly"></a>
            
                                <div class="bloginnerSocialText dinproBld blackText flex">
                                    <p><?php echo get_theme_mod('follow_us_text'); ?></p>
                                </div>
                            </div> 
                            <div class="bloginnermenu" id="resource">
                                <h4 class="dinProStd whiteText greenBG">RESOURCES</h4>
                                <ul class="dinproBld blackText">
                                    <?php
                                        foreach ($arr as $key) {

                                                ?>
                                                    <li><a href="<?php echo $key->url; ?>"><?php echo $key->title; ?></a></li>

                                                    <?php
                                        }
                                            ?>
                                </ul>
                            </div>
                            <div class="bloginnerSoludemo" id="lms">
                                <div class="homeContactText">
                                    <h5 class="dinProStd greenText"><?php echo get_theme_mod('contact_us_text'); ?></h5>
                                    <p class="blackText"><?php echo get_theme_mod('mobile_number'); ?></p>
                                </div>
                                <div class="homeGetDemo">
                                    <a class="greenText"
                                        href="<?php echo get_theme_mod('get_a_demo_link'); ?>"><?php echo get_theme_mod('get_a_demo_text'); ?> <i
                                            class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </div>
                            </div>  

                        <div class="col-sm-9 col-md-9 col-lg-9 col-xs-9">
                            <div class="bloginnerBannertxt whiteText">
                                <h1 class="dinProStd">WHAT'S THE BUZZ IN HOSPITALITY?</h1>
                            </div>
                        </div>
                    </div> 
                </div>

            </div>
        </div>
    </div>
</div>



<div class="container-fluid bloginnerContent">
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-7">
            <div class=" ">
            <h2 class="dinProStd greenText h2"><?php echo $post_title; ?></h2> 
                    <div class="row blogInnerDaterow">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-6">
                            <div class="bloginnerDate">
                                <p class="blackText"><?php echo $date; ?></p>
                            </div>
                            <div class="bloginnerSocial blackText">
                            <a href="#" onclick="back_to_all();" class="violetText">Back to all <i class="fa fa-arrow-right"
                                        aria-hidden="true"></i></a>
                            </div>
                        </div> 
                    </div>  
                    <br/>
                    <div class="blogInnerContent">
                         
                        <?php echo html_entity_decode($post_content); ?>
                    </div> 

            </div>
        </div>
        <div class="col-lg-3">
            <div class="greyBG">
                <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('news-events')): ?>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid blogInnernextpost">

    <div class="row">
        <div class="col-md-12">
        <?php
            if ($keyxx != 0) {
                    ?>
                <a href="#" onClick="navigate_to_page('<?php echo $next; ?>');" class="homeBannerButton whiteText leftArrow"><i
                        class="fa fa-arrow-left" aria-hidden="true"></i> Newer
                    Posts </a>
                <?php
            }
                ?>

                <?php
            if ($keyxx + 1 != $len) {
                    ?>
                <a href="#" onClick="navigate_to_page('<?php echo $prev; ?>');" class="homeBannerButton whiteText leftArrow">Older
                    Posts <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                <?php
            }
        ?>
        </div>
    </div>
</div> 


<script>
function back_to_all() {
    document.location.href = 'test-main-blog-page/';
}

function navigate_to_page(slug) {

    document.location.href = slug + '/';
}
</script>

<?php

} elseif (in_array("blogs", $cat_array)) {

    $menu_name = 'header-menu';

    if (($locations = get_nav_menu_locations()) && isset($locations[$menu_name])) {
        $the_menu = wp_get_nav_menu_object($locations[$menu_name]);

        $the_menu_items = wp_get_nav_menu_items($the_menu->term_id);

    }

    $about_id = 0;

    foreach ($the_menu_items as $key) {

        if ($key->title == 'Resources') {
            $about_id = $key->ID;
        }
    }

    $arr = array();

    foreach ($the_menu_items as $key) {

        if ($key->menu_item_parent == $about_id) {
            array_push($arr, $key);
        }

    }

    $facebook_link = get_post_meta($id, 'facebook_link');

    $twitter_link = get_post_meta($id, 'twitter_link');
    $linkedin_link = get_post_meta($id, 'linkedin_link');
    ?>

<div class="container-fluid" id="resource">
    <div class="row bloginnerBanner">
        

        <div class="col-md-12 col-md-12 col-lg-12 col-xs-12">
            <div class="bloginnerBg"
                style='background: url(<?php echo $image[0]; ?>) no-repeat !important;background-size: cover;background-position: center; height: 460px;'>
                <div class="row">
                     
                        <div class="col-sm-3 col-md-3 col-lg-3 col-xs-3 bloginnerMaxwid">
                            <div class="bloginner-social-menu-icons flex">
                                <a href="<?php echo get_theme_mod('facebook_link'); ?>"><img
                                        src="<?php echo get_theme_mod('facebook_image_settings'); ?>" class="img-fluid" alt="fb"></a>
                                <a href="<?php echo get_theme_mod('twitter_link'); ?>"><img
                                        src="<?php echo get_theme_mod('twitter_image_settings'); ?>" class="img-fluid" alt="twitter"></a>
                                <a href="<?php echo get_theme_mod('linkedin_link'); ?>"><img
                                        src="<?php echo get_theme_mod('linkedin_image_settings'); ?>" class="img-fluid" alt="linked"></a>
                                <a href="<?php echo get_theme_mod('instagram_link'); ?>"><img
                                        src="<?php echo get_theme_mod('instagram_image_settings'); ?>" class="img-fluid"
                                        alt="instagram"></a>
                                <a href="<?php echo get_theme_mod('pinterest_link'); ?>"><img
                                        src="<?php echo get_theme_mod('pinterest_image_settings'); ?>" class="img-fluid" alt="volly"></a>
            
                                <div class="bloginnerSocialText dinproBld blackText flex">
                                    <p><?php echo get_theme_mod('follow_us_text'); ?></p>
                                </div>
                            </div> 
                            <div class="bloginnermenu" id="resource">
                                <h4 class="dinProStd whiteText greenBG">RESOURCES</h4>
                                <ul class="dinproBld blackText">
                                    <?php
                                        foreach ($arr as $key) {

                                                ?>
                                                    <li><a href="<?php echo $key->url; ?>"><?php echo $key->title; ?></a></li>

                                                    <?php
                                        }
                                            ?>
                                </ul>
                            </div>
                            <div class="bloginnerSoludemo" id="lms">
                                <div class="homeContactText">
                                    <h5 class="dinProStd greenText"><?php echo get_theme_mod('contact_us_text'); ?></h5>
                                    <p class="blackText"><?php echo get_theme_mod('mobile_number'); ?></p>
                                </div>
                                <div class="homeGetDemo">
                                    <a class="greenText"
                                        href="<?php echo get_theme_mod('get_a_demo_link'); ?>"><?php echo get_theme_mod('get_a_demo_text'); ?> <i
                                            class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </div>
                            </div>  
                        </div> 
                        <div class="col-sm-9 col-md-9 col-lg-9 col-xs-9">
                            <div class="bloginnerBannertxt whiteText">
                                <h1 class="dinProStd">WHAT'S THE BUZZ IN HOSPITALITY?</h1>
                            </div>
                        </div>
                    
                </div>

            </div>
        </div>
    </div>
</div>


   
<div class="container-fluid bloginnerContent">
    <div class="row">
        <div class="col-sm-2 col-md-2 col-lg-2 col-xs-2">

        </div>
        <div class="col-sm-6 col-md-6 col-lg-6 col-xs-6">
            <h2 class="dinProStd greenText h2"><?php echo $post_title; ?></h2>
            <div class="row blogInnerDaterow">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xs-6">
                    <div class="bloginnerDate">
                        <p class="blackText"><?php echo $date; ?></p>
                    </div>
                    <div class="bloginnerSocial blackText">
                        <a href="<?php echo $facebook_link[0]; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="<?php echo $twitter_link[0]; ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="<?php echo $linkedin_link[0]; ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        <a href="#"> <i class="fa fa-print" aria-hidden="true"></i></a>
                    </div>
                </div> 
            </div>
            <div class="blogInnerContent">
                <!-- <img class="img-fluid" src="<?php echo $image[0]; ?>" alt="" />-->
                <?php echo html_entity_decode($post_content); ?>
            </div>
        </div>
        <div class="col-sm-3 col-md-3 col-lg-3 col-xs-3"> 
            <div class="blogInnerinput">
                <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('blogs-page')): ?> 
                <?php endif;?>
            </div> 
            </div>
        </div>
        <div class="col-sm-1 col-md-1 col-lg-1 col-xs-1"> </div>
    </div>
</div>

 <div class="container-fluid blogInnernextpost">

     <div class="row">
         <div class="col-md-12">
            <?php
                if ($keyxx != 0) {
                        ?>
                    <a href="#" onClick="navigate_to_page('<?php echo $next; ?>');" class="homeBannerButton whiteText leftArrow"><i
                            class="fa fa-arrow-left" aria-hidden="true"></i> Newer
                        Posts </a>
                    <?php
                }
                    ?>

                    <?php
                if ($keyxx + 1 != $len) {
                        ?>
                    <a href="#" onClick="navigate_to_page('<?php echo $prev; ?>');" class="homeBannerButton whiteText leftArrow">Older
                        Posts <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                    <?php
                }
            ?>
         </div>
    </div>
</div>

 





<script>
function navigate_to_page(slug) {

    document.location.href = slug + '/';
}
</script>


<?php
}

?> 

 

<div class="fullwidth">
    <div class="row">
        <div class="col-lg-12">

            <?php

echo $footer;

?>
        </div>
    </div>
</div>

<?php get_footer();?>
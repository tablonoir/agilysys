//menu js
$(document).ready(function(){
    $(window).on('load', function(){
        if ($(document).scrollTop() > 50) {
            $('.header, .mainLogo, .headerNav').addClass('shrink');
        } else {
            $('.header, .mainLogo, .headerNav').removeClass('shrink');
        }
    });

    $(window).scroll(function(){
        if ($(document).scrollTop() > 50) {
            $('.header, .mainLogo, .headerNav').addClass('shrink');
        } else {
            $('.header, .mainLogo, .headerNav').removeClass('shrink');
        }
    });
    
    openNav();
    closeNav();       
});

function openNav() {
        document.getElementById("mobileNav").style.width = "35%";
    }

function closeNav() {
    document.getElementById("mobileNav").style.width = "0";
}

$(".headerNav ul li a#industriesSubMenu").hover(function(){
    $("#industSubMenuShow").show();
    $("#solSubMenuShow").hide();
    $("#resSubMenuShow").hide();
    $("#supSubMenuShow").hide();
    $("#aboutSubMenuShow").hide();
});

$(".headerNav ul li a#solutionsSubMenu").hover(function(){
    $("#solSubMenuShow").show();
    $("#industSubMenuShow").hide();
    $("#resSubMenuShow").hide();
    $("#supSubMenuShow").hide();
    $("#aboutSubMenuShow").hide();
});

$(".headerNav ul li a#resourcesSubMenu").hover(function(){
    $("#resSubMenuShow").show();
    $("#industSubMenuShow").hide();
    $("#solSubMenuShow").hide();    
    $("#supSubMenuShow").hide();
    $("#aboutSubMenuShow").hide();
});

$(".headerNav ul li a#supportSubMenu").hover(function(){
    $("#supSubMenuShow").show();
    $("#industSubMenuShow").hide();
    $("#solSubMenuShow").hide();    
    $("#resSubMenuShow").hide();
    $("#aboutSubMenuShow").hide();
});

$(".headerNav ul li a#aboutSubMenu").hover(function(){
    $("#aboutSubMenuShow").show();
    $("#industSubMenuShow").hide();
    $("#solSubMenuShow").hide();    
    $("#resSubMenuShow").hide();    
    $("#supSubMenuShow").hide();
});


//hamburger menu js start //
$(".hamburgerMenu").click(function(){
    $("#hamMegaMenu").slideToggle("slow");
    $("#mobileBG").addClass("noScroll");
  });

$(".hamMegaMenuList ul li").hover(
  function () {
    $(this).addClass("active");
  },
  function () {
    $(this).removeClass("active");
    $(".hamMegaMenuList ul li#industriesShow").removeClass("active");      
  }
);


$(".hamMegaMenuList ul li#industriesShow").hover(function(){
    $("#industries").show();
    $("#solutions").hide();
    $("#resources").hide();
    $("#support").hide();
    $("#about").hide();
    $("#myAgilysys").hide();
    
    $(".hamMegaMenuContanier").mouseover(function(){
        $(".hamMegaMenuList ul li#industriesShow a").addClass("menuActive");
        $(".hamMegaMenuList ul li#solutionsShow a").removeClass("menuActive");
        $(".hamMegaMenuList ul li#resourcesShow a").removeClass("menuActive");
        $(".hamMegaMenuList ul li#supportShow a").removeClass("menuActive");
        $(".hamMegaMenuList ul li#aboutShow a").removeClass("menuActive");
        $(".hamMegaMenuList ul li#myAgilysysShow a").removeClass("menuActive");
    });
      $(".hamMegaMenuContanier").mouseout(function(){
        $(".hamMegaMenuList ul li#industriesShow a").removeClass("menuActive");
        
    });
});

$(".hamMegaMenuList ul li#solutionsShow").hover(function(){
    $("#industries").hide();
    $("#solutions").show();
    $("#resources").hide();
    $("#support").hide();
    $("#about").hide();
    $("#myAgilysys").hide();
    
    $(".hamMegaMenuContanier").mouseover(function(){
        $(".hamMegaMenuList ul li#solutionsShow a").addClass("menuActive");
        $(".hamMegaMenuList ul li#industriesShow a").removeClass("menuActive");
        $(".hamMegaMenuList ul li#resourcesShow a").removeClass("menuActive");
        $(".hamMegaMenuList ul li#supportShow a").removeClass("menuActive");
        $(".hamMegaMenuList ul li#aboutShow a").removeClass("menuActive");
        $(".hamMegaMenuList ul li#myAgilysysShow a").removeClass("menuActive");
    });
      $(".hamMegaMenuContanier").mouseout(function(){
        $(".hamMegaMenuList ul li#solutionsShow a").removeClass("menuActive");
    });
});

$(".hamMegaMenuList ul li#resourcesShow").hover(function(){
    $("#industries").hide();
    $("#solutions").hide();
    $("#resources").show();
    $("#support").hide();
    $("#about").hide();
    $("#myAgilysys").hide();
    
    $(".hamMegaMenuContanier").mouseover(function(){
        $(".hamMegaMenuList ul li#resourcesShow a").addClass("menuActive");
        $(".hamMegaMenuList ul li#industriesShow a").removeClass("menuActive");
        $(".hamMegaMenuList ul li#solutionsShow a").removeClass("menuActive");
        $(".hamMegaMenuList ul li#supportShow a").removeClass("menuActive");
        $(".hamMegaMenuList ul li#aboutShow a").removeClass("menuActive");
        $(".hamMegaMenuList ul li#myAgilysysShow a").removeClass("menuActive");
    });
      $(".hamMegaMenuContanier").mouseout(function(){
        $(".hamMegaMenuList ul li#resourcesShow a").removeClass("menuActive");
    });
});

$(".hamMegaMenuList ul li#supportShow").hover(function(){
    $("#industries").hide();
    $("#solutions").hide();    
    $("#resources").hide();    
    $("#support").show();
    $("#about").hide();
    $("#myAgilysys").hide();
    
    $(".hamMegaMenuContanier").mouseover(function(){
        $(".hamMegaMenuList ul li#supportShow a").addClass("menuActive");        
        $(".hamMegaMenuList ul li#industriesShow a").removeClass("menuActive");
        $(".hamMegaMenuList ul li#solutionsShow a").removeClass("menuActive");
        $(".hamMegaMenuList ul li#resourcesShow a").removeClass("menuActive");
        $(".hamMegaMenuList ul li#aboutShow a").removeClass("menuActive");
        $(".hamMegaMenuList ul li#myAgilysysShow a").removeClass("menuActive");
    });
      $(".hamMegaMenuContanier").mouseout(function(){
        $(".hamMegaMenuList ul li#supportShow a").removeClass("menuActive");
    });
    
});

$(".hamMegaMenuList ul li#aboutShow").hover(function(){
    $("#industries").hide();
    $("#solutions").hide();    
    $("#resources").hide();
    $("#support").hide();
    $("#about").show();
    $("#myAgilysys").hide();
    
    $(".hamMegaMenuContanier").mouseover(function(){
        $(".hamMegaMenuList ul li#aboutShow a").addClass("menuActive");
        $(".hamMegaMenuList ul li#industriesShow a").removeClass("menuActive");
        $(".hamMegaMenuList ul li#solutionsShow a").removeClass("menuActive");
        $(".hamMegaMenuList ul li#resourcesShow a").removeClass("menuActive");
        $(".hamMegaMenuList ul li#supportShow a").removeClass("menuActive");
        $(".hamMegaMenuList ul li#myAgilysysShow a").removeClass("menuActive");
        
    });
      $(".hamMegaMenuContanier").mouseout(function(){
        $(".hamMegaMenuList ul li#aboutShow a").removeClass("menuActive");
    });
});

$(".hamMegaMenuList ul li#myAgilysysShow").hover(function(){
    $("#industries").hide();
    $("#solutions").hide();    
    $("#resources").hide();
    $("#support").hide();
    $("#about").hide();
    $("#myAgilysys").show();
    
    $(".hamMegaMenuContanier").mouseover(function(){
        $(".hamMegaMenuList ul li#myAgilysysShow a").addClass("menuActive");
        $(".hamMegaMenuList ul li#industriesShow a").removeClass("menuActive");
        $(".hamMegaMenuList ul li#solutionsShow a").removeClass("menuActive");
        $(".hamMegaMenuList ul li#resourcesShow a").removeClass("menuActive");
        $(".hamMegaMenuList ul li#supportShow a").removeClass("menuActive");
        $(".hamMegaMenuList ul li#aboutShow a").removeClass("menuActive");
    });
      $(".hamMegaMenuContanier").mouseout(function(){
        $(".hamMegaMenuList ul li#myAgilysysShow a").removeClass("menuActive");
    });
});

//hamburger submenu active js
$(".hamMenuLinks ul li a").hover(
  function () {
    $(this).addClass("active");
    $(".hamMenuListRow").addClass("active");    
  },
  function () {
    $(this).removeClass("active");
    $(".hamMenuListRow").removeClass("active");
    $(".hamMenuLinks ul li a#firstLink").removeClass("active");      
  }
);

//hamburger submenu image and content show js
 $('#industries .showIndustries').hover(function(){
      $('#industries .hamMenuImgSection').hide();
      $('#indust'+$(this).attr('target')).show();
});

$('#solutions .showSolutions').hover(function(){
      $('#solutions .hamMenuImgSection').hide();
      $('#sol'+$(this).attr('target')).show();
});

$('#resources .showResources').hover(function(){
      $('#resources .hamMenuImgSection').hide();
      $('#res'+$(this).attr('target')).show();
});

$('#support .showSupport').hover(function(){
      $('#support .hamMenuImgSection').hide();
      $('#sup'+$(this).attr('target')).show();
});

$('#about .showAbout').hover(function(){
      $('#about .hamMenuImgSection').hide();
      $('#about'+$(this).attr('target')).show();
});
//hamburger menu js end //


$('.dropMenuLink').click(function(event){
    $('.active').removeClass('active');    
    $(this).addClass('active'); 
    //$(this).find("ul.dropMenuPanel").addClass("active");
    event.preventDefault();
});

//mobile menu show js
$('#showDiv1').click(function(){
    $('div[id^=div]').hide();
    $('#div1').addClass("active");
});
$('#showDiv2').click(function(){
    $('div[id^=div]').hide();
    $('#div2').addClass("active");
});

$('#showDiv3').click(function(){
    $('div[id^=div]').hide();
    $('#div3').addClass("active");
});

$('#showDiv4').click(function(){
    $('div[id^=div]').hide();
    $('#div4').addClass("active");
 }); 

$('#showDiv5').click(function(){
    $('div[id^=div]').hide();
    $('#div5').addClass("active");
 }); 
    
(function ($) {
    $(document).ready(function () {
        $("#cssmenu").menumaker({
            format: "multitoggle"
        });
    });
})(jQuery);


/* purposes only */
$(".hover").mouseleave(
  function () {
    $(this).removeClass("hover");
  }
);

//preloader settings

var overlay = document.getElementById("overlay");

window.addEventListener('load',function(){
    overlay.style.display='none';
})
 $(window).on('load', function () { document.onreadystatechange = function() { 
            if (document.readyState !== "complete") { 
                document.querySelector( 
                  "header").style.visibility = "hidden"; 
                document.querySelector( 
                  "body").style.visibility = "hidden";
                document.querySelector( 
                  "#loader").style.visibility = "visible"; 
            } else { 
                document.querySelector( 
                  "#loader").style.display = "none"; 
                  document.querySelector( 
                  "header").style.visibility = "visible"; 
                document.querySelector( 
                  "body").style.visibility = "visible"; 
            } 
        }; 
});

//language choose js
    $el_languages = $('.languages');
    $el_languages.hover(function () {
        $el_languages.find('li').show();
    }, function () {
        $el_languages.find('li:not(.active)').hide();
    });

$("#desktop.headerSearch").mouseover(function(){
     $('#cssmenu ul form').show();
    }).mouseout(function(){
    $('#cssmenu ul form').hide();
        
});

$('#mobile').click(function() {
    $('#cssmenu .searchform').show();
});


$('#menu-item-1405').click(function(){
    $(this).children('.sub-menu').slideToggle('slow');
}).children('ul').find('.third').click(function (event) {
    event.stopPropagation();
    console.log('hello!');
    return false;
});

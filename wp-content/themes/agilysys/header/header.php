<?php 
    $activePage = basename($_SERVER['PHP_SELF'], ".php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
   	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--	<title><?php //echo $title; ?></title>-->
	<title>Agilysys</title>
	<meta name="charset" content="UTF-8">
    <meta name="theme-color" content="#fff">
<!--	<meta name="description" content="<?php //echo $description ?>">-->
<!--	<meta name="keywords" content="<?php //echo $keywords ?>">-->
	<meta name="copyright" content="Copyright © <?php echo date('Y'); ?> <?php //echo $title; ?> ">
<!--	<link rel="canonical" href="<?php //echo $canonical ?>"/>-->
<!--	<meta name="author" content="<?php //echo $title; ?> ">-->
<!--	<meta name="designer" content="<?php// echo $title; ?>">-->
	<meta name="robots" content="index, follow">
	<meta name="googlebot" content="index, follow">
	<meta http-equiv="cache-control" content="no-cache" />
        
 
    
</head>  
     
    
<body >
   
  
     <?php
            wp_head();
        ?>
    
        <div id="overlay">
            <div class="spinner"></div>
        </div>
<header class="header">
    <div class="hamburgerMenu" id="hamburgerAnimation">
      <div class="bar1"></div>
      <div class="bar2"></div>
      <div class="bar3"></div>
    </div>
    <div class="mainLogo">
        <a href="index.php">
            <img class="img-fluid" src="img/agylisi-header-logo.png" alt="Agilysys Logo" >                
        </a>
    </div>
    <nav class="headerNav">
        <ul class="menu">              
            <li>
                <a href="#" class="ffBold downArrow hoverEffect" id="industriesSubMenu">Industries</a>
                <div class="dropMenu greenBG" id="industSubMenuShow">
                    <ul class="dropMenuList">
                        <li>  
                            <a class="dropMenuLink active">Hotels &amp; Resorts</a>
                                <ul class="dropMenuPanel active">
                                    <li class="panelLeft">
                                        <h3 class="greenText dinProStd">Hotels &amp; Resorts</h3>
                                        <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                        <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">Casino Resorts</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Casino Resorts</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">Tribal Gaming</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Tribal Gaming</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">Cruise Lines</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Cruise Lines</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">Corp Cafeteria</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Corp Cafeteria</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">Healthcare</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Healthcare</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">Universities</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Universities</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">Sports &amp; Entertainment</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Sports &amp; Entertainment</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">Restaurants</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Restaurants</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a href="#" class="ffBold downArrow hoverEffect" id="solutionsSubMenu">Solutions</a>
                <div class="dropMenu greenBG" id="solSubMenuShow">
                    <ul class="dropMenuList">
                        <li>  
                            <a class="dropMenuLink active">Property Management</a>
                            <ul class="dropMenuPanel active">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Property Management</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                                <li class="panelRight">
                                    <ul>
                                        <li><a href="#">Agilysys Stay</a></li>
                                        <li><a href="#">Agilysys LMS</a></li>
                                        <li><a href="#">Agilysys Visual One PMS</a></li>
                                        <li><a href="#">Agilysys Sales and Catering</a></li>
                                        <li><a href="#">rGuest® Book</a></li>
                                        <li><a href="#">rGuest® Express Kiosk </a></li>
                                        <li><a href="#">rGuest® Express Mobile</a></li>
                                        <li><a href="#">rGuest® Service</a></li>
                                        <li><a href="#">b4</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">Point of Sale</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Point of Sale</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                                <li class="panelRight"></li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">Payment Solution</a>
                            <ul class="dropMenuPanel hied">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Payment Solution</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                                <li class="panelRight"></li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">Analytics &amp; Marketing Loyalty</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Analytics &amp; Marketing Loyalty</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                                <li class="panelRight"></li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">Inventory &amp; Procurement</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Inventory &amp; Procurement</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                                <li class="panelRight"></li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">Reservations and Table Management</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Reservations and Table Management</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                                <li class="panelRight"></li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">Activity Scheduling</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Activity Scheduling</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                                <li class="panelRight"></li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">Document Management</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Document Management</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                                <li class="panelRight"></li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">Services</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Services</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                                <li class="panelRight"></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a href="#" class="ffBold downArrow hoverEffect" id="resourcesSubMenu">Resources</a>
                <div class="dropMenu greenBG" id="resSubMenuShow">
                        <ul class="dropMenuList">
                            <li>  
                                <a class="dropMenuLink active">Knowledge Center</a>
                                <ul class="dropMenuPanel active">
                                    <li class="panelLeft">
                                        <h3 class="greenText dinProStd">Knowledge Center</h3>
                                        <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                        <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </li>
                            <li>  
                                <a class="dropMenuLink">Articles</a>
                                <ul class="dropMenuPanel hide">
                                    <li class="panelLeft">
                                        <h3 class="greenText dinProStd">Articles</h3>
                                        <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                        <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="dropMenuLink">Product Resources</a>
                                <ul class="dropMenuPanel hide">
                                    <li class="panelLeft">
                                        <h3 class="greenText dinProStd">Product Resources</h3>
                                        <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                        <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                    </li>
<!--                                    <li class="panelRight"></li>-->
                                </ul>
                            </li>
                            <li><a class="dropMenuLink">Product Showcase</a>
                                <ul class="dropMenuPanel hide">
                                    <li class="panelLeft">
                                        <h3 class="greenText dinProStd">Product Showcase</h3>
                                        <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                        <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                    </li>
<!--                                    <li class="panelRight"></li>-->
                                </ul>
                            </li>
                            <li>
                                <a class="dropMenuLink">Customer Stories</a>
                                <ul class="dropMenuPanel hide">
                                    <li class="panelLeft">
                                        <h3 class="greenText dinProStd">Customer Stories</h3>
                                        <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                        <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                    </li>
<!--                                    <li class="panelRight"></li>-->
                                </ul>
                            </li>
                            <li>
                                <a class="dropMenuLink">Videos</a>
                                <ul class="dropMenuPanel hide">
                                    <li class="panelLeft">
                                        <h3 class="greenText dinProStd">Videos</h3>
                                        <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                        <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                    </li>
<!--                                    <li class="panelRight"></li>-->
                                </ul>
                            </li>
                            <li>
                                <a class="dropMenuLink">Webinars</a>
                                <ul class="dropMenuPanel hide">
                                    <li class="panelLeft">
                                        <h3 class="greenText dinProStd">Webinars</h3>
                                        <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                        <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                    </li>
<!--                                    <li class="panelRight"></li>-->
                                </ul>
                            </li>
                            <li>
                                <a class="dropMenuLink">Industry Reports</a>
                                <ul class="dropMenuPanel hide">
                                    <li class="panelLeft">
                                        <h3 class="greenText dinProStd">Industry Reports</h3>
                                        <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                        <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                    </li>
<!--                                    <li class="panelRight"></li>-->
                                </ul>
                            </li>
                            <li>
                                <a class="dropMenuLink">Blog</a>
                                <ul class="dropMenuPanel hide">
                                    <li class="panelLeft">
                                        <h3 class="greenText dinProStd">Blog</h3>
                                        <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                        <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                    </li>
<!--                                    <li class="panelRight"></li>-->
                                </ul>
                            </li>
                        </ul>
                </div>
            </li>
            <li>
                <a href="#" class="ffBold downArrow hoverEffect" id="supportSubMenu">Support</a>
                <div class="dropMenu greenBG" id="supSubMenuShow">
                    <ul class="dropMenuList">
                        <li>  
                            <a class="dropMenuLink active">Payment Center</a>
                            <ul class="dropMenuPanel active">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Payment Center</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">Contact Support</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Contact Support</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">Mircrosoft Patches</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Mircrosoft Patches</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">RMA Requests</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">RMA Requests</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">Supply Orders</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Supply Orders</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">LMS/SWS Licensed Upgrade Request</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">LMS/SWS Licensed Upgrade Request</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a href="#" class="ffBold downArrow hoverEffect " id="aboutSubMenu">About</a>
                <div class="dropMenu greenBG" id="aboutSubMenuShow">
                    <ul class="dropMenuList">
                        <li>  
                            <a class="dropMenuLink active">Careers</a>
                            <ul class="dropMenuPanel active">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Careers</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">Contact Us</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Contact Us</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">Investor Relations</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Investor Relations</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">Leadership BOD</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Leadership BOD</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">News &amp; Events</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">News &amp; Events</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">Our Customers</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Our Customers</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </li>
                        <li>  
                            <a class="dropMenuLink">Solution Partners</a>
                            <ul class="dropMenuPanel hide">
                                <li class="panelLeft">
                                    <h3 class="greenText dinProStd">Solution Partners</h3>
                                    <p>Access our library and read about the latest in emerging technology and other hospitality trends. Find tips and insights on accelerating business growth and improving guest satisfaction.</p>
                                    <a href="#" class="aboutButton">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="singleLink"><a href="#" class="ffBold hoverEffect">My Agilysys</a></li>
            <li class="singleLink">
                <div class="headerLang">
                    <select class="">
                        <option>EN</option>
                        <option>CH</option>
                        <option>JP</option>
                    </select>
                </div>
            </li>
            <li class="singleLink">
                <div class="headerNumber">
                    <a href="tel:877 369 6208">877 369 6208</a>
                </div>
            </li>
            <li class="singleLink">
                <div class="headerSeach">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>
            </li>
        </ul>
        
    </nav>
        
     <!-- menu mobile display -->
        <div id="mobileNav" class="sidenav">
            <div class="activePage center">
            <?php echo $activePage;?>
            </div>
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            
            <ul class="mobileMainMenu">   
                <li>
                    <div data-widget="showhide" data-type="panel" class="agMobileMenu" >
                        <h2 id="showDiv1">
                            <a href="javascript:void();" class="ffBold" >Industries <span class="menuIcon"></span></a>
                        </h2>                        
                        <ul class="subMenu" id="div1">
                            <li><a href=#>Industries</a></li>
                            <li><a href=#>Hotels &amp; Resorts</a></li>
                            <li><a href=#>Casino Resorts</a></li>
                            <li><a href=#>Tribal Gaming</a></li>
                            <li><a href=#>Cruise Lines</a></li>
                            <li><a href=#>Corp Cafeteria</a></li>
                            <li><a href=#>Healthcare</a></li>
                            <li><a href=#>Universities</a></li>
                            <li><a href=#>Sports &amp; Entertainment</a></li>
                            <li><a href=#>Restaurants</a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <div data-widget="showhide" data-type="panel" class="agMobileMenu">
                        <h2 id="showDiv2">
                            <a href="javascript:void();">Solutions <span class="menuIcon"></span></a>
                        </h2>
                    
                        <ul class="subMenu" id="div2">
                      <li><a href="#">Solutions</a>
                      <li><a href="javascript:void();">Property Management</a>
                        <ul class="childSubMenu">
                            <li><a href=#>Agilysys Stay</a></li>
                            <li><a href=#>Agilysys LMS</a></li>
                            <li><a href=#>Agilysys Visual One PMS</a></li>
                            <li><a href=#>Agilysys Sales and Catering</a></li>
                            <li><a href=#>rGuest® Book</a></li>
                            <li><a href=#>rGuest® Express Kiosk </a></li>
                            <li><a href=#>rGuest® Express Mobile</a></li>
                            <li><a href=#>rGuest® Service</a></li>
                            <li><a href=#>b4</a></li>
                        </ul>
                      </li>
                      <li><a href=javascript:void();>Point of Sale</a>
                        <ul class="childSubMenu">
                            <li><a href=#>Agilysys InfoGenesis</a></li>
                            <li><a href=#>IG Flex</a></li>
                            <li><a href=#>IG Buy</a></li>
                            <li><a href=#>IG OnDemand</a></li>
                        </ul>
                      </li>
                      <li><a href=#>Payment Solution</a>
                        <ul class="childSubMenu">
                            <li><a href="">Agilysys Pay</a></li>  
                        </ul>
                      </li>
                      <li><a href=#>Analytics &amp; Marketing Loyalty</a>
                        <ul class="childSubMenu">
                            <li><a href=#>Agilysys Analyze</a></li>  
                        </ul>
                      </li>
                      <li><a href=#>Inventory &amp; Procurement</a>
                        <ul class="childSubMenu">
                            <li><a href="#">Agilysys Eatec</a></li>
                            <li><a href="#">Agilysys SWS</a></li>
                        </ul>
                      </li>
                      <li><a href=#>Reservations and Table Management</a>
                        <ul class="childSubMenu">
                            <li><a href="#">Agilysys Seat</a></li>  
                        </ul>
                      </li>
                      <li><a href=#>Activity Scheduling</a>
                        <ul class="childSubMenu">
                            <li><a href="#">Agilysys Golf</a></li>  
                            <li><a href="#">Agilysys Spa</a></li>  
                        </ul>
                      </li>
                      <li><a href=#>Document Management</a>
                        <ul class="childSubMenu">
                            <li><a href="#">Agilysys DataMagine</a></li>  
                        </ul>
                      </li>
                      <li><a href=#>Services</a>
                        <ul class="childSubMenu">
                            <li><a href="#">Professional Services</a></li>  
                        </ul>
                      </li>
                  </ul>
                    </div>
                </li>
                <li>
                    <div data-widget="showhide" data-type="panel" class="agMobileMenu">
                    <h2 id="showDiv3">
                        <a href="javascript:void();" class="ffBold">Resources <span class="menuIcon"></span></a>
                    </h2>
                        <ul class="subMenu" id="div3">
                            <li><a href="#">Resources</a></li>
                            <li><a href="#">Knowledge Center</a></li>
                            <li><a href="#">Articles</a></li>
                            <li><a href="#">Product Resources</a></li>
                            <li><a href="#">Product Showcase</a></li>
                            <li><a href="#">Customer Stories</a></li>
                            <li><a href="#">Videos</a></li>
                            <li><a href="#">Webinars</a></li>
                            <li><a href="#">Industry Reports</a></li>
                            <li><a href="#">Blog</a></li>
                        </ul>
                    </div>
                 </li>
                <li> 
                    <div data-widget="showhide" data-type="panel" class="agMobileMenu">
                        <h2 id="showDiv4">
                            <a href="javascript:void();" class="ffBold">Support <span class="menuIcon"></span></a>
                        </h2>
                        <ul class="subMenu" id="div4">
                            <li><a href="#">Support</a></li>
                            <li><a href="#">Payment Center</a></li>
                            <li><a href="#">Contact Support</a></li>
                            <li><a href="#">Mircrosoft Patches</a></li>
                            <li><a href="#">RMA Requests</a></li>
                            <li><a href="#">Supply Orders</a></li>
                            <li><a href="#">LMS/SWS Licensed Upgrade Request</a></li>
                        </ul> 
                    </div>
                </li>
                <li> 
                    <div data-widget="showhide" data-type="panel" class="agMobileMenu">
                        <h2 id="showDiv5">
                            <a href="javascript:void();" class="ffBold" > About <span class="menuIcon"></span></a>
                        </h2>
                        <ul class="subMenu" id="div5">                        
                            <li><a href="#">About</a></li>
                            <li><a href="#">Careers</a></li>
                            <li><a href="#">Contact Us</a></li>
                            <li><a href="#">Investor Relations</a></li>
                            <li><a href="#">Leadership BOD</a></li>
                            <li><a href="#">News &amp; Events</a></li>
                            <li><a href="#">Our Customers</a></li>
                            <li><a href="#">Solution Partners</a></li>
                        </ul>
                    </div>
                </li>
                <li>  
                    <h2>
                        <a href="my-agilysys.php" class="<?php if ($activePage=="my-agilysys") { echo "activeNav"; }?>  ffBold"> My Agilysys</a>
                    </h2>
                </li>
            </ul>
        </div>

        <span class="openNav" onclick="openNav()">&#9776;</span>
        <!-- menu mobile display end-->
     
     <div class="hamMegaMenu whiteBG" id="hamMegaMenu">
        <div class="hamMegaMenuOption flex">
            <div class="hamMegaMenuList greyBG">
                <ul>
                    <li role="presentation" class="blackText"><strong>Explore</strong></li>
                    <li role="presentation" class="blackText active" id="industriesShow"><a class="blackText">Industries</a></li>
                    <li role="presentation" class="blackText" id="solutionsShow" ><a class="blackText" >Solutions</a></li>
                    <li role="presentation" class="blackText" id="resourcesShow"><a class="blackText" >Resources</a></li>
                    <li role="presentation" class="blackText" id="supportShow"><a class="blackText" >Support</a></li>
                    <li role="presentation" class="blackText" id="aboutShow"><a class="blackText" >About</a></li>
                    <li role="presentation" class="blackText" id="myAgilysysShow"><a class="blackText" >My Agilysys</a></li>
                </ul>
            </div>
            <div class="hamMegaMenuContanier">                
                <div class="hamMenuListRow" id="industries">
                    <div class="hamMenuListColumn flex">                    
                    <div class="hamMenuLinks">
                        <h2 class="dinProStd h2"><a class="greenText" href="#">Industries</a></h2>   
                        <ul>
                            <li><a class="showIndustries blackText active" id="firstLink" href="#" target="1">Hotels &amp; Resorts</a></li>
                            <li><a class="showIndustries blackText" href="#" target="2">Casino Resorts</a></li>
                            <li><a class="showIndustries blackText" href="#" target="3">Tribal Gaming</a></li>
                            <li><a class="showIndustries blackText" href="#" target="4">Cruise Lines</a></li>
                            <li><a class="blackText" href="#" target="5">Corp Cafeteria</a></li>
                            <li><a class="showIndustries blackText" href="#" target="6">Healthcare</a></li>
                            <li><a class="showIndustries blackText" href="#" target="7">Universities</a></li>
                            <li><a class="showIndustries blackText" href="#" target="8">Sports &amp; Entertainment</a></li>
                            <li><a class="showIndustries blackText" href="#" target="9">Restaurants</a></li>
                        </ul>
                    </div>
                    <div class="hamMenuImgSection active" id="indust1">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Hotels &amp; Resorts</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="indust2">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Casino Resorts</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="indust3">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Tribal Gaming</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="indust4">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Cruise Lines</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="indust5">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Corp Cafeteria</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="indust6">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Healthcare</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="indust7">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Universities</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="indust8">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Sports &amp; Entertainment</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="indust9">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Restaurants</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                </div>
                </div>
                <div class="hamMenuListRow hide" id="solutions">
                    <div class="hamMenuListColumn flex">
                    <div class="hamMenuLinks">
                        <h2 class="dinProStd h2"><a class="greenText" href="#">Solutions</a></h2>   
                        <ul>
                            <li><a class="showSolutions blackText active" id="firstLink" href="#" target="1">Property Management</a></li>
                            <li><a class="showSolutions blackText" href="#" target="2">Point of Sale</a></li>
                            <li><a class="showSolutions blackText" href="#" target="3">Payment Solution</a></li>
                            <li><a class="showSolutions blackText" href="#" target="4">Analytics &amp; Marketing Loyalty</a></li>
                            <li><a class="showSolutions blackText" href="#" target="5">Inventory &amp; Procurement</a></li>
                            <li><a class="showSolutions blackText" href="#" target="6">Reservations and Table Management</a></li>
                            <li><a class="showSolutions blackText" href="#" target="7">Activity Scheduling</a></li>
                            <li><a class="showSolutions blackText" href="#" target="8">Document Management</a></li>
                            <li><a class="showSolutions blackText" href="#" target="9">Services</a></li>
                        </ul>
                    </div>
                    <div class="hamMenuImgSection active" id="sol1">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Property Management</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="sol2">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Point of Sale</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="sol3">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Payment Solution</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="sol4">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Analytics &amp; Marketing Loyalty</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="sol5">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Inventory &amp; Procurement</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="sol6">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Reservations and Table Management</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="sol7">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Activity Scheduling</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="sol8">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Document Management</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="sol9">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Services</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                </div>
                    </div>
                <div class="hamMenuListRow hide" id="resources">
                    <div class="hamMenuListColumn flex">
                    <div class="hamMenuLinks">
                        <h2 class="dinProStd h2"><a class="greenText" href="#">Resources</a></h2>  
                        <ul>
                            <li><a class="showResources blackText active" id="firstLink" href="#" target="1">Knowledge Center</a></li>
                            <li><a class="showResources blackText" href="#" target="2">Articles</a></li>
                            <li><a class="showResources blackText" href="#" target="3">Product Resources</a></li>
                            <li><a class="showResources blackText" href="#" target="4">Product Showcase</a></li>
                            <li><a class="showResources blackText" href="#" target="5">Customer Stories</a></li>
                            <li><a class="showResources blackText" href="#" target="6">Videos</a></li>
                            <li><a class="showResources blackText" href="#" target="7">Webinars</a></li>
                            <li><a class="showResources blackText" href="#" target="8">Industry Reports</a></li>
                            <li><a class="showResources blackText" href="#" target="9">Blog</a></li>
                        </ul>
                    </div>
                    <div class="hamMenuImgSection active" id="res1">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Knowledge Center</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="res2">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Articles</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="res3">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Product Resources</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="res4">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Product Showcase</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="res5">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Customer Stories</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="res6">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Videos</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="res7">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Webinars</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="res8">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Industry Reports</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="res9">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Blog</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                </div>
                </div>
                <div class="hamMenuListRow hide" id="support">
                    <div class="hamMenuListColumn flex">
                    
                    <div class="hamMenuLinks">
                        <h2 class="dinProStd h2"><a href="#" class="greenText">Support</a></h2>   
                        <ul>
                            <li><a class="showSupport blackText active" id="firstLink" href="#" target="1">Payment Center</a></li>
                            <li><a class="showSupport blackText" href="#" target="2">Contact Support</a></li>
                            <li><a class="showSupport blackText" href="#" target="3">Mircrosoft Patches</a></li>
                            <li><a class="showSupport blackText" href="#" target="4">RMA Requests</a></li>
                            <li><a class="showSupport blackText" href="#" target="5">Supply Orders</a></li>
                            <li><a class="showSupport blackText" href="#" target="6">LMS/SWS Licensed Upgrade Request</a></li>
                        </ul>
                    </div>
                    <div class="hamMenuImgSection active" id="sup1">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Payment Center</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="sup2">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Contact Support</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="sup3">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Mircrosoft Patches</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="sup4">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">RMA Requests</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="sup5">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Supply Orders</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="sup6">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">LMS/SWS Licensed Upgrade Request</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                </div>
            </div>
                <div class="hamMenuListRow hide" id="about">
                    <div class="hamMenuListColumn flex">
                    <div class="hamMenuLinks">
                        <h2 class="dinProStd h2"><a href="#" class="greenText">About</a></h2>   
                        <ul>
                            <li><a class="showAbout blackText active" id="firstLink" href="#" target="1">Careers</a></li>
                            <li><a class="showAbout blackText" href="#" target="2">Contact Us</a></li>
                            <li><a class="showAbout blackText" href="#" target="3">Investor Relations</a></li>
                            <li><a class="showAbout blackText" href="#" target="4">Leadership BOD</a></li>
                            <li><a class="showAbout blackText" href="#" target="5">News &amp; Events</a></li>
                            <li><a class="showAbout blackText" href="#" target="6">Our Customers</a></li>
                            <li><a class="showAbout blackText" href="#" target="7">Solution Partners</a></li>
                        </ul>
                    </div>
                    <div class="hamMenuImgSection active" id="about1">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Careers</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="about2">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Contact Us</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="about3">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Investor Relations</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="about4">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Leadership BOD</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="about5">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">News &amp; Events</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="about6">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Our Customers</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                    <div class="hamMenuImgSection hide" id="about7">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">Solution Partners</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                </div>
            </div>
                <div class="hamMenuListRow hide" id="myAgilysys">
                    <div class="hamMenuListColumn flex">
                    <div class="hamMenuLinks">
                        <h2 class="dinProStd h2"><a href="#" class="greenText">My Agilysys</a></h2>   
                        <ul>
                            <li><a class="blackText active" id="firstLink"  href="#">My Agilysys</a></li>
                        </ul>
                    </div>
                    <div class="hamMenuImgSection">
                        <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                        <h4 class="blackText">My Agilysys</h4>
                        <p>From the front desk to the back office, and everywhere in between, Agilysys solutions are designed to streamline operations, so you can focus on accelerating business growth and improving guest satisfaction.</p>
                    </div>
                </div>
            </div>
            </div>
         </div>
     </div>
</header> 
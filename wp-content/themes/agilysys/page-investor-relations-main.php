<?php
error_reporting(E_ERROR | E_PARSE);

/*
 *
Template Name: investor relations Main
 */
get_header();

global $page;
global $wp_query;
$post_id = $wp_query->post->ID;
$post = get_post($post_id);
$slug = $post->post_title;
$url = get_the_post_thumbnail_url($post_id, 'thumbnail');

$pageid = get_the_id();
$content_post = get_post($pageid);
$content = $content_post->post_content;

$image = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'single-post-thumbnail');

$image[0] = preg_replace('/\s+/', '', $image[0]);

$menu_name = 'header-menu';

if (($locations = get_nav_menu_locations()) && isset($locations[$menu_name])) {
    $the_menu = wp_get_nav_menu_object($locations[$menu_name]);

    $the_menu_items = wp_get_nav_menu_items($the_menu->term_id);

}

$about_id = 0;

foreach ($the_menu_items as $key) {

    if ($key->title == 'About') {
        $about_id = $key->ID;
    }
}

$arr = array();

foreach ($the_menu_items as $key) {

    if ($key->menu_item_parent == $about_id) {
        array_push($arr, $key);
    }

}



$caption = $content;






$dom = new DOMDocument();
$dom->loadHTML($caption);
$xpath = new DOMXPath($dom);
$div = $xpath->query('//section[@class="agilysysBanner"]');
$div = $div->item(0);
$banner = stripslashes($dom->saveHTML($div));


$caption = $content;

$doc = new DOMDocument();
$doc->loadHTML(stripslashes($caption));
$xpath = new DOMXPath($doc);
$nlist = $xpath->query("//section[@class='agilysysBanner']");
$node = $nlist->item(0);
$node->parentNode->removeChild($node);

$html = $doc->saveHTML();

$doc = new DOMDocument();
$doc->loadHTML(stripslashes($html));
$xpath = new DOMXPath($doc);
$nlist = $xpath->query("//section[@class='homeFooterImg']");

$node = $nlist->item(0);

$node->parentNode->removeChild($node);

$html = $doc->saveHTML();

$caption = $content;

$doc = new DOMDocument();
$doc->loadHTML(stripslashes($caption));
$xpath = new DOMXPath($doc);
$nlist = $xpath->query("//section[@class='agilysysBanner']");
$node = $nlist->item(0);
$node->parentNode->removeChild($node);

$footer = $doc->saveHTML();

$doc = new DOMDocument();
$doc->loadHTML(stripslashes($footer));
$xpath = new DOMXPath($doc);
$nlist = $xpath->query("//div[@id='container']");
$node = $nlist->item(0);
$node->parentNode->removeChild($node);

$footer = $doc->saveHTML();

$doc = new DOMDocument();
$doc->loadHTML(stripslashes($footer));
$xpath = new DOMXPath($doc);
$nlist = $xpath->query("//div[@id='container']");
$node = $nlist->item(0);
$node->parentNode->removeChild($node);

$footer = $doc->saveHTML();

?>




<div class="fullwidth investorRelationsInnerPages" style="margin-top:70px !important;">
    <div class="row">
        <div class="col-lg-12">

            <?php

echo $banner;

?>
     </div>
    </div>
</div>


<div class="fullwidth investorRelationsInnerPages aboutInvestor">
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-7 investorRelationsInnerPagesLeft">
            <?php

echo $html;

?>
        </div>
        <div class="col-lg-3 investorRelationsInnerPagesRight">
            <aside id="secondary" class="left-sidebar widget-area" role="complementary">
                <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('investor-relations')):

endif;?>
            </aside>

        </div>
    </div>




</div>
<div class="fullwidth investorRelationsInnerPages">
    

            <?php

echo $footer;

?>
        
</div>





<?php get_footer();?>
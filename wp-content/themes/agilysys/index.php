<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post();

/* post template defined on custom fields? */
$post_layout = get_post_meta($post->ID, true);

/* Any subtitle entered to post? */
$subtitle = get_post_meta($post->ID, 'subtitle', true);
?>

<?php /* Post lead */ ?>
<section class="row">
  <div class="col-md-12">
    <div class="post-lead">
      <p class="post-category"><?php the_category(' &middot; '); ?></p>
      <h2 class="post-title"><?php the_title(); ?></h2>
      <p class="post-datecomment">
        <?php
        $authorlink = '<a href="'.get_author_posts_url(get_the_author_meta( 'ID' )).'">'.  get_the_author() . '</a>';
        printf(esc_attr__('%2$s Autor: %1$s','gabfire'), $authorlink, get_the_date());
        ?>
        <?php
        /* get the number of comments */
        $num_comments = get_comments_number();
        if ( comments_open() ){
          if($num_comments == 0){  $comments = __('Nema komentara', 'gabfire');  }
          elseif($num_comments > 1){ $comments = $num_comments. __(' Komentara', 'gabfire'); }
          else{ $comments = __('1 komentar', 'gabfire'); }
        }
        echo '<span class="commentnr">' . $comments . '</span>';
        ?>
      </p>

    
    </div>
  </div>
</section>
<div class="row">
  <div class="col-md-8">
        <h2 class="post-title"><?php the_title(); ?></h2>
    <?php the_content(); ?>
  </div>

</div>



<?php endwhile; else : endif; ?>

  <?php get_footer(); ?>
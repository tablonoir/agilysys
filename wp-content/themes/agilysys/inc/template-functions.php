<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package agilysys
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */



/*First row*/

if( ! function_exists('agilysys_footer_sidebars')):

  function agilysys_footer_sidebars(){
    $columns = intval( esc_attr( get_theme_mod( 'footer_widget_areas' ) ) );
    if( ! $columns ){

      $columns = 4;
    }

    for( $i = 1; $i <= $columns; $i++):
      $args = array(
            'name'          => __( 'Footer First Row Column ', 'agilysys' ) . $i,
            'id'            => 'agilysys-footer-'.$i,    
            'description'   => '',
            'class'         => 'col footerCol',
            'before_widget' => '<li id="%1$s" class="widget %2$s">',
            'after_widget'  => '</li>',
            'before_title'  => '<h2>',
            'after_title'   => '</h2>' 
            );
            register_sidebar( $args );
    endfor;
  }

endif;
add_action( 'init' , 'agilysys_footer_sidebars');

/*Second row*/

if( ! function_exists('agilysys_footer_sidebars2')):

  function agilysys_footer_sidebars2(){
    $columns = intval( esc_attr( get_theme_mod( 'footer_widget_areas2' ) ) );
    if( ! $columns ){

      $columns = 4;
    }

    for( $i = 1; $i <= $columns; $i++):
      $args = array(
            'name'          => __( 'Footer Second Row Column ', 'agilysys' ) . $i,
            'id'            => 'agilysys-footer-2'.$i,    
            'description'   => '',
            'class'         => 'col footerCol',
            'before_widget' => '<li id="%1$s" class="widget %2$s">',
            'after_widget'  => '</li>',
            'before_title'  => '<h2>',
            'after_title'   => '</h2>' 
            );
            register_sidebar( $args );
    endfor;
  }

endif;
add_action( 'init' , 'agilysys_footer_sidebars2');

<?php 
/**
 * Customizer options
 * @package     Agilysys
 * 
 * since        1.0.0
 * 
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */
    
    // footer area
    $wp_customize->add_section(
        'agilysys_footer',
        array(
            'title'         => __('Footer Settings', 'agilysys'),
            'priority'      => 28,
        )
    );

    // footer widget ares
    $wp_customize->add_setting(
        'footer_widget_areas',
        array(
            'default'           => '4',
            'sanitize_callback' => 'agilysys_sanitize_footer_widget',
        )
    );

    $wp_customize->add_control(
        'footer_widget_areas',
        array(
            'type'        => 'radio',
            'label'       => __('Footer Row 1', 'agilysys'),
            'section'     => 'agilysys_footer',
            'description' => __('No of widgets you want on footer. You can add widgets from Appearance->Widgets.', 'agilysys'),
            'choices' => array(
                '1'     => __('One', 'agilysys'),
                '2'     => __('Two', 'agilysys'),
                '3'     => __('Three', 'agilysys'),
                '4'     => __('Four', 'agilysys'),
            ),
        )
    );

    // footer widget ares
    $wp_customize->add_setting(
        'footer_widget_areas2',
        array(
            'default'           => '4',
            'sanitize_callback' => 'agilysys_sanitize_footer_widget2',
        )
    );

    $wp_customize->add_control(
        'footer_widget_areas2',
        array(
            'type'        => 'radio',
            'label'       => __('Footer Row 2', 'agilysys'),
            'section'     => 'agilysys_footer',
            'description' => __('No of widgets you want on footer. You can add widgets from Appearance->Widgets.', 'agilysys'),
            'choices' => array(
                '1'     => __('One', 'agilysys'),
                '2'     => __('Two', 'agilysys'),
                '3'     => __('Three', 'agilysys'),
                '4'     => __('Four', 'agilysys'),
            ),
        )
    );

   

/**
 * Sanitazation 
 */

    // footer widget areas
    function agilysys_sanitize_footer_widget( $input ) {
        $valid = array(
            '1'     => __('One', 'agilysys'),
            '2'     => __('Two', 'agilysys'),
            '3'     => __('Three', 'agilysys'),
            '4'     => __('Four', 'agilysys')
        );
        if ( array_key_exists( $input, $valid ) ) {

            return $input;

        } else {

            return '';
        }
    }

   
    // footer widget areas
    function agilysys_sanitize_footer_widget2( $input ) {
        $valid = array(
            '1'     => __('One', 'agilysys'),
            '2'     => __('Two', 'agilysys'),
            '3'     => __('Three', 'agilysys'),
            '4'     => __('Four', 'agilysys')
        );
        if ( array_key_exists( $input, $valid ) ) {

            return $input;

        } else {

            return '';
        }
    }


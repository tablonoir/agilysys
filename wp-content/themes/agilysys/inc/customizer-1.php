<?php
/**
 * Agilysys Theme Customizer
 *
 * @package agilysys
 */

   

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function agilysys_customize_register( $wp_customize ) {

	require_once trailingslashit( get_template_directory() ) . 'inc/options/footer.php';

}
add_action( 'customize_register', 'agilysys_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function agilysys_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function agilysys_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function agilysys_customize_preview_js() {
	wp_enqueue_script( 'agilysys-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'agilysys_customize_preview_js' );

/**
 * Customizer control scripts and styles.
 *
 * @since 1.0.7
 */



add_action('customize_register', 'agilysys_register_theme_customizer');
/*
 * Register Our Customizer Stuff Here
 */
function agilysys_register_theme_customizer($wp_customize)
{
    // Create custom panel.
    $wp_customize->add_panel('social_media_links', array(
        'priority' => 500,
        'theme_supports' => '',
        'title' => __('Social Media Links', 'agilysys'),
        'description' => __('Set editable text for certain content.', 'agilysys'),
    ));
    // Add Facebook Link
    // Add section.
    $wp_customize->add_section('sm_link', array(
        'title' => __('Social Media Links', 'agilysys'),
        'panel' => 'social_media_links',
        'priority' => 10,
    ));

// Add Customize Section
//    $wp_customize->add_section('favicon_section', array(
//        'title' => 'Favicon Image',
//        'description' => 'Update Favicon Image',
//        'panel' => 'social_media_links',
//        'priority' => 11,
//    ));
//
//    $wp_customize->add_setting('favicon_settings', array(
//        //default value
//    ));
//
//    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'favicon_image_control', array(
//        'label' => 'Edit Favicon',
//        'settings' => 'favicon_settings',
//        'section' => 'favicon_section',
//    )));

    $wp_customize->add_section('logo_section', array(
        'title' => 'Logo Image',
        'description' => 'Update Logo Image',
        'panel' => 'social_media_links',
        'priority' => 12,
    ));

    $wp_customize->add_setting('logo_settings', array(
        //default value
    ));

    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'logo_image_control', array(
        'label' => 'Edit Logo',
        'settings' => 'logo_settings',
        'section' => 'logo_section',
    )));

    // Add setting
    $wp_customize->add_setting('facebook_link', array(
        'default' => __('', 'agilysys'),
        'sanitize_callback' => 'sanitize_text',
    ));
    // Add control
    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'facebook_link',
        array(
            'label' => __('Facebook Link', 'agilysys'),
            'section' => 'sm_link',
            'settings' => 'facebook_link',
            'type' => 'text',
        )
    )
    );

    $wp_customize->add_setting('facebook_image_settings', array(
        //default value
    ));

    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'facebook_image', array(
        'label' => 'Edit Facebook Image',
        'settings' => 'facebook_image_settings',
        'section' => 'sm_link',
    )));

// Add setting
    $wp_customize->add_setting('twitter_link', array(
        'default' => __('', 'agilysys'),
        'sanitize_callback' => 'sanitize_text',
    ));
// Add control
    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'twitter_link',
        array(
            'label' => __('Twitter Link', 'agilysys'),
            'section' => 'sm_link',
            'settings' => 'twitter_link',
            'type' => 'text',
        )
    ));

    $wp_customize->add_setting('twitter_image_settings', array(
        //default value
    ));

    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'twitter_image', array(
        'label' => 'Edit Twitter Image',
        'settings' => 'twitter_image_settings',
        'section' => 'sm_link',
    )));

    // Add setting
    $wp_customize->add_setting('linkedin_link', array(
        'default' => __('', 'agilysys'),
        'sanitize_callback' => 'sanitize_text',
    ));
// Add control
    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'linkedin_link',
        array(
            'label' => __('Linkedin Link', 'agilysys'),
            'section' => 'sm_link',
            'settings' => 'linkedin_link',
            'type' => 'text',
        )
    ));

    $wp_customize->add_setting('linkedin_image_settings', array(
        //default value
    ));

    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'linkedin_image', array(
        'label' => 'Edit Linkedin Image',
        'settings' => 'linkedin_image_settings',
        'section' => 'sm_link',
    )));

    // Add setting
    $wp_customize->add_setting('instagram_link', array(
        'default' => __('', 'agilysys'),
        'sanitize_callback' => 'sanitize_text',
    ));
// Add control
    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'instagram_link',
        array(
            'label' => __('Instagram Link', 'agilysys'),
            'section' => 'sm_link',
            'settings' => 'instagram_link',
            'type' => 'text',
        )
    ));

    $wp_customize->add_setting('instagram_image_settings', array(
        //default value
    ));

    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'instagram_image', array(
        'label' => 'Edit Instagram Image',
        'settings' => 'instagram_image_settings',
        'section' => 'sm_link',
    )));

// Add setting
    $wp_customize->add_setting('pinterest_link', array(
        'default' => __('', 'agilysys'),
        'sanitize_callback' => 'sanitize_text',
    ));
// Add control
    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'pinterest_link',
        array(
            'label' => __('Pinterest Link', 'agilysys'),
            'section' => 'sm_link',
            'settings' => 'pinterest_link',
            'type' => 'text',
        )
    ));

    $wp_customize->add_setting('pinterest_image_settings', array(
        //default value
    ));

    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'pinterest_image', array(
        'label' => 'Edit Pinterest Image',
        'settings' => 'pinterest_image_settings',
        'section' => 'sm_link',
	)));
	
// Add setting
$wp_customize->add_setting('follow_us_text', array(
	'default' => __('', 'agilysys'),
	'sanitize_callback' => 'sanitize_text',
));
// Add control
$wp_customize->add_control(new WP_Customize_Control(
	$wp_customize,
	'follow_us_text',
	array(
		'label' => __('Follow Us Text', 'agilysys'),
		'section' => 'sm_link',
		'settings' => 'follow_us_text',
		'type' => 'text',
	)
));


// Add setting
$wp_customize->add_setting('contact_us_text', array(
	'default' => __('', 'agilysys'),
	'sanitize_callback' => 'sanitize_text',
));
// Add control
$wp_customize->add_control(new WP_Customize_Control(
	$wp_customize,
	'contact_us_text',
	array(
		'label' => __('Contact Us Text', 'agilysys'),
		'section' => 'sm_link',
		'settings' => 'contact_us_text',
		'type' => 'text',
	)
));


// Add setting
$wp_customize->add_setting('mobile_number', array(
	'default' => __('', 'agilysys'),
	'sanitize_callback' => 'sanitize_text',
));
// Add control
$wp_customize->add_control(new WP_Customize_Control(
	$wp_customize,
	'mobile_number',
	array(
		'label' => __('Mobile Number', 'agilysys'),
		'section' => 'sm_link',
		'settings' => 'mobile_number',
		'type' => 'text',
	)
));

// Add setting
$wp_customize->add_setting('get_a_demo_link', array(
	'default' => __('', 'agilysys'),
	'sanitize_callback' => 'sanitize_text',
));
// Add control
$wp_customize->add_control(new WP_Customize_Control(
	$wp_customize,
	'get_a_demo_link',
	array(
		'label' => __('Get a Demo Url', 'agilysys'),
		'section' => 'sm_link',
		'settings' => 'get_a_demo_link',
		'type' => 'text',
	)
));

// Add setting
    $wp_customize->add_setting('get_a_demo_text', array(
        'default' => __('', 'agilysys'),
        'sanitize_callback' => 'sanitize_text',
    ));
// Add control
    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'get_a_demo_text',
        array(
            'label' => __('Get a Demo Text', 'agilysys'),
            'section' => 'sm_link',
            'settings' => 'get_a_demo_text',
            'type' => 'text',
        )
    ));
    // Sanitize text
    function sanitize_text($text)
    {
        return sanitize_text_field($text);
    }
}
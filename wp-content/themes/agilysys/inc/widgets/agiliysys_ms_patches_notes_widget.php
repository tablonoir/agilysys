<?php

function load_agiliysys_ms_patches_notes_widget()
{
    register_widget('agiliysys_ms_patches_notes_widget');
}

add_action('widgets_init', 'load_agiliysys_ms_patches_notes_widget');

class agiliysys_ms_patches_notes_widget extends WP_Widget
{

    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys MS Patches Notes Widget', 'AGILYSYS_TEXT_DOMAIN'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {

        $description = $instance['description'];
        $title = $instance['title'];
        $notes = $instance['notes'];
        $ms_patches_rows = $instance['ms_patches_rows'];
        echo $args['before_widget'];
        ?>


<section class="microContent">

    <p class="dinproBld"><?php echo $description; ?></p>

    <h2 class="dinProStd blackText"> <?php echo $title; ?></h2>
    <p><?php echo $notes; ?></p>
    <ul>
        <?php

        $count = count($instance['list']);
        for ($i = 0; $i < $count; $i++) {
            ?>
        <li><?php echo $instance['list'][$i]; ?></li>
        <?php
}
        ?>
    </ul>
</section>


<?php

        echo $args['after_widget'];
    }

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['description'] = $new_instance['description'];
        $instance['notes'] = $new_instance['notes'];
        $instance['title'] = $new_instance['title'];
        $instance['ms_patches_rows'] = $new_instance['ms_patches_rows'];

        $count = count($new_instance['list']);

        for ($i = 0; $i < $count; $i++) {

            $instance['list'][$i] = strip_tags($new_instance['list'][$i]);

        }

        return $instance;
    }

    public function form($display_instance)
    {
        $widget_add_id_slider_client = $this->get_field_id('') . "add";

        $description = ($display_instance['description']);
        $title = ($display_instance['title']);
        $notes = ($display_instance['notes']);
        $ms_patches_rows = ($display_instance['ms_patches_rows']);

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('description') . '"> ' . __('Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<textarea rows="6" cols="28"  id="' . $this->get_field_name('description') . '" name="' . $this->get_field_name('description') . '">' . $description . '</textarea>';
        $rew_html .= '</p><br><br><br><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('title') . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input class="title" id="' . $this->get_field_name('title') . '" name="' . $this->get_field_name('title') . '" type="text" value="' . $title . '" />';
        $rew_html .= '</p><br><br><br><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('notes') . '"> ' . __('Notes', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<textarea rows="6" cols="28"  id="' . $this->get_field_name('notes') . '" name="' . $this->get_field_name('notes') . '">' . $notes . '</textarea>';
        $rew_html .= '</p><br><br><br><br><br><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('ms_patches_rows') . '"> ' . __('No. of rows do you want to add*', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input class="ms_patches_rows" id="' . $this->get_field_name('ms_patches_rows') . '" name="' . $this->get_field_name('ms_patches_rows') . '" type="number" value="' . $ms_patches_rows . '" />';
        $rew_html .= '</p><br><br>';

        $count = count($display_instance['list']);

        $rew_html .= '<div class="add_new_rowxx-input-containers"><div id="entries_agiliysys_ms_patches_notes_widget">';

        $rew_html .= '<input class="cnt909" id="cnt909" name="cnt" type="hidden" value="' . $count . '">';
        for ($i = 0; $i < $count; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '"  class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'AGILYSYS_TEXT_DOMAIN') . ' </span>';

            $rew_html .= '<div class="entry-desc cf">';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('list' . $i) . '"> ' . __('List Item', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input class="list" id="' . $this->get_field_name('list' . $i) . '" name="' . $this->get_field_name('list[]') . '" type="text" value="' . $display_instance['list'][$i] . '" />';
            $rew_html .= '</p>';

            $k = $i + 1;
            $rew_html .= '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' . $k . ');">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';
        }

        $rew_html .= '</div></div>';

        $rew_html .= '<div class="' . $widget_add_id_slider_client . '" style="margin-top: 36px;text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;" onClick="add_new_row();">' . __('ADD ROW', 'AGILYSYS_TEXT_DOMAIN') . '</div>';

        ?>

<script>
function add_new_row() {
    var cnt = '';

    jQuery.each(jQuery("#entries_agiliysys_ms_patches_notes_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            cnt = jQuery(this).val();
        }
    });




    var ms_patches_rows = jQuery('.ms_patches_rows').val();



   console.log(cnt);

 if (parseInt(cnt) < parseInt(ms_patches_rows)) {

        cnt++;

        jQuery.each(jQuery("#entries_agiliysys_ms_patches_notes_widget .cnt909"), function() {
            if (jQuery(this).val() != '') {
                jQuery(this).val(cnt);
            }
        });

        var new_row = '<div id="entry' + cnt +
            '"  class="entrys"><span class="entry-title" onclick = "slider(this);"><?php echo __('Add New Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span>';
        new_row += '<div class="entry-desc cf">';






        new_row += '<p>';
        new_row += '<label><?php echo __('Image Content', 'agilysys_solution'); ?>:</label>';
        new_row += '<input  name="<?php echo $this->get_field_name('list[]'); ?>" type="text">';
        new_row += '</p>';




        var new_cnt = cnt;

        new_row += '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' + new_cnt +
            ');"><?php echo __('Delete Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span></a></p>';
        new_row += '</div></div>';

        jQuery('.add_new_rowxx-input-containers #entries_agiliysys_ms_patches_notes_widget').append(new_row);

    }


}



function delete_row(cnt) {
    jQuery.each(jQuery(".add_new_rowxx-input-containers #entries_agiliysys_ms_patches_notes_widget"), function() {
        jQuery(' #entry' + cnt).remove();
    });
    var last_cnt = 0;

    jQuery.each(jQuery("#entries_agiliysys_ms_patches_notes_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            last_cnt = jQuery(this).val();
        }
    });

    last_cnt--;
    jQuery.each(jQuery("#entries_agiliysys_ms_patches_notes_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(last_cnt);
        }
    });
    jQuery(".ms_patches_rows").val(last_cnt);
    jQuery('.ms_patches_rows').trigger('change');

}
</script>


<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agiliysys_ms_patches_notes_widget select {
    float: left;
    width: 60%;
    margin-top: 20px !important;
    margin-bottom: 10px !important;
}

#rew_container_agiliysys_ms_patches_notes_widget input,

textarea {
    float: right;
    width: 60%;
}

#rew_container_agiliysys_ms_patches_notes_widget label {
    width: 40%;
    float: left;
}

#rew_container_agiliysys_ms_patches_notes_widget p {
    padding: 20px;
}

<?php echo '.' . $widget_add_id_slider_client;

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}


#entries_agiliysys_ms_patches_notes_widget {
    padding: 10px 0 0;
}

#entries_agiliysys_ms_patches_notes_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agiliysys_ms_patches_notes_widget .entrys:first-child {
    margin: 0;
}

#entries_agiliysys_ms_patches_notes_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agiliysys_ms_patches_notes_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agiliysys_ms_patches_notes_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agiliysys_ms_patches_notes_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agiliysys_ms_patches_notes_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agiliysys_ms_patches_notes_widget #entries_agiliysys_ms_patches_notes_widget p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agiliysys_ms_patches_notes_widget">
    <?php echo $rew_html; ?>
</div>


<?php

    }

}
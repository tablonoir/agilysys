<?php

function load_agilysys_product_resource_property_management_solutions_suite_widget()
{
    register_widget('agilysys_product_resource_property_management_solutions_suite_widget');
}

add_action('widgets_init', 'load_agilysys_product_resource_property_management_solutions_suite_widget');

class agilysys_product_resource_property_management_solutions_suite_widget extends WP_Widget
{
/**
 * constructor -- name this the same as the class above
 */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Product Resource Property Management Solutions Suite Widget', 'AGILYSYS_TEXT_DOMAIN'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];

        $section_title = $instance['section_title'];

        $image_uri = $instance['image_uri'];
        $image_uri_alt = $instance['image_uri_alt'];

        $image_uri = preg_replace("/\s+/", "", $image_uri);

        ?>

<section class="propetyTitleSection">
    <div class="row">
        <div class="propetyTitleBox col-12 col-sm-12 col-md-4 col-lg-4">
            <div class="propetyTitleText">
                <h2 class="dinProStd greenText h2"><?php echo $section_title; ?></h2>
            </div>
            <div class="propetyTitleImg">
                <img class="img-fluid" src="<?php echo $image_uri; ?>" alt="<?php echo $image_uri_alt; ?>" />
            </div>

        </div>
        <div class="propetyTitleSlider waterBG whiteText col-12 col-sm-12 col-md-8 col-lg-8">
            <div class="swiper-container">
                <div class="swiper-wrapper">

                    <?php
$count = count($instance['titlea']);
        for ($i = 0; $i < $count; $i++) {

            $titlea = $instance['titlea'][$i];

            $descriptiona = $instance['descriptiona'][$i];
            $learn_more_texta = $instance['learn_more_texta'][$i];
            $urla = $instance['pdf_uria'][$i];

            $titleb = $instance['titleb'][$i];
            $descriptionb = $instance['descriptionb'][$i];
            $learn_more_textb = $instance['learn_more_textb'][$i];
            $urlb = $instance['pdf_urib'][$i];

            $titlec = $instance['titlec'][$i];
            $descriptionc = $instance['descriptionc'][$i];
            $learn_more_textc = $instance['learn_more_textc'][$i];
            $urlc = $instance['pdf_uric'][$i];

            $titled = $instance['titled'][$i];
            $descriptiond = $instance['descriptiond'][$i];
            $learn_more_textd = $instance['learn_more_textd'][$i];
            $urld = $instance['pdf_urid'][$i];

            ?>
                    <div class="swiper-slide">
                        <div class="productSlide flex">
                            <?php
                            
                            if($titlea!="" && $descriptiona!="")
                            {
                            
                            ?>
                            
                            <div class="productSlideBox">
                                <h4 class="dinProStd"><?php echo $titlea; ?></h4>
                                <p><?php echo substr($descriptiona,0,250); ?></p>
                                <a href="<?php echo $urla; ?>" class="homeBannerButton whiteText">
                                    <?php echo $learn_more_texta; ?> <i class="fa fa-arrow-right"
                                        aria-hidden="true"></i></a>
                            </div>
                            
                            <?php
                            
                            }
                            ?>
                            
                             <?php
                            
                            if($titleb!="" && $descriptionb!="")
                            {
                            
                            ?>
                            <div class="productSlideBox">
                                <h4 class="dinProStd"><?php echo $titleb; ?></h4>
                                <p><?php echo substr($descriptionb,0,250); ?></p>
                                <a href="<?php echo $urlb; ?>" class="homeBannerButton whiteText">
                                    <?php echo $learn_more_textb; ?> <i class="fa fa-arrow-right"
                                        aria-hidden="true"></i></a>
                            </div>
                            
                             <?php
                            
                            }
                            ?>
                             <?php
                            
                            if($titlec!="" && $descriptionc!="")
                            {
                            
                            ?>
                            <div class="productSlideBox">
                                <h4 class="dinProStd"><?php echo $titlec; ?></h4>
                                <p><?php echo substr($descriptionc,0,250); ?></p>
                                <a href="<?php echo $urlc; ?>" class="homeBannerButton whiteText">
                                    <?php echo $learn_more_textc; ?> <i class="fa fa-arrow-right"
                                        aria-hidden="true"></i></a>
                            </div>
                            
                             <?php
                            
                            }
                            ?>
                             <?php
                            
                            if($titled!="" && $descriptiond!="")
                            {
                            
                            ?>
                            <div class="productSlideBox">
                                <h4 class="dinProStd"><?php echo $titled; ?></h4>
                                <p><?php echo substr($descriptiond,0,250); ?></p>
                                <a href="<?php echo $urld; ?>" class="homeBannerButton whiteText">
                                    <?php echo $learn_more_textd; ?> <i class="fa fa-arrow-right"
                                        aria-hidden="true"></i></a>
                            </div>
                            
                             <?php
                            
                            }
                            ?>
                        </div>
                    </div>
                    <?php

        }

        ?>

                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
    </div>
</section>



<?php
echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['section_title'] = strip_tags($new_instance['section_title']);
        $instance['what_makes_rows'] = $new_instance['what_makes_rows'];
        $instance['image_uri'] = $new_instance['image_uri'];
        $instance['image_uri_alt'] = $new_instance['image_uri_alt'];

        $count = count($new_instance['titlea']);

        for ($i = 0; $i < $count; $i++) {

            $instance['titlea'][$i] = $new_instance['titlea'][$i];
            $instance['descriptiona'][$i] = $new_instance['descriptiona'][$i];
            $instance['learn_more_texta'][$i] = $new_instance['learn_more_texta'][$i];
            $instance['pdf_uria'][$i] = $new_instance['pdf_uria'][$i];

            $instance['titleb'][$i] = $new_instance['titleb'][$i];
            $instance['descriptionb'][$i] = $new_instance['descriptionb'][$i];
            $instance['learn_more_textb'][$i] = $new_instance['learn_more_textb'][$i];
            $instance['pdf_urib'][$i] = $new_instance['pdf_urib'][$i];

            $instance['titlec'][$i] = $new_instance['titlec'][$i];
            $instance['descriptionc'][$i] = $new_instance['descriptionc'][$i];
            $instance['learn_more_textc'][$i] = $new_instance['learn_more_textc'][$i];
            $instance['pdf_uric'][$i] = $new_instance['pdf_uric'][$i];

            $instance['titled'][$i] = $new_instance['titled'][$i];
            $instance['descriptiond'][$i] = $new_instance['descriptiond'][$i];
            $instance['learn_more_textd'][$i] = $new_instance['learn_more_textd'][$i];
            $instance['pdf_urid'][$i] = $new_instance['pdf_urid'][$i];

        }
        return $instance;
    }

    public function form($display_instance)
    {
        
        
     
        $max_entries_agilysys_product_resource_property_management_solutions_suite_widget_slider_image = 15;

        $widget_add_id_slider = $this->get_field_id('') . "add_agilysys_product_resource_property_management_solutions_suite_widget";

        $title = ($display_instance['section_title']);
        if (!empty($display_instance['what_makes_rows'])) {
            $what_makes_rows = ($display_instance['what_makes_rows']);
        } else {
            $what_makes_rows = 0;
        }
        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('section_title') . '"> ' . __('Slider Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('section_title') . '" name="' . $this->get_field_name('section_title') . '" type="text" value="' . $title . '" />';
        $rew_html .= '</p><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('what_makes_rows') . '"> ' . __('No. of rows do you want to add*', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input class="what_makes_rows" id="' . $this->get_field_name('what_makes_rows') . '" name="' . $this->get_field_name('what_makes_rows') . '" type="number" value="' . $what_makes_rows . '" />';
        $rew_html .= '</p><br>';

        $rew_html .= '<div class="widg-img">';
        $show1 = (empty($display_instance['image_uri'])) ? 'style="display:none;"' : '';
        $rew_html .= '<label id="image_uri_agilysys_product_resource_property_management_solutions_suite_widget"></label><br><img class="' . $this->get_field_id('image_id') . '_media_image custom_media_image" src="' . $display_instance['image_uri'] . '" ' . $show1 . ' width=200" height="120"/>';
        $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id') . '_media_id custom_media_id" name="' . $this->get_field_name('image_id') . '" id="' . $this->get_field_id('image_id') . '" value="' . $display_instance['image_id'] . '" />';
        $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id') . '_media_url custom_media_url" name="' . $this->get_field_name('image_uri') . '" id="' . $this->get_field_id('image_uri') . '" value="' . $display_instance['image_uri'] . '">';
        $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_upload" id="' . $this->get_field_id('image_id') . '"/>';

        $rew_html .= '</div><br><br>';
        ?>

<script>
jQuery(document).ready(function() {
    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    if (attachment.height == 603 && attachment.width == 449) {
                        jQuery('.' + button_id_s + '_media_id').val(attachment.id);
                        jQuery('.' + button_id_s + '_media_url').val(attachment.url);
                        jQuery('.' + button_id_s + '_media_image').attr('src', attachment.url).css(
                            'display', 'block');


                        jQuery('#image_uri_agilysys_product_resource_property_management_solutions_suite_widget')
                            .html("");
                    } else {
                        jQuery('#image_uri_agilysys_product_resource_property_management_solutions_suite_widget')
                            .html("Please Enter the correct Dimensions 449 x603").css(
                                'color', 'red');

                    }
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload');

});
</script>

<?php
$rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt') . '"> ' . __('Slider Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt') . '" name="' . $this->get_field_name('image_uri_alt') . '" type="text" value="' . $display_instance['image_uri_alt'] . '" />';
        $rew_html .= '</p>';

        $count = count($display_instance['titlea']);

        $rew_html .= '<div class="add_new_rowxx-input-containers"><div id="entries_agilysys_product_resource_property_management_solutions_suite_widget">';

        $rew_html .= '<input class="cnt909" id="cnt909" name="cnt" type="hidden" value="' . $count . '">';
        for ($i = 0; $i < $count; $i++) {

         

            $rew_html .= '<div id="entry' . ($i + 1) . '"  class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'AGILYSYS_TEXT_DOMAIN') . ' </span>';

            $rew_html .= '<div class="entry-desc cf">';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('titlea' . $i) . '"> ' . __('Title 1', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('titlea' . $i) . '" name="' . $this->get_field_name('titlea[]') . '" type="text" value="' . $display_instance['titlea'][$i] . '">';
            $rew_html .= '</p><br>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('descriptiona' . $i) . '"> ' . __('Description 1', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<textarea rows="6" cols="35" id="' . $this->get_field_id('descriptiona' . $i) . '" name="' . $this->get_field_name('descriptiona[]') . '">' . $display_instance['descriptiona'][$i] . '</textarea>';
            $rew_html .= '</p><br><br><br><br><br><br>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('learn_more_texta' . $i) . '"> ' . __('Learn More Text 1', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('learn_more_texta' . $i) . '" name="' . $this->get_field_name('learn_more_texta[]') . '" type="text" value="' . $display_instance['learn_more_texta'][$i] . '">';
            $rew_html .= '</p><br>';

            $rew_html .= '<div class="widg-img">';
            $show1 = (empty($display_instance['pdf_uria'][$i])) ? 'style="display:none;"' : '';
            $rew_html .= '<span style="color:green;display:none;" class="' . $this->get_field_id('pdf_id' . $i) . '_media_imagexx">File Uploaded Successfully</span>';
            $rew_html .= '<span style="color:green;display:none;" class="' . $this->get_field_id('pdf_id' . $i) . '_media_image123xx">File Uploaded Successfully</span>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('pdf_id' . $i) . '_media_idxx custom_media_idxx" name="' . $this->get_field_name('pdf_id[]') . '" id="' . $this->get_field_id('pdf_id' . $i) . '" value="' . $display_instance['pdf_id' . $i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('pdf_id' . $i) . '_media_urlxx custom_media_urlxx" name="' . $this->get_field_name('pdf_uria[]') . '" id="' . $this->get_field_id('pdf_uria' . $i) . '" value="' . $display_instance['pdf_uria'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload PDF or ZIP File" class="button custom_media_uploadxx" id="' . $this->get_field_id('pdf_id' . $i) . '"/>';

            $rew_html .= '</div><br><br>';

            ?>
<script>
jQuery(document).ready(function() {
    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_uploadxx', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {
                    jQuery('.' + button_id_s + '_media_idxx').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_urlxx').val(attachment.url);
                    jQuery('.' + button_id_s + '_media_imagexx').css(
                        'display', 'block');
                    jQuery('.' + button_id_s + '_media_image123xx').css(
                        'display', 'block').html(attachment.url);

                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_uploadxx');

});
</script>


<?php

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('titleb' . $i) . '"> ' . __('Title 2', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('titleb' . $i) . '" name="' . $this->get_field_name('titleb[]') . '" type="text" value="' . $display_instance['titleb'][$i] . '">';
            $rew_html .= '</p><br>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('descriptionb' . $i) . '"> ' . __('Description 2', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<textarea rows="6" cols="35" id="' . $this->get_field_id('descriptionb' . $i) . '" name="' . $this->get_field_name('descriptionb[]') . '">' . $display_instance['descriptionb'][$i] . '</textarea>';
            $rew_html .= '</p><br><br><br><br><br><br>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('learn_more_textb' . $i) . '"> ' . __('Learn More Text 2', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('learn_more_textb' . $i) . '" name="' . $this->get_field_name('learn_more_textb[]') . '" type="text" value="' . $display_instance['learn_more_texta'][$i] . '">';
            $rew_html .= '</p><br>';

            $rew_html .= '<div class="widg-img">';
            $show1 = (empty($display_instance['pdf_urib'][$i])) ? 'style="display:none;"' : '';
            $rew_html .= '<span style="color:green;display:none;" class="' . $this->get_field_id('pdf_id' . $i) . '_media_imageyy">File Uploaded Successfully</span>';
            $rew_html .= '<span style="color:green;display:none;" class="' . $this->get_field_id('pdf_id' . $i) . '_media_image123yy">File Uploaded Successfully</span>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('pdf_id' . $i) . '_media_idyy custom_media_idyy" name="' . $this->get_field_name('pdf_id[]') . '" id="' . $this->get_field_id('pdf_id' . $i) . '" value="' . $display_instance['pdf_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('pdf_id' . $i) . '_media_urlyy custom_media_urlyy" name="' . $this->get_field_name('pdf_urib[]') . '" id="' . $this->get_field_id('pdf_urib' . $i) . '" value="' . $display_instance['pdf_urib'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload PDF or ZIP File" class="button custom_media_uploadyy" id="' . $this->get_field_id('pdf_id' . $i) . '"/>';

            $rew_html .= '</div><br><br>';

            ?>
<script>
jQuery(document).ready(function() {
    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_uploadyy', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {
                    jQuery('.' + button_id_s + '_media_idyy').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_urlyy').val(attachment.url);
                    jQuery('.' + button_id_s + '_media_imageyy').css(
                        'display', 'block');
                    jQuery('.' + button_id_s + '_media_image123yy').css(
                        'display', 'block').html(attachment.url);

                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_uploadyy');

});
</script>


<?php

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('titlec' . $i) . '"> ' . __('Title 3', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('titlec' . $i) . '" name="' . $this->get_field_name('titlec[]') . '" type="text" value="' . $display_instance['titlec'][$i] . '">';
            $rew_html .= '</p><br>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('descriptionc' . $i) . '"> ' . __('Description 3', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<textarea rows="6" cols="35" id="' . $this->get_field_id('descriptionc' . $i) . '" name="' . $this->get_field_name('descriptionc[]') . '">' . $display_instance['descriptionc'][$i] . '</textarea>';
            $rew_html .= '</p><br><br><br><br><br><br>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('learn_more_textc' . $i) . '"> ' . __('Learn More Text 3', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('learn_more_textc' . $i) . '" name="' . $this->get_field_name('learn_more_textc[]') . '" type="text" value="' . $display_instance['learn_more_textc'][$i] . '">';
            $rew_html .= '</p><br>';

            $rew_html .= '<div class="widg-img">';
            $show1 = (empty($display_instance['pdf_uric'][$i])) ? 'style="display:none;"' : '';
            $rew_html .= '<span style="color:green;display:none;" class="' . $this->get_field_id('pdf_id' . $i) . '_media_imagezz">File Uploaded Successfully</span>';
            $rew_html .= '<span style="color:green;display:none;" class="' . $this->get_field_id('pdf_id' . $i) . '_media_image123zz">File Uploaded Successfully</span>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('pdf_id' . $i) . '_media_idzz custom_media_idzz" name="' . $this->get_field_name('pdf_id[]') . '" id="' . $this->get_field_id('pdf_id' . $i) . '" value="' . $display_instance['pdf_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('pdf_id' . $i) . '_media_urlzz custom_media_urlzz" name="' . $this->get_field_name('pdf_uric[]') . '" id="' . $this->get_field_id('pdf_uric' . $i) . '" value="' . $display_instance['pdf_uric'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload PDF or ZIP File" class="button custom_media_uploadzz" id="' . $this->get_field_id('pdf_id' . $i) . '"/>';

            $rew_html .= '</div><br><br>';

            ?>
<script>
jQuery(document).ready(function() {
    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_uploadzz', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {
                    jQuery('.' + button_id_s + '_media_idzz').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_urlzz').val(attachment.url);
                    jQuery('.' + button_id_s + '_media_imagezz').css(
                        'display', 'block');
                    jQuery('.' + button_id_s + '_media_image123zz').css(
                        'display', 'block').html(attachment.url);

                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_uploadzz');

});
</script>


<?php

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('titled' . $i) . '"> ' . __('Title 4', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('titled' . $i) . '" name="' . $this->get_field_name('titled[]') . '" type="text" value="' . $display_instance['titled'][$i] . '">';
            $rew_html .= '</p><br>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('descriptiond' . $i) . '"> ' . __('Description 4', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<textarea rows="6" cols="35" id="' . $this->get_field_id('descriptiond' . $i) . '" name="' . $this->get_field_name('descriptiond[]') . '">' . $display_instance['descriptiond'][$i] . '</textarea>';
            $rew_html .= '</p><br><br><br><br><br><br>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('learn_more_textd' . $i) . '"> ' . __('Learn More Text 4', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('learn_more_textd' . $i) . '" name="' . $this->get_field_name('learn_more_textd[]') . '" type="text" value="' . $display_instance['learn_more_textd'][$i] . '">';
            $rew_html .= '</p><br>';

            $rew_html .= '<div class="widg-img">';
            $show1 = (empty($display_instance['pdf_urid'][$i])) ? 'style="display:none;"' : '';
            $rew_html .= '<span style="color:green;display:none;" class="' . $this->get_field_id('pdf_id' . $i) . '_media_imageaa">File Uploaded Successfully</span>';
            $rew_html .= '<span style="color:green;display:none;" class="' . $this->get_field_id('pdf_id' . $i) . '_media_image123aa">File Uploaded Successfully</span>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('pdf_id' . $i) . '_media_idaa custom_media_idaa" name="' . $this->get_field_name('pdf_id[]') . '" id="' . $this->get_field_id('pdf_id' . $i) . '" value="' . $display_instance['pdf_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('pdf_id' . $i) . '_media_urlaa custom_media_urlaa" name="' . $this->get_field_name('pdf_urid[]') . '" id="' . $this->get_field_id('pdf_urid' . $i) . '" value="' . $display_instance['pdf_urid'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload PDF or ZIP File" class="button custom_media_uploadaa" id="' . $this->get_field_id('pdf_id' . $i) . '"/>';

            $rew_html .= '</div><br><br>';

            ?>
<script>
jQuery(document).ready(function() {
    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_uploadaa', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {
                    jQuery('.' + button_id_s + '_media_idaa').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_urlaa').val(attachment.url);
                    jQuery('.' + button_id_s + '_media_imageaa').css(
                        'display', 'block');
                    jQuery('.' + button_id_s + '_media_image123aa').css(
                        'display', 'block').html(attachment.url);

                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_uploadaa');

});
</script>


<?php

            $k = $i + 1;
            $rew_html .= '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' . $k . ');">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';

        }

        $rew_html .= '</div></div>';
        $rew_html .= '<div id="message">' . __('Sorry, you reached to the limit of', 'AGILYSYS_TEXT_DOMAIN') . ' "' . $max_entries_agilysys_product_resource_property_management_solutions_suite_widget_slider_image . '" ' . __('maximum entries_agilysys_product_resource_property_management_solutions_suite_widget', 'AGILYSYS_TEXT_DOMAIN') . '.</div>';

        $rew_html .= '<div class="' . $widget_add_id_slider . '" style="text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;" onClick="add_new_row();">' . __('ADD ROW', 'AGILYSYS_TEXT_DOMAIN') . '</div>';
        ?>
<script>
function add_new_row() {
    var cnt = '';

    jQuery.each(jQuery("#entries_agilysys_product_resource_property_management_solutions_suite_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            cnt = jQuery(this).val();
        }
    });




    var what_makes_rows = jQuery('.what_makes_rows').val();

console.log(cnt);

    if (parseInt(cnt) < parseInt(what_makes_rows)) {

        cnt++;

        jQuery.each(jQuery("#entries_agilysys_product_resource_property_management_solutions_suite_widget .cnt909"), function() {
            if (jQuery(this).val() != '') {
                jQuery(this).val(cnt);
            }
        });

        var new_row = '<div id="entry' + cnt +
            '"  class="entrys"><span class="entry-title" onclick = "slider(this);"><?php echo __('Add New Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span>';
        new_row += '<div class="entry-desc cf">';


        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Title 1', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('titlea[]')); ?>" type="text" value="">';
        new_row += '</p>';


        new_row += '<p>';
        new_row += '<label><?php echo __('Description 1', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
        new_row +=
            '<textarea rows="6" cols="35"  name="<?php echo $this->get_field_name('descriptiona[]'); ?>"></textarea>';
        new_row += '</p><br><br><br><br><br><br>';

        new_row += '<p>';
        new_row += '<label><?php echo __('Learn More Text 1', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
        new_row += '<input name="<?php echo $this->get_field_name('learn_more_texta[]'); ?>" type="text" value="">';
        new_row += '</p><br>';

        new_row += '<div class="widg-img">';

        new_row += '<span style="color:green;display:none;" class="<?php echo $this->get_field_id('pdf_id'); ?>' + cnt +
            '_media_imagexx">File Uploaded Successfully</span>';
        new_row += '<span style="color:green;display:none;" class="<?php echo $this->get_field_id('pdf_id'); ?>' + cnt +
            '_media_image123xx">File Uploaded Successfully</span>';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('pdf_id'); ?>' + cnt +
            '_media_idxx custom_media_idxx" name="<?php echo $this->get_field_name('pdf_id[]'); ?>"  />';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('pdf_id'); ?>' + cnt +
            '_media_urlxx custom_media_urlxx" name="<?php echo $this->get_field_name('pdf_uria[]'); ?>">';
        new_row +=
            '<input type="button" value="Upload PDF or ZIP File" class="button custom_media_uploadxx" id="<?php echo $this->get_field_id('pdf_id'); ?>' +
            cnt + '"/>';

        new_row += '</div><br><br>';



        jQuery(document).ready(function() {
            function media_upload(button_class) {
                var _custom_media = true,
                    _orig_send_attachment = wp.media.editor.send.attachment;
                jQuery('body').on('click', '.custom_media_uploadxx', function(e) {
                    var button_id = '#' + jQuery(this).attr('id');
                    var button_id_s = jQuery(this).attr('id');
                    console.log(button_id);
                    var self = jQuery(button_id);
                    var send_attachment_bkp = wp.media.editor.send.attachment;
                    var button = jQuery(button_id);
                    var id = button.attr('id').replace('_button', '');
                    _custom_media = true;

                    wp.media.editor.send.attachment = function(props, attachment) {
                        if (_custom_media) {
                            jQuery('.' + button_id_s + '_media_idxx').val(attachment.id);
                            jQuery('.' + button_id_s + '_media_urlxx').val(attachment.url);
                            jQuery('.' + button_id_s + '_media_imagexx').css(
                                'display', 'block');
                            jQuery('.' + button_id_s + '_media_image123xx').css(
                                'display', 'block').html(attachment.url);

                        } else {
                            return _orig_send_attachment.apply(button_id, [props, attachment]);
                        }
                    }
                    wp.media.editor.open(button);
                    return false;
                });
            }
            media_upload('.custom_media_uploadxx');

        });


        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Title 2', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('titleb[]')); ?>" type="text" value="">';
        new_row += '</p>';


        new_row += '<p>';
        new_row += '<label><?php echo __('Description 2', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
        new_row +=
            '<textarea rows="6" cols="35"  name="<?php echo $this->get_field_name('descriptionb[]'); ?>"></textarea>';
        new_row += '</p><br><br><br><br><br><br>';

        new_row += '<p>';
        new_row += '<label><?php echo __('Learn More Text 2', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
        new_row += '<input name="<?php echo $this->get_field_name('learn_more_textb[]'); ?>" type="text" value="">';
        new_row += '</p><br>';






        new_row += '<div class="widg-img">';

        new_row += '<span style="color:green;display:none;" class="<?php echo $this->get_field_id('pdf_id'); ?>' + cnt +
            '_media_imageyy">File Uploaded Successfully</span>';
        new_row += '<span style="color:green;display:none;" class="<?php echo $this->get_field_id('pdf_id'); ?>' + cnt +
            '_media_image123yy">File Uploaded Successfully</span>';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('pdf_id'); ?>' + cnt +
            '_media_idyy custom_media_urlyy" name="<?php echo $this->get_field_name('pdf_id[]'); ?>"  />';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('pdf_id'); ?>' + cnt +
            '_media_urlyy _media_urlyy" name="<?php echo $this->get_field_name('pdf_urib[]'); ?>">';
        new_row +=
            '<input type="button" value="Upload PDF or ZIP File" class="button custom_media_uploadyy" id="<?php echo $this->get_field_id('pdf_id'); ?>' +
            cnt + '"/>';

        new_row += '</div><br><br>';


        jQuery(document).ready(function() {
            function media_upload(button_class) {
                var _custom_media = true,
                    _orig_send_attachment = wp.media.editor.send.attachment;
                jQuery('body').on('click', '.custom_media_uploadyy', function(e) {
                    var button_id = '#' + jQuery(this).attr('id');
                    var button_id_s = jQuery(this).attr('id');
                    console.log(button_id);
                    var self = jQuery(button_id);
                    var send_attachment_bkp = wp.media.editor.send.attachment;
                    var button = jQuery(button_id);
                    var id = button.attr('id').replace('_button', '');
                    _custom_media = true;

                    wp.media.editor.send.attachment = function(props, attachment) {
                        if (_custom_media) {
                            jQuery('.' + button_id_s + '_media_idyy').val(attachment.id);
                            jQuery('.' + button_id_s + '_media_urlyy').val(attachment.url);
                            jQuery('.' + button_id_s + '_media_imageyy').css(
                                'display', 'block');
                            jQuery('.' + button_id_s + '_media_image123yy').css(
                                'display', 'block').html(attachment.url);

                        } else {
                            return _orig_send_attachment.apply(button_id, [props, attachment]);
                        }
                    }
                    wp.media.editor.open(button);
                    return false;
                });
            }
            media_upload('.custom_media_uploadyy');

        });


        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Title 3', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('titlec[]')); ?>" type="text" value="">';
        new_row += '</p>';


        new_row += '<p>';
        new_row += '<label><?php echo __('Description 3', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
        new_row +=
            '<textarea rows="6" cols="35"  name="<?php echo $this->get_field_name('descriptionc[]'); ?>"></textarea>';
        new_row += '</p><br><br><br><br><br><br>';

        new_row += '<p>';
        new_row += '<label><?php echo __('Learn More Text 3', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
        new_row += '<input name="<?php echo $this->get_field_name('learn_more_textc[]'); ?>" type="text" value="">';
        new_row += '</p><br><br>';





        new_row += '<div class="widg-img">';

        new_row += '<span style="color:green;display:none;" class="<?php echo $this->get_field_id('pdf_id'); ?>' + cnt +
            '_media_imagezz">File Uploaded Successfully</span>';
        new_row += '<span style="color:green;display:none;" class="<?php echo $this->get_field_id('pdf_id'); ?>' + cnt +
            '_media_image123zz">File Uploaded Successfully</span>';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('pdf_id'); ?>' + cnt +
            '_media_idzz custom_media_idzz" name="<?php echo $this->get_field_name('pdf_id[]'); ?>"  />';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('pdf_id'); ?>' + cnt +
            '_media_urlzz _media_urlzz" name="<?php echo $this->get_field_name('pdf_uric[]'); ?>">';
        new_row +=
            '<input type="button" value="Upload PDF or ZIP File" class="button custom_media_uploadzz" id="<?php echo $this->get_field_id('pdf_id'); ?>' +
            cnt + '"/>';

        new_row += '</div><br><br>';


        jQuery(document).ready(function() {
            function media_upload(button_class) {
                var _custom_media = true,
                    _orig_send_attachment = wp.media.editor.send.attachment;
                jQuery('body').on('click', '.custom_media_uploadzz', function(e) {
                    var button_id = '#' + jQuery(this).attr('id');
                    var button_id_s = jQuery(this).attr('id');
                    console.log(button_id);
                    var self = jQuery(button_id);
                    var send_attachment_bkp = wp.media.editor.send.attachment;
                    var button = jQuery(button_id);
                    var id = button.attr('id').replace('_button', '');
                    _custom_media = true;

                    wp.media.editor.send.attachment = function(props, attachment) {
                        if (_custom_media) {
                            jQuery('.' + button_id_s + '_media_idzz').val(attachment.id);
                            jQuery('.' + button_id_s + '_media_urlzz').val(attachment.url);
                            jQuery('.' + button_id_s + '_media_imagezz').css(
                                'display', 'block');
                            jQuery('.' + button_id_s + '_media_image123zz').css(
                                'display', 'block').html(attachment.url);

                        } else {
                            return _orig_send_attachment.apply(button_id, [props, attachment]);
                        }
                    }
                    wp.media.editor.open(button);
                    return false;
                });
            }
            media_upload('.custom_media_uploadzz');

        });

        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Title 4', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('titled[]')); ?>" type="text" value="">';
        new_row += '</p>';


        new_row += '<p>';
        new_row += '<label><?php echo __('Description 4', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
        new_row +=
            '<textarea rows="6" cols="35"  name="<?php echo $this->get_field_name('descriptiond[]'); ?>"></textarea>';
        new_row += '</p><br><br><br><br><br><br>';

        new_row += '<p>';
        new_row += '<label><?php echo __('Learn More Text 4', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
        new_row += '<input name="<?php echo $this->get_field_name('learn_more_textd[]'); ?>" type="text" value="">';
        new_row += '</p><br><br>';





        new_row += '<div class="widg-img">';

        new_row += '<span style="color:green;display:none;" class="<?php echo $this->get_field_id('pdf_id'); ?>' + cnt +
            '_media_imageaa">File Uploaded Successfully</span>';
        new_row += '<span style="color:green;display:none;" class="<?php echo $this->get_field_id('pdf_id'); ?>' + cnt +
            '_media_image123aa">File Uploaded Successfully</span>';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('pdf_id'); ?>' + cnt +
            '_media_idaa custom_media_idaa" name="<?php echo $this->get_field_name('pdf_id[]'); ?>"  />';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('pdf_id'); ?>' + cnt +
            '_media_urlaa _media_urlaa" name="<?php echo $this->get_field_name('pdf_urid[]'); ?>">';
        new_row +=
            '<input type="button" value="Upload PDF or ZIP File" class="button custom_media_uploadaa" id="<?php echo $this->get_field_id('pdf_id'); ?>' +
            cnt + '"/>';

        new_row += '</div><br><br>';

        jQuery(document).ready(function() {
            function media_upload(button_class) {
                var _custom_media = true,
                    _orig_send_attachment = wp.media.editor.send.attachment;
                jQuery('body').on('click', '.custom_media_uploadaa', function(e) {
                    var button_id = '#' + jQuery(this).attr('id');
                    var button_id_s = jQuery(this).attr('id');
                    console.log(button_id);
                    var self = jQuery(button_id);
                    var send_attachment_bkp = wp.media.editor.send.attachment;
                    var button = jQuery(button_id);
                    var id = button.attr('id').replace('_button', '');
                    _custom_media = true;

                    wp.media.editor.send.attachment = function(props, attachment) {
                        if (_custom_media) {
                            jQuery('.' + button_id_s + '_media_idaa').val(attachment.id);
                            jQuery('.' + button_id_s + '_media_urlaa').val(attachment.url);
                            jQuery('.' + button_id_s + '_media_imageaa').css(
                                'display', 'block');
                            jQuery('.' + button_id_s + '_media_image123aa').css(
                                'display', 'block').html(attachment.url);

                        } else {
                            return _orig_send_attachment.apply(button_id, [props, attachment]);
                        }
                    }
                    wp.media.editor.open(button);
                    return false;
                });
            }
            media_upload('.custom_media_uploadaa');

        });


        var new_cnt = cnt;

        new_row += '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' + new_cnt +
            ');"><?php echo __('Delete Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span></a></p>';
        new_row += '</div></div>';

        jQuery('.add_new_rowxx-input-containers #entries_agilysys_product_resource_property_management_solutions_suite_widget').append(new_row);

    }


}

function show_hide_div(val, i) {

    if (val == 'page') {
        jQuery("#page_div" + i).show();
        jQuery("#link_div" + i).hide();
    } else if (val == 'link') {
        jQuery("#page_div" + i).hide();
        jQuery("#link_div" + i).show();
    }

}

function delete_row(cnt) {
    jQuery.each(jQuery(".add_new_rowxx-input-containers #entries_agilysys_product_resource_property_management_solutions_suite_widget"), function() {
        jQuery(' #entry' + cnt).remove();
    });
    var last_cnt = 0;

    jQuery.each(jQuery("#entries_agilysys_product_resource_property_management_solutions_suite_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            last_cnt = jQuery(this).val();
        }
    });

    last_cnt--;
    jQuery.each(jQuery("#entries_agilysys_product_resource_property_management_solutions_suite_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(last_cnt);
        }
    });
    jQuery(".what_makes_rows").val(last_cnt);
    jQuery('.what_makes_rows').trigger('change');

}
</script>
<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_product_resource_property_management_solutions_suite_widget input,
select,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_product_resource_property_management_solutions_suite_widget label {
    width: 40%;
}

<?php echo '.'. $widget_add_id_slider;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}



#entries_agilysys_product_resource_property_management_solutions_suite_widget {
    padding: 10px 0 0;
}

#entries_agilysys_product_resource_property_management_solutions_suite_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_product_resource_property_management_solutions_suite_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_product_resource_property_management_solutions_suite_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_product_resource_property_management_solutions_suite_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_product_resource_property_management_solutions_suite_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_product_resource_property_management_solutions_suite_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_product_resource_property_management_solutions_suite_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_product_resource_property_management_solutions_suite_widget #entries_agilysys_product_resource_property_management_solutions_suite_widget p {
    padding:10px;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_product_resource_property_management_solutions_suite_widget">
    <?php echo $rew_html; ?>
</div>

<?php
}

}

?>
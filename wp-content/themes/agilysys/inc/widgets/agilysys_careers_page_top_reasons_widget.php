<?php

function load_agilysys_careers_page_top_reasons_widget()
{
    register_widget('agilysys_careers_page_top_reasons_widget');
}

add_action('widgets_init', 'load_agilysys_careers_page_top_reasons_widget');

class agilysys_careers_page_top_reasons_widget extends WP_Widget
{
/**
 * constructor -- name this the same as the class above
 */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Careers Page Top Reasons Widget', 'AGILYSYS_TEXT_DOMAIN'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {

        $max_entries_agilysys_careers_page_top_reasons_widget_slider_image = 15;
        $section_title = $instance['section_title'];
        $image_uri = $instance['image_uri'];
        $image_uri_alt = $instance['image_uri_alt'];
        echo $args['before_widget'];
        ?>


<section class="careerJoinTeamSection flex">
    <div class="careerJoinImg">
        <div class="careerJoinImgInner">
            <img class="img-fluid" src="<?php echo $image_uri; ?>" alt="<?php echo $image_uri_alt; ?>" />
        </div>
    </div>
    <div class="careerJoinContent whiteText waterBG">
        <div class="careerJoinContentBox">
            <h3 class="dinProStd"><?php echo $section_title; ?></h3>
            <ul>

                <?php

        $count = count($instance['title']);

        for ($i = 0; $i < $count; $i++) {

            $title = $instance['title'][$i];

            ?>
                <li><?php echo $title; ?></li>


                <?php

        }
        ?>
            </ul>
        </div>
    </div>
</section>



<?php
echo $args['after_widget'];
    }

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $max_entries_agilysys_careers_page_top_reasons_widget_slider_image = 15;

        $instance['section_title'] = strip_tags($new_instance['section_title']);
        $instance['what_makes_rows'] = strip_tags($new_instance['what_makes_rows']);
        $instance['image_uri'] = strip_tags($new_instance['image_uri']);
        $instance['image_uri_alt'] = strip_tags($new_instance['image_uri_alt']);

        $count = count($new_instance['title']);

        for ($i = 0; $i < $count; $i++) {

            $instance['title'][$i] = $new_instance['title'][$i];

        }
        return $instance;
    }

    public function form($display_instance)
    {
        $max_entries_agilysys_careers_page_top_reasons_widget_slider_image = 15;

        $widget_add_id_slider = $this->get_field_id('') . "add_agilysys_careers_page_top_reasons_widget";

        $title = ($display_instance['section_title']);
        $image_uri_alt = $display_instance['image_uri_alt'];

        if (!empty($display_instance['what_makes_rows'])) {
            $what_makes_rows = ($display_instance['what_makes_rows']);
        } else {
            $what_makes_rows = 0;
        }

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('section_title') . '"> ' . __('Slider Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('section_title') . '" name="' . $this->get_field_name('section_title') . '" type="text" value="' . $title . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('what_makes_rows') . '"> ' . __('No. of rows do you want to add*', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input class="what_makes_rows" id="' . $this->get_field_name('what_makes_rows') . '" name="' . $this->get_field_name('what_makes_rows') . '" type="number" value="' . $what_makes_rows . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<div class="widg-img">';
        $show1 = (empty($display_instance['image_uri'])) ? 'style="display:none;"' : '';
        $rew_html .= '<label id="image_uri_agilysys_careers_page_top_reasons_widget"></label><br><img class="' . $this->get_field_id('image_id') . '_media_image custom_media_image" src="' . $display_instance['image_uri'] . '" ' . $show1 . ' width=200" height="120"/>';
        $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id') . '_media_id custom_media_id" name="' . $this->get_field_name('image_id') . '" id="' . $this->get_field_id('image_id') . '" value="' . $display_instance['image_id'] . '" />';
        $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id') . '_media_url custom_media_url" name="' . $this->get_field_name('image_uri') . '" id="' . $this->get_field_id('image_uri') . '" value="' . $display_instance['image_uri'] . '">';
        $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_upload" id="' . $this->get_field_id('image_id') . '"/>';

        $rew_html .= '</div>';

        ?>

<script>
jQuery(document).ready(function() {
    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {
                    //if (attachment.height == 1080 && attachment.width == 1920) {
                    jQuery('.' + button_id_s + '_media_id').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_url').val(attachment.url);
                    jQuery('.' + button_id_s + '_media_image').attr('src', attachment.url).css(
                        'display', 'block');

                        /*jQuery('#image_uri_agilysys_careers_page_top_reasons_widget')
                            .html("");
                    }
                    else
                    {
                        jQuery('#image_uri_agilysys_careers_page_top_reasons_widget')
                            .html("Please Enter the correct Dimensions 1920x1080").css('color',
                                'red');
                    }
                    */
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload');

});
</script>


<?php

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt') . '"> ' . __('Slider Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt') . '" name="' . $this->get_field_name('image_uri_alt') . '" type="text" value="' . $image_uri_alt . '" />';
        $rew_html .= '</p>';

        $count = count($display_instance['title']);

        $rew_html .= '<div class="add_new_rowxx-input-containers"><div id="entries_agilysys_careers_page_top_reasons_widget">';

        $rew_html .= '<input class="cnt909" id="cnt909" name="cnt" type="hidden" value="' . $count . '">';

        for ($i = 0; $i < $count; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'AGILYSYS_TEXT_DOMAIN') . ' </span>';

            $rew_html .= '<div class="entry-desc cf">';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('title' . $i) . '"> ' . __('Reason', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('title' . $i) . '" name="' . $this->get_field_name('title[]') . '" type="text" value="' . $display_instance['title'][$i] . '">';
            $rew_html .= '</p>';

            $k = $i + 1;
            $rew_html .= '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' . $k . ');">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';

        }

        $rew_html .= '</div></div>';
        $rew_html .= '<div id="message">' . __('Sorry, you reached to the limit of', 'AGILYSYS_TEXT_DOMAIN') . ' "' . $max_entries_agilysys_careers_page_top_reasons_widget_slider_image . '" ' . __('maximum entries_agilysys_careers_page_top_reasons_widget', 'AGILYSYS_TEXT_DOMAIN') . '.</div>';

        $rew_html .= '<div class="' . $widget_add_id_slider . '" style="text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;" onClick="add_new_row();">' . __('ADD ROW', 'AGILYSYS_TEXT_DOMAIN') . '</div>';

        ?>
<script>
function add_new_row() {
    var cnt = '';

    jQuery.each(jQuery("#entries_agilysys_careers_page_top_reasons_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            cnt = jQuery(this).val();
        }
    });




    var what_makes_rows = jQuery('.what_makes_rows').val();

 console.log(cnt);  

    if (parseInt(cnt) < parseInt(what_makes_rows)) {

        cnt++;

        jQuery.each(jQuery("#entries_agilysys_careers_page_top_reasons_widget .cnt909"), function() {
            if (jQuery(this).val() != '') {
                jQuery(this).val(cnt);
            }
        });

        var new_row = '<div id="entry' + cnt +
            '"  class="entrys"><span class="entry-title" onclick = "slider(this);"><?php echo __('Add New Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span>';
        new_row += '<div class="entry-desc cf">';

        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Title', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';

        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('title[]')); ?>" type="text" value="">';
        new_row += '</p>';


        var new_cnt = cnt;

        new_row +=
            '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' + new_cnt +
            ');"><?php echo __('Delete Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span></a></p>';
        new_row += '</div></div>';

        jQuery('.add_new_rowxx-input-containers #entries_agilysys_careers_page_top_reasons_widget').append(new_row);

    }


}



function delete_row(cnt) {
    jQuery.each(jQuery(".add_new_rowxx-input-containers #entries_agilysys_careers_page_top_reasons_widget"), function() {
        jQuery(' #entry' + cnt).remove();
    });
    var last_cnt = 0;

    jQuery.each(jQuery("#entries_agilysys_careers_page_top_reasons_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            last_cnt = jQuery(this).val();
        }
    });

    last_cnt--;
    jQuery.each(jQuery("#entries_agilysys_careers_page_top_reasons_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(last_cnt);
        }
    });
    jQuery(".what_makes_rows").val(last_cnt);
    jQuery('.what_makes_rows').trigger('change');

}
</script>
<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_careers_page_top_reasons_widget p {
    padding:10px !important;
}

#rew_container_agilysys_careers_page_top_reasons_widget input,
select,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_careers_page_top_reasons_widget label {
    width: 40%;
}

<?php echo '.' . $widget_add_id_slider;

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}



#entries_agilysys_careers_page_top_reasons_widget {
    padding: 10px 0 0;
}

#entries_agilysys_careers_page_top_reasons_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_careers_page_top_reasons_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_careers_page_top_reasons_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_careers_page_top_reasons_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_careers_page_top_reasons_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_careers_page_top_reasons_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_careers_page_top_reasons_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_careers_page_top_reasons_widget #entries_agilysys_careers_page_top_reasons_widget p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_careers_page_top_reasons_widget">
    <?php echo $rew_html; ?>
</div>
<?php
} //Function form ends here

}
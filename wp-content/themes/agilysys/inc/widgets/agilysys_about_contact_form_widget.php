<?php
use PHPMailer\PHPMailer\PHPMailer;

require_once ABSPATH . WPINC . '/PHPMailer/PHPMailer.php';
require_once ABSPATH . WPINC . '/PHPMailer/Exception.php';
function load_agilysys_about_contact_form_widget()
{
    register_widget('agilysys_about_contact_form_widget');
}

add_action('widgets_init', 'load_agilysys_about_contact_form_widget');

class agilysys_about_contact_form_widget extends WP_Widget
{

    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys About Contact Form Widget', 'AGILYSYS_TEXT_DOMAIN'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {



        $description = $instance['description'];
        $title1 = $instance['title1'];

        $description2 = $instance['description2'];
        $title2 = $instance['title2'];

        echo $args['before_widget'];


        if (isset($_POST['submit_contact_form_widget'])) {

            $email = $_POST['email'];
            $firstname = $_POST['first_name'];
            $lastname = $_POST['last_name'];
            $company = $_POST['company_name'];

     
            $phone = $_POST['phone'];
            $interested_in = $_POST['interested_in'];
            $how_can = $_POST['how_can'];

          

            $message = '';
            $message .= '<p>Email: ' . $email . '</p>';
            $message .= '<p>Name: ' . $firstname . ' ' . $lastname . '</p>';
            $message .= '<p>Company: ' . $company . '</p>';
            
            $message .= '<p>Phone: ' . $phone . '</p>';
            $message .= '<p>what are you Interested In: ' . $interested_in . '</p>';
            $message .= '<p>how can we help you: ' . $how_can . '</p>';
     

            

            $subject = "New Enquiry";

            $mail = new PHPMailer();

//$mail->isSMTP();

            $mail->SMTPDebug = false;

            $mail->Host = 'smtp.gmail.com';

            $mail->SMTPAuth = true;

            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = 587;

            $mail->Username = 'sau3334@gmail.com';

            $mail->Password = 'rwwtpnhahufyxqih';

            $mail->setFrom('testing@abiteoffrance.com', 'Testing');
            $mail->addReplyTo('sau3334@gmail.com', 'Saurav Daga');

            $mail->addAddress('vandhiyadevan.jayasooriyan@agilysys.com', 'Deva');

            $mail->Subject = 'About Contact Form';

            $mail->Body = $message;

            $mail->AltBody = 'About Contact Form';

            if (!$mail->send()) {
                echo 'Mailer Error: ' . $mail->ErrorInfo;
            } else {
               ?>
               
               <script type="text/javascript">
    jQuery(window).on('load', function() {
        jQuery('#myModal').modal('show');
    });
</script>
               
               <?php
            }

        }
        ?>


<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js">
</script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/additional-methods.js">
</script>

<style id="compiled-css" type="text/css">
.error {
    color: red !important;
    font-size:18px !important;
}

#myform {
    margin: 10px;
}

#myform>div {
    margin-top: 10px;
}

/* EOS */
</style>


<section class="about-contact-us greenBG dinProStd">
    <div class="container">
        <div class=" desc center">
            <h3 class="blackText dinProStd"><?php echo $title1; ?></h3>
            <p class="whiteText"><?php echo $description; ?></p>
            <h4 class="blackText dinProStd"><?php echo $title2; ?></h4>
            <p class="whiteText"><?php echo $description2; ?></p>

            <form class="about-contact-form" name="about-contact-form" id="aboutContactForm" action="" method="post"
                novalidate="novalidate">

                <div class="row">
                    <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                        <div class="col-md-12 whiteText dinProStd">
                            <label for="">EMAIL ADDRESS*</label>
                        </div>
                        <div class="col-md-12 whiteText dinProStd">
                            <input type="text" id="email" name="email" placeholder="Enter your Email Address">
                        </div>
                    </div>
                    <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                        <div class="col-md-12 whiteText dinProStd">
                            <label for="">FIRST NAME*</label>
                        </div>
                        <div class="col-md-12 whiteText dinProStd">
                            <input type="text" id="first_name" name="first_name" placeholder="Enter your First Name">
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                        <div class="col-md-12 whiteText dinProStd">
                            <label for="">LAST NAME*</label>
                        </div>
                        <div class="col-md-12 whiteText  dinProStd">
                            <input type="text" id="last_name" name="last_name" placeholder="Enter your Last Name">
                        </div>
                    </div>
                    <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                        <div class="col-md-12 whiteText dinProStd">
                            <label for="">COMPANY NAME*</label>
                        </div>
                        <div class="col-md-12 whiteText dinProStd">
                            <input type="text" id="company_name" name="company_name"
                                placeholder="Enter your Company Name">
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                        <div class="col-md-12 whiteText dinProStd">
                            <label for="">PHONE NUMBER*</label>
                        </div>
                        <div class="col-md-12 whiteText  dinProStd">
                            <input type="text" id="phone" name="phone" placeholder="Enter your Phone Number">
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-12 blackText">
                            <label for="">What are you interested in?*</label>
                        </div>
                        <div class="col-md-12">
                            <select name="interested_in" id="interested_in
                            ">
                            <option value="">Please Select</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-12">
                            <label for="">How can we help you today?</label>
                        </div>
                        <div class="col-md-12">
                            <textarea name="how_can" id="how_can" cols="30" rows="10">

                      </textarea>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="mx-auto text-center">
                        <button type="submit" name="submit_contact_form_widget" class="btn" value="1">submit <i class="fa fa-arrow-right"
                                aria-hidden="true"></i></button>
                    </div>



                </div>
            </form>
            <br>
            <p class="blackText">
                <a href="">CLICK HERE</a> to view our privacy policy
            </p>
        </div>
    </div>
</section>


<script>
jQuery(document).ready(function($) {

    $.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "Letters and spaces only please");




    $("#aboutContactForm").validate({
        onfocusout: false,
        rules: {
            email: {
                required: true,
                email: true
            },
            first_name: {
                required: true,
                lettersonly: true,
            },
            last_name: {
                required: true,
                lettersonly: true,
            },
            company_name: {
                required: true,
                lettersonly: true,
            },
            phone: {

                required: true,
                number: true,
            },

            interested_in: {
                required: true,
            },
            how_can: {
                required: true,
            },
        },
        errorPlacement: function(error, element) {


            error.insertAfter(element);

        },

        messages: {

            email: "Please enter a valid Email Address",
            first_name: {
                required: "Please enter your firstname",
                lettersonly: "Please enter Letters Only",
            },



            last_name: {
                required: "Please enter your Lastname",
                lettersonly: "Please enter Letters Only",
            },
            company_name: {
                required: "Please enter your Company Name",
                lettersonly: "Please enter Letters Only",
            },

            phone: {

                required: "Please enter your Phone Number",
                number: "Please enter Numbers Only",
            },

            interested_in: {
                required: "Please enter What are you interested in",
            },
            how_can: {
                required: "Please enter how we can help you",
            },

        },


        submitHandler: function(form) {




            form.submit();



        }
    });
});
</script>

<?php

        echo $args['after_widget'];
    }

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['description'] = $new_instance['description'];
        $instance['title1'] = $new_instance['title1'];

        $instance['description2'] = $new_instance['description2'];
        $instance['title2'] = $new_instance['title2'];
        
        return $instance;

    }

    public function form($display_instance)
    {
        $widget_add_id_slider_client = $this->get_field_id('') . "add";

        $description = ($display_instance['description']);
        $title1 = ($display_instance['title1']);


        $description2 = ($display_instance['description2']);
        $title2 = ($display_instance['title2']);

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('description') . '"> ' . __('Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<textarea rows="6" cols="28"  id="' . $this->get_field_name('description') . '" name="' . $this->get_field_name('description') . '">' . $description . '</textarea>';
        $rew_html .= '</p><br><br><br><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('title1') . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input class="title" id="' . $this->get_field_name('title1') . '" name="' . $this->get_field_name('title1') . '" type="text" value="' . $title1 . '" />';
        $rew_html .= '</p><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('description') . '"> ' . __('Description 2', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<textarea rows="6" cols="28"  id="' . $this->get_field_name('description2') . '" name="' . $this->get_field_name('description2') . '">' . $description2 . '</textarea>';
        $rew_html .= '</p><br><br><br><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('title2') . '"> ' . __('Title 2', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input class="title" id="' . $this->get_field_name('title2') . '" name="' . $this->get_field_name('title2') . '" type="text" value="' . $title2 . '" />';
        $rew_html .= '</p><br>';
        

        ?>




<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container input,
select,
textarea {
    float: right;
    width: 60%;
}

#rew_container label {
    width: 40%;
}

<?php echo '.'. $widget_add_id_slider_client;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}


#entries {
    padding: 10px 0 0;
}

#entries .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries .entrys:first-child {
    margin: 0;
}

#entries .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries .entry-title.active:after {
    content: '\f142';
}

#entries .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container #entries p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container">
    <?php echo $rew_html; ?>
</div>


<?php

    }

}
<?php

function load_agilysys_about_when_you_work_widget()
{
    register_widget('agilysys_about_when_you_work_widget');
}

add_action('widgets_init', 'load_agilysys_about_when_you_work_widget');

class agilysys_about_when_you_work_widget extends WP_Widget
{

    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys About When You Work Widget', 'AGILYSYS_TEXT_DOMAIN'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        $title2 = $instance['title2'];
        $description = $instance['description'];
        $learn_more_text = $instance['learn_more_text'];

        $link_type = $instance['link_type'];
        if ($link_type == "link") {
            $link = $instance['link'];
        } elseif ($link_type == "page") {
            $post_id = $instance['page'];
            $post = get_post($post_id);
            $link = home_url($post->post_name) . "/";
        }

        $image_uri = $instance['image_uri'];
        $image_uri_alt = $instance['image_uri_alt'];

        $div_title0 = $instance['div_title0'];
        $image_uri0 = $instance['image_uri0'];
        $image_uri_alt0 = $instance['image_uri_alt0'];

        $div_title1 = $instance['div_title1'];
        $image_uri1 = $instance['image_uri1'];
        $image_uri_alt1 = $instance['image_uri_alt1'];

        $div_title2 = $instance['div_title2'];
        $image_uri2 = $instance['image_uri2'];
        $image_uri_alt2 = $instance['image_uri_alt2'];

        $div_title3 = $instance['div_title3'];
        $image_uri3 = $instance['image_uri3'];
        $image_uri_alt3 = $instance['image_uri_alt3'];

        $div_title4 = $instance['div_title4'];
        $image_uri4 = $instance['image_uri4'];
        $image_uri_alt4 = $instance['image_uri_alt4'];

        $div_title5 = $instance['div_title5'];
        $image_uri5 = $instance['image_uri5'];
        $image_uri_alt5 = $instance['image_uri_alt5'];

        $div_title6 = $instance['div_title6'];
        $image_uri6 = $instance['image_uri6'];
        $image_uri_alt6 = $instance['image_uri_alt6'];

        $div_title7 = $instance['div_title7'];
        $image_uri7 = $instance['image_uri7'];
        $image_uri_alt7 = $instance['image_uri_alt7'];

        $div_title8 = $instance['div_title8'];
        $image_uri8 = $instance['image_uri8'];
        $image_uri_alt8 = $instance['image_uri_alt8'];

        ?>

<section class="aboutHospitality greenBG whiteText">
    <div class="row">
    <div class="aboutHospitalityContent col-12 col-sm-12 col-md-12 col-lg-3" data-aos="fade-left" data-aos-delay="300" data-aos-duration="400"
        data-aos-once="true">
        <h2 class="dinProStd whiteText h2"><?php echo $title2; ?></h2>
        <p><?php echo $description; ?></p>
        <a class="homeBannerButton" href="<?php echo $link; ?>"><?php echo $learn_more_text; ?> <i
                class="fa fa-arrow-right" aria-hidden="true"></i></a>
    </div>
    <div class="aboutHospitalityIcons flex col-12 col-sm-12 col-md-12 col-lg-9">
        <div class="row1">
            <div class="aboutHospitalityIconsBox flex" data-aos="zoom-out" data-aos-delay="300" data-aos-duration="500" data-aos-once="true">
                <div class="aboutIconBoxImg">
                    <img class="img-fluid" src="<?php echo $image_uri0; ?>" alt="<?php echo $image_uri_alt0; ?>" />
                </div>
                <div class="aboutIconBoxTitle">
                    <h4 class="dinproReg"><?php echo $div_title0; ?></h4>
                </div>
            </div>
            <div class="aboutHospitalityIconsBox flex" data-aos="zoom-out" data-aos-delay="300" data-aos-duration="500" data-aos-once="true">
                <div class="aboutIconBoxImg">
                    <img class="img-fluid" src="<?php echo $image_uri1; ?>" alt="<?php echo $image_uri_alt1; ?>" />
                </div>
                <div class="aboutIconBoxTitle">
                    <h4 class="dinproReg"><?php echo $div_title1; ?></h4>
                </div>
            </div>
            <div class="aboutHospitalityIconsBox flex" data-aos="zoom-out" data-aos-delay="300" data-aos-duration="500" data-aos-once="true">
                <div class="aboutIconBoxImg">
                    <img class="img-fluid" src="<?php echo $image_uri2; ?>" alt="<?php echo $image_uri_alt2; ?>" />
                </div>
                <div class="aboutIconBoxTitle">
                    <h4 class="dinproReg"><?php echo $div_title2; ?></h4>
                </div>
            </div>
        </div>
        <div class="row2">
            <div class="aboutHospitalityIconsBox flex" data-aos="zoom-out" data-aos-delay="300" data-aos-duration="500" data-aos-once="true">
                <div class="aboutIconBoxImg">
                    <img class="img-fluid" src="<?php echo $image_uri3; ?>" alt="<?php echo $image_uri_alt3; ?>" />
                </div>
                <div class="aboutIconBoxTitle">
                    <h4 class="dinproReg"><?php echo $div_title3; ?></h4>
                </div>
            </div>
            <div class="aboutHospitalityIconsBox flex" data-aos="zoom-out" data-aos-delay="300" data-aos-duration="500" data-aos-once="true">
                <div class="aboutIconBoxImg">
                    <img class="img-fluid" src="<?php echo $image_uri4; ?>" alt="<?php echo $image_uri_alt4; ?>" />
                </div>
                <div class="aboutIconBoxTitle">
                    <h4 class="dinproReg"><?php echo $div_title4; ?></h4>
                </div>
            </div>
            <div class="aboutHospitalityIconsBox flex" data-aos="zoom-out" data-aos-delay="300" data-aos-duration="500" data-aos-once="true">
                <div class="aboutIconBoxImg">
                    <img class="img-fluid" src="<?php echo $image_uri5; ?>" alt="<?php echo $image_uri_alt5; ?>" />
                </div>
                <div class="aboutIconBoxTitle">
                    <h4 class="dinproReg"><?php echo $div_title5; ?></h4>
                </div>
            </div>
        </div>
        <div class="row3">
            <div class="aboutHospitalityIconsBox flex" data-aos="zoom-out" data-aos-delay="300" data-aos-duration="500" data-aos-once="true">
                <div class="aboutIconBoxImg">
                    <img class="img-fluid" src="<?php echo $image_uri6; ?>" alt="<?php echo $image_uri_alt6; ?>" />
                </div>
                <div class="aboutIconBoxTitle">
                    <h4 class="dinproReg"><?php echo $div_title6; ?></h4>
                </div>
            </div>
            <div class="aboutHospitalityIconsBox flex" data-aos="zoom-out" data-aos-delay="300" data-aos-duration="500" data-aos-once="true">
                <div class="aboutIconBoxImg">
                    <img class="img-fluid" src="<?php echo $image_uri7; ?>" alt="<?php echo $image_uri_alt7; ?>" />
                </div>
                <div class="aboutIconBoxTitle">
                    <h4 class="dinproReg"><?php echo $div_title7; ?></h4>
                </div>
            </div>
            <div class="aboutHospitalityIconsBox flex" data-aos="zoom-out" data-aos-delay="300" data-aos-duration="500" data-aos-once="true">
                <div class="aboutIconBoxImg">
                    <img class="img-fluid" src="<?php echo $image_uri8; ?>" alt="<?php echo $image_uri_alt8; ?>" />
                </div>
                <div class="aboutIconBoxTitle">
                    <h4 class="dinproReg"><?php echo $div_title8; ?></h4>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>

<?php
echo $args['after_widget'];
    }

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['title2'] = $new_instance['title2'];
        $instance['description'] = $new_instance['description'];
        $instance['learn_more_text'] = $new_instance['learn_more_text'];

        $instance['link_type'] = $new_instance['link_type'];
        if ($new_instance['link_type'] == 'page') {
            $instance['page'] = $new_instance['page'];
            $instance['link'] = '';
        } elseif ($new_instance['link_type'] == 'link') {
            $instance['link'] = $new_instance['link'];
            $instance['page'] = '';

        }

        $instance['image_uri'] = $new_instance['image_uri'];
        $instance['image_uri_alt'] = $new_instance['image_uri_alt'];

        
        for ($i = 0; $i < 9; $i++) {

            $instance['block-' . $i] = $new_instance['block-' . $i];
            $instance['div_title' . $i] = strip_tags($new_instance['div_title' . $i]);
            $instance['image_uri' . $i] = strip_tags($new_instance['image_uri' . $i]);
            $instance['image_uri_alt' . $i] = strip_tags($new_instance['image_uri_alt' . $i]);

           
        }

        return $instance;
    }

    public function form($display_instance)
    {
        $widget_add_id_slider_client = $this->get_field_id('') . "add";

        $description = $display_instance['description'];
        $learn_more_text = $display_instance['learn_more_text'];
        $link = $display_instance['link'];
        $title2 = $display_instance['title2'];

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('title2') . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('title2') . '" name="' . $this->get_field_name('title2') . '" type="text" value="' . $title2 . '" />';
        $rew_html .= '</p><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('description') . '"> ' . __('Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<textarea rows="6" cols="35" id="' . $this->get_field_id('description') . '" name="' . $this->get_field_name('description') . '" >' . $description . '</textarea>';
        $rew_html .= '</p><br><br><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('learn_more_text') . '"> ' . __('Learn More Text', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('learn_more_text') . '" name="' . $this->get_field_name('learn_more_text') . '" type="text" value="' . $learn_more_text . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('link_type') . '"> ' . __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<select id="' . $this->get_field_id('link_type') . '" name="' . $this->get_field_name('link_type') . '" onChange="show_hide_div_agilysys_about_when_you_work_widget(this.value);">';
        $rew_html .= '<option value="">Please Select</option>';

        $link_type = $display_instance['link_type'];

        if ($link_type == 'page') {
            $rew_html .= '<option value="page" selected="selected">Internal Page Link</option>';
        } else {
            $rew_html .= '<option value="page">Internal Page Link</option>';
        }

        if ($link_type == 'link') {
            $rew_html .= '<option value="link" selected="selected">External Link</option>';
        } else {
            $rew_html .= '<option value="link">External Link</option>';
        }

        $rew_html .= '</select>';
        $rew_html .= '</p>';

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block"';
            $show2 = 'style="display:none"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none"';
            $show2 = 'style="display:block"';

        } else {
            $show1 = 'style="display:none"';
            $show2 = 'style="display:none"';
        }
        $rew_html .= '<div id="page_div_agilysys_about_when_you_work_widget" ' . $show1 . '><p>';
        $rew_html .= '<label for="' . $this->get_field_id('page') . '"> ' . __('Page', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<select id="' . $this->get_field_id('page') . '" name="' . $this->get_field_name('page') . '">';
        $rew_html .= '<option value="">Please Select</option>';

        $page = $display_instance['page'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                $rew_html .= '<option value="' . $key->ID . '" selected="selected">' . $key->post_title . '</option>';
            } else {
                $rew_html .= '<option value="' . $key->ID . '">' . $key->post_title . '</option>';
            }

        }

        $rew_html .= '</select>';
        $rew_html .= '</p></div><br>';

        $rew_html .= '<div id="link_div_agilysys_about_when_you_work_widget" ' . $show2 . '><p>';
        $rew_html .= '<label for="' . $this->get_field_id('link') . '"> ' . __('Link', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('link') . '" name="' . $this->get_field_name('link') . '" type="text" value="' . $link . '" />';
        $rew_html .= '</p></div><br>';
        ?>
<script>
function show_hide_div_agilysys_about_when_you_work_widget(val) {
 console.log(val);  
    if (val == 'page') {
        jQuery("#page_div_agilysys_about_when_you_work_widget").show();
        jQuery("#link_div_agilysys_about_when_you_work_widget").hide();
    } else if (val == 'link') {
        jQuery("#page_div_agilysys_about_when_you_work_widget").hide();
        jQuery("#link_div_agilysys_about_when_you_work_widget").show();
    }

}
</script>

<?php

        $rew_html .= '<div class="' . $widget_add_id_what_makes . '-input-containers"><div id="entries_agilysys_about_when_you_work_widget">';

        for ($i = 0; $i < 9; $i++) {
            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'AGILYSYS_TEXT_DOMAIN') . ' </span>';

            $display = (!isset($display_instance['block-' . $i]) || ($display_instance['block-' . $i] == "")) ? 'style="display:block;"' : '';

            $rew_html .= '<div class="entry-desc cf">';
            $rew_html .= '<input id="' . $this->get_field_id('block-' . $i) . '" name="' . $this->get_field_name('block-' . $i) . '" type="hidden" value="' . $display_instance['block-' . $i] . '">';

            $rew_html .= '<br><p><label for="' . $this->get_field_id('div_title' . $i) . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('div_title' . $i) . '" name="' . $this->get_field_name('div_title' . $i) . '" type="text" value="' . $display_instance['div_title' . $i] . '">';
            $rew_html .= '</p>';

            $rew_html .= '<br><div class="widg-img' . $i . '">';
            $show1 = (empty($display_instance['image_uri' . $i])) ? 'style="display:none;"' : '';
            $rew_html .= '<label id="image_uri_agilysys_about_when_you_work_widget'. $i.'"></label><br><img class="' . $this->get_field_id('image_uri' . $i) . '_media_image' . $i . ' custom_media_image' . $i . '" src="' . $display_instance['image_uri' . $i] . '" ' . $show1 . ' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_uri' . $i) . '_media_id' . $i . ' custom_media_id' . $i . '" name="' . $this->get_field_name('image_uri' . $i) . '" id="' . $this->get_field_id('image_uri' . $i) . '" value="' . $display_instance['image_uri' . $i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_uri' . $i) . '_media_url' . $i . ' custom_media_url' . $i . '" name="' . $this->get_field_name('image_uri' . $i) . '" id="' . $this->get_field_id('image_uri' . $i) . '" value="' . $display_instance['image_uri' . $i] . '">';
            $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_upload' . $i . '" id="' . $this->get_field_id('image_uri' . $i) . '"/>';

            $rew_html .= '</div><br>';


            $rew_html .= '<p><label for="' . $this->get_field_id('image_uri_alt' . $i) . '"> ' . __('Image Alt', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt' . $i) . '" name="' . $this->get_field_name('image_uri_alt' . $i) . '" type="text" value="' . $display_instance['image_uri_alt' . $i] . '">';
            $rew_html .= '</p>';
            ?>

<script>
jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {


                    if(attachment.height==1080 && attachment.width==1920)
                    {
                    jQuery('.' + button_id_s + '_media_id<?php echo $i; ?>').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_url<?php echo $i; ?>').val(attachment.url);
                    jQuery('.' + button_id_s + '_media_image<?php echo $i; ?>').attr('src',
                        attachment.url).css('display', 'block');
                        jQuery('#image_uri_agilysys_about_when_you_work_widget<?php echo $i; ?>').html("");
                    }
                    else
                    {
                        jQuery('#image_uri_agilysys_about_when_you_work_widget<?php echo $i; ?>').html("Please Enter the correct Dimensions 1920x1080").css('color','red');
                    }


                        
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload<?php echo $i; ?>');

});
</script>

<?php
$rew_html .= '</div></div>';
        }
        $rew_html .= '</div></div>';
        ?>


<style>
#rew_container_agilysys_about_when_you_work_widget p{
    padding:20px;
}

.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}


#rew_container_agilysys_about_when_you_work_widget select {
    float: left;
    width: 60%;
    margin-top: 20px !important;
    margin-bottom: 10px !important;
}

#rew_container_agilysys_about_when_you_work_widget input,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_about_when_you_work_widget label {
    width: 40%;
    float: left;
}

#rew_container_agilysys_about_when_you_work_widget p {
    padding:20px !important;
}

<?php echo '.' . $widget_add_id_slider_client;

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}


#entries_agilysys_about_when_you_work_widget {
    padding: 10px 0 0;
}

#entries_agilysys_about_when_you_work_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_about_when_you_work_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_about_when_you_work_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_about_when_you_work_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_about_when_you_work_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_about_when_you_work_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_about_when_you_work_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_about_when_you_work_widget #entries_agilysys_about_when_you_work_widget p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_about_when_you_work_widget">
    <?php echo $rew_html; ?>
</div>


<?php

    }

}
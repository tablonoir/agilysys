<?php

function agilysys_home_bottom_strips()
{
    register_widget('agilysys_home_bottom_strip');
}

add_action('widgets_init', 'agilysys_home_bottom_strips');

class agilysys_home_bottom_strip extends WP_Widget
{
    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Home Bottom Strip', 'AGILYSYS_TEXT_DOMAIN'));

        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {
        extract($args);
        $max_entries_agilysys_home_bottom_strip_row_icon = 9;
        echo $args['before_widget'];

        ?>


<section class="homebannerStrip flex">

    <?php for ($i = 0; $i < $max_entries_agilysys_home_bottom_strip_row_icon + 1; $i++) {
            $block = $instance['block-' . $i];

            $home_bottom_icon_image = esc_url($instance['home_bottom_icon_image' . $i]);

            $image_uri_alt = $instance['image_uri_alt' . $i];
            $review_icon_url = $instance['review_icon_url-' . $i];
            $home_bottom_icon_text = $instance['home_bottom_icon_text-' . $i];

            ?>
    <div class="homeStripBox flex">
        <div class="">
            <!--<img class="img-fluid" src="<?php echo $home_bottom_icon_image; ?>" alt="" />-->
            <img class="img-fluid" src="<?php echo $home_bottom_icon_image; ?>" alt="<?php echo $image_uri_alt; ?>" />
        </div>
        <div class="homeStripBoxContent">
            <h4 class="dinProStd"><?php echo $home_bottom_icon_text; ?></h4>
        </div>
    </div>
    <?php }?>

</section>

<?php
echo $args['after_widget'];
        ?>
<?php
}

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $max_entries_agilysys_home_bottom_strip_review_icon = 9;

        for ($i = 0; $i < $max_entries_agilysys_home_bottom_strip_review_icon; $i++) {
            $block = $new_instance['block-' . $i];
            $instance['home_bottom_icon_image' . $i] = strip_tags($new_instance['home_bottom_icon_image' . $i]);
            $instance['image_uri_alt' . $i] = strip_tags($new_instance['image_uri_alt' . $i]);
            $instance['review_icon_url-' . $i] = strip_tags($new_instance['review_icon_url-' . $i]);
            $instance['home_bottom_icon_text-' . $i] = strip_tags($new_instance['home_bottom_icon_text-' . $i]);
        }
        return $instance;
    }

    public function form($display_instance)
    {

        $max_entries_agilysys_home_bottom_strip_review_count = 5;

//                print_r($max_entries_agilysys_home_bottom_strip_review_count); $widget_add_id_review_ico

        $widget_add_id_review_ico = $this->get_field_id('') . "add";

        $rew_html .= '<div class="' . $widget_add_id_review_ico . '-input-containers"><div id="entries_agilysys_home_bottom_strip">';

        for ($i = 0; $i < 9; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Row', 'agilysys_text_domain') . ' </span>';
            $rew_html .= '<div class="entry-desc cf">';
            $rew_html .= '<input id="' . $this->get_field_id('block-' . $i) . '" name="' . $this->get_field_name('block-' . $i) . '" type="hidden" value="' . $display_instance['block-' . $i] . '">';
            /**
             * Block Caption
             */$display = (isset($display_instance['block-' . $i]) || ($display_instance['block-' . $i] == "")) ? 'style="display:block;"' : '';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('home_bottom_icon_text-' . $i) . '"> ' . __('Title', 'agilysys_text_domain') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('home_bottom_icon_text-' . $i) . '" name="' . $this->get_field_name('home_bottom_icon_text-' . $i) . '" type="text" value="' . $display_instance['home_bottom_icon_text-' . $i] . '">';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('review_icon_url-' . $i) . '"> ' . __('URL', 'agilysys_text_domain') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('review_icon_url-' . $i) . '" name="' . $this->get_field_name('review_icon_url-' . $i) . '" type="text" value="' . $display_instance['review_icon_url-' . $i] . '">';
            $rew_html .= '</p><br><br>';

       
            $rew_html.= '<label style="margin-left:10px !important;" for="' . $this->get_field_id('home_bottom_icon_image' . $i) . '"> ' . __('Image', 'agilysys_text_domain') . ' :</label>';
            $rew_html.= '<div class="widg-img' . $i . '">';
            $show2 = (empty($display_instance['home_bottom_icon_image' . $i])) ? 'style="display:none;"' : '';
            $rew_html.= '<label id="image_uri_agilysys_home_bottom_strip' . $i . '"><br></label><img class="' . $this->get_field_id('home_bottom_icon_image' . $i) . '_media_imageb' . $i . ' custom_media_imageb' . $i . '" src="' . $display_instance['home_bottom_icon_image' . $i] . '" ' . $show2 . ' width=200" height="120"/>';
            $rew_html.= '<input type="hidden" class="' . $this->get_field_id('home_bottom_icon_image' . $i) . '_media_idb' . $i . ' custom_media_idb' . $i . '" name="' . $this->get_field_name('home_bottom_icon_image' . $i) . '" id="' . $this->get_field_id('home_bottom_icon_image' . $i) . '" value="' . $display_instance['home_bottom_icon_image' . $i] . '" />';
            $rew_html.= '<input type="hidden" class="' . $this->get_field_id('home_bottom_icon_image' . $i) . '_media_urlb' . $i . ' custom_media_urlb' . $i . '" name="' . $this->get_field_name('home_bottom_icon_image' . $i) . '" id="' . $this->get_field_id('home_bottom_icon_image' . $i) . '" value="' . $display_instance['home_bottom_icon_image' . $i] . '">';
            $rew_html.= '<input type="button" style="margin-top:-20px;" value="Upload Image" class="button custom_media_uploadb' . $i . '" id="' . $this->get_field_id('home_bottom_icon_image' . $i) . '"/>';
            $rew_html.= '</div><br>';


            $rew_html.= '<p>';
            $rew_html.= '<label for="' . $this->get_field_id('image_uri_alt' . $i) . '"> ' . __('Image Alt', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html.= '<input id="' . $this->get_field_id('image_uri_alt' . $i) . '" name="' . $this->get_field_name('image_uri_alt' . $i) . '" type="text" value="' . $display_instance['image_uri_alt' . $i] . '">';
            $rew_html.= '</p>';

            ?>

<script>
jQuery(document).ready(function() {


    function media_uploadb(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_uploadb<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    if (attachment.height == 1080 && attachment.width == 1920) {
                        jQuery('.' + button_id_s + '_media_idb<?php echo $i; ?>').val(attachment
                        .id);
                        jQuery('.' + button_id_s + '_media_urlb<?php echo $i; ?>').val(attachment
                            .url);
                        jQuery('.' + button_id_s + '_media_imageb<?php echo $i; ?>').attr('src',
                            attachment.url).css('display', 'block');

                        jQuery('#image_uri_agilysys_home_bottom_strip<?php echo $i; ?>')
                            .html("");
                    } else {
                        jQuery('#image_uri_agilysys_home_bottom_strip<?php echo $i; ?>')
                            .html("Please Enter the correct Dimensions 1920x1080").css('color',
                                'red');

                    }
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_uploadb('.custom_media_uploadb<?php echo $i; ?>');

});
</script> <?php

            $rew_html .= '</div></div>';

        }

        $rew_html .= '</div></div>';

        ?>
<script>
jQuery(document).ready(function(e) {
    jQuery.each(jQuery(".<?php echo $widget_add_id_review_ico; ?>-input-containers #entries_agilysys_home_bottom_strip").children(),
        function() {
            //                        alert(jQuery(this).find('input').val());
            if (jQuery(this).find('input').val() != '') {
                jQuery(this).show();
            }
        });
    jQuery(".<?php echo $widget_add_id_review_ico; ?>").bind('click', function(e) {
        var rows = 1;
        jQuery.each(jQuery(".<?php echo $widget_add_id_review_ico; ?>-input-containers #entries_agilysys_home_bottom_strip")
            .children(),
            function() {
                if (jQuery(this).find('input').val() == '') {
                    jQuery(this).find(".entry-title").addClass("active");
                    jQuery(this).find(".entry-desc").slideDown();
                    jQuery(this).find('input').first().val('0');
                    jQuery(this).show();
                    return false;
                } else {
                    rows++;
                    jQuery(this).show();
                    jQuery(this).find(".entry-title").removeClass("active");
                    jQuery(this).find(".entry-desc").slideUp();
                }
            });
        if (rows == '<?php echo $max_entries_agilysys_home_bottom_strip_review_count; ?>') {
            jQuery("#rew_container #message").show();
        }
    });
    jQuery(".delete-row").bind('click', function(e) {
        var count = 1;
        var current = jQuery(this).closest('.entrys').attr('id');
        jQuery.each(jQuery("#entries_agilysys_home_bottom_strip #" + current + " .entry-desc").children(), function() {
            jQuery(this).val('');
        });
        jQuery.each(jQuery("#entries_agilysys_home_bottom_strip #" + current + " .entry-desc p").children(), function() {
            jQuery(this).val('');
        });
        jQuery('#entries_agilysys_home_bottom_strip #' + current + " .entry-title").removeClass('active');
        jQuery('#entries_agilysys_home_bottom_strip #' + current + " .entry-desc").hide();
        jQuery('#entries_agilysys_home_bottom_strip #' + current).remove();
        jQuery.each(jQuery(".<?php echo $widget_add_id_review_ico; ?>-input-containers #entries_agilysys_home_bottom_strip")
            .children(),
            function() {
                if (jQuery(this).find('input').val() != '') {
                    jQuery(this).find('input').first().val(count);
                }
                count++;
            });
    });
});
</script>
<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_home_bottom_strip p{
    padding:10px;
}

#rew_container_agilysys_home_bottom_strip input,
select,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_home_bottom_strip label {
    width: 40%;
}

<?php echo '.'. $widget_add_id_review_ico;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}

#entries_agilysys_home_bottom_strip #remove-img-bg {
    background: url('<?php echo ZWREW_URL; ?>assets/images/deleteimg.png') center center no-repeat;
    width: 20px;
    height: 22px;
    display: none;
}

#entries_agilysys_home_bottom_strip {
    padding: 10px 0 0;
}

#entries_agilysys_home_bottom_strip .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_home_bottom_strip .entrys:first-child {
    margin: 0;
}

#entries_agilysys_home_bottom_strip .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_home_bottom_strip .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_home_bottom_strip .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_home_bottom_strip .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_home_bottom_strip .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_home_bottom_strip #entries_agilysys_home_bottom_strip plast label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_home_bottom_strip">
    <?php echo $rew_html; ?>
</div>
<?php
} //Function form ends here
}
<?php

function agilysys_customer_success_stories_widgets()
{
    register_widget('agilysys_customer_success_stories_widget');
}

add_action('widgets_init', 'agilysys_customer_success_stories_widgets');

class agilysys_customer_success_stories_widget extends WP_Widget
{
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Customer Success Stories Widget', 'agilysys_text_domain'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
        add_action('load-widgets.php', array(&$this, 'agilysys_color_picker_load'));

    }
    public function agilysys_color_picker_load()
    {
        wp_enqueue_style('wp-color-picker');
        wp_enqueue_script('wp-color-picker');
    }
    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        extract($args);
        $max_entries_agilysys_customer_success_stories_widget_hospitality_software = 25;
        $bg_color = !empty($instance['background_color']) ? $instance['background_color'] : '';
        $review_title = !empty($instance['review_title']) ? $instance['review_title'] : '';

        ?>
<section class="cusSuccessSection waterBG" style="background:<?php echo $bg_color; ?>">
    <h2 class="dinProStd whiteText h2 center"><?php echo $review_title; ?></h2>

    <div class="cusSuccessStories flex">
        <?php

        $count = count($instance['type']);
        for ($i = 0; $i < $count; $i++) {

            $type = $instance['type'][$i];
            $image_uri = $instance['image_uri'][$i];
            $image_uri_alt = $instance['image_uri_alt'][$i];
            $leadersDesc = $instance['leadersDesc'][$i];
            $leadersDesc4 = $instance['leadersDesc4'][$i];
            $leadersDesc5 = $instance['leadersDesc5'][$i];

            $link_type = $instance['link_type'][$i];
            if ($link_type == "link") {
                $leadersDesc6 = $instance['leadersDesc6'][$i];
            } elseif ($link_type == "page") {
                $post_id = $instance['page'][$i];
                $post = get_post($post_id);
                $leadersDesc6 = home_url($post->post_name) . "/";
            }

            ?>
        <?php

            if ($type == "image") {
                ?>
        <div class="cusSuccessBox flex">
            <div class="cusSuccessBoxImg">
                <img class="img-fluid" src="<?php echo $image_uri; ?>" alt="<?php echo $image_uri_alt; ?>" />
            </div>
            <div class="cusSuccessContent center">
                <h4 class="dinProStd greenText"><?php echo $leadersDesc; ?></h4>
                <p><?php echo $leadersDesc4; ?></p>
                <a href="<?php echo $leadersDesc6; ?>" class="aboutButton violetText"><?php echo $leadersDesc5; ?> <i
                        class="fa fa-arrow-right" aria-hidden="true"></i></a>
            </div>
        </div>
        <?php

            }

        }
        ?>
    </div>

    <a class="homeBannerButton whiteText" href="#">See all customer sucess stories <i class="fa fa-arrow-right"
            aria-hidden="true"></i></a>

</section>

<?php
echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $max_entries_agilysys_customer_success_stories_widget_hospitality_software = 25;
        $instance['background_color'] = $new_instance['background_color'];

        $instance['review_title'] = ($new_instance['review_title']);
        $count = count($new_instance['type']);
        for ($i = 0; $i < $count; $i++) {

            $instance['type'][$i] = $new_instance['type'][$i];
            $instance['leadersDesc'][$i] = $new_instance['leadersDesc'][$i];
            $instance['leadersDesc4'][$i] = $new_instance['leadersDesc4'][$i];
            $instance['leadersDesc5'][$i] = $new_instance['leadersDesc5'][$i];
            $instance['image_uri'][$i] = $new_instance['image_uri'][$i];
            $instance['image_uri_alt'][$i] = $new_instance['image_uri_alt'][$i];


            

            $instance['link_type'][$i] = $new_instance['link_type'][$i];
            if ($new_instance['link_type'][$i] == 'page') {
                $instance['page'][$i] = $new_instance['page'][$i];
                $instance['leadersDesc6'][$i] = '';
            } elseif ($new_instance['link_type'][$i] == 'link') {
                $instance['leadersDesc6'][$i] = $new_instance['leadersDesc6'][$i];
                $instance['page'][$i] = '';

            }

        }
        return $instance;
    }
    public function form($display_instance)
    {
        ?>
<script>
jQuery(document).ready(function($) {
    $('.my-color-picker').wpColorPicker();
});
</script>
<?php

        $max_entries_agilysys_customer_success_stories_widget_hospitality_software = 25;

        $widget_add_id_hosipitality_software = $this->get_field_id('') . "add";
        $review_title = ($display_instance['review_title']);
        $contentDesc = ($display_instance['content']);
        $background_color = ($display_instance['background_color']);

        $rew_html .= '<label for="' . $this->get_field_id('background_color') . '"> ' . __('Color', 'agilysys_text_domain') . ' :</label>';
        $rew_html .= '<input  class="my-color-picker" id="' . $this->get_field_id('background_color') . '" name="' . $this->get_field_name('background_color') . '" type="text" value="' . $background_color . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<label for="' . $this->get_field_id('review_title') . '"> ' . __('Title', 'agilysys_text_domain') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('review_title') . '" name="' . $this->get_field_name('review_title') . '" type="text" value="' . $review_title . '" />';
        $rew_html .= '</p><br><br>';

        $count = count($display_instance['type']);

        $rew_html .= '<div class="add_new_rowxx-input-containers"><div id="entries_agilysys_customer_success_stories_widget">';

        $rew_html .= '<input class="cnt909" id="cnt909" name="cnt" type="hidden" value="' . $count . '">';

        for ($i = 0; $i < $count; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'agilysys_text_domain') . ' </span>';
            $rew_html .= '<div class="entry-desc cf">';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('type' . $i) . '"> ' . __('Media Type', 'agilysys_text_domain') . ' :</label>';
            $rew_html .= '<select name="' . $this->get_field_name('type[]') . '" id="' . $this->get_field_id('type' . $i) . '" onChange="show_hide_media_agilysys_customer_success_stories_widget(this.value,' . $i . ');">';
            $rew_html .= '<option value="">Please Select</option>';

            if ($display_instance['type'][$i] == "image") {
                $rew_html .= '<option value="image" selected="selected">Image</option>';
            } else {
                $rew_html .= '<option value="image">Image</option>';
            }

            $rew_html .= '</select>';
            $rew_html .= '</p>';

            $show1 = (!empty($display_instance['image_uri'][$i]) && $display_instance['type'][$i] == "image") ? '' : 'style="display:none;"';
            $rew_html .= '<div class="widg-img' . $i . '" ' . $show1 . '>';
            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('image_id-' . $i) . '"> ' . __('Image', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<img class="' . $this->get_field_id('image_id' . $i) . '_media_image' . $i . ' custom_media_image' . $i . '" src="' . $display_instance['image_uri'][$i] . '" ' . $show1 . ' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id' . $i) . '_media_id' . $i . ' custom_media_id' . $i . '" name="' . $this->get_field_name('image_id[]') . '" id="' . $this->get_field_id('image_id' . $i) . '" value="' . $display_instance['image_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id' . $i) . '_media_url' . $i . ' custom_media_url' . $i . '" name="' . $this->get_field_name('image_uri[]') . '" id="' . $this->get_field_id('image_uri-' . $i) . '" value="' . $display_instance['image_uri'][$i] . '" />';
            $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_upload' . $i . '" id="' . $this->get_field_id('image_id' . $i) . '"/>';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt' . $i) . '"> ' . __('Image Alt', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt' . $i) . '" name="' . $this->get_field_name('image_uri_alt[]') . '" type="text" value="' . $display_instance['image_uri_alt'][$i] . '" />';
            $rew_html .= '</p>';


            

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('leadersDesc' . $i) . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('leadersDesc' . $i) . '" name="' . $this->get_field_name('leadersDesc[]') . '" type="text" value="' . $display_instance['leadersDesc'][$i] . '" />';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('leadersDesc4' . $i) . '"> ' . __('Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('leadersDesc4' . $i) . '" name="' . $this->get_field_name('leadersDesc4[]') . '" type="text" value="' . $display_instance['leadersDesc4'][$i] . '" />';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('leadersDesc5-' . $i) . '"> ' . __('Button Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('leadersDesc5-' . $i) . '" name="' . $this->get_field_name('leadersDesc5[]') . '" type="text" value="' . $display_instance['leadersDesc5'][$i] . '" />';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('link_type' . $i) . '"> ' . __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('link_type' . $i) . '" name="' . $this->get_field_name('link_type[]') . '" onChange="show_hide_div_agilysys_customer_success_stories_widget(this.value,' . $i . ');">';
            $rew_html .= '<option value="">Please Select</option>';

            $link_type = $display_instance['link_type'][$i];

            if ($link_type == 'page') {
                $rew_html .= '<option value="page" selected="selected">Internal Page Link</option>';
            } else {
                $rew_html .= '<option value="page">Internal Page Link</option>';
            }

            if ($link_type == 'link') {
                $rew_html .= '<option value="link" selected="selected">External Link</option>';
            } else {
                $rew_html .= '<option value="link">External Link</option>';
            }

            $rew_html .= '</select>';
            $rew_html .= '</p><br><br>';

            $args = array(
                'sort_order' => 'desc',
                'sort_column' => 'post_title',
                'hierarchical' => 1,
                'exclude' => '',
                'include' => '',
                'meta_key' => '',
                'meta_value' => '',
                'authors' => '',
                'child_of' => 0,
                'parent' => -1,
                'exclude_tree' => '',
                'number' => '',
                'offset' => 0,
                'post_type' => 'page',
                'post_status' => 'publish',
            );
            $pages = get_pages($args); // get all pages based on supplied args

            if ($link_type == 'page') {
                $show1 = 'style="display:block"';
                $show2 = 'style="display:none"';
            } elseif ($link_type == 'link') {
                $show1 = 'style="display:none"';
                $show2 = 'style="display:block"';

            } else {
                $show1 = 'style="display:none"';
                $show2 = 'style="display:none"';
            }
            $rew_html .= '<div id="page_div_agilysys_customer_success_stories_widget' . $i . '" ' . $show1 . '><p>';
            $rew_html .= '<label for="' . $this->get_field_id('page' . $i) . '"> ' . __('Page', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('page' . $i) . '" name="' . $this->get_field_name('page[]') . '">';
            $rew_html .= '<option value="">Please Select</option>';

            $page = $display_instance['page'][$i];

            foreach ($pages as $key) {

                if ($page == $key->ID) {
                    $rew_html .= '<option value="' . $key->ID . '" selected="selected">' . $key->post_title . '</option>';
                } else {
                    $rew_html .= '<option value="' . $key->ID . '">' . $key->post_title . '</option>';
                }

            }

            $rew_html .= '</select>';
            $rew_html .= '</p></div><br><br>';

            $rew_html .= '<div id="link_div_agilysys_customer_success_stories_widget' . $i . '" ' . $show2 . '><p>';
            $rew_html .= '<label for="' . $this->get_field_id('leadersDesc6' . $i) . '"> ' . __('Link', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('leadersDesc6' . $i) . '" name="' . $this->get_field_name('leadersDesc6[]') . '" type="text" value="' . $display_instance['leadersDesc6'][$i] . '" />';
            $rew_html .= '</p></div><br><br>';
            ?>
<script>
function show_hide_div_agilysys_customer_success_stories_widget(val, i) {
console.log(val);
    if (val == 'page') {
        jQuery("#page_div_agilysys_customer_success_stories_widget" + i).show();
        jQuery("#link_div_agilysys_customer_success_stories_widget" + i).hide();
    } else if (val == 'link') {
        jQuery("#page_div_agilysys_customer_success_stories_widget" + i).hide();
        jQuery("#link_div_agilysys_customer_success_stories_widget" + i).show();
    }

}
</script>

<?php

            $rew_html .= '</div>';

            ?>


<script>
function show_hide_media_agilysys_customer_success_stories_widget(value, id) {

console.log(value);

    if (value == "image") {

        jQuery('.widg-img' + id).show();
        jQuery('.widg-youtube' + id).hide();
        jQuery('.widg-video' + id).hide();

    } else if (value == "youtube") {
        jQuery('.widg-img' + id).hide();
        jQuery('.widg-youtube' + id).show();
        jQuery('.widg-video' + id).hide();
    } else if (value == "video") {
        jQuery('.widg-img' + id).hide();
        jQuery('.widg-youtube' + id).hide();
        jQuery('.widg-video' + id).show();
    }

}

jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {


                    if (attachment.height == 1080 && attachment.width == 1920) {
                        jQuery('.' + button_id_s + '_media_id<?php echo $i; ?>').val(attachment.id);
                        jQuery('.' + button_id_s + '_media_url<?php echo $i; ?>').val(attachment
                            .url);
                        jQuery('.' + button_id_s + '_media_image<?php echo $i; ?>').attr('src',
                            attachment.url).css('display', 'block');
                        jQuery('#image_uri_agilysys_customer_success_stories_widget<?php echo $i; ?>')
                            .html("");
                    } else {
                        jQuery('#image_uri_agilysys_customer_success_stories_widget<?php echo $i; ?>')
                            .html("Please Enter the correct Dimensions 1920x1080").css('color',
                                'red');
                    }

                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload<?php echo $i; ?>');

});
</script>





<?php

            $k = $i + 1;
            $rew_html .= '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' . $k . ');">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';

        }

        $rew_html .= '</div></div>';

        $rew_html .= '<div class="' . $widget_add_id_hosipitality_software . '" style="margin-bottom: 36px;text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;" onClick="add_new_row();">' . __('ADD ROW', 'AGILYSYS_TEXT_DOMAIN') . '</div>';
        ?>
<script>
function add_new_row() {
    var cnt = '';

    jQuery.each(jQuery("#entries_agilysys_customer_success_stories_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            cnt = jQuery(this).val();
        }
    });

    cnt++;

    jQuery.each(jQuery("#entries_agilysys_customer_success_stories_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(cnt);
        }
    });
    
    console.log(cnt);

    var new_row = '<div id="entry' + cnt +
        '"  class="entrys"><span class="entry-title" onclick = "slider(this);"><?php echo __('Add New Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span>';
    new_row += '<div class="entry-desc cf">';


    new_row += '<p>';
    new_row += '<label><?php echo __('Media Type', 'agilysys_text_domain'); ?>:</label>';
    new_row += '<select name="<?php echo $this->get_field_name('type[]'); ?>" onChange="show_hide_media_agilysys_customer_success_stories_widget(this.value,' +
        cnt + ');">';
    new_row += '<option value="">Please Select</option>';
    new_row += '<option value="image">Image</option>';
    new_row += '</select>';
    new_row += '</p>';


    new_row += '<div class="widg-img' + cnt + '" style="display:none;">';
    new_row += '<p>';
    new_row += '<label><?php echo __('Image', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
    new_row += '<label id="image_uri_agilysys_customer_success_stories_widget' + cnt +
            '"></label><br><img class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_image' + cnt +
        ' custom_media_image' + cnt + '" src="" style="display:none;" width=200" height="120"/>';
    new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_id' + cnt +
        ' custom_media_id' + cnt + '" name="<?php echo $this->get_field_name('image_id[]'); ?>"  />';
    new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_url' + cnt +
        ' custom_media_url' + cnt + '" name="<?php echo $this->get_field_name('image_uri[]'); ?>"  />';
    new_row += '<input type="button" value="Upload Image" class="button custom_media_upload' + cnt +
        '" id="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '"/>';
    new_row += '</p>';

    new_row += '<p>';
    new_row += '<label for=""><?php echo __('Image Alt', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';

    new_row +=
        '<input class="" name="<?php echo esc_attr($this->get_field_name('image_uri_alt[]')); ?>" type="text" value="">';
    new_row += '</p>';

    new_row += '<p>';
    new_row += '<label> <?php echo __('Title', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
    new_row += '<input name="<?php echo $this->get_field_name('leadersDesc[]'); ?>" type="text" value="" />';
    new_row += '</p>';

    jQuery(document).ready(function() {




        function media_upload(button_class) {
            var _custom_media = true,
                _orig_send_attachment = wp.media.editor.send.attachment;
            jQuery('body').on('click', '.custom_media_upload' + cnt, function(e) {
                var button_id = '#' + jQuery(this).attr('id');
                var button_id_s = jQuery(this).attr('id');
                console.log(button_id);
                var self = jQuery(button_id);
                var send_attachment_bkp = wp.media.editor.send.attachment;
                var button = jQuery(button_id);
                var id = button.attr('id').replace('_button', '');
                _custom_media = true;

                wp.media.editor.send.attachment = function(props, attachment) {
                    if (_custom_media) {

                        if (attachment.height == 1080 && attachment.width == 1920) {
                            jQuery('.' + button_id_s + '_media_id' + cnt).val(attachment.id);
                            jQuery('.' + button_id_s + '_media_url' + cnt).val(attachment.url);
                            jQuery('.' + button_id_s + '_media_image' + cnt).attr('src',
                                attachment.url).css('display', 'block');
                            jQuery('#image_uri_agilysys_customer_success_stories_widget' + cnt)
                                .html("");
                        } else {
                            jQuery('#image_uri_agilysys_customer_success_stories_widget' + cnt)
                                .html("Please Enter the correct Dimensions 1920x1080").css(
                                    'color', 'red');

                        }

                    } else {
                        return _orig_send_attachment.apply(button_id, [props, attachment]);
                    }
                }
                wp.media.editor.open(button);
                return false;
            });
        }
        media_upload('.custom_media_upload' + cnt);

    });





    new_row += '<p>';
    new_row += '<label><?php echo __('Description', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
    new_row += '<input  name="<?php echo $this->get_field_name('leadersDesc4[]'); ?>" type="text" value="" />';
    new_row += '</p>';

    new_row += '<p>';
    new_row += '<label> <?php echo __('Button Title', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
    new_row += '<input name="<?php echo $this->get_field_name('leadersDesc5[]'); ?>" type="text"  />';
    new_row += '</p>';


    new_row += '<p>';
    new_row += '<label><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
    new_row +=
        '<select name="<?php echo $this->get_field_name('link_type[]'); ?>" onChange="show_hide_div_agilysys_customer_success_stories_widget(this.value,' +
        cnt + ');">';
    new_row += '<option value="">Please Select</option>';

    new_row += '<option value="page">Internal Page Link</option>';

    new_row += '<option value="link">External Link</option>';

    new_row += '</select>';
    new_row += '</p><br>';


    <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        ?>


    new_row += '<div id="page_div_agilysys_customer_success_stories_widget' + cnt + '" style="display:none;"><p>';
    new_row += '<label><?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
    new_row += '<select  name="<?php echo $this->get_field_name('page[]'); ?>">';
    new_row += '<option value="">Please Select</option>';


    <?php
foreach ($pages as $key) {
            ?>



    new_row += '<option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>';


    <?php
}
        ?>

    new_row += '</select>';
    new_row += '</p></div><br><br>';

    new_row += '<div id="link_div_agilysys_customer_success_stories_widget' + cnt + '" style="display:none;"><p>';
    new_row += '<label> <?php echo __('Link', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
    new_row += '<input name="<?php echo $this->get_field_name('leadersDesc6[]'); ?>" type="text"  />';
    new_row += '</p></div><br>';



    var new_cnt = cnt;

    new_row += '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' + new_cnt +
        ');"><?php echo __('Delete Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span></a></p>';
    new_row += '</div></div>';

    jQuery('.add_new_rowxx-input-containers #entries_agilysys_customer_success_stories_widget').append(new_row);




}

function show_hide_div_agilysys_customer_success_stories_widget(val, i) {
console.log(val);
    if (val == 'page') {
        jQuery("#page_div_agilysys_customer_success_stories_widget" + i).show();
        jQuery("#link_div_agilysys_customer_success_stories_widget" + i).hide();
    } else if (val == 'link') {
        jQuery("#page_div_agilysys_customer_success_stories_widget" + i).hide();
        jQuery("#link_div_agilysys_customer_success_stories_widget" + i).show();
    }

}

function show_hide_media_agilysys_customer_success_stories_widget(value, id) {

console.log(value);

    if (value == "image") {

        jQuery('.widg-img' + id).show();
        jQuery('.widg-youtube' + id).hide();
        jQuery('.widg-video' + id).hide();

    } else if (value == "youtube") {
        jQuery('.widg-img' + id).hide();
        jQuery('.widg-youtube' + id).show();
        jQuery('.widg-video' + id).hide();
    } else if (value == "video") {
        jQuery('.widg-img' + id).hide();
        jQuery('.widg-youtube' + id).hide();
        jQuery('.widg-video' + id).show();
    }

}

function delete_row(cnt) {
    jQuery.each(jQuery(".add_new_rowxx-input-containers #entries_agilysys_customer_success_stories_widget"), function() {
        jQuery(' #entry' + cnt).remove();
    });
    var last_cnt = 0;

    jQuery.each(jQuery("#entries_agilysys_customer_success_stories_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            last_cnt = jQuery(this).val();
        }
    });

    last_cnt--;
    jQuery.each(jQuery("#entries_agilysys_customer_success_stories_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(last_cnt);
        }
    });
    jQuery(".what_makes_rows").val(last_cnt);
    jQuery('.what_makes_rows').trigger('change');

}
</script>
<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}


#rew_container_agilysys_customer_success_stories_widget select {
    float: left;
    width: 60%;
    margin-top: 20px !important;
    margin-bottom: 10px !important;
}

#rew_container_agilysys_customer_success_stories_widget input,

textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_customer_success_stories_widget label {
    width: 40%;
    float: left;
}

#rew_container_agilysys_customer_success_stories_widget p {
    padding: 20px;
}

<?php echo '.'. $widget_add_id_hosipitality_software;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}

#entries_agilysys_customer_success_stories_widget #remove-img-bg {
    background: url('<?php echo ZWREW_URL; ?>assets/images/deleteimg.png') center center no-repeat;
    width: 20px;
    height: 22px;
    display: none;
}

#entries_agilysys_customer_success_stories_widget {
    padding: 10px 0 0;
}

#entries_agilysys_customer_success_stories_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_customer_success_stories_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_customer_success_stories_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_customer_success_stories_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_customer_success_stories_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_customer_success_stories_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_customer_success_stories_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_customer_success_stories_widget #entries_agilysys_customer_success_stories_widget plast label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_customer_success_stories_widget">
    <?php echo $rew_html; ?>
</div>
<?php
} //Function form ends here
} // class ends here
<?php
use PHPMailer\PHPMailer\PHPMailer;

require_once ABSPATH . WPINC . '/PHPMailer/PHPMailer.php';
require_once ABSPATH . WPINC . '/PHPMailer/Exception.php';
function load_agilysys_lms_form_widget()
{
    register_widget('agilysys_lms_form_widget');
}

add_action('widgets_init', 'load_agilysys_lms_form_widget');

class agilysys_lms_form_widget extends WP_Widget
{

    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys LMS FORM Widget', 'AGILYSYS_TEXT_DOMAIN'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {
 echo $args['before_widget'];
        ?>
           

<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js">
</script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/additional-methods.js">
</script>

<style id="compiled-css" type="text/css">


.error {
    color: red;
}

#myform {
    margin: 10px;
}

#myform>div {
    margin-top: 10px;
}

/* EOS */
</style>
<?php

        $div_color = $instance['div_color'];
        $form_title = $instance['form_title'];
        $section_title = $instance['section_title'];
        $description = $instance['description'];

        if (isset($_POST['lms_submit'])) {

            $email = $_POST['email'];
            $firstname = $_POST['firstname'];
            $lastname = $_POST['lastname'];
            $company = $_POST['company'];

            $jobtitle = $_POST['jobtitle'];
            $phone = $_POST['phone'];
            $email = $_POST['email'];

            $country = $_POST['country'];
            $address = $_POST['address'];
            $city = $_POST['city'];

            $reason1 = implode(",", $_POST['reason1']);

            $lms_version = $_POST['lms_version'];

            $reason2 = implode(",", $_POST['reason2']);
            $otaName = $_POST['otaName'];

            $versionSWS = $_POST['versionSWS'];
            $sws = implode(",", $_POST['sws']);

            $versionDMS = $_POST['versionDMS'];

            $dms = implode(",", $_POST['dms']);

            $lms_additional = $_POST['lms_additional'];

            $message = '';
            $message .= '<p>Email: ' . $email . '</p>';
            $message .= '<p>Name: ' . $firstname . ' ' . $lastname . '</p>';
            $message .= '<p>Company: ' . $company . '</p>';
            $message .= '<p>jobtitle: ' . $jobtitle . '</p>';
            $message .= '<p>email: ' . $email . '</p>';
            $message .= '<p>country: ' . $country . '</p>';
            $message .= '<p>address: ' . $address . '</p>';
            $message .= '<p>city: ' . $city . '</p>';

            $message .= '<p>Reason for LPU Request: ' . $reason1 . '</p>';
            $message .= '<p>Lms Version: ' . $lms_version . '</p>';
            $message .= '<p>Reason for LPU Request 2: ' . $reason2 . '</p>';
            $message .= '<p>otaName: ' . $otaName . '</p>';

            $message .= '<p>versionSWS: ' . $versionSWS . '</p>';
            $message .= '<p>SWS: ' . $sws . '</p>';
            $message .= '<p>versionDMS: ' . $versionDMS . '</p>';
            $message .= '<p>dms: ' . $dms . '</p>';
            $message .= '<p>lms_additional: ' . $lms_additional . '</p>';

            $subject = "New Enquiry";

            $mail = new PHPMailer();

//$mail->isSMTP();

            $mail->SMTPDebug = false;

            $mail->Host = 'smtp.gmail.com';

            $mail->SMTPAuth = true;

            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = 587;

            $mail->Username = 'sau3334@gmail.com';

            $mail->Password = 'rwwtpnhahufyxqih';

            $mail->setFrom('testing@abiteoffrance.com', 'Testing');
            $mail->addReplyTo('sau3334@gmail.com', 'Saurav Daga');

            $mail->addAddress('vandhiyadevan.jayasooriyan@agilysys.com', 'Deva');

            $mail->Subject = 'LMS Form';

            $mail->Body = $message;

            $mail->AltBody = 'LMS Form';

            if (!$mail->send()) {
                echo 'Mailer Error: ' . $mail->ErrorInfo;
            } else {
              ?>
              <script type="text/javascript">
    jQuery(window).on('load', function() {
        jQuery('#myModal').modal('show');
    });
</script>
              
              <?php
            }

        }

        ?>



<section class="lmsContent center whiteText " style="background:<?php echo $div_color; ?>">
    <h2 class="dinProStd"><?php echo $section_title; ?></h2>
    <p><?php echo $description; ?></p>
    <h4 class="dinproMed"> <?php echo $form_title; ?></h4>
</section>

<section class="lmsForm">
    <form id="myform" action="" method="post">
        <div class="lmsFormLabel flex">
            <div class="lmsLabelBox flex">
                <label for="First Name" class="dinProStd blackText">* First Name:</label>
                <input type="text" id="lms_firstName" name="firstname" />
            </div>
            <div class="lmsLabelBox flex">
                <label for="Last Name" class="dinProStd blackText">* Last Name:</label>
                <input type="text" id="lms_lastName" name="lastname" />
            </div>
        </div>
        <div class="lmsFormLabel flex">
            <div class="lmsLabelBox flex">
                <label for="Company" class="dinProStd blackText">* Company:</label>
                <input type="text" id="lms_company" name="company" />
            </div>
            <div class="lmsLabelBox flex">
                <label for="Job Title" class="dinProStd blackText">* Job Title:</label>
                <input type="text" id="lms_jobtitle" name="jobtitle" />
            </div>
        </div>
        <div class="lmsFormLabel flex">
            <div class="lmsLabelBox flex">
                <label for="Phone" class="dinProStd blackText">* Phone:</label>
                <input class="number-only" type="text" id="lms_phone" name="phone" />
            </div>
            <div class="lmsLabelBox flex">
                <label for="Email" class="dinProStd blackText">* Email:</label>
                <input type="text" id="lms_email" name="email" />
            </div>
        </div>
        <div class="lmsFormLabel flex">
            <div class="lmsLabelBox flex">
                <label for="Country" class="dinProStd blackText">* Country:</label>
                <input type="text" id="lms_country" name="country" />
            </div>
            <div class="lmsLabelBox flex">
                <label for="Address" class="dinProStd blackText">* Address:</label>
                <input type="text" id="lms_address" name="address" />
            </div>
        </div>
        <div class="lmsFormLabel flex">
            <div class="lmsLabelBox flex">
                <label for="City" class="dinProStd blackText">* City:</label>
                <input type="text" id="lms_city" name="city" />
            </div>
        </div>
        <div class="lmsReson">
            <h2 class="dinProStd greenText">Reason for LPU Request:</h2>
            <div class="flex">
                <input type="checkbox" class="customCheckBox require-one" name="reason1[]" value="You have been notified that your system challenges called into support have been
                    addressed">
                <label for="test1">You have been notified that your system challenges called into support have been
                    addressed</label>
            </div>
            <div class="flex">
                <input type="checkbox" class="customCheckBox require-one" name="reason1[]"
                    value="You want to implement a newly added enhancement">
                <label for="test2">You want to implement a newly added enhancement</label>
            </div>
            <div class="flex">
                <input type="checkbox" class="customCheckBox require-one" name="reason1[]"
                    value="You want to be on the most current release of the software">
                <label for="test3">You want to be on the most current release of the software</label>
            </div>
            <div class="flex">
                <input type="checkbox" class="customCheckBox require-one" name="reason1[]"
                    value="You are on a quarterly schedule with Agilysys">
                <label for="test4">You are on a quarterly schedule with Agilysys</label>
            </div>
            <div class="flex">
                <input type="checkbox" class="customCheckBox require-one" name="reason1[]" value="EMV">
                <label for="test5">EMV</label>
            </div>
            <div class="flex">
                <input type="checkbox" class="customCheckBox require-one" name="reason1[]" value="Other">
                <label for="test6">Other</label>
            </div>

            <label id="reason1_error"></label>

        </div>

        <div class="lmsFormLabel flex">
            <div class="lmsLabelBox flex">
                <label for="version" class="dinProStd blackText"> Current Version of LMS</label>
                <input type="text" id="lms_version" name="lms_version" />
            </div>
        </div>


        <div class="lmsReson">
            <h2 class="dinProStd greenText">Reason for LPU Request:</h2>
            <div class="flex">
                <div class="checkBox">
                <input type="checkbox" class="customCheckBox require-one" name="reason2[]" value="AR">
                <label for="test1">AR</label>
                </div>

                  <div class="checkBox">
                <input type="checkbox" class="customCheckBox require-one" name="reason2[]"
                    value="IMM">
                <label for="test2">IMM</label>
                </div>
            </div>
         
            <div class="flex">
                <div class="checkBox">
                <input type="checkbox" class="customCheckBox require-one" name="reason2[]"
                    value="ARTS">
                <label for="test3">ARTS</label>
                 </div>
           
             <div class="checkBox">
                <input type="checkbox" class="customCheckBox require-one" name="reason2[]"
                    value="RESNET">
                <label for="test4">RESNET</label>
                </div>
            </div>
            <div class="flex">
                 <div class="checkBox">
                <input type="checkbox" class="customCheckBox require-one" name="reason2[]" value="RESHUB">
                <label for="test5">RESHUB</label>
                </div>
          
              <div class="checkBox">
                <input type="checkbox" class="customCheckBox require-one" name="reason2[]" value="KIOSK">
                <label for="test6">KIOSK</label>
                </div>
            </div>

            <div class="flex">
                  <div class="checkBox">
                <input type="checkbox" class="customCheckBox require-one" name="reason2[]" value="ROVING AGENT">
                <label for="test5">ROVING AGENT</label>
                 </div>
          
             <div class="checkBox">
                <input type="checkbox" class="customCheckBox require-one" name="reason2[]" value="SHOWGATE">
                <label for="test6">SHOWGATE</label>
                </div>
            </div>


            <div class="flex">
                <div class="checkBox">
                <input type="checkbox" class="customCheckBox require-one" name="reason2[]" value="OTA">
                <label for="test5">OTA</label>
                </div>
           
              <div class="checkBox">
                <input type="checkbox" class="customCheckBox require-one" name="reason2[]" value="SHOWNET">
                <label for="test6">SHOWNET</label>
                   </div>
            </div>


            <label id="reason2_error"></label>

        </div>

     

        <div class="lmsFormLabel flex">
            <div class="lmsLabelBox flex">
                <label for="otaName" class="dinProStd blackText"> OTA Name(s) - if applicable</label>
                <input type="text" id="lms_otaName" name="otaName" />
            </div>
        </div>

        <div class="lmsFormLabel flex">
            <div class="lmsLabelBox flex">
                <label for="versionSWS" class="dinProStd blackText"> Current Version of SWS</label>
                <input type="text" id="lms_versionSWS" name="versionSWS" />
            </div>
        </div>

        <div class="lmsSWSRequest lmsReson">
            <h2 class="dinProStd greenText">SWS Module(s) Requested</h2>
            <div class="flex">
                <div class="checkBox">
                <input type="checkbox" class="customCheckBox" id="inventory" name="sws[]" value="INVENTORY">
                <label for="inventory">INVENTORY</label>
                </div>
         
                <div class="checkBox">
                <input type="checkbox" class="customCheckBox" id="purchasing" name="sws[]" value="PURCHASING">
                <label for="purchasing">PURCHASING</label>
                </div>
                
            </div>
            <div class="flex">
                 <div class="checkBox">
                <input type="checkbox" class="customCheckBox" id="retail" name="sws[]" value="RETAIL">
                <label for="retail">RETAIL</label>
                </div> 
                
                 <div class="checkBox">
                <input type="checkbox" class="customCheckBox" id="swsBarCoding" name="sws[]" value="BAR CODING">
                <label for="swsBarCoding">BAR CODING</label>
                </div>
            </div>
            <div class="flex">
                 <div class="checkBox">
                <input type="checkbox" class="customCheckBox" id="menuRecipe" name="sws[]" value="MENU/RECIPE ANALYSIS">
                <label for="menuRecipe">MENU/RECIPE ANALYSIS</label>
                </div>
           
            <div class="checkBox">
                <input type="checkbox" class="customCheckBox" id="nutritional" name="sws[]"
                    value="NUTRITIONAL ANALYSIS">
                <label for="nutritional">NUTRITIONAL ANALYSIS</label>
                </div>
            
            <div class="checkBox">
                <input type="checkbox" class="customCheckBox" id="swsDirect" name="sws[]" value="SWS DIRECT">
                <label for="swsDirect">SWS DIRECT</label>
                </div>
            </div>

            
            <label id="sws_error"></label>
        </div>

        <div class="lmsFormLabel flex">
            <div class="lmsLabelBox flex">
                <label for="versionDMS" class="dinProStd blackText"> Current Version of DMS</label>
                <input type="text" id="lms_versionDMS" name="versionDMS" />
            </div>
        </div>

       <div class="lmsDMSRequest">
            <h2 class="dinProStd greenText">DMS Module(s) Requested</h2>
            
            <div class="lmsCheckBox flex">                        
              <input type="checkbox" class="customCheckBox" id="imaging" name="" value="">
              <label for="imaging">IMAGING</label>
            </div>
            <div class="lmsCheckBox flex">                        
              <input type="checkbox" class="customCheckBox" id="singnatureCap" name="" value="">
              <label for="singnatureCap">SIGNATURE CAPTURE</label>
            </div>
            <div class="lmsCheckBox flex">                        
              <input type="checkbox" class="customCheckBox" id="pcArchive" name="" value="">
              <label for="pcArchive">PC ARCHIVE</label>
            </div>
            <div class="lmsCheckBox flex">                        
              <input type="checkbox" class="customCheckBox" id="docFlow" name="" value="">
              <label for="docFlow">DOCUMENT FLOW</label>
            </div>
            <div class="lmsCheckBox flex">                        
              <input type="checkbox" class="customCheckBox" id="cdBuring" name="" value="">
              <label for="cdBuring">CD BURNING</label>
            </div>
            <div class="lmsCheckBox flex">                        
              <input type="checkbox" class="customCheckBox" id="dmsBarCoding" name="" value="">
              <label for="dmsBarCoding">BAR CODING</label>
            </div>
            <div class="lmsCheckBox flex">                        
              <input type="checkbox" class="customCheckBox" id="web" name="" value="">
              <label for="web">WEB</label>
            </div>
        </div>

        <div class="lmsFormLabel flex">
            <div class="lmsLabelBox flex">
                <label for="additional" class="dinProStd blackText"> Additional Information</label>
                <input type="text" id="lms_additional" name="lms_additional" />
            </div>
        </div>

        <div class="lmsFormLabel">
            <input type="submit" id="lms_submit" name="lms_submit" value="Submit Request" />
        </div>
    </form>
</section>

<section class="lmsBG orangeBG"></section>

<script>
jQuery(document).ready(function($) {

$.validator.addMethod("lettersonly", function(value, element) 
{
return this.optional(element) || /^[a-z," "]+$/i.test(value);
}, "Letters and spaces only please");   




    $("#myform").validate({
          onfocusout: false,
        rules: {

            firstname: {
               required:true,
               lettersonly: true,        
           },
            lastname: {
               required:true,
               lettersonly: true,        
           },
            company: {
               required:true,
               lettersonly: true,        
           },
            jobtitle: {
               required:true,
               lettersonly: true,        
           },
            phone: {
               
               required:true,
               number: true,
           },
            email: {
                required: true,
                email: true
            },
            country: {
               required:true,
               lettersonly: true,        
           },
            address: "required",
            city: {
               required:true,
               lettersonly: true,        
           },
            'reason1[]': {
                required: true
            },
            lms_version: "required",
            'reason2[]': {
                required: true,
            },
            otaName: "required",
            versionSWS: "required",
            'sws[]': {
                required: true,
            },
            versionDMS: "required",
            'dms[]': {

                required: true,
            },
            lms_additional: "required"
        },
        errorPlacement: function(error, element) {

            if (element.attr("name") == "reason1[]") {
                error.appendTo($('#reason1_error'));
            } else if (element.attr("name") == "reason2[]") {
                error.appendTo($('#reason2_error'));
            } 
            else if (element.attr("name") == "sws[]") {
                error.appendTo($('#sws_error'));
            } 
            else if (element.attr("name") == "dms[]") {
                error.appendTo($('#dms_error'));
            }
           else {
                error.insertAfter(element);
            }
        },

        messages: {
            firstname: {
               required:"Please enter your firstname",
               lettersonly: "Please enter Letters Only",       
           },
            
            
            
            lastname: {
               required:"Please enter your Lastname",
               lettersonly: "Please enter Letters Only",       
           },
            company: {
               required:"Please enter your Company Name",
               lettersonly: "Please enter Letters Only",       
           },
            jobtitle: {
               required:"Please enter your Job Title",
               lettersonly: "Please enter Letters Only",       
           },
            phone: {
               
               required:"Please enter your Phone Number",
               number: "Please enter Numbers Only",
           },
            email: "Please enter a valid Email Address",
            country: {
               required:"Please enter your Country Name",
               lettersonly: "Please enter Letters Only",       
           },
            address: "Please enter your Address",
            city: {
               required:"Please enter your City Name",
               lettersonly: "Please enter Letters Only",       
           },
            'reason1[]': {
                required: "Please enter Reason for LPU Request",
            },
            lms_version: "Please enter Current Version of LMS",
            'reason2[]': {
                required: "Please enter Reason for LPU Request",
            },
            otaName: "Please enter OTA Name(s)",
            versionSWS: "Please enter Current Version of SWS",
            'sws[]': {
                required: "Please Enter SWS Module(s) Requested",
            },
            versionDMS: "Please enter Current Version of DMS",
            'dms[]': {

                required: "Please Enter DMS Module(s) Requested",
            },
            lms_additional: "Please Enter Additional Information"

        },


        submitHandler: function(form) {




            form.submit();



        }
    });
});
</script>



<?php
echo $args['after_widget'];
    }

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['form_title'] = strip_tags($new_instance['form_title']);
        $instance['section_title'] = strip_tags($new_instance['section_title']);
        $instance['div_color'] = strip_tags($new_instance['div_color']);

        $instance['description'] = $new_instance['description'];

        return $instance;
    }

    public function form($display_instance)
    {
        ?>
<script type="text/javascript">
jQuery(document).ready(function($) {
    jQuery('.color-picker').on('focus', function() {
        var parent = jQuery(this).parent();
        jQuery(this).wpColorPicker()
        parent.find('.wp-color-result').click();
    });
});
</script>


<?php

        $section_title = $display_instance['section_title'];
        $form_title = $display_instance['form_title'];

        $description = $display_instance['description'];

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('section_title') . '"> ' . __('Section Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('section_title') . '" name="' . $this->get_field_name('section_title') . '" type="text" value="' . $section_title . '" />';
        $rew_html .= '</p><br><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('description') . '"> ' . __('Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<textarea rows="6" cols="35" id="' . $this->get_field_id('description') . '" name="' . $this->get_field_name('description') . '" >' . $description . '</textarea>';
        $rew_html .= '</p><br><br><br><br><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('div_color') . '">' . __('Div Color', 'AGILYSYS_TEXT_DOMAIN') . '</label>';
        $rew_html .= '<input class="widefat color-picker" id="' . $this->get_field_id('div_color') . '" name="' . $this->get_field_name('div_color') . '" type="text" value="' . esc_attr($display_instance['div_color']) . '" />';
        $rew_html .= '</p><br><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('form_title') . '"> ' . __('Form Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('form_title') . '" name="' . $this->get_field_name('form_title') . '" type="text" value="' . $form_title . '" />';
        $rew_html .= '</p><br><br>';

        ?>


<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container input,
select,
textarea {
    float: right;
    width: 60%;
}

#rew_container label {
    width: 40%;
}

<?php echo '.'. $widget_add_id_slider;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}



#entries {
    padding: 10px 0 0;
}

#entries .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries .entrys:first-child {
    margin: 0;
}

#entries .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries .entry-title.active:after {
    content: '\f142';
}

#entries .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container #entries p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container">
    <?php echo $rew_html; ?>
</div>

<?php
}

}
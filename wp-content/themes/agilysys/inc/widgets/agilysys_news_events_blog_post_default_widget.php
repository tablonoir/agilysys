<?php

add_action('widgets_init', 'agil_load_agilysys_news_events_blog_post_default_widget');

function agil_load_agilysys_news_events_blog_post_default_widget()
{
    register_widget('agilysys_news_events_blog_post_default_widget');
}

class agilysys_news_events_blog_post_default_widget extends WP_Widget
{

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys News and Events Blog Post Default Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');

        wp_enqueue_script('jquery-ui-datepicker');
        wp_register_style('jquery-ui', 'https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css');
        wp_enqueue_style('jquery-ui');

    }

    public function widget($args, $instance)
    {

        $section_title = $instance['section_title'];
        $per_page = $instance['per_page'];
        $category = $instance['category'];
 //echo $args['before_widget'];
        ?>

<style>
.cvf_pag_loading {
    padding: 20px;
}

.cvf-universal-pagination ul {
    margin: 0;
    padding: 0;
}

.cvf-universal-pagination ul li {
    display: inline;
    margin: 3px;
    padding: 4px 8px;
    background: #FFF;
    color: black;
}

.cvf-universal-pagination ul li.active:hover {
    cursor: pointer;
    background: #1E8CBE;
    color: white;
}

.cvf-universal-pagination ul li.inactive {
    background: #7E7E7E;
}

.cvf-universal-pagination ul li.selected {
    background: #1E8CBE;
    color: white;
}

#pagination {
    float: right;
    margin-right: 20%;
}


.loader {
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3498db;
    width: 120px;
    height: 120px;
    -webkit-animation: spin 2s linear infinite;
    /* Safari */
    animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
    0% {
        -webkit-transform: rotate(0deg);
    }

    100% {
        -webkit-transform: rotate(360deg);
    }
}

@keyframes spin {
    0% {
        transform: rotate(0deg);
    }

    100% {
        transform: rotate(360deg);
    }
}
</style>

<style>

 
  .loader_agilysys_news_events_blog_post_default_widget {
    width: 80px;
    height: 80px;
    background: #fff;
    border: 2px solid #f3f3f3;
    border-top: 3px solid #008000;
    border-radius: 100%;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 45%;
    margin: auto;
    animation: spin 1s infinite;
    display:none;

}

@keyframes spin {
    from {
        transform: rotate(0deg);
    }

    to {
        transform: rotate(360deg);
    }
}

</style>

 <div id="overlay_agilysys_news_events_blog_post_default_widget">
        <div class="loader_agilysys_news_events_blog_post_default_widget"></div>
    </div>

<section class="newsSection flex">
    <div class="newsLatest">
        <div class="newsLatestTitle flex">
            <h2 class="dinProStd greenText"><?php echo $section_title;?></h2>
            <a href="#" class="aboutButton violetText center">View All <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
        </div>
        <div class="videoFilter flex">
            <div class="videoFilterBox">
                <?php

        $args = array(
            "hide_empty" => 0,
            "type" => "post",
            'child_of' => $category,
            "orderby" => "name",
            "order" => "ASC",
        );
        $child_cats = get_categories($args);

        ?>
                <select class="filterOne" id="sub_cat" name="sub_cat" onChange="cvf_load_all_posts(1);">
                    <option value="<?php echo $category; ?>">Select a category</option>
                    <?php

        foreach ($child_cats as $child_cat) {
            $childID = $child_cat->cat_ID;

            ?>
                    <option value="<?php echo $childID; ?>"><?php echo $child_cat->name; ?></option>
                    <?php
}

        ?>
                </select>
            </div>
            <div class="videoFilterBox">
                <select class="filterTwo" name="sort_by" id="sort_by" onChange="cvf_load_all_posts(1);">
                    <option value="">Sort By</option>
                    <option value="post_date">Date</option>

                </select>
            </div>
            <div class="videoFilterBox">
                <select class="filterThree" name="sort_order" id="sort_order" onChange="cvf_load_all_posts(1);">
                    <option value="">Sort Order</option>
                    <option value="asc">ASCENDING</option>
                    <option value="desc">DESCENDING</option>

                </select>
            </div>
        </div>
        <div class="newsBoxSection" id="articleList">




        </div>
    </div>
</section>
<div id="pagination" class="newsEvntsPagination">


</div>

<script>
function navigate_to_page(slug) {
   
   document.location.href = slug+'/';
}
</script>

<script type="text/javascript">
jQuery(document).ready(function($) {
    // This is required for AJAX to work on our page



    // Load page 1 as the default
    cvf_load_all_posts(1);




    jQuery(document).on('click', '#pagination .cvf-universal-pagination li.active', function() {

        var page = jQuery(this).attr('p');
        cvf_load_all_posts(page);
    });
});


function cvf_load_all_posts(page) {
     jQuery(".loader_agilysys_news_events_blog_post_default_widget").fadeIn("slow");
    var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

    var sort_by = jQuery('#sort_by').val();
    var sort_order = jQuery('#sort_order').val();

    // Data to receive from our server
    // the value in 'action' is the key that will be identified by the 'wp_ajax_' hook
    var data = {
        page: page,
        sort_order: sort_order,
        sort_by: sort_by,
        action: "demo-pagination-load-posts-agilysys-news-events-blog-post-default-widget",
        per_page: '<?php echo $per_page; ?>',
        sub_cat: jQuery('#sub_cat').val(),

    };
    

    // Send the data
    jQuery.post(ajaxurl, data, function(response) {


        var dataxx = JSON.parse(response);

        jQuery("#articleList").html(dataxx.msg);

        jQuery("#pagination").html(dataxx.pag_container);


         jQuery(".loader_agilysys_news_events_blog_post_default_widget").fadeOut("slow");

    });
}
</script>

<?php
//echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['section_title'] = strip_tags($new_instance['section_title']);
        $instance['per_page'] = strip_tags($new_instance['per_page']);
        $instance['category'] = strip_tags($new_instance['category']);

        return $instance;

    }

    public function form($display_instance)
    {

        $section_title = ($display_instance['section_title']);
        $per_page = ($display_instance['per_page']);
        $selected_category = $display_instance['category'];

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('section_title') . '"> ' . __('Section Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('section_title') . '" name="' . $this->get_field_name('section_title') . '" type="text" value="' . $section_title . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('per_page') . '"> ' . __('Per page', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input class="what_makes_rows" id="' . $this->get_field_name('per_page') . '" name="' . $this->get_field_name('per_page') . '" type="number" value="' . $per_page . '" />';
        $rew_html .= '</p>';

        $args = array(
            "hide_empty" => 0,
            "type" => "post",
            "orderby" => "name",
            "order" => "ASC",
        );
        $categories = get_categories($args);

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('category') . '"> ' . __('Select Blog Category', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<select id="' . $this->get_field_id('category') . '" name="' . $this->get_field_name('category') . '">';
        $rew_html .= '<option value="">Please Select</option>';

        foreach ($categories as $key) {
            if ($key->name != 'Uncategorized') {

                if ($key->cat_ID == $selected_category) {
                    $rew_html .= '<option value="' . $key->cat_ID . '" selected="selected">' . $key->name . '</option>';
                } else {
                    $rew_html .= '<option value="' . $key->cat_ID . '">' . $key->name . '</option>';
                }
            }

        }

        $rew_html .= '</select>';
        $rew_html .= '</p><br><br><br><br>';

        ?>


<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_news_events_blog_post_default_widget select {
    float: left;
    width: 60%;
    margin-top: 20px !important;
    margin-bottom: 10px !important;
}

#rew_container_agilysys_news_events_blog_post_default_widget input,

textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_news_events_blog_post_default_widget label {
    width: 40%;
    float: left;
}

#rew_container_agilysys_news_events_blog_post_default_widget p {
   padding:10px;
}


<?php echo '.' . $widget_add_id_slider;

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}



#entries {
    padding: 10px 0 0;
}

#entries .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries .entrys:first-child {
    margin: 0;
}

#entries .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries .entry-title.active:after {
    content: '\f142';
}

#entries .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_news_events_blog_post_default_widget #entries p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_news_events_blog_post_default_widget">
    <?php echo $rew_html; ?>
</div>

<?php
}

}
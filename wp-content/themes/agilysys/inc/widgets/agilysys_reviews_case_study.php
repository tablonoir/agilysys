<?php

function agilysys_review_casestduy_Widget()
{
    register_widget('agilysys_review_Widget');
}

add_action('widgets_init', 'agilysys_review_casestduy_Widget');

class agilysys_review_Widget extends WP_Widget
{
    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys reviews and casestudies Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
        add_action('load-widgets.php', array(&$this, 'agilysys_color_picker_load'));

    }

    public function agilysys_color_picker_load()
    {
        wp_enqueue_style('wp-color-picker');
        wp_enqueue_script('wp-color-picker');
    }
    /**
     * @see WP_Widget::widget -- do not rename this         * This is for front end
     */
    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        extract($args);
        $max_entries_agilysys_review_Widget_review = 25;
        $bg_color = !empty($instance['background_color']) ? $instance['background_color'] : '';

//                var_dump($max_entries_agilysys_review_Widget_review);
        ?>

<section class="homereviewCaseStudy">



    <div class="homeReviewCaseStudies lightBlackBG" style="background:<?php echo $bg_color; ?>" data-aos="fade-right"
        data-aos-delay="300" data-aos-duration="400" data-aos-once="true">
        <h2 class="reviewsCaseHeader dinProStd whiteText"><?php echo $instance['review_title']; ?></h2>

        <p class="whiteText dinproMed"><?php echo substr($instance['content'],0,315); ?></p>
    </div>



    <div class="homeReviewCaseBG" data-aos="fade-left" data-aos-delay="300" data-aos-duration="400"
        data-aos-once="true">
        <div class="homeReviewCaseBGImg">
            <div class="swiper-container">

                <div class="swiper-wrapper">
                    <?php

        $count = count($instance['star_rating']);
        for ($i = 0; $i < $count; $i++) {

            $image_url_bg_image = esc_url($instance['image_url_bg_image'][$i]);

            ?>
                    <div class="swiper-slide" style="background: url( <?php echo $image_url_bg_image; ?> ); background-repeat:no-repeat;background-position: center;
    background-size: cover;
    background-repeat: no-repeat;">
                    </div>
                    <?php

        }
        ?>
                </div>
            </div>

        </div>
        <div class="homeReviewTestimonial whiteBG">
            <div class="swiper-container" data-aos="fade-left" data-aos-delay="300" data-aos-duration="400"
                data-aos-once="true">
                <div class="swiper-wrapper">
                    <?php

        $count = count($instance['star_rating']);
        for ($i = 0; $i < $count; $i++) {

            $star_rating = esc_attr($instance['star_rating'][$i]);
            $full_description = apply_filters('the_content', $instance['full_desc'][$i]);
            $author_name = $instance['endorser_name'][$i];

            $star_rating1 = $star_rating;
            $full_desc = $full_description;

            ?>

                    <div class="swiper-slide homeTestSlider col-sm-12 col-md-12">
                        <div class="homereviewRating flex">
                            <div class="homereviewTestimonialIcon" data-aos="fade-left" data-aos-delay="300"
                                data-aos-duration="400" data-aos-once="true">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/home/testimonials-icon.png"
                                    class="img-fluid" alt="testimonial">
                            </div>
                            <div class="homereviewRatingStars flex" data-aos="fade-left" data-aos-delay="300"
                                data-aos-duration="400" data-aos-once="true">
                                <?php
if ($star_rating1 == 1) {
            }

            if ($star_rating1 == 2) {
                echo ' <img src="' . get_template_directory_uri() . '/img/home/star.png" class="img-fluid" alt="star">';
            }

            if ($star_rating1 == 3) {
                echo ' <img src="' . get_template_directory_uri() . '/img/home/star.png" class="img-fluid" alt="star">';
                echo ' <img src="' . get_template_directory_uri() . '/img/home/star.png" class="img-fluid" alt="star">';
            }

            if ($star_rating1 == 4) {
                echo ' <img src="' . get_template_directory_uri() . '/img/home/star.png" class="img-fluid" alt="star">';
                echo ' <img src="' . get_template_directory_uri() . '/img/home/star.png" class="img-fluid" alt="star">';
                echo ' <img src="' . get_template_directory_uri() . '/img/home/star.png" class="img-fluid" alt="star">';
            }
            if ($star_rating1 == 5) {
                echo ' <img src="' . get_template_directory_uri() . '/img/home/star.png" class="img-fluid" alt="star">';
                echo ' <img src="' . get_template_directory_uri() . '/img/home/star.png" class="img-fluid" alt="star">';
                echo ' <img src="' . get_template_directory_uri() . '/img/home/star.png" class="img-fluid" alt="star">';
                echo ' <img src="' . get_template_directory_uri() . '/img/home/star.png" class="img-fluid" alt="star">';}
            ?>
                                <img src="<?php echo get_template_directory_uri(); ?>/img/home/star.png"
                                    class="img-fluid" alt="star">
                            </div>

                            <!--  ending of star rating -->
                        </div>

                        <!-- starting of testimonial content and author name-->
                        <div class="hometestimonialsContent" data-aos="fade-left" data-aos-delay="300"
                            data-aos-duration="400" data-aos-once="true">


                            <p><?php echo $full_desc; ?></p>

                            <p> -- <?php echo $author_name; ?></p>


                        </div>
                    </div>

                    <?php

        }

        ?>

                </div>
                <!-- Add Pagination and navigation arrows -->
                <!--<div class="swiper-pagination"></div>-->

                <div class="swiper-button-next">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/home/arrow-right.png" class="img-fluid"
                        alt="arrow-left">
                </div>

                <div class="swiper-button-prev">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/home/arrow-leftt.png" class="img-fluid"
                        alt="arrow-right">
                </div>

            </div>

        </div>

    </div>
</section>
<?php
echo $args['after_widget'];
    }

    //Function widget ends here

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $max_entries_agilysys_review_Widget_review = 25;
        $instance['background_color'] = $new_instance['background_color'];
        $instance['review_title'] = strip_tags($new_instance['review_title']);
        $instance['content'] = strip_tags($new_instance['content']);
        $instance['review_rows'] = strip_tags($new_instance['review_rows']);

        $count = count($new_instance['star_rating']);

        for ($i = 0; $i < $count; $i++) {

            $instance['star_rating'][$i] = strip_tags($new_instance['star_rating'][$i]);
            $instance['image_url_bg_image'][$i] = strip_tags($new_instance['image_url_bg_image'][$i]);

            $instance['full_desc'][$i] = strip_tags($new_instance['full_desc'][$i]);
            $instance['endorser_name'][$i] = strip_tags($new_instance['endorser_name'][$i]);

        }
        return $instance;
    }
    //Function update ends here

    /**
     * @see WP_Widget::form -- do not rename this
     */
    public function form($display_instance)
    {
        ?>
<script type='text/javascript'>
jQuery(document).ready(function($) {
    $('.my-color-picker').wpColorPicker();
});
</script>
<?php
//$widget_add_id_review = $this->id . "-add";
        $widget_add_id_review = $this->get_field_id('') . "add";
        $review_title = ($display_instance['review_title']);
        $contentDesc = ($display_instance['content']);

        $background_color = ($display_instance['background_color']);

        if (!empty($display_instance['review_rows'])) {
            $review_rows = ($display_instance['review_rows']);
        } else {
            $review_rows = 0;
        }

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('review_title') . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('review_title') . '" name="' . $this->get_field_name('review_title') . '" type="text" value="' . $review_title . '" />';
        $rew_html .= '</p><br>';
        $rew_html .= '<label for="' . $this->get_field_id('content') . '"> ' . __('Sub Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('content') . '" name="' . $this->get_field_name('content') . '" type="text" value="' . $contentDesc . '" />';
        $rew_html .= '</p><br>';
        $rew_html .= '<label for="' . $this->get_field_id('review_rows') . '"> ' . __('No. of rows do you want to add*', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input class="review_rows" id="review_rows" name="' . $this->get_field_name('review_rows') . '" type="number" value="' . $review_rows . '" />';
        $rew_html .= '</p><br>';
        $rew_html .= '<label for="' . $this->get_field_id('background_color') . '"> ' . __('Background Color', 'agilysys_text_domain') . ' :</label>';
        $rew_html .= '<input  class="my-color-picker" id="' . $this->get_field_id('background_color') . '" name="' . $this->get_field_name('background_color') . '" type="text" value="' . $background_color . '" />';
        $rew_html .= '</p><br>';

        $count = count($display_instance['star_rating']);

        $rew_html .= '<div class="add_new_rowxx-input-containers"><div id="entries_agilysys_review_Widget">';

        $rew_html .= '<input class="cnt909" id="cnt909" name="cnt" type="hidden" value="' . $count . '">';

        for ($i = 0; $i < $count; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'AGILYSYS_TEXT_DOMAIN') . ' </span>';

            $rew_html .= '<div class="entry-desc cf">';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('star_rating' . $i) . '"> ' . __('Rating', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('star_rating' . $i) . '" name="' . $this->get_field_name('star_rating[]') . '" type="text" value="' . $display_instance['star_rating'][$i] . '">';
            $rew_html .= '</p><br><br>';

            $rew_html .= '<br><br><div class="widg-img' . $i . '">';
            $show1 = (empty($display_instance['image_url_bg_image'][$i])) ? 'style="display:none;"' : '';
            $rew_html .= '<label id="image_uri_agilysys_review_Widget' . $i . '"></label><br><img class="' . $this->get_field_id('image_id' . $i) . '_media_image' . $i . ' custom_media_image' . $i . '" src="' . $display_instance['image_url_bg_image'][$i] . '" ' . $show1 . ' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id' . $i) . '_media_id' . $i . ' custom_media_id' . $i . '" name="' . $this->get_field_name('image_id[]') . '" id="' . $this->get_field_id('image_id' . $i) . '" value="' . $display_instance['image_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id' . $i) . '_media_url' . $i . ' custom_media_url' . $i . '" name="' . $this->get_field_name('image_url_bg_image[]') . '" id="' . $this->get_field_id('image_url_bg_image' . $i) . '" value="' . $display_instance['image_url_bg_image'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_upload' . $i . '" id="' . $this->get_field_id('image_id' . $i) . '"/>';

            $rew_html .= '</div><br><br>';

            ?>
            
            <style>
                .wp-picker-container{
                    margin-left:32%;
                }
                
            </style>

<script>
jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    if (attachment.height == 949 && attachment.width == 2000) {
                        jQuery('.' + button_id_s + '_media_id<?php echo $i; ?>').val(attachment.id);
                        jQuery('.' + button_id_s + '_media_url<?php echo $i; ?>').val(attachment
                            .url);
                        jQuery('.' + button_id_s + '_media_image<?php echo $i; ?>').attr('src',
                            attachment.url).css('display', 'block');
                        jQuery('#image_uri_agilysys_review_Widget<?php echo $i; ?>')
                            .html("");
                    } else {
                        jQuery('#image_uri_agilysys_review_Widget<?php echo $i; ?>')
                            .html("Please Enter the correct Dimensions 2000x949").css('color',
                                'red');

                    }
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload<?php echo $i; ?>');

});
</script>

<?php

            /**
             * Description
             */
            $short_desc = esc_attr($display_instance['short_desc'][$i]);
            $short_desc_link = esc_attr($display_instance['short_desc_link'][$i]);
            $full_desc = esc_attr($display_instance['full_desc'][$i]);
            $endorser_name = esc_attr($display_instance['endorser_name'][$i]);

            $rew_html .= '<p class="last desc">';
            $rew_html .= '<label for="' . $this->get_field_id('full_desc' . $i) . '"> ' . __('Full Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<textarea id="' . $this->get_field_id('full_desc' . $i) . '" name="' . $this->get_field_name('full_desc[]') . '">' . $full_desc . '</textarea>';
            $rew_html .= '</p>';

            /**
             * Read More Button
             */$rew_html .= '<p class="last">';
            $rew_html .= '<label for="' . $this->get_field_id('endorser_name' . $i) . '"> ' . __('Endorser Name', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('endorser_name' . $i) . '" name="' . $this->get_field_name('endorser_name[]') . '" type="text" value="' . $endorser_name . '" />';
            $rew_html .= '</p>';
            $k = $i + 1;
            $rew_html .= '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' . $k . ');">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';

        }
        $rew_html .= '</div></div>';
        $rew_html .= '<div class="' . $widget_add_id_review . '" style="margin-bottom: 36px;text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;" onClick="add_new_row();">' . __('ADD ROW', 'AGILYSYS_TEXT_DOMAIN') . '</div>';

        ?>
<script>
function add_new_row() {
    var cnt = '';

    jQuery.each(jQuery("#entries_agilysys_review_Widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            cnt = jQuery(this).val();
        }
    });




    var review_rows = jQuery('.review_rows').val();

console.log(cnt);

    if (parseInt(cnt) < parseInt(review_rows)) {

        cnt++;

        jQuery.each(jQuery("#entries_agilysys_review_Widget .cnt909"), function() {
            if (jQuery(this).val() != '') {
                jQuery(this).val(cnt);
            }
        });

        var new_row = '<div id="entry' + cnt +
            '"  class="entrys"><span class="entry-title" onclick = "slider(this);"><?php echo __('Add New Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span>';
        new_row += '<div class="entry-desc cf">';


        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Star Rating', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('star_rating[]')); ?>" type="text" value="">';
        new_row += '</p>';


        new_row += '<br><br><div class="widg-img' + cnt + '">';
        new_row += '<label id="image_uri_agilysys_review_Widget' + cnt +
            '"></label><br><img class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_image' + cnt +
            ' custom_media_image' + cnt + '" src="" style="display:none;" width=200" height="120"/>';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_id' +
            cnt + ' custom_media_id' + cnt + '" name="<?php echo $this->get_field_name('image_id[]'); ?>"  value="" />';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_url' +
            cnt + ' custom_media_url' + cnt +
            '" name="<?php echo $this->get_field_name('image_url_bg_image[]'); ?>" id="<?php echo $this->get_field_id('image_id'); ?>' +
            cnt + '" value="">';
        new_row += '<input type="button" value="Upload Image" class="button custom_media_upload' + cnt +
            '" id="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '"/>';
        new_row += '</div><br><br>';


        jQuery(document).ready(function() {




            function media_upload(button_class) {
                var _custom_media = true,
                    _orig_send_attachment = wp.media.editor.send.attachment;
                jQuery('body').on('click', '.custom_media_upload' + cnt, function(e) {
                    var button_id = '#' + jQuery(this).attr('id');
                    var button_id_s = jQuery(this).attr('id');
                    console.log(button_id);
                    var self = jQuery(button_id);
                    var send_attachment_bkp = wp.media.editor.send.attachment;
                    var button = jQuery(button_id);
                    var id = button.attr('id').replace('_button', '');
                    _custom_media = true;

                    wp.media.editor.send.attachment = function(props, attachment) {
                        if (_custom_media) {

                            if (attachment.height == 949 && attachment.width == 2000) {
                                jQuery('.' + button_id_s + '_media_id' + cnt).val(attachment.id);
                                jQuery('.' + button_id_s + '_media_url' + cnt).val(attachment.url);
                                jQuery('.' + button_id_s + '_media_image' + cnt).attr('src',
                                    attachment.url).css('display', 'block');

                                jQuery('#image_uri_agilysys_review_Widget' + cnt)
                                    .html("");
                            } else {
                                jQuery('#image_uri_agilysys_review_Widget' + cnt)
                                    .html("Please Enter the correct Dimensions 2000x949").css(
                                        'color', 'red');

                            }
                        } else {
                            return _orig_send_attachment.apply(button_id, [props, attachment]);
                        }
                    }
                    wp.media.editor.open(button);
                    return false;
                });
            }
            media_upload('.custom_media_upload' + cnt);

        });



        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Full Description', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';

        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('full_desc[]')); ?>" type="text" value="">';
        new_row += '</p>';

        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Endorser Name', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';

        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('endorser_name[]')); ?>" type="text" value="">';
        new_row += '</p>';





        var new_cnt = cnt;

        new_row += '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' + new_cnt +
            ');"><?php echo __('Delete Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span></a></p>';
        new_row += '</div></div>';

        jQuery('.add_new_rowxx-input-containers #entries_agilysys_review_Widget').append(new_row);

    }


}



function delete_row(cnt) {
    jQuery.each(jQuery(".add_new_rowxx-input-containers #entries_agilysys_review_Widget"), function() {
        jQuery(' #entry' + cnt).remove();
    });
    var last_cnt = 0;

    jQuery.each(jQuery("#entries_agilysys_review_Widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            last_cnt = jQuery(this).val();
        }
    });

    last_cnt--;
    jQuery.each(jQuery("#entries_agilysys_review_Widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(last_cnt);
        }
    });
    jQuery(".review_rows").val(last_cnt);
    jQuery('.review_rows').trigger('change');

}
</script>
<style>
#rew_container_agilysys_review_Widget .wp-picker-container {
    margin-left: 0%;
}


.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_review_Widget select {
    float: left;
    width: 60%;
    margin-top: 20px !important;
    margin-bottom: 10px !important;
}

#rew_container_agilysys_review_Widget input,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_review_Widget label {
    width: 40%;
    float: left;
}

#rew_container_agilysys_review_Widget p {
    padding:20px;
}

<?php echo '.'. $widget_add_id_review;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}

#entries_agilysys_review_Widget #remove-img-bg {
    background: url('<?php echo ZWREW_URL; ?>assets/images/deleteimg.png') center center no-repeat;
    width: 20px;
    height: 22px;
    display: none;
}

#entries_agilysys_review_Widget {
    padding: 10px 0 0;
}

#entries_agilysys_review_Widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_review_Widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_review_Widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_review_Widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_review_Widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_review_Widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_review_Widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_review_Widget #entries_agilysys_review_Widget plast label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_review_Widget">
    <?php echo $rew_html; ?>
</div>
<?php
} //Function form ends here
} // class ends here
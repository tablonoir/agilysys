<?php

function load_agilysys_blog_page_sidebar_blog_post_widget()
{
    register_widget('agilysys_blog_page_sidebar_blog_post_widget');
}

add_action('widgets_init', 'load_agilysys_blog_page_sidebar_blog_post_widget');

class agilysys_blog_page_sidebar_blog_post_widget extends WP_Widget
{

    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Blog Page Sidebar Blog Post Widget', 'AGILYSYS_TEXT_DOMAIN'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');

    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        $subcategory_id = $instance['subcategory_id'];
        $posts_drop = $instance['posts_drop'];

        $args = array(
            'post_type' => 'post',
            'post__in' => array($posts_drop),
        );

        $posts = get_posts($args);

        $image = wp_get_attachment_image_src(get_post_thumbnail_id($posts[0]->ID), 'single-post-thumbnail');

        $image[0] = preg_replace('/\s+/', '', $image[0]);

        $post_date = date("F d, Y", strtotime($posts[0]->post_date));

        $post_title = $posts[0]->post_title;

        $post_content = substr(strip_tags(html_entity_decode($posts[0]->post_content)), 0, 100);

        $postyy = get_post($posts[0]->ID);
        $slug = $postyy->post_name;

        $facebook_link = get_post_meta($posts[0]->ID, 'facebook_link');

        $twitter_link = get_post_meta($posts[0]->ID, 'twitter_link');
        $linkedin_link = get_post_meta($posts[0]->ID, 'linkedin_link');
        ?>


<div class="blogsAd flex">
    <div class="blogAdImg">
        <img class="img-fluid" src="<?php echo $image[0]; ?>" alt="" />
    </div>
    <div class="blogAdContent greenBG whiteText">
        <p><?php echo $post_date; ?></p>
        <a href="<?php echo home_url($slug)."/";?>"><h4 class="dinProStd"><?php echo $post_title; ?></h4></a>
        <p><?php echo $post_content; ?></p>
        <div class="blogSliderContentLink flex">
            <a href="<?php echo home_url($slug); ?>/" class="homeBannerButton whiteText"> Read More <i
                    class="fa fa-arrow-right" aria-hidden="true"></i></a>
            <div class="blogSliderContentsocial">
            <a href="<?php echo $facebook_link[0]; ?>"><i class="fa fa-facebook"
                                        aria-hidden="true"></i></a>
                                <a href="<?php echo $twitter_link[0]; ?>"><i class="fa fa-twitter"
                                        aria-hidden="true"></i></a>
                                <a href="<?php echo $linkedin_link[0]; ?>"><i class="fa fa-linkedin"
                                        aria-hidden="true"></i></a>
                                <i class="fa fa-print" aria-hidden="true"></i>
            </div>
        </div>
    </div>
</div>

<?php
echo $args['after_widget'];
    }

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {

        $instance = array();

        $instance['subcategory_id'] = strip_tags($new_instance['subcategory_id']);
        $instance['posts_drop'] = strip_tags($new_instance['posts_drop']);

        return $instance;
    }

    public function form($display_instance)
    {

        $widget_add_id_slider_client = $this->get_field_id('') . "add_agilysys_blog_page_sidebar_blog_post_widget";

        $category_id = get_cat_ID('blogpost');
        $args = array(
            "hide_empty" => 0,
            "type" => "post",
            "orderby" => "name",
            "order" => "ASC",
            "child_of" => $category_id,
        );
        $categories = get_categories($args);

        $rew_html .= '<p>';
        $rew_html .= '<select name="' . $this->get_field_name('subcategory_id') . '" id="' . $this->get_field_id('subcategory_id') . '" onChange="fetch_posts_by_subcat(this.value);">';

        $rew_html .= '<option value="">Please Select</option>';
        foreach ($categories as $key) {

            if ($display_instance['subcategory_id'] == $key->slug) {
                $rew_html .= '<option value="' . $key->slug . '" selected="selected">' . $key->name . '</option>';
            } else {
                $rew_html .= '<option value="' . $key->slug . '">' . $key->name . '</option>';
            }
        }
        $rew_html .= '</select>';
        $rew_html .= '</p>';

        if ($display_instance['subcategory_id'] == "") {
            $rew_html .= '<p>';
            $rew_html .= '<select class="posts_drop" name="' . $this->get_field_name('posts_drop') . '" id="' . $this->get_field_id('posts_drop') . '">';
            $rew_html .= '<option value="">Please Select</option>';

            $rew_html .= '</select>';
            $rew_html .= '</p>';
        } elseif ($display_instance['subcategory_id'] != "") {

            $args = array('category_name' => $display_instance['subcategory_id']);
            $posts = get_posts($args);

            $rew_html .= '<p>';
            $rew_html .= '<select class="posts_drop" name="' . $this->get_field_name('posts_drop') . '" id="' . $this->get_field_id('posts_drop') . '">';
            $rew_html .= '<option value="">Please Select</option>';
            foreach ($posts as $key) {

                if ($display_instance['posts_drop'] == $key->ID) {
                    $rew_html .= '<option value="' . $key->ID . '" selected="selected">' . $key->post_title . '</option>';
                } else {
                    $rew_html .= '<option value="' . $key->ID . '">' . $key->post_title . '</option>';

                }
            }
            $rew_html .= '</select>';
            $rew_html .= '</p>';

        }
        ?>

<script>
function fetch_posts_by_subcat(val) {
    var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

    var data = {

        action: "fetch-posts-by-subcat",
        subcat: val
    };
    jQuery('.posts_drop').html('');

    // Send the data
    jQuery.post(ajaxurl, data, function(response) {

        var dataxx = JSON.parse(response);

        jQuery('.posts_drop').append(dataxx.options);


    });

}
</script>


<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_blog_page_sidebar_blog_post_widget input,
select,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_blog_page_sidebar_blog_post_widget label {
    width: 40%;
}

<?php echo '.' . $widget_add_id_slider_client;

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}


#entries {
    padding: 10px 0 0;
}

#entries .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries .entrys:first-child {
    margin: 0;
}

#entries .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries .entry-title.active:after {
    content: '\f142';
}

#entries .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_blog_page_sidebar_blog_post_widget #entries p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_blog_page_sidebar_blog_post_widget">
    <?php echo $rew_html; ?>
</div>


<?php

    }

}
<?php

use PHPMailer\PHPMailer\PHPMailer;

require_once ABSPATH . WPINC . '/PHPMailer/PHPMailer.php';
require_once ABSPATH . WPINC . '/PHPMailer/Exception.php';

function load_agilysys_rma_request_widget()
{
    register_widget('agilysys_rma_request_widget');
}

add_action('widgets_init', 'load_agilysys_rma_request_widget');

class agilysys_rma_request_widget extends WP_Widget
{

    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Rma Request Widget', 'AGILYSYS_TEXT_DOMAIN'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {
 echo $args['before_widget'];
        $sub_title = $instance['sub_title'];
        $section_title = $instance['section_title'];
        $image_uri = $instance['image_uri'];
        $image_uri_alt = $instance['image_uri_alt'];
        
        $k = 0;

        ?>
       
        <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js">
        </script>
        <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/additional-methods.js">
        </script>
        
        <style id="compiled-css" type="text/css">
    
        
        .error {
            color: red;
        }
        
        #myform {
            margin: 10px;
        }
        
        #myform>div {
            margin-top: 10px;
        }
        
        /* EOS */
        </style>
        <?php

        if (isset($_POST['rma_Submit'])) {

            $email = $_POST['email'];

            $message = '';
            $message .= '<p>Email: ' . $email . '</p>';

            $mail = new PHPMailer();

//$mail->isSMTP();

            $mail->SMTPDebug = false;

            $mail->Host = 'smtp.gmail.com';

            $mail->SMTPAuth = true;

            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = 587;

            $mail->Username = 'sau3334@gmail.com';

            $mail->Password = 'rwwtpnhahufyxqih';

            $mail->setFrom('testing@abiteoffrance.com', 'Testing');
            $mail->addReplyTo('sau3334@gmail.com', 'Saurav Daga');

            $mail->addAddress('vandhiyadevan.jayasooriyan@agilysys.com', 'Deva');

            $mail->Subject = 'RMA Form';

            $mail->Body = $message;

            $mail->AltBody = 'RMA Form';

            if (!$mail->send()) {
                echo 'Mailer Error: ' . $mail->ErrorInfo;
            } else {
            ?>
            
            <script type="text/javascript">
    jQuery(window).on('load', function() {
        jQuery('#myModal').modal('show');
    });
</script>
            
            <?php
            }
            
            

        }

        ?>


<section class="rmaRequest center">
  
    <!--<h5 class="dinproBld blackText"><?php echo $sub_title; ?></h5>-->
    <!--<form class="rmaForm flex" id="myform1" method="post" action="">-->
    <!--    <input type="text" id="rma_Email" class="rma_Email" name="email" placeholder="Enter Email Address" />-->
    <!--    <input type="submit" id="rma_Submit" class="rma_Submit" name="rma_Submit" value="Submit" />-->
    <!--</form>-->
</section>




<section class="rmaRequest" id="frmRmaSection">
    
      <h2 class="dinProStd blackText center"><?php echo $section_title; ?></h2>
     
    <!--<button class="rma_Submit" onclick="document.location=document.location;">Start Over</button>-->
 <form method="post" class="" action="" id="frmRma" style="padding:80px;">

<div class="row rmsRequestRow">
                <div class="col-md-6 ">
                    <div class="form-group ">
                        <label for="requestDate" class="blackText dinProStd font18">REQUEST DATE</label>
                        <input name="requestDate" type="text" id="requestDate" class="form-control" placeholder="Request Date" disabled="disabled" value="<?php echo date("m/d/Y");?>" />
                    </div>
                    <div class="form-group">
                        <label for="email" class="blackText dinProStd font18">CONTACT EMAIL</label>
                        <input name="email" type="text" id="email" class="form-control" placeholder="Contact Email" required="required" value="<?php echo $email;?>" />
                        </div>
                    <div class="form-group">
                        <label for="opportunitynum" class="blackText dinProStd font18">OPPORTUNITY NUMBER</label>
                        <input name="opportunitynum" type="text" id="opportunitynum " class="form-control" autofocus="" placeholder="Opportunity Number" />
                    </div>
                </div>
             
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="requestorname" class="blackText dinProStd font18">REQUESTORS NAME</label>
                        <input name="requestorname" type="text" id="requestorname" class="form-control" placeholder="Requestors Name" onblur="contact.value = this.value;" />
                    </div>
                    <div class="form-group">
                        <label for="customer" class="blackText dinProStd font18">CUSTOMER NAME</label>
                        <input name="customer" type="text" id="customer" class="form-control" placeholder="Customer Name" />
                     </div>
                    <div class="form-group">
                        <label for="contact" class="blackText dinProStd font18">CONTACT NAME</label>
                        <input name="contact" type="text" id="contact" class="form-control" placeholder="Contact Name" />
                     </div>
                  </div>
            </div><!--row-->
            <div id="itemErrMsg" class="alert-danger">
            </div>
            <br><br>
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <span class="partheader dinProStd blackText font18"> ITEMS AND QUANTITIES REQUESTING RETURN APPROVAL</span>
                </div>
            </div><br><br>
            <div class="col-md-2">
                
                </div>
            <div class="col-md-8">
            <table class="table-responsive">
                <thead>
                    <tr>
                        <td class="blackText dinProStd font18">PART # </td>
                        <td class="blackText dinProStd font18">QUANTITY </td>
                        <td class="blackText dinProStd font18">Serial Numbers Separated by Commas (serial1,serial2,serial3...) </td>
                        <td class="blackText dinProStd font18">REASON FOR RETURN </td>
                        <td class="blackText dinProStd font18"></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <input name="part_number" type="text" id="part_number" class="form-control " />
                        </td>
                        <td>
                            <input name="qty" type="text" id="qty" class="form-control1 " size="2" />
                        </td>
                        <td>
                            <input name="serial_number" type="text" id="serial_number" class="form-control " />
                        </td>
                        <td>
                            <select class="form-control" name="dropReasons" id="dropReasons" style="height:35px;" >
		<option value="-1">----Please Select----</option>
		<option value="1">Not Needed - Package Opened</option>
		<option value="2">Not Needed - In New Condition</option>
		<option value="3">Sales Error - In New Condition</option>
		<option value="4">Sales Error - Package Opened</option>
		<option value="8">DOA - With Packaging</option>
		<option value="9">Warranty Return</option>

	</select>
                        </td>
                        <td>
                            <button id="addItem"  class="aboutButtonx" onclick="addRow();" type="button">
                                Add
                            </button>
                            <!--"--></td>
                    </tr>
                </tbody>
            </table>
            </div>
                  <div class="col-md-2">
                
                </div>
               

</section>


<style>

table {
    border-collapse: separate;
    border-spacing: 0 1em;
}
    
    #frmRmaSection tbody #part_number{
        width:180px;
        margin-right:10px;
        
    }
    
    
     #frmRmaSection tbody #qty{
        width:60px;
        margin-left:10px;
        margin-right:10px;
        
    }
    
     #frmRmaSection tbody #serial_number{
        width:460px;
        margin-left:10px;
        margin-right:10px;
        
    }
    
    
    #frmRmaSection tbody #dropReasons
    {
        width:180px;
        margin-left:10px;
        
    }
    
    
    #frmRmaSection tbody #addItem{
           width:150px;
        margin-left:20px;
    }
    
</style>


<section class="rmaRequestImg" style="margin-top:-50px;margin-bottom:80px;">
      <div class="row">
      <div class="col-md-3">
                
                </div>
                
                  <div class="col-md-6">
                
               
<div id="jsGridxx" style="display:block;">
</div>
 </div>
  <div class="col-md-3">
                
                </div>
                </div>
</section>

<section class="rmaRequestImg center">
<div class="col-md-12">
                <div id="frmSubmit" style="visibility:hidden;" class="text-center">
                    <p>
                        <!--<input name="rmaGridHTML" type="hidden" id="rmaGridHTML" />-->
                        <input name="rmaGridArray" type="hidden" id="rmaGridArray" />
                       <br />
                        <button id="submit" name="rma_Submit" value="rma_Submit" class="aboutButtonx" type="submit" onclick="return frmRmaSubmit();">
                            Submit&nbsp;&nbsp;<i class="fa fa-arrow-right" aria-hidden="true"></i>
                        </button>
                    </p>
                </div>
                <div id="submitmsg">
                </div>
            </div>
</form>
</section>

<section class="rmaRequestImg center">
    <img class="img-fluid" src="<?php echo $image_uri; ?>" alt="<?php echo $image_uri_alt; ?>" />
</section>

<section class="rmaRequestBG orangeBG">
</section>


<script>
var rmaJSON = new Array();


function frmRmaSubmit() {

    if (rmaJSON.length < 1) {
        document.getElementById("itemErrMsg").innerHTML = "No RMA lines have been added";
        return false;
    }
    else if (document.getElementById("opportunitynum").value.length == 0)// if ((document.getElementById("requestDate").value.length == 0) || (document.getElementById("email").value.length == 0)
    {
        document.getElementById("itemErrMsg").innerHTML = "Opportunity Number is Required.";
        document.getElementById("opportunitynum").focus();
        return false;
    }
    else if (document.getElementById("requestorname").value.length == 0) {
        document.getElementById("itemErrMsg").innerHTML = "Requestor Name is Required.";
        document.getElementById("requestorname").focus();
        return false;
    }
    else if (document.getElementById("customer").value.length == 0) {
        document.getElementById("itemErrMsg").innerHTML = "Customer Name is Required.";
        document.getElementById("customer").focus();
        return false;
    }
    else if (document.getElementById("contact").value.length == 0) {
        document.getElementById("itemErrMsg").innerHTML = "Contact Name is Required.";
        document.getElementById("contact").focus();
        return false;
    }
    else {
        // var jsGrid = document.getElementById("jsGrid");
        var jsGridArray = document.getElementById("rmaGridArray");

        jsGridArray.value = JSON.stringify({ rmaLines: rmaJSON });//serializeArray(rmaArray);
        //alert(jsGridArray.value);

        return true;
    }
        
}



function addRow() {

    document.getElementById("itemErrMsg").innerHTML = "";

    if (frmRma.part_number.value.length > 0 && frmRma.qty.value.length > 0 && frmRma.dropReasons.value > 0) {
        if (parseFloat(frmRma.qty.value)) {
         
            rmaJSON.push({ "part_number": frmRma.part_number.value, "qty": frmRma.qty.value, "serial_numbers": frmRma.serial_number.value.replace(/,/g, ", "), "reason_text": frmRma.dropReasons.options[frmRma.dropReasons.selectedIndex].text, "reason_id": frmRma.dropReasons.value, "line_id": rmaJSON.length});
          
            
            console.log(rmaJSON);
            var jsGrid = document.getElementById("jsGridxx");
            var frmSubmit = document.getElementById("frmSubmit");
            jsGrid.innerHTML = (makeTableHTML(rmaJSON));
            jsGrid.style.visibility = "visible";
            frmSubmit.style.visibility = "visible";
            document.getElementById("part_number").value = "";
            document.getElementById("qty").value = "";
            document.getElementById("serial_number").value = "";
            document.getElementById("dropReasons").selectedIndex = 0;
        }
        else {
           
            document.getElementById("itemErrMsg").innerHTML = "Please Enter a Numeric Value for Quantity.";
         
        }
    }
    else {
        document.getElementById("itemErrMsg").innerHTML = "All Line Fields Except Serial Number are Required.";
       
    }

}
function deleteRow(rowIndex) {
    if (confirm("Are you sure you want to delete part " + rmaJSON[parseInt(rowIndex)].part_number.toString()+" from this RMA?")){
        //rmaArray.splice(parseInt(rowIndex), 1);
        rmaJSON.splice(parseInt(rowIndex), 1);
        var jsGrid = document.getElementById("jsGridxx");
        if (rmaJSON.length > 0) {
            jsGrid.innerHTML = makeTableHTML(rmaJSON);
        }
        else {
            jsGrid.innerHTML = "All RMA Lines Deleted.";
            var frmSubmit = document.getElementById("frmSubmit");
            frmSubmit.style.visibility = "hidden";
        }
    }
}

function makeTableHTML(myArray) {
    var result = "<div class=\"jsgrid-grid\">"
        + " <table class=\"jsgrid-table\">"
        + "    <tr class=\"jsgrid-header-row\">"
        + "      <th class=\"jsgrid-header-cell\" style=\"width: 210px;\">Part #</th>"
        + "      <th class=\"jsgrid-header-cell\" style=\"width: 70px;\">Qty</th>"
        + "      <th class=\"jsgrid-header-cell\" style=\"width: 400px;\">Serial #</th>"
        + "      <th class=\"jsgrid-header-cell\" style=\"width: 245px;\">Reason</th>"
        + "      <th class=\"jsgrid-header-cell\" style=\"width: 65px;\">Delete</th>"
        + "    </tr>"
        + " <div class=\"jsgrid-grid-body\"><table class=\"jsgrid-table\">"
        + "  <tbody>";
    for (var i = 0; i < myArray.length; i++) {
        if (i % 2 == 0){
            result += "    <tr class=\"jsgrid-row\">";
        }else{
            result += "    <tr class=\"jsgrid-alt-row\">";
        }
    
            result += "      <td class=\"jsgrid-cell\" style=\"width: 210px;\">" + myArray[i].part_number + "</td>";
            result += "      <td class=\"jsgrid-cell\" style=\"width: 70px;\">" + myArray[i].qty + "</td>";
            result += "      <td class=\"jsgrid-cell\" style=\"width: 400px;\">" + myArray[i].serial_numbers + "</td>";
            result += "      <td class=\"jsgrid-cell\" style=\"width: 245px;\">" + myArray[i].reason_text + "</td>";
          
            result += "      <td class=\"jsgrid-cell\"style=\"width: 45px;text-align:center;\"><button type=\"button\" class=\"fa fa-trash-o aboutButtonx\" aria-hidden=\"true\" style=\"width: 40px\" onclick=\"deleteRow(" + i + ");\"></button></td>";
          
    }
    result += "    </tr>"
        + "  </tbody>"
        + " </div>"
        + " </table>"
        + "</div>";



    return result;
}


jQuery(document).ready(function($) {






    $("#myform1").validate({
        rules: {

           
            email: {
                required: true,
                email: true
            },
           
        },
        errorPlacement: function(error, element) {

           
                error.insertAfter(element);
           
        },

        messages: {
        
            email: "Please enter a valid Email Address",
         

        },


        submitHandler: function(form) {




            form.submit();



        }
    });
});
</script>


        <?php
echo $args['after_widget'];
    }

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['sub_title'] = strip_tags($new_instance['sub_title']);
        $instance['section_title'] = strip_tags($new_instance['section_title']);
        $instance['image_uri'] = strip_tags($new_instance['image_uri']);
         $instance['image_uri_alt'] = strip_tags($new_instance['image_uri_alt']);
        
        

        return $instance;
    }

    public function form($display_instance)
    {

        ?>
        <script type="text/javascript">
        jQuery(document).ready(function($) {
            jQuery('.color-picker').on('focus', function() {
                var parent = jQuery(this).parent();
                jQuery(this).wpColorPicker()
                parent.find('.wp-color-result').click();
            });
        });
        </script>


        <?php

        $section_title = $display_instance['section_title'];
        $sub_title = $display_instance['sub_title'];

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('section_title') . '"> ' . __('Section Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('section_title') . '" name="' . $this->get_field_name('section_title') . '" type="text" value="' . $section_title . '" />';
        $rew_html .= '</p><br><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('sub_title') . '"> ' . __('Sub Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('sub_title') . '" name="' . $this->get_field_name('sub_title') . '" type="text" value="' . $sub_title . '" />';
        $rew_html .= '</p><br><br>';

        $rew_html .= '<p><label class="widg-label widg-img-label" for="' . $this->get_field_id('image_uri') . '">Image</label>';
        $rew_html .= '<div class="widg-img">';
        $rew_html .= '<img class="' . $this->get_field_id('image_id') . '_media_image custom_media_image" src="' . $display_instance['image_uri'] . '" height="120"/>';
        $rew_html .= '<input input type="hidden" type="text" class="' . $this->get_field_id('image_id') . '_media_id custom_media_id" name="' . $this->get_field_name('image_id') . '" id="' . $this->get_field_id('image_id') . '" value="' . $display_instance['image_id'] . '" />';
        $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id') . '_media_url custom_media_url" name="' . $this->get_field_name('image_uri') . '" id="' . $this->get_field_id('image_uri') . '" value="' . $display_instance['image_uri'] . '" >';
        $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_upload" id="' . $this->get_field_id('image_id') . '"/></div></p><br><br>';



$rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt') . '"> ' . __('Image Alt', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt') . '" name="' . $this->get_field_name('image_uri_alt') . '" type="text" value="' . $display_instance['image_uri_alt'] . '" />';
        $rew_html .= '</p><br><br>';
        ?>

<script>

jQuery(document ).ready( function(){
    function media_upload( button_class ) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
         jQuery('body').on('click','.custom_media_upload',function(e) {
            var button_id ='#'+jQuery(this).attr( 'id' );
            var button_id_s = jQuery(this).attr( 'id' );
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr( 'id' ).replace( '_button', '' );
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment ){
                if ( _custom_media ) {
                    jQuery( '.' + button_id_s + '_media_id' ).val(attachment.id);
                    jQuery( '.' + button_id_s + '_media_url' ).val(attachment.url);
                    jQuery( '.' + button_id_s + '_media_image' ).attr( 'src',attachment.url).css( 'display','block' );
                } else {
                    return _orig_send_attachment.apply( button_id, [props, attachment] );
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload( '.custom_media_upload' );

});


</script>


<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container input,
select,
textarea {
    float: right;
    width: 60%;
}

#rew_container label {
    width: 40%;
}

<?php echo '.' . $widget_add_id_slider;

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}



#entries {
    padding: 10px 0 0;
}

#entries .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries .entrys:first-child {
    margin: 0;
}

#entries .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries .entry-title.active:after {
    content: '\f142';
}

#entries .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container #entries p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container">
    <?php echo $rew_html; ?>
</div>




<?php

    }

}

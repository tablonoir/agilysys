<?php

function agilysys_solutions_3_column_sliders()
{
    register_widget('agilysys_solutions_3_column_slider');
}

add_action('widgets_init', 'agilysys_solutions_3_column_sliders');

class agilysys_solutions_3_column_slider extends WP_Widget
{
    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys solutions 3 columns slider', 'agilysys_text_domain'));

        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
        add_action('load-widgets.php', array(&$this, 'my_custom_load'));
    }

    public function my_custom_load()
    {
        wp_enqueue_style('wp-color-picker');
        wp_enqueue_script('wp-color-picker');
    }
    /**
     * @see WP_Widget::widget -- do not rename this         * This is for front end
     */
    public function widget($args, $instance)
    {
        extract($args);

        echo $args['before_widget'];
//                 $widget_add_id_what_makes =
        $bg_color = !empty($instance['background_color']) ? $instance['background_color'] : '';
        $max_entries_agilysys_solutions_3_column_slider_solution_image_slider_count = 150;
        $solution_3_column_title = (isset($instance['solution_3_column_title'])) ? $instance['solution_3_column_title'] : "";

        $widget_add_id_solution_sliders = (isset($instance['unique_id'])) ? $instance['unique_id'] : "";
        echo $widget_add_id_solution_sliders;
        if ($widget_add_id_solution_sliders) {

            ?>
<!--<style>
    .solution3columnSlider .swiper-slide.swiper-slide-next .solution3columnSlide{
            border-top-color: <?php// echo $bg_color; ?>;
            border-right-color: <?php// echo $bg_color; ?>;
    }
</style>-->

<?php }?>
<section class="solution3columnSlider">
    <div class="solution3columnSliderTitle">
        <h2 class="dinProStd waterText" style="color:<?php echo $bg_color; ?>;"><?php echo $solution_3_column_title; ?>
        </h2>
    </div>
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <?php

        $count = count($instance['solution_3_column_sub_title']);
        for ($i = 0; $i < $count; $i++) {

            $solution_3_column_sub_title = esc_attr($instance['solution_3_column_sub_title'][$i]);
            $solution_3_column_slider_image = esc_attr($instance['solution_3_column_slider_image'][$i]);
            $image_uri_alt = $instance['image_uri_alt'][$i];

            $link_type = $instance['link_type'][$i];
            if ($link_type == "link") {
                $link = $instance['link'][$i];
            } elseif ($link_type == "page") {
                $post_id = $instance['page'][$i];
                $post = get_post($post_id);
                $link = home_url($post->post_name) . "/";
            }
            ?>

            <div class="swiper-slide">
                <div class="homeLeadVideo">

                    <div class="solution3columnSliderImg">
                        <div class="solution3columnSlide"
                            style=" border-top-color: <?php echo $bg_color; ?>;border-right-color: <?php echo $bg_color; ?>; ">
                            <img src="<?php echo $solution_3_column_slider_image; ?>" class="img-fluid"
                                alt="<?php echo $image_uri_alt; ?>">
                        </div>
                        <div class="solution3columnSliderContent">
                            <a href="<?php echo $link;?>" style="color:<?php echo $bg_color; ?>"><?php echo $solution_3_column_sub_title; ?></a>
                        </div>
                    </div>

                </div>
            </div>

            <?php
}
        ?>
        </div>
        <!-- Add Pagination and navigation arrows -->

        <div class="swiper-button-next">
            <i class="fa fa-angle-right" style="font-size: 94px;color:<?php echo $bg_color; ?>"></i>

        </div>

        <div class="swiper-button-prev">
            <i class="fa fa-angle-left" style="font-size: 94px;color:<?php echo $bg_color; ?>"></i>

        </div>
    </div>
    <?php
echo $args['after_widget'];
        ?>

</section>
<?php
}
    //Function widget ends here

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['solution_3_column_title'] = strip_tags($new_instance['solution_3_column_title']);
        $instance['background_color'] = $new_instance['background_color'];

        $count = count($new_instance['solution_3_column_sub_title']);
        for ($i = 0; $i < $count; $i++) {

            $instance['solution_3_column_sub_title'][$i] = strip_tags($new_instance['solution_3_column_sub_title'][$i]);
            $instance['solution_3_column_slider_image'][$i] = strip_tags($new_instance['solution_3_column_slider_image'][$i]);
            $instance['image_uri_alt'][$i] = strip_tags($new_instance['image_uri_alt'][$i]);

            $instance['link_type'][$i] = $new_instance['link_type'][$i];
            if ($new_instance['link_type'][$i] == 'page') {
                $instance['page'][$i] = $new_instance['page'][$i];
                $instance['link'][$i] = '';
            } elseif ($new_instance['link_type'][$i] == 'link') {
                $instance['link'][$i] = $new_instance['link'][$i];
                $instance['page'][$i] = '';

            }

        }
        return $instance;
    }
    //Function update ends here

    /**
     * @see WP_Widget::form -- do not rename this
     */
    public function form($display_instance)
    {

        $max_entries_agilysys_solutions_3_column_slider_solutions_slider = 15;

        $widget_add_id_solution_slider = $this->get_field_id('') . "agilysys_solutions_3_column_sliders";

//                  $widget_add_id_solution_sliders=$widget_add_id_solution_slider;

//                  echo $widget_add_id_solution_slider;
        $solution_3_column_title = ($display_instance['solution_3_column_title']);

        $background_color = ($display_instance['background_color']);

//                 $widget_add_id_solution_sliders  = ($display_instance['unique_id']);

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('solution_3_column_title') . '"> ' . __('Title', 'agilysys_text_domain') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('solution_3_column_title') . '" name="' . $this->get_field_name('solution_3_column_title') . '" type="text" value="' . $solution_3_column_title . '" />';
        $rew_html .= '</p><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('background_color') . '"> ' . __('Select Background Color', 'agilysys_text_domain') . ' :</label>';
        $rew_html .= '<input  class="my-color-picker" id="' . $this->get_field_id('background_color') . '" name="' . $this->get_field_name('background_color') . '" type="text" value="' . $background_color . '" />';
        $rew_html .= '</p>';
        $rew_html .= '</p>';

        ?>

<script type='text/javascript'>
jQuery(document).ready(function($) {
    $('.my-color-picker').wpColorPicker();
});
</script><?php
$count = count($display_instance['solution_3_column_slider_image']);

        $rew_html .= '<div class="add_new_rowxx-input-containers"><div id="entries_agilysys_solutions_3_column_slider">';

        $rew_html .= '<input class="cnt909" id="cnt909" name="cnt" type="hidden" value="' . $count . '">';

        for ($i = 0; $i < $count; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'agilysys_text_domain') . ' </span>';
            $rew_html .= '<div class="entry-desc cf">';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('solution_3_column_slider_image' . $i) . '">' . __('Image(253x300)', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';

            $rew_html .= '<div class="widg-img' . $i . '">';
            $show1 = (!empty($display_instance['solution_3_column_slider_image'][$i])) ? 'style="display:block;"' : '';
            $rew_html .= '<label id="image_uri_agilysys_solutions_3_column_slider' . $i . '"></label><br><img  class="' . $this->get_field_id('image_id' . $i) . '_media_image' . $i . ' custom_media_image' . $i . '" src="' . $display_instance['solution_3_column_slider_image'][$i] . '" ' . $show1 . ' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id' . $i) . '_media_id' . $i . ' custom_media_id' . $i . '" name="' . $this->get_field_name('image_id' . $i) . '" id="' . $this->get_field_id('image_id' . $i) . '" value="' . $display_instance['image_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id' . $i) . '_media_url' . $i . ' custom_media_url' . $i . '" name="' . $this->get_field_name('solution_3_column_slider_image[]') . '" id="' . $this->get_field_id('solution_3_column_slider_image' . $i) . '" value="' . $display_instance['solution_3_column_slider_image'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_upload' . $i . '" id="' . $this->get_field_id('image_id' . $i) . '"/>';

            $rew_html .= '</div>';
            $rew_html .= '</p><br><br><br><br><br><br>';
            ?>
<script>
jQuery(document).ready(function() {


    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    //if (attachment.height == 1080 && attachment.width == 1920) {
                        jQuery('.' + button_id_s + '_media_id<?php echo $i; ?>').val(attachment.id);
                        jQuery('.' + button_id_s + '_media_url<?php echo $i; ?>').val(attachment
                            .url);
                        jQuery('.' + button_id_s + '_media_image<?php echo $i; ?>').attr('src',
                            attachment.url).css('display', 'block');
                        /*jQuery('#image_uri_agilysys_solutions_3_column_slider<?php echo $i; ?>')
                            .html("");
                    } else {
                        jQuery('#image_uri_agilysys_solutions_3_column_slider<?php echo $i; ?>')
                            .html("Please Enter the correct Dimensions 1920x1080").css('color',
                                'red');

                    }
                    */
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload<?php echo $i; ?>');




});
</script>
<?php

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt' . $i) . '"> ' . __('Image Alt', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt' . $i) . '" name="' . $this->get_field_name('image_uri_alt[]') . '" type="text" value="' . $display_instance['image_uri_alt'][$i] . '">';
            $rew_html .= '</p><br>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('solution_3_column_sub_title' . $i) . '"> ' . __('Title', 'agilysys_text_domain') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('solution_3_column_sub_title' . $i) . '" name="' . $this->get_field_name('solution_3_column_sub_title[]') . '" type="text" value="' . $display_instance['solution_3_column_sub_title'][$i] . '">';
            $rew_html .= '</p><br>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('link_type' . $i) . '"> ' . __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('link_type' . $i) . '" name="' . $this->get_field_name('link_type[]') . '" onChange="show_hide_div_agilysys_solutions_3_column_slider(this.value,' . $i . ');">';
            $rew_html .= '<option value="">Please Select</option>';

            $link_type = $display_instance['link_type'][$i];

            if ($link_type == 'page') {
                $rew_html .= '<option value="page" selected="selected">Internal Page Link</option>';
            } else {
                $rew_html .= '<option value="page">Internal Page Link</option>';
            }

            if ($link_type == 'link') {
                $rew_html .= '<option value="link" selected="selected">External Link</option>';
            } else {
                $rew_html .= '<option value="link">External Link</option>';
            }

            $rew_html .= '</select>';
            $rew_html .= '</p><br>';

            $args = array(
                'sort_order' => 'desc',
                'sort_column' => 'post_title',
                'hierarchical' => 1,
                'exclude' => '',
                'include' => '',
                'meta_key' => '',
                'meta_value' => '',
                'authors' => '',
                'child_of' => 0,
                'parent' => -1,
                'exclude_tree' => '',
                'number' => '',
                'offset' => 0,
                'post_type' => 'page',
                'post_status' => 'publish',
            );
            $pages = get_pages($args); // get all pages based on supplied args

            if ($link_type == 'page') {
                $show1 = 'style="display:block"';
                $show2 = 'style="display:none"';
            } elseif ($link_type == 'link') {
                $show1 = 'style="display:none"';
                $show2 = 'style="display:block"';

            } else {
                $show1 = 'style="display:none"';
                $show2 = 'style="display:none"';
            }
            $rew_html .= '<div id="page_div_agilysys_solutions_3_column_slider' . $i . '" ' . $show1 . '><p>';
            $rew_html .= '<label for="' . $this->get_field_id('page' . $i) . '"> ' . __('Page', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('page' . $i) . '" name="' . $this->get_field_name('page[]') . '">';
            $rew_html .= '<option value="">Please Select</option>';

            $page = $display_instance['page'][$i];

            foreach ($pages as $key) {

                if ($page == $key->ID) {
                    $rew_html .= '<option value="' . $key->ID . '" selected="selected">' . $key->post_title . '</option>';
                } else {
                    $rew_html .= '<option value="' . $key->ID . '">' . $key->post_title . '</option>';
                }

            }

            $rew_html .= '</select>';
            $rew_html .= '</p></div><br>';

            $rew_html .= '<div id="link_div_agilysys_solutions_3_column_slider' . $i . '" ' . $show2 . '><p>';
            $rew_html .= '<label for="' . $this->get_field_id('link' . $i) . '"> ' . __('Link', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('link' . $i) . '" name="' . $this->get_field_name('link[]') . '" type="text" value="' . $display_instance['link'][$i] . '" />';
            $rew_html .= '</p></div><br>';
            ?>

<script>
//show hide div based on i value and value of dropdown
function show_hide_div_agilysys_solutions_3_column_slider(val, i) {
console.log(val);
    if (val == 'page') {
        jQuery("#page_div_agilysys_solutions_3_column_slider" + i).show();
        jQuery("#link_div_agilysys_solutions_3_column_slider" + i).hide();
    } else if (val == 'link') {
        jQuery("#page_div_agilysys_solutions_3_column_slider" + i).hide();
        jQuery("#link_div_agilysys_solutions_3_column_slider" + i).show();
    }

}
</script>

<?php

            $k = $i + 1;
            $rew_html .= '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' . $k . ');">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';

        }

        $rew_html .= '</div></div>';
        $rew_html .= '<div id="message">' . __('Sorry, you reached to the limit of', 'agilysys_text_domain') . ' "' . $max_entries_agilysys_solutions_3_column_slider_solutions_slider . '" ' . __('maximum entries_agilysys_solutions_3_column_slider', 'agilysys_text_domain') . '.</div>';

        $rew_html .= '<div class="' . $widget_add_id_solution_slider . '" style="margin-bottom: 36px;text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;" onClick="add_new_row();">' . __('ADD ROW', 'AGILYSYS_TEXT_DOMAIN') . '</div>';
        ?>
<script>
function add_new_row() {
    var cnt = '';

    jQuery.each(jQuery("#entries_agilysys_solutions_3_column_slider .cnt909"), function() {
        if (jQuery(this).val() != '') {
            cnt = jQuery(this).val();
        }
    });






    cnt++;

    jQuery.each(jQuery("#entries_agilysys_solutions_3_column_slider .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(cnt);
        }
    });

    var new_row = '<div id="entry' + cnt +
        '"  class="entrys"><span class="entry-title" onclick = "slider(this);"><?php echo __('Add New Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span>';
    new_row += '<div class="entry-desc cf">';


    new_row += '<div class="widg-img' + cnt + '">';
    new_row += '<label id="image_uri_agilysys_solutions_3_column_slider' + cnt +
        '"></label><br><img class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_image' + cnt +
        ' custom_media_image' + cnt + '" style="display:none;" width=200" height="120"/>';
    new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_id' +
        cnt + ' custom_media_id' + cnt + '" name="<?php echo $this->get_field_name('image_id[]'); ?>" />';
    new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_url' +
        cnt + ' custom_media_url' + cnt +
        '" name="<?php echo $this->get_field_name('solution_3_column_slider_image[]'); ?>">';
    new_row += '<input type="button" value="Upload Image" class="button custom_media_upload' + cnt +
        '" id="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '"/>';

    new_row += '</div><br><br><br>';


    jQuery(document).ready(function() {




        function media_upload(button_class) {
            var _custom_media = true,
                _orig_send_attachment = wp.media.editor.send.attachment;
            jQuery('body').on('click', '.custom_media_upload' + cnt, function(e) {
                var button_id = '#' + jQuery(this).attr('id');
                var button_id_s = jQuery(this).attr('id');
                console.log(button_id);
                var self = jQuery(button_id);
                var send_attachment_bkp = wp.media.editor.send.attachment;
                var button = jQuery(button_id);
                var id = button.attr('id').replace('_button', '');
                _custom_media = true;

                wp.media.editor.send.attachment = function(props, attachment) {
                    if (_custom_media) {


                       // if (attachment.height == 1080 && attachment.width == 1920) {
                            jQuery('.' + button_id_s + '_media_id' + cnt).val(attachment.id);
                            jQuery('.' + button_id_s + '_media_url' + cnt).val(attachment.url);
                            jQuery('.' + button_id_s + '_media_image' + cnt).attr('src',
                                attachment.url).css('display', 'block');

                            /*jQuery('#image_uri_agilysys_solutions_3_column_slider' + cnt)
                                .html("");
                        } else {
                            jQuery('#image_uri_agilysys_solutions_3_column_slider' + cnt)
                                .html("Please Enter the correct Dimensions 1920x1080").css(
                                    'color', 'red');

                        }
                        */
                    } else {
                        return _orig_send_attachment.apply(button_id, [props, attachment]);
                    }
                }
                wp.media.editor.open(button);
                return false;
            });
        }
        media_upload('.custom_media_upload' + cnt);

    });

    new_row += '<p>';
    new_row += '<label for=""><?php echo __('Image Alt', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';

    new_row +=
        '<input class="" name="<?php echo esc_attr($this->get_field_name('image_uri_alt[]')); ?>" type="text" value="">';
    new_row += '</p>';


    new_row += '<p>';
    new_row += '<label><?php echo __('Title', 'agilysys_solution'); ?>:</label>';
    new_row += '<input  name="<?php echo $this->get_field_name('solution_3_column_sub_title[]'); ?>" type="text">';
    new_row += '</p><br><br>';


    new_row += '<p>';
    new_row += '<label for=""><?php echo __('Select Link type url:', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
    new_row +=
        '<select  name="<?php echo $this->get_field_name('link_type[]'); ?>" onChange="show_hide_div1_agilysys_solutions_3_column_slider(this.value,' +
        cnt + ');">';
    new_row += '<option value="">Please Select</option>';
    new_row += '<option value="page">Internal Page Link</option>';
    new_row += '<option value="link">External Link</option>';
    new_row += '</select>';
    new_row += '</p><br><br>';



    <?php
$args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        ?>


    new_row += '<div id="page_div_agilysys_solutions_3_column_slider' + cnt + '" style="display:none;"><p>';
    new_row += '<label for=""><?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
    new_row += '<select  name="<?php echo $this->get_field_name('page[]'); ?>">';
    new_row += '<option value="">Please Select</option>';



    <?php

        foreach ($pages as $key) {
            ?>

    new_row += '<option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>';


    <?php

        }
        ?>
    new_row += '</select>';

    new_row += '</p></div><br><br>';

    new_row += '<div id="link_div_agilysys_solutions_3_column_slider' + cnt + '" style="display:none;"><p>';
    new_row += '<label for=""><?php echo __('Link', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
    new_row += '<input name="<?php echo $this->get_field_name('leadersDesc7[]'); ?>" type="text" value="" />';
    new_row += '</p></div><br><br>';




    var new_cnt = cnt;

    new_row += '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' + new_cnt +
        ');"><?php echo __('Delete Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span></a></p>';
    new_row += '</div></div>';

    jQuery('.add_new_rowxx-input-containers #entries_agilysys_solutions_3_column_slider').append(new_row);




}

//show hide div based on i value and value of dropdown
function show_hide_div1_agilysys_solutions_3_column_slider(val, i) {
console.log(val);
if (val == 'page') {
    jQuery("#page_div_agilysys_solutions_3_column_slider" + i).show();
    jQuery("#link_div_agilysys_solutions_3_column_slider" + i).hide();
} else if (val == 'link') {
    jQuery("#page_div_agilysys_solutions_3_column_slider" + i).hide();
    jQuery("#link_div_agilysys_solutions_3_column_slider" + i).show();
}

}

function delete_row(cnt) {
    jQuery.each(jQuery(".add_new_rowxx-input-containers #entries_agilysys_solutions_3_column_slider"), function() {
        jQuery(' #entry' + cnt).remove();
    });
    var last_cnt = 0;

    jQuery.each(jQuery("#entries_agilysys_solutions_3_column_slider .cnt909"), function() {
        if (jQuery(this).val() != '') {
            last_cnt = jQuery(this).val();
        }
    });

    last_cnt--;
    jQuery.each(jQuery("#entries_agilysys_solutions_3_column_slider .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(last_cnt);
        }
    });


}
</script>
<style>

.wp-picker-container{
    margin-left:0%;
}
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}


#rew_container_agilysys_solutions_3_column_slider p {
    padding:10px;
}


#entries_agilysys_solutions_3_column_slider img{
    float:left !important;
}

#rew_container_agilysys_solutions_3_column_slider  select{
     float: left;
    width: 60%;
    margin-top:20px !important;
    margin-bottom:10px !important;
}

#rew_container_agilysys_solutions_3_column_slider input,

textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_solutions_3_column_slider label {
    width: 40%;
     float: left;
}

<?php echo '.'. $widget_add_id_solution_slider;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}

#entries_agilysys_solutions_3_column_slider #remove-img-bg {
    background: url('<?php echo ZWREW_URL; ?>assets/images/deleteimg.png') center center no-repeat;
    width: 20px;
    height: 22px;
    display: none;
}

#entries_agilysys_solutions_3_column_slider {
    padding: 10px 0 0;
}

#entries_agilysys_solutions_3_column_slider .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_solutions_3_column_slider .entrys:first-child {
    margin: 0;
}

#entries_agilysys_solutions_3_column_slider .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_solutions_3_column_slider .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_solutions_3_column_slider .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_solutions_3_column_slider .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_solutions_3_column_slider .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_solutions_3_column_slider #entries_agilysys_solutions_3_column_slider plast label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_solutions_3_column_slider">
    <?php echo $rew_html; ?>
</div>
<?php
} //Function form ends here
} // class ends here
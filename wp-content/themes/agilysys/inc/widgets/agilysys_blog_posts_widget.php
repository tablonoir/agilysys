<?php

function load1_agilysys_blog_posts_widget()
{
    register_widget('agilysys_blog_posts_widget');
}

add_action('widgets_init', 'load1_agilysys_blog_posts_widget');

class agilysys_blog_posts_widget extends WP_Widget
{
    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Blog Post Widget', 'agilysys_text_domain'));
        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');

        wp_enqueue_script('jquery-ui-datepicker');

        // You need styling for the datepicker. For simplicity I've linked to the jQuery UI CSS on a CDN.
        wp_register_style('jquery-ui', 'https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css');
        wp_enqueue_style('jquery-ui');

    }

    public function widget($args, $instance)
    {

        echo $args['before_widget'];
        $from_date = date("F d,Y", strtotime($instance['from_date']));
        $to_date = date("F d,Y", strtotime($instance['to_date']));

        $no_of_posts = $instance['no_of_posts'];

        $category = ($instance['sub_category']);

        $the_query = new WP_Query(
            array(
                'post_type' => 'post',
                'post_status ' => 'publish',
                'date_query' => array(
                    array(
                        'after' => $from_date,
                        'before' => $to_date,
                        'inclusive' => true,
                    ),
                ),
                'category_name' => $category,
                'posts_per_page' => -1,
            )
        );

        ?>


<section class="homePosts">
    <div class="swiper-container">
        <!-- Additional required wrapper -->
        <?php if (count($the_query->posts) > 0): ?>
        <div class="swiper-wrapper">

            <?php foreach ($the_query->posts as $key => $post): ?>

            <?php
$image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail');

        ?>
            <div class="swiper-slide">
                <div class="homePostsInner">


                    <div class="homePostsCard homePostsCard1">

                        <a class="homePostGrid" href="<?php echo $post->post_name; ?>">

                            <div class="homePostsCardImg">

                                <img alt="posts" class="img-fluid" src="<?php echo $image[0]; ?>">

                            </div>

                            <div class="homePostsCardContent">
                                <div class="homePostsCardSMDate">
                                    <p><time datetime="<?php echo get_the_date('c'); ?>"
                                            itemprop="datePublished"><?php echo date("F d,Y", strtotime($post->post_date)); ?></time>
                                    </p>
                                </div>
                                <div class="homePostsCardContent1">
                                    <h4 class="blackText"><?php echo $post->post_title; ?></h4>

                                    <?php

        $trimmed_content = wp_trim_words($post->post_content, 20, '... <a href="' . get_permalink() . '"></a>');

        ?>
                                    <p><?php echo $trimmed_content; ?></p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
            <?php wp_reset_postdata();?>

            <?php else: ?>
            <p><?php __('No News');?></p>
            <?php endif;?>
        </div>

        <!-- If we need navigation buttons -->
        <div class="swiper-button-next">
            <img src="<?php echo get_template_directory_uri(); ?>/img/arrow-right-white.png" class="img-fluid"
                alt="arrow-left">
        </div>

        <div class="swiper-button-prev">
            <img src="<?php echo get_template_directory_uri(); ?>/img/arrow-left-white.png" class="img-fluid"
                alt="arrow-right">
        </div>
    </div>

    <div class="flex homePostEnd">
        <div class="homePostEnd1">
            <h2 class="dinProStd whiteText">What's Happening in Hospitality?</h2>
        </div>
        <div class="homePostEnd2">
            <a href="<?php //the_field( 'home_post_blog_link' ); ?>" class="homebuttonRounded">View All Posts
                <i class="fa fa-arrow-right" aria-hidden="true"></i>
            </a>
        </div>
    </div>

</section>








<?php
echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {

        $instance = array();

        $instance['no_of_posts'] = $new_instance['no_of_posts'];

        $instance['category'] = $new_instance['category'];
        $instance['sub_category'] = $new_instance['sub_category'];

        $from_date = explode("-", $new_instance['from_date']);
        $instance['from_date'] = $from_date[2] . "-" . $from_date[1] . "-" . $from_date[0];

        $to_date = explode("-", $new_instance['to_date']);
        $instance['to_date'] = $to_date[2] . "-" . $to_date[1] . "-" . $to_date[0];

        return $instance;
    }

    /**
     * @see WP_Widget::form -- do not rename this
     */
    public function form($display_instance)
    {

        $max_entries_hospitality_software = 25;
        $widget_add_id_hosipitality_software = $this->get_field_id('') . "add_agilysys_blog_posts_widget";

        $no_of_posts = $display_instance['no_of_posts'];

        $selected_category = $display_instance['category'];

        $from_datexx = explode("-", $display_instance['from_date']);
        $from_date = $from_datexx[2] . "-" . $from_datexx[1] . "-" . $from_datexx[0];

        $to_datexx = explode("-", $display_instance['to_date']);
        $to_date = $to_datexx[2] . "-" . $to_datexx[1] . "-" . $to_datexx[0];

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('from_date') . '"> ' . __('From Date', 'agilysys_text_domain') . ' :</label>';
        $rew_html .= '<input id="from_date" name="' . $this->get_field_name('from_date') . '" type="text" value="' . $from_date . '" />';
        $rew_html .= '</p><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('from_date') . '"> ' . __('To Date', 'agilysys_text_domain') . ' :</label>';
        $rew_html .= '<input id="to_date" name="' . $this->get_field_name('to_date') . '" type="text" value="' . $to_date . '" />';
        $rew_html .= '</p><br>';
        ?>

<script>
jQuery(document).ready(function($) {
    $("#from_date").datepicker({
        dateFormat: 'dd-mm-yy'
    });
    $("#to_date").datepicker({
        dateFormat: 'dd-mm-yy'
    });
});
</script>

<?php

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('no_of_posts') . '"> ' . __('No of Posts', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<select id="' . $this->get_field_id('no_of_posts') . '" name="' . $this->get_field_name('no_of_posts') . '">';
        $rew_html .= '<option value="">Please Select</option>';

        if ($no_of_posts == "5") {
            $rew_html .= '<option value="5" selected="selected">5 Posts</option>';
        } else {
            $rew_html .= '<option value="5">5 Posts</option>';
        }

        if ($no_of_posts == "10") {
            $rew_html .= '<option value="10" selected="selected">10 Posts</option>';
        } else {
            $rew_html .= '<option value="10">10 Posts</option>';
        }

        if ($no_of_posts == "15") {
            $rew_html .= '<option value="15" selected="selected">15 Posts</option>';
        } else {
            $rew_html .= '<option value="15">15 Posts</option>';
        }

        if ($no_of_posts == "20") {
            $rew_html .= '<option value="20" selected="selected">20 Posts</option>';
        } else {
            $rew_html .= '<option value="20">20 Posts</option>';
        }

        if ($no_of_posts == "25") {
            $rew_html .= '<option value="25" selected="selected">25 Posts</option>';
        } else {
            $rew_html .= '<option value="25">25 Posts</option>';
        }

        $rew_html .= '</select>';
        $rew_html .= '</p><br>';

        $args = array("hide_empty" => 0,

            "parent" => 0,
            "type" => "post",
            "orderby" => "name",
            "order" => "ASC");
        $parent_categories = get_categories($args);

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('category') . '"> ' . __('Select Blog Category', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<select id="' . $this->get_field_id('category') . '" name="' . $this->get_field_name('category') . '" onChange="fetch_subcat(this.value);">';
        $rew_html .= '<option value="">Please Select</option>';

        foreach ($parent_categories as $key) {

            if ($key->name != 'Uncategorized') {

                if ($key->term_id == $display_instance['category']) {
                    $rew_html .= '<option value="' . $key->term_id . '" selected="selected">' . $key->name . '</option>';
                } else {
                    $rew_html .= '<option value="' . $key->term_id . '">' . $key->name . '</option>';
                }

            }

        }

        $rew_html .= '</select>';
        $rew_html .= '</p><br>';

        if ($display_instance['category'] != "") {

            $args = array("hide_empty" => 0,

                "parent" => $display_instance['category'],
                "type" => "post",
                "orderby" => "name",
                "order" => "ASC");
            $child_cat = get_categories($args);

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('sub_category') . '"> ' . __('Select Blog SubCategory', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="sub_category_agilysys_blog_posts_widget" name="' . $this->get_field_name('sub_category') . '">';
            $rew_html .= '<option value="">Please Select</option>';
     
            foreach ($child_cat as $key) {

                if ($key->name == $display_instance['sub_category']) {
                    $rew_html .= '<option value="' . $key->name . '" selected="selected">' . $key->name . '</option>';
                } else {
                    $rew_html .= '<option value="' . $key->name . '">' . $key->name . '</option>';
                }

            }
            $rew_html .= '</select>';
            $rew_html .= '</p><br>';

        }
        else
        {
            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('sub_category') . '"> ' . __('Select Blog SubCategory', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="sub_category_agilysys_blog_posts_widget" name="' . $this->get_field_name('sub_category') . '">';
            $rew_html .= '<option value="">Please Select</option>';
         
            $rew_html .= '</select>';
            $rew_html .= '</p><br>';
        }

        ?>

<script>
function fetch_subcat(value) {
 // This is required for AJAX to work on our page
 var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

 jQuery("#sub_category_agilysys_blog_posts_widget").html('<option value="">Please Select</option>');


// Data to receive from our server
// the value in 'action' is the key that will be identified by the 'wp_ajax_' hook
var data = {

    action: "fetch-all-subcategories-based-on-category",
    value: value,

};


// Send the data
jQuery.post(ajaxurl, data, function(response) {


    var dataxx = JSON.parse(response);

    jQuery("#sub_category_agilysys_blog_posts_widget").append(dataxx.msg);






});
}
</script>


<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_blog_posts_widget select {
    float: left;
    width: 60%;
    margin-top: 20px !important;
    margin-bottom: 10px !important;
}

#rew_container_agilysys_blog_posts_widget input,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_blog_posts_widget label {
    width: 40%;
    float: left;
}

#rew_container_agilysys_blog_posts_widget p {
    padding: 10px;
}


<?php echo '.'. $widget_add_id_hosipitality_software;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}


#entries {
    padding: 10px 0 0;
}

#entries .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries .entrys:first-child {
    margin: 0;
}

#entries .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries .entry-title.active:after {
    content: '\f142';
}

#entries .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_blog_posts_widget #entries p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_blog_posts_widget">
    <?php echo $rew_html; ?>
</div>




<?php
}
}
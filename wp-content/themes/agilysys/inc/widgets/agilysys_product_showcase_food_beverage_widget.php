<?php

function load_agilysys_product_showcase_food_beverage_widget()
{
    register_widget('agilysys_product_showcase_food_beverage_widget');
}
add_action('widgets_init', 'load_agilysys_product_showcase_food_beverage_widget');

class agilysys_product_showcase_food_beverage_widget extends WP_Widget
{

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Product Showcase Food Beverage Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {

        echo $args['before_widget'];

        $section_title1 = $instance['section_title1'];

        $industries_arr = array(
            1 => 'Hotels & Resorts',
            2 => 'Casino Resorts',
            3 => 'Tribal Gaming',
            4 => 'Cruise Lines',
            5 => 'Foodservice Management',
            6 => 'Sports & Entertainment',
            7 => 'Restaurants',
            8 => 'Spa',
            9 => 'Golf',
        );

        $products_arr = array(
            'PROPERTY MANAGEMENT' => array(
                1 => 'Agilysys Stay',
                2 => 'Agilysys LMS',
                3 => 'Agilysys Visual One PMS',
                4 => 'Agilysys Sales and Catering',
                5 => 'rGuest® Book',
                6 => 'rGuest® Express Kiosk',
                7 => 'rGuest® Express Mobile',
                8 => 'rGuest® Service',
                9 => 'b4checkin',
            ),

            'POINT OF SALE' => array(
                10 => 'Agilysys InfoGenesis',
                11 => 'IG Flex',
                12 => 'IG Buy',
                13 => 'IG OnDemand',
            ),
            'PAYMENT SOLUTION' => array(
                14 => 'Agilysys Pay',

            ),
            'ANALYTICS & MARKETING LOYALTY' => array(
                15 => 'Agilysys Analyze',
            ),
            'INVENTORY & PROCUREMENT' => array(
                16 => 'Agilysys Eatec',
                17 => 'Agilysys SWS',

            ),
            'RESERVATIONS AND TABLE MANAGEMENT' => array(
                18 => 'Agilysys Seat',
            ),
            'ACTIVITY SCHEDULING' => array(
                19 => 'Agilysys Golf',
                20 => 'Agilysys Spa',
            ),
            'DOCUMENT MANAGEMENT' => array(
                21 => 'Agilysys DataMagine',
            ),
            'SERVICES' => array(
                22 => 'Professional Services',
            ),

        );
        ?>


       <style>

 
  .loader_agilysys_product_showcase_food_beverage_widget {
    width: 80px;
    height: 80px;
    background: #fff;
    border: 2px solid #f3f3f3;
    border-top: 3px solid #008000;
    border-radius: 100%;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 45%;
    margin: auto;
    animation: spin 1s infinite;

}

@keyframes spin {
    from {
        transform: rotate(0deg);
    }

    to {
        transform: rotate(360deg);
    }
}

</style>

 <div id="overlay_agilysys_product_showcase_food_beverage_widget">
        <div class="loader_agilysys_product_showcase_food_beverage_widget"></div>
    </div>

<section class="videosTitle food">
    <div class="videosTitleBox waterBG center">
        <h2 class="dinProStd h2 whiteText"><?php echo $section_title1; ?></h2>
        <div class="videoFilter flex whiteText">
            <p>FILTER BY :</p>
            <div class="videoFilterBox">
                <select class="filterOne" name="industries"
                    id="industries_agilysys_product_showcase_food_beverage_widget"
                    onChange="fetch_data_agilysys_product_showcase_food_beverage_widget(1);">
                    <option value="">All Industries</option>

                    <?php

        foreach ($industries_arr as $key => $val) {

            ?>
                    <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                    <?php
}
        ?>
                </select>
            </div>
            <div class="videoFilterBox">
                <select class="filterTwo" name="products" id="products_agilysys_product_showcase_food_beverage_widget"
                    onChange="fetch_data_agilysys_product_showcase_food_beverage_widget(1);">
                    <option value="">All Products</option>
                    <?php

        foreach ($products_arr as $key => $val) {

            if (is_array($val)) {
                ?>
                    <optgroup label="<?php echo $key; ?>">
                        <?php
foreach ($val as $k => $v) {
                    ?>
                        <option value="<?php echo $k; ?>"><?php echo $v; ?></option>
                        <?php
}
                ?>
                    </optgroup>
                    <?php
}

        }
        ?>
                </select>
            </div>

            <div class="videoClear" onClick="reset_filters_agilysys_product_showcase_food_beverage_widget();">
                <p>CLEAR FILTERS</p>
            </div>

        </div>
    </div>

    <div id="articleList_agilysys_product_showcase_food_beverage_widget">

    </div>


</section>

<div id="pagination_agilysys_product_showcase_food_beverage_widget">


</div>

<!-- Modal -->


<div class="modal foodbevarageModalPopup fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

            <button type="button" class="close" data-dismiss="modal">&times;</button>


            <video controls id="videoxx">
                <source src="" type="video/mp4">


            </video>

            <iframe  src="" id="youtubexx">
            </iframe>



        </div>

    </div>
</div>
<script>
function reset_filters_agilysys_product_showcase_food_beverage_widget() {

    jQuery('#industries_agilysys_product_showcase_food_beverage_widget').val('');
    jQuery('#products_agilysys_product_showcase_food_beverage_widget').val('');
    fetch_data_agilysys_product_showcase_food_beverage_widget(1);
}

jQuery(document).on("click", ".aboutButton", function() {
    var id = jQuery(this).data('id');


    var video_type = jQuery('#video_type_' + id).val();
    var video_uri = jQuery('#video_uri_' + id).val();
    var youtube_url = jQuery('#youtube_url_' + id).val();

    if (video_type == "video") {
        jQuery('#videoxx').attr('src', video_uri);
        jQuery('#videoxx').show();
        jQuery('#youtubexx').hide();
    }

    if (video_type == "youtube") {
        jQuery('#youtubexx').attr('src', youtube_url);
        jQuery('#videoxx').hide();
        jQuery('#youtubexx').show();
    }
});

jQuery(document).ready(function(){

jQuery("#myModal").on("hidden.bs.modal", function () {
    jQuery('#youtubexx').attr('src', '');
      jQuery('#videoxx').attr('src', '');
});
});
</script>
<style>
.cvf_pag_loading {
    padding: 20px;
}

.cvf-universal-pagination ul {
    margin: 0;
    padding: 0;
}

.cvf-universal-pagination ul li {
    display: inline;
    margin: 3px;
    padding: 4px 8px;
    background: #FFF;
    color: black;
}

.cvf-universal-pagination ul li.active:hover {
    cursor: pointer;
    background: #1E8CBE;
    color: white;
}

.cvf-universal-pagination ul li.inactive {
    background: #7E7E7E;
}

.cvf-universal-pagination ul li.selected {
    background: #1E8CBE;
    color: white;
}

#pagination {
    float: right;
    margin-right: 15%;
}
</style>

<script>
jQuery(document).ready(function($) {
    fetch_data_agilysys_product_showcase_food_beverage_widget(1);

    jQuery(document).on('click', '#pagination_agilysys_product_showcase_food_beverage_widget .cvf-universal-pagination li.active', function() {

        var page = jQuery(this).attr('p');
        fetch_data_agilysys_product_showcase_food_beverage_widget(page);
    });

});



function fetch_data_agilysys_product_showcase_food_beverage_widget(page) {
    
    jQuery(".loader_agilysys_product_showcase_food_beverage_widget").fadeIn("slow");
    var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';


    var data = {

        action: "fetch-data-agilysys-product-showcase-food-beverage-widget",
        industries: jQuery('#industries_agilysys_product_showcase_food_beverage_widget').val(),
        products: jQuery('#products_agilysys_product_showcase_food_beverage_widget').val(),
        data: '<?php echo json_encode($instance); ?>',
        page: page,
    };



    jQuery.post(ajaxurl, data, function(response) {


        var dataxx = JSON.parse(response);


        jQuery("#articleList_agilysys_product_showcase_food_beverage_widget").html(dataxx.msg);

        jQuery("#pagination_agilysys_product_showcase_food_beverage_widget").html(dataxx.pag_container);

jQuery(".loader_agilysys_product_showcase_food_beverage_widget").fadeOut("slow");


    });
}
</script>




<?php
echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['section_title1'] = strip_tags($new_instance['section_title1']);
        $instance['what_makes_rows'] = strip_tags($new_instance['what_makes_rows']);

        $count = count($new_instance['section_title']);

        for ($i = 0; $i < $count; $i++) {

            $instance['section_title'][$i] = strip_tags($new_instance['section_title'][$i]);
            $instance['webinars_info_front_image'][$i] = strip_tags($new_instance['webinars_info_front_image'][$i]);
            $instance['webinars_info_front_image_alt'][$i] = strip_tags($new_instance['webinars_info_front_image_alt'][$i]);

            $instance['webinars_info_front_logo'][$i] = strip_tags($new_instance['webinars_info_front_logo'][$i]);
            $instance['webinars_info_front_logo_alt'][$i] = strip_tags($new_instance['webinars_info_front_logo_alt'][$i]);

            // $instance['webinars_info_desc-' . $i] = strip_tags($new_instance['webinars_info_desc-' . $i]);
            $instance['webinars_info_url_title'][$i] = strip_tags($new_instance['webinars_info_url_title'][$i]);
            $instance['industries'][$i] = strip_tags($new_instance['industries'][$i]);
            $instance['products'][$i] = strip_tags($new_instance['products'][$i]);

            $instance['video_type'][$i] = strip_tags($new_instance['video_type'][$i]);
            $video_type = strip_tags($new_instance['video_type'][$i]);

            if ($video_type == "youtube") {
                $instance['youtube_url'][$i] = strip_tags($new_instance['youtube_url'][$i]);
                $instance['video_uri'][$i] = '';
            } elseif ($video_type == 'video') {
                $instance['youtube_url'][$i] = '';
                $instance['video_uri'][$i] = strip_tags($new_instance['video_uri'][$i]);
            }

        }
        return $instance;
    }

    /**
     * @see WP_Widget::form -- do not rename this
     */
    public function form($display_instance)
    {




        //$widget_add_id_webinars_info = $this->id . "-add";
        $widget_add_id_webinars_info = $this->get_field_id('') . "add_agilysys_product_showcase_food_beverage_widget";

        $section_title1 = $display_instance['section_title1'];

        $industries_arr = array(
            1 => 'Hotels & Resorts',
            2 => 'Casino Resorts',
            3 => 'Tribal Gaming',
            4 => 'Cruise Lines',
            5 => 'Foodservice Management',
            6 => 'Sports & Entertainment',
            7 => 'Restaurants',
            8 => 'Spa',
            9 => 'Golf',
        );

        $products_arr = array(
            'PROPERTY MANAGEMENT' => array(
                1 => 'Agilysys Stay',
                2 => 'Agilysys LMS',
                3 => 'Agilysys Visual One PMS',
                4 => 'Agilysys Sales and Catering',
                5 => 'rGuest® Book',
                6 => 'rGuest® Express Kiosk',
                7 => 'rGuest® Express Mobile',
                8 => 'rGuest® Service',
                9 => 'b4checkin',
            ),

            'POINT OF SALE' => array(
                10 => 'Agilysys InfoGenesis',
                11 => 'IG Flex',
                12 => 'IG Buy',
                13 => 'IG OnDemand',
            ),
            'PAYMENT SOLUTION' => array(
                14 => 'Agilysys Pay',

            ),
            'ANALYTICS & MARKETING LOYALTY' => array(
                15 => 'Agilysys Analyze',
            ),
            'INVENTORY & PROCUREMENT' => array(
                16 => 'Agilysys Eatec',
                17 => 'Agilysys SWS',

            ),
            'RESERVATIONS AND TABLE MANAGEMENT' => array(
                18 => 'Agilysys Seat',
            ),
            'ACTIVITY SCHEDULING' => array(
                19 => 'Agilysys Golf',
                20 => 'Agilysys Spa',
            ),
            'DOCUMENT MANAGEMENT' => array(
                21 => 'Agilysys DataMagine',
            ),
            'SERVICES' => array(
                22 => 'Professional Services',
            ),

        );

        if (!empty($display_instance['what_makes_rows'])) {
            $what_makes_rows = ($display_instance['what_makes_rows']);
        } else {
            $what_makes_rows = 0;
        }

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('section_title1') . '"> ' . __('Slider Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('section_title1') . '" name="' . $this->get_field_name('section_title1') . '" type="text" value="' . $section_title1 . '" />';
        $rew_html .= '</p><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('what_makes_rows') . '"> ' . __('No. of rows do you want to add*', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input class="what_makes_rows" id="' . $this->get_field_name('what_makes_rows') . '" name="' . $this->get_field_name('what_makes_rows') . '" type="number" value="' . $what_makes_rows . '" />';
        $rew_html .= '</p><br><br>';

        $count = count($display_instance['section_title']);

        $rew_html .= '<div class="add_new_rowxx-input-containers"><div id="entries_agilysys_product_showcase_food_beverage_widget">';

        $rew_html .= '<input class="cnt909" id="cnt909" name="cnt" type="hidden" value="' . $count . '">';

        for ($i = 0; $i < $count; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'AGILYSYS_TEXT_DOMAIN') . ' </span>';

            $rew_html .= '<div class="entry-desc cf">';

            $rew_html .= '<p class="last">';
            $rew_html .= '<label for="' . $this->get_field_id('section_title' . $i) . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('section_title' . $i) . '" name="' . $this->get_field_name('section_title[]') . '" type="text" value="' . $display_instance['section_title'][$i] . '" />';
            $rew_html .= '</p>';

            $rew_html .= '<br><br><div class="widg-img' . $i . '">';
            $show1 = (empty($display_instance['webinars_info_front_image'][$i])) ? 'style="display:none;"' : '';
            $rew_html .= '<label id="image_uri_agilysys_product_showcase_food_beverage_widget' . $i . '"></label><br><img class="' . $this->get_field_id('webinars_info_front_image' . $i) . '_media_image' . $i . ' custom_media_image' . $i . '" src="' . $display_instance['webinars_info_front_image'][$i] . '" ' . $show1 . ' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('webinars_info_front_image' . $i) . '_media_id' . $i . ' custom_media_id' . $i . '" name="' . $this->get_field_name('webinars_info_front_image_id[]') . '" id="' . $this->get_field_id('webinars_info_front_image' . $i) . '" value="' . $display_instance['webinars_info_front_image'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('webinars_info_front_image' . $i) . '_media_url' . $i . ' custom_media_url' . $i . '" name="' . $this->get_field_name('webinars_info_front_image[]') . '" id="' . $this->get_field_id('webinars_info_front_image' . $i) . '" value="' . $display_instance['webinars_info_front_image'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_upload' . $i . '" id="' . $this->get_field_id('webinars_info_front_image' . $i) . '"/>';

            $rew_html .= '</div><br><br>';
            ?>

<script>
jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                   // if (attachment.height == 1080 && attachment.width == 1920) {
                        jQuery('.' + button_id_s + '_media_id<?php echo $i; ?>').val(attachment.id);
                        jQuery('.' + button_id_s + '_media_url<?php echo $i; ?>').val(attachment
                            .url);
                        jQuery('.' + button_id_s + '_media_image<?php echo $i; ?>').attr('src',
                            attachment.url).css('display', 'block');

                        /*jQuery(
                                '#image_uri_agilysys_product_showcase_food_beverage_widget<?php echo $i; ?>'
                            )
                            .html("");
                    } else {
                        jQuery(
                                '#image_uri_agilysys_product_showcase_food_beverage_widget<?php echo $i; ?>'
                            )
                            .html("Please Enter the correct Dimensions 1920x1080").css(
                                'color', 'red');

                    }
                    */
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload<?php echo $i; ?>');

});
</script>


<?php

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('webinars_info_front_image_alt') . '"> ' . __('Front Image Alt', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('webinars_info_front_image_alt') . '" name="' . $this->get_field_name('webinars_info_front_image_alt[]') . '" type="text" value="' . $display_instance['webinars_info_front_image_alt'][$i] . '" />';
            $rew_html .= '</p>';

            $rew_html .= '<br><br><div class="widg-img' . $i . '">';
            $show1 = (empty($display_instance['webinars_info_front_logo'][$i])) ? 'style="display:none;"' : '';
            $rew_html .= '<label id="image_urix_agilysys_product_showcase_food_beverage_widget' . $i . '"></label><br><img class="' . $this->get_field_id('webinars_info_front_logo' . $i) . '_media_imageyy' . $i . ' custom_media_imageyy' . $i . '" src="' . $display_instance['webinars_info_front_logo'][$i] . '" ' . $show1 . ' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('webinars_info_front_logo' . $i) . '_media_idyy' . $i . ' custom_media_idyy' . $i . '" name="' . $this->get_field_name('webinars_info_front_logo_id[]') . '" id="' . $this->get_field_id('webinars_info_front_logo' . $i) . '" value="' . $display_instance['webinars_info_front_logo'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('webinars_info_front_logo' . $i) . '_media_urlyy' . $i . ' custom_media_urlyy' . $i . '" name="' . $this->get_field_name('webinars_info_front_logo[]') . '" id="' . $this->get_field_id('webinars_info_front_logo' . $i) . '" value="' . $display_instance['webinars_info_front_logo'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload Logo" class="button custom_media_uploadyy' . $i . '" id="' . $this->get_field_id('webinars_info_front_logo' . $i) . '"/>';

            $rew_html .= '</div><br><br>';

            ?>

<script>
jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_uploadyy<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    //if (attachment.height == 1080 && attachment.width == 1920) {
                        jQuery('.' + button_id_s + '_media_idyy<?php echo $i; ?>').val(attachment
                            .id);
                        jQuery('.' + button_id_s + '_media_urlyy<?php echo $i; ?>').val(attachment
                            .url);
                        jQuery('.' + button_id_s + '_media_imageyy<?php echo $i; ?>').attr('src',
                            attachment.url).css('display', 'block');

                      /*  jQuery(
                                '#image_urix_agilysys_product_showcase_food_beverage_widget<?php echo $i; ?>'
                            )
                            .html("");
                    } else {
                        jQuery(
                                '#image_urix_agilysys_product_showcase_food_beverage_widget<?php echo $i; ?>'
                            )
                            .html("Please Enter the correct Dimensions 1920x1080").css(
                                'color', 'red');

                    }
                    */
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload<?php echo $i; ?>');

});
</script>






<?php

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('webinars_info_front_logo_alt') . '"> ' . __('Front Logo Alt', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('webinars_info_front_logo_alt') . '" name="' . $this->get_field_name('webinars_info_front_logo_alt[]') . '" type="text" value="' . $display_instance['webinars_info_front_logo_alt'][$i] . '" />';
            $rew_html .= '</p>';

            $webinars_info_desc = esc_attr($display_instance['webinars_info_desc'][$i]);
            $webinars_info_url_title = esc_attr($display_instance['webinars_info_url_title'][$i]);

            $rew_html .= '<p class="last">';
            $rew_html .= '<label for="' . $this->get_field_id('webinars_info_url_title' . $i) . '"> ' . __('Learn More Text', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('webinars_info_url_title' . $i) . '" name="' . $this->get_field_name('webinars_info_url_title[]') . '" type="text" value="' . $webinars_info_url_title . '" />';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('industries' . $i) . '"> ' . __('Industries', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('industries' . $i) . '" name="' . $this->get_field_name('industries[]') . '">';

            $selected_industry = $display_instance['industries'][$i];

            foreach ($industries_arr as $key => $val) {

                if ($key == $selected_industry) {
                    $rew_html .= '<option value="' . $key . '" selected="selected">' . $val . '</option>';
                } else {
                    $rew_html .= '<option value="' . $key . '">' . $val . '</option>';
                }
            }

            $rew_html .= '</select>';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('products' . $i) . '"> ' . __('Products', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('products' . $i) . '" name="' . $this->get_field_name('products[]') . '">';

            $selected_products = $display_instance['products'][$i];

            foreach ($products_arr as $key => $val) {

                if (is_array($val)) {

                    $rew_html .= '<optgroup label="' . $key . '">';

                    foreach ($val as $k => $v) {
                        if ($k == $selected_products) {
                            $rew_html .= '<option value="' . $k . '" selected="selected">' . $v . '</option>';
                        } else {
                            $rew_html .= '<option value="' . $k . '">' . $v . '</option>';
                        }
                    }

                    $rew_html .= '</optgroup>';

                }
            }

            $rew_html .= '</select>';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('video_type' . $i) . '"> ' . __('Video Type', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('video_type' . $i) . '" name="' . $this->get_field_name('video_type[]') . '" onChange="show_hide_video_agilysys_product_showcase_food_beverage_widget(this.value,' . $i . ');">';

            $selected_video_type = $display_instance['video_type'][$i];

            $video_type_arr = array(
                'youtube' => 'Youtube Url',
                'video' => 'Video Upload',
            );
            $rew_html .= '<option value="">Please Select</option>';
            foreach ($video_type_arr as $key => $val) {

                if ($key == $selected_video_type) {
                    $rew_html .= '<option value="' . $key . '" selected="selected">' . $val . '</option>';
                } else {
                    $rew_html .= '<option value="' . $key . '">' . $val . '</option>';
                }
            }

            $rew_html .= '</select>';
            $rew_html .= '</p>';

            $youtube_url = $display_instance['youtube_url'][$i];

            if ($selected_video_type == 'youtube') {
                $show1 = '';
                $show2 = 'style="display:none;"';
            } elseif ($selected_video_type == 'video') {
                $show1 = 'style="display:none;"';
                $show2 = '';
            } else {
                $show1 = 'style="display:none;"';
                $show2 = 'style="display:none;"';
            }
            $rew_html .= '<div id="youtube_div_' . $i . '" ' . $show1 . '><p>';
            $rew_html .= '<label for="' . $this->get_field_id('youtube_url' . $i) . '"> ' . __('Youtube url', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('youtube_url' . $i) . '" name="' . $this->get_field_name('youtube_url[]') . '" type="text" value="' . $youtube_url . '" />';
            $rew_html .= '</p></div>';

            $rew_html .= '<div id="video_div_' . $i . '" class="widg-video' . $i . '" ' . $show2 . '>';
            $showx = (empty($display_instance['video_uri'][$i])) ? 'style="display:none;"' : '';

            $rew_html .= '<br><br><div id="vid_div_class_' . $i . '"' . $showx . '><video class="' . $this->get_field_id('video_id' . $i) . '_media_imagex' . $i . ' custom_media_imagex' . $i . '" width="320" height="240" controls><source src="' . $display_instance['video_uri'][$i] . '"  type="video/mp4"></video></div>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('video_id' . $i) . '_media_idx' . $i . ' custom_media_idx' . $i . '" name="' . $this->get_field_name('video_id[]') . '" id="' . $this->get_field_id('video_id' . $i) . '" value="' . $display_instance['video_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('video_id' . $i) . '_media_urlx' . $i . ' custom_media_urlx' . $i . '" name="' . $this->get_field_name('video_uri[]') . '" id="' . $this->get_field_id('video_uri' . $i) . '" value="' . $display_instance['video_uri'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload Video" class="button custom_media_uploadx' . $i . '" id="' . $this->get_field_id('video_id' . $i) . '"/>';
            $rew_html .= '</div>';
            ?>



<script>
jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_uploadx<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {
                    jQuery('.' + button_id_s + '_media_idx<?php echo $i; ?>').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_urlx<?php echo $i; ?>').val(attachment.url);
                    jQuery('.' + button_id_s + '_media_imagex<?php echo $i; ?>').attr('src',
                        attachment.url).css('display', 'block');
                    jQuery('#vid_div_class_<?php echo $i; ?>').show();
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_uploadx<?php echo $i; ?>');

});
</script>


<?php

            $k = $i + 1;
            $rew_html .= '<br><br><p><a href="#delete"><span class="delete-row" onClick="delete_row(' . $k . ');">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';

        }
        $rew_html .= '</div></div>';

        $rew_html .= '<div class="' . $widget_add_id_webinars_info . '" style="margin-bottom: 36px;text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;" onClick="add_new_row();">' . __('ADD ROW', 'ZWREW_TEXT_DOMAIN') . '</div>';

        ?>
<script>
function show_hide_video_agilysys_product_showcase_food_beverage_widget(value, i) {

    if (value == 'youtube') {

        jQuery('#youtube_div_' + i).show();
        jQuery('#video_div_' + i).hide();

    } else if (value == 'video') {
        jQuery('#youtube_div_' + i).hide();
        jQuery('#video_div_' + i).show();
    }


}

function add_new_row() {
    var cnt = '';

    jQuery.each(jQuery("#entries_agilysys_product_showcase_food_beverage_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            cnt = jQuery(this).val();
        }
    });




    var what_makes_rows = jQuery('.what_makes_rows').val();



    if (parseInt(cnt) < parseInt(what_makes_rows)) {

        cnt++;

        jQuery.each(jQuery("#entries_agilysys_product_showcase_food_beverage_widget .cnt909"), function() {
            if (jQuery(this).val() != '') {
                jQuery(this).val(cnt);
            }
        });

        var new_row = '<div id="entry' + cnt +
            '"  class="entrys"><span class="entry-title" onclick = "slider(this);"><?php echo __('Add New Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span>';
        new_row += '<div class="entry-desc cf">';


        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Section Title', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('section_title[]')); ?>" type="text" value="">';
        new_row += '</p>';


        new_row += '<br><br><div class="widg-img' + cnt + '">';

        new_row += '<label id="image_uri_agilysys_product_showcase_food_beverage_widget' + cnt +
            '"></label><br><img class="<?php echo $this->get_field_id('webinars_info_front_image'); ?>' + cnt +
            '_media_image' +
            cnt + ' custom_media_image' + cnt + '" style="display:none;" width=200" height="120"/>';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('webinars_info_front_image'); ?>' + cnt +
            '_media_id' + cnt + ' custom_media_id' + cnt +
            '" name="<?php echo $this->get_field_name('webinars_info_front_image_id[]'); ?>"  />';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('webinars_info_front_image'); ?>' + cnt +
            '_media_url' + cnt + ' custom_media_url' + cnt +
            '" name="<?php echo $this->get_field_name('webinars_info_front_image[]'); ?>">';
        new_row += '<input type="button" value="Upload Image" class="button custom_media_upload' + cnt +
            '" id="<?php echo $this->get_field_id('webinars_info_front_image'); ?>' + cnt + '"/>';

        new_row += '</div><br><br>';


        jQuery(document).ready(function() {




            function media_upload(button_class) {
                var _custom_media = true,
                    _orig_send_attachment = wp.media.editor.send.attachment;
                jQuery('body').on('click', '.custom_media_upload' + cnt, function(e) {
                    var button_id = '#' + jQuery(this).attr('id');
                    var button_id_s = jQuery(this).attr('id');
                    console.log(button_id);
                    var self = jQuery(button_id);
                    var send_attachment_bkp = wp.media.editor.send.attachment;
                    var button = jQuery(button_id);
                    var id = button.attr('id').replace('_button', '');
                    _custom_media = true;

                    wp.media.editor.send.attachment = function(props, attachment) {
                        if (_custom_media) {

                           // if (attachment.height == 1080 && attachment.width == 1920) {
                           
                                jQuery('.' + button_id_s + '_media_id' + cnt).val(attachment.id);
                                jQuery('.' + button_id_s + '_media_url' + cnt).val(attachment.url);
                                jQuery('.' + button_id_s + '_media_image' + cnt).attr('src',
                                    attachment.url).css('display', 'block');
                                /*jQuery('#image_uri_agilysys_product_showcase_food_beverage_widget' +
                                        cnt)
                                    .html("");
                            } else {
                                jQuery('#image_uri_agilysys_product_showcase_food_beverage_widget' +
                                        cnt)
                                    .html("Please Enter the correct Dimensions 1920x1080").css(
                                        'color', 'red');

                            }
                            */


                        } else {
                            return _orig_send_attachment.apply(button_id, [props, attachment]);
                        }
                    }
                    wp.media.editor.open(button);
                    return false;
                });
            }
            media_upload('.custom_media_upload' + cnt);

        });


        new_row += '<br><br><div class="widg-img' + cnt + '">';
        new_row += '<label id="image_urix_agilysys_product_showcase_food_beverage_widget' + cnt +
            '"></label><br><img class="<?php echo $this->get_field_id('webinars_info_front_logo'); ?>' + cnt +
            '_media_imageyy' + cnt + ' custom_media_imageyy' + cnt +
            '" style="display:none;" width=200" height="120"/>';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('webinars_info_front_logo'); ?>' + cnt +
            '_media_idyy' + cnt + ' custom_media_idyy' + cnt +
            '" name="<?php echo $this->get_field_name('webinars_info_front_logo_id[]'); ?>"  />';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('webinars_info_front_logo'); ?>' + cnt +
            '_media_urlyy' + cnt + ' custom_media_urlyy' + cnt +
            '" name="<?php echo $this->get_field_name('webinars_info_front_logo[]'); ?>">';
        new_row += '<input type="button" value="Upload Logo" class="button custom_media_uploadyy' +
            cnt + '" id="<?php echo $this->get_field_id('webinars_info_front_logo'); ?>' + cnt + '"/>';

        new_row += '</div><br><br>';


        jQuery(document).ready(function() {




            function media_upload(button_class) {
                var _custom_media = true,
                    _orig_send_attachment = wp.media.editor.send.attachment;
                jQuery('body').on('click', '.custom_media_uploadyy' + cnt, function(e) {
                    var button_id = '#' + jQuery(this).attr('id');
                    var button_id_s = jQuery(this).attr('id');
                    console.log(button_id);
                    var self = jQuery(button_id);
                    var send_attachment_bkp = wp.media.editor.send.attachment;
                    var button = jQuery(button_id);
                    var id = button.attr('id').replace('_button', '');
                    _custom_media = true;

                    wp.media.editor.send.attachment = function(props, attachment) {
                        if (_custom_media) {

                           // if (attachment.height == 1080 && attachment.width == 1920) {
                                jQuery('.' + button_id_s + '_media_idyy' + cnt).val(attachment.id);
                                jQuery('.' + button_id_s + '_media_urlyy' + cnt).val(attachment
                                    .url);
                                jQuery('.' + button_id_s + '_media_imageyy' + cnt).attr('src',
                                    attachment.url).css('display', 'block');

                               /* jQuery('#image_urix_agilysys_product_showcase_food_beverage_widget' +
                                        cnt)
                                    .html("");
                            } else {
                                jQuery('#image_urix_agilysys_product_showcase_food_beverage_widget' +
                                        cnt)
                                    .html("Please Enter the correct Dimensions 1920x1080").css(
                                        'color', 'red');

                            }
                            */


                        } else {
                            return _orig_send_attachment.apply(button_id, [props, attachment]);
                        }
                    }
                    wp.media.editor.open(button);
                    return false;
                });
            }
            media_upload('.custom_media_upload' + cnt);

        });

        new_row += '<p class="last">';
        new_row += '<label><?php echo __('Learn More Text', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
        new_row += '<input name="<?php echo $this->get_field_name('webinars_info_url_title[]'); ?>" type="text"  />';
        new_row += '</p>';

        new_row += '<p>';
        new_row += '<label><?php echo __('Industries', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
        new_row += '<select name="<?php echo $this->get_field_name('industries[]'); ?>">';


        <?php
foreach ($industries_arr as $key => $val) {
            ?>

        new_row += '<option value="<?php echo $key; ?>"><?php echo $val; ?></option>';

        <?php
}
        ?>

        new_row += '</select>';
        new_row += '</p>';

        new_row += '<p>';
        new_row += '<label ><?php echo __('Products', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
        new_row += '<select name="<?php echo $this->get_field_name('products[]'); ?>">';

        <?php

        foreach ($products_arr as $key => $val) {

            if (is_array($val)) {
                ?>

        new_row += '<optgroup label="<?php echo $key; ?>">';
        <?php
foreach ($val as $k => $v) {
                    ?>
        new_row += '<option value="<?php echo $k; ?>"><?php echo $v; ?></option>';


        <?php
}
                ?>
        new_row += '</optgroup>';
        <?php
}

        }
        ?>
        new_row += '</select>';
        new_row += '</p>';


        new_row += '<p>';
        new_row += '<label> <?php echo __('Video Type', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
        new_row +=
            '<select name="<?php echo $this->get_field_name('video_type[]'); ?>" onChange="show_hide_video_agilysys_product_showcase_food_beverage_widget(this.value,' +
            cnt + ');">';

        <?php

        $video_type_arr = array(
            'youtube' => 'Youtube Url',
            'video' => 'Video Upload',
        );

        ?>
        new_row += '<option value="">Please Select</option>';

        <?php
foreach ($video_type_arr as $key => $val) {
            ?>

        new_row += '<option value="<?php echo $key; ?>"><?php echo $val; ?></option>';

        <?php
}

        ?>

        new_row += '</select>';
        new_row += '</p>';


        new_row += '<div id="youtube_div_' + cnt + '" style="display:none;"><p>';
        new_row += '<label><?php echo __('Youtube url', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
        new_row += '<input name="<?php echo $this->get_field_name('youtube_url[]'); ?>" type="text" />';
        new_row += '</p></div>';

        new_row += '<br><br><div id="video_div_' + cnt + '" class="widg-video' + cnt + '" style="display:none;">';


        new_row += '<div id="vid_div_class_' + cnt +
            '" style="display:none;"><video class="<?php echo $this->get_field_id('video_id'); ?>' + cnt +
            '_media_imagex' + cnt + ' custom_media_imagex' + cnt +
            '" width="320" height="240" controls><source src="" type="video/mp4"></video></div>';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('video_id'); ?>' + cnt + '_media_idx' +
            cnt + ' custom_media_idx' + cnt + '" name="<?php echo $this->get_field_name('video_id[]'); ?>"   />';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('video_id'); ?>' + cnt + '_media_urlx' +
            cnt + ' custom_media_urlx' + cnt + '" name="<?php echo $this->get_field_name('video_uri[]'); ?>">';
        new_row += '<input type="button" value="Upload Video" class="button custom_media_uploadx' + cnt +
            '" id="<?php echo $this->get_field_id('video_id'); ?>' + cnt + '"/>';
        new_row += '</div>';


        jQuery(document).ready(function() {




            function media_upload(button_class) {
                var _custom_media = true,
                    _orig_send_attachment = wp.media.editor.send.attachment;
                jQuery('body').on('click', '.custom_media_uploadx' + cnt, function(e) {
                    var button_id = '#' + jQuery(this).attr('id');
                    var button_id_s = jQuery(this).attr('id');
                    console.log(button_id);
                    var self = jQuery(button_id);
                    var send_attachment_bkp = wp.media.editor.send.attachment;
                    var button = jQuery(button_id);
                    var id = button.attr('id').replace('_button', '');
                    _custom_media = true;

                    wp.media.editor.send.attachment = function(props, attachment) {
                        if (_custom_media) {
                            jQuery('.' + button_id_s + '_media_idx' + cnt).val(attachment.id);
                            jQuery('.' + button_id_s + '_media_urlx' + cnt).val(attachment.url);
                            jQuery('.' + button_id_s + '_media_imagex' + cnt).attr('src',
                                attachment.url).css('display', 'block');
                            jQuery('#vid_div_class_' + cnt).show();
                        } else {
                            return _orig_send_attachment.apply(button_id, [props, attachment]);
                        }
                    }
                    wp.media.editor.open(button);
                    return false;
                });
            }
            media_upload('.custom_media_uploadx' + cnt);

        });


        var new_cnt = cnt;

        new_row += '<br><br><p><a href="#delete"><span class="delete-row" onClick="delete_row(' + new_cnt +
            ');"><?php echo __('Delete Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span></a></p>';
        new_row += '</div></div>';

        jQuery('.add_new_rowxx-input-containers #entries_agilysys_product_showcase_food_beverage_widget').append(new_row);

    }


}



function delete_row(cnt) {
    jQuery.each(jQuery(".add_new_rowxx-input-containers #entries_agilysys_product_showcase_food_beverage_widget"), function() {
        jQuery(' #entry' + cnt).remove();
    });
    var last_cnt = 0;

    jQuery.each(jQuery("#entries_agilysys_product_showcase_food_beverage_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            last_cnt = jQuery(this).val();
        }
    });

    last_cnt--;
    jQuery.each(jQuery("#entries_agilysys_product_showcase_food_beverage_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(last_cnt);
        }
    });
    jQuery(".what_makes_rows").val(last_cnt);
    jQuery('.what_makes_rows').trigger('change');

}
</script>
<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}



#rew_container_agilysys_product_showcase_food_beverage_widget select {
    float: left;
    width: 60%;
    margin-top: 20px !important;
    margin-bottom: 10px !important;
}

#rew_container_agilysys_product_showcase_food_beverage_widget input,

textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_product_showcase_food_beverage_widget label {
    width: 40%;
    float: left;
}

#rew_container_agilysys_product_showcase_food_beverage_widget p {
    padding:20px;
}


<?php echo '.' . $widget_add_id_webinars_info;

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}

#entries_agilysys_product_showcase_food_beverage_widget #remove-img-bg {
    background: url('<?php echo ZWREW_URL; ?>assets/images/deleteimg.png') center center no-repeat;
    width: 20px;
    height: 22px;
    display: none;
}

#entries_agilysys_product_showcase_food_beverage_widget {
    padding: 10px 0 0;
}

#entries_agilysys_product_showcase_food_beverage_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_product_showcase_food_beverage_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_product_showcase_food_beverage_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_product_showcase_food_beverage_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_product_showcase_food_beverage_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_product_showcase_food_beverage_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_product_showcase_food_beverage_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_product_showcase_food_beverage_widget #entries_agilysys_product_showcase_food_beverage_widget plast label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_product_showcase_food_beverage_widget">
    <?php echo $rew_html; ?>
</div>
<?php
}
}
<?php

function load_agilysys_text_editor_widget()
{
    register_widget('agilysys_text_editor_widget');
}

add_action('widgets_init', 'load_agilysys_text_editor_widget');

class agilysys_text_editor_widget extends WP_Widget
{
    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Text Editor Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_editor();
        wp_enqueue_media();

    }

    public function widget($args, $instance)
    {

        echo $args['before_widget'];
        ?>


<style>
.mt {
    margin-top: 5%;
    margin-bottom: 5%;
}
</style>


<section class="mt">
    <div class="container" id="container">

        <?php
echo "<div class='aboutCareerMainContent'><h3>" . $instance['title'] . "</h3></div>";

        echo '<br><br>';

        $tags = '<img>,<br>,<span>,<p>,<h1>,<h2>,<h3>,<h4>,<h5>,<h6>,<strong>,<em>,<abbr>,<acronym>,<address>,<bdo>,<blockquote>,<cite>,<q>,<code>,<ins>,<del>,<dfn>,<kbd>,<pre>,<samp>,<var>,<a>,<base>,<img>,<area>,<map>,<param>,<object>,<ul>,<ol>,<li>,<dl>,<dt>,<dd>';

        echo strip_tags($instance['editor_content999'], $tags);
        ?>
    </div>
</section>


<?php

        echo $args['after_widget'];
    }

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {
        //return $new_instance;

        $instance = array();

        $instance['title'] = $new_instance['title'];
        $instance['editor_content999'] = $new_instance['editor_content999'];

        return $instance;
    }

    public function form($display_instance)
    {

        ?>
<style>
#rew_container input {
    width: 4%;
}

.title {
    width: 60% !important;
}
</style>
<?php

        $rew_html = '';

        $content = $display_instance['editor_content999'];

        $ed_id = $this->get_field_id('editor_content999');
        $ed_name = $this->get_field_name('editor_content999');

        $title = $display_instance['title'];

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('title') . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input class="title" id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" type="text" value="' . $title . '" />';
        $rew_html .= '</p><br><br>';

        ?>



<!--<script src="<?php //echo bloginfo('home') ?>/wp-includes/js/quicktags.min.js"></script>-->
<script>
/*jQuery(document).ready(function() {

    ed_id = "<?php //echo $ed_id; ?>";


    quicktags({
        id: ed_id,
        buttons: "strong,em,link,block,del,ins,img,ul,ol,li,code,more,close,dfw"
    });


    tinyMCE.execCommand('mceAddEditor', false, ed_id);


});
*/


jQuery(document).on("click", ".button-primary", function() {
    jQuery("input").trigger('change');
    jQuery('.switch-html').trigger('click');
    jQuery('.switch-tmce').trigger('click');


});


jQuery(function($) {
    // Was needed a timeout since RTE is not initialized when this code run.
    setTimeout(function() {
        for (var i = 0; i < tinyMCE.editors.length; i++) {
            tinyMCE.editors[i].onChange.add(function(ed, e) {
                // Update HTML view textarea (that is the one used to send the data to server).
                ed.save();
            });
        }
    }, 1000);
});
</script>






<div id="rew_container">
    <?php

        echo $rew_html;

        $rand = rand(0, 9999);

        ?>

    <textarea id="basic-example<?php echo $rand; ?>" name="<?php echo $ed_name; ?>" style="width:100%;border:none;">
            <?php echo $content; ?>

        </textarea>
</div>

<script>
jQuery(function($) {
    var editor_id = 'basic-example<?php echo $rand; ?>';
    wp.oldEditor.initialize(
        editor_id, {
            tinymce: {
                wpautop: true,
                plugins: 'charmap colorpicker compat3x directionality fullscreen hr image lists media paste tabfocus textcolor wordpress wpautoresize wpdialogs wpeditimage wpemoji wpgallery wplink wptextpattern wpview',
                toolbar1: 'undo redo | formatselect | bold italic underline strikethrough | bullist numlist | blockquote hr wp_more | alignleft aligncenter alignright | link unlink | fullscreen | wp_adv',
                toolbar2: 'formatselect alignjustify forecolor | pastetext removeformat charmap | outdent indent | undo redo | wp_help'
            },
            quicktags: true,
            mediaButtons: true,
        }
    );
});
</script>


<?php

    }

}

?>
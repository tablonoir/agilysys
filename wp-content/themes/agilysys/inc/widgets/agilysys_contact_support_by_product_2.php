<?php

function agilysys_contact_support_by_product_2s() {
    register_widget( 'agilysys_contact_support_by_product_2' );
}
add_action( 'widgets_init', 'agilysys_contact_support_by_product_2s' );


class agilysys_contact_support_by_product_2 extends WP_Widget {
 

function __construct() {
 parent::__construct (false, $name = __( 'Agilysys Contact Support By Product Widget 2', 'AGILYSYS_TEXT_DOMAIN' ) );
	
 wp_enqueue_media();         
 wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'),'null', true );
 wp_enqueue_script ('add-sd-js');
}
  

function widget( $args, $instance ) {
    
    $section_title = $instance['section_title'];
    $title2 = $instance['title2'];
    $title3 = $instance['title3'];
    $learn_more_text = $instance['learn_more_text'];
    $email_id_1= $instance['email_id_1'];
    $email_id_2= $instance['email_id_2'];

 

 ?>


<section class="contactSupportMap">
    <div class="contactMap flex">
        <div class="contactMapBox">
            <h4 class="dinProStd greenText"><?php echo $section_title; ?>™</h4>
            <h5 class="dinproBld blackText"><?php echo $title2; ?></h5>
            <a href="mailto:<?php echo $email_id_1; ?>"><?php echo $email_id_1; ?></a>
            <h5 class="dinproBld blackText"><?php echo $title3; ?></h5>
            <a href="mailto:<?php echo $email_id_2; ?>"><?php echo $email_id_2; ?></a>
        </div>
        </div>
    
</section>


<?php
    
 
}

function update( $new_instance, $old_instance ) {
 $instance = array();
 
 
 $instance['section_title'] = strip_tags($new_instance['section_title']);
 $instance['contact_support_rows'] = $new_instance['contact_support_rows'];
 
 $instance['title2'] = $new_instance['title2'];
 $instance['title3'] = $new_instance['title3'];
 $instance['email_id_2'] = $new_instance['email_id_2'];
 $instance['email_id_1'] = $new_instance['email_id_1'];
 
 

 
 return $instance;
}
 
          

function form( $display_instance ) {
    
 $max_entries_slider_image = 15;
        
                $widget_add_id_slider = $this->get_field_id('') . "add";
        
		$section_title   = ($display_instance['section_title']);
                $title2   = ($display_instance['title2']);
                $title3   = ($display_instance['title3']);
                
                $email_id_2 = ($display_instance['email_id_2']);
                $email_id_1 = ($display_instance['email_id_1']);                
                
                $link   = ($display_instance['link']);
                
		$contact_support_rows   = ($display_instance['contact_support_rows']);
		$rew_html = '<p>';
		$rew_html .= '<label for="'.$this->get_field_id('section_title').'"> '. __( 'Main Title', 'AGILYSYS_TEXT_DOMAIN' ) .' :</label>';
		$rew_html .= '<input id="'.$this->get_field_id('section_title').'" name="'.$this->get_field_name('section_title').'" type="text" value="'.$section_title.'" />';
		$rew_html .='</p>'; 
                
                $rew_html .= '<p>';
		$rew_html .= '<label for="'.$this->get_field_id('title2').'"> '. __( 'Country Name 1', 'AGILYSYS_TEXT_DOMAIN' ) .' :</label>';
		$rew_html .= '<input id="'.$this->get_field_id('title2').'" name="'.$this->get_field_name('title2').'" type="text" value="'.$title2.'" />';
		$rew_html .='</p>';
                
                     $rew_html .= '<p>';
		$rew_html .= '<label for="'.$this->get_field_id('email_id_1').'"> '. __( 'Mobile Number 1', 'AGILYSYS_TEXT_DOMAIN' ) .' :</label>';
		$rew_html .= '<input id="'.$this->get_field_id('email_id_1').'" name="'.$this->get_field_name('email_id_1').'" type="text" value="'.$email_id_1.'" />';
		$rew_html .='</p>';
                
              
                $rew_html .= '<p>';
		$rew_html .= '<label for="'.$this->get_field_id('title3').'"> '. __( 'Country Name 2', 'AGILYSYS_TEXT_DOMAIN' ) .' :</label>';
		$rew_html .= '<input id="'.$this->get_field_id('title3').'" name="'.$this->get_field_name('title3').'" type="text" value="'.$title3.'" />';
		$rew_html .='</p>';
                
           
                  $rew_html .= '<p>';
		$rew_html .= '<label for="'.$this->get_field_id('email_id_2').'"> '. __( 'Mobile Number 2', 'AGILYSYS_TEXT_DOMAIN' ) .' :</label>';
		$rew_html .= '<input id="'.$this->get_field_id('email_id_2').'" name="'.$this->get_field_name('email_id_2').'" type="text" value="'.$email_id_2.'" />';
		$rew_html .='</p>';
                

                   ?>
	
<style>
			.cf:before, .cf:after { content: ""; display: table; }
			.cf:after { clear: both; }
			.cf { zoom: 1; }
			.clear { clear: both; }
			.clearfix:after { content: "."; display: block; height: 0; clear: both; visibility: hidden; }
			.clearfix { display: inline-block; }
			* html .clearfix { height: 1%; }
			.clearfix { display: block;}

			#rew_container input,select,textarea{ float: right;width: 60%;}
			#rew_container label{width:40%;}
			<?php echo '.'.$widget_add_id_slider; ?>{
			background: #ccc none repeat scroll 0 0;font-weight: bold;margin: 20px 0px 9px;padding: 6px;text-align: center;display:block !important; cursor:pointer;
			}
			.block-image{width:50px; height:30px; float: right; display:none;}
			.desc{height:55px;}


                        
			#entries{ padding:10px 0 0;}
			#entries .entrys{ padding:0; border:1px solid #e5e5e5; margin:10px 0 0; clear:both;}
			#entries .entrys:first-child{ margin:0;}
			#entries .delete-row{margin-top:20px;float:right;text-decoration: underline;color:red;}
			#entries .entry-title{ display:block; font-size:14px; line-height:18px; font-weight:600; background:#f1f1f1; padding:7px 5px; position:relative;}
			#entries .entry-title:after{ content: '\f140'; font: 400 20px/1 dashicons; position:absolute; right:10px; top:6px; color:#a0a5aa;}
			#entries .entry-title.active:after{ content: '\f142';}
			#entries .entry-desc{ display:none; padding:0 10px 10px; border-top:1px solid #e5e5e5;}
			#rew_container #entries p.last label{ white-space: pre-line; float:left; width:39%;}
			#message{padding:6px;display:none;color:red;font-weight:bold;}
		</style>
		<div id="rew_container">
		  <?php echo $rew_html;?>
		</div>


<?php
            
              
 
}
      



} 


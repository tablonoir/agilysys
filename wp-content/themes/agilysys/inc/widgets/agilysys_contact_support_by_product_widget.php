<?php

function load_agilysys_contact_support_by_product_widget()
{
    register_widget('agilysys_contact_support_by_product_widget');
}
add_action('widgets_init', 'load_agilysys_contact_support_by_product_widget');

class agilysys_contact_support_by_product_widget extends WP_Widget
{

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Contact Support By Product Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        $section_title = $instance['section_title'];
        $contact_support_rows = $instance['contact_support_rows'];

        $title2 = $instance['title2'];
        $title3 = $instance['title3'];
        $learn_more_text = $instance['learn_more_text'];
        $link_type = $instance['link_type'];
        if ($link_type == "link") {
            $link = $instance['link'];
        } elseif ($link_type == "page") {
            $post_id = $instance['page'];
            $post = get_post($post_id);
            $link = home_url($post->post_name) . "/";
        }

        $arr = array();

        $count = count($instance['country_name']);

        for ($i = 0; $i < $count; $i++) {
            $country_name = $instance['country_name'][$i];
            $contact_number = $instance['contact_number'][$i];

            $arr1 = array();

            if ($country_name != "" && $contact_number != "") {
                $arr1['country_name'] = $country_name;
                $arr1['contact_number'] = $contact_number;
                array_push($arr, $arr1);
            }

        }

        ?>


<section class="contactSupport center">
    <h2 class="dinProStd greenText"><?php echo $section_title; ?></h2>
    <h4 class="dinProStd blackText"><?php echo $title2; ?></h4>



    <?php
$cnt = 0;
        foreach ($arr as $key) {
            if ($cnt == 0) {
                $country_name = $key['country_name'];
                $contact_number = $key['contact_number'];

                ?>

    <p class="blackText"><?php echo $country_name; ?>:
        <a class="contactSupportNumber" href="tel:<?php echo $contact_number; ?>"><?php echo $contact_number; ?></a></p>

    <?php
$cnt++;
            } else {
                if ($cnt % 3 == 1) {
                    ?>

    <div class="flex">

        <?php
}

                $country_name = $key['country_name'];
                $contact_number = $key['contact_number'];

                ?>
        <p class="blackText"><?php echo $country_name; ?>: <a class="contactSupportNumber"
                href="tel:<?php echo $contact_number; ?>"><?php echo $contact_number; ?></a></p>




        <?php

                if ($cnt + 1 == count($arr) || $cnt % 3 == 0) {
                    ?>
    </div>
    <?php
}

                $cnt++;
                ?>




    <?php
}

        }

        ?>

    <h4 class="dinProStd blackText"><?php echo $title3; ?></h4>
    <a class="homeOurButton" href="<?php echo $link; ?>"><?php echo $learn_more_text; ?> <i class="fa fa-arrow-right"
            aria-hidden="true"></i></a>
</section>


<?php
echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['section_title'] = strip_tags($new_instance['section_title']);
        $instance['contact_support_rows'] = $new_instance['contact_support_rows'];

        $instance['title2'] = $new_instance['title2'];
        $instance['title3'] = $new_instance['title3'];
        $instance['learn_more_text'] = $new_instance['learn_more_text'];
        $instance['link_type'] = $new_instance['link_type'];
        if ($new_instance['link_type'] == 'page') {
            $instance['page'] = $new_instance['page'];
            $instance['link'] = '';
        } elseif ($new_instance['link_type'] == 'link') {
            $instance['link'] = $new_instance['link'];
            $instance['page'] = '';

        }

        $count = count($new_instance['country_name']);

        for ($i = 0; $i < $count; $i++) {

            $instance['country_name'][$i] = $new_instance['country_name'][$i];
            $instance['contact_number'][$i] = $new_instance['contact_number'][$i];

        }

        return $instance;
    }

    public function form($display_instance)
    {

        $max_entries_agilysys_contact_support_by_product_widget_slider_image = 15;

        $widget_add_id_slider = $this->get_field_id('') . "add";

        $section_title = ($display_instance['section_title']);
        $title2 = ($display_instance['title2']);
        $title3 = ($display_instance['title3']);
        $learn_more_text = ($display_instance['learn_more_text']);
        $link = ($display_instance['link']);

        if (!empty($display_instance['contact_support_rows'])) {
            $contact_support_rows = ($display_instance['contact_support_rows']);
        } else {
            $contact_support_rows = 0;
        }

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('section_title') . '"> ' . __('Section Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('section_title') . '" name="' . $this->get_field_name('section_title') . '" type="text" value="' . $section_title . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('title2') . '"> ' . __('Title 2', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('title2') . '" name="' . $this->get_field_name('title2') . '" type="text" value="' . $title2 . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('title3') . '"> ' . __('Title 3', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('title3') . '" name="' . $this->get_field_name('title3') . '" type="text" value="' . $title3 . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('learn_more_text') . '"> ' . __('Learn More Text', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('learn_more_text') . '" name="' . $this->get_field_name('learn_more_text') . '" type="text" value="' . $learn_more_text . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('link_type') . '"> ' . __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<select id="' . $this->get_field_id('link_type') . '" name="' . $this->get_field_name('link_type') . '" onChange="show_hide_div_agilysys_contact_support_by_product_widget(this.value);">';
        $rew_html .= '<option value="">Please Select</option>';

        $link_type = $display_instance['link_type'];

        if ($link_type == 'page') {
            $rew_html .= '<option value="page" selected="selected">Internal Page Link</option>';
        } else {
            $rew_html .= '<option value="page">Internal Page Link</option>';
        }

        if ($link_type == 'link') {
            $rew_html .= '<option value="link" selected="selected">External Link</option>';
        } else {
            $rew_html .= '<option value="link">External Link</option>';
        }

        $rew_html .= '</select>';
        $rew_html .= '</p><br><br>';

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block"';
            $show2 = 'style="display:none"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none"';
            $show2 = 'style="display:block"';

        } else {
            $show1 = 'style="display:none"';
            $show2 = 'style="display:none"';
        }
        $rew_html .= '<div id="page_div" ' . $show1 . '><p>';
        $rew_html .= '<label for="' . $this->get_field_id('page') . '"> ' . __('Page', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<select id="' . $this->get_field_id('page') . '" name="' . $this->get_field_name('page') . '">';
        $rew_html .= '<option value="">Please Select</option>';

        $page = $display_instance['page'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                $rew_html .= '<option value="' . $key->ID . '" selected="selected">' . $key->post_title . '</option>';
            } else {
                $rew_html .= '<option value="' . $key->ID . '">' . $key->post_title . '</option>';
            }

        }

        $rew_html .= '</select>';
        $rew_html .= '</p></div><br><br>';

        $rew_html .= '<div id="link_div" ' . $show2 . '><p>';
        $rew_html .= '<label for="' . $this->get_field_id('link') . '"> ' . __('Link', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('link') . '" name="' . $this->get_field_name('link') . '" type="text" value="' . $link . '" />';
        $rew_html .= '</p></div><br><br>';
        ?>
<script>
function show_hide_div_agilysys_contact_support_by_product_widget(val) {
console.log(val);
    if (val == 'page') {
        jQuery("#page_div_agilysys_contact_support_by_product_widget").show();
        jQuery("#link_div_agilysys_contact_support_by_product_widget").hide();
    } else if (val == 'link') {
        jQuery("#page_div_agilysys_contact_support_by_product_widget").hide();
        jQuery("#link_div_agilysys_contact_support_by_product_widget").show();
    }

}
</script>

<?php

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('contact_support_rows') . '"> ' . __('No. of rows do you want to add*', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input class="contact_support_rows" id="' . $this->get_field_name('contact_support_rows') . '" name="' . $this->get_field_name('contact_support_rows') . '" type="number" value="' . $contact_support_rows . '" />';
        $rew_html .= '</p><br><br>';

        $count = count($display_instance['country_name']);

        $rew_html .= '<div class="add_new_rowxx-input-containers"><div id="entries_agilysys_contact_support_by_product_widget">';

        $rew_html .= '<input class="cnt909" id="cnt909" name="cnt" type="hidden" value="' . $count . '">';

        for ($i = 0; $i < $count; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'AGILYSYS_TEXT_DOMAIN') . ' </span>';

            $rew_html .= '<div class="entry-desc cf">';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('country_name' . $i) . '"> ' . __('Country Name', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('country_name' . $i) . '" name="' . $this->get_field_name('country_name[]') . '" type="text" value="' . $display_instance['country_name'][$i] . '">';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('contact_number' . $i) . '"> ' . __('Contact Number', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('contact_number' . $i) . '" name="' . $this->get_field_name('contact_number[]') . '" type="text" value="' . $display_instance['contact_number'][$i] . '">';
            $rew_html .= '</p><br><br>';

            $k = $i + 1;
            $rew_html .= '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' . $k . ');">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';
        }

        $rew_html .= '</div></div>';
        $rew_html .= '<div id="message">' . __('Sorry, you reached to the limit of', 'AGILYSYS_TEXT_DOMAIN') . ' "' . $contact_support_rows . '" ' . __('maximum entries_agilysys_contact_support_by_product_widget', 'AGILYSYS_TEXT_DOMAIN') . '.</div>';

        $rew_html .= '<div class="' . $widget_add_id_slider . '" style="text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;" onClick="add_new_row();">' . __('ADD ROW', 'AGILYSYS_TEXT_DOMAIN') . '</div>';
        ?>
<script>
function add_new_row() {
    var cnt = '';

    jQuery.each(jQuery("#entries_agilysys_contact_support_by_product_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            cnt = jQuery(this).val();
        }
    });




    var contact_support_rows = jQuery('.contact_support_rows').val();



console.log(cnt);

 if (parseInt(cnt) < parseInt(contact_support_rows)) {


        cnt++;

        jQuery.each(jQuery("#entries_agilysys_contact_support_by_product_widget .cnt909"), function() {
            if (jQuery(this).val() != '') {
                jQuery(this).val(cnt);
            }
        });

        var new_row = '<div id="entry' + cnt +
            '"  class="entrys"><span class="entry-title" onclick = "slider(this);"><?php echo __('Add New Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span>';
        new_row += '<div class="entry-desc cf">';


        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Country Name', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('country_name[]')); ?>" type="text" value="">';
        new_row += '</p>';


        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Contact Number', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('contact_number[]')); ?>" type="text" value="">';
        new_row += '</p><br><br>';



        var new_cnt = cnt;

        new_row +=
            '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' + new_cnt +
            ');"><?php echo __('Delete Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span></a></p>';
        new_row += '</div></div>';

        jQuery('.add_new_rowxx-input-containers #entries_agilysys_contact_support_by_product_widget').append(new_row);

    }


}



function delete_row(cnt) {
    jQuery.each(jQuery(".add_new_rowxx-input-containers #entries_agilysys_contact_support_by_product_widget"), function() {
        jQuery(' #entry' + cnt).remove();
    });
    var last_cnt = 0;

    jQuery.each(jQuery("#entries_agilysys_contact_support_by_product_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            last_cnt = jQuery(this).val();
        }
    });

    last_cnt--;
    jQuery.each(jQuery("#entries_agilysys_contact_support_by_product_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(last_cnt);
        }
    });
    jQuery(".contact_support_rows").val(last_cnt);
    jQuery('.contact_support_rows').trigger('change');

}
</script>

<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_contact_support_by_product_widget  select{
     float: left;
    width: 60%;
    margin-top:20px !important;
    margin-bottom:10px !important;
}

#rew_container_agilysys_contact_support_by_product_widget input,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_contact_support_by_product_widget p {
  padding:10px !important;
}

#rew_container_agilysys_contact_support_by_product_widget label {
    width: 40%;
     float: left;
}

<?php echo '.'. $widget_add_id_slider;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}



#entries_agilysys_contact_support_by_product_widget {
    padding: 10px 0 0;
}

#entries_agilysys_contact_support_by_product_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_contact_support_by_product_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_contact_support_by_product_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_contact_support_by_product_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_contact_support_by_product_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_contact_support_by_product_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_contact_support_by_product_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_contact_support_by_product_widget #entries_agilysys_contact_support_by_product_widget p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_contact_support_by_product_widget">
    <?php echo $rew_html; ?>
</div>


<?php

    }

}
<?php

add_action('widgets_init', 'agil_load_pos_titlebox');

function agil_load_pos_titlebox()
{
    register_widget('agilysys_pos_titlebox_widget');
}

class agilysys_pos_titlebox_widget extends WP_Widget
{

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Pos Title Box Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');

    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];

        $title = $instance['title'];
        $heading = $instance['heading'];
        $desc = $instance['desc'];
        ?>

<section class="posTitle center">
    <h2 class="dinProStd greenText"><?php echo $heading; ?></h2>
</section>


<section class="posTitleBox waterBG">
    <div class="row">
        <div class="posTitleBoxOne col-12 col-sm-12 col-md-6 col-lg-6">
            <h2 class="dinProStd whiteText"><?php echo $title; ?></h2>
        </div>
        <div class="posTitleBoxTwo col-12 col-sm-12 col-md-6 col-lg-6">
            <p class="whiteText"><?php echo $desc; ?></p>
        </div>
    </div>
</section>

<?php
echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {

        $instance = array();

        $instance['title'] = strip_tags($new_instance['title']);
        $instance['desc'] = strip_tags($new_instance['desc']);
        $instance['heading'] = strip_tags($new_instance['heading']);
        return $instance;
    }

    public function form($display_instance)
    {
        $title = ($display_instance['title']);
        $desc = ($display_instance['desc']);
        $heading = ($display_instance['heading']);

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('heading') . '"> ' . __('Heading', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('heading') . '" name="' . $this->get_field_name('heading') . '" type="text" value="' . $heading . '" />';
        $rew_html .= '</p><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('title') . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" type="text" value="' . $title . '" />';
        $rew_html .= '</p><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('desc') . '"> ' . __('Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<textarea id="' . $this->get_field_id('desc') . '" name="' . $this->get_field_name('desc') . '">' . $desc . '</textarea>';
        $rew_html .= '</p>';

        ?>


<style>
#rew_container_agilysys_pos_titlebox_widget p {
    padding: 10px;
}

.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}


#rew_container_agilysys_pos_titlebox_widget select {
    float: left;
    width: 60%;
    margin-top: 20px !important;
    margin-bottom: 10px !important;
}

#rew_container_agilysys_pos_titlebox_widget input,

textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_pos_titlebox_widget label {
    width: 40%;
    float: left;
}

<?php echo '.'. $widget_add_id_slider;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}



#entries_agilysys_pos_titlebox_widget {
    padding: 10px 0 0;
}

#entries_agilysys_pos_titlebox_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_pos_titlebox_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_pos_titlebox_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_pos_titlebox_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_pos_titlebox_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_pos_titlebox_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_pos_titlebox_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_pos_titlebox_widget #entries_agilysys_pos_titlebox_widget p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_pos_titlebox_widget">
    <?php echo $rew_html; ?>
</div>

<?php

    }
}
<?php

function agilysys_Slides_widgets()
{
    register_widget('agilysys_Slide_widgetss');
}

add_action('widgets_init', 'agilysys_Slides_widgets');

class agilysys_Slide_widgetss extends WP_Widget
{

    /**
     * constructor -- name this the same as the class above
     */

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Contactless Guest Engament Slider Widget', ZWREW_TEXT_DOMAIN));

        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    /**
     * @see WP_Widget::widget -- do not rename this
     * This is for front end
     */
    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        extract($args);
        $max_entries_slider_image = esc_attr(get_option('rew_max'));
        $max_entries_slider_image = (empty($max_entries_slider_image)) ? '5' : $max_entries_slider_image;
        $link_target = get_option('rew_link_target');
        $alignment = esc_attr(get_option('content_align'));?>


<section class="homeSolutionSlider">
    <h2 class="greenText dinProStd" data-aos="fade-right" data-aos-delay="300"  data-aos-duration="400" data-aos-once="true"><?php echo $instance['slider_title'] ?></h2>

    <div class="swiper-container">
        <div class="swiper-wrapper">

               <?php

        for ($i = 0; $i < $max_entries_slider_image; $i++) {
            $block = $instance['block-' . $i];
            if (isset($block) && $block != "") {
                //Caption
                $caption = esc_attr($instance['sldier_caption-' . $i]);

                //Slider Image
                $image_url_slider = esc_url($instance['image_uri_slide-' . $i]);
                $slider_size_front = esc_attr($instance['img_size_for_slider-' . $i]);
                $image_link_front = esc_url($instance['image_link_front-' . $i]);
                if ($slider_size_front != 'custom') {
                    $attachment_id_front = zwrew_get_attachment_id1($image_url_slider);
                    $image_front = wp_get_attachment_image_src($attachment_id_front, $slider_size_front);
                    $image_url_slider = $image_front[0];
                    $width_front = $image_front[1];
                }

                $image_front = '<img src="' . $image_url_slider . '" alt="' . $instance['alternate_text_front-' . $i] . '" height="' . $height_front . '" width="' . $width_front . '"/>';
                if (get_option('rew_image_link') == 1 && !empty($image_link_front)) {
                    if ($link_target == '_window') {
                        $image_target_front = "href='#' onClick=\"window.open('" . $image_link_front . "','MyWindow','toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=600,height=300'); return false;\"";
                    } else {
                        $image_target_front = 'href="' . $image_link_front . '" target="' . $link_target . '"';
                    }
                    $image_front = '<a ' . $image_target_front . '><img src="' . $image_url_slider . '" alt="' . $instance['alternate_text_front-' . $i] . '" height="' . $height_front . '" width="' . $width_front . '"/></a>';
                }

                //Description
                $short_desc = apply_filters('the_content', $instance['short_desc-' . $i]);
                if (strlen($short_desc) > 200) {
                    $short_desc = substr($short_desc, 0, 200) . '...';
                }

                $full_description = apply_filters('the_content', $instance['full_desc-' . $i]);
                if (strlen($full_description) > 200) {
                    $full_desc = substr($full_description, 0, 200) . '...';
                } else {
                    $full_desc = $full_description;
                }

                $short_desc_link = $instance['short_desc_link-' . $i];
                $button_title = empty($instance['button_title-' . $i]) ? 'Read More' : $instance['button_title-' . $i];
                $short_desc_button = "";
                if (get_option('rew_description') == 'short' && (!empty($short_desc_link) && $short_desc_link != "")) {
                    if ($link_target == '_window') {
                        $button_target = "href='" . $short_desc_link . "' onClick=\"window.open('" . $short_desc_link . "','MyWindow','toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=600,height=300'); return false;\"";
                    } else {
                        $button_target = 'href="' . $short_desc_link . '" target="' . $link_target . '"';
                    }
                    $short_desc_button = '<a ' . $button_target . '><button name="external_link">' . $button_title . '</button></a>';
                }
                ?>




          <div class="swiper-slide">
            <div class="homeSolSlider center flex">
                <div class="homeSolSliderImg" data-aos="fade-up" data-aos-delay="300"  data-aos-duration="400" data-aos-once="true">
                    <img src="<?php echo $image_url_slider; ?>" class="img-fluid" alt="slider-img">
                </div>
                <div class="homeSolSliderText" data-aos="fade-down" data-aos-delay="500"  data-aos-duration="600">
                    <p><?php echo $caption; ?></p>
                </div>
            </div>
          </div>

                <?php
}
        }

        ?>
              </div>
    </div>

    <div class="homeSolSliderButton" data-aos="fade-left" data-aos-delay="300"  data-aos-duration="400" data-aos-once="true">
            <div class="swiper-button-next">
                <img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/img/solution/sloution-slider-next-arrow.png" alt="" />
            </div>
            <div class="swiper-button-prev">
                <img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/img/solution/sloution-slider-prev-arrow.png" alt="" />
            </div>
    </div>
     </section>
                <?php

        echo $args['after_widget'];
    }
    //Function widget ends here

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $max_entries_slider_image = get_option('rew_max');
        $max_entries_slider_image = (empty($max_entries_slider_image)) ? '5' : $max_entries_slider_image;
        $instance['slider_title'] = strip_tags($new_instance['slider_title']);

        for ($i = 0; $i < $max_entries_slider_image; $i++) {
            $block = $new_instance['block-' . $i];
            if ($block == 0 || $block == "") {
                $instance['block-' . $i] = $new_instance['block-' . $i];
                $instance['sldier_caption-' . $i] = strip_tags($new_instance['sldier_caption-' . $i]);
                $instance['caption_link-' . $i] = strip_tags($new_instance['caption_link-' . $i]);
                $instance['image_uri_slide-' . $i] = strip_tags($new_instance['image_uri_slide-' . $i]);
                $instance['alternate_text_front-' . $i] = strip_tags($new_instance['alternate_text_front-' . $i]);
                $instance['img_size_for_slider-' . $i] = strip_tags($new_instance['img_size_for_slider-' . $i]);
                $instance['width_front-' . $i] = strip_tags($new_instance['width_front-' . $i]);
                $instance['height_front-' . $i] = strip_tags($new_instance['height_front-' . $i]);
                $instance['image_link_front-' . $i] = strip_tags($new_instance['image_link_front-' . $i]);

            } else {
                $count = $block - 1;
                $instance['block-' . $count] = $new_instance['block-' . $i];
                $instance['sldier_caption-' . $count] = strip_tags($new_instance['sldier_caption-' . $i]);
                $instance['caption_link-' . $count] = strip_tags($new_instance['caption_link-' . $i]);
                $instance['image_uri_slide-' . $count] = strip_tags($new_instance['image_uri_slide-' . $i]);
                $instance['alternate_text_front-' . $count] = strip_tags($new_instance['alternate_text_front-' . $i]);
                $instance['img_size_for_slider-' . $count] = strip_tags($new_instance['img_size_for_slider-' . $i]);
                $instance['width_front-' . $count] = strip_tags($new_instance['width_front-' . $i]);
                $instance['height_front-' . $count] = strip_tags($new_instance['height_front-' . $i]);
                $instance['image_link_front-' . $count] = strip_tags($new_instance['image_link_front-' . $i]);

            }
        }
        return $instance;
    }
    //Function update ends here

    /**0
     * @see WP_Widget::form -- do not rename this
     * this is for admin panel
     */
    public function form($display_instance)
    {
        $max_entries_slider_image = get_option('rew_max');
        $max_entries_slider_image = (empty($max_entries_slider_image)) ? '5' : $max_entries_slider_image;
        //$widget_add_id_slider = $this->id . "-add";
        $widget_add_id_slider_client = $this->get_field_id('') . "add";
        $title = ($display_instance['slider_title']);
        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('slider_title') . '"> ' . __('Slider Title', ZWREW_TEXT_DOMAIN) . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('slider_title') . '" name="' . $this->get_field_name('slider_title') . '" type="text" value="' . $title . '" />';

        $rew_html .= '<div class="' . $widget_add_id_slider . '-input-containers"><div id="entries">';
        for ($i = 0; $i < $max_entries_slider_image; $i++) {
            if (isset($display_instance['block-' . $i]) || isset($display_instance['sldier_caption-' . $i]) || isset($display_instance['image_uri_slide-' . $i])) {

                $display = (!isset($display_instance['block-' . $i]) || ($display_instance['block-' . $i] == "")) ? 'style="display:none;"' : '';

                $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', ZWREW_TEXT_DOMAIN) . ' </span>';
                $rew_html .= '<div class="entry-desc cf">';
                $rew_html .= '<input id="' . $this->get_field_id('block-' . $i) . '" name="' . $this->get_field_name('block-' . $i) . '" type="hidden" value="' . $display_instance['block-' . $i] . '">';
                /**
                 * Block Caption
                 */

                /**
                 * Block Image
                 */
                if (get_option('rew_image') == 1) {
                    $slider_size_front = esc_attr($display_instance['img_size_for_slider-' . $i]);
                    $style = ($slider_size_front == 'custom') ? 'style="display:block;"' : 'style="display:none;"';
                    $rew_html .= '<p>';
                    $rew_html .= '<label for="' . $this->get_field_id('image_uri_slide-' . $i) . '">' . __(' Slider Image', ZWREW_TEXT_DOMAIN) . ' :</label>';
                    $show = (!empty($display_instance['image_uri_slide-' . $i])) ? 'style="display:block;"' : '';
                    $rew_html .= '<input type="button name="removeimg" id="remove-img-back" class="button button-secondary" onclick="removeImageSlider(' . $i . ');" ' . $show . '>';
                    $rew_html .= '<img src="' . $display_instance['image_uri_slide-' . $i] . '" class="block-image" ' . $show . '>';
                    $rew_html .= '<input type="hidden" class="img' . $i . '" style="width:auto;" name="' . $this->get_field_name('image_uri_slide-' . $i) . '" id="' . $this->get_field_id('image_uri_slide-' . $i) . '" value="' . $display_instance['image_uri_slide-' . $i] . '" />';
                    $rew_html .= '<input type="button" class="select-img-slider' . $i . '" style="width:auto;" value=" Image" onclick="selectImageslider(' . $i . ');"/>';
                    $rew_html .= '</p><p>';
                    $possible_sizes_front_img['custom'] = __('Custom');

                }

                if (get_option('rew_caption') == 1) {
                    $rew_html .= '<p>';
                    $rew_html .= '<label for="' . $this->get_field_id('sldier_caption-' . $i) . '"> ' . __('Slider Caption', ZWREW_TEXT_DOMAIN) . ' :</label>';
                    $rew_html .= '<input id="' . $this->get_field_id('sldier_caption-' . $i) . '" name="' . $this->get_field_name('sldier_caption-' . $i) . '" type="text" value="' . $display_instance['sldier_caption-' . $i] . '">';
                    $rew_html .= '</p>';

                }
                /**
                 * Description
                 */
                $short_desc = esc_attr($display_instance['short_desc-' . $i]);
                $short_desc_link = esc_attr($display_instance['short_desc_link-' . $i]);
                $full_desc = esc_attr($display_instance['full_desc-' . $i]);
                $button_title = esc_attr($display_instance['button_title-' . $i]);

                if (get_option('rew_description') == 'short') {
                    $rew_html .= '<p class="last desc">';
                    $rew_html .= '<label for="' . $this->get_field_id('short_desc-' . $i) . '"> ' . __('Short Description', ZWREW_TEXT_DOMAIN) . ' :</label>';
                    $rew_html .= '<textarea id="' . $this->get_field_id('short_desc-' . $i) . '" name="' . $this->get_field_name('short_desc-' . $i) . '">' . $short_desc . '</textarea>';
                    $rew_html .= '</p><p>';
                    $rew_html .= '<label for="' . $this->get_field_id('short_desc_link-' . $i) . '"> ' . __('External Link', ZWREW_TEXT_DOMAIN) . ' :</label>';
                    $rew_html .= '<input class="widefat" id="' . $this->get_field_id('short_desc_link-' . $i) . '" name="' . $this->get_field_name('short_desc_link-' . $i) . '" type="text" value="' . $short_desc_link . '" />';
                    $rew_html .= '</p>';
                } else {

                }
                /**
                 * Read More Button
                 *//**
                 * Read More Button
                 */
                $rew_html .= '<p><a href="#delete"><span class="delete-row">' . __('Delete Row', ZWREW_TEXT_DOMAIN) . '</span></a></p>';
                $rew_html .= '</div></div>';

            }
        }
        $rew_html .= '</div></div>';
        $rew_html .= '<div id="message">' . __('Sorry, you reached to the limit of', ZWREW_TEXT_DOMAIN) . ' "' . $max_entries_slider_image . '" ' . __('maximum entries', ZWREW_TEXT_DOMAIN) . '.</div>';
        $rew_html .= '<div class="' . $widget_add_id_slider . '" style="display:none;">' . __('ADD ROW', ZWREW_TEXT_DOMAIN) . '</div>';
        ?>
		<script>
		  jQuery(document).ready(function(e) {
			jQuery.each(jQuery(".<?php echo $widget_add_id_slider; ?>-input-containers #entries").children(), function(){
				if(jQuery(this).find('input').val() != ''){
					jQuery(this).show();
				}
			});
			jQuery(".<?php echo $widget_add_id_slider; ?>" ).bind('click', function(e) {
				var rows = 0;
				jQuery.each(jQuery(".<?php echo $widget_add_id_slider; ?>-input-containers #entries").children(), function(){
					if(jQuery(this).find('input').val() == ''){
						jQuery(this).find(".entry-title").addClass("active");
						jQuery(this).find(".entry-desc").slideDown();
						jQuery(this).find('input').first().val('0');
						jQuery(this).show();
						return false;
					}
					else{
					  rows++;
					  jQuery(this).show();
					  jQuery(this).find(".entry-title").removeClass("active");
					  jQuery(this).find(".entry-desc").slideUp();
					}
				});
				if(rows == '<?php echo $max_entries_slider_image; ?>')
				{
					jQuery("#rew_container #message").show();
				}
			});
			jQuery(".delete-row" ).bind('click', function(e) {
				var count = 1;
				var current = jQuery(this).closest('.entrys').attr('id');
				jQuery.each(jQuery("#entries #"+current+" .entry-desc").children(), function(){
					jQuery(this).val('');
				});
				jQuery.each(jQuery("#entries #"+current+" .entry-desc p").children(), function(){
					jQuery(this).val('');
				});
				jQuery('#entries #'+current+" .entry-title").removeClass('active');
				jQuery('#entries #'+current+" .entry-desc").hide();
				jQuery('#entries #'+current).remove();
				jQuery.each(jQuery(".<?php echo $widget_add_id_slider; ?>-input-containers #entries").children(), function(){
					if(jQuery(this).find('input').val() != ''){
						jQuery(this).find('input').first().val(count);
					}
					count++;
				});
			});
		});
		</script>
		<style>
			.cf:before, .cf:after { content: ""; display: table; }
			.cf:after { clear: both; }
			.cf { zoom: 1; }
			.clear { clear: both; }
			.clearfix:after { content: "."; display: block; height: 0; clear: both; visibility: hidden; }
			.clearfix { display: inline-block; }
			* html .clearfix { height: 1%; }
			.clearfix { display: block;}

			#rew_container input,select,textarea{ float: right;width: 60%;}
			#rew_container label{width:40%;}
			<?php echo '.' . $widget_add_id_slider; ?>{
			background: #ccc none repeat scroll 0 0;font-weight: bold;margin: 20px 0px 9px;padding: 6px;text-align: center;display:block !important; cursor:pointer;
			}
			.block-image{width:50px; height:30px; float: right; display:none;}
			.desc{height:55px;}
			#entries #remove-img{background:url('<?php echo ZWREW_URL; ?>assets/images/deleteimg.png') center center no-repeat; width:20px; height:22px;display:none;}
                        #entries #remove-img-back{background:url('<?php echo ZWREW_URL; ?>assets/images/deleteimg.png') center center no-repeat; width:20px; height:22px;display:none;}
			#entries #remove-img2{background:url('<?php echo ZWREW_URL; ?>assets/images/deleteimg.png') center center no-repeat; width:20px; height:22px;display:none;}
			#entries #remove-img3{background:url('<?php echo ZWREW_URL; ?>assets/images/deleteimg.png') center center no-repeat; width:20px; height:22px;display:none;}
			#entries #remove-img4{background:url('<?php echo ZWREW_URL; ?>assets/images/deleteimg.png') center center no-repeat; width:20px; height:22px;display:none;}


			#entries{ padding:10px 0 0;}
			#entries .entrys{ padding:0; border:1px solid #e5e5e5; margin:10px 0 0; clear:both;}
			#entries .entrys:first-child{ margin:0;}
			#entries .delete-row{margin-top:20px;float:right;text-decoration: underline;color:red;}
			#entries .entry-title{ display:block; font-size:14px; line-height:18px; font-weight:600; background:#f1f1f1; padding:7px 5px; position:relative;}
			#entries .entry-title:after{ content: '\f140'; font: 400 20px/1 dashicons; position:absolute; right:10px; top:6px; color:#a0a5aa;}
			#entries .entry-title.active:after{ content: '\f142';}
			#entries .entry-desc{ display:none; padding:0 10px 10px; border-top:1px solid #e5e5e5;}
			#rew_container #entries p.last label{ white-space: pre-line; float:left; width:39%;}
			#message{padding:6px;display:none;color:red;font-weight:bold;}
		</style>
		<div id="rew_container">
		  <?php echo $rew_html; ?>
		</div>
		<?php
} //Function form ends here
}

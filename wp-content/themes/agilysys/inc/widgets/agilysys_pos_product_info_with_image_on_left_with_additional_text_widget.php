<?php
add_action('widgets_init', 'agil_load_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget');

function agil_load_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget()
{
    register_widget('agilysys_pos_product_info_with_image_on_left_with_additional_text_widget');
}

class agilysys_pos_product_info_with_image_on_left_with_additional_text_widget extends WP_Widget
{

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Pos Product Info With Image On Left With Additional Text Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');

    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        $title = $instance['title'];
        $title2 = $instance['title2'];
        $desc = $instance['desc'];
        $learn_more = $instance['learn_more'];

        $link_type = $instance['link_type'];
        if ($link_type == "link") {
            $link = $instance['link'];
        } elseif ($link_type == "page") {
            $post_id = $instance['page'];
            $post = get_post($post_id);
            $link = home_url($post->post_name) . "/";
        }

        $image_uri = $instance['image_uri'];
        $image_uri_alt = $instance['image_uri_alt'];

        $type = $instance['type'];
        if ($type == "left") {
            ?>

<section class="posProductBox">
    <div class=row>
        <div class="posProductImage col-12 col-sm-12 col-md-6 col-lg-6">
            <img class="img-fluid" src="<?php echo $image_uri; ?>" alt="<?php echo $image_uri_alt; ?>" />
        </div>
        <div class="posProductContent  col-12 col-sm-12 col-md-6 col-lg-6">
            <h2 class="dinProStd waterText"><?php echo $title; ?></h2>
            <h3 class="dinProStd greenText"><?php echo $title2; ?></h3>
            <p><?php echo $desc; ?></p>
            <a class="homeBannerButton violetText" href="<?php echo $link; ?>"><?php echo $learn_more; ?> <i
                    class="fa fa-arrow-right" aria-hidden="true"></i></a>
        </div>
    </div>
</section>
<?php }
        if ($type == "right") {
            ?>

<section class="posProductBox row">

    <div class="posProductContent  col-lg-6">
        <h2 class="dinProStd waterText"><?php echo $title; ?></h2>
        <h3 class="dinProStd greenText"><?php echo $title2; ?></h3>
        <p><?php echo $desc; ?></p>
        <a class="homeBannerButton violetText" href="<?php echo $link; ?>"><?php echo $learn_more; ?> <i
                class="fa fa-arrow-right" aria-hidden="true"></i></a>
    </div>

    <div class="posProductImage col-lg-6">
        <img class="img-fluid" src="<?php echo $image_uri; ?>" alt="" />
    </div>
</section>
<?php }?>
<?php
echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {

        $instance = array();

        $instance['title'] = strip_tags($new_instance['title']);
        $instance['title2'] = strip_tags($new_instance['title2']);
        $instance['desc'] = strip_tags($new_instance['desc']);
        $instance['learn_more'] = strip_tags($new_instance['learn_more']);
        $instance['link_type'] = $new_instance['link_type'];
        if ($new_instance['link_type'] == 'page') {
            $instance['page'] = $new_instance['page'];
            $instance['link'] = '';
        } elseif ($new_instance['link_type'] == 'link') {
            $instance['link'] = $new_instance['link'];
            $instance['page'] = '';

        }
        $instance['image_uri'] = strip_tags($new_instance['image_uri']);
        $instance['image_uri_alt'] = strip_tags($new_instance['image_uri_alt']);

        
        $instance['type'] = strip_tags($new_instance['type']);

        return $instance;

    }

    public function form($display_instance)
    {

        $title = ($display_instance['title']);
        $title2 = ($display_instance['title2']);
        $desc = ($display_instance['desc']);
        $learn_more = ($display_instance['learn_more']);
        $link = ($display_instance['link']);
        $image_uri = ($display_instance['image_uri']);
        $type = ($display_instance['type']);
        $image_uri_alt = $display_instance['image_uri_alt']; 


        

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt') . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt') . '" name="' . $this->get_field_name('image_uri_alt') . '" type="text" value="' . $image_uri_alt . '" />';
        $rew_html .= '</p>';


        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('title') . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" type="text" value="' . $title . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('title2') . '"> ' . __('Title2', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('title2') . '" name="' . $this->get_field_name('title2') . '" type="text" value="' . $title2 . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('desc') . '"> ' . __('Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<textarea id="' . $this->get_field_id('desc') . '" name="' . $this->get_field_name('desc') . '">' . $desc . '</textarea>';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('learn_more') . '"> ' . __('Learn More Text', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('learn_more') . '" name="' . $this->get_field_name('learn_more') . '" type="text" value="' . $learn_more . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('link_type') . '"> ' . __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<select id="' . $this->get_field_id('link_type') . '" name="' . $this->get_field_name('link_type') . '" onChange="show_hide_div_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget(this.value);">';
        $rew_html .= '<option value="">Please Select</option>';

        $link_type = $display_instance['link_type'];

        if ($link_type == 'page') {
            $rew_html .= '<option value="page" selected="selected">Internal Page Link</option>';
        } else {
            $rew_html .= '<option value="page">Internal Page Link</option>';
        }

        if ($link_type == 'link') {
            $rew_html .= '<option value="link" selected="selected">External Link</option>';
        } else {
            $rew_html .= '<option value="link">External Link</option>';
        }

        $rew_html .= '</select>';
        $rew_html .= '</p><br><br>';

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block"';
            $show2 = 'style="display:none"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none"';
            $show2 = 'style="display:block"';

        } else {
            $show1 = 'style="display:none"';
            $show2 = 'style="display:none"';
        }
        $rew_html .= '<div id="page_div_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget" ' . $show1 . '><p>';
        $rew_html .= '<label for="' . $this->get_field_id('page') . '"> ' . __('Page', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<select id="' . $this->get_field_id('page') . '" name="' . $this->get_field_name('page') . '">';
        $rew_html .= '<option value="">Please Select</option>';

        $page = $display_instance['page'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                $rew_html .= '<option value="' . $key->ID . '" selected="selected">' . $key->post_title . '</option>';
            } else {
                $rew_html .= '<option value="' . $key->ID . '">' . $key->post_title . '</option>';
            }

        }

        $rew_html .= '</select>';
        $rew_html .= '</p></div><br><br>';

        $rew_html .= '<div id="link_div_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget" ' . $show2 . '><p>';
        $rew_html .= '<label for="' . $this->get_field_id('link') . '"> ' . __('Link', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('link') . '" name="' . $this->get_field_name('link') . '" type="text" value="' . $link . '" />';
        $rew_html .= '</p></div><br><br>';
        ?>
<script>
function show_hide_div_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget(val) {

    if (val == 'page') {
        jQuery("#page_div_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget").show();
        jQuery("#link_div_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget").hide();
    } else if (val == 'link') {
        jQuery("#page_div_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget").hide();
        jQuery("#link_div_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget").show();
    }

}
</script>

<?php

 $rew_html .= '<p>';
$rew_html .= '<label for="' . $this->get_field_id('type') . '"> ' . __('Select which side text: ', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<select id="' . $this->get_field_id('type') . '" name="' . $this->get_field_name('type') . '">';
        $rew_html .= '<option value="">Please Select</option>';

        if ($type == "left") {
            $rew_html .= '<option value="left" selected="selected">Left side Text</option>';
        } else {
            $rew_html .= '<option value="left">Left side Text</option>';
        }

        if ($type == "right") {
            $rew_html .= '<option value="right" selected="selected">Right side Text</option>';
        } else {
            $rew_html .= '<option value="right">Right side Text</option>';
        }

        $rew_html .= '</select>';
        $rew_html .= '</p><br><br>';

        ?>

<label class="widg-label widg-img-label" for="<?php echo $this->get_field_id('image_uri'); ?>">Image</label>
<div class="widg-img">
    <label id="image_uri_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget"></label><br><img
        class="<?php echo $this->get_field_id('image_id'); ?>_media_image custom_media_image"
        src="<?php if (!empty($display_instance['image_uri'])) {echo $display_instance['image_uri'];}?>" />
    <input input type="hidden" type="text"
        class="<?php echo $this->get_field_id('image_id'); ?>_media_id custom_media_id"
        name="<?php echo $this->get_field_name('image_id'); ?>" id="<?php echo $this->get_field_id('image_id'); ?>"
        value="<?php echo $display_instance['image_id']; ?>" />
    <input type="hidden" class="<?php echo $this->get_field_id('image_id'); ?>_media_url custom_media_url"
        name="<?php echo $this->get_field_name('image_uri'); ?>" id="<?php echo $this->get_field_id('image_uri'); ?>"
        value="<?php echo $display_instance['image_uri']; ?>">
    <input type="button" value="Upload Image" class="button custom_media_upload"
        id="<?php echo $this->get_field_id('image_id'); ?>" />
</div>

<script>
jQuery(document).ready(function() {
    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    if (attachment.height == 1080 && attachment.width == 1920) {
                        jQuery('.' + button_id_s + '_media_id').val(attachment.id);
                        jQuery('.' + button_id_s + '_media_url').val(attachment.url);
                        jQuery('.' + button_id_s + '_media_image').attr('src', attachment.url).css(
                            'display', 'block');

                        jQuery(
                                '#image_uri_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget')
                            .html("");
                    } else {
                        jQuery(
                                '#image_uri_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget')
                            .html("Please Enter the correct Dimensions 1920x1080").css(
                                'color', 'red');

                    }

                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload');

});
</script>


<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}



#rew_container_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget p {
    padding:20px;
}


#rew_container_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget select {
    float: left;
    width: 60%;
    margin-top: 20px !important;
    margin-bottom: 10px !important;
}

#rew_container_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget input,

textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget label {
    width: 40%;
    float: left;
}

<?php echo '.'. $widget_add_id_slider;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}



#entries_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget {
    padding: 10px 0 0;
}

#entries_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget #entries_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_pos_product_info_with_image_on_left_with_additional_text_widget">
    <?php echo $rew_html; ?>
</div>

<?php

    }
}
<?php

add_action('widgets_init', 'load_agilysys_customer_stories_slider_widget');

function load_agilysys_customer_stories_slider_widget()
{
    register_widget('agilysys_customer_stories_slider_widget');
}

class agilysys_customer_stories_slider_widget extends WP_Widget
{
    /**
     * constructor -- name this the same as the class above
     */

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Customer Stories Slider Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');

    }

    public function widget($args, $instance)
    {



        echo $args['before_widget'];
        $section_title = $instance['section_title'];
        $industries_arr = array(
            1 => 'Hotels & Resorts',
            2 => 'Casino Resorts',
            3 => 'Tribal Gaming',
            4 => 'Cruise Lines',
            5 => 'Foodservice Management',
            6 => 'Sports & Entertainment',
            7 => 'Restaurants',
            8 => 'Spa',
            9 => 'Golf',
        );

        $q = '';
        if (isset($_GET['q'])) {
            $q = htmlspecialchars_decode(urldecode($_GET['q']));
        } else {
            $q = '';
        }

        $products_arr = array(
            'PROPERTY MANAGEMENT' => array(
                1 => 'Agilysys Stay',
                2 => 'Agilysys LMS',
                3 => 'Agilysys Visual One PMS',
                4 => 'Agilysys Sales and Catering',
                5 => 'rGuest® Book',
                6 => 'rGuest® Express Kiosk',
                7 => 'rGuest® Express Mobile',
                8 => 'rGuest® Service',
                9 => 'b4checkin',
            ),

            'POINT OF SALE' => array(
                10 => 'Agilysys InfoGenesis',
                11 => 'IG Flex',
                12 => 'IG Buy',
                13 => 'IG OnDemand',
            ),
            'PAYMENT SOLUTION' => array(
                14 => 'Agilysys Pay',

            ),
            'ANALYTICS & MARKETING LOYALTY' => array(
                15 => 'Agilysys Analyze',
            ),
            'INVENTORY & PROCUREMENT' => array(
                16 => 'Agilysys Eatec',
                17 => 'Agilysys SWS',

            ),
            'RESERVATIONS AND TABLE MANAGEMENT' => array(
                18 => 'Agilysys Seat',
            ),
            'ACTIVITY SCHEDULING' => array(
                19 => 'Agilysys Golf',
                20 => 'Agilysys Spa',
            ),
            'DOCUMENT MANAGEMENT' => array(
                21 => 'Agilysys DataMagine',
            ),
            'SERVICES' => array(
                22 => 'Professional Services',
            ),

        );
        ?>





<div id="overlay_agilysys_customer_stories_slider_widget">
    <div class="loader_agilysys_customer_stories_slider_widget"></div>
</div>
<section class="videosTitle food">
    <div class="videosTitleBox greenBG center">
        <h2 class="dinProStd h2 whiteText"><?php echo $section_title; ?></h2>
        <div class="videoFilter flex whiteText">
            <p>FILTER BY :</p>
            <div class="videoFilterBox">
                <select class="filterOne" name="industries1" id="industries_agilysys_customer_stories_slider_widget"
                    onChange="fetch_data_agilysys_customer_stories_slider_widget();">
                    <option value="">All Industries</option>

                    <?php

        foreach ($industries_arr as $key => $val) {

            if ($val == $q) {
                ?>

                    <option value="<?php echo $key; ?>" selected="selected"><?php echo $val; ?></option>

                    <?php

            } else {

                ?>
                    <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                    <?php
}
        }
        ?>
                </select>
            </div>
            <div class="videoFilterBox">
                <select class="filterTwo" name="products1" id="products_agilysys_customer_stories_slider_widget"
                    onChange="fetch_data_agilysys_customer_stories_slider_widget();">
                    <option value="">All Products</option>
                    <?php

        foreach ($products_arr as $key => $val) {

            if (is_array($val)) {
                ?>
                    <optgroup label="<?php echo $key; ?>">
                        <?php
foreach ($val as $k => $v) {
                    ?>
                        <option value="<?php echo $k; ?>"><?php echo $v; ?></option>
                        <?php
}
                ?>
                    </optgroup>
                    <?php
}

        }
        ?>
                </select>
            </div>

            <div class="videoClear" onClick="reset_filters_agilysys_customer_stories_slider_widget();">
                <p>CLEAR FILTERS</p>
            </div>

        </div>
    </div>

</section>

<section class="customerStoriesSliderSection">
    <div class="swiper-container">
        <div class="swiper-wrapper" id="articleList_agilysys_customer_stories_slider_widget">






        </div>
        <!-- Add Arrows -->
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
</section>


<style>
.loader_agilysys_customer_stories_slider_widget {
    width: 80px;
    height: 80px;
    background: #fff;
    border: 2px solid #f3f3f3;
    border-top: 3px solid #008000;
    border-radius: 100%;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 45%;
    margin: auto;
    animation: spin 1s infinite;

}

@keyframes spin {
    from {
        transform: rotate(0deg);
    }

    to {
        transform: rotate(360deg);
    }
}
</style>

<script>
function reset_filters_agilysys_customer_stories_slider_widget() {

    jQuery('#industries_agilysys_customer_stories_slider_widget').val('');
    jQuery('#products_agilysys_customer_stories_slider_widget').val('');
    fetch_data_agilysys_customer_stories_slider_widget();
}

jQuery(document).ready(function($) {
    fetch_data_agilysys_customer_stories_slider_widget();
});

function fetch_data_agilysys_customer_stories_slider_widget() {
    jQuery(".loader_agilysys_customer_stories_slider_widget").fadeIn("slow");
    var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';


    var data = {

        action: "fetch-data-agilysys-customer-stories-slider-widget",
        industries: jQuery('#industries_agilysys_customer_stories_slider_widget').val(),
        products: jQuery('#products_agilysys_customer_stories_slider_widget').val(),
        data: '<?php echo json_encode($instance); ?>'
    };



    jQuery.post(ajaxurl, data, function(response) {


        var dataxx = JSON.parse(response);


        jQuery("#articleList_agilysys_customer_stories_slider_widget").html(dataxx.msg);


        //Home-experiences SLIDER
        var homeLeaderVideo = new Swiper('.customerStoriesSliderSection .swiper-container', {
            speed: 600,
            spaceBetween: 50,
            initialSlide: 0,
            //autoHeight: true,
            // Optional parameters
            direction: 'horizontal',
            loop: true,
            autoplay: false,
            autoplayStopOnLast: false, // loop false also
            effect: 'slide',
            // Distance between slides in px.
            spaceBetween: 50,
            //
            slidesPerView: 2,
            //
            centeredSlides: true,
            //
            slidesOffsetBefore: 0,
            //
            grabCursor: true,

            navigation: {
                nextEl: '.customerStoriesSliderSection .swiper-button-next',
                prevEl: '.customerStoriesSliderSection .swiper-button-prev',
            },

            breakpoints: {

                640: {
                    slidesPerView: 1,
                    spaceBetween: 20
                },
                320: {
                    slidesPerView: 1,
                    spaceBetween: 10
                }
            }
        });

        jQuery(".loader_agilysys_customer_stories_slider_widget").fadeOut("slow");


    });
}
</script>



<?php

        echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['section_title'] = strip_tags($new_instance['section_title']);
        $instance['what_makes_rows'] = strip_tags($new_instance['what_makes_rows']);

        $count = count($new_instance['title']);

        for ($i = 0; $i < $count; $i++) {

            $instance['title'][$i] = strip_tags($new_instance['title'][$i]);
            $instance['slider_image'][$i] = strip_tags($new_instance['slider_image'][$i]);
            $instance['image_uri_alt'][$i] = strip_tags($new_instance['image_uri_alt'][$i]);

            $instance['logo'][$i] = strip_tags($new_instance['logo'][$i]);
            $instance['logo_alt'][$i] = strip_tags($new_instance['logo_alt'][$i]);

            $instance['description'][$i] = strip_tags($new_instance['description'][$i]);
            $instance['url_text'][$i] = strip_tags($new_instance['url_text'][$i]);

            $instance['pdf_uri'][$i] = $new_instance['pdf_uri'][$i];
            $instance['thankyou_required'][$i] = $new_instance['thankyou_required'][$i];

            $instance['industries'][$i] = strip_tags($new_instance['industries'][$i]);
            $instance['products'][$i] = strip_tags($new_instance['products'][$i]);

        }
        return $instance;
    }

    public function form($display_instance)
    {

        $widget_add_id_webinars_info = $this->get_field_id('') . "add_agilysys_customer_stories_slider_widget";

        $section_title = $display_instance['section_title'];

        $industries_arr = array(
            1 => 'Hotels & Resorts',
            2 => 'Casino Resorts',
            3 => 'Tribal Gaming',
            4 => 'Cruise Lines',
            5 => 'Foodservice Management',
            6 => 'Sports & Entertainment',
            7 => 'Restaurants',
            8 => 'Spa',
            9 => 'Golf',
        );

        $products_arr = array(
            'PROPERTY MANAGEMENT' => array(
                1 => 'Agilysys Stay',
                2 => 'Agilysys LMS',
                3 => 'Agilysys Visual One PMS',
                4 => 'Agilysys Sales and Catering',
                5 => 'rGuest® Book',
                6 => 'rGuest® Express Kiosk',
                7 => 'rGuest® Express Mobile',
                8 => 'rGuest® Service',
                9 => 'b4checkin',
            ),

            'POINT OF SALE' => array(
                10 => 'Agilysys InfoGenesis',
                11 => 'IG Flex',
                12 => 'IG Buy',
                13 => 'IG OnDemand',
            ),
            'PAYMENT SOLUTION' => array(
                14 => 'Agilysys Pay',

            ),
            'ANALYTICS & MARKETING LOYALTY' => array(
                15 => 'Agilysys Analyze',
            ),
            'INVENTORY & PROCUREMENT' => array(
                16 => 'Agilysys Eatec',
                17 => 'Agilysys SWS',

            ),
            'RESERVATIONS AND TABLE MANAGEMENT' => array(
                18 => 'Agilysys Seat',
            ),
            'ACTIVITY SCHEDULING' => array(
                19 => 'Agilysys Golf',
                20 => 'Agilysys Spa',
            ),
            'DOCUMENT MANAGEMENT' => array(
                21 => 'Agilysys DataMagine',
            ),
            'SERVICES' => array(
                22 => 'Professional Services',
            ),

        );

        if (!empty($display_instance['what_makes_rows'])) {
            $what_makes_rows = ($display_instance['what_makes_rows']);
        } else {
            $what_makes_rows = 0;
        }

        $max = 15;

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('section_title') . '"> ' . __('Slider Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('section_title') . '" name="' . $this->get_field_name('section_title') . '" type="text" value="' . $section_title . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('what_makes_rows') . '"> ' . __('No. of rows do you want to add*', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input class="what_makes_rows" id="' . $this->get_field_name('what_makes_rows') . '" name="' . $this->get_field_name('what_makes_rows') . '" type="number" value="' . $what_makes_rows . '" />';
        $rew_html .= '</p><br><br>';

        $count = count($display_instance['title']);

        $rew_html .= '<div class="add_new_rowxx-input-containers"><div id="entries_agilysys_customer_stories_slider_widget">';

        $rew_html .= '<input class="cnt909" id="cnt909" name="cnt" type="hidden" value="' . $count . '">';

        for ($i = 0; $i < $count; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'AGILYSYS_TEXT_DOMAIN') . ' </span>';

            $rew_html .= '<div class="entry-desc cf">';

            $rew_html .= '<p >';
            $rew_html .= '<label for="' . $this->get_field_id('title' . $i) . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('title' . $i) . '" name="' . $this->get_field_name('title[]') . '" type="text" value="' . $display_instance['title'][$i] . '" />';
            $rew_html .= '</p>';

            $rew_html .= '<p >';
            $rew_html .= '<label for="' . $this->get_field_id('description' . $i) . '"> ' . __('Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<textarea rows="6" cols="35" id="' . $this->get_field_id('description' . $i) . '" name="' . $this->get_field_name('description[]') . '">' . $display_instance['description'][$i] . '</textarea>';
            $rew_html .= '</p><br><br><br><br><br><br>';

            $rew_html .= '<div class="widg-img' . $i . '">';
            $show1 = (empty($display_instance['slider_image'][$i])) ? 'style="display:none;"' : '';
            $rew_html .= '<label id="image_uri_agilysys_customer_stories_slider_widget' . $i . '"></label><br><img class="' . $this->get_field_id('slider_image' . $i) . '_media_image' . $i . ' custom_media_image' . $i . '" src="' . $display_instance['slider_image'][$i] . '" ' . $show1 . ' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('slider_id' . $i) . '_media_id' . $i . ' custom_media_id' . $i . '" name="' . $this->get_field_name('slider_id[]') . '" id="' . $this->get_field_id('slider_id' . $i) . '" value="' . $display_instance['slider_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('slider_image' . $i) . '_media_url' . $i . ' custom_media_url' . $i . '" name="' . $this->get_field_name('slider_image[]') . '" id="' . $this->get_field_id('slider_image' . $i) . '" value="' . $display_instance['slider_image'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload Slider Image" class="button custom_media_upload' . $i . '" id="' . $this->get_field_id('slider_image' . $i) . '"/>';

            $rew_html .= '</div><br><br>';
            ?>


<script>
jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {


                    //if (attachment.height == 1080 && attachment.width == 1920) {
                    jQuery('.' + button_id_s + '_media_id<?php echo $i; ?>').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_url<?php echo $i; ?>').val(attachment
                        .url);
                    jQuery('.' + button_id_s + '_media_image<?php echo $i; ?>').attr('src',
                        attachment.url).css('display', 'block');
                    /*  jQuery('#image_uri_agilysys_customer_stories_slider_widget<?php echo $i; ?>')
                            .html("");
                    } else {
                        jQuery('#image_uri_agilysys_customer_stories_slider_widget<?php echo $i; ?>')
                            .html("Please Enter the correct Dimensions 1920x1080").css('color',
                                'red');
                    }
*/





                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload<?php echo $i; ?>');

});
</script>
<?php

            $rew_html .= '<p >';
            $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt' . $i) . '"> ' . __('Image Alt', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt' . $i) . '" name="' . $this->get_field_name('image_uri_alt[]') . '" type="text" value="' . $display_instance['image_uri_alt'][$i] . '" />';
            $rew_html .= '</p>';

            $rew_html .= '<br><br><div class="widg-img' . $i . '">';
            $show1 = (empty($display_instance['logo'][$i])) ? 'style="display:none;"' : '';
            $rew_html .= '<label id="logo_agilysys_customer_stories_slider_widget' . $i . '"></label><br><img class="' . $this->get_field_id('logo' . $i) . '_media_imageyy' . $i . ' custom_media_imageyy' . $i . '" src="' . $display_instance['logo'][$i] . '" ' . $show1 . ' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('logo' . $i) . '_media_idyy' . $i . ' custom_media_idyy' . $i . '" name="' . $this->get_field_name('logo_id[]') . '" id="' . $this->get_field_id('logo' . $i) . '" value="' . $display_instance['logo_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('logo' . $i) . '_media_urlyy' . $i . ' custom_media_urlyy' . $i . '" name="' . $this->get_field_name('logo[]') . '" id="' . $this->get_field_id('logo' . $i) . '" value="' . $display_instance['logo'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload Logo" class="button custom_media_uploadyy' . $i . '" id="' . $this->get_field_id('logo' . $i) . '"/>';

            $rew_html .= '</div><br><br>';

            ?>

<script>
jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_uploadyy<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {


                    //if (attachment.height == 1080 && attachment.width == 1920) {
                    jQuery('.' + button_id_s + '_media_idyy<?php echo $i; ?>').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_urlyy<?php echo $i; ?>').val(attachment.url);
                    jQuery('.' + button_id_s + '_media_imageyy<?php echo $i; ?>').attr('src',
                        attachment.url).css('display', 'block');

                    /*jQuery('#logo_agilysys_customer_stories_slider_widget<?php echo $i; ?>')
                            .html("");
                    } else {
                        jQuery('#logo_agilysys_customer_stories_slider_widget<?php echo $i; ?>')
                            .html("Please Enter the correct Dimensions 1920x1080").css('color',
                                'red');
                    }
                    */


                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_uploadyy<?php echo $i; ?>');

});
</script>


<?php

            $rew_html .= '<p >';
            $rew_html .= '<label for="' . $this->get_field_id('logo_alt' . $i) . '"> ' . __('Logo Alt', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('logo_alt' . $i) . '" name="' . $this->get_field_name('logo_alt[]') . '" type="text" value="' . $display_instance['logo_alt'][$i] . '" />';
            $rew_html .= '</p>';

            $description = esc_attr($display_instance['description'][$i]);
            $url_text = esc_attr($display_instance['url_text'][$i]);
            $url_link = esc_attr($display_instance['url_link'][$i]);

            $rew_html .= '<p >';
            $rew_html .= '<label for="' . $this->get_field_id('url_text' . $i) . '"> ' . __('Learn More Text', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('url_text' . $i) . '" name="' . $this->get_field_name('url_text[]') . '" type="text" value="' . $url_text . '" />';
            $rew_html .= '</p><br>';

            $thankyou_required = $display_instance['thankyou_required'][$i];

            if ($thankyou_required == "yes") {
                $checked1 = 'checked="checked"';
                $checked2 = '';
            } elseif ($thankyou_required == "no") {
                $checked1 = '';
                $checked2 = 'checked="checked"';
            } else {
                $checked1 = '';
                $checked2 = '';
            }

            $rew_html .= '<p><label style="float:left;">Thank You Page Required</label>';

            $rew_html .= '<span style="float:left;">Yes</span><input style="width:1% !important;float:left;" type="radio" id="' . $this->get_field_id('thankyou_required' . $i) . '" name="' . $this->get_field_name('thankyou_required[]') . '" value="yes" ' . $checked1 . '>';
            $rew_html .= '<span style="float:left;">No</span><input style="width:1% !important;float:left;" type="radio" id="' . $this->get_field_id('thankyou_required' . $i) . '" name="' . $this->get_field_name('thankyou_required[]') . '" value="no" ' . $checked2 . '></p><br><br>';

            $rew_html .= '<div class="widg-img' . $i . '">';
            $show1 = (empty($display_instance['pdf_uri'][$i])) ? 'style="display:none;"' : '';
            $rew_html .= '<label class="' . $this->get_field_id('pdf_id' . $i) . '_media_image' . $i . ' custom_media_image' . $i . '" value="' . $display_instance['pdf_uri'][$i] . '" ' . $show1 . '>' . $display_instance['pdf_uri'][$i] . ' </label>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('pdf_id' . $i) . '_media_id' . $i . ' custom_media_id' . $i . '" name="' . $this->get_field_name('pdf_id[]') . '" id="' . $this->get_field_id('pdf_id' . $i) . '" value="' . $display_instance['pdf_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('pdf_id' . $i) . '_media_url' . $i . ' custom_media_url' . $i . '" name="' . $this->get_field_name('pdf_uri[]') . '" id="' . $this->get_field_id('pdf_uri' . $i) . '" value="' . $display_instance['pdf_uri'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload PDF/Zip File" class="button custom_media_upload' . $i . '" id="' . $this->get_field_id('pdf_id' . $i) . '"/>';

            $rew_html .= '</div><br><br>';

            ?>

<script>
jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {
                    jQuery('.' + button_id_s + '_media_id<?php echo $i; ?>').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_url<?php echo $i; ?>').val(attachment.url);
                    jQuery('.' + button_id_s + '_media_image<?php echo $i; ?>').text(
                        attachment.url).css('display', 'block');
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload<?php echo $i; ?>');

});
</script>


<?php

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('industries' . $i) . '"> ' . __('Industries', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('industries' . $i) . '" name="' . $this->get_field_name('industries[]') . '">';

            $selected_industry = $display_instance['industries'][$i];

            foreach ($industries_arr as $key => $val) {

                if ($key == $selected_industry) {
                    $rew_html .= '<option value="' . $key . '" selected="selected">' . $val . '</option>';
                } else {
                    $rew_html .= '<option value="' . $key . '">' . $val . '</option>';
                }
            }

            $rew_html .= '</select>';
            $rew_html .= '</p><br>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('products' . $i) . '"> ' . __('Products', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('products' . $i) . '" name="' . $this->get_field_name('products[]') . '">';

            $selected_products = $display_instance['products'][$i];

            foreach ($products_arr as $key => $val) {

                if (is_array($val)) {

                    $rew_html .= '<optgroup label="' . $key . '">';

                    foreach ($val as $k => $v) {
                        if ($k == $selected_products) {
                            $rew_html .= '<option value="' . $k . '" selected="selected">' . $v . '</option>';
                        } else {
                            $rew_html .= '<option value="' . $k . '">' . $v . '</option>';
                        }
                    }

                    $rew_html .= '</optgroup>';

                }
            }

            $rew_html .= '</select>';
            $rew_html .= '</p><br>';

            $k = $i + 1;
            $rew_html .= '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' . $k . ');">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';

        }

        $rew_html .= '</div></div>';

        $rew_html .= '<div class="' . $widget_add_id_webinars_info . '" style="margin-bottom: 36px;text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;" onClick="add_new_row();">' . __('ADD ROW', 'ZWREW_TEXT_DOMAIN') . '</div>';

        ?>

<script>
function add_new_row() {
    var cnt = '';

    jQuery.each(jQuery("#entries_agilysys_customer_stories_slider_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            cnt = jQuery(this).val();
        }
    });




    var what_makes_rows = jQuery('.what_makes_rows').val();

    console.log(cnt);

    if (parseInt(cnt) < parseInt(what_makes_rows)) {

        cnt++;

        jQuery.each(jQuery("#entries_agilysys_customer_stories_slider_widget .cnt909"), function() {
            if (jQuery(this).val() != '') {
                jQuery(this).val(cnt);
            }
        });

        var new_row = '<div id="entry' + cnt +
            '"  class="entrys"><span class="entry-title" onclick = "slider(this);"><?php echo __('Add New Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span>';
        new_row += '<div class="entry-desc cf">';


        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Title', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('title[]')); ?>" type="text" value="">';
        new_row += '</p>';


        new_row += '<p >';
        new_row += '<label><?php echo __('Description', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
        new_row +=
            '<textarea rows="6" cols="35"  name="<?php echo $this->get_field_name('description[]'); ?>"></textarea>';
        new_row += '</p><br><br><br><br><br><br>';



        new_row += '<div class="widg-img' + cnt + '">';

        new_row += '<label id="image_uri_agilysys_customer_stories_slider_widget' + cnt +
            '"></label><br><img class="<?php echo $this->get_field_id('slider_image'); ?>' + cnt + '_media_image' +
            cnt +
            ' custom_media_image' + cnt + '" style="display:none;" src="" width=200" height="120"/>';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('slider_image'); ?>' + cnt +
            '_media_id' + cnt + ' custom_media_id' + cnt +
            '" name="<?php echo $this->get_field_name('slider_id[]'); ?>" />';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('slider_image'); ?>' + cnt +
            '_media_url' + cnt + ' custom_media_url' + cnt +
            '" name="<?php echo $this->get_field_name('slider_image[]'); ?>" value="">';
        new_row +=
            '<input type="button" value="Upload Slider Image" id="<?php echo $this->get_field_id('slider_image'); ?>' +
            cnt + '" class="button custom_media_upload' + cnt + '" />';

        new_row += '</div><br><br>';


        jQuery(document).ready(function() {




            function media_upload(button_class) {
                var _custom_media = true,
                    _orig_send_attachment = wp.media.editor.send.attachment;
                jQuery('body').on('click', '.custom_media_upload' + cnt, function(e) {
                    var button_id = '#' + jQuery(this).attr('id');
                    var button_id_s = jQuery(this).attr('id');
                    console.log(button_id);
                    var self = jQuery(button_id);
                    var send_attachment_bkp = wp.media.editor.send.attachment;
                    var button = jQuery(button_id);
                    var id = button.attr('id').replace('_button', '');
                    _custom_media = true;

                    wp.media.editor.send.attachment = function(props, attachment) {
                        if (_custom_media) {

                            // if (attachment.height == 1080 && attachment.width == 1920) {
                            jQuery('.' + button_id_s + '_media_id' + cnt).val(attachment.id);
                            jQuery('.' + button_id_s + '_media_url' + cnt).val(attachment.url);
                            jQuery('.' + button_id_s + '_media_image' + cnt).attr('src',
                                attachment.url).css('display', 'block');

                            /*jQuery('#image_uri_agilysys_customer_stories_slider_widget' + cnt)
                                    .html("");
                            } else {
                                jQuery('#image_uri_agilysys_customer_stories_slider_widget' + cnt)
                                    .html("Please Enter the correct Dimensions 1920x1080").css(
                                        'color', 'red');

                            }
                            */
                        } else {
                            return _orig_send_attachment.apply(button_id, [props, attachment]);
                        }
                    }
                    wp.media.editor.open(button);
                    return false;
                });
            }
            media_upload('.custom_media_upload' + cnt);

        });

        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Image Alt', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';

        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('image_uri_alt[]')); ?>" type="text" value="">';
        new_row += '</p>';



        new_row += '<br><br><div class="widg-img' + cnt + '">';

        new_row += '<label id="logo_agilysys_customer_stories_slider_widget' + cnt +
            '"></label><br><img class="<?php echo $this->get_field_id('logo'); ?>' + cnt + '_media_imageyy' + cnt +
            ' custom_media_imageyy' + cnt + '" src="" style="display:none;" width=200" height="120"/>';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('logo'); ?>' + cnt + '_media_idyy' +
            cnt + 'custom_media_idyy' + cnt + '" name="<?php echo $this->get_field_name('logo_id[]'); ?>" />';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('logo'); ?>' + cnt + '_media_urlyy' +
            cnt + ' custom_media_urlyy' + cnt + '" name="<?php echo $this->get_field_name('logo[]'); ?>">';
        new_row += '<input type="button" value="Upload Logo" id="<?php echo $this->get_field_id('logo'); ?>' + cnt +
            '" class="button custom_media_uploadyy' + cnt + '"/>';

        new_row += '</div><br><br>';


        jQuery(document).ready(function() {




            function media_uploadabc(button_class) {
                var _custom_media = true,
                    _orig_send_attachment = wp.media.editor.send.attachment;
                jQuery('body').on('click', '.custom_media_uploadyy' + cnt, function(e) {
                    var button_id = '#' + jQuery(this).attr('id');
                    var button_id_s = jQuery(this).attr('id');
                    console.log(button_id);
                    var self = jQuery(button_id);
                    var send_attachment_bkp = wp.media.editor.send.attachment;
                    var button = jQuery(button_id);
                    var id = button.attr('id').replace('_button', '');
                    _custom_media = true;

                    wp.media.editor.send.attachment = function(props, attachment) {
                        if (_custom_media) {

                            //if (attachment.height == 1080 && attachment.width == 1920) {
                            jQuery('.' + button_id_s + '_media_idyy' + cnt).val(attachment.id);
                            jQuery('.' + button_id_s + '_media_urlyy' + cnt).val(attachment.url);
                            jQuery('.' + button_id_s + '_media_imageyy' + cnt).attr('src',
                                attachment.url).css('display', 'block');

                            /*jQuery('#logo_agilysys_customer_stories_slider_widget' + cnt)
                                    .html("");
                            } else {
                                jQuery('#logo_agilysys_customer_stories_slider_widget' + cnt)
                                    .html("Please Enter the correct Dimensions 1920x1080").css(
                                        'color', 'red');

                            }
                            */


                        } else {
                            return _orig_send_attachment.apply(button_id, [props, attachment]);
                        }
                    }
                    wp.media.editor.open(button);
                    return false;
                });
            }
            media_uploadabc('.custom_media_uploadyy' + cnt);

        });



        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Logo Alt', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';

        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('logo_alt[]')); ?>" type="text" value="">';
        new_row += '</p>';

        new_row += '<p >';
        new_row += '<label><?php echo __('Learn More Text', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
        new_row += '<input name="<?php echo $this->get_field_name('url_text[]'); ?>" type="text"  />';
        new_row += '</p><br>';


        new_row += '<p><label style="float:left;">Thank You Page Required</label>';

        new_row +=
            '<span style="float:left;">Yes</span><input style="width:1% !important;float:left;" type="radio"  name="<?php echo $this->get_field_name('thankyou_required[]'); ?> " value="yes">';
        new_row +=
            '<span style="float:left;">No</span><input style="width:1% !important;float:left;" type="radio" name="<?php echo $this->get_field_name('thankyou_required[]'); ?>" value="no"></p><br><br>';


        new_row += '<div class="widg-img' + cnt + '">';

        new_row += '<label id="pdf_uri_yyxxzz_' + cnt + '"> </label>';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('pdf_uri'); ?>' + cnt + '_media_idyyxx' +
            cnt + 'custom_media_idyyxx' + cnt + '" name="<?php echo $this->get_field_name('pdf_id[]'); ?>" />';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('pdf_uri'); ?>' + cnt +
            '_media_urlyyxx' +
            cnt + ' custom_media_urlyyxx' + cnt + '" name="<?php echo $this->get_field_name('pdf_uri[]'); ?>">';
        new_row += '<input type="button" value="Upload Pdf / Zip" id="<?php echo $this->get_field_id('pdf_uri'); ?>' +
            cnt +
            '" class="button custom_media_uploadyyxx' + cnt + '"/>';

        new_row += '</div><br><br>';


        jQuery(document).ready(function() {




function media_uploadabcxx(button_class) {
    var _custom_media = true,
        _orig_send_attachment = wp.media.editor.send.attachment;
    jQuery('body').on('click', '.custom_media_uploadyyxx' + cnt, function(e) {
        var button_id = '#' + jQuery(this).attr('id');
        var button_id_s = jQuery(this).attr('id');
        console.log(button_id);
        var self = jQuery(button_id);
        var send_attachment_bkp = wp.media.editor.send.attachment;
        var button = jQuery(button_id);
        var id = button.attr('id').replace('_button', '');
        _custom_media = true;

        wp.media.editor.send.attachment = function(props, attachment) {
            if (_custom_media) {


                jQuery('.' + button_id_s + '_media_idyyxx' + cnt).val(attachment.id);
                jQuery('.' + button_id_s + '_media_urlyyxx' + cnt).val(attachment.url);
                jQuery('#pdf_uri_yyxxzz_' + cnt).text(attachment.url);

            } else {
                return _orig_send_attachment.apply(button_id, [props, attachment]);
            }
        }
        wp.media.editor.open(button);
        return false;
    });
}
media_uploadabcxx('.custom_media_uploadyyxx' + cnt);

});


        new_row += '<p>';
        new_row += '<label> <?php echo __('Industries', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
        new_row += '<select  name="<?php echo $this->get_field_name('industries[]'); ?>">';


        <?php
foreach ($industries_arr as $key => $val) {
            ?>

        new_row += '<option value="<?php echo $key; ?>"><?php echo $val; ?></option>';
        <?php
}
        ?>
        new_row += '</select>';
        new_row += '</p><br>';

        new_row += '<p>';
        new_row += '<label> <?php echo __('Products', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
        new_row += '<select name="<?php echo $this->get_field_name('products[]'); ?>">';


        <?php
foreach ($products_arr as $key => $val) {

            if (is_array($val)) {

                ?>

        new_row += '<optgroup label="<?php echo $key; ?>">';


        <?php
foreach ($val as $k => $v) {

                    ?>

        new_row += '<option value="<?php echo $k; ?>"><?php echo $v; ?></option>';

        <?php

                }

                ?>

        new_row += '</optgroup>';

        <?php

            }

            ?>
        <?php

        }

        ?>

        new_row += '</select>';
        new_row += '</p><br>';




        var new_cnt = cnt;

        new_row += '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' + new_cnt +
            ');"><?php echo __('Delete Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span></a></p>';
        new_row += '</div></div>';

        jQuery('.add_new_rowxx-input-containers #entries_agilysys_customer_stories_slider_widget').append(new_row);

    }


}

function show_hide_div_agilysys_customer_stories_slider_widget(val, i) {
    console.log(val);
    if (val == 'page') {
        jQuery("#page_div_agilysys_customer_stories_slider_widget" + i).show();
        jQuery("#link_div_agilysys_customer_stories_slider_widget" + i).hide();
    } else if (val == 'link') {
        jQuery("#page_div_agilysys_customer_stories_slider_widget" + i).hide();
        jQuery("#link_div_agilysys_customer_stories_slider_widget" + i).show();
    }

}

function delete_row(cnt) {
    jQuery.each(jQuery(".add_new_rowxx-input-containers #entries_agilysys_customer_stories_slider_widget"), function() {
        jQuery(' #entry' + cnt).remove();
    });
    var last_cnt = 0;

    jQuery.each(jQuery("#entries_agilysys_customer_stories_slider_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            last_cnt = jQuery(this).val();
        }
    });

    last_cnt--;
    jQuery.each(jQuery("#entries_agilysys_customer_stories_slider_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(last_cnt);
        }
    });
    jQuery(".what_makes_rows").val(last_cnt);
    jQuery('.what_makes_rows').trigger('change');

}
</script>
<style>
#rew_container_agilysys_customer_stories_slider_widget p {
    padding: 20px !important;
}

.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_customer_stories_slider_widget select {
    float: left;
    width: 60%;
    margin-top: 20px !important;
    margin-bottom: 10px !important;
}

#rew_container_agilysys_customer_stories_slider_widget input,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_customer_stories_slider_widget label {
    width: 40%;
    float: left;
}

<?php echo '.'. $widget_add_id_webinars_info;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}

#entries_agilysys_customer_stories_slider_widget #remove-img-bg {
    background: url('<?php echo ZWREW_URL; ?>assets/images/deleteimg.png') center center no-repeat;
    width: 20px;
    height: 22px;
    display: none;
}

#entries_agilysys_customer_stories_slider_widget {
    padding: 10px 0 0;
}

#entries_agilysys_customer_stories_slider_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_customer_stories_slider_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_customer_stories_slider_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_customer_stories_slider_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_customer_stories_slider_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_customer_stories_slider_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_customer_stories_slider_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_customer_stories_slider_widget #entries_agilysys_customer_stories_slider_widget plast label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_customer_stories_slider_widget">
    <?php echo $rew_html; ?>
</div>

<?php

    }

}
<?php

function load_agilysys_bod_leadership_widget()
{
    register_widget('agilysys_bod_leadership_widget');
}

add_action('widgets_init', 'load_agilysys_bod_leadership_widget');

class agilysys_bod_leadership_widget extends WP_Widget
{
/**
 * constructor -- name this the same as the class above
 */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Bod Leadership Widget', 'AGILYSYS_TEXT_DOMAIN'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {

       
        $section_title = $instance['section_title'];
       // echo $args['before_widget'];
        ?>


<section class="bodSection greyBG">
    <style>
        
        .modal{
           margin-top:-40%;
        }
        
        .modal-header{
            position: relative;
            
        }
          .modal-lg {
                max-width: 80%;
            }
            
            
        .leadershipPopup .modal-dialog {
            -webkit-transform: translate(0,-50%);
            -o-transform: translate(0,-50%);
            transform: translate(0,-50%);
            top: 57%;
            margin: 0 auto;
            position: relative;
            border: 5px solid #fff
        }
        

        .leadersImg{
            text-align:center;
        }
        
            .leadersDescription {
                display: flex;
                align-items: center;
            }
                .leadershipPopup .modal-content{
                        background:  #F8901F;
                        border-radius:0;
                        color:#fff;
                        
                }
        
        @media only screen and (min-width:768px) and (max-width:768px){
            .modal.leadershipPopup {
                margin-top: -80%;
            }
        }      
        
        @media only screen and (min-width:367px) and (max-width:768px){
        
            .leadershipPopup .modal-dialog {
                 top: 31%;
                 text-align: center;
            }
            
            .modal.leadershipPopup {
                    margin-top: -40%;
                }
            
             .modal-lg {
                            max-width: 100%;
                        }
                        .leadersImg {
                text-align: center;
                width: 75%;
                margin: 0 auto;
            }
            
            .bodLeaderShip {
                width: 100%;
                left: 0;
            }
            
            .bodBox {
                width: 46%;
            }
        }
        
    </style>
    <h2 class="greenText dinProStd h2 center"><?php echo $section_title; ?></h2>

    <div class="bodLeaderShip flex">
        <?php
        
        $count = count($instance['image_uri']);

        for ($i = 0; $i < $count; $i++) {

         

                $image_uri = $instance['image_uri'][$i];
                $name = $instance['name'][$i];
                $designation = $instance['designation'][$i];
                $description = $instance['description'][$i];

                ?>
       <script>
             jQuery(document).ready(function() {
            jQuery("#bodBox<?php echo $i; ?>").click(function() {
                jQuery("#back<?php echo $i; ?>").modal('show');
                jQuery("#back<?php echo $i; ?>").css("display", "block");
                jQuery(".modal-backdrop").css("display", "block");
            });
            
            jQuery(".close").click(function(){
                 jQuery("#back<?php echo $i; ?>").modal('hide');
                jQuery("#back<?php echo $i; ?>").css("display", "none");
                jQuery(".modal-backdrop").css("display", "none");
             });
        });
        </script>
        <div class="bodBox" id="bodBox<?php echo $i; ?>">
            <div class="front" id="front<?php echo $i; ?>">
                <div class="bodBoxImg">
                    <img class="img-fluid" src="<?php echo $image_uri; ?>" alt="" />
                </div>
                <div class="bodBoxName">
                    <h4 class="greenText dinProStd"><?php echo $name; ?></h4>
                    <p><?php echo $designation; ?></p>
                </div>
            </div>
            
        </div>



        
        
         <div id="back<?php echo $i; ?>" class="modal leadershipPopup fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>  
                </div>
                <div class="modal-body">
                        <div class="row">
                            <div class="col-md-3 leadersImg">
                                <div class="modalImg" style=" display: flex;    align-items: center;    flex-direction: column;">
                          <img class="img-fluid" src="<?php echo $image_uri; ?>" alt="" />
                           <h4 style="position:relative;top:5px;" class="dinProStd"><?php echo $name; ?></h4>
                             <p><?php echo $designation; ?></p>
                            </div>
                                
                            </div>
                            <div class="col-md-9 leadersDescription" >
                                <p><?php echo $description; ?></p>          
                            </div>
                        </div>
                        

                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  
                </div>
            </div>
        </div>
    </div>
      
        <?php

        }

        ?>
  
    </div>
   
</section>


<?php
//echo $args['after_widget'];
    }

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $max_entries_agilysys_bod_leadership_widget_slider_image = 15;

        $instance['section_title'] = strip_tags($new_instance['section_title']);
        $instance['what_makes_rows'] = strip_tags($new_instance['what_makes_rows']);

        $count = count($new_instance['name']);

        for ($i = 0; $i < $count; $i++) {

            $instance['image_uri'][$i] = $new_instance['image_uri'][$i];
            $instance['image_uri_alt'][$i] = $new_instance['image_uri_alt'][$i];

            $instance['name'][$i] = $new_instance['name'][$i];
            $instance['designation'][$i] = $new_instance['designation'][$i];
            $instance['description'][$i] = $new_instance['description'][$i];

        }
        return $instance;
    }

    public function form($display_instance)
    {

        $widget_add_id_slider = $this->get_field_id('') . "add_agilysys_bod_leadership_widget";

        $title = ($display_instance['section_title']);

        if (!empty($display_instance['what_makes_rows'])) {
            $what_makes_rows = ($display_instance['what_makes_rows']);
        } else {
            $what_makes_rows = 0;
        }

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('section_title') . '"> ' . __('Slider Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('section_title') . '" name="' . $this->get_field_name('section_title') . '" type="text" value="' . $title . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('what_makes_rows') . '"> ' . __('No. of rows do you want to add*', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input class="what_makes_rows" id="' . $this->get_field_name('what_makes_rows') . '" name="' . $this->get_field_name('what_makes_rows') . '" type="number" value="' . $what_makes_rows . '" />';
        $rew_html .= '</p>';

        $count = count($display_instance['name']);

        $rew_html .= '<div class="add_new_rowxx-input-containers"><div id="entries_agilysys_bod_leadership_widget">';

        $rew_html .= '<input class="cnt909" id="cnt909" name="cnt" type="hidden" value="' . $count . '">';

        for ($i = 0; $i < $count; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'AGILYSYS_TEXT_DOMAIN') . ' </span>';

            $rew_html .= '<div class="entry-desc cf">';

            $rew_html .= '<div class="widg-img' . $i . '">';
            $show1 = (empty($display_instance['image_uri'][$i])) ? 'style="display:none;"' : '';
            $rew_html .= '<label id="image_uri_agilysys_bod_leadership_widget' . $i . '"></label><br><img class="' . $this->get_field_id('image_id' . $i) . '_media_image' . $i . ' custom_media_image' . $i . '" src="' . $display_instance['image_uri'][$i] . '" ' . $show1 . ' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id' . $i) . '_media_id' . $i . ' custom_media_id' . $i . '" name="' . $this->get_field_name('image_id[]') . '" id="' . $this->get_field_id('image_id' . $i) . '" value="' . $display_instance['image_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id' . $i) . '_media_url' . $i . ' custom_media_url' . $i . '" name="' . $this->get_field_name('image_uri[]') . '" id="' . $this->get_field_id('image_uri' . $i) . '" value="' . $display_instance['image_uri'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_upload' . $i . '" id="' . $this->get_field_id('image_id' . $i) . '"/>';

            $rew_html .= '</div><br>';

            ?>
<script>
jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    if (attachment.height == 550 && attachment.width == 550) {
                        jQuery('.' + button_id_s + '_media_id<?php echo $i; ?>').val(attachment.id);
                        jQuery('.' + button_id_s + '_media_url<?php echo $i; ?>').val(attachment
                            .url);
                        jQuery('.' + button_id_s + '_media_image<?php echo $i; ?>').attr('src',
                            attachment.url).css('display', 'block');
                        jQuery('#image_uri_agilysys_bod_leadership_widget<?php echo $i; ?>')
                            .html("");
                    } else {
                        jQuery('#image_uri_agilysys_bod_leadership_widget<?php echo $i; ?>')
                            .html("Please Enter the correct Dimensions 550x550").css('color',
                                'red');
                    }

                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload<?php echo $i; ?>');

});
</script>

<?php

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt' . $i) . '"> ' . __('Image Alt', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt' . $i) . '" name="' . $this->get_field_name('image_uri_alt[]') . '" type="text" value="' . $display_instance['image_uri_alt'][$i] . '">';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('name' . $i) . '"> ' . __('Name', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('name' . $i) . '" name="' . $this->get_field_name('name[]') . '" type="text" value="' . $display_instance['name'][$i] . '">';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('designation' . $i) . '"> ' . __('Designation', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('designation' . $i) . '" name="' . $this->get_field_name('designation[]') . '" type="text" value="' . $display_instance['designation'][$i] . '">';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('description' . $i) . '"> ' . __('Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<textarea rows="6" cols="35" id="' . $this->get_field_id('description' . $i) . '" name="' . $this->get_field_name('description[]') . '">' . $display_instance['description'][$i] . '</textarea>';
            $rew_html .= '</p><br><br><br><br>';

            $k = $i + 1;
            $rew_html .= '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' . $k . ');">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';

        }

        $rew_html .= '</div></div>';

        $rew_html .= '<div class="' . $widget_add_id_slider . '" style="text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;" onClick="add_new_row();">' . __('ADD ROW', 'AGILYSYS_TEXT_DOMAIN') . '</div>';

        ?>
<script>
function add_new_row() {
    var cnt = '';

    jQuery.each(jQuery("#entries_agilysys_bod_leadership_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            cnt = jQuery(this).val();
        }
    });




    var what_makes_rows = jQuery('.what_makes_rows').val();


 console.log(cnt);  
    if (parseInt(cnt) < parseInt(what_makes_rows)) {

        cnt++;



        jQuery.each(jQuery("#entries_agilysys_bod_leadership_widget .cnt909"), function() {
            if (jQuery(this).val() != '') {
                jQuery(this).val(cnt);
            }
        });

        var new_row = '<div id="entry' + cnt +
            '"  class="entrys"><span class="entry-title" onclick = "slider(this);"><?php echo __('Add New Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span>';
        new_row += '<div class="entry-desc cf">';


        new_row += '<div class="widg-img' + cnt + '">';
        new_row += '<label id="image_uri_agilysys_bod_leadership_widget' + cnt +'"></label><br><img class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_image' + cnt +' custom_media_image' + cnt + '" src="" width=200" height="120" style="display:none;">';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_url' +cnt + ' custom_media_url' + cnt +
            '" name="<?php echo $this->get_field_name('image_uri[]'); ?>" id="<?php echo $this->get_field_id('image_uri'); ?>' +cnt + '" value="">';
        new_row += '<input type="button" value="Upload Image" class="button custom_media_upload' + cnt + '" id="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '"/>';
        new_row += '</div><br><br>';


        jQuery(document).ready(function() {




            function media_upload(button_class) {
                var _custom_media = true,
                    _orig_send_attachment = wp.media.editor.send.attachment;
                jQuery('body').on('click', '.custom_media_upload' + cnt, function(e) {
                    var button_id = '#' + jQuery(this).attr('id');
                    var button_id_s = jQuery(this).attr('id');
                    console.log(button_id);
                    var self = jQuery(button_id);
                    var send_attachment_bkp = wp.media.editor.send.attachment;
                    var button = jQuery(button_id);
                    var id = button.attr('id').replace('_button', '');
                    _custom_media = true;

                    wp.media.editor.send.attachment = function(props, attachment) {
                        if (_custom_media) {

                            if (attachment.height == 550 && attachment.width == 550) {

                            jQuery('.' + button_id_s + '_media_id' + cnt).val(attachment.id);


                            jQuery('.' + button_id_s + '_media_url' + cnt).val(attachment.url);
                            jQuery('.' + button_id_s + '_media_image' + cnt).attr('src',
                                attachment.url).css('display', 'block');
                                jQuery('#image_uri_agilysys_bod_leadership_widget' + cnt)
                                    .html("");
                            }
                            else
                            {
                                jQuery('#image_uri_agilysys_bod_leadership_widget' + cnt)
                                    .html("Please Enter the correct Dimensions 550x550").css(
                                        'color', 'red');
                            }
                        } else {
                            return _orig_send_attachment.apply(button_id, [props, attachment]);
                        }
                    }
                    wp.media.editor.open(button);
                    return false;
                });
            }
            media_upload('.custom_media_upload' + cnt);

        });

        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Image Alt', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';

        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('image_uri_alt[]')); ?>" type="text" value="">';
        new_row += '</p>';

        new_row += '<p>';
        
        
        new_row += '<label for=""><?php echo __('Name', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('name[]')); ?>" type="text" value="">';
        new_row += '</p>';

        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Designation', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('designation[]')); ?>" type="text" value="">';
        new_row += '</p>';


        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Description', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



        new_row +=
            '<textarea rows="6" cols="35" name="<?php echo esc_attr($this->get_field_name('description[]')); ?>"></textarea>';
        new_row += '</p><br><br><br><br>';


        var new_cnt = cnt;

        new_row +=
            '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' + new_cnt +
            ');"><?php echo __('Delete Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span></a></p>';
        new_row += '</div></div>';

        jQuery('.add_new_rowxx-input-containers #entries_agilysys_bod_leadership_widget').append(new_row);

    }


}



function delete_row(cnt) {
    jQuery.each(jQuery(".add_new_rowxx-input-containers #entries_agilysys_bod_leadership_widget"), function() {
        jQuery(' #entry' + cnt).remove();
    });
    var last_cnt = 0;

    jQuery.each(jQuery("#entries_agilysys_bod_leadership_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            last_cnt = jQuery(this).val();
        }
    });

    last_cnt--;
    jQuery.each(jQuery("#entries_agilysys_bod_leadership_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(last_cnt);
        }
    });
    jQuery(".what_makes_rows").val(last_cnt);
    jQuery('.what_makes_rows').trigger('change');

}
</script>
<style>
#rew_container_agilysys_bod_leadership_widget p{
    padding:10px;
}
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_bod_leadership_widget input,
select,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_bod_leadership_widget label {
    width: 40%;
}

<?php echo '.' . $widget_add_id_slider;

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}



#entries_agilysys_bod_leadership_widget {
    padding: 10px 0 0;
}

#entries_agilysys_bod_leadership_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_bod_leadership_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_bod_leadership_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_bod_leadership_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_bod_leadership_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_bod_leadership_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_bod_leadership_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_bod_leadership_widget #entries_agilysys_bod_leadership_widget p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_bod_leadership_widget">
    <?php echo $rew_html; ?>
</div>
<?php
} //Function form ends here

}
<?php

add_action('widgets_init', 'agil_load_agilysys_solutions_partners_category_logo_widget');

function agil_load_agilysys_solutions_partners_category_logo_widget()
{
    register_widget('agilysys_solutions_partners_category_logo_widget');
}

class agilysys_solutions_partners_category_logo_widget extends WP_Widget
{

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Solutions Partners Category Logo Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');

    }

    public function widget($args, $instance)
    {

        echo $args['before_widget'];

        $category_arr = array(
            1 => 'Agilysys Resellers',
            2 => 'Booking Engine',
            3 => 'Casino Player Tracking',
            4 => 'Central Reservation Systems',
            5 => 'Channel Management',
            6 => 'Corporate Gaming',
            7 => 'CRM',
            8 => 'Cruise',
            9 => 'Environmental Control',
            10 => 'Foodservice Management',
            11 => 'Giftcards',
            12 => 'Hardware',
            13 => 'Healthcare',
            14 => 'High Availability',
            15 => 'Higher Education',
            16 => 'Hotel Internet Marketing',
            17 => 'Hotel Resort',
            18 => 'In-room Entertainment',
            19 => 'Inventory',
            20 => 'IT Services',
            21 => 'Kiosk',
            22 => 'Marketing and Promotions',
            23 => 'Mobile Payment',
            24 => 'Online Mobile Ordering',
            25 => 'Payment Processing',
            26 => 'PBX',
            27 => 'RFID',
            28 => 'Restaurant',
            29 => 'Software',
            30 => 'Stadium',
            31 => 'Surveillance',
            32 => 'Tribal Gaming',
            33 => 'Vending Solutions',
            34 => 'Yield Management',
        );

        $products_arr = array(
            1 => 'Analytics',
            2 => 'DataMagine',
            3 => 'Eatec',
            4 => 'Elevate',
            5 => 'Golf',
            6 => 'InfoGenesis Flex',
            7 => 'InfoGenesis Mobile',
            8 => 'InfoGenesis POS',
            9 => 'Insight Mobile Manager',
            10 => 'LMS',
            11 => 'ResPAK',
            12 => 'rGuest',
            13 => 'Spa Management',
            14 => 'SWS',
            15 => 'Visual One PMS',
            16 => 'WMx',
        );
        ?>

   <style>

 
  .loader_agilysys_solutions_partners_category_logo_widget {
    width: 80px;
    height: 80px;
    background: #fff;
    border: 2px solid #f3f3f3;
    border-top: 3px solid #008000;
    border-radius: 100%;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 45%;
    margin: auto;
    animation: spin 1s infinite;
    display:none;

}

@keyframes spin {
    from {
        transform: rotate(0deg);
    }

    to {
        transform: rotate(360deg);
    }
}

</style>

 <div id="overlay_agilysys_solutions_partners_category_logo_widget">
        <div class="loader_agilysys_solutions_partners_category_logo_widget"></div>
    </div>
<section class="solPartnerHelp center">
    <h3 class="dinProStd greenText">NEED HELP FINDING THE RIGHT PARTNER ?</h3>
    <p>Search through our roster of world class partners by selecting any combination of the filters below.</p>
    <div class="solPartnerFilterBox flex center">
        <div class="solPartnerFilter flex">
            <div class="solPartnerFilterTitle">
                <p>Category Of Business</p>
            </div>
            <select class="solPartnerCat filter1" name="category"
                id="category_agilysys_solutions_partners_category_logo_widget"
                onChange="fetch_data_agilysys_solutions_partners_category_logo_widget();">
                <option value="">Select Category</option>
                <?php
foreach ($category_arr as $key => $val) {
            ?>
                <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                <?php
}
        ?>



            </select>
        </div>
        <div class="solPartnerFilter flex">
            <div class="solPartnerFilterTitle">
                <p>Product to integrate with:</p>
            </div>
            <select class="solPartnerPro filter2" name="products"
                id="products_agilysys_solutions_partners_category_logo_widget"
                onChange="fetch_data_agilysys_solutions_partners_category_logo_widget();">
                <option value="">Select Product</option>
                <?php
foreach ($products_arr as $key => $val) {
            ?>
                <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                <?php
}
        ?>

            </select>
        </div>
    </div>

    <div class="solPartnerLogo flex" id="articleList_agilysys_solutions_partners_category_logo_widget">

        <?php

        $cnt = 0;

        $count = count($instance['category']);
        for ($i = 0; $i < $count; $i++) {

            if ($cnt < 30) {
                ?>
        <div class="solPartnerLogoImg">
            <img class="img-fluid" src="<?php echo $instance['image_uri'][$i]; ?>"
                alt="<?php echo $instance['image_uri_alt'][$i]; ?>" />
        </div>
        <?php
$cnt++;
            }
        }
        ?>
    </div>
</section>
<div class="container-fluid" style="margin-top:10%;margin-bottom:10%;">
    <div class="col-ld-12" style="text-align:center;">
        <a class="homeBannerButton whiteText" onclick="view_all_agilysys_solutions_partners_category_logo_widget();">View All <i
        class="fa fa-arrow-right" aria-hidden="true"></i></a>
    </div>
</div>

<script>
function view_all_agilysys_solutions_partners_category_logo_widget() {
    
      jQuery(".loader_agilysys_solutions_partners_category_logo_widget").fadeIn("slow");
    var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

    // Data to receive from our server
    // the value in 'action' is the key that will be identified by the 'wp_ajax_' hook
    var data = {

        action: "fetch-data-agilysys-solutions-partners-category-logo-widget-view-all",

        length: '<?php echo $length; ?>',
        data: '<?php echo json_encode($instance); ?>',
    };


    // Send the data
    jQuery.post(ajaxurl, data, function(response) {

        if (response != 0) {
            jQuery('#articleList_agilysys_solutions_partners_category_logo_widget').html(response);
        }


  jQuery(".loader_agilysys_solutions_partners_category_logo_widget").fadeOut("slow");


    });
}

function fetch_data_agilysys_solutions_partners_category_logo_widget() {
    jQuery(".loader_agilysys_solutions_partners_category_logo_widget").fadeIn("slow");
    var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

    // Data to receive from our server
    // the value in 'action' is the key that will be identified by the 'wp_ajax_' hook
    var data = {

        action: "fetch-data-agilysys-solutions-partners-category-logo-widget",

        category: jQuery('#category_agilysys_solutions_partners_category_logo_widget').val(),
        products: jQuery('#products_agilysys_solutions_partners_category_logo_widget').val(),
        length: '<?php echo $length; ?>',
        data: '<?php echo json_encode($instance); ?>',
    };


    // Send the data
    jQuery.post(ajaxurl, data, function(response) {

        if (response != 0) {
            jQuery('#articleList_agilysys_solutions_partners_category_logo_widget').html(response);
        } else {
            jQuery('#articleList_agilysys_solutions_partners_category_logo_widget').html('No porducts found');
        }


  jQuery(".loader_agilysys_solutions_partners_category_logo_widget").fadeOut("slow");

    });
}
</script>


<?php
echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {

        $instance = array();

        $count = count($new_instance['category']);

        for ($i = 0; $i < $count; $i++) {

            $instance['category'][$i] = $new_instance['category'][$i];
            $instance['products'][$i] = $new_instance['products'][$i];
            $instance['image_uri'][$i] = $new_instance['image_uri'][$i];
            $instance['image_uri_alt'][$i] = $new_instance['image_uri_alt'][$i];

        }

        return $instance;

    }

    public function form($display_instance)
    {

        $widget_add_id_what_makes = $this->get_field_id('') . "add_agilysys_solutions_partners_category_logo_widget";

        $category_arr = array(
            1 => 'Agilysys Resellers',
            2 => 'Booking Engine',
            3 => 'Casino Player Tracking',
            4 => 'Central Reservation Systems',
            5 => 'Channel Management',
            6 => 'Corporate Gaming',
            7 => 'CRM',
            8 => 'Cruise',
            9 => 'Environmental Control',
            10 => 'Foodservice Management',
            11 => 'Giftcards',
            12 => 'Hardware',
            13 => 'Healthcare',
            14 => 'High Availability',
            15 => 'Higher Education',
            16 => 'Hotel Internet Marketing',
            17 => 'Hotel Resort',
            18 => 'In-room Entertainment',
            19 => 'Inventory',
            20 => 'IT Services',
            21 => 'Kiosk',
            22 => 'Marketing and Promotions',
            23 => 'Mobile Payment',
            24 => 'Online Mobile Ordering',
            25 => 'Payment Processing',
            26 => 'PBX',
            27 => 'RFID',
            28 => 'Restaurant',
            29 => 'Software',
            30 => 'Stadium',
            31 => 'Surveillance',
            32 => 'Tribal Gaming',
            33 => 'Vending Solutions',
            34 => 'Yield Management',
        );

        $products_arr = array(
            1 => 'Analytics',
            2 => 'DataMagine',
            3 => 'Eatec',
            4 => 'Elevate',
            5 => 'Golf',
            6 => 'InfoGenesis Flex',
            7 => 'InfoGenesis Mobile',
            8 => 'InfoGenesis POS',
            9 => 'Insight Mobile Manager',
            10 => 'LMS',
            11 => 'ResPAK',
            12 => 'rGuest',
            13 => 'Spa Management',
            14 => 'SWS',
            15 => 'Visual One PMS',
            16 => 'WMx',
        );

        $count = count($display_instance['category']);

        $rew_html .= '<div class="add_new_rowxx-input-containers"><div id="entries_agilysys_solutions_partners_category_logo_widget">';

        $rew_html .= '<input class="cnt909" id="cnt909" name="cnt" type="hidden" value="' . $count . '">';

        for ($i = 0; $i < $count; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'AGILYSYS_TEXT_DOMAIN') . ' </span>';
            $rew_html .= '<div class="entry-desc cf">';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('category' . $i) . '"> ' . __('Category', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('category' . $i) . '" name="' . $this->get_field_name('category[]') . '">';

            $selected_category = $display_instance['category'][$i];

            foreach ($category_arr as $key => $val) {

                if ($key == $selected_category) {
                    $rew_html .= '<option value="' . $key . '" selected="selected">' . $val . '</option>';
                } else {
                    $rew_html .= '<option value="' . $key . '">' . $val . '</option>';
                }
            }

            $rew_html .= '</select>';
            $rew_html .= '</p><br><br><br><br>';

            $rew_html .= '<div class="widg-img' . $i . '">';
            $show1 = (empty($display_instance['image_uri'][$i])) ? 'style="display:none;"' : '';
            $rew_html .= '<label id="image_uri_agilysys_solutions_partners_category_logo_widget' . $i . '"></label><br><img class="' . $this->get_field_id('image_id' . $i) . '_media_image' . $i . ' custom_media_image' . $i . '" src="' . $display_instance['image_uri'][$i] . '" ' . $show1 . ' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id' . $i) . '_media_id' . $i . ' custom_media_id' . $i . '" name="' . $this->get_field_name('image_id[]') . '" id="' . $this->get_field_id('image_id' . $i) . '" value="' . $display_instance['image_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id' . $i) . '_media_url' . $i . ' custom_media_url' . $i . '" name="' . $this->get_field_name('image_uri[]') . '" id="' . $this->get_field_id('image_uri' . $i) . '" value="' . $display_instance['image_uri'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_upload' . $i . '" id="' . $this->get_field_id('image_id' . $i) . '"/>';

            $rew_html .= '</div><br><br><br><br>';

            ?>


<script>
jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                   
                        jQuery('.' + button_id_s + '_media_id<?php echo $i; ?>').val(attachment.id);
                        jQuery('.' + button_id_s + '_media_url<?php echo $i; ?>').val(attachment
                            .url);
                        jQuery('.' + button_id_s + '_media_image<?php echo $i; ?>').attr('src',
                            attachment.url).css('display', 'block');

                        jQuery(
                                '#image_uri_agilysys_solutions_partners_category_logo_widget<?php echo $i; ?>'
                            )
                            .html("");
                    
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload<?php echo $i; ?>');

});
</script>
<?php

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt' . $i) . '"> ' . __('Image Alt', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt' . $i) . '" name="' . $this->get_field_name('image_uri_alt[]') . '" type="text" value="' . $display_instance['image_uri_alt'][$i] . '">';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('products' . $i) . '"> ' . __('Products', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('products' . $i) . '" name="' . $this->get_field_name('products[]') . '">';

            $selected_product = $display_instance['products' . $i];

            foreach ($products_arr as $key => $val) {

                if ($key == $selected_product) {
                    $rew_html .= '<option value="' . $key . '" selected="selected">' . $val . '</option>';
                } else {
                    $rew_html .= '<option value="' . $key . '">' . $val . '</option>';
                }
            }

            $rew_html .= '</select>';
            $rew_html .= '</p>';

            $k = $i + 1;
            $rew_html .= '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' . $k . ');">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';
        }
        $rew_html .= '</div></div>';

        $rew_html .= '<div id="message">' . __('Sorry, you reached to the limit of', 'AGILYSYS_TEXT_DOMAIN') . ' 500 ' . __('maximum entries_agilysys_solutions_partners_category_logo_widget', 'AGILYSYS_TEXT_DOMAIN') . '.</div>';

        $rew_html .= '<div class="' . $widget_add_id_what_makes . '" style="margin-bottom: 36px;text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;" onClick="add_new_row();">' . __('ADD ROW', 'ZWREW_TEXT_DOMAIN') . '</div>';
        ?>


<script>
function add_new_row() {
    var cnt = '';

    jQuery.each(jQuery("#entries_agilysys_solutions_partners_category_logo_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            cnt = jQuery(this).val();
        }
    });





    cnt++;

    jQuery.each(jQuery("#entries_agilysys_solutions_partners_category_logo_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(cnt);
        }
    });
    
    console.log(cnt);

    var new_row = '<div id="entry' + cnt +
        '"  class="entrys"><span class="entry-title" onclick = "slider(this);"><?php echo __('Add New Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span>';
    new_row += '<div class="entry-desc cf">';

    new_row += '<p>';
    new_row += '<label><?php echo __('Category', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
    new_row += '<select name="<?php echo $this->get_field_name('category[]'); ?>">';


    <?php

        foreach ($category_arr as $key => $val) {
            ?>

    new_row += '<option value="<?php echo $key; ?>"><?php echo $val; ?></option>';

    <?php
}
        ?>

    new_row += '</select>';
    new_row += '</p><br><br><br><br>';



    new_row += '<div class="widg-img' + cnt + '">';
    new_row += '<label id="image_uri_agilysys_solutions_partners_category_logo_widget' + cnt +
        '"></label><br><img class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_image' + cnt +
        ' custom_media_image' + cnt + '" src="" style="display:none;" width=200" height="120"/>';
    new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_id' +
        cnt + ' custom_media_id' + cnt + '" name="<?php echo $this->get_field_name('image_id[]'); ?>"  value="" />';
    new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_url' +
        cnt + ' custom_media_url' + cnt +
        '" name="<?php echo $this->get_field_name('image_uri[]'); ?>" id="<?php echo $this->get_field_id('image_id'); ?>' +
        cnt + '" value="">';
    new_row += '<input type="button" value="Upload Image" class="button custom_media_upload' + cnt +
        '" id="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '"/>';
    new_row += '</div><br><br>';


    jQuery(document).ready(function() {




        function media_upload(button_class) {
            var _custom_media = true,
                _orig_send_attachment = wp.media.editor.send.attachment;
            jQuery('body').on('click', '.custom_media_upload' + cnt, function(e) {
                var button_id = '#' + jQuery(this).attr('id');
                var button_id_s = jQuery(this).attr('id');
                console.log(button_id);
                var self = jQuery(button_id);
                var send_attachment_bkp = wp.media.editor.send.attachment;
                var button = jQuery(button_id);
                var id = button.attr('id').replace('_button', '');
                _custom_media = true;

                wp.media.editor.send.attachment = function(props, attachment) {
                    if (_custom_media) {


                       
                            jQuery('.' + button_id_s + '_media_id' + cnt).val(attachment.id);
                            jQuery('.' + button_id_s + '_media_url' + cnt).val(attachment.url);
                            jQuery('.' + button_id_s + '_media_image' + cnt).attr('src',
                                attachment.url).css('display', 'block');


                            jQuery('#image_uri_agilysys_solutions_partners_category_logo_widget' +
                                    cnt)
                                .html("");
                       
                    } else {
                        return _orig_send_attachment.apply(button_id, [props, attachment]);
                    }
                }
                wp.media.editor.open(button);
                return false;
            });
        }
        media_upload('.custom_media_upload' + cnt);

    });

    new_row += '<p>';
    new_row += '<label for=""><?php echo __('Image Alt', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';

    new_row +=
        '<input class="" name="<?php echo esc_attr($this->get_field_name('image_uri_alt[]')); ?>" type="text" value="">';
    new_row += '</p>';


    new_row += '<p>';
    new_row += '<label><?php echo __('Products', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
    new_row += '<select  name="<?php echo $this->get_field_name('products[]'); ?>">';


    <?php
foreach ($products_arr as $key => $val) {
            ?>

    new_row += '<option value="<?php echo $key; ?>"><?php echo $val; ?></option>';


    <?php

        }
        ?>
    new_row += '</select>';
    new_row += '</p>';







    var new_cnt = cnt;

    new_row += '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' + new_cnt +
        ');"><?php echo __('Delete Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span></a></p>';
    new_row += '</div></div>';

    jQuery('.add_new_rowxx-input-containers #entries_agilysys_solutions_partners_category_logo_widget').append(new_row);




}



function delete_row(cnt) {
    jQuery.each(jQuery(".add_new_rowxx-input-containers #entries_agilysys_solutions_partners_category_logo_widget"), function() {
        jQuery(' #entry' + cnt).remove();
    });
    var last_cnt = 0;

    jQuery.each(jQuery("#entries_agilysys_solutions_partners_category_logo_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            last_cnt = jQuery(this).val();
        }
    });

    last_cnt--;
    jQuery.each(jQuery("#entries_agilysys_solutions_partners_category_logo_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(last_cnt);
        }
    });


}
</script>

<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_solutions_partners_category_logo_widget  p{
    padding:10px !important;
}

#rew_container_agilysys_solutions_partners_category_logo_widget #entries_agilysys_solutions_partners_category_logo_widget  select{
     float: left;
    width: 60%;
    margin-top:20px !important;
    margin-bottom:10px !important;
}

#rew_container_agilysys_solutions_partners_category_logo_widget #entries_agilysys_solutions_partners_category_logo_widget input,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_solutions_partners_category_logo_widget #entries_agilysys_solutions_partners_category_logo_widget label {
    width: 40%;
     float: left;
}

<?php echo '.'. $widget_add_id_what_makes;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}

#entries_agilysys_solutions_partners_category_logo_widget #remove-img-bg {
    background: url('<?php echo ZWREW_URL; ?>assets/images/deleteimg.png') center center no-repeat;
    width: 20px;
    height: 22px;
    display: none;
}

#entries_agilysys_solutions_partners_category_logo_widget {
    padding: 10px 0 0;
}

#entries_agilysys_solutions_partners_category_logo_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_solutions_partners_category_logo_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_solutions_partners_category_logo_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_solutions_partners_category_logo_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_solutions_partners_category_logo_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_solutions_partners_category_logo_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_solutions_partners_category_logo_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_solutions_partners_category_logo_widget #entries_agilysys_solutions_partners_category_logo_widget p last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_solutions_partners_category_logo_widget">
    <?php echo $rew_html; ?>
</div>


<?php
}
}
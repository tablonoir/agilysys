<?php

add_action('widgets_init', 'load_agilysys_slider_page_widget');

function load_agilysys_slider_page_widget()
{
    register_widget('agilysys_slider_page_widget');
}

class agilysys_slider_page_widget extends WP_Widget
{
    /**
     * constructor -- name this the same as the class above
     */

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Slider Page Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');

    }

    public function widget($args, $instance)
    {
        
      

        echo $args['before_widget'];

        $count = count($instance['title']);

        $section_title = $instance['section_title'];

        ?>


<section class="industry-slider">


    <h2 class="greenText dinProStd" data-aos="fade-right" data-aos-delay="300" data-aos-duration="400"
        data-aos-once="true"><?php echo $section_title;?></h2>


    <div class="swiper-container">
        <div class="swiper-wrapper">

            <?php

        for ($i = 0; $i < $count; $i++) {

            $title = strip_tags($instance['title'][$i]);

            $image_uri = strip_tags($instance['image_uri'][$i]);

            $image_uri_alt = $instance['image_uri_alt'];

            $link_type = $instance['link_type'][$i];
            if ($link_type == "link") {
                $link = $instance['url_link'][$i];
            } elseif ($link_type == "page") {
                $post_id = $instance['page'][$i];
                $post = get_post($post_id);
                $link = home_url($post->post_name) . "/";
            }

            ?>

            <div class="swiper-slide">
                <div class="industry-slider-Img">
                    <div class="solution3columnSlide">
                        <img src="<?php echo $image_uri; ?>" class="img-fluid" alt="<?php echo $image_uri_alt; ?>">
                    </div>
                    <div class="industry-slider-Content">
                        <a href="<?php echo $link; ?>"><?php echo $title; ?></a>
                    </div>
                </div>
            </div>
            <?php

        }

        ?>


        </div>

    </div>

    <div class="industry-slider-arrows">
        <div class="swiper-button-next">
            <img src="<?php echo get_template_directory_uri(); ?>/img/arrow-right-white.png" class="img-fluid"
                alt="right-arrow">
        </div>
        <div class="swiper-button-prev">
            <img src="<?php echo get_template_directory_uri(); ?>/img/arrow-left-white.png" class="img-fluid"
                alt="left-arrow">
        </div>

    </div>




</section>

<style>
.productSlider .swiper-button-next {
    background-image: url(<?php echo get_template_directory_uri();
    ?>/img/arrow-right2.png);
    background-size: auto;
    background-repeat: no-repeat;

}

.productSlider .swiper-button-prev {
    background-image: url(<?php echo get_template_directory_uri();
    ?>/img/arrow-left1.png);
    background-size: auto;
    background-repeat: no-repeat;
}
</style>




<?php
echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['what_makes_rows'] = strip_tags($new_instance['what_makes_rows']);

        $count = count($new_instance['title']);

        $instance['section_title'] = $new_instance['section_title'];

        for ($i = 0; $i < $count; $i++) {

            $instance['title'][$i] = strip_tags($new_instance['title'][$i]);

            $instance['image_uri'][$i] = strip_tags($new_instance['image_uri'][$i]);
            $instance['image_uri_alt'][$i] = strip_tags($new_instance['image_uri_alt'][$i]);

            $instance['link_type'][$i] = $new_instance['link_type'][$i];
            if ($new_instance['link_type'][$i] == 'page') {
                $instance['page'][$i] = $new_instance['page'][$i];
                $instance['url_link'][$i] = '';
            } elseif ($new_instance['link_type'][$i] == 'link') {
                $instance['url_link'][$i] = $new_instance['url_link'][$i];
                $instance['page'][$i] = '';

            }

        }
        return $instance;
    }

    public function form($display_instance)
    {
        
        
        
        
        $widget_add_id_webinars_info = $this->get_field_id('') . "add_agilysys_customer_stories_spotlight_widget";

        $section_title = $display_instance['section_title'];

        if (!empty($display_instance['what_makes_rows'])) {
            $what_makes_rows = ($display_instance['what_makes_rows']);
        } else {
            $what_makes_rows = 0;
        }


        $rew_html .= '<p><label>Title Required</label>';
        
          $rew_html .= '<span style="float:left;">Yes</span>&nbsp;<input style="width:1% !important;float:left;" type="radio" id="' . $this->get_field_id('button_required') . '" name="' . $this->get_field_name('button_required') . '" value="yes" ' . $checked1 . ' onClick="show_hide_title_agilysys_slider_page_widget(this.value);">';

        $rew_html .= '<span style="float:left;margin-left:10px;">No</span>&nbsp;<input style="width:1% !important;float:left;" type="radio" id="' . $this->get_field_id('button_required') . '" name="' . $this->get_field_name('button_required') . '" value="no" ' . $checked2 . ' onClick="show_hide_title_agilysys_slider_page_widget(this.value);"></p>';
      


        $rew_html .= '<div style="display:none;" id="link_type_id_p_agilysys_slider_page_widget">';


        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('section_title') . '"> ' . __('Section Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('section_title') . '" name="' . $this->get_field_name('section_title') . '" type="text" value="' . $display_instance['section_title'] . '" />';
        $rew_html .= '</p><br><br>';


        $rew_html .= '</div>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('what_makes_rows') . '"> ' . __('No. of rows do you want to add*', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input class="what_makes_rows" id="' . $this->get_field_name('what_makes_rows') . '" name="' . $this->get_field_name('what_makes_rows') . '" type="number" value="' . $what_makes_rows . '" />';
        $rew_html .= '</p><br><br>';

        $count = count($display_instance['title']);

        $rew_html .= '<div class="add_new_rowxx-input-containers"><div id="entries_agilysys_slider_page_widget">';

        $rew_html .= '<input class="cnt909" id="cnt909" name="cnt" type="hidden" value="' . $count . '">';

        for ($i = 0; $i < $count; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'AGILYSYS_TEXT_DOMAIN') . ' </span>';

            $rew_html .= '<div class="entry-desc cf">';

            $rew_html .= '<p class="last">';
            $rew_html .= '<label for="' . $this->get_field_id('title' . $i) . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('title' . $i) . '" name="' . $this->get_field_name('title[]') . '" type="text" value="' . $display_instance['title'][$i] . '" />';
            $rew_html .= '</p>';


            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('link_type' . $i) . '"> ' . __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('link_type' . $i) . '" name="' . $this->get_field_name('link_type[]') . '" onChange="show_hide_div_agilysys_slider_page_widget(this.value,' . $i . ');">';
            $rew_html .= '<option value="">Please Select</option>';

            $link_type = $display_instance['link_type'][$i];

            if ($link_type == 'page') {
                $rew_html .= '<option value="page" selected="selected">Internal Page Link</option>';
            } else {
                $rew_html .= '<option value="page">Internal Page Link</option>';
            }

            if ($link_type == 'link') {
                $rew_html .= '<option value="link" selected="selected">External Link</option>';
            } else {
                $rew_html .= '<option value="link">External Link</option>';
            }

            $rew_html .= '</select>';
            $rew_html .= '</p><br><br>';

            $args = array(
                'sort_order' => 'desc',
                'sort_column' => 'post_title',
                'hierarchical' => 1,
                'exclude' => '',
                'include' => '',
                'meta_key' => '',
                'meta_value' => '',
                'authors' => '',
                'child_of' => 0,
                'parent' => -1,
                'exclude_tree' => '',
                'number' => '',
                'offset' => 0,
                'post_type' => 'page',
                'post_status' => 'publish',
            );
            $pages = get_pages($args); // get all pages based on supplied args

            if ($link_type == 'page') {
                $show1 = 'style="display:block"';
                $show2 = 'style="display:none"';
            } elseif ($link_type == 'link') {
                $show1 = 'style="display:none"';
                $show2 = 'style="display:block"';

            } else {
                $show1 = 'style="display:none"';
                $show2 = 'style="display:none"';
            }
            $rew_html .= '<div id="page_div_agilysys_slider_page_widget' . $i . '" ' . $show1 . '><p>';
            $rew_html .= '<label for="' . $this->get_field_id('page' . $i) . '"> ' . __('Page', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('page' . $i) . '" name="' . $this->get_field_name('page[]') . '">';
            $rew_html .= '<option value="">Please Select</option>';

            $page = $display_instance['page'][$i];

            foreach ($pages as $key) {

                if ($page == $key->ID) {
                    $rew_html .= '<option value="' . $key->ID . '" selected="selected">' . $key->post_title . '</option>';
                } else {
                    $rew_html .= '<option value="' . $key->ID . '">' . $key->post_title . '</option>';
                }

            }

            $rew_html .= '</select>';
            $rew_html .= '</p></div><br><br>';

            $rew_html .= '<div id="link_div_agilysys_slider_page_widget' . $i . '" ' . $show2 . '><p>';
            $rew_html .= '<label for="' . $this->get_field_id('url_link' . $i) . '"> ' . __('Link', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('url_link' . $i) . '" name="' . $this->get_field_name('url_link[]') . '" type="text" value="' . $display_instance['url_link'][$i] . '" />';
            $rew_html .= '</p></div><br><br>';

            ?>
            <script>
            function show_hide_div_agilysys_slider_page_widget(val, i) {
            console.log(val);
                if (val == 'page') {
                    jQuery("#page_div_agilysys_slider_page_widget" + i).show();
                    jQuery("#link_div_agilysys_slider_page_widget" + i).hide();
                } else if (val == 'link') {
                    jQuery("#page_div_agilysys_slider_page_widget" + i).hide();
                    jQuery("#link_div_agilysys_slider_page_widget" + i).show();
                }
            
            }
            </script>
            
            <?php
            

            $rew_html .= '<div class="widg-img' . $i . '">';
            $show1 = (empty($display_instance['image_uri'][$i])) ? 'style="display:none;"' : '';
            $rew_html .= '<label id="image_uri_agilysys_slider_page_widget' . $i . '"></label><br><img class="' . $this->get_field_id('image_uri' . $i) . '_media_imageyy' . $i . ' custom_media_imageyy' . $i . '" src="' . $display_instance['image_uri'][$i] . '" ' . $show1 . ' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_uri' . $i) . '_media_idyy' . $i . ' custom_media_idyy' . $i . '" name="' . $this->get_field_name('image_id[]') . '" id="' . $this->get_field_id('image_uri' . $i) . '" value="' . $display_instance['image_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_uri' . $i) . '_media_urlyy' . $i . ' custom_media_urlyy' . $i . '" name="' . $this->get_field_name('image_uri[]') . '" id="' . $this->get_field_id('image_uri' . $i) . '" value="' . $display_instance['image_uri'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload Logo" class="button custom_media_uploadyy' . $i . '" id="' . $this->get_field_id('image_uri' . $i) . '"/>';

            $rew_html .= '</div><br><br>';

            ?>

<script>
jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_uploadyy<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    //if (attachment.height == 1080 && attachment.width == 1920) {
                        jQuery('.' + button_id_s + '_media_idyy<?php echo $i; ?>').val(attachment
                            .id);
                        jQuery('.' + button_id_s + '_media_urlyy<?php echo $i; ?>').val(attachment
                            .url);
                        jQuery('.' + button_id_s + '_media_imageyy<?php echo $i; ?>').attr('src',
                            attachment.url).css('display', 'block');

                        jQuery('#image_uri_agilysys_slider_page_widget<?php echo $i; ?>')
                            .html("");
                    /*} else {
                        jQuery('#image_uri_agilysys_slider_page_widget<?php echo $i; ?>')
                            .html("Please Enter the correct Dimensions 1920x1080").css('color',
                                'red');

                    }
                    */
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_uploadyy<?php echo $i; ?>');

});
</script>

<?php

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt' . $i) . '"> ' . __('Image Alt', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt' . $i) . '" name="' . $this->get_field_name('image_uri_alt[]') . '" type="text" value="' . $display_instance['image_uri_alt'][$i] . '">';
            $rew_html .= '</p><br><br>';

            $k = $i + 1;
            $rew_html .= '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' . $k . ');">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';

        }

        $rew_html .= '</div></div>';

        $rew_html .= '<div class="' . $widget_add_id_webinars_info . '" style="margin-bottom: 36px;text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;" onClick="add_new_row();">' . __('ADD ROW', 'ZWREW_TEXT_DOMAIN') . '</div>';
        ?>


<script>


function show_hide_title_agilysys_slider_page_widget(val){
    console.log(val);
    
    if(val=='yes')
    {
         jQuery("#link_type_id_p_agilysys_slider_page_widget").show();
        
    }
    else if(val=='no')
    {
        jQuery("#link_type_id_p_agilysys_slider_page_widget").hide();
        
    }
}

function add_new_row() {
    var cnt = '';

    jQuery.each(jQuery("#entries_agilysys_slider_page_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            cnt = jQuery(this).val();
        }
    });




    var what_makes_rows = jQuery('.what_makes_rows').val();

console.log(cnt);

    if (parseInt(cnt) < parseInt(what_makes_rows)) {

        cnt++;

        jQuery.each(jQuery("#entries_agilysys_slider_page_widget .cnt909"), function() {
            if (jQuery(this).val() != '') {
                jQuery(this).val(cnt);
            }
        });

        var new_row = '<div id="entry' + cnt +
            '"  class="entrys"><span class="entry-title" onclick = "slider(this);"><?php echo __('Add New Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span>';
        new_row += '<div class="entry-desc cf">';


        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Title', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('title[]')); ?>" type="text" value="">';
        new_row += '</p>';


        new_row += '<div class="widg-img' + cnt + '">';

        new_row += '<label id="image_uri_agilysys_slider_page_widget' + cnt +
            '"></label><br><img class="<?php echo $this->get_field_id('image_uri'); ?>' + cnt + '_media_imageyy' + cnt +
            ' custom_media_imageyy' + cnt + '" src="" style="display:none;" width=200" height="120"/>';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_uri'); ?>' + cnt + '_media_idyy' +
            cnt + 'custom_media_idyy' + cnt + '" name="<?php echo $this->get_field_name('image_id[]'); ?>" />';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_uri'); ?>' + cnt +
            '_media_urlyy' +
            cnt + ' custom_media_urlyy' + cnt + '" name="<?php echo $this->get_field_name('image_uri[]'); ?>">';
        new_row += '<input type="button" value="Upload Logo" id="<?php echo $this->get_field_id('image_uri'); ?>' +
            cnt + '" class="button custom_media_uploadyy' + cnt + '"/>';

        new_row += '</div><br><br>';


        jQuery(document).ready(function() {




            function media_uploadabc(button_class) {
                var _custom_media = true,
                    _orig_send_attachment = wp.media.editor.send.attachment;
                jQuery('body').on('click', '.custom_media_uploadyy' + cnt, function(e) {
                    var button_id = '#' + jQuery(this).attr('id');
                    var button_id_s = jQuery(this).attr('id');
                    console.log(button_id);
                    var self = jQuery(button_id);
                    var send_attachment_bkp = wp.media.editor.send.attachment;
                    var button = jQuery(button_id);
                    var id = button.attr('id').replace('_button', '');
                    _custom_media = true;

                    wp.media.editor.send.attachment = function(props, attachment) {
                        if (_custom_media) {

                            //if (attachment.height == 1080 && attachment.width == 1920) {
                                jQuery('.' + button_id_s + '_media_idyy' + cnt).val(attachment.id);
                                jQuery('.' + button_id_s + '_media_urlyy' + cnt).val(attachment
                                .url);
                                jQuery('.' + button_id_s + '_media_imageyy' + cnt).attr('src',
                                    attachment.url).css('display', 'block');
                                jQuery('#image_uri_agilysys_slider_page_widget' + cnt)
                                    .html("");
                            /*} else {
                                jQuery('#image_uri_agilysys_slider_page_widget' + cnt)
                                    .html("Please Enter the correct Dimensions 1920x1080").css(
                                        'color', 'red');

                            }
                            */
                        } else {
                            return _orig_send_attachment.apply(button_id, [props, attachment]);
                        }
                    }
                    wp.media.editor.open(button);
                    return false;
                });
            }
            media_uploadabc('.custom_media_uploadyy' + cnt);

        });

        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Image Alt', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';

        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('image_uri_alt[]')); ?>" type="text" value="">';
        new_row += '</p>';


        new_row += '<p>';
        new_row += '<label><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
        new_row +=
            '<select name="<?php echo $this->get_field_name('link_type[]'); ?>" onChange="show_hide_div_agilysys_slider_page_widget(this.value,' +
            cnt + ');">';
        new_row += '<option value="">Please Select</option>';

        new_row += '<option value="page">Internal Page Link</option>';

        new_row += '<option value="link">External Link</option>';

        new_row += '</select>';
        new_row += '</p><br><br>';


        <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        ?>


        new_row += '<div id="page_div_agilysys_slider_page_widget' + cnt + '" style="display:none;"><p>';
        new_row += '<label><?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
        new_row += '<select  name="<?php echo $this->get_field_name('page[]'); ?>">';
        new_row += '<option value="">Please Select</option>';


        <?php
foreach ($pages as $key) {
            ?>



        new_row += '<option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>';


        <?php
}
        ?>

        new_row += '</select>';
        new_row += '</p></div><br><br>';

        new_row += '<div id="link_div_agilysys_slider_page_widget' + cnt + '" style="display:none;"><p>';
        new_row += '<label> <?php echo __('Link', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
        new_row += '<input name="<?php echo $this->get_field_name('url_link[]'); ?>" type="text"  />';
        new_row += '</p></div><br><br>';

        var new_cnt = cnt;

        new_row += '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' + new_cnt +
            ');"><?php echo __('Delete Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span></a></p>';
        new_row += '</div></div>';

        jQuery('.add_new_rowxx-input-containers #entries_agilysys_slider_page_widget').append(new_row);

    }


}



function delete_row(cnt) {
    jQuery.each(jQuery(".add_new_rowxx-input-containers #entries_agilysys_slider_page_widget"), function() {
        jQuery(' #entry' + cnt).remove();
    });
    var last_cnt = 0;

    jQuery.each(jQuery("#entries_agilysys_slider_page_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            last_cnt = jQuery(this).val();
        }
    });

    last_cnt--;
    jQuery.each(jQuery("#entries_agilysys_slider_page_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(last_cnt);
        }
    });
    jQuery(".what_makes_rows").val(last_cnt);
    jQuery('.what_makes_rows').trigger('change');

}
</script>
<style>

#rew_container_agilysys_slider_page_widget p {
   padding:10px !important;
}

.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_slider_page_widget  select{
     float: left;
    width: 60%;
    margin-top:20px !important;
    margin-bottom:10px !important;
}

#rew_container_agilysys_slider_page_widget input,

textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_slider_page_widget label {
    width: 40%;
     float: left;
}

<?php echo '.'. $widget_add_id_webinars_info;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}

#entries_agilysys_slider_page_widget #remove-img-bg {
    background: url('<?php echo ZWREW_URL; ?>assets/images/deleteimg.png') center center no-repeat;
    width: 20px;
    height: 22px;
    display: none;
}

#entries_agilysys_slider_page_widget {
    padding: 10px 0 0;
}

#entries_agilysys_slider_page_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_slider_page_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_slider_page_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_slider_page_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_slider_page_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_slider_page_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_slider_page_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_slider_page_widget #entries_agilysys_slider_page_widget plast label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_slider_page_widget">
    <?php echo $rew_html; ?>
</div>

<?php

    }

}
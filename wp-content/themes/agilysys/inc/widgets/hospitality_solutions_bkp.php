<?php 

function agilysys_leader_hopitality_widget()
{
    register_widget('agilysys_hospitality_solution_widget');
}
add_action('widgets_init', 'agilysys_leader_hopitality_widget');

class agilysys_hospitality_solution_widget extends WP_Widget
{

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Hospitality Solutions Widget', 'ZWREW_TEXT_DOMAIN'));
    }

    public function widget($args, $instance)
    { 

        
        
     /*    echo trim($args['before_widget']); */

        //front end`
        $hospitality_title = !empty($instance['hospitality_title']) ? $instance['hospitality_title'] : '';

        $guest_title = !empty($instance['guest_title']) ? $instance['guest_title'] : '';

        $property = !empty($instance['property_title']) ? $instance['property_title'] : '';

        $booking_icon_src = !empty($instance['booking_icon_src']) ? $instance['booking_icon_src'] : '';
        $booking_icon_src_alt = !empty($instance['booking_icon_src_alt']) ? $instance['booking_icon_src_alt'] : '';

        $booking_hover_icon_src = !empty($instance['booking_hover_icon_src']) ? $instance['booking_hover_icon_src'] : '';
        $booking_hover_icon_src_alt = !empty($instance['booking_hover_icon_src_alt']) ? $instance['booking_hover_icon_src_alt'] : '';

        $link_type_booking_url = $instance['link_type_booking_url'];
        if ($link_type_booking_url == "link") {
            $booking_url = $instance['booking_url'];
        } elseif ($link_type_booking_url == "page") {
            $post_id = $instance['page_booking_url'];
            $post = get_post($post_id);
            $booking_url = home_url($post->post_name) . "/";
        }
        //$booking_url = !empty($instance['booking_url']) ? $instance['booking_url'] : '';

        $booking_title = !empty($instance['booking_url_title']) ? $instance['booking_url_title'] : '';

        $mobile_icon_src = !empty($instance['mobile_icon_src']) ? $instance['mobile_icon_src'] : '';
        $mobile_icon_src_alt = !empty($instance['mobile_icon_src_alt']) ? $instance['mobile_icon_src_alt'] : '';


        $mobile_hover_icon_src = !empty($instance['mobile_hover_icon_src']) ? $instance['mobile_hover_icon_src'] : '';
        $mobile_hover_icon_src_alt = !empty($instance['mobile_hover_icon_src_alt']) ? $instance['mobile_hover_icon_src_alt'] : '';


        $link_type_mobile_url = $instance['link_type_mobile_url'];
        if ($link_type_mobile_url == "link") {
            $mobile_url = $instance['mobile_url'];
        } elseif ($link_type_mobile_url == "page") {
            $post_id = $instance['page_mobile_url'];
            $post = get_post($post_id);
            $mobile_url = home_url($post->post_name) . "/";
        }

        //$mobile_url = !empty($instance['mobile_url']) ? $instance['mobile_url'] : '';
        $mobile_title = !empty($instance['mobile_url_title']) ? $instance['mobile_url_title'] : '';

        $golf_icon_src = !empty($instance['golf_icon_src']) ? $instance['golf_icon_src'] : '';
        $golf_icon_src_alt = !empty($instance['golf_icon_src_alt']) ? $instance['golf_icon_src_alt'] : '';
        $golf_hover_icon_src = !empty($instance['golf_hover_icon_src']) ? $instance['golf_hover_icon_src'] : '';
        $golf_hover_icon_src_alt = !empty($instance['golf_hover_icon_src_alt']) ? $instance['golf_hover_icon_src_alt'] : '';


        $link_type_golf_url = $instance['link_type_golf_url'];
        if ($link_type_golf_url == "link") {
            $golf_url = $instance['golf_url'];
        } elseif ($link_type_golf_url == "page") {
            $post_id = $instance['page_golf_url'];
            $post = get_post($post_id);
            $golf_url = home_url($post->post_name) . "/";
        }

        //$golf_url = !empty($instance['golf_url']) ? $instance['golf_url'] : '';
        $golf_title = !empty($instance['golf_url_title']) ? $instance['golf_url_title'] : '';

        $digital_icon_src = !empty($instance['digital_icon_src']) ? $instance['digital_icon_src'] : '';
        $digital_icon_src_alt = !empty($instance['digital_icon_src_alt']) ? $instance['digital_icon_src_alt'] : '';
        $digital_hover_icon_src = !empty($instance['digital_hover_icon_src']) ? $instance['digital_hover_icon_src'] : '';
        $digital_hover_icon_src_alt = !empty($instance['digital_hover_icon_src_alt']) ? $instance['digital_hover_icon_src_alt'] : '';

        $link_type_digital_url = $instance['link_type_digital_url'];
        if ($link_type_digital_url == "link") {
            $digital_url = $instance['digital_url'];
        } elseif ($link_type_digital_url == "page") {
            $post_id = $instance['page_digital_url'];
            $post = get_post($post_id);
            $digital_url = home_url($post->post_name) . "/";
        }
        //$digital_url = !empty($instance['digital_url']) ? $instance['digital_url'] : '';
        $digital_url_title = !empty($instance['digital_url_title']) ? $instance['digital_url_title'] : '';

        $dining_icon_src = !empty($instance['dining_icon_src']) ? $instance['dining_icon_src'] : '';
        $dining_icon_src_alt = !empty($instance['dining_icon_src_alt']) ? $instance['dining_icon_src_alt'] : '';

        $dining_hover_icon_src = !empty($instance['dining_hover_icon_src']) ? $instance['dining_hover_icon_src'] : '';
        $dining_hover_icon_src_alt = !empty($instance['dining_hover_icon_src_alt']) ? $instance['dining_hover_icon_src_alt'] : '';

        $link_type_dining_url = $instance['link_type_dining_url'];
        if ($link_type_dining_url == "link") {
            $dining_url = $instance['dining_url'];
        } elseif ($link_type_dining_url == "page") {
            $post_id = $instance['page_dining_url'];
            $post = get_post($post_id);
            $dining_url = home_url($post->post_name) . "/";
        }

        //$dining_url = !empty($instance['dining_url']) ? $instance['dining_url'] : '';
        $dining_url_title = !empty($instance['dining_url_title']) ? $instance['dining_url_title'] : '';

        $payment_icon_src = !empty($instance['payment_icon_src']) ? $instance['payment_icon_src'] : '';
        $payment_icon_src_alt = !empty($instance['payment_icon_src_alt']) ? $instance['payment_icon_src_alt'] : '';

        $payment_hover_icon_src = !empty($instance['payment_hover_icon_src']) ? $instance['payment_hover_icon_src'] : '';
        $payment_hover_icon_src_alt = !empty($instance['payment_hover_icon_src_alt']) ? $instance['payment_hover_icon_src_alt'] : '';

        $link_type_payment_url = $instance['link_type_payment_url'];
        if ($link_type_payment_url == "link") {
            $payment_url = $instance['payment_url'];
        } elseif ($link_type_payment_url == "page") {
            $post_id = $instance['page_payment_url'];
            $post = get_post($post_id);
            $payment_url = home_url($post->post_name) . "/";
        }

        //$payment_url = !empty($instance['payment_url']) ? $instance['payment_url'] : '';
        $payment_url_title = !empty($instance['payment_url_title']) ? $instance['payment_url_title'] : '';

        //         spa
        $spa_icon_src = !empty($instance['spa_icon_src']) ? $instance['spa_icon_src'] : '';
        $spa_icon_src_alt = !empty($instance['spa_icon_src_alt']) ? $instance['spa_icon_src_alt'] : '';
        $spa_hover_icon_src = !empty($instance['spa_hover_icon_src']) ? $instance['spa_hover_icon_src'] : '';
        $spa_hover_icon_src_alt = !empty($instance['spa_hover_icon_src_alt']) ? $instance['spa_hover_icon_src_alt'] : '';


        $link_type_spa_url = $instance['link_type_spa_url'];
        if ($link_type_spa_url == "link") {
            $spa_url = $instance['spa_url'];
        } elseif ($link_type_spa_url == "page") {
            $post_id = $instance['page_spa_url'];
            $post = get_post($post_id);
            $spa_url = home_url($post->post_name) . "/";
        }
        //$spa_url = !empty($instance['spa_url']) ? $instance['spa_url'] : '';
        $spa_url_title = !empty($instance['spa_url_title']) ? $instance['spa_url_title'] : '';

        //        venue
        $venue_icon_src = !empty($instance['venue_icon_src']) ? $instance['venue_icon_src'] : '';
        $venue_icon_src_alt = !empty($instance['venue_icon_src_alt']) ? $instance['venue_icon_src_alt'] : '';

        $venue_hover_icon_src = !empty($instance['venue_hover_icon_src']) ? $instance['venue_hover_icon_src'] : '';
        $venue_hover_icon_src_alt = !empty($instance['venue_hover_icon_src_alt']) ? $instance['venue_hover_icon_src_alt'] : '';

        $link_type_venue_url = $instance['link_type_venue_url'];
        if ($link_type_venue_url == "link") {
            $venue_url = $instance['venue_url'];
        } elseif ($link_type_venue_url == "page") {
            $post_id = $instance['page_venue_url'];
            $post = get_post($post_id);
            $venue_url = home_url($post->post_name) . "/";
        }
        //$venue_url = !empty($instance['venue_url']) ? $instance['venue_url'] : '';
        $venue_url_title = !empty($instance['venue_url_title']) ? $instance['venue_url_title'] : '';

        //        mobile checkout
        $mobile_check_icon_src = !empty($instance['mobile_check_icon_src']) ? $instance['mobile_check_icon_src'] : '';
        $mobile_check_icon_src_alt = !empty($instance['mobile_check_icon_src_alt']) ? $instance['mobile_check_icon_src_alt'] : '';
        $mobile_check_hover_icon_src = !empty($instance['mobile_check_hover_icon_src']) ? $instance['mobile_check_hover_icon_src'] : '';
        $mobile_check_hover_icon_src_alt = !empty($instance['mobile_check_hover_icon_src_alt']) ? $instance['mobile_check_hover_icon_src_alt'] : '';

        $link_type_mobile_check_url = $instance['link_type_mobile_check_url'];
        if ($link_type_mobile_check_url == "link") {
            $mobile_check_url = $instance['mobile_check_url'];
        } elseif ($link_type_mobile_check_url == "page") {
            $post_id = $instance['page_mobile_check_url'];
            $post = get_post($post_id);
            $mobile_check_url = home_url($post->post_name) . "/";
        }

        //$mobile_check_url = !empty($instance['mobile_check_url']) ? $instance['mobile_check_url'] : '';
        $mobile_check_url_title = !empty($instance['mobile_check_url_title']) ? $instance['mobile_check_url_title'] : '';

//              property 1st row
        $digital_marketing_src = !empty($instance['digital_marketing_src']) ? $instance['digital_marketing_src'] : '';
        $digital_marketing_src_alt = !empty($instance['digital_marketing_src_alt']) ? $instance['digital_marketing_src_alt'] : '';

        $digital_marketing_hover_src = !empty($instance['digital_marketing_hover_src']) ? $instance['digital_marketing_hover_src'] : '';
        $digital_marketing_hover_src_alt = !empty($instance['digital_marketing_hover_src_alt']) ? $instance['digital_marketing_hover_src_alt'] : '';

        $link_type_digital_marketing_url = $instance['link_type_digital_marketing_url'];
        if ($link_type_digital_marketing_url == "link") {
            $digital_marketing_url = $instance['digital_marketing_url'];
        } elseif ($link_type_digital_marketing_url == "page") {
            $post_id = $instance['page_digital_marketing_url'];
            $post = get_post($post_id);
            $digital_marketing_url = home_url($post->post_name) . "/";
        }

        //$digital_marketing_url = !empty($instance['digital_marketing_url']) ? $instance['digital_marketing_url'] : '';
        $digital_marketing_url_title = !empty($instance['digital_marketing_url_title']) ? $instance['digital_marketing_url_title'] : '';

        $property_booking_icon_src = !empty($instance['property_booking_icon_src']) ? $instance['property_booking_icon_src'] : '';
        $property_booking_icon_src_alt = !empty($instance['property_booking_icon_src_alt']) ? $instance['property_booking_icon_src_alt'] : '';
        $property_booking_hover_icon_src = !empty($instance['property_booking_hover_icon_src']) ? $instance['property_booking_hover_icon_src'] : '';
        $property_booking_hover_icon_src_alt = !empty($instance['property_booking_hover_icon_src_alt']) ? $instance['property_booking_hover_icon_src_alt'] : '';

        $link_type_property_booking_url = $instance['link_type_property_booking_url'];
        if ($link_type_property_booking_url == "link") {
            $property_booking_url = $instance['property_booking_url'];
        } elseif ($link_type_property_booking_url == "page") {
            $post_id = $instance['page_property_booking_url'];
            $post = get_post($post_id);
            $property_booking_url = home_url($post->post_name) . "/";
        }

        //$property_booking_url = !empty($instance['property_booking_url']) ? $instance['property_booking_url'] : '';
        $property_booking_url_title = !empty($instance['property_booking_url_title']) ? $instance['property_booking_url_title'] : '';

        $property_mgmt_icon_src = !empty($instance['property_mgmt_icon_src']) ? $instance['property_mgmt_icon_src'] : '';
        $property_mgmt_icon_src_alt = !empty($instance['property_mgmt_icon_src_alt']) ? $instance['property_mgmt_icon_src_alt'] : '';
        $property_mgmt_hover_icon_src = !empty($instance['property_mgmt_hover_icon_src']) ? $instance['property_mgmt_hover_icon_src'] : '';
        $property_mgmt_hover_icon_src_alt = !empty($instance['property_mgmt_hover_icon_src_alt']) ? $instance['property_mgmt_hover_icon_src_alt'] : '';

        $link_type_property_mgmt_url = $instance['link_type_property_mgmt_url'];
        if ($link_type_property_mgmt_url == "link") {
            $property_mgmt_url = $instance['property_mgmt_url'];
        } elseif ($link_type_property_mgmt_url == "page") {
            $post_id = $instance['page_property_mgmt_url'];
            $post = get_post($post_id);
            $property_mgmt_url = home_url($post->post_name) . "/";
        }

        //$property_mgmt_url = !empty($instance['property_mgmt_url']) ? $instance['property_mgmt_url'] : '';
        $property_mgmt_url_title = !empty($instance['property_mgmt_url_title']) ? $instance['property_mgmt_url_title'] : '';

        //              property 2nd row

        $link_type_pos_url = $instance['link_type_pos_url'];
        if ($link_type_pos_url == "link") {
            $pos_url = $instance['pos_url'];
        } elseif ($link_type_pos_url == "page") {
            $post_id = $instance['page_pos_url'];
            $post = get_post($post_id);
            $pos_url = home_url($post->post_name) . "/";
        }

        //$pos_url = !empty($instance['pos_url']) ? $instance['pos_url'] : '';
        $pos_url_title = !empty($instance['pos_url_title']) ? $instance['pos_url_title'] : '';

        $pos_icon_src = !empty($instance['pos_icon_src']) ? $instance['pos_icon_src'] : '';
        $pos_icon_src_alt = !empty($instance['pos_icon_src_alt']) ? $instance['pos_icon_src_alt'] : '';

        $pos_hover_icon_src = !empty($instance['pos_hover_icon_src']) ? $instance['pos_hover_icon_src'] : '';
        $pos_hover_icon_src_alt = !empty($instance['pos_hover_icon_src_alt']) ? $instance['pos_hover_icon_src_alt'] : '';

        $link_type_inventory_url = $instance['link_type_inventory_url'];
        if ($link_type_inventory_url == "link") {
            $inventory_url = $instance['inventory_url'];
        } elseif ($link_type_inventory_url == "page") {
            $post_id = $instance['page_inventory_url'];
            $post = get_post($post_id);
            $inventory_url = home_url($post->post_name) . "/";
        }

        //$inventory_url = !empty($instance['inventory_url']) ? $instance['inventory_url'] : '';
        $inventory_url_title = !empty($instance['inventory_url_title']) ? $instance['inventory_url_title'] : '';


        $inventory_icon_src = !empty($instance['inventory_icon_src']) ? $instance['inventory_icon_src'] : '';
        $inventory_icon_src_alt = !empty($instance['inventory_icon_src_alt']) ? $instance['inventory_icon_src_alt'] : '';
        $inventory_hover_icon_src = !empty($instance['inventory_hover_icon_src']) ? $instance['inventory_hover_icon_src'] : '';
        $inventory_hover_icon_src_alt = !empty($instance['inventory_hover_icon_src_alt']) ? $instance['inventory_hover_icon_src_alt'] : '';

        $link_type_document_url = $instance['link_type_document_url'];
        if ($link_type_document_url == "link") {
            $document_url = $instance['document_url'];
        } elseif ($link_type_document_url == "page") {
            $post_id = $instance['page_document_url'];
            $post = get_post($post_id);
            $document_url = home_url($post->post_name) . "/";
        }

        //$document_url = !empty($instance['document_url']) ? $instance['document_url'] : '';
        $document_url_title = !empty($instance['document_url_title']) ? $instance['document_url_title'] : '';


        $document_icon_src = !empty($instance['document_icon_src']) ? $instance['document_icon_src'] : '';
        $document_icon_src_alt = !empty($instance['document_icon_src_alt']) ? $instance['document_icon_src_alt'] : '';
        $document_hover_icon_src = !empty($instance['document_hover_icon_src']) ? $instance['document_hover_icon_src'] : '';
        $document_hover_icon_src_alt = !empty($instance['document_hover_icon_src_alt']) ? $instance['document_hover_icon_src_alt'] : '';

        $prporty_spa_icon_src = !empty($instance['prporty_spa_icon_src']) ? $instance['prporty_spa_icon_src'] : '';
        $prporty_spa_icon_src_alt = !empty($instance['prporty_spa_icon_src_alt']) ? $instance['prporty_spa_icon_src_alt'] : '';
        $prporty_spa_hover_icon_src = !empty($instance['prporty_spa_hover_icon_src']) ? $instance['prporty_spa_hover_icon_src'] : '';
        $prporty_spa_hover_icon_src_alt = !empty($instance['prporty_spa_hover_icon_src_alt']) ? $instance['prporty_spa_hover_icon_src_alt'] : '';

        $link_type_prporty_spa_url = $instance['link_type_prporty_spa_url'];
        if ($link_type_prporty_spa_url == "link") {
            $prporty_spa_url = $instance['prporty_spa_url'];
        } elseif ($link_type_prporty_spa_url == "page") {
            $post_id = $instance['page_prporty_spa_url'];
            $post = get_post($post_id);
            $prporty_spa_url = home_url($post->post_name) . "/";
        }

        //$prporty_spa_url = !empty($instance['prporty_spa_url']) ? $instance['prporty_spa_url'] : '';
        $prporty_spa_url_title = !empty($instance['prporty_spa_url_title']) ? $instance['prporty_spa_url_title'] : '';

        //        proeprty 3rd row

        $prporty_digital_key_url_icon_src = !empty($instance['prporty_digital_key_url_icon_src']) ? $instance['prporty_digital_key_url_icon_src'] : '';
        $prporty_digital_key_url_icon_src_alt = !empty($instance['prporty_digital_key_url_icon_src_alt']) ? $instance['prporty_digital_key_url_icon_src_alt'] : '';
        $prporty_digital_key_url_icon_hover_icon_src = !empty($instance['prporty_digital_key_url_icon_hover_icon_src']) ? $instance['prporty_digital_key_url_icon_hover_icon_src'] : '';

        $prporty_digital_key_url_icon_hover_icon_src_alt = !empty($instance['prporty_digital_key_url_icon_hover_icon_src_alt']) ? $instance['prporty_digital_key_url_icon_hover_icon_src_alt'] : '';


        $link_type_prporty_digital_key_url = $instance['link_type_prporty_digital_key_url'];
        if ($link_type_prporty_digital_key_url == "link") {
            $prporty_digital_key_url = $instance['prporty_digital_key_url'];
        } elseif ($link_type_prporty_digital_key_url == "page") {
            $post_id = $instance['page_prporty_digital_key_url'];
            $post = get_post($post_id);
            $prporty_digital_key_url = home_url($post->post_name) . "/";
        }

        //$prporty_digital_key_url = !empty($instance['prporty_digital_key_url']) ? $instance['prporty_digital_key_url'] : '';
        $prporty_digital_key_url_title = !empty($instance['prporty_digital_key_url_title']) ? $instance['prporty_digital_key_url_title'] : '';

        $prporty_mobile_checkin_icon_src = !empty($instance['prporty_mobile_checkin_icon_src']) ? $instance['prporty_mobile_checkin_icon_src'] : '';
        $prporty_mobile_checkin_icon_src_alt = !empty($instance['prporty_mobile_checkin_icon_src_alt']) ? $instance['prporty_mobile_checkin_icon_src_alt'] : '';

        $prporty_mobile_checkin_hover_src = !empty($instance['prporty_mobile_checkin_hover_src']) ? $instance['prporty_mobile_checkin_hover_src'] : '';
        $prporty_mobile_checkin_hover_src_alt = !empty($instance['prporty_mobile_checkin_hover_src_alt']) ? $instance['prporty_mobile_checkin_hover_src_alt'] : '';


        $link_type_prporty_mobile_checkin_key_url = $instance['link_type_prporty_mobile_checkin_key_url'];
        if ($link_type_prporty_mobile_checkin_key_url == "link") {
            $prporty_mobile_checkin_key_url = $instance['prporty_mobile_checkin_key_url'];
        } elseif ($link_type_prporty_mobile_checkin_key_url == "page") {
            $post_id = $instance['page_prporty_mobile_checkin_key_url'];
            $post = get_post($post_id);
            $prporty_mobile_checkin_key_url = home_url($post->post_name) . "/";
        }

        //$prporty_mobile_checkin_key_url = !empty($instance['prporty_mobile_checkin_key_url']) ? $instance['prporty_mobile_checkin_key_url'] : '';
        $prporty_mobile_checkin_key_url_title = !empty($instance['prporty_mobile_checkin_key_url_title']) ? $instance['prporty_mobile_checkin_key_url_title'] : '';

        $prporty_analytics_icon_src = !empty($instance['prporty_analytics_icon_src']) ? $instance['prporty_analytics_icon_src'] : '';
        $prporty_analytics_icon_src_alt = !empty($instance['prporty_analytics_icon_src_alt']) ? $instance['prporty_analytics_icon_src_alt'] : '';

        $prporty_analytics_icon_hover_src = !empty($instance['prporty_analytics_icon_hover_src']) ? $instance['prporty_analytics_icon_hover_src'] : '';
        $prporty_analytics_icon_hover_src_alt = !empty($instance['prporty_analytics_icon_hover_src_alt']) ? $instance['prporty_analytics_icon_hover_src_alt'] : '';


        $link_type_prporty_analytics_url = $instance['link_type_prporty_analytics_url'];
        if ($link_type_prporty_analytics_url == "link") {
            $prporty_analytics_url = $instance['prporty_analytics_url'];
        } elseif ($link_type_prporty_analytics_url == "page") {
            $post_id = $instance['page_prporty_analytics_url'];
            $post = get_post($post_id);
            $prporty_analytics_url = home_url($post->post_name) . "/";
        }

        $prporty_analytics_url = !empty($instance['prporty_analytics_url']) ? $instance['prporty_analytics_url'] : '';
        $prporty_analytics_url_title = !empty($instance['prporty_analytics_url_title']) ? $instance['prporty_analytics_url_title'] : '';

        $prporty_services_icon_src = !empty($instance['prporty_services_icon_src']) ? $instance['prporty_services_icon_src'] : '';
        $prporty_services_icon_src_alt = !empty($instance['prporty_services_icon_src_alt']) ? $instance['prporty_services_icon_src_alt'] : '';

        $prporty_services_icon_hover_src = !empty($instance['prporty_services_icon_hover_src']) ? $instance['prporty_services_icon_hover_src'] : '';
        $prporty_services_icon_hover_src_alt = !empty($instance['prporty_services_icon_hover_src_alt']) ? $instance['prporty_services_icon_hover_src_alt'] : '';

        $link_type_prporty_services_url = $instance['link_type_prporty_services_url'];
        if ($link_type_prporty_services_url == "link") {
            $prporty_services_url = $instance['prporty_services_url'];
        } elseif ($link_type_prporty_services_url == "page") {
            $post_id = $instance['page_prporty_services_url'];
            $post = get_post($post_id);
            $prporty_services_url = home_url($post->post_name) . "/";
        }

        // $prporty_services_url = !empty($instance['prporty_services_url']) ? $instance['prporty_services_url'] : '';
        $prporty_services_url_title = !empty($instance['prporty_services_url_title']) ? $instance['prporty_services_url_title'] : '';

        $prporty_self_services_icon_src = !empty($instance['prporty_self_services_icon_src']) ? $instance['prporty_self_services_icon_src'] : '';
        $prporty_self_services_icon_src_alt = !empty($instance['prporty_self_services_icon_src_alt']) ? $instance['prporty_self_services_icon_src_alt'] : '';
        $prporty_self_services_icon_hover_src = !empty($instance['prporty_self_services_icon_hover_src']) ? $instance['prporty_services_icon_hover_src'] : '';
        $prporty_self_services_icon_hover_src_alt = !empty($instance['prporty_self_services_icon_hover_src_alt']) ? $instance['prporty_self_services_icon_hover_src_alt'] : '';

        $link_type_prporty_self_services_url = $instance['link_type_prporty_self_services_url'];
        if ($link_type_prporty_self_services_url == "link") {
            $prporty_self_services_url = $instance['prporty_self_services_url'];
        } elseif ($link_type_prporty_self_services_url == "page") {
            $post_id = $instance['page_prporty_self_services_url'];
            $post = get_post($post_id);
            $prporty_self_services_url = home_url($post->post_name) . "/";
        }

        //$prporty_self_services_url = !empty($instance['prporty_self_services_url']) ? $instance['prporty_self_services_url'] : '';
        $prporty_self_services_url_title = !empty($instance['prporty_self_services_url_title']) ? $instance['prporty_self_services_url_title'] : '';

        $prporty_digital_payment_src_icon_src = !empty($instance['prporty_digital_payment_src_icon_src']) ? $instance['prporty_digital_payment_src_icon_src'] : '';
        $prporty_digital_payment_src_icon_src_alt = !empty($instance['prporty_digital_payment_src_icon_src_alt']) ? $instance['prporty_digital_payment_src_icon_src_alt'] : '';

        $prporty_digital_payment_src_icon_hover_src = !empty($instance['prporty_digital_payment_src_icon_hover_src']) ? $instance['prporty_digital_payment_src_icon_hover_src'] : '';
        $prporty_digital_payment_src_icon_hover_src_alt = !empty($instance['prporty_digital_payment_src_icon_hover_src_alt']) ? $instance['prporty_digital_payment_src_icon_hover_src_alt'] : '';
        $link_type_prporty_digital_payment_url = $instance['link_type_prporty_digital_payment_url'];
        if ($link_type_prporty_digital_payment_url == "link") {
            $prporty_digital_payment_url = $instance['prporty_digital_payment_url'];
        } elseif ($link_type_prporty_digital_payment_url == "page") {
            $post_id = $instance['page_prporty_digital_payment_url'];
            $post = get_post($post_id);
            $prporty_digital_payment_url = home_url($post->post_name) . "/";
        }

        //$prporty_digital_payment_url = !empty($instance['prporty_digital_payment_url']) ? $instance['prporty_digital_payment_url'] : '';
        $prporty_digital_payment_url_title = !empty($instance['prporty_digital_payment_url_title']) ? $instance['prporty_digital_payment_url_title'] : '';

//        4th row

        $link_type_property_sales_catering_url = $instance['link_type_property_sales_catering_url'];
        if ($link_type_property_sales_catering_url == "link") {
            $property_sales_catering_url = $instance['property_sales_catering_url'];
        } elseif ($link_type_property_sales_catering_url == "page") {
            $post_id = $instance['page_property_sales_catering_url'];
            $post = get_post($post_id);
            $property_sales_catering_url = home_url($post->post_name) . "/";
        }
       // $property_sales_catering_url = !empty($instance['property_sales_catering_url']) ? $instance['property_sales_catering_url'] : '';
        $property_sales_catering_url_title = !empty($instance['property_sales_catering_url_title']) ? $instance['property_sales_catering_url_title'] : '';


        $property_sales_catering_icon_src = !empty($instance['property_sales_catering_icon_src']) ? $instance['property_sales_catering_icon_src'] : '';
        $property_sales_catering_icon_src_alt = !empty($instance['property_sales_catering_icon_src_alt']) ? $instance['property_sales_catering_icon_src_alt'] : '';

        $property_sales_catering_icon_hover_src = !empty($instance['property_sales_catering_icon_hover_src']) ? $instance['property_sales_catering_icon_hover_src'] : '';
        $property_sales_catering_icon_hover_src_alt = !empty($instance['property_sales_catering_icon_hover_src_alt']) ? $instance['property_sales_catering_icon_hover_src_alt'] : '';




        $link_type_property_golf_url = $instance['link_type_property_golf_url'];
        if ($link_type_property_golf_url == "link") {
            $property_golf_url = $instance['property_golf_url'];
        } elseif ($link_type_property_golf_url == "page") {
            $post_id = $instance['page_property_golf_url'];
            $post = get_post($post_id);
            $property_golf_url = home_url($post->post_name) . "/";
        }

        //$property_golf_url = !empty($instance['property_golf_url']) ? $instance['property_golf_url'] : '';
        $property_golf_url_title = !empty($instance['property_golf_url_title']) ? $instance['property_golf_url_title'] : '';
        $property_golf_src = !empty($instance['property_golf_src']) ? $instance['property_golf_src'] : '';
        $property_golf_hover_src = !empty($instance['property_golf_hover_src']) ? $instance['property_golf_hover_src'] : '';

        ?>


<section class="homeHospitalitySection" data-aos="fade-down" data-aos-delay="300" data-aos-duration="400"
    data-aos-once="true">
    <div class="homeHospitalityTitle center">
        <h2 class="greenText dinProStd"><?php echo $hospitality_title; ?></h2>
    </div>

    <div class="homeHospitalityButton center flex" data-aos="fade-up" data-aos-delay="300" data-aos-duration="400"
        data-aos-once="true">
        <a class="activeButton" id="gustButton"><?php echo $guest_title; ?></a>
        <a class="" id="propertyButton"><?php echo $property; ?></a>
    </div>

    <div class="homeHospitality" id="gust" style="background:url(<?php echo get_template_directory_uri();?>/img/home-hospitality-bg-1.svg) no-repeat;position: relative;
    background-position: center;background-size: contain;">
        <div class="homeHospitalityRow1 flex">

            <div class="homeHospitalIconBox flex" leaders_hospitality_icon_image>
                <a href="<?php echo $booking_url; ?>">
                    <div class="homeHospitalImg">
                        <img class="img-fluid greenImg" src="<?php echo $booking_icon_src; ?>"
                            alt="<?php echo $booking_icon_src_alt; ?>" />
                        <img class="img-fluid pinkImg" src="<?php echo $booking_hover_icon_src ?>" ;
                            alt="<?php echo $booking_hover_icon_src_alt; ?>" />
                    </div>
                    <div class="homeHospitalText">
                        <p class="dinProStd"><?php echo $booking_title; ?></p>
                    </div>
                </a>
            </div>

            <div class="homeHospitalIconBox flex" leaders_hospitality_icon_image>
                <a href="<?php echo $mobile_url; ?>">
                    <div class="homeHospitalImg">
                        <img class="img-fluid greenImg" src="<?php echo $mobile_icon_src; ?>"
                            alt="<?php echo $mobile_icon_src_alt; ?>" />
                        <img class="img-fluid pinkImg" src="<?php echo $mobile_hover_icon_src; ?>" ;
                            alt="<?php echo $mobile_hover_icon_src_alt; ?>" />
                    </div>
                    <div class="homeHospitalText">
                        <p class="dinProStd"><?php echo $mobile_title; ?></p>
                    </div>
                </a>
            </div>


            <div class="homeHospitalIconBox flex" leaders_hospitality_icon_image>
                <a href="<?php echo $golf_url; ?>">
                    <div class="homeHospitalImg">
                        <img class="img-fluid greenImg" src="<?php echo $golf_icon_src; ?>"
                            alt="<?php echo $golf_icon_src_alt; ?>" />
                        <img class="img-fluid pinkImg" src="<?php echo $golf_hover_icon_src; ?>"
                            alt="<?php echo $golf_hover_icon_src_alt; ?>" />

                    </div>
                    <div class="homeHospitalText">
                        <p class="dinProStd"><?php echo $golf_title; ?></p>
                    </div>
                </a>
            </div>

        </div>

        <div class="homeHospitalityRow2 flex" leaders_hospitality_icon_image>

            <div class="homeHospitalIconBox flex" leaders_hospitality_icon_image>
                <a href="<?php echo $digital_url; ?>">
                    <div class="homeHospitalImg">
                        <img class="img-fluid greenImg" src="<?php echo $digital_icon_src; ?>"
                            alt="<?php echo $digital_icon_src_alt; ?>" />
                        <img class="img-fluid pinkImg" src="<?php echo $digital_hover_icon_src; ?>"
                            alt="<?php echo $digital_hover_icon_src_alt; ?>" />
                    </div>
                    <div class="homeHospitalText">
                        <p class="dinProStd"><?php echo $digital_url_title; ?></p>
                    </div>
                </a>
            </div>


            <div class="homeHospitalIconBox flex" leaders_hospitality_icon_image>
                <a href="<?php echo $dining_url; ?>">
                    <div class="homeHospitalImg">
                        <img class="img-fluid greenImg" src="<?php echo $dining_icon_src; ?>"
                            alt="<?php echo $dining_icon_src_alt; ?>" />
                        <img class="img-fluid pinkImg" src="<?php echo $dining_hover_icon_src; ?>"
                            alt="<?php echo $dining_hover_icon_src_alt; ?>" />
                    </div>
                    <div class="homeHospitalText">
                        <p class="dinProStd"><?php echo $dining_url_title; ?></p>
                    </div>
                </a>
            </div>


            <div class="homeHospitalIconBox flex" leaders_hospitality_icon_image>
                <a href="<?php echo $payment_url; ?>">
                    <div class="homeHospitalImg">
                        <img class="img-fluid greenImg" src="<?php echo $payment_icon_src; ?>"
                            alt="<?php echo $payment_icon_src_alt; ?>" />
                        <img class="img-fluid pinkImg" src="<?php echo $payment_hover_icon_src; ?>"
                            alt="<?php echo $payment_hover_icon_src_alt; ?>" />
                    </div>
                    <div class="homeHospitalText">
                        <p class="dinProStd"><?php echo $payment_url_title; ?></p>
                    </div>
                </a>
            </div>


            <div class="homeHospitalIconBox flex" leaders_hospitality_icon_image>
                <a href="<?php echo $spa_url; ?>">
                    <div class="homeHospitalImg">
                        <img class="img-fluid greenImg" src="<?php echo $spa_icon_src; ?>"
                            alt="<?php echo $spa_icon_src_alt; ?>" />
                        <img class="img-fluid pinkImg" src="<?php echo $spa_hover_icon_src; ?>"
                            alt="<?php echo $spa_hover_icon_src_alt; ?>" />
                    </div>
                    <div class="homeHospitalText">
                        <p class="dinProStd"><?php echo $spa_url_title; ?></p>
                    </div>
                </a>
            </div>

        </div>


        <div class="homeHospitalityRow3 flex">
            <div class="homeHospitalIconBox flex" leaders_hospitality_icon_image>
                <a href="<?php echo $venue_url; ?>">
                    <div class="homeHospitalImg">
                        <img class="img-fluid greenImg" src="<?php echo $venue_icon_src; ?>"
                            alt="<?php echo $venue_icon_src_alt; ?>" />
                        <img class="img-fluid pinkImg" src="<?php echo $venue_hover_icon_src; ?>"
                            alt="<?php echo $venue_hover_icon_src_alt; ?>" />
                    </div>
                    <div class="homeHospitalText">
                        <p class="dinProStd"><?php echo $venue_url_title; ?></p>
                    </div>
                </a>
            </div>
            <div class="homeHospitalIconBox flex" leaders_hospitality_icon_image>
                <a href="<?php echo $mobile_check_url; ?>">
                    <div class="homeHospitalImg">
                        <img class="img-fluid greenImg" src="<?php echo $mobile_check_icon_src; ?>"
                            alt="<?php echo $mobile_check_icon_src_alt; ?>" />
                        <img class="img-fluid pinkImg" src="<?php echo $mobile_check_hover_icon_src; ?>"
                            alt="<?php echo $mobile_check_hover_icon_src_alt; ?>" />
                    </div>
                    <div class="homeHospitalText">
                        <p class="dinProStd"><?php echo $mobile_check_url_title; ?></p>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <div class="homeHospitality hide" id="property" style="background:url(<?php echo get_template_directory_uri();?>/img/home-hospitality-bg.svg);background-repeat: no-repeat;position: relative;
    background-position: center;background-size: contain;">
        <div class="homeHospitalityRow1 flex">
            <!--            1st row-->
            <div class="homeHospitalIconBox flex" leaders_hospitality_icon_image>
                <a href="<?php echo $digital_marketing_url; ?>">
                    <div class="homeHospitalImg">
                        <img class="img-fluid greenImg" src="<?php echo $digital_marketing_src; ?>"
                            alt="<?php echo $digital_marketing_src_alt; ?>" />
                        <img class="img-fluid pinkImg" src="<?php echo $digital_marketing_hover_src; ?>" ;
                            alt="<?php echo $digital_marketing_hover_src_alt; ?>" />
                    </div>
                    <div class="homeHospitalText">
                        <p class="dinProStd"><?php echo $digital_marketing_url_title; ?></p>
                    </div>
                </a>
            </div>


            <div class="homeHospitalIconBox flex" leaders_hospitality_icon_image>
                <a href="<?php echo $property_booking_url; ?>">
                    <div class="homeHospitalImg">
                        <img class="img-fluid greenImg" src="<?php echo $property_booking_icon_src; ?>"
                            alt="<?php echo $property_booking_icon_src_alt; ?>" />
                        <img class="img-fluid pinkImg" src="<?php echo $property_booking_hover_icon_src; ?>"
                            alt="<?php echo $property_booking_hover_icon_src_alt; ?>" />
                    </div>
                    <div class="homeHospitalText">
                        <p class="dinProStd"><?php echo $property_booking_url_title; ?></p>
                    </div>
                </a>
            </div>

            <div class="homeHospitalIconBox flex" leaders_hospitality_icon_image>
                <a href="<?php echo $property_mgmt_url; ?>">
                    <div class="homeHospitalImg">
                        <img class="img-fluid greenImg" src="<?php echo $property_mgmt_icon_src; ?>"
                            alt="<?php echo $property_mgmt_icon_src_alt; ?>" />
                        <img class="img-fluid pinkImg" src="<?php echo $property_mgmt_hover_icon_src; ?>"
                            alt="<?php echo $property_mgmt_hover_icon_src_alt; ?>" />
                    </div>
                    <div class="homeHospitalText">
                        <p class="dinProStd"><?php echo $property_mgmt_url_title; ?></p>
                    </div>
                </a>
            </div>


        </div>

        <!--        2nd row-->

        <div class="homeHospitalityRow2 flex">
            <div class="homeHospitalIconBox flex" data-aos="zoom-in" data-aos-delay="300" data-aos-duration="400"
                data-aos-once="true">
                <a href="<?php echo $pos_url; ?>">
                    <div class="homeHospitalImg">
                        <img class="img-fluid greenImg" src="<?php echo $pos_icon_src; ?>"
                            alt="<?php echo $pos_icon_src_alt; ?>" />
                        <img class="img-fluid pinkImg" src="<?php echo $pos_hover_icon_src; ?>"
                            alt="<?php echo $pos_hover_icon_src_alt; ?>" />
                    </div>
                    <div class="homeHospitalText">
                        <p class="dinProStd"><?php echo $pos_url_title; ?></p>
                    </div>
                </a>
            </div>
            <div class="homeHospitalIconBox flex" data-aos="zoom-in" data-aos-delay="300" data-aos-duration="400"
                data-aos-once="true">
                <a href="<?php echo $inventory_url; ?>">
                    <div class="homeHospitalImg">
                        <img class="img-fluid greenImg" src="<?php echo $inventory_icon_src; ?>"
                            alt="<?php echo $inventory_icon_src_alt; ?>" />
                        <img class="img-fluid pinkImg" src="<?php echo $inventory_hover_icon_src; ?>"
                            alt="<?php echo $inventory_hover_icon_src_alt; ?>" />
                    </div>
                    <div class="homeHospitalText">
                        <p class="dinProStd"><?php echo $inventory_url_title; ?></p>
                    </div>
                </a>
            </div>
            <div class="homeHospitalIconBox flex" data-aos="zoom-in" data-aos-delay="300" data-aos-duration="400"
                data-aos-once="true">
                <a href="<?php echo $document_url; ?>">
                    <div class="homeHospitalImg">
                        <img class="img-fluid greenImg" src="<?php echo $document_icon_src; ?>"
                            alt="<?php echo $document_icon_src_alt; ?>" />
                        <img class="img-fluid pinkImg" src="<?php echo $document_hover_icon_src; ?>"
                            alt="<?php echo $document_hover_icon_src_alt; ?>" />
                    </div>
                    <div class="homeHospitalText">
                        <p class="dinProStd"><?php echo $document_url_title; ?></p>
                    </div>
                </a>
            </div>
            <div class="homeHospitalIconBox flex" data-aos="zoom-in" data-aos-delay="300" data-aos-duration="400"
                data-aos-once="true">
                <a href="<?php echo $prporty_spa_url; ?>">
                    <div class="homeHospitalImg">
                        <img class="img-fluid greenImg" src="<?php echo $prporty_spa_icon_src; ?>"
                            alt="<?php echo $prporty_spa_icon_src_alt; ?>" />
                        <img class="img-fluid pinkImg" src="<?php echo $prporty_spa_hover_icon_src; ?>"
                            alt="<?php echo $prporty_spa_hover_icon_src_alt; ?>" />
                    </div>
                    <div class="homeHospitalText">
                        <p class="dinProStd"><?php echo $prporty_spa_url_title; ?></p>
                    </div>
                </a>
            </div>
        </div>

        <!--3rd row-->
        <div class="homeHospitalityRow3 homePropertyRow3 flex">
            <div class="homeHospitalIconBox flex aos-init aos-animate" leaders_hospitality_icon_image>
                <a href="<?php echo $prporty_digital_key_url; ?>">
                    <div class="homeHospitalImg">
                        <img class="img-fluid greenImg" src="<?php echo $prporty_digital_key_url_icon_src; ?>"
                            alt="<?php echo $prporty_digital_key_url_icon_src_alt; ?>">
                        <img class="img-fluid pinkImg" src="<?php echo $prporty_digital_key_url_icon_hover_icon_src; ?>"
                            alt="<?php echo $prporty_digital_key_url_icon_hover_icon_src_alt; ?>">
                    </div>
                    <div class="homeHospitalText">
                        <p class="dinProStd"><?php echo $prporty_digital_key_url_title; ?></p>
                    </div>
                </a>
            </div>

            <div class="homeHospitalIconBox flex aos-init aos-animate" leaders_hospitality_icon_image>
                <a href="<?php echo $prporty_mobile_checkin_key_url; ?>">
                    <div class="homeHospitalImg">
                        <img class="img-fluid greenImg" src="<?php echo $prporty_mobile_checkin_icon_src; ?>"
                            alt="<?php echo $prporty_mobile_checkin_icon_src_alt; ?>">
                        <img class="img-fluid pinkImg" src="<?php echo $prporty_mobile_checkin_hover_src; ?>"
                            alt="<?php echo $prporty_mobile_checkin_hover_src_alt; ?>">
                    </div>
                    <div class="homeHospitalText">
                        <p class="dinProStd"><?php echo $prporty_mobile_checkin_key_url_title; ?></p>
                    </div>
                </a>
            </div>

            <!--//    $prporty_analytics_icon_src $prporty_analytics_icon_hover_src $prporty_analytics_url $prporty_analytics_url_title-->
            <div class="homeHospitalIconBox flex aos-init aos-animate" leaders_hospitality_icon_image>
                <a href="<?php echo $prporty_analytics_url; ?>">
                    <div class="homeHospitalImg">
                        <img class="img-fluid greenImg" src="<?php echo $prporty_analytics_icon_src; ?>"
                            alt="<?php echo $prporty_analytics_icon_src_alt; ?>">
                        <img class="img-fluid pinkImg" src="<?php echo $prporty_analytics_icon_hover_src; ?>"
                            alt="<?php echo $prporty_analytics_icon_hover_src_alt; ?>">
                    </div>
                    <div class="homeHospitalText">
                        <p class="dinProStd"><?php echo $prporty_analytics_url_title; ?></p>
                    </div>
                </a>
            </div>


            <div class="homeHospitalIconBox flex aos-init aos-animate" leaders_hospitality_icon_image>
                <a href="<?php echo $prporty_services_url; ?>">
                    <div class="homeHospitalImg">
                        <img class="img-fluid greenImg" src="<?php echo $prporty_services_icon_src; ?>"
                            alt="<?php echo $prporty_services_icon_src_alt; ?>">
                        <img class="img-fluid pinkImg" src="<?php echo $prporty_services_icon_hover_src; ?>"
                            alt="<?php echo $prporty_services_icon_hover_src_alt; ?>">
                    </div>
                    <div class="homeHospitalText">
                        <p class="dinProStd"><?php echo $prporty_services_url_title; ?></p>
                    </div>
                </a>
            </div>

            <div class="homeHospitalIconBox flex aos-init aos-animate" leaders_hospitality_icon_image>
                <a href="<?php echo $prporty_self_services_url; ?>">
                    <div class="homeHospitalImg">
                        <img class="img-fluid greenImg" src="<?php echo $prporty_self_services_icon_src; ?>"
                            alt="<?php echo $prporty_self_services_icon_src_alt; ?>">
                        <img class="img-fluid pinkImg" src="<?php echo $prporty_self_services_icon_hover_src; ?>"
                            alt="<?php echo $prporty_self_services_icon_hover_src_alt; ?>">
                    </div>
                    <div class="homeHospitalText">
                        <p class="dinProStd"><?php echo $prporty_self_services_url_title; ?></p>
                    </div>
                </a>
            </div>

            <div class="homeHospitalIconBox flex aos-init aos-animate" leaders_hospitality_icon_image>
                <a href="<?php echo $prporty_digital_payment_url; ?>">
                    <div class="homeHospitalImg">
                        <img class="img-fluid greenImg" src="<?php echo $prporty_digital_payment_src_icon_src; ?>"
                            alt="<?php echo $prporty_digital_payment_src_icon_src_alt; ?>">
                        <img class="img-fluid pinkImg" src="<?php echo $prporty_digital_payment_src_icon_hover_src; ?>"
                            alt="<?php echo $prporty_digital_payment_src_icon_hover_src_alt; ?>">
                    </div>
                    <div class="homeHospitalText">
                        <p class="dinProStd"><?php echo $prporty_digital_payment_url_title; ?></p>
                    </div>
                </a>
            </div>
        </div>



        <div class="homeHospitalityRow4 flex">
            <div class="homeHospitalIconBox flex aos-init aos-animate" leaders_hospitality_icon_image>
                <a href="<?php echo $property_sales_catering_url; ?>">
                    <div class="homeHospitalImg">
                        <img class="img-fluid greenImg" src="<?php echo $property_sales_catering_icon_src; ?>"
                            alt="<?php echo $property_sales_catering_icon_src_alt; ?>">
                        <img class="img-fluid pinkImg" src="<?php echo $property_sales_catering_icon_hover_src; ?>"
                            alt="<?php echo $property_sales_catering_icon_hover_src_alt; ?>">
                    </div>
                    <div class="homeHospitalText">
                        <p class="dinProStd"><?php echo $property_sales_catering_url_title; ?></p>
                    </div>
                </a>
            </div>
            <div class="homeHospitalIconBox flex aos-init aos-animate" leaders_hospitality_icon_image>
                <a href="<?php echo $property_golf_url; ?>">
                    <div class="homeHospitalImg">
                        <img class="img-fluid greenImg" src="<?php echo $property_golf_src; ?>"
                            alt="<?php echo $property_golf_src_alt; ?>">
                        <img class="img-fluid pinkImg" src="<?php echo $property_golf_hover_src; ?>"
                            alt="<?php echo $property_golf_hover_src_alt; ?>">
                    </div>
                    <div class="homeHospitalText">
                        <p class="dinProStd"><?php echo $property_golf_url_title; ?></p>
                    </div>
                </a>
            </div>
        </div>

    </div>

</section>
<?php

    }

    public function form($instance)
    {

        $hospitality_title = !empty($instance['hospitality_title']) ? $instance['hospitality_title'] : '';
        $guest_title = !empty($instance['guest_title']) ? $instance['guest_title'] : '';
        $property_title = !empty($instance['property_title']) ? $instance['property_title'] : '';

        $src = !empty($instance['booking_icon_src']) ? $instance['booking_icon_src'] : '';
        $alt = !empty($instance['booking_icon_src_alt']) ? $instance['booking_icon_src_alt'] : '';
        $hoversrc = !empty($instance['booking_hover_icon_src']) ? $instance['booking_hover_icon_src'] : '';
        $hoveralt = !empty($instance['booking_hover_icon_src_alt']) ? $instance['booking_hover_icon_src_alt'] : '';

        $booking_url = !empty($instance['booking_url']) ? $instance['booking_url'] : '';
        $booking_url_title = !empty($instance['booking_url_title']) ? $instance['booking_url_title'] : '';

        $mobile_icon_src = !empty($instance['mobile_icon_src']) ? $instance['mobile_icon_src'] : '';
        $mobile_icon_src_alt = !empty($instance['mobile_icon_src_alt']) ? $instance['mobile_icon_src_alt'] : '';
        $mobile_hover_icon_src = !empty($instance['mobile_hover_icon_src']) ? $instance['mobile_hover_icon_src'] : '';
        $mobile_hover_icon_src_alt = !empty($instance['mobile_hover_icon_src_alt']) ? $instance['mobile_hover_icon_src_alt'] : '';

        $mobile_url = !empty($instance['mobile_url']) ? $instance['mobile_url'] : '';
        $mobile_url_title = !empty($instance['mobile_url_title']) ? $instance['mobile_url_title'] : '';

        $golf_icon_src = !empty($instance['golf_icon_src']) ? $instance['golf_icon_src'] : '';
        $golf_icon_src_alt = !empty($instance['golf_icon_src_alt']) ? $instance['golf_icon_src_alt'] : '';

        $golf_hover_icon_src = !empty($instance['golf_hover_icon_src']) ? $instance['golf_hover_icon_src'] : '';
        $golf_hover_icon_src_alt = !empty($instance['golf_hover_icon_src_alt']) ? $instance['golf_hover_icon_src_alt'] : '';

        $golf_url = !empty($instance['golf_url']) ? $instance['golf_url'] : '';
        $golf_url_title = !empty($instance['golf_url_title']) ? $instance['golf_url_title'] : '';

        $digital_icon_src = !empty($instance['digital_icon_src']) ? $instance['digital_icon_src'] : '';
        $digital_icon_src_alt = !empty($instance['digital_icon_src_alt']) ? $instance['digital_icon_src_alt'] : '';
        $digital_hover_icon_src = !empty($instance['digital_hover_icon_src']) ? $instance['digital_hover_icon_src'] : '';
        $digital_hover_icon_src_alt = !empty($instance['digital_hover_icon_src_alt']) ? $instance['digital_hover_icon_src_alt'] : '';

        $digital_url = !empty($instance['digital_url']) ? $instance['digital_url'] : '';
        $digital_url_title = !empty($instance['digital_url_title']) ? $instance['digital_url_title'] : '';

        $dining_icon_src = !empty($instance['dining_icon_src']) ? $instance['dining_icon_src'] : '';
        $dining_icon_src_alt = !empty($instance['dining_icon_src_alt']) ? $instance['dining_icon_src_alt'] : '';
        $dining_hover_icon_src = !empty($instance['dining_hover_icon_src']) ? $instance['dining_hover_icon_src'] : '';
        $dining_hover_icon_src_alt = !empty($instance['dining_hover_icon_src_alt']) ? $instance['dining_hover_icon_src_alt'] : '';
        $dining_url = !empty($instance['dining_url']) ? $instance['dining_url'] : '';
        $dining_url_title = !empty($instance['dining_url_title']) ? $instance['dining_url_title'] : '';

        $payment_icon_src = !empty($instance['payment_icon_src']) ? $instance['payment_icon_src'] : '';
        $payment_icon_src_alt = !empty($instance['payment_icon_src_alt']) ? $instance['payment_icon_src_alt'] : '';
        $payment_hover_icon_src = !empty($instance['payment_hover_icon_src']) ? $instance['payment_hover_icon_src'] : '';
        $payment_hover_icon_src_alt = !empty($instance['payment_hover_icon_src_alt']) ? $instance['payment_hover_icon_src_alt'] : '';
        $payment_url = !empty($instance['payment_url']) ? $instance['payment_url'] : '';
        $payment_url_title = !empty($instance['payment_url_title']) ? $instance['payment_url_title'] : '';

//         spa
        $spa_icon_src = !empty($instance['spa_icon_src']) ? $instance['spa_icon_src'] : '';
        $spa_icon_src_alt = !empty($instance['spa_icon_src_alt']) ? $instance['spa_icon_src_alt'] : '';
        $spa_hover_icon_src = !empty($instance['spa_hover_icon_src']) ? $instance['spa_hover_icon_src'] : '';
        $spa_hover_icon_src_alt = !empty($instance['spa_hover_icon_src_alt']) ? $instance['spa_hover_icon_src_alt'] : '';
        $spa_url = !empty($instance['spa_url']) ? $instance['spa_url'] : '';
        $spa_url_title = !empty($instance['spa_url_title']) ? $instance['spa_url_title'] : '';

//        venue
        $venue_icon_src = !empty($instance['venue_icon_src']) ? $instance['venue_icon_src'] : '';
        $venue_icon_src_alt = !empty($instance['venue_icon_src_alt']) ? $instance['venue_icon_src_alt'] : '';

        $venue_hover_icon_src = !empty($instance['venue_hover_icon_src']) ? $instance['venue_hover_icon_src'] : '';
        $venue_hover_icon_src_alt = !empty($instance['venue_hover_icon_src_alt']) ? $instance['venue_hover_icon_src_alt'] : '';

        $venue_url = !empty($instance['venue_url']) ? $instance['venue_url'] : '';
        $venue_url_title = !empty($instance['venue_url_title']) ? $instance['venue_url_title'] : '';

        //        mobile checkout
        $mobile_check_icon_src = !empty($instance['mobile_check_icon_src']) ? $instance['mobile_check_icon_src'] : '';
        $mobile_check_icon_src_alt = !empty($instance['mobile_check_icon_src_alt']) ? $instance['mobile_check_icon_src_alt'] : '';

        $mobile_check_hover_icon_src = !empty($instance['mobile_check_hover_icon_src']) ? $instance['mobile_check_hover_icon_src'] : '';
        $mobile_check_hover_icon_src_alt = !empty($instance['mobile_check_hover_icon_src_alt']) ? $instance['mobile_check_hover_icon_src_alt'] : '';
        $mobile_check_url = !empty($instance['mobile_check_url']) ? $instance['mobile_check_url'] : '';
        $mobile_check_url_title = !empty($instance['mobile_check_url_title']) ? $instance['mobile_check_url_title'] : '';

//                       property

        $digital_marketing_src = !empty($instance['digital_marketing_src']) ? $instance['digital_marketing_src'] : '';
        $digital_marketing_src_alt = !empty($instance['digital_marketing_src_alt']) ? $instance['digital_marketing_src_alt'] : '';

        $digital_marketing_hover_src = !empty($instance['digital_marketing_hover_src']) ? $instance['digital_marketing_hover_src'] : '';
        $digital_marketing_hover_src_alt = !empty($instance['digital_marketing_hover_src_alt']) ? $instance['digital_marketing_hover_src_alt'] : '';


        $digital_marketing_url = !empty($instance['digital_marketing_url']) ? $instance['digital_marketing_url'] : '';
        $digital_marketing_url_title = !empty($instance['digital_marketing_url_title']) ? $instance['digital_marketing_url_title'] : '';

        $property_booking_icon_src = !empty($instance['property_booking_icon_src']) ? $instance['property_booking_icon_src'] : '';
        $property_booking_icon_src_alt = !empty($instance['property_booking_icon_src_alt']) ? $instance['property_booking_icon_src_alt'] : '';


        $property_booking_hover_icon_src = !empty($instance['property_booking_hover_icon_src']) ? $instance['property_booking_hover_icon_src'] : '';
        $property_booking_hover_icon_src_alt = !empty($instance['property_booking_hover_icon_src_alt']) ? $instance['property_booking_hover_icon_src_alt'] : '';

        $property_booking_url = !empty($instance['property_booking_url']) ? $instance['property_booking_url'] : '';
        $property_booking_url_title = !empty($instance['property_booking_url_title']) ? $instance['property_booking_url_title'] : '';

        $property_mgmt_icon_src = !empty($instance['property_mgmt_icon_src']) ? $instance['property_mgmt_icon_src'] : '';
        $property_mgmt_icon_src_alt = !empty($instance['property_mgmt_icon_src_alt']) ? $instance['property_mgmt_icon_src_alt'] : '';
        $property_mgmt_hover_icon_src = !empty($instance['property_mgmt_hover_icon_src']) ? $instance['property_mgmt_hover_icon_src'] : '';
        $property_mgmt_hover_icon_src_alt = !empty($instance['property_mgmt_hover_icon_src_alt']) ? $instance['property_mgmt_hover_icon_src_alt'] : '';

        $property_mgmt_url = !empty($instance['property_mgmt_url']) ? $instance['property_mgmt_url'] : '';
        $property_mgmt_url_title = !empty($instance['property_mgmt_url_title']) ? $instance['property_mgmt_url_title'] : '';

        $pos_icon_src = !empty($instance['pos_icon_src']) ? $instance['pos_icon_src'] : '';
        $pos_icon_src_alt = !empty($instance['pos_icon_src_alt']) ? $instance['pos_icon_src_alt'] : '';
        $pos_hover_icon_src = !empty($instance['pos_hover_icon_src']) ? $instance['pos_hover_icon_src'] : '';
        $pos_hover_icon_src_alt = !empty($instance['pos_hover_icon_src_alt']) ? $instance['pos_hover_icon_src_alt'] : '';

        $pos_url = !empty($instance['pos_url']) ? $instance['pos_url'] : '';
        $pos_url_title = !empty($instance['pos_url_title']) ? $instance['pos_url_title'] : '';

        $inventory_icon_src = !empty($instance['inventory_icon_src']) ? $instance['inventory_icon_src'] : '';
        $inventory_icon_src_alt = !empty($instance['inventory_icon_src_alt']) ? $instance['inventory_icon_src_alt'] : '';

        $inventory_hover_icon_src = !empty($instance['inventory_hover_icon_src']) ? $instance['inventory_hover_icon_src'] : '';
        $inventory_hover_icon_src_alt = !empty($instance['inventory_hover_icon_src_alt']) ? $instance['inventory_hover_icon_src_alt'] : '';


        $inventory_url = !empty($instance['inventory_url']) ? $instance['inventory_url'] : '';
        $inventory_url_title = !empty($instance['inventory_url_title']) ? $instance['inventory_url_title'] : '';

        $document_icon_src = !empty($instance['document_icon_src']) ? $instance['document_icon_src'] : '';
        $document_icon_src_alt = !empty($instance['document_icon_src_alt']) ? $instance['document_icon_src_alt'] : '';

        $document_hover_icon_src = !empty($instance['document_hover_icon_src']) ? $instance['document_hover_icon_src'] : '';
        $document_hover_icon_src_alt = !empty($instance['document_hover_icon_src_alt']) ? $instance['document_hover_icon_src_alt'] : '';
        $document_url = !empty($instance['document_url']) ? $instance['document_url'] : '';
        $document_url_title = !empty($instance['document_url_title']) ? $instance['document_url_title'] : '';

//         property  spa
        $prporty_spa_icon_src = !empty($instance['prporty_spa_icon_src']) ? $instance['prporty_spa_icon_src'] : '';
        $prporty_spa_icon_src_alt = !empty($instance['prporty_spa_icon_src_alt']) ? $instance['prporty_spa_icon_src_alt'] : '';


        $prporty_spa_hover_icon_src = !empty($instance['prporty_spa_hover_icon_src']) ? $instance['prporty_spa_hover_icon_src'] : '';
        $prporty_spa_hover_icon_src_alt = !empty($instance['prporty_spa_hover_icon_src_alt']) ? $instance['prporty_spa_hover_icon_src_alt'] : '';

        $prporty_spa_url = !empty($instance['prporty_spa_url']) ? $instance['prporty_spa_url'] : '';
        $prporty_spa_url_title = !empty($instance['prporty_spa_url_title']) ? $instance['prporty_spa_url_title'] : '';

//         3rd row
        $prporty_digital_key_url_icon_src = !empty($instance['prporty_digital_key_url_icon_src']) ? $instance['prporty_digital_key_url_icon_src'] : '';
        $prporty_digital_key_url_icon_src_alt = !empty($instance['prporty_digital_key_url_icon_src_alt']) ? $instance['prporty_digital_key_url_icon_src_alt'] : '';


        $prporty_digital_key_url_icon_hover_icon_src = !empty($instance['prporty_digital_key_url_icon_hover_icon_src']) ? $instance['prporty_digital_key_url_icon_hover_icon_src'] : '';
        $prporty_digital_key_url_icon_hover_icon_src_alt = !empty($instance['prporty_digital_key_url_icon_hover_icon_src_alt']) ? $instance['prporty_digital_key_url_icon_hover_icon_src_alt'] : '';

        $prporty_digital_key_url = !empty($instance['prporty_digital_key_url']) ? $instance['prporty_digital_key_url'] : '';
        $prporty_digital_key_url_title = !empty($instance['prporty_digital_key_url_title']) ? $instance['prporty_digital_key_url_title'] : '';

        $prporty_mobile_checkin_icon_src = !empty($instance['prporty_mobile_checkin_icon_src']) ? $instance['prporty_mobile_checkin_icon_src'] : '';
        $prporty_mobile_checkin_icon_src_alt = !empty($instance['prporty_mobile_checkin_icon_src_alt']) ? $instance['prporty_mobile_checkin_icon_src_alt'] : '';

        $prporty_mobile_checkin_hover_src = !empty($instance['prporty_mobile_checkin_hover_src']) ? $instance['prporty_mobile_checkin_hover_src'] : '';
        $prporty_mobile_checkin_hover_src_alt = !empty($instance['prporty_mobile_checkin_hover_src_alt']) ? $instance['prporty_mobile_checkin_hover_src_alt'] : '';

        $prporty_mobile_checkin_key_url = !empty($instance['prporty_mobile_checkin_key_url']) ? $instance['prporty_mobile_checkin_key_url'] : '';
        $prporty_mobile_checkin_key_url_title = !empty($instance['prporty_mobile_checkin_key_url_title']) ? $instance['prporty_mobile_checkin_key_url_title'] : '';

        $prporty_analytics_icon_src = !empty($instance['prporty_analytics_icon_src']) ? $instance['prporty_analytics_icon_src'] : '';
        $prporty_analytics_icon_src_alt = !empty($instance['prporty_analytics_icon_src_alt']) ? $instance['prporty_analytics_icon_src_alt'] : '';
        $prporty_analytics_icon_hover_src = !empty($instance['prporty_analytics_icon_hover_src']) ? $instance['prporty_analytics_icon_hover_src'] : '';
        $prporty_analytics_icon_hover_src_alt = !empty($instance['prporty_analytics_icon_hover_src_alt']) ? $instance['prporty_analytics_icon_hover_src_alt'] : '';


        $prporty_analytics_url = !empty($instance['prporty_analytics_url']) ? $instance['prporty_analytics_url'] : '';
        $prporty_analytics_url_title = !empty($instance['prporty_analytics_url_title']) ? $instance['prporty_analytics_url_title'] : '';

        $prporty_services_icon_src = !empty($instance['prporty_services_icon_src']) ? $instance['prporty_services_icon_src'] : '';

        
        $prporty_services_icon_src_alt = !empty($instance['prporty_services_icon_src_alt']) ? $instance['prporty_services_icon_src_alt'] : '';

        $prporty_services_icon_hover_src = !empty($instance['prporty_services_icon_hover_src']) ? $instance['prporty_services_icon_hover_src'] : '';
        $prporty_services_icon_hover_src_alt = !empty($instance['prporty_services_icon_hover_src_alt']) ? $instance['prporty_services_icon_hover_src_alt'] : '';
        $prporty_services_url = !empty($instance['prporty_services_url']) ? $instance['prporty_services_url'] : '';
        $prporty_services_url_title = !empty($instance['prporty_services_url_title']) ? $instance['prporty_services_url_title'] : '';

        $prporty_self_services_icon_src = !empty($instance['prporty_self_services_icon_src']) ? $instance['prporty_self_services_icon_src'] : '';
        $prporty_self_services_icon_src_alt = !empty($instance['prporty_self_services_icon_src_alt']) ? $instance['prporty_self_services_icon_src_alt'] : '';
        $prporty_self_services_icon_hover_src = !empty($instance['prporty_self_services_icon_hover_src']) ? $instance['prporty_services_icon_hover_src'] : '';
        $prporty_self_services_icon_hover_src_alt = !empty($instance['prporty_self_services_icon_hover_src_alt']) ? $instance['prporty_self_services_icon_hover_src_alt'] : '';

        $prporty_self_services_url = !empty($instance['prporty_self_services_url']) ? $instance['prporty_self_services_url'] : '';
        $prporty_self_services_url_title = !empty($instance['prporty_self_services_url_title']) ? $instance['prporty_self_services_url_title'] : '';

        $prporty_digital_payment_src_icon_src = !empty($instance['prporty_digital_payment_src_icon_src']) ? $instance['prporty_digital_payment_src_icon_src'] : '';
        $prporty_digital_payment_src_icon_src_alt = !empty($instance['prporty_digital_payment_src_icon_src_alt']) ? $instance['prporty_digital_payment_src_icon_src_alt'] : '';

        $prporty_digital_payment_src_icon_hover_src = !empty($instance['prporty_digital_payment_src_icon_hover_src']) ? $instance['prporty_digital_payment_src_icon_hover_src'] : '';
        $prporty_digital_payment_src_icon_hover_src_alt = !empty($instance['prporty_digital_payment_src_icon_hover_src_alt']) ? $instance['prporty_digital_payment_src_icon_hover_src_alt'] : '';

        $prporty_digital_payment_url = !empty($instance['prporty_digital_payment_url']) ? $instance['prporty_digital_payment_url'] : '';
        $prporty_digital_payment_url_title = !empty($instance['prporty_digital_payment_url_title']) ? $instance['prporty_digital_payment_url_title'] : '';

        //4th row

        $property_sales_catering_url = !empty($instance['property_sales_catering_url']) ? $instance['property_sales_catering_url'] : '';
        $property_sales_catering_url_title = !empty($instance['property_sales_catering_url_title']) ? $instance['property_sales_catering_url_title'] : '';



        $property_sales_catering_icon_src = !empty($instance['property_sales_catering_icon_src']) ? $instance['property_sales_catering_icon_src'] : '';
        $property_sales_catering_icon_src_alt = !empty($instance['property_sales_catering_icon_src_alt']) ? $instance['property_sales_catering_icon_src_alt'] : '';
        $property_sales_catering_icon_hover_src = !empty($instance['property_sales_catering_icon_hover_src']) ? $instance['property_sales_catering_icon_hover_src'] : '';
        $property_sales_catering_icon_hover_src_alt = !empty($instance['property_sales_catering_icon_hover_src_alt']) ? $instance['property_sales_catering_icon_hover_src_alt'] : '';

        $property_golf_url = !empty($instance['property_golf_url']) ? $instance['property_golf_url'] : '';
        $property_golf_url_title = !empty($instance['property_golf_url_title']) ? $instance['property_golf_url_title'] : '';


        $property_golf_src = !empty($instance['property_golf_src']) ? $instance['property_golf_src'] : '';
        $property_golf_src_alt = !empty($instance['property_golf_src_alt']) ? $instance['property_golf_src_alt'] : '';

        $property_golf_hover_src = !empty($instance['property_golf_hover_src']) ? $instance['property_golf_hover_src'] : '';
        $property_golf_hover_src_alt = !empty($instance['property_golf_hover_src_alt']) ? $instance['property_golf_hover_src_alt'] : '';


        wp_enqueue_media();

        ?>
<style type="text/css">
.striped {
    position: relative;
    text-align: center;
    background: #ccc;
    padding: 1%;
    font-size: 1rem;
    font-weight: bold;
}

.ag_hospitality_option_toggle_controller {
    cursor: pointer;
    display: block;
    text-align: center;
    margin: 10px !important;
}

.ag_hospitality_option_toggle {
    display: block;
    border: 1px solid #eee;
    padding: 10px;
    background: #fefefe;
}

.ag_hospitality_widget_wrapper label {
    min-width: 110px;
    display: inline-block;
}

.img-fluid {
    width: 100%;
    height: auto;
}

.flex {
    display: flex;
}

.video-widget-dropdown {}

.widget-content a.add_field_upload_media_widget {
    font-size: 20px;
    text-decoration: none;
    margin-left: 5px;
    background: #0073aa;
    color: #fff;
    padding: 0px 5px;
    line-height: 20px;
    vertical-align: sub;
}

.widget-content a.sisw_upload_image_media {
    font-size: 12px;
    text-decoration: none;
    margin-left: 5px;
    background: #0073aa;
    color: #fff;
    padding: 5px 9px;
    line-height: 20px;
    vertical-align: initial;
    font-weight: 600;
    text-transform: uppercase;
}

h4.sisw_heading-section {
    text-align: center;
    margin: 10px 0 15px;
    background: rgb(245, 245, 245);
    color: #0073aa;
    padding: 5px 0;
    font-size: 14px;
    font-weight: 500;
}

.sisw_admin_image_preview {
    /*display: none;*/
    width: 25px;
    height: 25px;
    vertical-align: top;
}

.sisw_admin_image_preview.active {
    display: inline-block;
}

.sisw_individual_image_section input.sisw_image_input_field {
    width: 100%;
}

span.sisw_drag_Section {
    cursor: move;
}

.widget-content a.sisw_remove_field_upload_media_widget {
    font-size: 20px;
    text-decoration: none;
    margin-left: 5px;
    background: rgb(255, 0, 0);
    color: #fff;
    padding: 0px 5px;
    line-height: 20px;
    vertical-align: sub;
}

table.sisw_multi_image_slider_table_wrapper {
    padding-bottom: 6px;
    border-bottom: 1px solid #ccc;
    margin-bottom: 9px;
    margin-top: 9px;
    width: 100%;

}

table.sisw_multi_image_slider_table_wrapper tr {
    width: 100%;
}

.sisw_slider_options_section {
    display: block;
    width: 100%;
    padding: 10px 0 0;
}

.sisw_slider_options_section label {
    font-size: 13px;
    text-transform: capitalize;
    display: inline-block;
    min-width: 69px;
    font-weight: 500;
}

.sisw_slider_options_section input[type="radio"] {
    vertical-align: middle;
    display: inline-block;
    margin: 0 5px;
}

.sisw_slider_options_section select {
    width: 100%;
    margin-top: 6px;
}

.sisw_multi_image_slider_setting {
    background: #f4f4f4;
    padding: 15px;
    margin: 15px 0;
}

.sisw_multi_image_slider_setting h4 {
    text-align: center;
    text-transform: uppercase;
    margin: 0 !important;
}

.sisw_multi_image_slider_table_wrapper .sisw_individual_image_section span.drag_Section {
    vertical-align: initial;
    padding-right: 6px;
    cursor: pointer;
}

html .widget-content .sisw_remove_field_upload_media_widget {
    margin: 0;
    display: block;
    text-align: center;
    padding: 17px 5px;
}

.sisw_individual_image_section {
    background-color: #eee;
}


div#sisw_single_image_wrapper {
    width: 100%;
    display: block;
    text-align: center;
}

div#sisw_single_image_wrapper img {
    width: 100%;
}

.clearfix:after {
    visibility: hidden;
    display: block;
    font-size: 0;
    content: " ";
    clear: both;
    height: 0;
}

* html .clearfix {
    zoom: 1;
}

/* IE6 */
*:first-child+html .clearfix {
    zoom: 1;
}

/* IE7 */
/*Slider css*/
#jssor_1 {
    position: relative;
    margin: 0 auto;
    top: 0px;
    left: 0px;
    width: 980px;
    height: 380px;
    overflow: hidden;
    visibility: hidden;
}

#jssor_1 .jssor_1_wrapper {
    cursor: default;
    position: relative;
    top: 0px;
    left: 0px;
    width: 980px;
    height: 380px;
    overflow: hidden;
}

h1.slider_widget_title {
    font-size: 24px;
    font-weight: 600;
    text-transform: capitalize;
    text-align: center;
}

.owl-item {
    display: inline-block;
}

.sisw_multi_image_slider_wrapper {
    overflow: hidden;
    text-align: center;
    width: 100%;
    position: relative;
}

.sisw_multi_image_slider_wrapper .owl-item,
.sisw_multi_image_slider_wrapper .item {
    margin: 0;
    padding: 0;
    text-align: center;
}

.sisw_multi_image_slider_wrapper .item {
    display: none;
}

.sisw_multi_image_slider_wrapper .item:first-child {
    display: block;
}

.sisw_multi_image_slider_wrapper a,
.sisw_multi_image_slider_wrapper a:focus {
    display: block;
    margin: 0;
    padding: 0;
    outline: 0;
}

.sisw_multi_image_slider_wrapper a img {
    display: inline-block;
    margin: 0;
    padding: 0;
}

.sisw_multi_image_slider_wrapper .owl-nav {
    position: absolute;
    top: 50%;
    width: 100%;
    margin-top: -23px !important;
}

.sisw_multi_image_slider_wrapper .owl-nav button.owl-next {
    position: absolute;
    right: 0;
    background: rgba(0, 0, 0, 0.4);
    font-size: 32px;
    line-height: 32px;
    padding: 0;
    outline: 0;
    height: 38px;
    width: 28px;
}

.sisw_multi_image_slider_wrapper .owl-nav button.owl-next:hover {
    background: rgba(0, 0, 0, 0.4);
}

.sisw_multi_image_slider_wrapper .owl-nav button span {
    font-size: 32px;
    font-weight: 400;
    line-height: 32px;
    text-align: center;
    display: block;
    width: 30px;
    height: 100%;
}

.sisw_multi_image_slider_wrapper .owl-nav button.owl-prev {
    position: absolute;
    left: 0;
    background: rgba(0, 0, 0, 0.4);
    font-size: 32px;
    line-height: 32px;
    padding: 0;
    outline: 0;
    height: 38px;
    width: 28px;
}

.sisw_multi_image_slider_wrapper .owl-nav button.owl-prev:hover {
    background: rgba(0, 0, 0, 0.4);
}

.sisw_multi_image_slider_wrapper .owl-nav.disabled {
    display: none;
}

.urlOptions select {
    width: 100px;
}

.agilysys-hospitality-solution-widget {
    width: 550px;
    align-items: center;
}

.ag-hosp-widget-title-lable {
    width: 320px;
}

.ag-hosp-widget-title-input-text {
    width: 300px !important;
    position: relative;
    left: 15%;
}

.ag-hosp-widget-title-input-text input {
    margin-bottom: 5px;
}

.ag-hosp-widget-title-input-text select {
    width: 265px !important;
}
</style>

<style>
/* Responsive Tabs */

/* Mobile First */
.tabSection {
    background: #3C9BD6;

}

.tabSection+.tabSection {
    border-top-width: 0;
}

.tabContent {

    background: #fff;
    padding: 20px !important;

}

.js .tabContent {
    display: none;
}

.tabContent.active {
    display: block;
}

.tabNav {
    display: none;
}

.tabHeader {
    display: block;
    padding: 1em;
    color: #fff;
    cursor: pointer;
    text-decoration: none;
    font-size: 18px;
    padding: 20px !important;

}

.tabHeader:hover,
.tabHeader:focus,
.active {
    background: #3C9BD6 !important;
    text-decoration: none;
}

.js .tabHeader:before {
    content: '\2b\a0';
}

.js .tabHeader.active:before {
    content: '\2013\a0';
}

/* Large View - for most sites, min-width should be 60em or 40em, for this demo it's 30em */
@media (min-width: 30em) {
    .tabNav {
        display: block;
    }

    .tabNav a {
        display: block;
        padding: 0.5em 1.5em;
        float: left;
        background: #fff;
        color: #5a5a5a;
        text-decoration: none;
        padding: 20px !important;
    }

    .tabNav a:hover,
    .tabNav a:focus {
        text-decoration: none;
        background: #fff;

    }

    .tabNav a.active {
        background: #fff;
        color: #fff;
    }

    .tabHeader {
        display: none;
    }

    .tabPanel {}

    .tabContent {
        padding: 1em
    }

    .tabSection {
        display: none;
    }

    .tabSection+.tabSection {
        border-top-width: 1px;
    }

    .tabSection.active {
        display: block;
    }

    .tabSection.active .tabContent {
        display: block;
    }

    .no-js .tabSection {
        display: block;
    }
}

/* Demo styles (DON'T COPY) */
.clearfix:after {
    content: '';
    display: table;
    clear: both;
}

.col-md-6 {
    width: 50%;
    float: left;
}
</style>


<script>
jQuery(document).ready(function() {


    jQuery('.tabs').each(function(i) {
        var tab = jQuery(this);
        tab.wrapInner('<div class="tabPanel">')
        tab.prepend('<div class="tabNav clearfix" />');
        tab.find('.tabSection').each(function(j) {
            jQuery(this).attr('id', 'tabs' + i + j);
            jQuery(this).children('.tabHeader').attr('href', '#tabs' + i + j);
            tab.children('.tabNav').append('<a href="#tabs' + i + j + '">' + jQuery(this)
                .children('.tabHeader').html() + '</a>');
        });

        //Activates the first tab for desktop
        tab.find('.tabSection:first').addClass('active');
        tab.find('.tabNav a:first').addClass('active');

        //Accordian
        tab.find('.tabHeader').each(function(j) {
            jQuery(this).on('click', function(e) {
                e.preventDefault();
                var index = jQuery(this).parent().index();
                if (!jQuery(this).hasClass('active')) {
                    tab.find('.tabSection > .active').removeClass('active');
                    tab.find('.tabSection:eq(' + index + ')').children().toggleClass(
                        'active');
                } else {
                    tab.find('.tabSection > .active').removeClass('active');
                };
            })
        });

        //Tab
        tab.find('.tabNav a').each(function(j) {
            jQuery(this).on('click', function(e) {
                e.preventDefault();
                var index = jQuery(this).index();
                tab.find('.tabSection, .tabNav a').removeClass('active');
                tab.find('.tabSection:eq(' + index + '), .tabNav a:eq(' + index + ')')
                    .toggleClass('active');
            })
        });
    });
});
</script>


<script type="text/javascript">
jQuery(document).ready(function($) {
    $('.ag_hospitality_option_toggle_controller').on('click', function() {
        var toToggle = $(this).siblings('.ag_hospitality_option_toggle').eq(0);
        toToggle.fadeIn();
    });
    if ($('.ag_hospitality_upload_btn').length > 0) {
        if (typeof wp !== 'undefined' && wp.media && wp.media.editor) {
            $(document).on('click', '.ag_hospitality_upload_btn', function(e) {
                e.preventDefault();
                var button = $(this);
                var id = button.prev();
                wp.media.editor.send.attachment = function(props, attachment) {
                    id.val(attachment.url);

                };
                wp.media.editor.open(button);
                return false;
            });
        }
    }
});

jQuery(document).ready(function($) {

    // Instantiates the variable that holds the media library frame.
    var meta_image_frame;

    // Runs when the image button is clicked.

    $('.dynamic-image-button').click(function(e) {
        // Prevents the default action from occuring.
        e.preventDefault();
        // If the frame already exists, re-open it.
        if (meta_image_frame) {
            meta_image_frame.open();
            return;
        }
        // Sets up the media library frame
        meta_image_frame = wp.media.frames.meta_image_frame = wp.media({
            title: meta_image.title,
            button: {
                text: meta_image.button
            },
            library: {
                type: 'image'
            }
        });
        // Runs when an image is selected.
        meta_image_frame.on('select', function() {
            var media_attachment = meta_image_frame.state().get('selection').first().toJSON();
            $('.dynamic-image').val(media_attachment.url);
        });
        // Opens the media library frame.
        meta_image_frame.open();
    });
});
</script>





<div class="ag_hospitality_widget_wrapper">


    <p>
    <div class="flex agilysys-hospitality-solution-widget">
        <div class="ag-hosp-widget-title-lable">
            <label
                for="<?php echo esc_attr($this->get_field_id('hospitality_title')); ?>"><?php esc_attr_e('Title', 'agilysys');?>:</label>
        </div>

        <div class="ag-hosp-widget-title-input-text">
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('hospitality_title')); ?>"
                name="<?php echo esc_attr($this->get_field_name('hospitality_title')); ?>" type="text"
                value="<?php echo esc_attr($hospitality_title); ?>">
        </div>

    </div>
    </p>



    <div class="tabs">
        <div class="tabSection">
            <a class="tabHeader">
                <div class="">Guest Section</div>
            </a>
            <div class="tabContent">

                <div class="row">
                    <div class="col-md-6">

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('guest_title')); ?>"><?php esc_attr_e('Guest Name', 'agilysys');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('guest_title')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('guest_title')); ?>" type="text"
                                    value="<?php echo esc_attr($guest_title); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo $this->get_field_id('link_type_booking_url'); ?>"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?></label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <select id="<?php echo $this->get_field_id('link_type_booking_url'); ?>"
                                    name="<?php echo $this->get_field_name('link_type_booking_url'); ?>"
                                    onChange="show_hide_div_booking_url(this.value);">
                                    <option value="">Please Select</option>

                                    <?php
$link_type = $instance['link_type_booking_url'];

        if ($link_type == 'page') {
            ?>
                                    <option value="page" selected="selected">Internal Page Link</option>
                                    <?php
} else {
            ?>
                                    <option value="page">Internal Page Link</option>

                                    <?php
}

        if ($link_type == 'link') {
            ?>
                                    <option value="link" selected="selected">External Link</option>
                                    <?php

        } else {
            ?>
                                    <option value="link">External Link</option>
                                    <?php
}

        ?>
                                </select>
                            </div>

                        </div>
                        </p>

                        <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block !important;"';
            $show2 = 'style="display:none !important;"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:block !important;"';

        } else {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:none !important;"';
        }
        ?>

                        <div <?php echo $show1; ?> id="page_div_booking_url">
                            <div class="flex agilysys-hospitality-solution-widget">

                                <div class="ag-hosp-widget-title-lable">
                                    <label for="<?php echo $this->get_field_id('page_booking_url'); ?>">
                                        <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>
                                </div>
                                <div class="ag-hosp-widget-title-input-text">
                                    <select id="<?php echo $this->get_field_id('page_booking_url'); ?>"
                                        name="<?php echo $this->get_field_name('page_booking_url'); ?>">
                                        <option value="">Please Select</option>


                                        <?php

        $page = $display_instance['page'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                ?>
                                        <option value="<?php echo $key->ID; ?>" selected="selected">
                                            <?php echo $key->post_title; ?></option>

                                        <?php
} else {
                ?>
                                        <option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>

                                        <?php
}
            ?>

                                        <?php
}
        ?>

                                    </select>
                                </div>

                            </div>
                        </div>

                        <script>
                        function show_hide_div_booking_url(val) {

                            if (val == 'page') {
                                jQuery("#page_div_booking_url").show();
                                jQuery("#link_div_booking_url").hide();
                            } else if (val == 'link') {
                                jQuery("#page_div_booking_url").hide();
                                jQuery("#link_div_booking_url").show();
                            }

                        }
                        </script>





                        <div <?php echo $show2; ?> id="link_div_booking_url">
                            <div class="flex agilysys-hospitality-solution-widget">
                                <div class="ag-hosp-widget-title-lable">
                                    <label
                                        for="<?php echo esc_attr($this->get_field_id('booking_url')); ?>"><?php esc_attr_e('Booking icon Url', 'ag_hospitality');?>:</label>
                                </div>

                                <div class="ag-hosp-widget-title-input-text">
                                    <input class="widefat"
                                        id="<?php echo esc_attr($this->get_field_id('booking_url')); ?>"
                                        name="<?php echo esc_attr($this->get_field_name('booking_url')); ?>" type="text"
                                        value="<?php echo esc_attr($booking_url); ?>">
                                </div>

                            </div>
                        </div>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('booking_url_title')); ?>"><?php esc_attr_e('Button Title', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('booking_url_title')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('booking_url_title')); ?>"
                                    type="text" value="<?php echo esc_attr($booking_url_title); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('booking_icon_src')); ?>"><?php esc_attr_e('Booking Icon Image', 'ag_hospitality');?>:</label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('booking_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('booking_icon_src')); ?>"
                                    type="text" value="<?php echo esc_attr($src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>


                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('booking_icon_src_alt')); ?>"><?php esc_attr_e('Booking Icon Image alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('booking_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('booking_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($booking_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('booking_hover_icon_src')); ?>"><?php esc_attr_e('Booking Hover Icon Image', 'ag_hospitality');?>:</label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('booking_hover_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('booking_hover_icon_src')); ?>"
                                    type="text" value="<?php echo esc_attr($hoversrc); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('booking_hover_icon_src_alt')); ?>"><?php esc_attr_e('Booking Icon Hover Image alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('booking_hover_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('booking_hover_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($booking_hover_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>

                    </div>
                    <div class="col-md-6">

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo $this->get_field_id('link_type_mobile_url'); ?>"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?></label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <select id="<?php echo $this->get_field_id('link_type_mobile_url'); ?>"
                                    name="<?php echo $this->get_field_name('link_type_mobile_url'); ?>"
                                    onChange="show_hide_div_mobile_url(this.value);">
                                    <option value="">Please Select</option>

                                    <?php
$link_type = $instance['link_type_mobile_url'];

        if ($link_type == 'page') {
            ?>
                                    <option value="page" selected="selected">Internal Page Link</option>
                                    <?php
} else {
            ?>
                                    <option value="page">Internal Page Link</option>

                                    <?php
}

        if ($link_type == 'link') {
            ?>
                                    <option value="link" selected="selected">External Link</option>
                                    <?php

        } else {
            ?>
                                    <option value="link">External Link</option>
                                    <?php
}

        ?>
                                </select>
                            </div>
                        </div>
                        </p>

                        <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block !important;"';
            $show2 = 'style="display:none !important;"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:block !important;"';

        } else {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:none !important;"';
        }
        ?>
                        <div <?php echo $show1; ?> id="page_div_mobile_url">
                            <div class="flex agilysys-hospitality-solution-widget">

                                <div class="ag-hosp-widget-title-lable">
                                    <label for="<?php echo $this->get_field_id('page_mobile_url'); ?>">
                                        <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>
                                </div>

                                <div class="ag-hosp-widget-title-input-text">
                                    <select id="<?php echo $this->get_field_id('page_mobile_url'); ?>"
                                        name="<?php echo $this->get_field_name('page_mobile_url'); ?>">
                                        <option value="">Please Select</option>


                                        <?php

        $page = $display_instance['page'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                ?>
                                        <option value="<?php echo $key->ID; ?>" selected="selected">
                                            <?php echo $key->post_title; ?></option>

                                        <?php
} else {
                ?>
                                        <option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>

                                        <?php
}
            ?>

                                        <?php
}
        ?>

                                    </select>
                                </div>
                            </div>
                        </div>


                        <script>
                        function show_hide_div_mobile_url(val) {

                            if (val == 'page') {
                                jQuery("#page_div_mobile_url").show();
                                jQuery("#link_div_mobile_url").hide();
                            } else if (val == 'link') {
                                jQuery("#page_div_mobile_url").hide();
                                jQuery("#link_div_mobile_url").show();
                            }

                        }
                        </script>



                        <div <?php echo $show2; ?> id="link_div_mobile_url">
                            <div class="flex agilysys-hospitality-solution-widget">
                                <div class="ag-hosp-widget-title-lable">
                                    <label
                                        for="<?php echo esc_attr($this->get_field_id('mobile_url')); ?>"><?php esc_attr_e('Mobile icon Url', 'ag_hospitality');?>:</label>
                                </div>

                                <div class="ag-hosp-widget-title-input-text">
                                    <input class="widefat"
                                        id="<?php echo esc_attr($this->get_field_id('mobile_url')); ?>"
                                        name="<?php echo esc_attr($this->get_field_name('mobile_url')); ?>" type="text"
                                        value="<?php echo esc_attr($mobile_url); ?>">
                                </div>

                            </div>
                        </div>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('mobile_url_title')); ?>"><?php esc_attr_e('Button Title', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('mobile_url_title')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('mobile_url_title')); ?>"
                                    type="text" value="<?php echo esc_attr($mobile_url_title); ?>">
                            </div>

                        </div>
                        </p>


                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('mobile_icon_src')); ?>"><?php esc_attr_e('Mobile Icon Image', 'ag_hospitality');?>:</label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('mobile_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('mobile_icon_src')); ?>" type="text"
                                    value="<?php echo esc_attr($mobile_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>


                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('mobile_icon_src_alt')); ?>"><?php esc_attr_e('Mobile Icon Image alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('mobile_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('mobile_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($mobile_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('mobile_hover_icon_src')); ?>"><?php esc_attr_e('Mobile Hover Icon Image', 'ag_hospitality');?>:</label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('mobile_hover_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('mobile_hover_icon_src')); ?>"
                                    type="text" value="<?php echo esc_attr($mobile_hover_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>


                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('mobile_hover_icon_src')); ?>"><?php esc_attr_e('Mobile Icon Hover Image alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('mobile_hover_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('mobile_hover_icon_src')); ?>"
                                    type="text" value="<?php echo esc_attr($mobile_hover_icon_src); ?>">
                            </div>

                        </div>
                        </p>

                    </div>
                </div>

                <div style="clear:both;"></div>
                <div class="row">
                    <div class="col-md-6">
                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo $this->get_field_id('link_type_golf_url'); ?>"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?></label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <select id="<?php echo $this->get_field_id('link_type_golf_url'); ?>"
                                    name="<?php echo $this->get_field_name('link_type_golf_url'); ?>"
                                    onChange="show_hide_div_golf_url(this.value);">
                                    <option value="">Please Select</option>

                                    <?php
$link_type = $instance['link_type_golf_url'];

        if ($link_type == 'page') {
            ?>
                                    <option value="page" selected="selected">Internal Page Link</option>
                                    <?php
} else {
            ?>
                                    <option value="page">Internal Page Link</option>

                                    <?php
}

        if ($link_type == 'link') {
            ?>
                                    <option value="link" selected="selected">External Link</option>
                                    <?php

        } else {
            ?>
                                    <option value="link">External Link</option>
                                    <?php
}

        ?>
                                </select>
                            </div>

                        </div>
                        </p>
                        <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:inline-block !important;"';
            $show2 = 'style="display:none !important;"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:block !important;"';

        } else {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:none !important;"';
        }
        ?>



                        <div <?php echo $show1; ?> id="page_div_golf_url">
                            <div class="flex agilysys-hospitality-solution-widget">

                                <div class="ag-hosp-widget-title-lable">
                                    <label for="<?php echo $this->get_field_id('page_golf_url'); ?>">
                                        <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>
                                </div>
                                <div class="ag-hosp-widget-title-input-text">
                                    <select id="<?php echo $this->get_field_id('page_golf_url'); ?>"
                                        name="<?php echo $this->get_field_name('page_golf_url'); ?>">
                                        <option value="">Please Select</option>


                                        <?php

        $page = $display_instance['page_golf_url'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                ?>
                                        <option value="<?php echo $key->ID; ?>" selected="selected">
                                            <?php echo $key->post_title; ?></option>

                                        <?php
} else {
                ?>
                                        <option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>

                                        <?php
}
            ?>

                                        <?php
}
        ?>

                                    </select>
                                </div>

                            </div>
                        </div>

                        <script>
                        function show_hide_div_golf_url(val) {

                            if (val == 'page') {
                                jQuery("#page_div_golf_url").show();
                                jQuery("#link_div_golf_url").hide();
                            } else if (val == 'link') {
                                jQuery("#page_div_golf_url").hide();
                                jQuery("#link_div_golf_url").show();
                            }

                        }
                        </script>




                        <!--Golf-->
                        <div <?php echo $show2; ?> id="link_div_golf_url">
                            <div class="flex agilysys-hospitality-solution-widget">
                                <div class="ag-hosp-widget-title-lable">
                                    <label
                                        for="<?php echo esc_attr($this->get_field_id('golf_url')); ?>"><?php esc_attr_e('Golf icon Url', 'ag_hospitality');?>:</label>
                                </div>

                                <div class="ag-hosp-widget-title-input-text">
                                    <input class="widefat" id="<?php echo esc_attr($this->get_field_id('golf_url')); ?>"
                                        name="<?php echo esc_attr($this->get_field_name('golf_url')); ?>" type="text"
                                        value="<?php echo esc_attr($golf_url); ?>">
                                </div>

                            </div>
                        </div>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('golf_url_title')); ?>"><?php esc_attr_e('Button Title', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('golf_url_title')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('golf_url_title')); ?>" type="text"
                                    value="<?php echo esc_attr($golf_url_title); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class=" ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('golf_icon_src')); ?>"><?php esc_attr_e('Golf Icon Image', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('golf_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('golf_icon_src')); ?>" type="text"
                                    value="<?php echo esc_attr($mobile_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>


                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('golf_icon_src_alt')); ?>"><?php esc_attr_e('Golf Icon Image alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('golf_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('golf_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($golf_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class=" ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('golf_hover_icon_src')); ?>"><?php esc_attr_e('Golf Hover Icon Image', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('golf_hover_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('golf_hover_icon_src')); ?>"
                                    type="text" value="<?php echo esc_attr($golf_hover_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>
                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('golf_hover_icon_src_alt')); ?>"><?php esc_attr_e('Golf Icon Hover Image alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('golf_hover_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('golf_hover_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($golf_hover_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>
                    </div>
                    <div class="col-md-6">
                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo $this->get_field_id('link_type_digital_url'); ?>"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?></label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <select id="<?php echo $this->get_field_id('link_type_digital_url'); ?>"
                                    name="<?php echo $this->get_field_name('link_type_digital_url'); ?>"
                                    onChange="show_hide_div_digital_url(this.value);">
                                    <option value="">Please Select</option>

                                    <?php
$link_type = $instance['link_type_digital_url'];

        if ($link_type == 'page') {
            ?>
                                    <option value="page" selected="selected">Internal Page Link</option>
                                    <?php
} else {
            ?>
                                    <option value="page">Internal Page Link</option>

                                    <?php
}

        if ($link_type == 'link') {
            ?>
                                    <option value="link" selected="selected">External Link</option>
                                    <?php

        } else {
            ?>
                                    <option value="link">External Link</option>
                                    <?php
}

        ?>
                                </select>
                            </div>

                        </div>
                        </p><br>

                        <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block !important;"';
            $show2 = 'style="display:none !important;"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:block !important;"';

        } else {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:none !important;"';
        }
        ?>
                        <div <?php echo $show1; ?> id="page_div_digital_url">
                            <div class="flex agilysys-hospitality-solution-widget">

                                <div class="ag-hosp-widget-title-lable">
                                    <label for="<?php echo $this->get_field_id('page_digital_url'); ?>">
                                        <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>
                                </div>
                                <div class="ag-hosp-widget-title-input-text">
                                    <select id="<?php echo $this->get_field_id('page_digital_url'); ?>"
                                        name="<?php echo $this->get_field_name('page_digital_url'); ?>">
                                        <option value="">Please Select</option>


                                        <?php

        $page = $display_instance['page_digital_url'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                ?>
                                        <option value="<?php echo $key->ID; ?>" selected="selected">
                                            <?php echo $key->post_title; ?></option>

                                        <?php
} else {
                ?>
                                        <option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>

                                        <?php
}
            ?>

                                        <?php
}
        ?>

                                    </select>
                                </div>

                            </div>
                        </div>


                        <script>
                        function show_hide_div_digital_url(val) {

                            if (val == 'page') {
                                jQuery("#page_div_digital_url").show();
                                jQuery("#link_div_digital_url").hide();
                            } else if (val == 'link') {
                                jQuery("#page_div_digital_url").hide();
                                jQuery("#link_div_digital_url").show();
                            }

                        }
                        </script>

                        <!--Digital-->
                        <div <?php echo $show2; ?> id="link_div_digital_url">
                            <div class="flex agilysys-hospitality-solution-widget">
                                <div class="ag-hosp-widget-title-lable">
                                    <label
                                        for="<?php echo esc_attr($this->get_field_id('digital_url')); ?>"><?php esc_attr_e('Digital icon Url', 'ag_hospitality');?>:</label>
                                </div>

                                <div class="ag-hosp-widget-title-input-text">
                                    <input class="widefat"
                                        id="<?php echo esc_attr($this->get_field_id('digital_url')); ?>"
                                        name="<?php echo esc_attr($this->get_field_name('digital_url')); ?>" type="text"
                                        value="<?php echo esc_attr($digital_url); ?>">
                                </div>

                            </div>
                        </div>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('digital_url_title')); ?>"><?php esc_attr_e('Button Title', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('digital_url_title')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('digital_url_title')); ?>"
                                    type="text" value="<?php echo esc_attr($digital_url_title); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('digital_icon_src')); ?>"><?php esc_attr_e('Digital Icon Image', 'ag_hospitality');?>:</label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('digital_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('digital_icon_src')); ?>"
                                    type="text" value="<?php echo esc_attr($digital_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>

                        </p>



                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('digital_icon_src_alt')); ?>"><?php esc_attr_e('Digital Icon Image alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('digital_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('digital_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($digital_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('digital_hover_icon_src')); ?>"><?php esc_attr_e('Digital Hover Icon Image', 'ag_hospitality');?>:</label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('digital_hover_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('digital_hover_icon_src')); ?>"
                                    type="text" value="<?php echo esc_attr($digital_hover_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>

                        </p>



                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('digital_hover_icon_src_alt')); ?>"><?php esc_attr_e('Digital Icon Hover Image alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('digital_hover_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('digital_hover_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($digital_hover_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>

                    </div>

                </div>




                <div style="clear:both;"></div>

                <div class="row">
                    <div class="col-md-6">

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo $this->get_field_id('link_type_dining_url'); ?>"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?></label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <select id="<?php echo $this->get_field_id('link_type_dining_url'); ?>"
                                    name="<?php echo $this->get_field_name('link_type_dining_url'); ?>"
                                    onChange="show_hide_div_dining_url(this.value);">
                                    <option value="">Please Select</option>

                                    <?php
$link_type = $instance['link_type_dining_url'];

        if ($link_type == 'page') {
            ?>
                                    <option value="page" selected="selected">Internal Page Link</option>
                                    <?php
} else {
            ?>
                                    <option value="page">Internal Page Link</option>

                                    <?php
}

        if ($link_type == 'link') {
            ?>
                                    <option value="link" selected="selected">External Link</option>
                                    <?php

        } else {
            ?>
                                    <option value="link">External Link</option>
                                    <?php
}

        ?>
                                </select>
                            </div>

                        </div>
                        </p>

                        <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block !important;"';
            $show2 = 'style="display:none !important;"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:block !important;"';

        } else {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:none !important;"';
        }
        ?>
                        <div <?php echo $show1; ?> id="page_div_dining_url">
                            <div class="flex agilysys-hospitality-solution-widget">

                                <div class="ag-hosp-widget-title-lable">
                                    <label for="<?php echo $this->get_field_id('page_dining_url'); ?>">
                                        <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>
                                </div>
                                <div class="ag-hosp-widget-title-input-text">
                                    <select id="<?php echo $this->get_field_id('page_dining_url'); ?>"
                                        name="<?php echo $this->get_field_name('page_dining_url'); ?>">
                                        <option value="">Please Select</option>


                                        <?php

        $page = $display_instance['page_dining_url'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                ?>
                                        <option value="<?php echo $key->ID; ?>" selected="selected">
                                            <?php echo $key->post_title; ?></option>

                                        <?php
} else {
                ?>
                                        <option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>

                                        <?php
}
            ?>

                                        <?php
}
        ?>

                                    </select>
                                </div>

                            </div>
                        </div>


                        <script>
                        function show_hide_div_dining_url(val) {

                            if (val == 'page') {
                                jQuery("#page_div_dining_url").show();
                                jQuery("#link_div_dining_url").hide();
                            } else if (val == 'link') {
                                jQuery("#page_div_dining_url").hide();
                                jQuery("#link_div_dining_url").show();
                            }

                        }
                        </script>


                        <!--Dining-->
                        <div <?php echo $show2; ?> id="link_div_dining_url">
                            <div class="flex agilysys-hospitality-solution-widget">
                                <div class="ag-hosp-widget-title-lable">
                                    <label
                                        for="<?php echo esc_attr($this->get_field_id('dining_url')); ?>"><?php esc_attr_e('Dining icon Url', 'ag_hospitality');?>:</label>
                                </div>

                                <div class="ag-hosp-widget-title-input-text">
                                    <input class="widefat"
                                        id="<?php echo esc_attr($this->get_field_id('dining_url')); ?>"
                                        name="<?php echo esc_attr($this->get_field_name('dining_url')); ?>" type="text"
                                        value="<?php echo esc_attr($dining_url); ?>">
                                </div>

                            </div>
                        </div>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('dining_url_title')); ?>"><?php esc_attr_e('Button Title', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('dining_url_title')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('dining_url_title')); ?>"
                                    type="text" value="<?php echo esc_attr($dining_url_title); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('dining_icon_src')); ?>"><?php esc_attr_e('Dining Icon Image', 'ag_hospitality');?>:</label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('digital_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('dining_icon_src')); ?>" type="text"
                                    value="<?php echo esc_attr($dining_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>

                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('dining_icon_src_alt')); ?>"><?php esc_attr_e('Dining Icon Image Alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('dining_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('dining_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($dining_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>
                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('dining_hover_icon_src')); ?>"><?php esc_attr_e('Dining Hover Icon Image', 'ag_hospitality');?>:</label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('dining_hover_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('dining_hover_icon_src')); ?>"
                                    type="text" value="<?php echo esc_attr($dining_hover_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>

                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('dining_hover_icon_src_alt')); ?>"><?php esc_attr_e('Dining Hover Icon Image Alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('dining_hover_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('dining_hover_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($dining_hover_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>
                    </div>

                    <div class="col-md-6">
                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo $this->get_field_id('link_type_payment_url'); ?>"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?></label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <select id="<?php echo $this->get_field_id('link_type_payment_url'); ?>"
                                    name="<?php echo $this->get_field_name('link_type_payment_url'); ?>"
                                    onChange="show_hide_div_payment_url(this.value);">
                                    <option value="">Please Select</option>

                                    <?php
$link_type = $instance['link_type_payment_url'];

        if ($link_type == 'page') {
            ?>
                                    <option value="page" selected="selected">Internal Page Link</option>
                                    <?php
} else {
            ?>
                                    <option value="page">Internal Page Link</option>

                                    <?php
}

        if ($link_type == 'link') {
            ?>
                                    <option value="link" selected="selected">External Link</option>
                                    <?php

        } else {
            ?>
                                    <option value="link">External Link</option>
                                    <?php
}

        ?>
                                </select>
                            </div>

                        </div>
                        </p>

                        <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block !important;"';
            $show2 = 'style="display:none !important;"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:block !important;"';

        } else {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:none !important;"';
        }
        ?>
                        <div <?php echo $show1; ?> id="page_div_payment_url">
                            <div class="flex agilysys-hospitality-solution-widget">

                                <div class="ag-hosp-widget-title-lable">
                                    <label for="<?php echo $this->get_field_id('page_payment_url'); ?>">
                                        <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>
                                </div>
                                <div class="ag-hosp-widget-title-input-text">
                                    <select id="<?php echo $this->get_field_id('page_payment_url'); ?>"
                                        name="<?php echo $this->get_field_name('page_payment_url'); ?>">
                                        <option value="">Please Select</option>


                                        <?php

        $page = $display_instance['page_payment_url'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                ?>
                                        <option value="<?php echo $key->ID; ?>" selected="selected">
                                            <?php echo $key->post_title; ?></option>

                                        <?php
} else {
                ?>
                                        <option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>

                                        <?php
}
            ?>

                                        <?php
}
        ?>

                                    </select>
                                </div>

                            </div>
                        </div>


                        <script>
                        function show_hide_div_payment_url(val) {

                            if (val == 'page') {
                                jQuery("#page_div_payment_url").show();
                                jQuery("#link_div_payment_url").hide();
                            } else if (val == 'link') {
                                jQuery("#page_div_payment_url").hide();
                                jQuery("#link_div_payment_url").show();
                            }

                        }
                        </script>


                        <!--payment -->
                        <div <?php echo $show2; ?> id="link_div_payment_url">
                            <div class="flex agilysys-hospitality-solution-widget">
                                <div class="ag-hosp-widget-title-lable">
                                    <label
                                        for="<?php echo esc_attr($this->get_field_id('payment_url')); ?>"><?php esc_attr_e('Payment icon Url', 'ag_hospitality');?>:</label>
                                </div>

                                <div class="ag-hosp-widget-title-input-text">
                                    <input class="widefat"
                                        id="<?php echo esc_attr($this->get_field_id('payment_url')); ?>"
                                        name="<?php echo esc_attr($this->get_field_name('payment_url')); ?>" type="text"
                                        value="<?php echo esc_attr($payment_url); ?>">
                                </div>

                            </div>
                        </div>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('payment_url_title')); ?>"><?php esc_attr_e('Button Title', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('payment_url_title')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('payment_url_title')); ?>"
                                    type="text" value="<?php echo esc_attr($payment_url_title); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('payment_icon_src')); ?>"><?php esc_attr_e('Payment Icon Image', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('payment_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('payment_icon_src')); ?>"
                                    type="text" value="<?php echo esc_attr($payment_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>
                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('payment_icon_src_alt')); ?>"><?php esc_attr_e('Payment Icon Image Alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('payment_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('payment_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($payment_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('payment_hover_icon_src')); ?>"><?php esc_attr_e('Payment Hover Icon Image', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('payment_hover_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('payment_hover_icon_src')); ?>"
                                    type="text" value="<?php echo esc_attr($payment_hover_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('payment_hover_icon_src_alt')); ?>"><?php esc_attr_e('Payment Hover Icon Image Alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('payment_hover_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('payment_hover_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($payment_hover_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>

                    </div>
                </div>


                <div style="clear:both;"></div>


                <div class="row">

                    <div class="col-md-6">

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo $this->get_field_id('link_type_spa_url'); ?>"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?></label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <select id="<?php echo $this->get_field_id('link_type_spa_url'); ?>"
                                    name="<?php echo $this->get_field_name('link_type_spa_url'); ?>"
                                    onChange="show_hide_div_spa_url(this.value);">
                                    <option value="">Please Select</option>

                                    <?php
$link_type = $instance['link_type_spa_url'];

        if ($link_type == 'page') {
            ?>
                                    <option value="page" selected="selected">Internal Page Link</option>
                                    <?php
} else {
            ?>
                                    <option value="page">Internal Page Link</option>

                                    <?php
}

        if ($link_type == 'link') {
            ?>
                                    <option value="link" selected="selected">External Link</option>
                                    <?php

        } else {
            ?>
                                    <option value="link">External Link</option>
                                    <?php
}

        ?>
                                </select>
                            </div>

                        </div>
                        </p>

                        <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block !important;"';
            $show2 = 'style="display:none !important;"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:block !important;"';

        } else {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:none !important;"';
        }
        ?>
                        <div <?php echo $show1; ?> id="page_div_spa_url">
                            <div class="flex agilysys-hospitality-solution-widget">

                                <div class="ag-hosp-widget-title-lable">
                                    <label for="<?php echo $this->get_field_id('page_spa_url'); ?>">
                                        <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>
                                </div>
                                <div class="ag-hosp-widget-title-input-text">
                                    <select id="<?php echo $this->get_field_id('page_spa_url'); ?>"
                                        name="<?php echo $this->get_field_name('page_spa_url'); ?>">
                                        <option value="">Please Select</option>


                                        <?php

        $page = $display_instance['page_spa_url'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                ?>
                                        <option value="<?php echo $key->ID; ?>" selected="selected">
                                            <?php echo $key->post_title; ?></option>

                                        <?php
} else {
                ?>
                                        <option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>

                                        <?php
}
            ?>

                                        <?php
}
        ?>

                                    </select>
                                </div>

                            </div>
                        </div>


                        <script>
                        function show_hide_div_spa_url(val) {

                            if (val == 'page') {
                                jQuery("#page_div_spa_url").show();
                                jQuery("#link_div_spa_url").hide();
                            } else if (val == 'link') {
                                jQuery("#page_div_spa_url").hide();
                                jQuery("#link_div_spa_url").show();
                            }

                        }
                        </script>


                        <!--spa -->
                        <div <?php echo $show2; ?> id="link_div_spa_url">
                            <div class="flex agilysys-hospitality-solution-widget">
                                <div class="ag-hosp-widget-title-lable">
                                    <label
                                        for="<?php echo esc_attr($this->get_field_id('spa_url')); ?>"><?php esc_attr_e('Spa icon Url', 'ag_hospitality');?>:</label>
                                </div>

                                <div class="ag-hosp-widget-title-input-text">
                                    <input class="widefat" id="<?php echo esc_attr($this->get_field_id('spa_url')); ?>"
                                        name="<?php echo esc_attr($this->get_field_name('spa_url')); ?>" type="text"
                                        value="<?php echo esc_attr($spa_url); ?>">
                                </div>

                            </div>
                        </div>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('spa_url_title')); ?>"><?php esc_attr_e('Button Title', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('spa_url_title')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('spa_url_title')); ?>" type="text"
                                    value="<?php echo esc_attr($spa_url_title); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('spa_icon_src')); ?>"><?php esc_attr_e('Spa Icon Image', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('spa_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('spa_icon_src')); ?>" type="text"
                                    value="<?php echo esc_attr($spa_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>


                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('spa_icon_src_alt')); ?>"><?php esc_attr_e('Spa Icon Image Alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('spa_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('spa_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($spa_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>


                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('spa_hover_icon_src')); ?>"><?php esc_attr_e('Spa Hover Icon Image', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('spa_hover_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('spa_hover_icon_src')); ?>"
                                    type="text" value="<?php echo esc_attr($spa_hover_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>


                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('spa_hover_icon_src_alt')); ?>"><?php esc_attr_e('Spa Hover Icon Image Alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('spa_hover_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('spa_hover_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($spa_hover_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>
                    </div>

                    <div class="col-md-6">

                        <!--Venue Reservation-->

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo $this->get_field_id('link_type_venue_url'); ?>"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?></label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <select id="<?php echo $this->get_field_id('link_type_venue_url'); ?>"
                                    name="<?php echo $this->get_field_name('link_type_venue_url'); ?>"
                                    onChange="show_hide_div_venue_url(this.value);">
                                    <option value="">Please Select</option>

                                    <?php
$link_type = $instance['link_type_venue_url'];

        if ($link_type == 'page') {
            ?>
                                    <option value="page" selected="selected">Internal Page Link</option>
                                    <?php
} else {
            ?>
                                    <option value="page">Internal Page Link</option>

                                    <?php
}

        if ($link_type == 'link') {
            ?>
                                    <option value="link" selected="selected">External Link</option>
                                    <?php

        } else {
            ?>
                                    <option value="link">External Link</option>
                                    <?php
}

        ?>
                                </select>
                            </div>

                        </div>
                        </p><br>

                        <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block !important;"';
            $show2 = 'style="display:none !important;"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:block !important;"';

        } else {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:none !important;"';
        }
        ?>
                        <div <?php echo $show1; ?> id="page_div_venue_url">
                            <div class="flex agilysys-hospitality-solution-widget">

                                <div class="ag-hosp-widget-title-lable">
                                    <label for="<?php echo $this->get_field_id('page_venue_url'); ?>">
                                        <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>
                                </div>
                                <div class="ag-hosp-widget-title-input-text">
                                    <select id="<?php echo $this->get_field_id('page_venue_url'); ?>"
                                        name="<?php echo $this->get_field_name('page_venue_url'); ?>">
                                        <option value="">Please Select</option>


                                        <?php

        $page = $display_instance['page_venue_url'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                ?>
                                        <option value="<?php echo $key->ID; ?>" selected="selected">
                                            <?php echo $key->post_title; ?></option>

                                        <?php
} else {
                ?>
                                        <option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>

                                        <?php
}
            ?>

                                        <?php
}
        ?>

                                    </select>
                                </div>

                            </div>
                        </div>


                        <script>
                        function show_hide_div_venue_url(val) {

                            if (val == 'page') {
                                jQuery("#page_div_venue_url").show();
                                jQuery("#link_div_venue_url").hide();
                            } else if (val == 'link') {
                                jQuery("#page_div_venue_url").hide();
                                jQuery("#link_div_venue_url").show();
                            }

                        }
                        </script>

                        <div <?php echo $show2; ?> id="link_div_venue_url">
                            <div class="flex agilysys-hospitality-solution-widget">
                                <div class="ag-hosp-widget-title-lable">
                                    <label
                                        for="<?php echo esc_attr($this->get_field_id('venue_url')); ?>"><?php esc_attr_e('Venue icon Url', 'ag_hospitality');?>:</label>
                                </div>

                                <div class="ag-hosp-widget-title-input-text">
                                    <input class="widefat"
                                        id="<?php echo esc_attr($this->get_field_id('venue_url')); ?>"
                                        name="<?php echo esc_attr($this->get_field_name('venue_url')); ?>" type="text"
                                        value="<?php echo esc_attr($venue_url); ?>">
                                </div>

                            </div>
                        </div>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('venue_url_title')); ?>"><?php esc_attr_e('Button Title', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('venue_url_title')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('venue_url_title')); ?>" type="text"
                                    value="<?php echo esc_attr($venue_url_title); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('venue_icon_src')); ?>"><?php esc_attr_e('Venue Icon Image', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('venue_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('venue_icon_src')); ?>" type="text"
                                    value="<?php echo esc_attr($venue_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('venue_icon_src_alt')); ?>"><?php esc_attr_e('Venue Icon Image Alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('venue_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('venue_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($venue_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>


                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('venue_hover_icon_src')); ?>"><?php esc_attr_e('Venue Hover Icon Image', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('venue_hover_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('venue_hover_icon_src')); ?>"
                                    type="text" value="<?php echo esc_attr($venue_hover_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>


                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('venue_hover_icon_src_alt')); ?>"><?php esc_attr_e('Venue Hover Icon Image Alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('venue_hover_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('venue_hover_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($venue_hover_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>

                    </div>

                </div>

                <div style="clear:both;"></div>
                <div class="row">

                    <div class="col-md-6">
                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo $this->get_field_id('link_type_mobile_check_url'); ?>"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?></label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <select id="<?php echo $this->get_field_id('link_type_mobile_check_url'); ?>"
                                    name="<?php echo $this->get_field_name('link_type_mobile_check_url'); ?>"
                                    onChange="show_hide_div_mobile_check_url(this.value);">
                                    <option value="">Please Select</option>

                                    <?php
$link_type = $instance['link_type_mobile_check_url'];

        if ($link_type == 'page') {
            ?>
                                    <option value="page" selected="selected">Internal Page Link</option>
                                    <?php
} else {
            ?>
                                    <option value="page">Internal Page Link</option>

                                    <?php
}

        if ($link_type == 'link') {
            ?>
                                    <option value="link" selected="selected">External Link</option>
                                    <?php

        } else {
            ?>
                                    <option value="link">External Link</option>
                                    <?php
}

        ?>
                                </select>
                            </div>

                        </div>
                        </p>

                        <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block !important;"';
            $show2 = 'style="display:none !important;"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:block !important;"';

        } else {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:none !important;"';
        }
        ?>
                        <div <?php echo $show1; ?> id="page_div_mobile_check_url">
                            <div class="flex agilysys-hospitality-solution-widget">

                                <div class="ag-hosp-widget-title-lable">
                                    <label for="<?php echo $this->get_field_id('page_mobile_check_url'); ?>">
                                        <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>
                                </div>
                                <div class="ag-hosp-widget-title-input-text">
                                    <select id="<?php echo $this->get_field_id('page_mobile_check_url'); ?>"
                                        name="<?php echo $this->get_field_name('page_mobile_check_url'); ?>">
                                        <option value="">Please Select</option>


                                        <?php

        $page = $display_instance['page_mobile_check_url'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                ?>
                                        <option value="<?php echo $key->ID; ?>" selected="selected">
                                            <?php echo $key->post_title; ?></option>

                                        <?php
} else {
                ?>
                                        <option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>

                                        <?php
}
            ?>

                                        <?php
}
        ?>

                                    </select>
                                </div>

                            </div>
                        </div>


                        <script>
                        function show_hide_div_mobile_check_url(val) {

                            if (val == 'page') {
                                jQuery("#page_div_mobile_check_url").show();
                                jQuery("#link_div_mobile_check_url").hide();
                            } else if (val == 'link') {
                                jQuery("#page_div_mobile_check_url").hide();
                                jQuery("#link_div_mobile_check_url").show();
                            }

                        }
                        </script>

                        <div <?php echo $show2; ?> id="link_div_mobile_check_url">
                            <div class="flex agilysys-hospitality-solution-widget">
                                <div class="ag-hosp-widget-title-lable">
                                    <label
                                        for="<?php echo esc_attr($this->get_field_id('mobile_check_url')); ?>"><?php esc_attr_e('Mobile checkout icon Url', 'ag_hospitality');?>:</label>
                                </div>

                                <div class="ag-hosp-widget-title-input-text">
                                    <input class="widefat"
                                        id="<?php echo esc_attr($this->get_field_id('mobile_check_url')); ?>"
                                        name="<?php echo esc_attr($this->get_field_name('mobile_check_url')); ?>"
                                        type="text" value="<?php echo esc_attr($mobile_check_url); ?>">
                                </div>

                            </div>
                        </div>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('mobile_check_url_title')); ?>"><?php esc_attr_e('Button Title', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('mobile_check_url_title')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('mobile_check_url_title')); ?>"
                                    type="text" value="<?php echo esc_attr($mobile_check_url_title); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('mobile_check_icon_src')); ?>"><?php esc_attr_e('Mobile checkout Icon Image', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('mobile_check_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('mobile_check_icon_src')); ?>"
                                    type="text" value="<?php echo esc_attr($mobile_check_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('mobile_check_icon_src_alt')); ?>"><?php esc_attr_e('Mobile checkout Icon Image Alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('mobile_check_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('mobile_check_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($mobile_check_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>


                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('mobile_check_hover_icon_src')); ?>"><?php esc_attr_e('Mobile checkout Hover Icon Image', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('mobile_check_hover_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('mobile_check_hover_icon_src')); ?>"
                                    type="text" value="<?php echo esc_attr($mobile_check_hover_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('mobile_check_hover_icon_src_alt')); ?>"><?php esc_attr_e('Mobile checkout Hover Icon Image Alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('mobile_check_hover_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('mobile_check_hover_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($mobile_check_hover_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>
                    </div>

                    <div class="col-md-6">

                    </div>

                </div>


                <div style="clear:both;"></div>
                <!--Mobile checkout-->





            </div>
        </div>
        <div class="tabSection">
            <a class="tabHeader">
                <div class="">Property Section</div>
            </a>
            <div class="tabContent">

                <div class="row">


                    <div class="col-md-6">
                        <?php

//            property

        ?>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('property_title')); ?>"><?php esc_attr_e('Property Name', 'agilysys');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('property_title')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('property_title')); ?>" type="text"
                                    value="<?php echo esc_attr($property_title); ?>">
                            </div>

                        </div>
                        </p>


                        <!-- 1st row digital marketing -->
                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo $this->get_field_id('link_type_digital_marketing_url'); ?>"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?></label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <select id="<?php echo $this->get_field_id('link_type_digital_marketing_url'); ?>"
                                    name="<?php echo $this->get_field_name('link_type_digital_marketing_url'); ?>"
                                    onChange="show_hide_div_digital_marketing_url(this.value);">
                                    <option value="">Please Select</option>

                                    <?php
$link_type = $instance['link_type_digital_marketing_url'];

        if ($link_type == 'page') {
            ?>
                                    <option value="page" selected="selected">Internal Page Link</option>
                                    <?php
} else {
            ?>
                                    <option value="page">Internal Page Link</option>

                                    <?php
}

        if ($link_type == 'link') {
            ?>
                                    <option value="link" selected="selected">External Link</option>
                                    <?php

        } else {
            ?>
                                    <option value="link">External Link</option>
                                    <?php
}

        ?>
                                </select>
                            </div>

                        </div>
                        </p>

                        <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block !important;"';
            $show2 = 'style="display:none !important;"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:block !important;"';

        } else {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:none !important;"';
        }
        ?>
                        <div <?php echo $show1; ?> id="page_div_digital_marketing_url">
                            <div class="flex agilysys-hospitality-solution-widget">

                                <div class="ag-hosp-widget-title-lable">
                                    <label for="<?php echo $this->get_field_id('page_digital_marketing_url'); ?>">
                                        <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>
                                </div>
                                <div class="ag-hosp-widget-title-input-text">
                                    <select id="<?php echo $this->get_field_id('page_digital_marketing_url'); ?>"
                                        name="<?php echo $this->get_field_name('page_digital_marketing_url'); ?>">
                                        <option value="">Please Select</option>


                                        <?php

        $page = $display_instance['page_digital_marketing_url'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                ?>
                                        <option value="<?php echo $key->ID; ?>" selected="selected">
                                            <?php echo $key->post_title; ?></option>

                                        <?php
} else {
                ?>
                                        <option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>

                                        <?php
}
            ?>

                                        <?php
}
        ?>

                                    </select>
                                </div>

                            </div>
                        </div>


                        <script>
                        function show_hide_div_digital_marketing_url(val) {

                            if (val == 'page') {
                                jQuery("#page_div_digital_marketing_url").show();
                                jQuery("#link_div_digital_marketing_url").hide();
                            } else if (val == 'link') {
                                jQuery("#page_div_digital_marketing_url").hide();
                                jQuery("#link_div_digital_marketing_url").show();
                            }

                        }
                        </script>


                        <div <?php echo $show2; ?> id="link_div_digital_marketing_url">
                            <div class="flex agilysys-hospitality-solution-widget">
                                <div class="ag-hosp-widget-title-lable">
                                    <label
                                        for="<?php echo esc_attr($this->get_field_id('digital_marketing_url')); ?>"><?php esc_attr_e('Digital marketing icon Url', 'ag_hospitality');?>:</label>
                                </div>

                                <div class="ag-hosp-widget-title-input-text">
                                    <input class="widefat"
                                        id="<?php echo esc_attr($this->get_field_id('digital_marketing_url')); ?>"
                                        name="<?php echo esc_attr($this->get_field_name('digital_marketing_url')); ?>"
                                        type="text" value="<?php echo esc_attr($digital_marketing_url); ?>">
                                </div>

                            </div>
                        </div>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('digital_marketing_url_title')); ?>"><?php esc_attr_e('Button Title', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('digital_marketing_url_title')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('digital_marketing_url_title')); ?>"
                                    type="text" value="<?php echo esc_attr($digital_marketing_url_title); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('digital_marketing_src')); ?>"><?php esc_attr_e('Digital Marketing Icon Image', 'ag_hospitality');?>:</label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('digital_marketing_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('digital_marketing_src')); ?>"
                                    type="text" value="<?php echo esc_attr($digital_marketing_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('digital_marketing_src_alt')); ?>"><?php esc_attr_e('Digital Marketing Icon Image Alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('digital_marketing_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('digital_marketing_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($digital_marketing_src_alt); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('digital_marketing_hover_src')); ?>"><?php esc_attr_e('Digital Marketing Hover Icon Image', 'ag_hospitality');?>:</label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('digital_marketing_hover_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('digital_marketing_hover_src')); ?>"
                                    type="text" value="<?php echo esc_attr($digital_marketing_hover_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>



                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('digital_marketing_hover_src_alt')); ?>"><?php esc_attr_e('Digital Marketing Hover Icon Image Alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('digital_marketing_hover_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('digital_marketing_hover_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($digital_marketing_hover_src_alt); ?>">
                            </div>

                        </div>
                        </p>

                    </div>
                    <div class="col-md-6">
                        <!--Booking-->

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo $this->get_field_id('link_type_property_booking_url'); ?>"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?></label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <select id="<?php echo $this->get_field_id('link_type_property_booking_url'); ?>"
                                    name="<?php echo $this->get_field_name('link_type_property_booking_url'); ?>"
                                    onChange="show_hide_div_property_booking_url(this.value);">
                                    <option value="">Please Select</option>

                                    <?php
$link_type = $instance['link_type_property_booking_url'];

        if ($link_type == 'page') {
            ?>
                                    <option value="page" selected="selected">Internal Page Link</option>
                                    <?php
} else {
            ?>
                                    <option value="page">Internal Page Link</option>

                                    <?php
}

        if ($link_type == 'link') {
            ?>
                                    <option value="link" selected="selected">External Link</option>
                                    <?php

        } else {
            ?>
                                    <option value="link">External Link</option>
                                    <?php
}

        ?>
                                </select>
                            </div>

                        </div>
                        </p>

                        <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block !important;"';
            $show2 = 'style="display:none !important;"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:block !important;"';

        } else {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:none !important;"';
        }
        ?>
                        <div <?php echo $show1; ?> id="page_div_property_booking_url">
                            <div class="flex agilysys-hospitality-solution-widget">

                                <div class="ag-hosp-widget-title-lable">
                                    <label for="<?php echo $this->get_field_id('page_property_booking_url'); ?>">
                                        <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>
                                </div>
                                <div class="ag-hosp-widget-title-input-text">
                                    <select id="<?php echo $this->get_field_id('page_property_booking_url'); ?>"
                                        name="<?php echo $this->get_field_name('page_property_booking_url'); ?>">
                                        <option value="">Please Select</option>


                                        <?php

        $page = $display_instance['page_property_booking_url'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                ?>
                                        <option value="<?php echo $key->ID; ?>" selected="selected">
                                            <?php echo $key->post_title; ?></option>

                                        <?php
} else {
                ?>
                                        <option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>

                                        <?php
}
            ?>

                                        <?php
}
        ?>

                                    </select>
                                </div>

                            </div>
                        </div>


                        <script>
                        function show_hide_div_property_booking_url(val) {

                            if (val == 'page') {
                                jQuery("#page_div_property_booking_url").show();
                                jQuery("#link_div_property_booking_url").hide();
                            } else if (val == 'link') {
                                jQuery("#page_div_property_booking_url").hide();
                                jQuery("#link_div_property_booking_url").show();
                            }

                        }
                        </script>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget" <?php echo $show2; ?>
                            id="link_div_property_booking_url">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('property_booking_url')); ?>"><?php esc_attr_e('Booking icon Url', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('property_booking_url')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('property_booking_url')); ?>"
                                    type="text" value="<?php echo esc_attr($property_booking_url); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('property_booking_url_title')); ?>"><?php esc_attr_e('Booking Title', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('property_booking_url_title')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('property_booking_url_title')); ?>"
                                    type="text" value="<?php echo esc_attr($property_booking_url_title); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('property_booking_icon_src')); ?>"><?php esc_attr_e('Booking Icon Image', 'ag_hospitality');?>:</label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('property_booking_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('property_booking_icon_src')); ?>"
                                    type="text" value="<?php echo esc_attr($property_booking_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('property_booking_icon_src_alt')); ?>"><?php esc_attr_e('Property Booking Icon Image Alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('property_booking_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('property_booking_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($property_booking_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('property_booking_hover_icon_src')); ?>"><?php esc_attr_e('Booking Hover Icon Image', 'ag_hospitality');?>:</label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('property_booking_hover_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('property_booking_hover_icon_src')); ?>"
                                    type="text" value="<?php echo esc_attr($property_booking_hover_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('property_booking_hover_icon_src_alt')); ?>"><?php esc_attr_e('Property Booking Hover Icon Image Alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('property_booking_hover_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('property_booking_hover_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($property_booking_hover_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>

                    </div>

                </div>


                <div style="clear:both;"></div>

                <div class="row">
                    <div class="col-md-6">
                        <!--property mgmt -->

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo $this->get_field_id('link_type_property_mgmt_url'); ?>"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?></label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <select id="<?php echo $this->get_field_id('link_type_property_mgmt_url'); ?>"
                                    name="<?php echo $this->get_field_name('link_type_property_mgmt_url'); ?>"
                                    onChange="show_hide_div_property_mgmt_url(this.value);">
                                    <option value="">Please Select</option>

                                    <?php
$link_type = $instance['link_type_property_mgmt_url'];

        if ($link_type == 'page') {
            ?>
                                    <option value="page" selected="selected">Internal Page Link</option>
                                    <?php
} else {
            ?>
                                    <option value="page">Internal Page Link</option>

                                    <?php
}

        if ($link_type == 'link') {
            ?>
                                    <option value="link" selected="selected">External Link</option>
                                    <?php

        } else {
            ?>
                                    <option value="link">External Link</option>
                                    <?php
}

        ?>
                                </select>
                            </div>

                        </div>
                        </p>

                        <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block !important;"';
            $show2 = 'style="display:none !important;"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:block !important;"';

        } else {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:none !important;"';
        }
        ?>
                        <div <?php echo $show1; ?> id="page_div_property_mgmt_url">
                            <div class="flex agilysys-hospitality-solution-widget">

                                <div class="ag-hosp-widget-title-lable">
                                    <label for="<?php echo $this->get_field_id('page_property_mgmt_url'); ?>">
                                        <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>
                                </div>
                                <div class="ag-hosp-widget-title-input-text">
                                    <select id="<?php echo $this->get_field_id('page_property_mgmt_url'); ?>"
                                        name="<?php echo $this->get_field_name('page_property_mgmt_url'); ?>">
                                        <option value="">Please Select</option>


                                        <?php

        $page = $display_instance['page_property_mgmt_url'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                ?>
                                        <option value="<?php echo $key->ID; ?>" selected="selected">
                                            <?php echo $key->post_title; ?></option>

                                        <?php
} else {
                ?>
                                        <option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>

                                        <?php
}
            ?>

                                        <?php
}
        ?>

                                    </select>
                                </div>
                            </div>
                        </div>


                        <script>
                        function show_hide_div_property_mgmt_url(val) {

                            if (val == 'page') {
                                jQuery("#page_div_property_mgmt_url").show();
                                jQuery("#link_div_property_mgmt_url").hide();
                            } else if (val == 'link') {
                                jQuery("#page_div_property_mgmt_url").hide();
                                jQuery("#link_div_property_mgmt_url").show();
                            }

                        }
                        </script>

                        <div <?php echo $show2; ?> id="link_div_property_mgmt_url">
                            <div class="flex agilysys-hospitality-solution-widget">
                                <div class="ag-hosp-widget-title-lable">
                                    <label
                                        for="<?php echo esc_attr($this->get_field_id('property_mgmt_url')); ?>"><?php esc_attr_e('Property Mgmt icon Url', 'ag_hospitality');?>:</label>
                                </div>

                                <div class="ag-hosp-widget-title-input-text">
                                    <input class="widefat"
                                        id="<?php echo esc_attr($this->get_field_id('property_mgmt_url')); ?>"
                                        name="<?php echo esc_attr($this->get_field_name('property_mgmt_url')); ?>"
                                        type="text" value="<?php echo esc_attr($property_mgmt_url); ?>">
                                </div>

                            </div>
                        </div>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('property_mgmt_url_title')); ?>"><?php esc_attr_e('Button Title', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('property_mgmt_url_title')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('property_mgmt_url_title')); ?>"
                                    type="text" value="<?php echo esc_attr($property_mgmt_url_title); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('property_mgmt_icon_src')); ?>"><?php esc_attr_e('Property MGMT Icon Image', 'ag_hospitality');?>:</label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('property_mgmt_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('property_mgmt_icon_src')); ?>"
                                    type="text" value="<?php echo esc_attr($property_mgmt_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>


                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('property_mgmt_icon_src_alt')); ?>"><?php esc_attr_e('Property MGMT Icon Image Alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('property_mgmt_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('property_mgmt_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($property_mgmt_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>


                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('property_mgmt_hover_icon_src')); ?>"><?php esc_attr_e('Property MGMT Hover Icon Image', 'ag_hospitality');?>:</label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('property_mgmt_hover_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('property_mgmt_hover_icon_src')); ?>"
                                    type="text" value="<?php echo esc_attr($property_mgmt_hover_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('property_mgmt_hover_icon_src_alt')); ?>"><?php esc_attr_e('Property MGMT Hover Icon Image Alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('property_mgmt_hover_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('property_mgmt_hover_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($property_mgmt_hover_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>

                        <!--property management-->
                        <!--1st row ends-->

                    </div>
                    <div class="col-md-6">

                        <!--2nd row starts-->
                        <!--Pos mgmt -->

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo $this->get_field_id('link_type_pos_url'); ?>"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?></label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <select id="<?php echo $this->get_field_id('link_type_pos_url'); ?>"
                                    name="<?php echo $this->get_field_name('link_type_pos_url'); ?>"
                                    onChange="show_hide_div_pos_url(this.value);">
                                    <option value="">Please Select</option>

                                    <?php
$link_type = $instance['link_type_pos_url'];

        if ($link_type == 'page') {
            ?>
                                    <option value="page" selected="selected">Internal Page Link</option>
                                    <?php
} else {
            ?>
                                    <option value="page">Internal Page Link</option>

                                    <?php
}

        if ($link_type == 'link') {
            ?>
                                    <option value="link" selected="selected">External Link</option>
                                    <?php

        } else {
            ?>
                                    <option value="link">External Link</option>
                                    <?php
}

        ?>
                                </select>
                            </div>

                        </div>
                        </p>

                        <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block !important;"';
            $show2 = 'style="display:none !important;"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:block !important;"';

        } else {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:none !important;"';
        }
        ?>
                        <div <?php echo $show1; ?> id="page_div_pos_url">
                            <div class="flex agilysys-hospitality-solution-widget">
                                <div class="ag-hosp-widget-title-lable">
                                    <label for="<?php echo $this->get_field_id('page_pos_url'); ?>">
                                        <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>
                                </div>
                                <div class="ag-hosp-widget-title-input-text">
                                    <select id="<?php echo $this->get_field_id('page_pos_url'); ?>"
                                        name="<?php echo $this->get_field_name('page_pos_url'); ?>">
                                        <option value="">Please Select</option>


                                        <?php

        $page = $display_instance['page_pos_url'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                ?>
                                        <option value="<?php echo $key->ID; ?>" selected="selected">
                                            <?php echo $key->post_title; ?></option>

                                        <?php
} else {
                ?>
                                        <option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>

                                        <?php
}
            ?>

                                        <?php
}
        ?>

                                    </select>
                                </div>
                            </div>
                        </div>


                        <script>
                        function show_hide_div_pos_url(val) {

                            if (val == 'page') {
                                jQuery("#page_div_pos_url").show();
                                jQuery("#link_div_pos_url").hide();
                            } else if (val == 'link') {
                                jQuery("#page_div_pos_url").hide();
                                jQuery("#link_div_pos_url").show();
                            }

                        }
                        </script>

                        <div <?php echo $show2; ?> id="link_div_pos_url">
                            <div class="flex agilysys-hospitality-solution-widget">
                                <div class="ag-hosp-widget-title-lable">
                                    <label
                                        for="<?php echo esc_attr($this->get_field_id('pos_url')); ?>"><?php esc_attr_e('Pos icon Url', 'ag_hospitality');?>:</label>
                                </div>

                                <div class="ag-hosp-widget-title-input-text">
                                    <input class="widefat" id="<?php echo esc_attr($this->get_field_id('pos_url')); ?>"
                                        name="<?php echo esc_attr($this->get_field_name('pos_url')); ?>" type="text"
                                        value="<?php echo esc_attr($pos_url); ?>">
                                </div>

                            </div>
                        </div>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('pos_url_title')); ?>"><?php esc_attr_e('Button Title', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('pos_url_title')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('pos_url_title')); ?>" type="text"
                                    value="<?php echo esc_attr($pos_url_title); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('pos_icon_src')); ?>"><?php esc_attr_e('Pos Icon Image', 'ag_hospitality');?>:</label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('pos_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('pos_icon_src')); ?>" type="text"
                                    value="<?php echo esc_attr($pos_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('pos_icon_src_alt')); ?>"><?php esc_attr_e('Pos Icon Image Alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('pos_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('pos_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($pos_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('pos_hover_icon_src')); ?>"><?php esc_attr_e('Pos Hover Icon Image', 'ag_hospitality');?>:</label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('pos_hover_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('pos_hover_icon_src')); ?>"
                                    type="text" value="<?php echo esc_attr($pos_hover_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>


                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('pos_hover_icon_src_alt')); ?>"><?php esc_attr_e('Pos Hover Icon Image Alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('pos_hover_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('pos_hover_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($pos_hover_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>




                    </div>
                </div>


                <div style="clear:both;"></div>


                <div class="row">

                    <div class="col-md-6">


                        <!--Inventory mgmt -->

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo $this->get_field_id('link_type_inventory_url'); ?>"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?></label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <select id="<?php echo $this->get_field_id('link_type_inventory_url'); ?>"
                                    name="<?php echo $this->get_field_name('link_type_inventory_url'); ?>"
                                    onChange="show_hide_div_inventory_url(this.value);">
                                    <option value="">Please Select</option>

                                    <?php
$link_type = $instance['link_type_inventory_url'];

        if ($link_type == 'page') {
            ?>
                                    <option value="page" selected="selected">Internal Page Link</option>
                                    <?php
} else {
            ?>
                                    <option value="page">Internal Page Link</option>

                                    <?php
}

        if ($link_type == 'link') {
            ?>
                                    <option value="link" selected="selected">External Link</option>
                                    <?php

        } else {
            ?>
                                    <option value="link">External Link</option>
                                    <?php
}

        ?>
                                </select>
                            </div>

                        </div>
                        </p>

                        <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block !important;"';
            $show2 = 'style="display:none !important;"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:block !important;"';

        } else {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:none !important;"';
        }
        ?>
                        <div <?php echo $show1; ?> id="page_div_inventory_url">
                            <div class="flex agilysys-hospitality-solution-widget">
                                <div class="ag-hosp-widget-title-lable">
                                    <label for="<?php echo $this->get_field_id('page_inventory_url'); ?>">
                                        <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>
                                </div>
                                <div class="ag-hosp-widget-title-input-text">
                                    <select id="<?php echo $this->get_field_id('page_inventory_url'); ?>"
                                        name="<?php echo $this->get_field_name('page_inventory_url'); ?>">
                                        <option value="">Please Select</option>


                                        <?php

        $page = $display_instance['page_inventory_url'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                ?>
                                        <option value="<?php echo $key->ID; ?>" selected="selected">
                                            <?php echo $key->post_title; ?></option>

                                        <?php
} else {
                ?>
                                        <option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>

                                        <?php
}
            ?>

                                        <?php
}
        ?>

                                    </select>
                                </div>
                            </div>
                        </div>


                        <script>
                        function show_hide_div_inventory_url(val) {

                            if (val == 'page') {
                                jQuery("#page_div_inventory_url").show();
                                jQuery("#link_div_inventory_url").hide();
                            } else if (val == 'link') {
                                jQuery("#page_div_inventory_url").hide();
                                jQuery("#link_div_inventory_url").show();
                            }

                        }
                        </script>

                        <div <?php echo $show2; ?> id="link_div_inventory_url">
                            <div class="flex agilysys-hospitality-solution-widget">
                                <div class="ag-hosp-widget-title-lable">
                                    <label
                                        for="<?php echo esc_attr($this->get_field_id('inventory_url')); ?>"><?php esc_attr_e('Inventory icon Url', 'ag_hospitality');?>:</label>
                                </div>

                                <div class="ag-hosp-widget-title-input-text">
                                    <input class="widefat"
                                        id="<?php echo esc_attr($this->get_field_id('inventory_url')); ?>"
                                        name="<?php echo esc_attr($this->get_field_name('inventory_url')); ?>"
                                        type="text" value="<?php echo esc_attr($inventory_url); ?>">
                                </div>

                            </div>
                        </div>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('inventory_url_title')); ?>"><?php esc_attr_e('Button Title', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('inventory_url_title')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('inventory_url_title')); ?>"
                                    type="text" value="<?php echo esc_attr($inventory_url_title); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('inventory_icon_src')); ?>"><?php esc_attr_e('Inventory Icon Image', 'ag_hospitality');?>:</label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('inventory_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('inventory_icon_src')); ?>"
                                    type="text" value="<?php echo esc_attr($inventory_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('inventory_icon_src_alt')); ?>"><?php esc_attr_e('Inventory Icon Image Alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('inventory_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('inventory_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($inventory_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('inventory_hover_icon_src')); ?>"><?php esc_attr_e('Inventory Hover Icon Image', 'ag_hospitality');?>:</label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('inventory_hover_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('inventory_hover_icon_src')); ?>"
                                    type="text" value="<?php echo esc_attr($inventory_hover_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('inventory_hover_icon_src_alt')); ?>"><?php esc_attr_e('Inventory Hover Icon Image Alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('inventory_hover_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('inventory_hover_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($inventory_hover_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>
                    </div>

                    <div class="col-md-6">

                        <!--Documet mgmt -->

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo $this->get_field_id('link_type_document_url'); ?>"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?></label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <select id="<?php echo $this->get_field_id('link_type_document_url'); ?>"
                                    name="<?php echo $this->get_field_name('link_type_document_url'); ?>"
                                    onChange="show_hide_div_document_url(this.value);">
                                    <option value="">Please Select</option>

                                    <?php
$link_type = $instance['link_type_document_url'];

        if ($link_type == 'page') {
            ?>
                                    <option value="page" selected="selected">Internal Page Link</option>
                                    <?php
} else {
            ?>
                                    <option value="page">Internal Page Link</option>

                                    <?php
}

        if ($link_type == 'link') {
            ?>
                                    <option value="link" selected="selected">External Link</option>
                                    <?php

        } else {
            ?>
                                    <option value="link">External Link</option>
                                    <?php
}

        ?>
                                </select>
                            </div>

                        </div>
                        </p><br>

                        <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block !important;"';
            $show2 = 'style="display:none !important;"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:block !important;"';

        } else {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:none !important;"';
        }
        ?>

                        <div id="page_div_document_url" <?php echo $show1; ?>>
                            <div class="flex agilysys-hospitality-solution-widget">

                                <div class="ag-hosp-widget-title-lable">
                                    <label for="<?php echo $this->get_field_id('page_document_url'); ?>">
                                        <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>
                                </div>
                                <div class="ag-hosp-widget-title-input-text">
                                    <select id="<?php echo $this->get_field_id('page_document_url'); ?>"
                                        name="<?php echo $this->get_field_name('page_document_url'); ?>">
                                        <option value="">Please Select</option>


                                        <?php

        $page = $display_instance['page_document_url'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                ?>
                                        <option value="<?php echo $key->ID; ?>" selected="selected">
                                            <?php echo $key->post_title; ?></option>

                                        <?php
} else {
                ?>
                                        <option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>

                                        <?php
}
            ?>

                                        <?php
}
        ?>

                                    </select>
                                </div>
                            </div>
                        </div>


                        <script>
                        function show_hide_div_document_url(val) {

                            if (val == 'page') {
                                jQuery("#page_div_document_url").show();
                                jQuery("#link_div_document_url").hide();
                            } else if (val == 'link') {
                                jQuery("#page_div_document_url").hide();
                                jQuery("#link_div_document_url").show();
                            }

                        }
                        </script>

                        <div <?php echo $show2; ?> id="link_div_document_url">
                            <div class="flex agilysys-hospitality-solution-widget">
                                <div class="ag-hosp-widget-title-lable">
                                    <label
                                        for="<?php echo esc_attr($this->get_field_id('document_url')); ?>"><?php esc_attr_e('Document icon Url', 'ag_hospitality');?>:</label>
                                </div>

                                <div class="ag-hosp-widget-title-input-text">
                                    <input class="widefat"
                                        id="<?php echo esc_attr($this->get_field_id('document_url')); ?>"
                                        name="<?php echo esc_attr($this->get_field_name('document_url')); ?>"
                                        type="text" value="<?php echo esc_attr($document_url); ?>">
                                </div>

                            </div>
                        </div>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('document_url_title')); ?>"><?php esc_attr_e('Button Title', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('document_url_title')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('document_url_title')); ?>"
                                    type="text" value="<?php echo esc_attr($document_url_title); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('document_icon_src')); ?>"><?php esc_attr_e('Document Icon Image', 'ag_hospitality');?>:</label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('document_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('document_icon_src')); ?>"
                                    type="text" value="<?php echo esc_attr($document_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('document_icon_src_alt')); ?>"><?php esc_attr_e('Document Icon Image Alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('document_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('document_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($document_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('document_hover_icon_src')); ?>"><?php esc_attr_e('Document Hover Icon Image', 'ag_hospitality');?>:</label>
                            </div>
                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('document_hover_icon_src')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('document_hover_icon_src')); ?>"
                                    type="text" value="<?php echo esc_attr($document_hover_icon_src); ?>">
                                <button
                                    class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                            </div>
                        </div>
                        </p>

                        <p>
                        <div class="flex agilysys-hospitality-solution-widget">
                            <div class="ag-hosp-widget-title-lable">
                                <label
                                    for="<?php echo esc_attr($this->get_field_id('document_hover_icon_src_alt')); ?>"><?php esc_attr_e('Document Hover Icon Image Alt', 'ag_hospitality');?>:</label>
                            </div>

                            <div class="ag-hosp-widget-title-input-text">
                                <input class="widefat"
                                    id="<?php echo esc_attr($this->get_field_id('document_hover_icon_src_alt')); ?>"
                                    name="<?php echo esc_attr($this->get_field_name('document_hover_icon_src_alt')); ?>"
                                    type="text" value="<?php echo esc_attr($document_hover_icon_src_alt); ?>">
                            </div>

                        </div>
                        </p>

                        <!--Property spa mgmt -->

                    </div>
                </div>





                <div style="clear:both;"></div>


                <div class="row">

                    <div class="col-md-6">
                    <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo $this->get_field_id('link_type_prporty_spa_url'); ?>"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?></label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <select id="<?php echo $this->get_field_id('link_type_prporty_spa_url'); ?>"
                            name="<?php echo $this->get_field_name('link_type_prporty_spa_url'); ?>"
                            onChange="show_hide_div_prporty_spa_url(this.value);">
                            <option value="">Please Select</option>

                            <?php
$link_type = $instance['link_type_prporty_spa_url'];

        if ($link_type == 'page') {
            ?>
                            <option value="page" selected="selected">Internal Page Link</option>
                            <?php
} else {
            ?>
                            <option value="page">Internal Page Link</option>

                            <?php
}

        if ($link_type == 'link') {
            ?>
                            <option value="link" selected="selected">External Link</option>
                            <?php

        } else {
            ?>
                            <option value="link">External Link</option>
                            <?php
}

        ?>
                        </select>
                    </div>

                </div>
                </p><br>

                <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block !important;"';
            $show2 = 'style="display:none !important;"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:block !important;"';

        } else {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:none !important;"';
        }
        ?>
                <div id="page_div_prporty_spa_url" <?php echo $show1; ?>>
                    <div class="flex agilysys-hospitality-solution-widget">
                        <div class="ag-hosp-widget-title-lable">
                            <label for="<?php echo $this->get_field_id('page_prporty_spa_url'); ?>">
                                <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>
                        </div>
                        <div class="ag-hosp-widget-title-input-text">
                            <select id="<?php echo $this->get_field_id('page_prporty_spa_url'); ?>"
                                name="<?php echo $this->get_field_name('page_prporty_spa_url'); ?>">
                                <option value="">Please Select</option>


                                <?php

        $page = $display_instance['page_prporty_spa_url'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                ?>
                                <option value="<?php echo $key->ID; ?>" selected="selected">
                                    <?php echo $key->post_title; ?></option>

                                <?php
} else {
                ?>
                                <option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>

                                <?php
}
            ?>

                                <?php
}
        ?>

                            </select>
                        </div>
                    </div>
                </div>


                <script>
                function show_hide_div_prporty_spa_url(val) {

                    if (val == 'page') {
                        jQuery("#page_div_prporty_spa_url").show();
                        jQuery("#link_div_prporty_spa_url").hide();
                    } else if (val == 'link') {
                        jQuery("#page_div_prporty_spa_url").hide();
                        jQuery("#link_div_prporty_spa_url").show();
                    }

                }
                </script>

                <div <?php echo $show2; ?> id="link_div_prporty_spa_url">
                    <div class="flex agilysys-hospitality-solution-widget">
                        <div class="ag-hosp-widget-title-lable">
                            <label
                                for="<?php echo esc_attr($this->get_field_id('prporty_spa_url')); ?>"><?php esc_attr_e('Property spa icon Url', 'ag_hospitality');?>:</label>
                        </div>

                        <div class="ag-hosp-widget-title-input-text">
                            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('prporty_spa_url')); ?>"
                                name="<?php echo esc_attr($this->get_field_name('prporty_spa_url')); ?>" type="text"
                                value="<?php echo esc_attr($prporty_spa_url); ?>">
                        </div>

                    </div>
                </div>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_spa_url_title')); ?>"><?php esc_attr_e('Button Title', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_spa_url_title')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_spa_url_title')); ?>" type="text"
                            value="<?php echo esc_attr($prporty_spa_url_title); ?>">
                    </div>

                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_spa_icon_src')); ?>"><?php esc_attr_e('Property spa Icon Image', 'ag_hospitality');?>:</label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('prporty_spa_icon_src')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_spa_icon_src')); ?>" type="text"
                            value="<?php echo esc_attr($prporty_spa_icon_src); ?>">
                        <button
                            class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                    </div>
                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_spa_icon_src_alt')); ?>"><?php esc_attr_e('Property Icon Image Alt', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_spa_icon_src_alt')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_spa_icon_src_alt')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_spa_icon_src_alt); ?>">
                    </div>

                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_spa_hover_icon_src')); ?>"><?php esc_attr_e('Property spa Hover Icon Image', 'ag_hospitality');?>:</label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_spa_hover_icon_src')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_spa_hover_icon_src')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_spa_hover_icon_src); ?>">
                        <button
                            class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                    </div>
                </div>
                </p>



                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_spa_hover_icon_src_alt')); ?>"><?php esc_attr_e('Property Spa Hover Icon Image Alt', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_spa_hover_icon_src_alt')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_spa_hover_icon_src_alt')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_spa_hover_icon_src_alt); ?>">
                    </div>

                </div>
                </p>

                <!--2nd row ends here-->

              
                    </div>
                    <div class="col-md-6">
  <!--3rd row starts here-->
                <!--1. prporty_digital_key_url-->

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo $this->get_field_id('link_type_prporty_digital_key_url'); ?>"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?></label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <select id="<?php echo $this->get_field_id('link_type_prporty_digital_key_url'); ?>"
                            name="<?php echo $this->get_field_name('link_type_prporty_digital_key_url'); ?>"
                            onChange="show_hide_div_prporty_digital_key_url(this.value);">
                            <option value="">Please Select</option>

                            <?php
$link_type = $instance['link_type_prporty_digital_key_url'];

        if ($link_type == 'page') {
            ?>
                            <option value="page" selected="selected">Internal Page Link</option>
                            <?php
} else {
            ?>
                            <option value="page">Internal Page Link</option>

                            <?php
}

        if ($link_type == 'link') {
            ?>
                            <option value="link" selected="selected">External Link</option>
                            <?php

        } else {
            ?>
                            <option value="link">External Link</option>
                            <?php
}

        ?>
                        </select>
                    </div>

                </div>
                </p><br>

                <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block !important;"';
            $show2 = 'style="display:none !important;"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:block !important;"';

        } else {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:none !important;"';
        }
        ?>
                <div id="page_div_prporty_digital_key_url" <?php echo $show1; ?>>
                    <div class="flex agilysys-hospitality-solution-widget">
                        <div class="ag-hosp-widget-title-lable">
                            <label for="<?php echo $this->get_field_id('page_prporty_digital_key_url'); ?>">
                                <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>
                        </div>
                        <div class="ag-hosp-widget-title-input-text">
                            <select id="<?php echo $this->get_field_id('page_prporty_digital_key_url'); ?>"
                                name="<?php echo $this->get_field_name('page_prporty_digital_key_url'); ?>">
                                <option value="">Please Select</option>


                                <?php

        $page = $display_instance['page_prporty_digital_key_url'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                ?>
                                <option value="<?php echo $key->ID; ?>" selected="selected">
                                    <?php echo $key->post_title; ?></option>

                                <?php
} else {
                ?>
                                <option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>

                                <?php
}
            ?>

                                <?php
}
        ?>

                            </select>
                        </div>
                    </div>
                </div>


                <script>
                function show_hide_div_prporty_digital_key_url(val) {

                    if (val == 'page') {
                        jQuery("#page_div_prporty_digital_key_url").show();
                        jQuery("#link_div_prporty_digital_key_url").hide();
                    } else if (val == 'link') {
                        jQuery("#page_div_prporty_digital_key_url").hide();
                        jQuery("#link_div_prporty_digital_key_url").show();
                    }

                }
                </script>


                <div <?php echo $show2; ?> id="link_div_prporty_digital_key_url">
                    <div class="flex agilysys-hospitality-solution-widget">
                        <div class="ag-hosp-widget-title-lable">
                            <label
                                for="<?php echo esc_attr($this->get_field_id('prporty_digital_key_url')); ?>"><?php esc_attr_e('Property Digital key icon Url', 'ag_hospitality');?>:</label>
                        </div>

                        <div class="ag-hosp-widget-title-input-text">
                            <input class="widefat"
                                id="<?php echo esc_attr($this->get_field_id('prporty_digital_key_url')); ?>"
                                name="<?php echo esc_attr($this->get_field_name('prporty_digital_key_url')); ?>"
                                type="text" value="<?php echo esc_attr($prporty_digital_key_url); ?>">
                        </div>

                    </div>
                </div>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_digital_key_url_title')); ?>"><?php esc_attr_e('Button Title', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_digital_key_url_title')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_digital_key_url_title')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_digital_key_url_title); ?>">
                    </div>

                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_digital_key_url_icon_src')); ?>"><?php esc_attr_e('Property Digital Key Icon Image', 'ag_hospitality');?>:</label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_digital_key_url_icon_src')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_digital_key_url_icon_src')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_digital_key_url_icon_src); ?>">
                        <button
                            class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                    </div>
                </div>
                </p>


                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_digital_key_url_icon_src_alt')); ?>"><?php esc_attr_e('Property Digital Key Icon Image Alt', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_digital_key_url_icon_src_alt')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_digital_key_url_icon_src_alt')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_digital_key_url_icon_src_alt); ?>">
                    </div>

                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_digital_key_url_icon_hover_icon_src')); ?>"><?php esc_attr_e('Property Digital Key Hover Icon Image', 'ag_hospitality');?>:</label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_digital_key_url_icon_hover_icon_src')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_digital_key_url_icon_hover_icon_src')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_digital_key_url_icon_hover_icon_src); ?>">
                        <button
                            class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                    </div>
                </div>
                </p>


                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_digital_key_url_icon_hover_icon_src_alt')); ?>"><?php esc_attr_e('Property Digital Key Hover Icon Image Alt', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_digital_key_url_icon_hover_icon_src_alt')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_digital_key_url_icon_hover_icon_src_alt')); ?>"
                            type="text"
                            value="<?php echo esc_attr($prporty_digital_key_url_icon_hover_icon_src_alt); ?>">
                    </div>

                </div>
                </p>


                <!--2. prporty_mobile_checkin_key_url-->
                    </div>

                </div>

                <div style="clear:both;"></div>


                <div class="row">

<div class="col-md-6">
<p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo $this->get_field_id('link_type_prporty_mobile_checkin_key_url'); ?>"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?></label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <select id="<?php echo $this->get_field_id('link_type_prporty_mobile_checkin_key_url'); ?>"
                            name="<?php echo $this->get_field_name('link_type_prporty_mobile_checkin_key_url'); ?>"
                            onChange="show_hide_div_prporty_mobile_checkin_key_url(this.value);">
                            <option value="">Please Select</option>

                            <?php
$link_type = $instance['link_type_prporty_mobile_checkin_key_url'];

        if ($link_type == 'page') {
            ?>
                            <option value="page" selected="selected">Internal Page Link</option>
                            <?php
} else {
            ?>
                            <option value="page">Internal Page Link</option>

                            <?php
}

        if ($link_type == 'link') {
            ?>
                            <option value="link" selected="selected">External Link</option>
                            <?php

        } else {
            ?>
                            <option value="link">External Link</option>
                            <?php
}

        ?>
                        </select>
                    </div>

                </div>
                </p><br>

                <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block !important;"';
            $show2 = 'style="display:none !important;"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:block !important;"';

        } else {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:none !important;"';
        }
        ?>
                <div id="page_div_prporty_mobile_checkin_key_url" <?php echo $show1; ?>>
                    <div class="flex agilysys-hospitality-solution-widget">
                        <div class="ag-hosp-widget-title-lable">
                            <label for="<?php echo $this->get_field_id('page_prporty_mobile_checkin_key_url'); ?>">
                                <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>
                        </div>
                        <div class="ag-hosp-widget-title-input-text">
                            <select id="<?php echo $this->get_field_id('page_prporty_mobile_checkin_key_url'); ?>"
                                name="<?php echo $this->get_field_name('page_prporty_mobile_checkin_key_url'); ?>">
                                <option value="">Please Select</option>


                                <?php

        $page = $display_instance['page_prporty_mobile_checkin_key_url'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                ?>
                                <option value="<?php echo $key->ID; ?>" selected="selected">
                                    <?php echo $key->post_title; ?></option>

                                <?php
} else {
                ?>
                                <option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>

                                <?php
}
            ?>

                                <?php
}
        ?>

                            </select>
                        </div>
                    </div>
                </div>


                <script>
                function show_hide_div_prporty_mobile_checkin_key_url(val) {

                    if (val == 'page') {
                        jQuery("#page_div_prporty_mobile_checkin_key_url").show();
                        jQuery("#link_div_prporty_mobile_checkin_key_url").hide();
                    } else if (val == 'link') {
                        jQuery("#page_div_prporty_mobile_checkin_key_url").hide();
                        jQuery("#link_div_prporty_mobile_checkin_key_url").show();
                    }

                }
                </script>


                <div <?php echo $show2; ?> id="link_div_prporty_mobile_checkin_key_url">
                    <div class="flex agilysys-hospitality-solution-widget">
                        <div class="ag-hosp-widget-title-lable">
                            <label
                                for="<?php echo esc_attr($this->get_field_id('prporty_mobile_checkin_key_url')); ?>"><?php esc_attr_e('Property Mobile Checkin icon Url', 'ag_hospitality');?>:</label>
                        </div>

                        <div class="ag-hosp-widget-title-input-text">
                            <input class="widefat"
                                id="<?php echo esc_attr($this->get_field_id('prporty_mobile_checkin_key_url')); ?>"
                                name="<?php echo esc_attr($this->get_field_name('prporty_mobile_checkin_key_url')); ?>"
                                type="text" value="<?php echo esc_attr($prporty_mobile_checkin_key_url); ?>">
                        </div>

                    </div>
                </div>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_mobile_checkin_key_url_title')); ?>"><?php esc_attr_e('Button Title', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_mobile_checkin_key_url_title')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_mobile_checkin_key_url_title')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_mobile_checkin_key_url_title); ?>">
                    </div>

                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_mobile_checkin_icon_src')); ?>"><?php esc_attr_e('Property Mobile Checkin Image', 'ag_hospitality');?>:</label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_mobile_checkin_icon_src')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_mobile_checkin_icon_src')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_mobile_checkin_icon_src); ?>">
                        <button
                            class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                    </div>
                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_mobile_checkin_icon_src_alt')); ?>"><?php esc_attr_e('Property Mobile Checkin Image Alt', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_mobile_checkin_icon_src_alt')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_mobile_checkin_icon_src_alt')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_mobile_checkin_icon_src_alt); ?>">
                    </div>

                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_mobile_checkin_hover_src')); ?>"><?php esc_attr_e('Property Mobile Checkin Hover Icon Image', 'ag_hospitality');?>:</label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_mobile_checkin_hover_src')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_mobile_checkin_hover_src')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_mobile_checkin_hover_src); ?>">
                        <button
                            class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                    </div>
                </div>
                </p>


                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_mobile_checkin_hover_src_alt')); ?>"><?php esc_attr_e('Property Mobile Checkin Hover Image Alt', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_mobile_checkin_hover_src_alt')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_mobile_checkin_hover_src_alt')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_mobile_checkin_hover_src_alt); ?>">
                    </div>

                </div>
                </p>

                <!--3. Analytics checkin -->
</div>
<div class="col-md-6">
      <!-- prporty_analytics_url prporty_analytics_url_title prporty_analytics_icon_src prporty_analytics_icon_hover_src-->


      <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo $this->get_field_id('link_type_prporty_analytics_url'); ?>"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?></label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <select id="<?php echo $this->get_field_id('link_type_prporty_analytics_url'); ?>"
                            name="<?php echo $this->get_field_name('link_type_prporty_analytics_url'); ?>"
                            onChange="show_hide_div_prporty_analytics_url(this.value);">
                            <option value="">Please Select</option>

                            <?php
$link_type = $instance['link_type_prporty_analytics_url'];

        if ($link_type == 'page') {
            ?>
                            <option value="page" selected="selected">Internal Page Link</option>
                            <?php
} else {
            ?>
                            <option value="page">Internal Page Link</option>

                            <?php
}

        if ($link_type == 'link') {
            ?>
                            <option value="link" selected="selected">External Link</option>
                            <?php

        } else {
            ?>
                            <option value="link">External Link</option>
                            <?php
}

        ?>
                        </select>
                    </div>

                </div>
                </p>

                <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block !important;"';
            $show2 = 'style="display:none !important;"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:block !important;"';

        } else {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:none !important;"';
        }
        ?>
                <div id="page_div_prporty_analytics_url" <?php echo $show1; ?>>
                    <div class="flex agilysys-hospitality-solution-widget">
                        <div class="ag-hosp-widget-title-lable">
                            <label for="<?php echo $this->get_field_id('page_prporty_analytics_url'); ?>">
                                <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>
                        </div>
                        <div class="ag-hosp-widget-title-input-text">
                            <select id="<?php echo $this->get_field_id('page_prporty_analytics_url'); ?>"
                                name="<?php echo $this->get_field_name('page_prporty_analytics_url'); ?>">
                                <option value="">Please Select</option>


                                <?php

        $page = $display_instance['page_prporty_analytics_url'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                ?>
                                <option value="<?php echo $key->ID; ?>" selected="selected">
                                    <?php echo $key->post_title; ?></option>

                                <?php
} else {
                ?>
                                <option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>

                                <?php
}
            ?>

                                <?php
}
        ?>

                            </select>
                        </div>
                    </div>
                </div>


                <script>
                function show_hide_div_prporty_analytics_url(val) {

                    if (val == 'page') {
                        jQuery("#page_div_prporty_analytics_url").show();
                        jQuery("#link_div_prporty_analytics_url").hide();
                    } else if (val == 'link') {
                        jQuery("#page_div_prporty_analytics_url").hide();
                        jQuery("#link_div_prporty_analytics_url").show();
                    }

                }
                </script>

                <div <?php echo $show2; ?> id="link_div_prporty_analytics_url">
                    <div class="flex agilysys-hospitality-solution-widget">
                        <div class="ag-hosp-widget-title-lable">
                            <label
                                for="<?php echo esc_attr($this->get_field_id('prporty_analytics_url')); ?>"><?php esc_attr_e('Property Analytics Url', 'ag_hospitality');?>:</label>
                        </div>

                        <div class="ag-hosp-widget-title-input-text">
                            <input class="widefat"
                                id="<?php echo esc_attr($this->get_field_id('prporty_analytics_url')); ?>"
                                name="<?php echo esc_attr($this->get_field_name('prporty_analytics_url')); ?>"
                                type="text" value="<?php echo esc_attr($prporty_analytics_url); ?>">
                        </div>

                    </div>
                </div>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_analytics_url_title')); ?>"><?php esc_attr_e('Button Title', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_analytics_url_title')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_analytics_url_title')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_analytics_url_title); ?>">
                    </div>

                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_analytics_icon_src')); ?>"><?php esc_attr_e('Property Analytics Image', 'ag_hospitality');?>:</label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_analytics_icon_src')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_analytics_icon_src')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_analytics_icon_src); ?>">
                        <button
                            class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                    </div>
                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_analytics_icon_src_alt')); ?>"><?php esc_attr_e('Property Analytics Image Alt', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_analytics_icon_src_alt')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_analytics_icon_src_alt')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_analytics_icon_src_alt); ?>">
                    </div>

                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_analytics_icon_hover_src')); ?>"><?php esc_attr_e('Property Analytics Hover Icon Image', 'ag_hospitality');?>:</label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_analytics_icon_hover_src')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_analytics_icon_hover_src')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_analytics_icon_hover_src); ?>">
                        <button
                            class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                    </div>
                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_analytics_icon_hover_src_alt')); ?>"><?php esc_attr_e('Property Analytics Hover Icon Image Alt', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_analytics_icon_hover_src_alt')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_analytics_icon_hover_src_alt')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_analytics_icon_hover_src_alt); ?>">
                    </div>

                </div>
                </p>

                <!--4. Services-->
</div>

</div>

<div style="clear:both;"></div>

              
<div class="row">

<div class="col-md-6">
  <!--prporty_services_url prporty_services_url_title prporty_services_icon_hover_src prporty_services_icon_src-->

  <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo $this->get_field_id('link_type_prporty_services_url'); ?>"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?></label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <select id="<?php echo $this->get_field_id('link_type_prporty_services_url'); ?>"
                            name="<?php echo $this->get_field_name('link_type_prporty_services_url'); ?>"
                            onChange="show_hide_div_prporty_services_url(this.value);">
                            <option value="">Please Select</option>

                            <?php
$link_type = $instance['link_type_prporty_services_url'];

        if ($link_type == 'page') {
            ?>
                            <option value="page" selected="selected">Internal Page Link</option>
                            <?php
} else {
            ?>
                            <option value="page">Internal Page Link</option>

                            <?php
}

        if ($link_type == 'link') {
            ?>
                            <option value="link" selected="selected">External Link</option>
                            <?php

        } else {
            ?>
                            <option value="link">External Link</option>
                            <?php
}

        ?>
                        </select>
                    </div>

                </div>
                </p>

                <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block !important;"';
            $show2 = 'style="display:none !important;"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:block !important;"';

        } else {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:none !important;"';
        }
        ?>
                <div id="page_div_prporty_services_url" <?php echo $show1; ?>>
                    <div class="flex agilysys-hospitality-solution-widget">
                        <div class="ag-hosp-widget-title-lable">
                            <label for="<?php echo $this->get_field_id('page_prporty_services_url'); ?>">
                                <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>
                        </div>
                        <div class="ag-hosp-widget-title-input-text">
                            <select id="<?php echo $this->get_field_id('page_prporty_services_url'); ?>"
                                name="<?php echo $this->get_field_name('page_prporty_services_url'); ?>">
                                <option value="">Please Select</option>


                                <?php

        $page = $display_instance['page_prporty_services_url'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                ?>
                                <option value="<?php echo $key->ID; ?>" selected="selected">
                                    <?php echo $key->post_title; ?></option>

                                <?php
} else {
                ?>
                                <option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>

                                <?php
}
            ?>

                                <?php
}
        ?>

                            </select>
                        </div>
                    </div>
                </div>


                <script>
                function show_hide_div_prporty_services_url(val) {

                    if (val == 'page') {
                        jQuery("#page_div_prporty_services_url").show();
                        jQuery("#link_div_prporty_services_url").hide();
                    } else if (val == 'link') {
                        jQuery("#page_div_prporty_services_url").hide();
                        jQuery("#link_div_prporty_services_url").show();
                    }

                }
                </script>


                <div <?php echo $show2; ?> id="link_div_prporty_services_url">
                    <div class="flex agilysys-hospitality-solution-widget">
                        <div class="ag-hosp-widget-title-lable">
                            <label
                                for="<?php echo esc_attr($this->get_field_id('prporty_services_url')); ?>"><?php esc_attr_e('Property Services Url', 'ag_hospitality');?>:</label>
                        </div>

                        <div class="ag-hosp-widget-title-input-text">
                            <input class="widefat"
                                id="<?php echo esc_attr($this->get_field_id('prporty_services_url')); ?>"
                                name="<?php echo esc_attr($this->get_field_name('prporty_services_url')); ?>"
                                type="text" value="<?php echo esc_attr($prporty_services_url); ?>">
                        </div>

                    </div>
                </div>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_services_url_title')); ?>"><?php esc_attr_e('Button Title', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_services_url_title')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_services_url_title')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_services_url_title); ?>">
                    </div>

                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_services_icon_src')); ?>"><?php esc_attr_e('Property Services Image', 'ag_hospitality');?>:</label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_services_icon_src')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_services_icon_src')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_services_icon_src); ?>">
                        <button
                            class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                    </div>
                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_services_icon_src_alt')); ?>"><?php esc_attr_e('Property Services Image Alt', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_services_icon_src_alt')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_services_icon_src_alt')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_services_icon_src_alt); ?>">
                    </div>

                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_services_icon_hover_src')); ?>"><?php esc_attr_e('Property Services Hover Icon Image', 'ag_hospitality');?>:</label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_services_icon_hover_src')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_services_icon_hover_src')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_services_icon_hover_src); ?>">
                        <button
                            class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                    </div>
                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_services_icon_hover_src_alt')); ?>"><?php esc_attr_e('Property Services Hover Image Alt', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_services_icon_hover_src_alt')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_services_icon_hover_src_alt')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_services_icon_hover_src_alt); ?>">
                    </div>

                </div>
                </p>

                <!--5. Self-Service Ordering-->
</div>
<div class="col-md-6">
  <!--prporty_self_services_url   prporty_self_services_url_title prporty_self_services_icon_src prporty_services_icon_hover_src-->
                <!--prporty_digital_payment_url prporty_digital_payment_url prporty_digital_payment_src_icon_hover_src prporty_digital_payment_src_icon_src-->

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo $this->get_field_id('link_type_prporty_self_services_url'); ?>"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?></label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <select id="<?php echo $this->get_field_id('link_type_prporty_self_services_url'); ?>"
                            name="<?php echo $this->get_field_name('link_type_prporty_self_services_url'); ?>"
                            onChange="show_hide_div_prporty_self_services_url(this.value);">
                            <option value="">Please Select</option>

                            <?php
$link_type = $instance['link_type_prporty_self_services_url'];

        if ($link_type == 'page') {
            ?>
                            <option value="page" selected="selected">Internal Page Link</option>
                            <?php
} else {
            ?>
                            <option value="page">Internal Page Link</option>

                            <?php
}

        if ($link_type == 'link') {
            ?>
                            <option value="link" selected="selected">External Link</option>
                            <?php

        } else {
            ?>
                            <option value="link">External Link</option>
                            <?php
}

        ?>
                        </select>
                    </div>

                </div>
                </p><br>

                <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block !important;"';
            $show2 = 'style="display:none !important;"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:block !important;"';

        } else {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:none !important;"';
        }
        ?>
                <div id="page_div_prporty_self_services_url" <?php echo $show1; ?>>
                    <div class="flex agilysys-hospitality-solution-widget">
                        <div class="ag-hosp-widget-title-lable">
                            <label for="<?php echo $this->get_field_id('page_prporty_self_services_url'); ?>">
                                <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>
                        </div>
                        <div class="ag-hosp-widget-title-input-text">
                            <select id="<?php echo $this->get_field_id('page_prporty_self_services_url'); ?>"
                                name="<?php echo $this->get_field_name('page_prporty_self_services_url'); ?>">
                                <option value="">Please Select</option>


                                <?php

        $page = $display_instance['page_prporty_self_services_url'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                ?>
                                <option value="<?php echo $key->ID; ?>" selected="selected">
                                    <?php echo $key->post_title; ?></option>

                                <?php
} else {
                ?>
                                <option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>

                                <?php
}
            ?>

                                <?php
}
        ?>

                            </select>
                        </div>

                    </div>
                </div>

                <script>
                function show_hide_div_prporty_self_services_url(val) {

                    if (val == 'page') {
                        jQuery("#page_div_prporty_self_services_url").show();
                        jQuery("#link_div_prporty_self_services_url").hide();
                    } else if (val == 'link') {
                        jQuery("#page_div_prporty_self_services_url").hide();
                        jQuery("#link_div_prporty_self_services_url").show();
                    }

                }
                </script>

                <div <?php echo $show2; ?> id="link_div_prporty_self_services_url">
                    <div class="flex agilysys-hospitality-solution-widget">
                        <div class="ag-hosp-widget-title-lable">
                            <label
                                for="<?php echo esc_attr($this->get_field_id('prporty_self_services_url')); ?>"><?php esc_attr_e('Property Self Service Url', 'ag_hospitality');?>:</label>
                        </div>

                        <div class="ag-hosp-widget-title-input-text">
                            <input class="widefat"
                                id="<?php echo esc_attr($this->get_field_id('prporty_self_services_url')); ?>"
                                name="<?php echo esc_attr($this->get_field_name('prporty_self_services_url')); ?>"
                                type="text" value="<?php echo esc_attr($prporty_self_services_url); ?>">
                        </div>

                    </div>
                </div>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_self_services_url_title')); ?>"><?php esc_attr_e('Button Title', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_self_services_url_title')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_self_services_url_title')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_self_services_url_title); ?>">
                    </div>

                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_self_services_icon_src')); ?>"><?php esc_attr_e('Property Self Services Image', 'ag_hospitality');?>:</label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_self_services_icon_src')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_self_services_icon_src')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_self_services_icon_src); ?>">
                        <button
                            class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                    </div>
                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_self_services_icon_src_alt')); ?>"><?php esc_attr_e('Property Self Services Image Alt', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_self_services_icon_src_alt')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_self_services_icon_src_alt')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_self_services_icon_src_alt); ?>">
                    </div>

                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_self_services_icon_hover_src')); ?>"><?php esc_attr_e('Property Services Hover Icon Image', 'ag_hospitality');?>:</label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_self_services_icon_hover_src')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_self_services_icon_hover_src')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_self_services_icon_hover_src); ?>">
                        <button
                            class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                    </div>
                </div>
                </p>


                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_self_services_icon_hover_src_alt')); ?>"><?php esc_attr_e('Property Services Hover Image Alt', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_self_services_icon_hover_src_alt')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_self_services_icon_hover_src_alt')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_self_services_icon_hover_src_alt); ?>">
                    </div>

                </div>
                </p>

</div>

</div>

<div style="clear:both;"></div>
                

<div class="row">

<div class="col-md-6">
     <!--6. Digital Key Payment-->
                <!--prporty_digital_payment_url prporty_digital_payment_url prporty_digital_payment_src_icon_hover_src prporty_digital_payment_src_icon_src-->
                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo $this->get_field_id('link_type_prporty_digital_payment_url'); ?>"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?></label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <select id="<?php echo $this->get_field_id('link_type_prporty_digital_payment_url'); ?>"
                            name="<?php echo $this->get_field_name('link_type_prporty_digital_payment_url'); ?>"
                            onChange="show_hide_div_prporty_digital_payment_url(this.value);">
                            <option value="">Please Select</option>

                            <?php
$link_type = $instance['link_type_prporty_digital_payment_url'];

        if ($link_type == 'page') {
            ?>
                            <option value="page" selected="selected">Internal Page Link</option>
                            <?php
} else {
            ?>
                            <option value="page">Internal Page Link</option>

                            <?php
}

        if ($link_type == 'link') {
            ?>
                            <option value="link" selected="selected">External Link</option>
                            <?php

        } else {
            ?>
                            <option value="link">External Link</option>
                            <?php
}

        ?>
                        </select>
                    </div>

                </div>
                </p>

                <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block !important;"';
            $show2 = 'style="display:none !important;"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:block !important;"';

        } else {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:none !important;"';
        }
        ?>
                <div id="page_div_prporty_digital_payment_url" <?php echo $show1; ?>>
                    <div class="flex agilysys-hospitality-solution-widget">
                        <p>
                        <div class="ag-hosp-widget-title-lable">
                            <label for="<?php echo $this->get_field_id('page_prporty_digital_payment_url'); ?>">
                                <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>
                        </div>
                        <div class="ag-hosp-widget-title-input-text">
                            <select id="<?php echo $this->get_field_id('page_prporty_digital_payment_url'); ?>"
                                name="<?php echo $this->get_field_name('page_prporty_digital_payment_url'); ?>">
                                <option value="">Please Select</option>


                                <?php

        $page = $display_instance['page_prporty_digital_payment_url'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                ?>
                                <option value="<?php echo $key->ID; ?>" selected="selected">
                                    <?php echo $key->post_title; ?>
                                </option>

                                <?php
} else {
                ?>
                                <option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>

                                <?php
}
            ?>

                                <?php
}
        ?>

                            </select>
                        </div>
                    </div>
                </div>


                <script>
                function show_hide_div_prporty_digital_payment_url(val) {

                    if (val == 'page') {
                        jQuery("#page_div_prporty_digital_payment_url").show();
                        jQuery("#link_div_prporty_digital_payment_url").hide();
                    } else if (val == 'link') {
                        jQuery("#page_div_prporty_digital_payment_url").hide();
                        jQuery("#link_div_prporty_digital_payment_url").show();
                    }

                }
                </script>
                <div <?php echo $show2; ?> id="link_div_prporty_digital_payment_url">
                    <div class="flex agilysys-hospitality-solution-widget">
                        <div class="ag-hosp-widget-title-lable">
                            <label
                                for="<?php echo esc_attr($this->get_field_id('prporty_digital_payment_url')); ?>"><?php esc_attr_e('Digital Payment Url', 'ag_hospitality');?>:</label>
                        </div>

                        <div class="ag-hosp-widget-title-input-text">
                            <input class="widefat"
                                id="<?php echo esc_attr($this->get_field_id('prporty_digital_payment_url')); ?>"
                                name="<?php echo esc_attr($this->get_field_name('prporty_digital_payment_url')); ?>"
                                type="text" value="<?php echo esc_attr($prporty_digital_payment_url); ?>">
                        </div>

                    </div>
                </div>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_digital_payment_url_title')); ?>"><?php esc_attr_e('Button Title', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_digital_payment_url_title')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_digital_payment_url_title')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_digital_payment_url_title); ?>">
                    </div>

                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_digital_payment_src_icon_src')); ?>"><?php esc_attr_e('Property Digital Payment Image', 'ag_hospitality');?>:</label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_digital_payment_src_icon_src')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_digital_payment_src_icon_src')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_digital_payment_src_icon_src); ?>">
                        <button
                            class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                    </div>
                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_digital_payment_src_icon_src_alt')); ?>"><?php esc_attr_e('Property Digital Payment Image Alt', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_digital_payment_src_icon_src_alt')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_digital_payment_src_icon_src_alt')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_digital_payment_src_icon_src_alt); ?>">
                    </div>

                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_digital_payment_src_icon_hover_src')); ?>"><?php esc_attr_e('Property Digital Payment Hover Icon Image', 'ag_hospitality');?>:</label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_digital_payment_src_icon_hover_src')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_digital_payment_src_icon_hover_src')); ?>"
                            type="text" value="<?php echo esc_attr($prporty_digital_payment_src_icon_hover_src); ?>">
                        <button
                            class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                    </div>
                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('prporty_digital_payment_src_icon_hover_src_alt')); ?>"><?php esc_attr_e('Property Digital Payment Hover Image Alt', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('prporty_digital_payment_src_icon_hover_src_alt')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('prporty_digital_payment_src_icon_hover_src_alt')); ?>"
                            type="text"
                            value="<?php echo esc_attr($prporty_digital_payment_src_icon_hover_src_alt); ?>">
                    </div>

                </div>
                </p>



                <!--4th row-->
                <!-- sales catering property_sales_catering_url property_sales_catering_url_title
    property_sales_catering_icon_src  property_sales_catering_icon_hover_src
    -->
</div>
<div class="col-md-6">
    <!--property_golf_url property_golf_url_title  property_golf_src property_golf_hover_src-->

    <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo $this->get_field_id('link_type_property_sales_catering_url'); ?>"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?></label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <select id="<?php echo $this->get_field_id('link_type_property_sales_catering_url'); ?>"
                            name="<?php echo $this->get_field_name('link_type_property_sales_catering_url'); ?>"
                            onChange="show_hide_div_property_sales_catering_url(this.value);">
                            <option value="">Please Select</option>

                            <?php
$link_type = $instance['link_type_property_sales_catering_url'];

        if ($link_type == 'page') {
            ?>
                            <option value="page" selected="selected">Internal Page Link</option>
                            <?php
} else {
            ?>
                            <option value="page">Internal Page Link</option>

                            <?php
}

        if ($link_type == 'link') {
            ?>
                            <option value="link" selected="selected">External Link</option>
                            <?php

        } else {
            ?>
                            <option value="link">External Link</option>
                            <?php
}

        ?>
                        </select>
                    </div>

                </div>
                </p><br>

                <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block !important;"';
            $show2 = 'style="display:none !important;"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:block !important;"';

        } else {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:none !important;"';
        }
        ?>
                <div id="page_div_property_sales_catering_url" <?php echo $show1; ?>>
                    <div class="flex agilysys-hospitality-solution-widget">
                        <p>
                        <div class="ag-hosp-widget-title-lable">
                            <label for="<?php echo $this->get_field_id('page_property_sales_catering_url'); ?>">
                                <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>
                        </div>
                        <div class="ag-hosp-widget-title-input-text">
                            <select id="<?php echo $this->get_field_id('page_property_sales_catering_url'); ?>"
                                name="<?php echo $this->get_field_name('page_property_sales_catering_url'); ?>">
                                <option value="">Please Select</option>


                                <?php

        $page = $display_instance['page_property_sales_catering_url'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                ?>
                                <option value="<?php echo $key->ID; ?>" selected="selected">
                                    <?php echo $key->post_title; ?>
                                </option>

                                <?php
} else {
                ?>
                                <option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>

                                <?php
}
            ?>

                                <?php
}
        ?>

                            </select>
                        </div>
                    </div>
                </div>


                <script>
                function show_hide_div_property_sales_catering_url(val) {

                    if (val == 'page') {
                        jQuery("#page_div_property_sales_catering_url").show();
                        jQuery("#link_div_property_sales_catering_url").hide();
                    } else if (val == 'link') {
                        jQuery("#page_div_property_sales_catering_url").hide();
                        jQuery("#link_div_property_sales_catering_url").show();
                    }

                }
                </script>

                <div <?php echo $show2; ?> id="link_div_property_sales_catering_url">
                    <div class="flex agilysys-hospitality-solution-widget">
                        <div class="ag-hosp-widget-title-lable">
                            <label
                                for="<?php echo esc_attr($this->get_field_id('property_sales_catering_url')); ?>"><?php esc_attr_e('Property Sales Catering Url', 'ag_hospitality');?>:</label>
                        </div>

                        <div class="ag-hosp-widget-title-input-text">
                            <input class="widefat"
                                id="<?php echo esc_attr($this->get_field_id('property_sales_catering_url')); ?>"
                                name="<?php echo esc_attr($this->get_field_name('property_sales_catering_url')); ?>"
                                type="text" value="<?php echo esc_attr($property_sales_catering_url); ?>">
                        </div>

                    </div>
                </div>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('property_sales_catering_url_title')); ?>"><?php esc_attr_e('Button Title', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('property_sales_catering_url_title')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('property_sales_catering_url_title')); ?>"
                            type="text" value="<?php echo esc_attr($property_sales_catering_url_title); ?>">
                    </div>

                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('property_sales_catering_icon_src')); ?>"><?php esc_attr_e('Property Sales Catering Image', 'ag_hospitality');?>:</label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('property_sales_catering_icon_src')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('property_sales_catering_icon_src')); ?>"
                            type="text" value="<?php echo esc_attr($property_sales_catering_icon_src); ?>">
                        <button
                            class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                    </div>
                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('property_sales_catering_icon_src_alt')); ?>"><?php esc_attr_e('Property Sales Catering Image Alt', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('property_sales_catering_icon_src_alt')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('property_sales_catering_icon_src_alt')); ?>"
                            type="text" value="<?php echo esc_attr($property_sales_catering_icon_src_alt); ?>">
                    </div>

                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('property_sales_catering_icon_hover_src')); ?>"><?php esc_attr_e('Property Sales Catering Hover Image', 'ag_hospitality');?>:</label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('property_sales_catering_icon_hover_src')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('property_sales_catering_icon_hover_src')); ?>"
                            type="text" value="<?php echo esc_attr($property_sales_catering_icon_hover_src); ?>">
                        <button
                            class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                    </div>
                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('property_sales_catering_icon_hover_src_alt')); ?>"><?php esc_attr_e('Property Sales Catering Hover Image Alt', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('property_sales_catering_icon_hover_src_alt')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('property_sales_catering_icon_hover_src_alt')); ?>"
                            type="text" value="<?php echo esc_attr($property_sales_catering_icon_hover_src_alt); ?>">
                    </div>

                </div>
                </p>



                <!--golf-->
</div>

</div>

<div style="clear:both;"></div>      
              

              
<div class="row">

<div class="col-md-6">
  <!--property_golf_url property_golf_url_title  property_golf_src property_golf_hover_src-->

  <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo $this->get_field_id('link_type_property_golf_url'); ?>"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?></label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <select id="<?php echo $this->get_field_id('link_type_property_golf_url'); ?>"
                            name="<?php echo $this->get_field_name('link_type_property_golf_url'); ?>"
                            onChange="show_hide_div_property_golf_url(this.value);">
                            <option value="">Please Select</option>

                            <?php
$link_type = $instance['link_type_property_golf_url'];

        if ($link_type == 'page') {
            ?>
                            <option value="page" selected="selected">Internal Page Link</option>
                            <?php
} else {
            ?>
                            <option value="page">Internal Page Link</option>

                            <?php
}

        if ($link_type == 'link') {
            ?>
                            <option value="link" selected="selected">External Link</option>
                            <?php

        } else {
            ?>
                            <option value="link">External Link</option>
                            <?php
}

        ?>
                        </select>
                    </div>

                </div>
                </p><br>

                <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block !important;"';
            $show2 = 'style="display:none !important;"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:block !important;"';

        } else {
            $show1 = 'style="display:none !important;"';
            $show2 = 'style="display:none !important;"';
        }
        ?>
                <div id="page_div_property_golf_url" <?php echo $show1; ?>>
                    <div class="flex agilysys-hospitality-solution-widget">
                        <div class="ag-hosp-widget-title-lable">
                            <label for="<?php echo $this->get_field_id('page_property_golf_url'); ?>">
                                <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>
                        </div>
                        <div class="ag-hosp-widget-title-input-text">
                            <select id="<?php echo $this->get_field_id('page_property_golf_url'); ?>"
                                name="<?php echo $this->get_field_name('page_property_golf_url'); ?>">
                                <option value="">Please Select</option>


                                <?php

        $page = $display_instance['page_property_golf_url'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                ?>
                                <option value="<?php echo $key->ID; ?>" selected="selected">
                                    <?php echo $key->post_title; ?>
                                </option>

                                <?php
} else {
                ?>
                                <option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>

                                <?php
}
            ?>

                                <?php
}
        ?>

                            </select>
                        </div>
                    </div>
                </div>


                <script>
                function show_hide_div_property_golf_url(val) {

                    if (val == 'page') {
                        jQuery("#page_div_property_golf_url").show();
                        jQuery("#link_div_property_golf_url").hide();
                    } else if (val == 'link') {
                        jQuery("#page_div_property_golf_url").hide();
                        jQuery("#link_div_property_golf_url").show();
                    }

                }
                </script>

                <div <?php echo $show2; ?> id="link_div_property_golf_url">
                    <div class="flex agilysys-hospitality-solution-widget">
                        <div class="ag-hosp-widget-title-lable">
                            <label
                                for="<?php echo esc_attr($this->get_field_id('property_golf_url')); ?>"><?php esc_attr_e('Property Golf Url', 'ag_hospitality');?>:</label>
                        </div>

                        <div class="ag-hosp-widget-title-input-text">
                            <input class="widefat"
                                id="<?php echo esc_attr($this->get_field_id('property_golf_url')); ?>"
                                name="<?php echo esc_attr($this->get_field_name('property_golf_url')); ?>" type="text"
                                value="<?php echo esc_attr($property_golf_url); ?>">
                        </div>

                    </div>
                </div>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('property_golf_url_title')); ?>"><?php esc_attr_e('Button Title', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('property_golf_url_title')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('property_golf_url_title')); ?>" type="text"
                            value="<?php echo esc_attr($property_golf_url_title); ?>">
                    </div>

                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('property_golf_src')); ?>"><?php esc_attr_e('Property Golf Image', 'ag_hospitality');?>:</label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('property_golf_src')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('property_golf_src')); ?>" type="text"
                            value="<?php echo esc_attr($property_golf_src); ?>">
                        <button
                            class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                    </div>
                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('property_golf_src_alt')); ?>"><?php esc_attr_e('Property Golf Image Alt', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('property_golf_src_alt')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('property_golf_src_alt')); ?>" type="text"
                            value="<?php echo esc_attr($property_golf_src_alt); ?>">
                    </div>

                </div>
                </p>

                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('property_golf_hover_src')); ?>"><?php esc_attr_e('Property Golf Hover Image', 'ag_hospitality');?>:</label>
                    </div>
                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('property_golf_hover_src')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('property_golf_hover_src')); ?>" type="text"
                            value="<?php echo esc_attr($property_golf_hover_src); ?>">
                        <button
                            class="ag_hospitality_upload_btn button"><?php _e('Upload', 'ag_hospitality');?></button>
                    </div>
                </div>
                </p>


                <p>
                <div class="flex agilysys-hospitality-solution-widget">
                    <div class="ag-hosp-widget-title-lable">
                        <label
                            for="<?php echo esc_attr($this->get_field_id('property_golf_hover_src_alt')); ?>"><?php esc_attr_e('Property Golf Hover Image Alt', 'ag_hospitality');?>:</label>
                    </div>

                    <div class="ag-hosp-widget-title-input-text">
                        <input class="widefat"
                            id="<?php echo esc_attr($this->get_field_id('property_golf_hover_src_alt')); ?>"
                            name="<?php echo esc_attr($this->get_field_name('property_golf_hover_src_alt')); ?>"
                            type="text" value="<?php echo esc_attr($property_golf_hover_src_alt); ?>">
                    </div>

                </div>
                </p>



            </div>
        </div>

    </div>

</div>
<div class="col-md-6">

</div>

</div>

<div style="clear:both;"></div>
           

            

              








</div>


<?php
/* echo $args['after_widget']; */
}

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['hospitality_title'] = (!empty($new_instance['hospitality_title'])) ? strip_tags($new_instance['hospitality_title']) : '';

        $instance['guest_title'] = (!empty($new_instance['guest_title'])) ? strip_tags($new_instance['guest_title']) : '';
        $instance['property_title'] = (!empty($new_instance['property_title'])) ? strip_tags($new_instance['property_title']) : '';

        $instance['link_type_booking_url'] = $new_instance['link_type_booking_url'];
        if ($new_instance['link_type_booking_url'] == 'page') {
            $instance['page_booking_url'] = $new_instance['page_booking_url'];
            $instance['booking_url'] = '';
        } elseif ($new_instance['link_type_booking_url'] == 'link') {
            $instance['booking_url'] = $new_instance['booking_url'];
            $instance['page_booking_url'] = '';

        }

        //$instance['booking_url'] = (!empty($new_instance['booking_url'])) ? strip_tags($new_instance['booking_url']) : '';
        $instance['booking_url_title'] = (!empty($new_instance['booking_url_title'])) ? strip_tags($new_instance['booking_url_title']) : '';
        $instance['booking_icon_src'] = (!empty($new_instance['booking_icon_src'])) ? strip_tags($new_instance['booking_icon_src']) : '';
        $instance['booking_icon_src_alt'] = (!empty($new_instance['booking_icon_src_alt'])) ? strip_tags($new_instance['booking_icon_src_alt']) : '';


        $instance['booking_hover_icon_src'] = (!empty($new_instance['booking_hover_icon_src'])) ? strip_tags($new_instance['booking_hover_icon_src']) : '';
        $instance['booking_hover_icon_src_alt'] = (!empty($new_instance['booking_hover_icon_src_alt'])) ? strip_tags($new_instance['booking_hover_icon_src_alt']) : '';



        $instance['link_type_mobile_url'] = $new_instance['link_type_mobile_url'];
        if ($new_instance['link_type_mobile_url'] == 'page') {
            $instance['page_mobile_url'] = $new_instance['page_mobile_url'];
            $instance['mobile_url'] = '';
        } elseif ($new_instance['link_type_mobile_url'] == 'link') {
            $instance['mobile_url'] = $new_instance['mobile_url'];
            $instance['page_mobile_url'] = '';

        }

        //$instance['mobile_url'] = (!empty($new_instance['mobile_url'])) ? strip_tags($new_instance['mobile_url']) : '';
        $instance['mobile_hover_icon_src'] = (!empty($new_instance['mobile_hover_icon_src'])) ? strip_tags($new_instance['mobile_hover_icon_src']) : '';
        $instance['mobile_hover_icon_src_alt'] = (!empty($new_instance['mobile_hover_icon_src_alt'])) ? strip_tags($new_instance['mobile_hover_icon_src_alt']) : '';

        $instance['mobile_url_title'] = (!empty($new_instance['mobile_url_title'])) ? strip_tags($new_instance['mobile_url_title']) : '';
        $instance['mobile_icon_src'] = (!empty($new_instance['mobile_icon_src'])) ? strip_tags($new_instance['mobile_icon_src']) : '';
        $instance['mobile_icon_src_alt'] = (!empty($new_instance['mobile_icon_src_alt'])) ? strip_tags($new_instance['mobile_icon_src_alt']) : '';


        $instance['link_type_golf_url'] = $new_instance['link_type_golf_url'];
        if ($new_instance['link_type_golf_url'] == 'page') {
            $instance['page_golf_url'] = $new_instance['page_golf_url'];
            $instance['golf_url'] = '';
        } elseif ($new_instance['link_type_golf_url'] == 'link') {
            $instance['golf_url'] = $new_instance['golf_url'];
            $instance['page_golf_url'] = '';

        }

        //$instance['golf_url'] = (!empty($new_instance['golf_url'])) ? strip_tags($new_instance['golf_url']) : '';
        $instance['golf_url_title'] = (!empty($new_instance['golf_url_title'])) ? strip_tags($new_instance['golf_url_title']) : '';
        $instance['golf_icon_src'] = (!empty($new_instance['golf_icon_src'])) ? strip_tags($new_instance['golf_icon_src']) : '';
        $instance['golf_icon_src_alt'] = (!empty($new_instance['golf_icon_src_alt'])) ? strip_tags($new_instance['golf_icon_src_alt']) : '';

        $instance['golf_hover_icon_src'] = (!empty($new_instance['golf_hover_icon_src'])) ? strip_tags($new_instance['golf_hover_icon_src']) : '';
        $instance['golf_hover_icon_src_alt'] = (!empty($new_instance['golf_hover_icon_src_alt'])) ? strip_tags($new_instance['golf_hover_icon_src_alt']) : '';


        $instance['link_type_digital_url'] = $new_instance['link_type_digital_url'];
        if ($new_instance['link_type_digital_url'] == 'page') {
            $instance['page_digital_url'] = $new_instance['page_digital_url'];
            $instance['digital_url'] = '';
        } elseif ($new_instance['link_type_digital_url'] == 'link') {
            $instance['digital_url'] = $new_instance['digital_url'];
            $instance['page_digital_url'] = '';

        }

        // $instance['digital_url'] = (!empty($new_instance['digital_url'])) ? strip_tags($new_instance['digital_url']) : '';
        $instance['digital_url_title'] = (!empty($new_instance['digital_url_title'])) ? strip_tags($new_instance['digital_url_title']) : '';
        $instance['digital_icon_src'] = (!empty($new_instance['digital_icon_src'])) ? strip_tags($new_instance['digital_icon_src']) : '';
        $instance['digital_icon_src_alt'] = (!empty($new_instance['digital_icon_src_alt'])) ? strip_tags($new_instance['digital_icon_src_alt']) : '';

        $instance['digital_hover_icon_src'] = (!empty($new_instance['digital_hover_icon_src'])) ? strip_tags($new_instance['digital_hover_icon_src']) : '';
        $instance['digital_hover_icon_src_alt'] = (!empty($new_instance['digital_hover_icon_src_alt'])) ? strip_tags($new_instance['digital_hover_icon_src_alt']) : '';

        $instance['link_type_dining_url'] = $new_instance['link_type_dining_url'];
        if ($new_instance['link_type_dining_url'] == 'page') {
            $instance['page_dining_url'] = $new_instance['page_dining_url'];
            $instance['dining_url'] = '';
        } elseif ($new_instance['link_type_dining_url'] == 'link') {
            $instance['dining_url'] = $new_instance['dining_url'];
            $instance['page_dining_url'] = '';

        }

        //$instance['dining_url'] = (!empty($new_instance['dining_url'])) ? strip_tags($new_instance['dining_url']) : '';
        $instance['dining_url_title'] = (!empty($new_instance['dining_url_title'])) ? strip_tags($new_instance['dining_url_title']) : '';


        $instance['dining_icon_src'] = (!empty($new_instance['dining_icon_src'])) ? strip_tags($new_instance['dining_icon_src']) : '';
        $instance['dining_icon_src_alt'] = (!empty($new_instance['dining_icon_src_alt'])) ? strip_tags($new_instance['dining_icon_src_alt']) : '';

        $instance['dining_hover_icon_src'] = (!empty($new_instance['dining_hover_icon_src'])) ? strip_tags($new_instance['dining_hover_icon_src']) : '';
        $instance['dining_hover_icon_src_alt'] = (!empty($new_instance['dining_hover_icon_src_alt'])) ? strip_tags($new_instance['dining_hover_icon_src_alt']) : '';


        $instance['link_type_payment_url'] = $new_instance['link_type_payment_url'];
        if ($new_instance['link_type_payment_url'] == 'page') {
            $instance['page_payment_url'] = $new_instance['page_payment_url'];
            $instance['payment_url'] = '';
        } elseif ($new_instance['link_type_payment_url'] == 'link') {
            $instance['payment_url'] = $new_instance['payment_url'];
            $instance['page_payment_url'] = '';

        }

        //$instance['payment_url'] = (!empty($new_instance['payment_url'])) ? strip_tags($new_instance['payment_url']) : '';
        $instance['payment_url_title'] = (!empty($new_instance['payment_url_title'])) ? strip_tags($new_instance['payment_url_title']) : '';


        $instance['payment_icon_src'] = (!empty($new_instance['payment_icon_src'])) ? strip_tags($new_instance['payment_icon_src']) : '';
        $instance['payment_icon_src_alt'] = (!empty($new_instance['payment_icon_src_alt'])) ? strip_tags($new_instance['payment_icon_src_alt']) : '';

        $instance['payment_hover_icon_src'] = (!empty($new_instance['payment_hover_icon_src'])) ? strip_tags($new_instance['payment_hover_icon_src']) : '';
        $instance['payment_hover_icon_src_alt'] = (!empty($new_instance['payment_hover_icon_src_alt'])) ? strip_tags($new_instance['payment_hover_icon_src_alt']) : '';

        $instance['spa_url'] = (!empty($new_instance['spa_url'])) ? strip_tags($new_instance['spa_url']) : '';
        $instance['spa_url_title'] = (!empty($new_instance['spa_url_title'])) ? strip_tags($new_instance['spa_url_title']) : '';
        $instance['spa_icon_src'] = (!empty($new_instance['spa_icon_src'])) ? strip_tags($new_instance['spa_icon_src']) : '';
        $instance['spa_icon_src_alt'] = (!empty($new_instance['spa_icon_src_alt'])) ? strip_tags($new_instance['spa_icon_src_alt']) : '';


        $instance['spa_hover_icon_src'] = (!empty($new_instance['spa_hover_icon_src'])) ? strip_tags($new_instance['spa_hover_icon_src']) : '';
        $instance['spa_hover_icon_src_alt'] = (!empty($new_instance['spa_hover_icon_src_alt'])) ? strip_tags($new_instance['spa_hover_icon_src_alt']) : '';


        $instance['link_type_venue_url'] = $new_instance['link_type_venue_url'];
        if ($new_instance['link_type_venue_url'] == 'page') {
            $instance['page_venue_url'] = $new_instance['page_venue_url'];
            $instance['venue_url'] = '';
        } elseif ($new_instance['link_type_venue_url'] == 'link') {
            $instance['venue_url'] = $new_instance['venue_url'];
            $instance['page_venue_url'] = '';

        }
        // $instance['venue_url'] = (!empty($new_instance['venue_url'])) ? strip_tags($new_instance['venue_url']) : '';
        $instance['venue_url_title'] = (!empty($new_instance['venue_url_title'])) ? strip_tags($new_instance['venue_url_title']) : '';


        $instance['venue_icon_src'] = (!empty($new_instance['venue_icon_src'])) ? strip_tags($new_instance['venue_icon_src']) : '';
        $instance['venue_icon_src_alt'] = (!empty($new_instance['venue_icon_src_alt'])) ? strip_tags($new_instance['venue_icon_src_alt']) : '';
        $instance['venue_hover_icon_src'] = (!empty($new_instance['venue_hover_icon_src'])) ? strip_tags($new_instance['venue_hover_icon_src']) : '';
        $instance['venue_hover_icon_src_alt'] = (!empty($new_instance['venue_hover_icon_src_alt'])) ? strip_tags($new_instance['venue_hover_icon_src_alt']) : '';

        $instance['link_type_mobile_check_url'] = $new_instance['link_type_mobile_check_url'];
        if ($new_instance['link_type_mobile_check_url'] == 'page') {
            $instance['page_mobile_check_url'] = $new_instance['page_mobile_check_url'];
            $instance['mobile_check_url'] = '';
        } elseif ($new_instance['link_type_mobile_check_url'] == 'link') {
            $instance['mobile_check_url'] = $new_instance['mobile_check_url'];
            $instance['page_mobile_check_url'] = '';

        }

        // $instance['mobile_check_url'] = (!empty($new_instance['mobile_check_url'])) ? strip_tags($new_instance['mobile_check_url']) : '';
        $instance['mobile_check_url_title'] = (!empty($new_instance['mobile_check_url_title'])) ? strip_tags($new_instance['mobile_check_url_title']) : '';


        $instance['mobile_check_icon_src'] = (!empty($new_instance['mobile_check_icon_src'])) ? strip_tags($new_instance['mobile_check_icon_src']) : '';
        $instance['mobile_check_icon_src_alt'] = (!empty($new_instance['mobile_check_icon_src_alt'])) ? strip_tags($new_instance['mobile_check_icon_src_alt']) : '';
        $instance['mobile_check_hover_icon_src'] = (!empty($new_instance['mobile_check_hover_icon_src'])) ? strip_tags($new_instance['mobile_check_hover_icon_src']) : '';
        $instance['mobile_check_hover_icon_src_alt'] = (!empty($new_instance['mobile_check_hover_icon_src_alt'])) ? strip_tags($new_instance['mobile_check_hover_icon_src_alt']) : '';

//        property 1st row

        $instance['link_type_digital_marketing_url'] = $new_instance['link_type_digital_marketing_url'];
        if ($new_instance['link_type_digital_marketing_url'] == 'page') {
            $instance['page_digital_marketing_url'] = $new_instance['page_digital_marketing_url'];
            $instance['digital_marketing_url'] = '';
        } elseif ($new_instance['link_type_digital_marketing_url'] == 'link') {
            $instance['digital_marketing_url'] = $new_instance['digital_marketing_url'];
            $instance['page_digital_marketing_url'] = '';

        }

        $instance['digital_marketing_src'] = (!empty($new_instance['digital_marketing_src'])) ? strip_tags($new_instance['digital_marketing_src']) : '';
        $instance['digital_marketing_src_alt'] = (!empty($new_instance['digital_marketing_src_alt'])) ? strip_tags($new_instance['digital_marketing_src_alt']) : '';

        $instance['digital_marketing_hover_src'] = (!empty($new_instance['digital_marketing_hover_src'])) ? strip_tags($new_instance['digital_marketing_hover_src']) : '';
        $instance['digital_marketing_hover_src_alt'] = (!empty($new_instance['digital_marketing_hover_src_alt'])) ? strip_tags($new_instance['digital_marketing_hover_src_alt']) : '';

        //$instance['digital_marketing_url'] = (!empty($new_instance['digital_marketing_url'])) ? strip_tags($new_instance['digital_marketing_url']) : '';
        $instance['digital_marketing_url_title'] = (!empty($new_instance['digital_marketing_url_title'])) ? strip_tags($new_instance['digital_marketing_url_title']) : '';

        $instance['link_type_property_booking_url'] = $new_instance['link_type_property_booking_url'];
        if ($new_instance['link_type_property_booking_url'] == 'page') {
            $instance['page_property_booking_url'] = $new_instance['page_property_booking_url'];
            $instance['property_booking_url'] = '';
        } elseif ($new_instance['link_type_property_booking_url'] == 'link') {
            $instance['property_booking_url'] = $new_instance['property_booking_url'];
            $instance['page_property_booking_url'] = '';

        }

        //$instance['property_booking_url'] = (!empty($new_instance['property_booking_url'])) ? strip_tags($new_instance['property_booking_url']) : '';
        $instance['property_booking_url_title'] = (!empty($new_instance['property_booking_url_title'])) ? strip_tags($new_instance['property_booking_url_title']) : '';



        $instance['property_booking_icon_src'] = (!empty($new_instance['property_booking_icon_src'])) ? strip_tags($new_instance['property_booking_icon_src']) : '';
        $instance['property_booking_icon_src_alt'] = (!empty($new_instance['property_booking_icon_src_alt'])) ? strip_tags($new_instance['property_booking_icon_src_alt']) : '';

        $instance['property_booking_hover_icon_src'] = (!empty($new_instance['property_booking_hover_icon_src'])) ? strip_tags($new_instance['property_booking_hover_icon_src']) : '';
        $instance['property_booking_hover_icon_src_alt'] = (!empty($new_instance['property_booking_hover_icon_src_alt'])) ? strip_tags($new_instance['property_booking_hover_icon_src_alt']) : '';

        $instance['link_type_property_mgmt_url'] = $new_instance['link_type_property_mgmt_url'];
        if ($new_instance['link_type_property_mgmt_url'] == 'page') {
            $instance['page_property_mgmt_url'] = $new_instance['page_property_mgmt_url'];
            $instance['property_mgmt_url'] = '';
        } elseif ($new_instance['link_type_property_mgmt_url'] == 'link') {
            $instance['property_mgmt_url'] = $new_instance['property_mgmt_url'];
            $instance['page_property_mgmt_url'] = '';

        }
//
        //$instance['property_mgmt_url'] = (!empty($new_instance['property_mgmt_url'])) ? strip_tags($new_instance['property_mgmt_url']) : '';
        $instance['property_mgmt_url_title'] = (!empty($new_instance['property_mgmt_url_title'])) ? strip_tags($new_instance['property_mgmt_url_title']) : '';


        $instance['property_mgmt_icon_src'] = (!empty($new_instance['property_mgmt_icon_src'])) ? strip_tags($new_instance['property_mgmt_icon_src']) : '';
        $instance['property_mgmt_icon_src_alt'] = (!empty($new_instance['property_mgmt_icon_src_alt'])) ? strip_tags($new_instance['property_mgmt_icon_src_alt']) : '';

        $instance['property_mgmt_hover_icon_src'] = (!empty($new_instance['property_mgmt_hover_icon_src'])) ? strip_tags($new_instance['property_mgmt_hover_icon_src']) : '';
        $instance['property_mgmt_hover_icon_src_alt'] = (!empty($new_instance['property_mgmt_hover_icon_src_alt'])) ? strip_tags($new_instance['property_mgmt_hover_icon_src_alt']) : '';
        //        property 2nd row

        $instance['link_type_pos_url'] = $new_instance['link_type_pos_url'];
        if ($new_instance['link_type_pos_url'] == 'page') {
            $instance['page_pos_url'] = $new_instance['page_pos_url'];
            $instance['pos_url'] = '';
        } elseif ($new_instance['link_type_pos_url'] == 'link') {
            $instance['pos_url'] = $new_instance['pos_url'];
            $instance['page_pos_url'] = '';

        }

        //$instance['pos_url'] = (!empty($new_instance['pos_url'])) ? strip_tags($new_instance['pos_url']) : '';
        $instance['pos_url_title'] = (!empty($new_instance['pos_url_title'])) ? strip_tags($new_instance['pos_url_title']) : '';


        $instance['pos_icon_src'] = (!empty($new_instance['pos_icon_src'])) ? strip_tags($new_instance['pos_icon_src']) : '';
        $instance['pos_icon_src_alt'] = (!empty($new_instance['pos_icon_src_alt'])) ? strip_tags($new_instance['pos_icon_src_alt']) : '';
        $instance['pos_hover_icon_src'] = (!empty($new_instance['pos_hover_icon_src'])) ? strip_tags($new_instance['pos_hover_icon_src']) : '';
        $instance['pos_hover_icon_src_alt'] = (!empty($new_instance['pos_hover_icon_src_alt'])) ? strip_tags($new_instance['pos_hover_icon_src_alt']) : '';

        $instance['link_type_inventory_url'] = $new_instance['link_type_inventory_url'];
        if ($new_instance['link_type_inventory_url'] == 'page') {
            $instance['page_inventory_url'] = $new_instance['page_inventory_url'];
            $instance['inventory_url'] = '';
        } elseif ($new_instance['link_type_inventory_url'] == 'link') {
            $instance['inventory_url'] = $new_instance['inventory_url'];
            $instance['page_inventory_url'] = '';

        }
//
        //
        //$instance['inventory_url'] = (!empty($new_instance['inventory_url'])) ? strip_tags($new_instance['inventory_url']) : '';
        $instance['inventory_url_title'] = (!empty($new_instance['inventory_url_title'])) ? strip_tags($new_instance['inventory_url_title']) : '';
        $instance['inventory_icon_src'] = (!empty($new_instance['inventory_icon_src'])) ? strip_tags($new_instance['inventory_icon_src']) : '';
        $instance['inventory_icon_src_alt'] = (!empty($new_instance['inventory_icon_src_alt'])) ? strip_tags($new_instance['inventory_icon_src_alt']) : '';

        $instance['inventory_hover_icon_src'] = (!empty($new_instance['inventory_hover_icon_src'])) ? strip_tags($new_instance['inventory_hover_icon_src']) : '';
        $instance['inventory_hover_icon_src_alt'] = (!empty($new_instance['inventory_hover_icon_src_alt'])) ? strip_tags($new_instance['inventory_hover_icon_src_alt']) : '';

        $instance['link_type_document_url'] = $new_instance['link_type_document_url'];
        if ($new_instance['link_type_document_url'] == 'page') {
            $instance['page_document_url'] = $new_instance['page_document_url'];
            $instance['document_url'] = '';
        } elseif ($new_instance['link_type_document_url'] == 'link') {
            $instance['document_url'] = $new_instance['document_url'];
            $instance['page_document_url'] = '';

        }
//
        //
        //$instance['document_url'] = (!empty($new_instance['document_url'])) ? strip_tags($new_instance['document_url']) : '';
        $instance['document_url_title'] = (!empty($new_instance['document_url_title'])) ? strip_tags($new_instance['document_url_title']) : '';


        $instance['document_icon_src'] = (!empty($new_instance['document_icon_src'])) ? strip_tags($new_instance['document_icon_src']) : '';
        $instance['document_icon_src_alt'] = (!empty($new_instance['document_icon_src_alt'])) ? strip_tags($new_instance['document_icon_src_alt']) : '';

        $instance['document_hover_icon_src'] = (!empty($new_instance['document_hover_icon_src'])) ? strip_tags($new_instance['document_hover_icon_src']) : '';
        $instance['document_hover_icon_src_alt'] = (!empty($new_instance['document_hover_icon_src_alt'])) ? strip_tags($new_instance['document_hover_icon_src_alt']) : '';

        $instance['link_type_prporty_spa_url'] = $new_instance['link_type_prporty_spa_url'];
        if ($new_instance['link_type_prporty_spa_url'] == 'page') {
            $instance['page_prporty_spa_url'] = $new_instance['page_prporty_spa_url'];
            $instance['prporty_spa_url'] = '';
        } elseif ($new_instance['link_type_prporty_spa_url'] == 'link') {
            $instance['prporty_spa_url'] = $new_instance['prporty_spa_url'];
            $instance['page_prporty_spa_url'] = '';

        }
//
        //$instance['prporty_spa_url'] = (!empty($new_instance['prporty_spa_url'])) ? strip_tags($new_instance['prporty_spa_url']) : '';
        $instance['prporty_spa_url_title'] = (!empty($new_instance['prporty_spa_url_title'])) ? strip_tags($new_instance['prporty_spa_url_title']) : '';


        $instance['prporty_spa_icon_src'] = (!empty($new_instance['prporty_spa_icon_src'])) ? strip_tags($new_instance['prporty_spa_icon_src']) : '';
        $instance['prporty_spa_icon_src_alt'] = (!empty($new_instance['prporty_spa_icon_src_alt'])) ? strip_tags($new_instance['prporty_spa_icon_src_alt']) : '';
        
        $instance['prporty_spa_hover_icon_src'] = (!empty($new_instance['prporty_spa_hover_icon_src'])) ? strip_tags($new_instance['prporty_spa_hover_icon_src']) : '';
        $instance['prporty_spa_hover_icon_src_alt'] = (!empty($new_instance['prporty_spa_hover_icon_src_alt'])) ? strip_tags($new_instance['prporty_spa_hover_icon_src_alt']) : '';
        //        proeprty 2nd row ends

//        proeprty 3rd row

        $instance['link_type_prporty_digital_key_url'] = $new_instance['link_type_prporty_digital_key_url'];
        if ($new_instance['link_type_prporty_digital_key_url'] == 'page') {
            $instance['page_prporty_digital_key_url'] = $new_instance['page_prporty_digital_key_url'];
            $instance['prporty_digital_key_url'] = '';
        } elseif ($new_instance['link_type_prporty_digital_key_url'] == 'link') {
            $instance['prporty_digital_key_url'] = $new_instance['prporty_digital_key_url'];
            $instance['page_prporty_digital_key_url'] = '';

        }

        //$instance['prporty_digital_key_url'] = (!empty($new_instance['prporty_digital_key_url'])) ? strip_tags($new_instance['prporty_digital_key_url']) : '';
        $instance['prporty_digital_key_url_title'] = (!empty($new_instance['prporty_digital_key_url_title'])) ? strip_tags($new_instance['prporty_digital_key_url_title']) : '';
        $instance['prporty_digital_key_url_icon_src'] = (!empty($new_instance['prporty_digital_key_url_icon_src'])) ? strip_tags($new_instance['prporty_digital_key_url_icon_src']) : '';
        $instance['prporty_digital_key_url_icon_src_alt'] = (!empty($new_instance['prporty_digital_key_url_icon_src_alt'])) ? strip_tags($new_instance['prporty_digital_key_url_icon_src_alt']) : '';


        $instance['prporty_digital_key_url_icon_hover_icon_src'] = (!empty($new_instance['prporty_digital_key_url_icon_hover_icon_src'])) ? strip_tags($new_instance['prporty_digital_key_url_icon_hover_icon_src']) : '';

        $instance['prporty_digital_key_url_icon_hover_icon_src_alt'] = (!empty($new_instance['prporty_digital_key_url_icon_hover_icon_src_alt'])) ? strip_tags($new_instance['prporty_digital_key_url_icon_hover_icon_src_alt']) : '';

        $instance['link_type_prporty_mobile_checkin_key_url'] = $new_instance['link_type_prporty_mobile_checkin_key_url'];
        if ($new_instance['link_type_prporty_mobile_checkin_key_url'] == 'page') {
            $instance['page_prporty_mobile_checkin_key_url'] = $new_instance['page_prporty_mobile_checkin_key_url'];
            $instance['prporty_mobile_checkin_key_url'] = '';
        } elseif ($new_instance['link_type_prporty_mobile_checkin_key_url'] == 'link') {
            $instance['prporty_mobile_checkin_key_url'] = $new_instance['prporty_mobile_checkin_key_url'];
            $instance['page_prporty_mobile_checkin_key_url'] = '';

        }

        //$instance['prporty_mobile_checkin_key_url'] = (!empty($new_instance['prporty_mobile_checkin_key_url'])) ? strip_tags($new_instance['prporty_mobile_checkin_key_url']) : '';
        $instance['prporty_mobile_checkin_key_url_title'] = (!empty($new_instance['prporty_mobile_checkin_key_url_title'])) ? strip_tags($new_instance['prporty_mobile_checkin_key_url_title']) : '';


        $instance['prporty_mobile_checkin_icon_src'] = (!empty($new_instance['prporty_mobile_checkin_icon_src'])) ? strip_tags($new_instance['prporty_mobile_checkin_icon_src']) : '';
        $instance['prporty_mobile_checkin_icon_src_alt'] = (!empty($new_instance['prporty_mobile_checkin_icon_src_alt'])) ? strip_tags($new_instance['prporty_mobile_checkin_icon_src_alt']) : '';

       
        $instance['prporty_mobile_checkin_hover_src'] = (!empty($new_instance['prporty_mobile_checkin_hover_src'])) ? strip_tags($new_instance['prporty_mobile_checkin_hover_src']) : '';
        $instance['prporty_mobile_checkin_hover_src_alt'] = (!empty($new_instance['prporty_mobile_checkin_hover_src_alt'])) ? strip_tags($new_instance['prporty_mobile_checkin_hover_src_alt']) : '';


        $instance['link_type_prporty_analytics_url'] = $new_instance['link_type_prporty_analytics_url'];
        if ($new_instance['link_type_prporty_analytics_url'] == 'page') {
            $instance['page_prporty_analytics_url'] = $new_instance['page_prporty_analytics_url'];
            $instance['prporty_analytics_url'] = '';
        } elseif ($new_instance['link_type_prporty_analytics_url'] == 'link') {
            $instance['prporty_analytics_url'] = $new_instance['prporty_analytics_url'];
            $instance['page_prporty_analytics_url'] = '';

        }

        //$instance['prporty_analytics_url'] = (!empty($new_instance['prporty_analytics_url'])) ? strip_tags($new_instance['prporty_analytics_url']) : '';
        $instance['prporty_analytics_url_title'] = (!empty($new_instance['prporty_analytics_url_title'])) ? strip_tags($new_instance['prporty_analytics_url_title']) : '';
        $instance['prporty_analytics_icon_src'] = (!empty($new_instance['prporty_analytics_icon_src'])) ? strip_tags($new_instance['prporty_analytics_icon_src']) : '';
        $instance['prporty_analytics_icon_src_alt'] = (!empty($new_instance['prporty_analytics_icon_src_alt'])) ? strip_tags($new_instance['prporty_analytics_icon_src_alt']) : '';



        $instance['prporty_analytics_icon_hover_src'] = (!empty($new_instance['prporty_analytics_icon_hover_src'])) ? strip_tags($new_instance['prporty_analytics_icon_hover_src']) : '';
        $instance['prporty_analytics_icon_hover_src_alt'] = (!empty($new_instance['prporty_analytics_icon_hover_src_alt'])) ? strip_tags($new_instance['prporty_analytics_icon_hover_src_alt']) : '';

        $instance['link_type_prporty_services_url'] = $new_instance['link_type_prporty_services_url'];
        if ($new_instance['link_type_prporty_services_url'] == 'page') {
            $instance['page_prporty_services_url'] = $new_instance['page_prporty_services_url'];
            $instance['prporty_services_url'] = '';
        } elseif ($new_instance['link_type_prporty_services_url'] == 'link') {
            $instance['prporty_services_url'] = $new_instance['prporty_services_url'];
            $instance['page_prporty_services_url'] = '';

        }

        //$instance['prporty_services_url'] = (!empty($new_instance['prporty_services_url'])) ? strip_tags($new_instance['prporty_services_url']) : '';
        $instance['prporty_services_url_title'] = (!empty($new_instance['prporty_services_url_title'])) ? strip_tags($new_instance['prporty_services_url_title']) : '';
        $instance['prporty_services_icon_src'] = (!empty($new_instance['prporty_services_icon_src'])) ? strip_tags($new_instance['prporty_services_icon_src']) : '';
        $instance['prporty_services_icon_src_alt'] = (!empty($new_instance['prporty_services_icon_src_alt'])) ? strip_tags($new_instance['prporty_services_icon_src_alt']) : '';

        $instance['prporty_services_icon_hover_src'] = (!empty($new_instance['prporty_services_icon_hover_src'])) ? strip_tags($new_instance['prporty_services_icon_hover_src']) : '';
        $instance['prporty_services_icon_hover_src_alt'] = (!empty($new_instance['prporty_services_icon_hover_src_alt'])) ? strip_tags($new_instance['prporty_services_icon_hover_src_alt']) : '';


        $instance['link_type_prporty_self_services_url'] = $new_instance['link_type_prporty_self_services_url'];
        if ($new_instance['link_type_prporty_self_services_url'] == 'page') {
            $instance['page_prporty_self_services_url'] = $new_instance['page_prporty_self_services_url'];
            $instance['prporty_self_services_url'] = '';
        } elseif ($new_instance['link_type_prporty_self_services_url'] == 'link') {
            $instance['prporty_self_services_url'] = $new_instance['prporty_self_services_url'];
            $instance['page_prporty_self_services_url'] = '';

        }

        //$instance['prporty_self_services_url'] = (!empty($new_instance['prporty_self_services_url'])) ? strip_tags($new_instance['prporty_self_services_url']) : '';
        $instance['prporty_self_services_url_title'] = (!empty($new_instance['prporty_self_services_url_title'])) ? strip_tags($new_instance['prporty_self_services_url_title']) : '';
        $instance['prporty_digital_payment_src_icon_src'] = (!empty($new_instance['prporty_digital_payment_src_icon_src'])) ? strip_tags($new_instance['prporty_digital_payment_src_icon_src']) : '';

        $instance['prporty_digital_payment_src_icon_src_alt'] = (!empty($new_instance['prporty_digital_payment_src_icon_src_alt'])) ? strip_tags($new_instance['prporty_digital_payment_src_icon_src_alt']) : '';

        $instance['prporty_self_services_icon_hover_src'] = (!empty($new_instance['prporty_self_services_icon_hover_src'])) ? strip_tags($new_instance['prporty_self_services_icon_hover_src']) : '';

        $instance['prporty_self_services_icon_hover_src_alt'] = (!empty($new_instance['prporty_self_services_icon_hover_src_alt'])) ? strip_tags($new_instance['prporty_self_services_icon_hover_src_alt']) : '';

        $instance['link_type_prporty_digital_payment_url'] = $new_instance['link_type_prporty_digital_payment_url'];
        if ($new_instance['link_type_prporty_digital_payment_url'] == 'page') {
            $instance['page_prporty_digital_payment_url'] = $new_instance['page_prporty_digital_payment_url'];
            $instance['prporty_digital_payment_url'] = '';
        } elseif ($new_instance['link_type_prporty_digital_payment_url'] == 'link') {
            $instance['prporty_digital_payment_url'] = $new_instance['prporty_digital_payment_url'];
            $instance['page_prporty_digital_payment_url'] = '';

        }

        //$instance['prporty_digital_payment_url'] = (!empty($new_instance['prporty_digital_payment_url'])) ? strip_tags($new_instance['prporty_digital_payment_url']) : '';
        $instance['prporty_digital_payment_url_title'] = (!empty($new_instance['prporty_digital_payment_url_title'])) ? strip_tags($new_instance['prporty_digital_payment_url_title']) : '';
        $instance['prporty_self_services_icon_src'] = (!empty($new_instance['prporty_self_services_icon_src'])) ? strip_tags($new_instance['prporty_self_services_icon_src']) : '';
        $instance['prporty_self_services_icon_src_alt'] = (!empty($new_instance['prporty_self_services_icon_src_alt'])) ? strip_tags($new_instance['prporty_self_services_icon_src_alt']) : '';

        
        
        $instance['prporty_digital_payment_src_icon_hover_src'] = (!empty($new_instance['prporty_digital_payment_src_icon_hover_src'])) ? strip_tags($new_instance['prporty_digital_payment_src_icon_hover_src']) : '';
        $instance['prporty_digital_payment_src_icon_hover_src_alt'] = (!empty($new_instance['prporty_digital_payment_src_icon_hover_src_alt'])) ? strip_tags($new_instance['prporty_digital_payment_src_icon_hover_src_alt']) : '';

//          property 4th row

        $instance['link_type_property_sales_catering_url'] = $new_instance['link_type_property_sales_catering_url'];
        if ($new_instance['link_type_property_sales_catering_url'] == 'page') {
            $instance['page_property_sales_catering_url'] = $new_instance['page_property_sales_catering_url'];
            $instance['property_sales_catering_url'] = '';
        } elseif ($new_instance['link_type_property_sales_catering_url'] == 'link') {
            $instance['property_sales_catering_url'] = $new_instance['property_sales_catering_url'];
            $instance['page_property_sales_catering_url'] = '';

        }

        //$instance['property_sales_catering_url'] = (!empty($new_instance['property_sales_catering_url'])) ? strip_tags($new_instance['property_sales_catering_url']) : '';
        $instance['property_sales_catering_url_title'] = (!empty($new_instance['property_sales_catering_url_title'])) ? strip_tags($new_instance['property_sales_catering_url_title']) : '';
        $instance['property_sales_catering_icon_src'] = (!empty($new_instance['property_sales_catering_icon_src'])) ? strip_tags($new_instance['property_sales_catering_icon_src']) : '';
        $instance['property_sales_catering_icon_src_alt'] = (!empty($new_instance['property_sales_catering_icon_src_alt'])) ? strip_tags($new_instance['property_sales_catering_icon_src_alt']) : '';

        $instance['property_sales_catering_icon_hover_src'] = (!empty($new_instance['property_sales_catering_icon_hover_src'])) ? strip_tags($new_instance['property_sales_catering_icon_hover_src']) : '';
        $instance['property_sales_catering_icon_hover_src_alt'] = (!empty($new_instance['property_sales_catering_icon_hover_src_alt'])) ? strip_tags($new_instance['property_sales_catering_icon_hover_src_alt']) : '';


        $instance['link_type_property_golf_url'] = $new_instance['link_type_property_golf_url'];
        if ($new_instance['link_type_property_golf_url'] == 'page') {
            $instance['page_property_golf_url'] = $new_instance['page_property_golf_url'];
            $instance['property_golf_url'] = '';
        } elseif ($new_instance['link_type_property_golf_url'] == 'link') {
            $instance['property_golf_url'] = $new_instance['property_golf_url'];
            $instance['page_property_golf_url'] = '';

        }

        //$instance['property_golf_url'] = (!empty($new_instance['property_golf_url'])) ? strip_tags($new_instance['property_golf_url']) : '';
        $instance['property_golf_url_title'] = (!empty($new_instance['property_golf_url_title'])) ? strip_tags($new_instance['property_golf_url_title']) : '';
        $instance['property_golf_src'] = (!empty($new_instance['property_golf_src'])) ? strip_tags($new_instance['property_golf_src']) : '';
        $instance['property_golf_src_alt'] = (!empty($new_instance['property_golf_src_alt'])) ? strip_tags($new_instance['property_golf_src_alt']) : '';

        $instance['property_golf_hover_src'] = (!empty($new_instance['property_golf_hover_src'])) ? strip_tags($new_instance['property_golf_hover_src']) : '';
        $instance['property_golf_hover_src_alt'] = (!empty($new_instance['property_golf_hover_src_alt'])) ? strip_tags($new_instance['property_golf_hover_src_alt']) : '';
        return $instance;
    }
}
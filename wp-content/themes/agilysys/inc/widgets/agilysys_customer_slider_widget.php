<?php

function agilysys_customer_slider_widgets()
{
    register_widget('agilysys_customer_slider_widget');
}

add_action('widgets_init', 'agilysys_customer_slider_widgets');

class agilysys_customer_slider_widget extends WP_Widget
{
    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {

        parent::__construct(false, $name = __('Agilysys Customer Single Slider Widget', 'agilysys_text_domain'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
        add_action('load-widgets.php', array(&$this, 'agilysys_color_picker_load'));

    }

    public function agilysys_color_picker_load()
    {
        wp_enqueue_style('wp-color-picker');
        wp_enqueue_script('wp-color-picker');
    }

    /**
     * @see WP_Widget::widget -- do not rename this         * This is for front end
     */
    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        extract($args);

        $max_entries_agilysys_customer_slider_widget_hospitality_software = 25;

        $bg_color = !empty($instance['background_color']) ? $instance['background_color'] : '';

        ?>


<!-- Slider main container -->

<section class="cusStoriesSlider">
    <div class="swiper-container">
        <div class="swiper-wrapper">

            <?php

        $count = count($instance['leadersDesc']);
        for ($i = 0; $i < $count; $i++) {

            $type = $instance['type'][$i];
            $image_uri = $instance['image_uri'][$i];
            $image_uri_alt = $instance['image_uri_alt'][$i];
            $leadersDesc = $instance['leadersDesc'][$i];
//            $leadersDesc2 = $instance['leadersDesc2-'.$i];
            $leadersDesc5 = $instance['leadersDesc5'][$i];
            $leadersDesc4 = $instance['leadersDesc4'][$i];

            $link_type = $instance['link_type'][$i];
            if ($link_type == "link") {
                $leadersDesc6 = $instance['leadersDesc6'][$i];
            } elseif ($link_type == "page") {
                $post_id = $instance['page'][$i];
                $post = get_post($post_id);
                $leadersDesc6 = home_url($post->post_name) . "/";
            }

            $customer_single_video_slider_link = $instance['customer_single_video_slider_link'][$i];

            $link_typea = $instance['link_typea'][$i];
            if ($link_typea == "link") {
                $customer_single_video_slider_link = $instance['customer_single_video_slider_link'][$i];
            } elseif ($link_typea == "page") {
                $post_id = $instance['pagea'][$i];
                $post = get_post($post_id);
                $customer_single_video_slider_link = home_url($post->post_name) . "/";
            }

            $customer_single_video_slider_link_title = $instance['customer_single_video_slider_link_title'][$i];
            $customer_single_video_slider_desc = $instance['customer_single_video_slider_desc'][$i];
            $customer_single_video_slider_endorser = $instance['customer_single_video_slider_endorser'][$i];
            $video_uri = $instance['video_uri'][$i];

            $youtube_uri = $instance['youtube_uri'][$i];
            ?>

            <div class="swiper-slide">

                <?php

            if ($type == "image") {
                ?>
                <div class="cusStoriesSlide flex">
                    <div class="cusStoriesSlideImg">
                        <img class="img-fluid" src="<?php echo $image_uri; ?>" alt="<?php echo $image_uri_alt; ?>" />
                    </div>
                    <div class="cusStoriesSlideContent">
                        <h3 class="dinProStd whiteText"><?php echo $leadersDesc; ?></h3>
                        <p class="whiteText"><?php echo ($leadersDesc4); ?></p>
                        <a class="homeBannerButton whiteText" href="<?php echo $leadersDesc6; ?>">
                            <?php echo $leadersDesc5; ?> <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                    </div>
                </div>
                <?php
}
            ?>

                <?php

            if ($type == "video") {
                ?>
                <div class="cusStoriesSlide flex">

                    <video width="100%" height="100%" autoplay muted>
                        <source src="<?php echo $video_uri; ?>" type="video/mp4">
                    </video>
                    <div class="cusStoriesSlideContent">
                        <h3 class="dinProStd whiteText"><?php echo $customer_single_video_slider_desc; ?></h3>
                        <p class="whiteText"><?php echo ($customer_single_video_slider_endorser); ?></p>
                        <a class="homeBannerButton whiteText" href="<?php echo $customer_single_video_slider_link; ?>">
                            <?php echo $customer_single_video_slider_link_title; ?> <i class="fa fa-arrow-right"
                                aria-hidden="true"></i></a>
                    </div>
                </div>
                <?php
}
            ?>
            </div>
            <?php

        }
        ?>
        </div>
        <!-- Add Arrows -->
        <div class="cusSliderButton">
            <div class="swiper-button-next swiper-button-disabled" tabindex="0" role="button" aria-label="Next slide"
                aria-disabled="true">
                <img class="img-fluid"
                    src="/wp-content/themes/agilysys/img/solution/sloution-slider-next-arrow.png"
                    alt="">
            </div>
            <div class="swiper-button-prev swiper-button-disabled" tabindex="0" role="button"
                aria-label="Previous slide" aria-disabled="true">
                <img class="img-fluid"
                    src="/wp-content/themes/agilysys/img/solution/sloution-slider-prev-arrow.png"
                    alt="">
            </div>
        </div>
    </div>
</section>

<?php
echo $args['after_widget'];
    }
    //Function widget ends here

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['background_color'] = $new_instance['background_color'];

        $count = count($new_instance['leadersDesc']);

        for ($i = 0; $i < $count; $i++) {

            $instance['type'][$i] = $new_instance['type'][$i];
            $instance['leadersDesc'][$i] = $new_instance['leadersDesc'][$i];

            $instance['leadersDesc4'][$i] = $new_instance['leadersDesc4'][$i];
            $instance['leadersDesc5'][$i] = $new_instance['leadersDesc5'][$i];

            $instance['link_type'][$i] = $new_instance['link_type'][$i];
            if ($new_instance['link_type'][$i] == 'page') {
                $instance['page'][$i] = $new_instance['page'][$i];
                $instance['leadersDesc6'][$i] = '';
            } elseif ($new_instance['link_type'][$i] == 'link') {
                $instance['leadersDesc6'][$i] = $new_instance['leadersDesc6'][$i];
                $instance['page'][$i] = '';

            }
            $instance['leadersDesc7'][$i] = $new_instance['leadersDesc7'][$i];
            $instance['image_uri'][$i] = $new_instance['image_uri'][$i];

            $instance['image_uri_alt'][$i] = $new_instance['image_uri_alt'][$i];
            $instance['video_uri'][$i] = $new_instance['video_uri'][$i];
            $instance['youtube_uri'][$i] = $new_instance['youtube_uri'][$i];

            $instance['customer_single_video_slider_desc'][$i] = $new_instance['customer_single_video_slider_desc'][$i];
            $instance['customer_single_video_slider_endorser'][$i] = $new_instance['customer_single_video_slider_endorser'][$i];
            $instance['customer_single_video_slider_link_title'][$i] = $new_instance['customer_single_video_slider_link_title'][$i];

            $instance['link_typea'][$i] = $new_instance['link_typea'][$i];
            if ($new_instance['link_typea'][$i] == 'page') {
                $instance['pagea'][$i] = $new_instance['pagea'][$i];
                $instance['customer_single_video_slider_link'][$i] = '';
            } elseif ($new_instance['link_typea'][$i] == 'link') {
                $instance['customer_single_video_slider_link'][$i] = $new_instance['customer_single_video_slider_link'][$i];
                $instance['pagea'][$i] = '';

            }

        }
        return $instance;
    }

    public function form($display_instance)
    {

        ?>
<script>
jQuery(document).ready(function($) {
    $('.my-color-picker').wpColorPicker();
});
</script>
<?php

        $max_entries_agilysys_customer_slider_widget_hospitality_software = 25;

        $widget_add_id_hosipitality_software = $this->get_field_id('') . "add_agilysys_customer_slider_widget";
        $review_title = ($display_instance['review_title']);
        $contentDesc = ($display_instance['content']);
        $background_color = ($display_instance['background_color']);

        $rew_html .= '<label for="' . $this->get_field_id('background_color') . '"> ' . __('Color', 'agilysys_text_domain') . ' :</label>';
        $rew_html .= '<input  class="my-color-picker" id="' . $this->get_field_id('background_color') . '" name="' . $this->get_field_name('background_color') . '" type="text" value="' . $background_color . '" />';
        $rew_html .= '</p>';

        //get the count of total number of rows of array item

        $count = count($display_instance['leadersDesc']);

        $rew_html .= '<div class="add_new_rowxx-input-containers"><div id="entries_agilysys_customer_slider_widget">';

        //store the count variable in hidden field and store all element names as array inside the lopp

        $rew_html .= '<input class="cnt909" id="cnt909" name="cnt" type="hidden" value="' . $count . '">';

        for ($i = 0; $i < $count; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'agilysys_text_domain') . ' </span>';
            $rew_html .= '<div class="entry-desc cf">';

            /**
             * Block Caption
             */

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('type' . $i) . '"> ' . __('Media Type', 'agilysys_text_domain') . ' :</label>';
            $rew_html .= '<select name="' . $this->get_field_name('type[]') . '" id="' . $this->get_field_id('type' . $i) . '" onChange="show_hide_media_agilysys_customer_slider_widget(this.value,' . $i . ');">';
            $rew_html .= '<option value="">Please Select</option>';

            if ($display_instance['type'][$i] == "image") {
                $rew_html .= '<option value="image" selected="selected">Image</option>';
            } else {
                $rew_html .= '<option value="image">Image</option>';
            }

            if ($display_instance['type'][$i] == "video") {
                $rew_html .= '<option value="video" selected="selected">Video</option>';
            } else {
                $rew_html .= '<option value="video">Video</option>';
            }

            if ($display_instance['type'][$i] == "youtube") {
                $rew_html .= '<option value="youtube" selected="selected">Youtube url</option>';
            } else {
                $rew_html .= '<option value="youtube">Youtube url</option>';
            }

            $rew_html .= '</select>';
            $rew_html .= '</p>';

            $show1 = (!empty($display_instance['image_uri'][$i]) && $display_instance['type'][$i] == "image") ? '' : 'style="display:none;"';
            $rew_html .= '<div class="widg-img' . $i . '" ' . $show1 . '>';
            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('image_id' . $i) . '"> ' . __('Image', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<label id="image_uri_agilysys_customer_slider_widget' . $i . '"></label><br><img class="' . $this->get_field_id('image_id' . $i) . '_media_image' . $i . ' custom_media_image' . $i . '" src="' . $display_instance['image_uri'][$i] . '" ' . $show1 . ' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id' . $i) . '_media_id' . $i . ' custom_media_id' . $i . '" name="' . $this->get_field_name('image_id[]') . '" id="' . $this->get_field_id('image_id' . $i) . '" value="' . $display_instance['image_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id' . $i) . '_media_url' . $i . ' custom_media_url' . $i . '" name="' . $this->get_field_name('image_uri[]') . '" id="' . $this->get_field_id('image_uri' . $i) . '" value="' . $display_instance['image_uri'][$i] . '" />';
            $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_upload' . $i . '" id="' . $this->get_field_id('image_id' . $i) . '"/>';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt' . $i) . '"> ' . __('Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt' . $i) . '" name="' . $this->get_field_name('image_uri_alt[]') . '" type="text" value="' . $display_instance['image_uri_alt'][$i] . '" />';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('leadersDesc' . $i) . '"> ' . __('Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('leadersDesc' . $i) . '" name="' . $this->get_field_name('leadersDesc[]') . '" type="text" value="' . $display_instance['leadersDesc'][$i] . '" />';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('leadersDesc4' . $i) . '"> ' . __('Endorser Name', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('leadersDesc4' . $i) . '" name="' . $this->get_field_name('leadersDesc4[]') . '" type="text" value="' . $display_instance['leadersDesc4'][$i] . '" />';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('leadersDesc5' . $i) . '"> ' . __('Button Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('leadersDesc5' . $i) . '" name="' . $this->get_field_name('leadersDesc5[]') . '" type="text" value="' . $display_instance['leadersDesc5'][$i] . '" />';
            $rew_html .= '</p>';


            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('link_type' . $i) . '"> ' . __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('link_type' . $i) . '" name="' . $this->get_field_name('link_type[]') . '" onChange="show_hide_div_agilysys_customer_slider_widget(this.value,' . $i . ');">';
            $rew_html .= '<option value="">Please Select</option>';

            $link_type = $display_instance['link_type'][$i];

            if ($link_type == 'page') {
                $rew_html .= '<option value="page" selected="selected">Internal Page Link</option>';
            } else {
                $rew_html .= '<option value="page">Internal Page Link</option>';
            }

            if ($link_type == 'link') {
                $rew_html .= '<option value="link" selected="selected">External Link</option>';
            } else {
                $rew_html .= '<option value="link">External Link</option>';
            }

            $rew_html .= '</select>';
            $rew_html .= '</p><br><br>';

            $args = array(
                'sort_order' => 'desc',
                'sort_column' => 'post_title',
                'hierarchical' => 1,
                'exclude' => '',
                'include' => '',
                'meta_key' => '',
                'meta_value' => '',
                'authors' => '',
                'child_of' => 0,
                'parent' => -1,
                'exclude_tree' => '',
                'number' => '',
                'offset' => 0,
                'post_type' => 'page',
                'post_status' => 'publish',
            );
            $pages = get_pages($args); // get all pages based on supplied args

            if ($link_type == 'page') {
                $show1 = 'style="display:block"';
                $show2 = 'style="display:none"';
            } elseif ($link_type == 'link') {
                $show1 = 'style="display:none"';
                $show2 = 'style="display:block"';

            } else {
                $show1 = 'style="display:none"';
                $show2 = 'style="display:none"';
            }
            $rew_html .= '<div id="page_div_agilysys_customer_slider_widget' . $i . '" ' . $show1 . '><p>';
            $rew_html .= '<label for="' . $this->get_field_id('page' . $i) . '"> ' . __('Page', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('page' . $i) . '" name="' . $this->get_field_name('page[]') . '">';
            $rew_html .= '<option value="">Please Select</option>';

            $page = $display_instance['page'][$i];

            foreach ($pages as $key) {

                if ($page == $key->ID) {
                    $rew_html .= '<option value="' . $key->ID . '" selected="selected">' . $key->post_title . '</option>';
                } else {
                    $rew_html .= '<option value="' . $key->ID . '">' . $key->post_title . '</option>';
                }

            }

            $rew_html .= '</select>';
            $rew_html .= '</p></div><br><br>';

            $rew_html .= '<div id="link_div_agilysys_customer_slider_widget' . $i . '" ' . $show2 . '><p>';
            $rew_html .= '<label for="' . $this->get_field_id('leadersDesc7' . $i) . '"> ' . __('Link', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('leadersDesc7' . $i) . '" name="' . $this->get_field_name('leadersDesc7[]') . '" type="text" value="' . $display_instance['leadersDesc7'][$i] . '" />';
            $rew_html .= '</p></div><br><br>';
            ?>
<script>
//show hide div based on i value and value of dropdown
function show_hide_div_agilysys_customer_slider_widget(val, i) {
console.log(val);
    if (val == 'page') {
        jQuery("#page_div_agilysys_customer_slider_widget" + i).show();
        jQuery("#link_div_agilysys_customer_slider_widget" + i).hide();
    } else if (val == 'link') {
        jQuery("#page_div_agilysys_customer_slider_widget" + i).hide();
        jQuery("#link_div_agilysys_customer_slider_widget" + i).show();
    }

}
</script>

<?php

            $rew_html .= '</div>';

            ?>

<script>
//show hide media based on id and value of dropdown
function show_hide_media_agilysys_customer_slider_widget(value, id) {

     console.log(value);         

    if (value == "image") {

        jQuery('.widg-img' + id).show();
        jQuery('.widg-youtube' + id).hide();
        jQuery('.widg-video' + id).hide();

    } else if (value == "youtube") {
        jQuery('.widg-img' + id).hide();
        jQuery('.widg-youtube' + id).show();
        jQuery('.widg-video' + id).hide();
    } else if (value == "video") {
        jQuery('.widg-img' + id).hide();
        jQuery('.widg-youtube' + id).hide();
        jQuery('.widg-video' + id).show();
    }

}

jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    if (attachment.height == 1080 && attachment.width == 1920) {
                        jQuery('.' + button_id_s + '_media_id<?php echo $i; ?>').val(attachment.id);
                        jQuery('.' + button_id_s + '_media_url<?php echo $i; ?>').val(attachment
                            .url);
                        jQuery('.' + button_id_s + '_media_image<?php echo $i; ?>').attr('src',
                            attachment.url).css('display', 'block');

                        jQuery('#image_uri_agilysys_customer_slider_widget<?php echo $i; ?>')
                            .html("");
                    } else {
                        jQuery('#image_uri_agilysys_customer_slider_widget<?php echo $i; ?>')
                            .html("Please Enter the correct Dimensions 1920x1080").css('color',
                                'red');

                    }



                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload<?php echo $i; ?>');

});
</script>

<?php

            $show2 = (!empty($display_instance['video_uri'][$i]) && $display_instance['type'][$i] == "video") ? '' : 'style="display:none;"';
            $rew_html .= '<div class="widg-video' . $i . '" ' . $show2 . '>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('video_id' . $i) . '"> ' . __('Video', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<video class="' . $this->get_field_id('video_id' . $i) . '_media_videov' . $i . ' custom_media_videov' . $i . '" width="320" height="240" controls ' . $show2 . '><source src="' . $display_instance['video_uri'][$i] . '" type="video/mp4"></video>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('video_id' . $i) . '_media_idv' . $i . ' custom_media_idv' . $i . '" name="' . $this->get_field_name('video_id[]' . $i) . '" id="' . $this->get_field_id('video_id' . $i) . '" value="' . $display_instance['video_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('video_id' . $i) . '_media_urlv' . $i . ' custom_media_urlv' . $i . '" name="' . $this->get_field_name('video_uri[]') . '" id="' . $this->get_field_id('video_uri' . $i) . '" value="' . $display_instance['video_uri'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload Video" class="button custom_media_uploadv' . $i . '" id="' . $this->get_field_id('video_id' . $i) . '"/>';
            $rew_html .= '</p>';
            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('customer_single_video_slider_desc' . $i) . '"> ' . __('Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('customer_single_video_slider_desc' . $i) . '" name="' . $this->get_field_name('customer_single_video_slider_desc[]') . '" type="text" value="' . $display_instance['customer_single_video_slider_desc'][$i] . '" />';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('customer_single_video_slider_endorser' . $i) . '"> ' . __('Endorser Name', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('customer_single_video_slider_endorser' . $i) . '" name="' . $this->get_field_name('customer_single_video_slider_endorser[]') . '" type="text" value="' . $display_instance['customer_single_video_slider_endorser'][$i] . '" />';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('customer_single_video_slider_link_title' . $i) . '"> ' . __('Button Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('customer_single_video_slider_link_title' . $i) . '" name="' . $this->get_field_name('customer_single_video_slider_link_title[]') . '" type="text" value="' . $display_instance['customer_single_video_slider_link_title'][$i] . '" />';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('customer_single_video_slider_link' . $i) . '"> ' . __('Link Name', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('customer_single_video_slider_link' . $i) . '" name="' . $this->get_field_name('customer_single_video_slider_link[]') . '" type="text" value="' . $display_instance['customer_single_video_slider_link'][$i] . '" />';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('link_typea' . $i) . '"> ' . __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('link_typea' . $i) . '" name="' . $this->get_field_name('link_typea[]') . '" onChange="show_hide_diva_agilysys_customer_slider_widget(this.value,' . $i . ');">';
            $rew_html .= '<option value="">Please Select</option>';

            $link_type = $display_instance['link_typea'][$i];

            if ($link_type == 'page') {
                $rew_html .= '<option value="page" selected="selected">Internal Page Link</option>';
            } else {
                $rew_html .= '<option value="page">Internal Page Link</option>';
            }

            if ($link_type == 'link') {
                $rew_html .= '<option value="link" selected="selected">External Link</option>';
            } else {
                $rew_html .= '<option value="link">External Link</option>';
            }

            $rew_html .= '</select>';
            $rew_html .= '</p><br><br>';

            $args = array(
                'sort_order' => 'desc',
                'sort_column' => 'post_title',
                'hierarchical' => 1,
                'exclude' => '',
                'include' => '',
                'meta_key' => '',
                'meta_value' => '',
                'authors' => '',
                'child_of' => 0,
                'parent' => -1,
                'exclude_tree' => '',
                'number' => '',
                'offset' => 0,
                'post_type' => 'page',
                'post_status' => 'publish',
            );
            $pages = get_pages($args); // get all pages based on supplied args

            if ($link_type == 'page') {
                $show1 = 'style="display:block"';
                $show2 = 'style="display:none"';
            } elseif ($link_type == 'link') {
                $show1 = 'style="display:none"';
                $show2 = 'style="display:block"';

            } else {
                $show1 = 'style="display:none"';
                $show2 = 'style="display:none"';
            }
            $rew_html .= '<div id="page_diva_agilysys_customer_slider_widget' . $i . '" ' . $show1 . '><p>';
            $rew_html .= '<label for="' . $this->get_field_id('pagea' . $i) . '"> ' . __('Page', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('pagea' . $i) . '" name="' . $this->get_field_name('pagea[]') . '">';
            $rew_html .= '<option value="">Please Select</option>';

            $page = $display_instance['pagea'][$i];

            foreach ($pages as $key) {

                if ($page == $key->ID) {
                    $rew_html .= '<option value="' . $key->ID . '" selected="selected">' . $key->post_title . '</option>';
                } else {
                    $rew_html .= '<option value="' . $key->ID . '">' . $key->post_title . '</option>';
                }

            }

            $rew_html .= '</select>';
            $rew_html .= '</p></div><br><br>';

            $rew_html .= '<div id="link_diva_agilysys_customer_slider_widget' . $i . '" ' . $show2 . '><p>';
            $rew_html .= '<label for="' . $this->get_field_id('customer_single_video_slider_link' . $i) . '"> ' . __('Link', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('customer_single_video_slider_link' . $i) . '" name="' . $this->get_field_name('customer_single_video_slider_link[]') . '" type="text" value="' . $display_instance['customer_single_video_slider_link'][$i] . '" />';
            $rew_html .= '</p></div><br><br>';
            ?>
<script>
//show hide div based on i value and value of dropdown
function show_hide_diva_agilysys_customer_slider_widget(val, i) {
console.log(val);
    if (val == 'page') {
        jQuery("#page_diva_agilysys_customer_slider_widget" + i).show();
        jQuery("#link_diva_agilysys_customer_slider_widget" + i).hide();
    } else if (val == 'link') {
        jQuery("#page_diva_agilysys_customer_slider_widget" + i).hide();
        jQuery("#link_diva_agilysys_customer_slider_widget" + i).show();
    }

}
</script>

<?php

            $rew_html .= '</div>';

            $show3 = (!empty($display_instance['youtube_uri'][$i]) && $display_instance['type'][$i] == "youtube") ? '' : 'style="display:none;"';

            $rew_html .= '<div class="widg-youtube' . $i . '" ' . $show3 . '>';
            $rew_html .= '<input type="text"  name="' . $this->get_field_name('youtube_uri[]') . '" id="' . $this->get_field_id('media_uri' . $i) . '" value="' . $display_instance['youtube_uri'][$i] . '" />';
            $rew_html .= '</p>';
            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('leadersDesc3' . $i) . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('leadersDesc3' . $i) . '" name="' . $this->get_field_name('leadersDesc3[]') . '" type="text" value="' . $display_instance['leadersDesc3'][$i] . '" />';
            $rew_html .= '</p>';
            $rew_html .= '</div>';
            ?>

<script>
jQuery(document).ready(function() {




    function media_uploadv(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_uploadv<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {
                    jQuery('.' + button_id_s + '_media_idv<?php echo $i; ?>').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_urlv<?php echo $i; ?>').val(attachment.url);
                    jQuery('.' + button_id_s + '_media_videov<?php echo $i; ?>').attr('src',
                        attachment.url).css('display', 'block');
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_uploadv('.custom_media_uploadv<?php echo $i; ?>');


});
</script>

<?php

            $k = $i + 1;
            $rew_html .= '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' . $k . ');">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';

        }

        $rew_html .= '</div></div>';

        $rew_html .= '<div class="' . $widget_add_id_hosipitality_software . '" style="margin-bottom: 36px;text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;" onClick="add_new_row();">' . __('ADD ROW', 'AGILYSYS_TEXT_DOMAIN') . '</div>';
        ?>
<script>
//Add new row
function add_new_row() {
    var cnt = '';

    jQuery.each(jQuery("#entries_agilysys_customer_slider_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            cnt = jQuery(this).val();
        }
    });



    cnt++;

    jQuery.each(jQuery("#entries_agilysys_customer_slider_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(cnt);
        }
    });
    
    console.log(cnt);

    var new_row = '<div id="entry' + cnt +
        '"  class="entrys"><span class="entry-title" onclick = "slider(this);"><?php echo __('Add New Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span>';
    new_row += '<div class="entry-desc cf">';


    new_row += '<p>';
    new_row += '<label for=""><?php echo __('Media Type', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



    new_row +=
        '<select name="<?php echo $this->get_field_name('type[]'); ?>"  onChange="show_hide_media1_agilysys_customer_slider_widget(this.value,' +
        cnt +
        ');">';
    new_row += '<option value="">Please Select</option>';
    new_row += '<option value="image">Image</option>';
    new_row += '<option value="video">Video</option>';
    new_row += '<option value="youtube">Youtube url</option>';

    new_row += '</select>';
    new_row += '</p>';






    new_row += '<div class="widg-img' + cnt + '">';
    new_row += '<label id="image_uri_agilysys_customer_slider_widget' + cnt +'"></label><br><img class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_image' + cnt +
        ' custom_media_image' + cnt + '" src="" width=200" height="120" style="display:none;">';
    new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_url' +
        cnt + ' custom_media_url' + cnt +
        '" name="<?php echo $this->get_field_name('image_uri[]'); ?>" id="<?php echo $this->get_field_id('image_uri'); ?>' +
        cnt + '" value="">';
    new_row += '<input type="button" value="Upload Image" class="button custom_media_upload' + cnt +
        '" id="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '"/>';



    jQuery(document).ready(function() {




        function media_upload(button_class) {
            var _custom_media = true,
                _orig_send_attachment = wp.media.editor.send.attachment;
            jQuery('body').on('click', '.custom_media_upload' + cnt, function(e) {
                var button_id = '#' + jQuery(this).attr('id');
                var button_id_s = jQuery(this).attr('id');
                console.log(button_id);
                var self = jQuery(button_id);
                var send_attachment_bkp = wp.media.editor.send.attachment;
                var button = jQuery(button_id);
                var id = button.attr('id').replace('_button', '');
                _custom_media = true;

                wp.media.editor.send.attachment = function(props, attachment) {
                    if (_custom_media) {

                        if (attachment.height == 1080 && attachment.width == 1920) {
                            jQuery('.' + button_id_s + '_media_id' + cnt).val(attachment.id);


                            jQuery('.' + button_id_s + '_media_url' + cnt).val(attachment.url);
                            jQuery('.' + button_id_s + '_media_image' + cnt).attr('src',
                                attachment.url).css('display', 'block');

                            jQuery('#image_uri_agilysys_customer_slider_widget' + cnt)
                                .html("");
                        } else {
                            jQuery('#image_uri_agilysys_customer_slider_widget' + cnt)
                                .html("Please Enter the correct Dimensions 1920x1080").css(
                                    'color', 'red');
                        }
                    } else {
                        return _orig_send_attachment.apply(button_id, [props, attachment]);
                    }
                }
                wp.media.editor.open(button);
                return false;
            });
        }
        media_upload('.custom_media_upload' + cnt);

    });

    new_row += '<p>';
    new_row += '<label for=""><?php echo __('Image Alt', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';

    new_row +=
        '<input class="" name="<?php echo esc_attr($this->get_field_name('image_uri_alt[]')); ?>" type="text" value="">';
    new_row += '</p>';


    new_row += '<p>';
    new_row += '<label for=""><?php echo __('Description', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



    new_row +=
        '<input name="<?php echo esc_attr($this->get_field_name('leadersDesc[]')); ?>" type="text" value="">';
    new_row += '</p>';


    new_row += '<p>';
    new_row += '<label for=""><?php echo __('Endorser Name', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



    new_row +=
        '<input name="<?php echo esc_attr($this->get_field_name('leadersDesc4[]')); ?>" type="text" value="">';
    new_row += '</p>';


    new_row += '<p>';
    new_row += '<label for=""><?php echo __('Button Title', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



    new_row +=
        '<input name="<?php echo esc_attr($this->get_field_name('leadersDesc5[]')); ?>" type="text" value="">';
    new_row += '</p><br><br><br><br>';


  


    new_row += '<p>';
    new_row += '<label for=""><?php echo __('Select Link type url:', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
    new_row +=
        '<select  name="<?php echo $this->get_field_name('link_type[]'); ?>" onChange="show_hide_div1_agilysys_customer_slider_widget(this.value,' +
        cnt + ');">';
    new_row += '<option value="">Please Select</option>';
    new_row += '<option value="page">Internal Page Link</option>';
    new_row += '<option value="link">External Link</option>';
    new_row += '</select>';
    new_row += '</p><br><br>';



    <?php
$args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        ?>


    new_row += '<div id="page_div_agilysys_customer_slider_widget' + cnt + '" style="display:none;"><p>';
    new_row += '<label for=""><?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
    new_row += '<select  name="<?php echo $this->get_field_name('page[]'); ?>">';
    new_row += '<option value="">Please Select</option>';



    <?php

        foreach ($pages as $key) {
            ?>

    new_row += '<option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>';


    <?php

        }
        ?>
    new_row += '</select>';

    new_row += '</p></div><br><br>';

    new_row += '<div id="link_div_agilysys_customer_slider_widget' + cnt + '" style="display:none;"><p>';
    new_row += '<label for=""><?php echo __('Link', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
    new_row += '<input name="<?php echo $this->get_field_name('leadersDesc7[]'); ?>" type="text" value="" />';
    new_row += '</p></div><br><br></div>';



    

   

    new_row += '<div class="widg-video' + cnt + '" style="display:none;">';

    new_row += '<p>';
    new_row += '<label for=""><?php echo __('Video', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
    new_row += '<video class="<?php echo $this->get_field_id('video_id'); ?>' + cnt + '_media_videov' + cnt +
        'custom_media_videov' + cnt +'" width="320" height="240" style="display:none;" controls><source src="" type="video/mp4"></video>';
    new_row += '<input type="hidden" class="<?php echo $this->get_field_id('video_id'); ?>' + cnt + '_media_idv' + cnt +
        ' custom_media_idv' + cnt +
        '" name="<?php echo $this->get_field_name('video_id'); ?>" id="<?php echo $this->get_field_id('video_id'); ?>' +
        cnt + '" value="" />';

    new_row += '<input type="hidden" class="<?php echo $this->get_field_id('video_id'); ?>' + cnt + '_media_urlv' +
        cnt + ' custom_media_urlv' + cnt + '" name="<?php echo $this->get_field_name('video_uri[]'); ?>">';

    new_row += '<input type="button" value="Upload Video" class="button custom_media_uploadv' + cnt +
        '" id="<?php echo $this->get_field_id('video_id'); ?>' + cnt + '" />';
    new_row += '</p>';


    jQuery(document).ready(function() {




        function media_uploadv(button_class) {
            var _custom_media = true,
                _orig_send_attachment = wp.media.editor.send.attachment;
            jQuery('body').on('click', '.custom_media_uploadv' + cnt, function(e) {
                var button_id = '#' + jQuery(this).attr('id');
                var button_id_s = jQuery(this).attr('id');
                console.log(button_id);
                var self = jQuery(button_id);
                var send_attachment_bkp = wp.media.editor.send.attachment;
                var button = jQuery(button_id);
                var id = button.attr('id').replace('_button', '');
                _custom_media = true;

                wp.media.editor.send.attachment = function(props, attachment) {
                    if (_custom_media) {
                        jQuery('.' + button_id_s + '_media_idv' + cnt).val(attachment.id);
                        jQuery('.' + button_id_s + '_media_urlv' + cnt).val(attachment.url);
                        jQuery('.' + button_id_s + '_media_videov' + cnt).attr('src',
                            attachment.url).css('display', 'block');
                    } else {
                        return _orig_send_attachment.apply(button_id, [props, attachment]);
                    }
                }
                wp.media.editor.open(button);
                return false;
            });
        }
        media_uploadv('.custom_media_uploadv' + cnt);


    });


    new_row += '<p>';
    new_row += '<label><?php echo __('Description', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
    new_row +=
        '<input name="<?php echo $this->get_field_name('customer_single_video_slider_desc[]'); ?>" type="text"  />';
    new_row += '</p>';


    new_row += '<p>';
    new_row += '<label> <?php echo __('Endorser Name', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
    new_row +=
        '<input  name="<?php echo $this->get_field_name('customer_single_video_slider_endorser[]'); ?>" type="text" value="" />';
    new_row += '</p>';

    new_row += '<p>';
    new_row += '<label> <?php echo __('Button Title', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
    new_row +=
        '<input  name="<?php echo $this->get_field_name('customer_single_video_slider_link_title[]'); ?>" type="text" value="" />';
    new_row += '</p>';

    new_row += '<p>';
    new_row += '<label> <?php echo __('Link Name', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
    new_row +=
        '<input  name="<?php echo $this->get_field_name('customer_single_video_slider_link[]'); ?>" type="text"  />';
    new_row += '</p>';


    new_row += '<p>';
    new_row += '<label> <?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
    new_row +=
        '<select  name="<?php echo $this->get_field_name('link_typea[]'); ?>" onChange="show_hide_diva1_agilysys_customer_slider_widget(this.value,' +
        cnt + ');">';
    new_row += '<option value="">Please Select</option>';
    new_row += '<option value="page">Internal Page Link</option>';
    new_row += '<option value="link">External Link</option>';
    new_row += '</select>';
    new_row += '</p><br><br>';


    <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        ?>


    new_row += '<div id="page_diva_agilysys_customer_slider_widget' + cnt + '" style="display:none;"><p>';
    new_row += '<label> <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
    new_row += '<select name="<?php echo $this->get_field_name('pagea[]'); ?>">';
    new_row += '<option value="">Please Select</option>';


    <?php
foreach ($pages as $key) {
            ?>

    new_row += '<option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>';

    <?php
}
        ?>
    new_row += '</select>';
    new_row += '</p></div><br><br>';


    new_row += '<div id="link_diva_agilysys_customer_slider_widget' + cnt + '" style="display:none;"><p>';
    new_row += '<label> <?php echo __('Link', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
    new_row +=
        '<input name="<?php echo $this->get_field_name('customer_single_video_slider_link[]'); ?>" type="text" value="" />';
    new_row += '</p></div><br><br>';

    new_row += '</div>';








    new_row += '<div class="widg-youtube' + cnt + '" style="display:none;">';
    new_row += '<input type="text"  name="<?php echo $this->get_field_name('youtube_uri[]'); ?>"  />';
    new_row += '</p>';
    new_row += '<p>';
    new_row += '<label><?php echo __('Title', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
    new_row += '<input  name="<?php echo $this->get_field_name('leadersDesc3[]'); ?>" type="text" value="" />';
    new_row += '</p>';
    new_row += '</div>';





    var new_cnt = cnt;

    new_row +=
        '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' + new_cnt +
        ');"><?php echo __('Delete Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span></a></p>';
    new_row += '</div></div>';

    new_row += '</div>';

    jQuery('.add_new_rowxx-input-containers #entries_agilysys_customer_slider_widget').append(new_row);



}
//show hide div based on i value and value of dropdown
function show_hide_div1_agilysys_customer_slider_widget(val, i) {
console.log(val);
    if (val == 'page') {
        jQuery("#page_div_agilysys_customer_slider_widget" + i).show();
        jQuery("#link_div_agilysys_customer_slider_widget" + i).hide();
    } else if (val == 'link') {
        jQuery("#page_div_agilysys_customer_slider_widget" + i).hide();
        jQuery("#link_div_agilysys_customer_slider_widget" + i).show();
    }

}
//show hide media based on i value and value of dropdown
function show_hide_media1_agilysys_customer_slider_widget(value, id) {

console.log(value);

    if (value == "image") {

        jQuery('.widg-img' + id).show();
        jQuery('.widg-youtube' + id).hide();
        jQuery('.widg-video' + id).hide();

    } else if (value == "youtube") {
        jQuery('.widg-img' + id).hide();
        jQuery('.widg-youtube' + id).show();
        jQuery('.widg-video' + id).hide();
    } else if (value == "video") {
        jQuery('.widg-img' + id).hide();
        jQuery('.widg-youtube' + id).hide();
        jQuery('.widg-video' + id).show();
    }

}

//show hide div based on i value and value of dropdown
function show_hide_diva1_agilysys_customer_slider_widget(val, i) {
console.log(val);
    if (val == 'page') {
        jQuery("#page_diva_agilysys_customer_slider_widget" + i).show();
        jQuery("#link_diva_agilysys_customer_slider_widget" + i).hide();
    } else if (val == 'link') {
        jQuery("#page_diva_agilysys_customer_slider_widget" + i).hide();
        jQuery("#link_diva_agilysys_customer_slider_widget" + i).show();
    }

}
//delete row
function delete_row(cnt) {
    jQuery.each(jQuery(".add_new_rowxx-input-containers #entries_agilysys_customer_slider_widget"), function() {
        jQuery(' #entry' + cnt).remove();
    });
    var last_cnt = 0;

    jQuery.each(jQuery("#entries_agilysys_customer_slider_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            last_cnt = jQuery(this).val();
        }
    });

    last_cnt--;
    jQuery.each(jQuery("#entries_agilysys_customer_slider_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(last_cnt);
        }
    });
    jQuery(".client_logo_rows").val(last_cnt);
    jQuery('.client_logo_rows').trigger('change');

}
</script>
<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_customer_slider_widget select {
    float: left;
    width: 60%;
    margin-top: 20px !important;
    margin-bottom: 10px !important;
}

#rew_container_agilysys_customer_slider_widget input,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_customer_slider_widget label {
    width: 40%;
    float: left;
}

#rew_container_agilysys_customer_slider_widget p {
   padding:20px;
}

<?php echo '.'. $widget_add_id_hosipitality_software;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}

#entries_agilysys_customer_slider_widget #remove-img-bg {
    background: url('<?php echo ZWREW_URL; ?>assets/images/deleteimg.png') center center no-repeat;
    width: 20px;
    height: 22px;
    display: none;
}

#entries_agilysys_customer_slider_widget {
    padding: 10px 0 0;
}

#entries_agilysys_customer_slider_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_customer_slider_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_customer_slider_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_customer_slider_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_customer_slider_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_customer_slider_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_customer_slider_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_customer_slider_widget #entries_agilysys_customer_slider_widget plast label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_customer_slider_widget">
    <?php echo $rew_html; ?>
</div>
<?php
} //Function form ends here
} // class ends here
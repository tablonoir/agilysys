<?php

add_action('widgets_init', 'agil_load_guest_service');

function agil_load_guest_service()
{
    register_widget('agilysys_guest_service_widget');
}

class agilysys_guest_service_widget extends WP_Widget
{

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Guest Service Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');

    }

    public function widget($args, $instance)
    {

        echo $args['before_widget'];
        $title = $instance['title'];

        $desc = $instance['desc'];

        $link_type = $instance['link_type'];
        if ($link_type == "link") {
            $link = $instance['link'];
        } elseif ($link_type == "page") {
            $post_id = $instance['page'];
            $post = get_post($post_id);
            $link = home_url($post->post_name) . "/";
        }

        $button_text = $instance['button_text'];

        $div_color = $instance['div_color'];
        $title_color = $instance['title_color'];
        $desc_color = $instance['desc_color'];

        $button_required = $instance['button_required'];
        ?>



<section class="rguestEnriched" style="background-color:<?php echo $div_color; ?> !important;width:100%;">
    <div class="row">
        <div class="rguestEnrichedTitle col-12 col-sm-12 col-md-6 col-lg-4"  data-aos="fade-right" data-aos-delay="300" data-aos-duration="500" data-aos-once="true">
            <h2 class="dinProStd greenText" style="color:<?php echo $title_color; ?>"><?php echo $title; ?> </h2>
        </div>
        <div class="rguestEnrichedContent col-12 col-sm-12 col-md-6 col-lg-5"  data-aos="fade-left" data-aos-delay="300" data-aos-duration="500" data-aos-once="true">
            <p class="dinproMed" style="color:<?php echo $desc_color; ?>"><?php echo substr($desc,0,400); ?> </p>

<?php
if ($button_required == "yes") {
            ?>
            <a class="homeBannerButton whiteText" href="<?php echo $link; ?>">
                    <?php echo $button_text; ?> <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
 <?php
}
        ?>


        </div>
    </div>
</section>



<?php
echo $args['after_widget'];

    }
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['title'] = strip_tags($new_instance['title']);
        $instance['desc'] = strip_tags($new_instance['desc']);
        $instance['div_color'] = strip_tags($new_instance['div_color']);
        $instance['title_color'] = strip_tags($new_instance['title_color']);
        $instance['desc_color'] = strip_tags($new_instance['desc_color']);

        $instance['button_required'] = strip_tags($new_instance['button_required']);
        $instance['button_text'] = strip_tags($new_instance['button_text']);

        $instance['link_type'] = $new_instance['link_type'];

        if ($new_instance['link_type'] == 'page') {
            $instance['page'] = $new_instance['page'];
            $instance['link'] = '';
        } elseif ($new_instance['link_type'] == 'link') {
            $instance['link'] = $new_instance['link'];
            $instance['page'] = '';

        }

        return $instance;
    }

    public function form($display_instance)
    {
        
     

        $title = ($display_instance['title']);
        $desc = ($display_instance['desc']);
        $div_color = $display_instance['div_color'];
        $button_required = $display_instance['button_required'];
        $title_color = $display_instance['title_color'];
        $desc_color = $display_instance['desc_color'];
        ?>

        <script type='text/javascript'>
            jQuery(document).ready(function($) {
                $('.my-color-picker').wpColorPicker();
            });
        </script>


        <?php

        $rand = rand(0, 999999);

        $widget_add_id_webinars_info = $this->get_field_id('') . "add_agilysys_guest_service_widget_" . $rand;

        $rew_html .= '<div id="agilysys_guest_service_widget_' . $rand . '">';

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('title') . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" type="text" value="' . $title . '" />';
        $rew_html .= '</p><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('title_color') . '">' . __('Div Color', 'AGILYSYS_TEXT_DOMAIN') . '</label>';

        $rew_html .= '<input class="my-color-picker" type="text" id="' . $this->get_field_id('title_color') . '" name="' . $this->get_field_name('title_color') . '" value="' . esc_attr($title_color) . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<br><p>';
        $rew_html .= '<label for="' . $this->get_field_id('desc') . '"> ' . __('Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<textarea rows="6" cols="35" id="' . $this->get_field_id('desc') . '" name="' . $this->get_field_name('desc') . '" >' . $desc . '</textarea>';
        $rew_html .= '</p><br><br><br><br><br><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('desc_color') . '">' . __('Description Color', 'AGILYSYS_TEXT_DOMAIN') . '</label>';

        $rew_html .= '<input class="my-color-picker" type="text" id="' . $this->get_field_id('desc_color') . '" name="' . $this->get_field_name('desc_color') . '" value="' . esc_attr($desc_color) . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('div_color') . '">' . __('Div Color', 'AGILYSYS_TEXT_DOMAIN') . '</label>';

        $rew_html .= '<input class="my-color-picker" type="text" id="' . $this->get_field_id('div_color') . '" name="' . $this->get_field_name('div_color') . '" value="' . esc_attr($div_color) . '" />';
        $rew_html .= '</p>';
        
        
         $button_required = $display_instance['button_required'];
         
          

        if($button_required=="yes")
        {
           $checked1 = 'checked="checked"';
           $checked2 = '';
           $show1 = 'style="display:block"';
        }
        elseif($button_required=="no")
        {
            $checked1 = '';
            $checked2 = 'checked="checked"';
            $show1 = 'style="display:none"';
        }
        else
        {
            $checked1 = '';
            $checked2 = '';
            $show1 = 'style="display:none"';
        }

        $rew_html .= '<br><br><label style="margin-left:10px;">Button Required</label>';

        $rew_html .= '<p style="margin-left:40%;margin-top:-20px !important;">&nbsp;&nbsp;<input style="width:1% !important;float:left;" type="radio" id="' . $this->get_field_id('button_required') . '" name="' . $this->get_field_name('button_required') . '" value="yes" ' . $checked1 . ' onClick="show_hide_link_type_id_p_agilysys_guest_service_widget_' . $rand . '(this.value);">&nbsp;&nbsp;<span style="float:left;">Yes</span>';

        $rew_html .= '&nbsp;&nbsp;<input style="width:1% !important;margin-left:1%;float:left;" type="radio" id="' . $this->get_field_id('button_required') . '" name="' . $this->get_field_name('button_required') . '" value="no" ' . $checked2 . ' onClick="show_hide_link_type_id_p_agilysys_guest_service_widget_' . $rand . '(this.value);">&nbsp;&nbsp;<span style="float:left;">No</span></p>';

        $rew_html .= '<div '.$show1.' id="link_type_id_p_agilysys_guest_service_widget_' . $rand . '">';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('button_text') . '"> ' . __('Button Text', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('button_text') . '" name="' . $this->get_field_name('button_text') . '" type="text" value="' . $display_instance['button_text'] . '" />';
        $rew_html .= '</p><br><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('link_type') . '"> ' . __('Link', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<select id="' . $this->get_field_id('link_type') . '" name="' . $this->get_field_name('link_type') . '" onChange="show_hide_div_agilysys_guest_service_widget_' . $rand . '(this.value);">';
        $rew_html .= '<option value="">Please Select</option>';

        $link_type = $display_instance['link_type'];

        if ($link_type == 'page') {
            $rew_html .= '<option value="page" selected="selected">Page</option>';
        } else {
            $rew_html .= '<option value="page">Page</option>';
        }

        if ($link_type == 'link') {
            $rew_html .= '<option value="link" selected="selected">Link</option>';
        } else {
            $rew_html .= '<option value="link">Link</option>';
        }

        $rew_html .= '</select>';
        $rew_html .= '</p><br>';

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block"';
            $show2 = 'style="display:none"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none"';
            $show2 = 'style="display:block"';

        } else {
            $show1 = 'style="display:none"';
            $show2 = 'style="display:none"';
        }
        $rew_html .= '<div id="page_div_agilysys_guest_service_widget_' . $rand . '" ' . $show1 . '><p>';
        $rew_html .= '<label for="' . $this->get_field_id('page') . '"> ' . __('Page', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<select id="' . $this->get_field_id('page') . '" name="' . $this->get_field_name('page') . '">';
        $rew_html .= '<option value="">Please Select</option>';

        $page = $display_instance['page'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                $rew_html .= '<option value="' . $key->ID . '" selected="selected">' . $key->post_title . '</option>';
            } else {
                $rew_html .= '<option value="' . $key->ID . '">' . $key->post_title . '</option>';
            }

        }

        $rew_html .= '</select>';
        $rew_html .= '</p></div>';

        $rew_html .= '<div id="link_div_agilysys_guest_service_widget_' . $rand . '" ' . $show2 . '><p>';
        $rew_html .= '<label for="' . $this->get_field_id('link') . '"> ' . __('Link', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('link') . '" name="' . $this->get_field_name('link') . '" type="text" value="' . $link . '" />';
        $rew_html .= '</p></div><br></div>';
        //$rew_html .= '</div>';

        ?>

<script>

function show_hide_link_type_id_p_agilysys_guest_service_widget_<?php echo $rand; ?>(val){
    console.log(val);
    if(val=='yes')
    {
         jQuery("#link_type_id_p_agilysys_guest_service_widget_<?php echo $rand; ?>").show();

    }
    else if(val=='no')
    {
        jQuery("#link_type_id_p_agilysys_guest_service_widget_<?php echo $rand; ?>").hide();

    }
}

function show_hide_div_agilysys_guest_service_widget_<?php echo $rand; ?>(val){
console.log(val);
if(val=='page')
{
     jQuery("#page_div_agilysys_guest_service_widget_<?php echo $rand; ?>").show();
     jQuery("#link_div_agilysys_guest_service_widget_<?php echo $rand; ?>").hide();
}
else if(val=='link')
{
    jQuery("#page_div_agilysys_guest_service_widget_<?php echo $rand; ?>").hide();
     jQuery("#link_div_agilysys_guest_service_widget_<?php echo $rand; ?>").show();
}

}


</script>

<style>

#rew_container_agilysys_guest_service_widget p{
    padding:10px;
}

.wp-picker-container{
    float: right;
    width: 60%;
}
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}



#rew_container_agilysys_guest_service_widget select {
    float: left;
    width: 60%;
    margin-top: 20px !important;
    margin-bottom: 10px !important;
}

#rew_container_agilysys_guest_service_widget input,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_guest_service_widget label {
    width: 40%;
    float: left;
}

#rew_container_agilysys_guest_service_widget p {
    padding:20px;
}

<?php echo '.' . $widget_add_id_slider;

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}



#entries_agilysys_guest_service_widget {
    padding: 10px 0 0;
}

#entries_agilysys_guest_service_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_guest_service_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_guest_service_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_guest_service_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_guest_service_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_guest_service_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_guest_service_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_guest_service_widget #entries_agilysys_guest_service_widget p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>

<div id="rew_container_agilysys_guest_service_widget">
    <?php echo $rew_html; ?>
</div>



    <?php
}
}
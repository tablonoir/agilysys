<?php

function load_agilysys_product_resource_custom_solutions_widget()
{
    register_widget('agilysys_product_resource_custom_solutions_widget');
}

add_action('widgets_init', 'load_agilysys_product_resource_custom_solutions_widget');

class agilysys_product_resource_custom_solutions_widget extends WP_Widget
{

    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Product Resource Custom Solutions Widget', 'AGILYSYS_TEXT_DOMAIN'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        $section_title = $instance['section_title'];
        $image_uri = $instance['image_uri'];
        $image_uri_alt = $instance['image_uri_alt'];
        $max_entries_agilysys_product_resource_custom_solutions_widget_slider_image = 2;
        ?>


<section class="propertySolutionSection flex">
    <div class="propertySolutionImg">
        <img class="img-fluid" src="<?php echo $image_uri; ?>" alt="<?php echo $image_uri_alt; ?>" />
    </div>
    <div class="propertySolutionContent flex">
        <h2 class="dinProStd greenText h2"><?php echo $section_title; ?></h2>


        <?php
for ($i = 0; $i < $max_entries_agilysys_product_resource_custom_solutions_widget_slider_image; $i++) {

            $title = $instance['title' . $i];
            $description = $instance['description' . $i];

            $pdf_uri = $instance['pdf_uri' . $i];
            $learn_more_text = $instance['learn_more_text' . $i];
            ?>
        <div class="propertySolutionContentBox">
            <h5 class="dinProStd greenText"><?php echo $title; ?></h5>
            <p><?php echo substr($description,0,400); ?></p>
            <a href="<?php echo $pdf_uri; ?>" class="aboutButton violetText"> <?php echo $learn_more_text; ?> <i
                    class="fa fa-arrow-right" aria-hidden="true"></i></a>
        </div>
        <?php
}

        ?>
    </div>
</section>

<?php
echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {

        $instance = array();
        $max_entries_agilysys_product_resource_custom_solutions_widget_slider_image = 2;

        $instance['section_title'] = strip_tags($new_instance['section_title']);

        $instance['image_uri'] = strip_tags($new_instance['image_uri']);
        $instance['image_uri_alt'] = strip_tags($new_instance['image_uri_alt']);

        for ($i = 0; $i < $max_entries_agilysys_product_resource_custom_solutions_widget_slider_image; $i++) {
            $block = $new_instance['block-' . $i];

            $instance['block-' . $i] = $new_instance['block-' . $i];

            $instance['title' . $i] = $new_instance['title' . $i];
            $instance['description' . $i] = $new_instance['description' . $i];
            $instance['learn_more_text' . $i] = $new_instance['learn_more_text' . $i];
            $instance['pdf_uri' . $i] = $new_instance['pdf_uri' . $i];

        }
        return $instance;
    }

    public function form($display_instance)
    {
        $max_entries_agilysys_product_resource_custom_solutions_widget_slider_image = 2;

        $widget_add_id_slider = $this->get_field_id('') . "add_agilysys_product_resource_point_of_sale_solutions_suite_widget";

        $title = ($display_instance['section_title']);

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('section_title') . '"> ' . __('Section Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('section_title') . '" name="' . $this->get_field_name('section_title') . '" type="text" value="' . $title . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<div class="widg-img">';
        $show1 = (empty($display_instance['image_uri'])) ? 'style="display:none;"' : '';
        $rew_html .= '<label id="image_uri_agilysys_product_resource_custom_solutions_widget"></label><br><img class="' . $this->get_field_id('image_id') . '_media_image custom_media_image" src="' . $display_instance['image_uri'] . '" ' . $show1 . ' width=200" height="120"/>';
        $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id') . '_media_id custom_media_id" name="' . $this->get_field_name('image_id') . '" id="' . $this->get_field_id('image_id') . '" value="' . $display_instance['image_id'] . '" />';
        $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id') . '_media_url custom_media_url" name="' . $this->get_field_name('image_uri') . '" id="' . $this->get_field_id('image_uri') . '" value="' . $display_instance['image_uri'] . '">';
        $rew_html .= '<input type="button" value="Upload Background Image" class="button custom_media_upload" id="' . $this->get_field_id('image_id') . '"/>';

        $rew_html .= '</div>';
        ?>

<script>
jQuery(document).ready(function() {
    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {
                    if (attachment.height == 873 && attachment.width == 871) {
                        jQuery('.' + button_id_s + '_media_id').val(attachment.id);
                        jQuery('.' + button_id_s + '_media_url').val(attachment.url);
                        jQuery('.' + button_id_s + '_media_image').attr('src', attachment.url).css(
                            'display', 'block');
                        jQuery('#image_uri_agilysys_product_resource_custom_solutions_widget')
                            .html("");
                    } else {
                        jQuery('#image_uri_agilysys_product_resource_custom_solutions_widget')
                            .html("Please Enter the correct Dimensions 871 x 873").css(
                                'color', 'red');

                    }
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload');

});
</script>

<?php

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt') . '"> ' . __('Image Alt', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt') . '" name="' . $this->get_field_name('image_uri_alt') . '" type="text" value="' . $display_instance['image_uri_alt'] . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<div class="' . $widget_add_id_slider . '-input-containers"><div id="entries_agilysys_product_resource_custom_solutions_widget">';
        for ($i = 0; $i < $max_entries_agilysys_product_resource_custom_solutions_widget_slider_image; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'AGILYSYS_TEXT_DOMAIN') . ' </span>';

            $rew_html .= '<div class="entry-desc cf">';
            $rew_html .= '<input id="' . $this->get_field_id('block-' . $i) . '" name="' . $this->get_field_name('block-' . $i) . '" type="hidden" value="0">';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('title' . $i) . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('title' . $i) . '" name="' . $this->get_field_name('title' . $i) . '" type="text" value="' . $display_instance['title' . $i] . '">';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('description' . $i) . '"> ' . __('Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<textarea rows="6" cols="35" id="' . $this->get_field_id('description' . $i) . '" name="' . $this->get_field_name('description' . $i) . '">' . $display_instance['description' . $i] . '</textarea>';
            $rew_html .= '</p><br><br><br>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('learn_more_text' . $i) . '"> ' . __('Learn More Text 1', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('learn_more_text' . $i) . '" name="' . $this->get_field_name('learn_more_text' . $i) . '" type="text" value="' . $display_instance['learn_more_text' . $i] . '">';
            $rew_html .= '</p><br>';

            $rew_html .= '<div class="widg-img">';
            $show1 = (empty($display_instance['pdf_uri' . $i])) ? 'style="display:none;"' : '';
            $rew_html .= '<span style="color:green;display:none;" class="' . $this->get_field_id('pdf_id' . $i) . '_media_imagexx">File Uploaded Successfully</span>';
            $rew_html .= '<span style="color:green;display:none;" class="' . $this->get_field_id('pdf_id' . $i) . '_media_image123xx">File Uploaded Successfully</span>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('pdf_id' . $i) . '_media_idxx custom_media_idxx" name="' . $this->get_field_name('pdf_id' . $i) . '" id="' . $this->get_field_id('pdf_id' . $i) . '" value="' . $display_instance['pdf_id' . $i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('pdf_id' . $i) . '_media_urlxx custom_media_urlxx" name="' . $this->get_field_name('pdf_uri' . $i) . '" id="' . $this->get_field_id('pdf_uri' . $i) . '" value="' . $display_instance['pdf_uri' . $i] . '">';
            $rew_html .= '<input type="button" value="Upload PDF or ZIP File" class="button custom_media_uploadxx" id="' . $this->get_field_id('pdf_id' . $i) . '"/>';

            $rew_html .= '</div>';

            ?>
<script>
jQuery(document).ready(function() {
    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_uploadxx', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {
                    jQuery('.' + button_id_s + '_media_idxx').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_urlxx').val(attachment.url);
                    jQuery('.' + button_id_s + '_media_imagexx').css(
                        'display', 'block');
                    jQuery('.' + button_id_s + '_media_image123xx').css(
                        'display', 'block').html(attachment.url);

                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_uploadxx');

});
</script>


<?php
$rew_html .= '</div></div>';

        }

        $rew_html .= '</div></div>';

        ?>


<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_product_resource_custom_solutions_widget input,
select,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_product_resource_custom_solutions_widget label {
    width: 40%;
}

#rew_container_agilysys_product_resource_custom_solutions_widget p {
    padding:10px;
}

<?php echo '.'. $widget_add_id_slider;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}



#entries_agilysys_product_resource_custom_solutions_widget {
    padding: 10px 0 0;
}

#entries_agilysys_product_resource_custom_solutions_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_product_resource_custom_solutions_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_product_resource_custom_solutions_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_product_resource_custom_solutions_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_product_resource_custom_solutions_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_product_resource_custom_solutions_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_product_resource_custom_solutions_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_product_resource_custom_solutions_widget #entries_agilysys_product_resource_custom_solutions_widget p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_product_resource_custom_solutions_widget">
    <?php echo $rew_html; ?>
</div>

<?php

    }

}
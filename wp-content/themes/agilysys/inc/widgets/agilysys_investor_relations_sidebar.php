<?php

function agilysys_investor_relations_sidebars()
{
    register_widget('agilysys_investor_relations_sidebar');
}
add_action('widgets_init', 'agilysys_investor_relations_sidebars');

class agilysys_investor_relations_sidebar extends WP_Widget
{

    public function __construct()
    {
        parent::__construct(false, $name = __('Investor Relations Sidebar Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');

    }

    public function widget($args, $instance)
    {
  echo $args['before_widget'];
        $menu_title = (isset($instance['menu_title'])) ? $instance['menu_title'] : "";

        ?>


<div class="invsetorSideBar greenBG whiteText">
    <h3 class="dinProStd"><?php echo $menu_title; ?></h3>

    <?php

        $nav_menu = !empty($instance['nav_menu']) ? wp_get_nav_menu_object($instance['nav_menu']) : false;

        if (!$nav_menu) {
            return;
        }
        if ($loop == 'on') {
            $extra_attr .= ' loop="on" ';
        }
        if ($autoplay == 1) {
            $extra_attr .= ' autoplay="1" ';
        }
        if ($preload == 'metadata') {
            $extra_attr .= ' preload="metadata" ';
        }

        $format = current_theme_supports('html5', 'navigation-widgets') ? 'html5' : 'xhtml';

        $format = apply_filters('navigation_widgets_format', $format);

        if ('html5' === $format) {
            // The title may be filtered: Strip out HTML and make sure the aria-label is never empty.
            $title = trim(strip_tags($title));
            $aria_label = $title ? $title : $default_title;

            $nav_menu_args = array(
                'fallback_cb' => '',
                'menu' => $nav_menu,
                'container' => 'nav',
                'container_aria_label' => $aria_label,
                'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            );
        } else {
            $nav_menu_args = array(
                'fallback_cb' => '',
                'menu' => $nav_menu,
            );
        }

        ?>
    <ul>
        <?php

        wp_nav_menu(apply_filters('widget_nav_menu_args', $nav_menu_args, $nav_menu, $args, $instance));
        ?>

    </ul>
</div>




<script>
//news events onclick function
jQuery("#seeAllArchive").click(function() {
    jQuery("#closeArchive").show();
    jQuery("#archiveBoxList").show();
    jQuery("#seeAllArchive").hide();
    jQuery('#partial').hide();
});

jQuery("#closeArchive").click(function() {
    jQuery("#seeAllArchive").show();
    jQuery("#closeArchive").hide();
    jQuery("#archiveBoxList").hide();
    jQuery('#partial').show();
});
</script>
<?php
echo $args['after_widget'];
    }

    public function form($display_instance)
    {
        $nav_menu = isset($display_instance['nav_menu']) ? $display_instance['nav_menu'] : '';

        // Get menus.
        $menus = wp_get_nav_menus();

        $empty_menus_style = '';
        $not_empty_menus_style = '';
        if (empty($menus)) {
            $empty_menus_style = ' style="display:none" ';
        } else {
            $not_empty_menus_style = ' style="display:none" ';
        }

        $nav_menu_style = '';
        if (!$nav_menu) {
            $nav_menu_style = 'display: none;';
        }

        $menu_title = !empty($display_instance['menu_title']) ? $display_instance['menu_title'] : '';

        ?>


<p>
<div class="flex agilysys-banner-widget-title">
    <div class="widget-title-lable">
        <label
            for="<?php echo esc_attr($this->get_field_id('menu_title')); ?>"><?php esc_attr_e('Menu Title', 'agilysys');?>:</label>
    </div>

    <div class="widget-title-input-text">
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('menu_title')); ?>"
            name="<?php echo esc_attr($this->get_field_name('menu_title')); ?>" type="text"
            value="<?php echo esc_attr($menu_title); ?>">
    </div>

</div>
</p>



<p>
<div class="nav-menu-widget-form-controls" <?php echo $empty_menus_style; ?>>
    <div class="flex menu-select-class">
        <div class="menu-select-class-nav">
            <label for="<?php echo $this->get_field_id('nav_menu'); ?>"><?php _e('Select Menu:');?></label>
        </div>

        <div class="menu-select-nav">
            <select id="<?php echo $this->get_field_id('nav_menu'); ?>"
                name="<?php echo $this->get_field_name('nav_menu'); ?>">
                <option value="0"><?php _e('&mdash; Select &mdash;');?></option>
                <?php foreach ($menus as $menu): ?>
                <option value="<?php echo esc_attr($menu->term_id); ?>" <?php selected($nav_menu, $menu->term_id);?>>
                    <?php echo esc_html($menu->name); ?>
                </option>
                <?php endforeach;?>
            </select>
        </div>
    </div>
</div>
</p>


<?php

    }

    public function update($new_instance, $old_instance)
    {

        $instance = array();

        $instance['nav_menu'] = (int) $new_instance['nav_menu'];

        $instance['nav_option'] = $new_instance['nav_option'];

        $instance['menu_title'] = (!empty($new_instance['menu_title'])) ? strip_tags($new_instance['menu_title']) : '';
        return $instance;
    }

}
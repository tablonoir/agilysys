<?php

function load_agilysys_support_widget()
{
    register_widget('agilysys_support_widget');
}

add_action('widgets_init', 'load_agilysys_support_widget');

class agilysys_support_widget extends WP_Widget
{
    /**
     *  constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Support Page Widget', 'AGILYSYS_TEXT_DOMAIN'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }
    public function widget($args, $instance)
    {

        echo $args['before_widget'];
        $second_desc = $instance['second_desc'];
        $description = $instance['description'];
        $title = $instance['title'];
        $signup_link = $instance['signup_link'];

        $forgot_password_link = $instance['forgot_password_link'];
        $support_image_uri = $instance['support_image_uri0'];

//        print_r($support_image_uri);
        $support_icon_text = $instance['support_icon_text'];
        $support_icon_btn_url = $instance['support_icon_btn_url'];

        $signup_link_text = $instance['signup_link_text'];
        $forgot_password_link_text = $instance['forgot_password_link_text'];
        $online_support_text = $instance['online_support_text'];
        $image_uri = $instance['image_uri'];
        $image_uri_alt = $instance['image_uri_alt'];

        ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"
    type="text/javascript"></script>



<section class="supportCommentSection center" data-aos="fade-down" data-aos-delay="300" data-aos-duration="400" data-aos-once="true">
    <h2 class="dinProStd greenText"><?php echo $title; ?></h2>
    <p><?php echo substr($description,0,400); ?></p>
</section>

<section class="supportBoxSection orangeBG">
    <div class="supportBoxContent center" data-aos="fade-up" data-aos-delay="300" data-aos-duration="400" data-aos-once="true">
        <p class="whiteText"><?php echo $second_desc; ?></p>
        <div class="supportBoxButton">
            <div class="row">
                <a class="homeBannerButton whiteText col-6 col-sm-6 col-md-3 col-lg-3"
                    href="<?php echo $signup_link; ?>"><?php echo $signup_link_text; ?> <i class="fa fa-arrow-right"
                        aria-hidden="true"></i></a>
                <a class="homeBannerButton whiteText col-6 col-sm-6 col-md-3 col-lg-3"
                    href="<?php echo $forgot_password_link; ?>"><?php echo $forgot_password_link_text; ?> <i
                        class="fa fa-arrow-right" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
    <div class="supportIconBox">
        <div class="row">
            <div class="supportBoxIcon center flex whiteBG col-12 col-sm-6 col-md-4 col-lg-4" data-aos="fade-right" data-aos-delay="300" data-aos-duration="400" data-aos-once="true">
                <div class="supportBoxIconImg">
                    <img class="img-fluid" src="<?php echo $image_uri; ?>" alt="<?php echo $image_uri_alt; ?>" />
                </div>
                <div class="supportBoxIconText">
                    <h3 class="dinProStd blackText"><?php echo $online_support_text; ?></h3>
                    <a class="homeBannerButton whiteText"
                        href="<?php echo $forgot_password_link; ?>"><?php echo $support_icon_text; ?> <i
                            class="fa fa-arrow-right" aria-hidden="true"></i></a>
                </div>
            </div>
            <div class="supportBoxIcon center col-12 col-sm-6 col-md-4 col-lg-4" id="img2"
                style="background:url(<?php echo $support_image_uri; ?>);background-size: cover;
    background-position: center;    background-repeat: no-repeat;" data-aos="fade-left" data-aos-delay="300" data-aos-duration="400" data-aos-once="true">
                <h3 class="dinProStd whiteText" data-toggle="modal" data-target="#registerForm"><a
                        class="homeBannerButton whiteText">REGISTER </a></h3>
            </div>
        </div>
    </div>
</section>

<style>
    
 .aboutButtonx{
        width:20%;
    }
    
        #registerForm label {
   font-size:24px;
}

#registerForm .close {
    
    right: -0.5%;
}

 #registerForm  .error {
            color: red;
            font-size:16px;
        }
        
        
    
</style>




<div class="modal fade bd-example-modal-lg" id="registerForm" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body">
                <h2 class="dinProStd greenText center" id="title">MyAgilysys Registration </h2>
                <p class="popupContent center">Get more when you register! Create a MyAgilysys account with your
                    customer number for a more customized experience. Once registered, you'll have access to product
                    support documentation and more.</p>
                    
                    
             <form method="post" class="" action="https://info.agilysys.com/l/76642/2020-12-28/5zcf8y" id="frmRma" style="padding:80px;">

<div class="row">
                <div class="col-md-6 ">
                    <div class="form-group ">
                        <label for="FirstName" class="blackText dinProStd font18">FIRST NAME</label>
                        <input name="FirstName" type="text" id="FirstName" class="form-control" placeholder="First Name"  tabindex="1"/>
                    </div>
                    <div class="form-group">
                        <label for="LastName" class="blackText dinProStd font18">LAST NAME</label>
                        <input name="LastName" type="text" id="LastName" class="form-control" placeholder="Last Name"  tabindex="3"/>
                        </div>
                    <div class="form-group">
                        <label for="Company" class="blackText dinProStd font18">COMPANY NAME</label>
                        <input name="Company" type="text" id="Company " class="form-control"  placeholder="Company" tabindex="5"/>
                    </div>
                      <div class="form-group">
                        <label for="Phone" class="blackText dinProStd font18">PHONE</label>
                        <input name="Phone" type="text" id="Phone" class="form-control" placeholder="Phone Number" tabindex="7" />
                    </div>
                </div>
             
                <div class="col-md-6">
                  
                    <div class="form-group ">
                        <label for="Email" class="blackText dinProStd font18">EMAIL</label>
                        <input name="Email" type="text" id="Email" class="form-control" placeholder="Email"  tabindex="2"/>
                    </div>
                    
                      <div class="form-group">
                        <label for="CompanyAddress" class="blackText dinProStd font18">ADDRESS 1</label>
                        <input name="CompanyAddress" type="text" id="CompanyAddress" class="form-control" placeholder="Address 1" tabindex="4"/>
                     </div>
                     
                      <div class="form-group">
                        <label for="Address2" class="blackText dinProStd font18">ADDRESS 2</label>
                        <input name="Address2" type="text" id="Address2" class="form-control" placeholder="Address 2" tabindex="6"/>
                     </div>
                     
                      <div class="form-group">
                        <label for="City" class="blackText dinProStd font18">CITY</label>
                        <input name="City" type="text" id="City" class="form-control" placeholder="City" tabindex="8"/>
                     </div>
                  </div>
            </div><!--row-->
            
           <div class="row">
                <div class="col-md-6 ">
                   
                   
                   
                     
                      <div class="form-group">
                        <label for="Country" class="blackText dinProStd font18">COUNTRY</label>
                        <input name="Country" type="text" id="Country" class="form-control" placeholder="Country" tabindex="9"/>
                     </div>
                     
                </div>
             
             
            </div><!--row--> 
            
           
                 
               <div class="col-md-12">
                <div id="frmSubmit" style="display:block;" class="text-center">
                    <p>
                       
                        <button id="submit" class="aboutButtonx"  type="submit">
                            Submit&nbsp;&nbsp;<i class="fa fa-arrow-right" aria-hidden="true"></i>
                        </button>
                    </p>
                </div>
                <div id="submitmsg">
                </div>
            </div>
</form>        
                    
                    
                <!--<form class="" name="registration" id="supportRegisterForm" action="" method="post">
                    <div class="customerForm flex">
                        <div class="customerFormCheck">
                            <h2 class="dinProStd greenText">Customer Information</h2>
                            <h2 class="dinProStd blackText">Are you an existing customer?</h2>
                            <div class="flex">
                                <div class="checkBox">
                                    <input type="checkbox" id="yes" name="yesCheck" value="Yes">
                                    <span class="checkmark"></span>
                                    <label for="yes">Yes</label>
                                </div>
                                <div class="checkBox">
                                    <input type="checkbox" id="no" name="noCheck" value="No" checked>
                                    <span class="checkmark"></span>
                                    <label for="no">No</label>
                                </div>
                            </div>
                        </div>
                        <div class="customerFormContent">
                            <p>All fields are required unless marked (optional). Non-US registrants: please select "NA"
                                for State / Province. NOTE: All information entered into this form is for your
                                individual MyAgilysys profile. If you need to update account information, such as a
                                company address, please contact your <a href="#">Financial Service Representative.</a>
                            </p>
                        </div>
                    </div>
                    <div class="customerFormLabel flex">
                        <div class="customerFormLabel1">
                            <label for="USER NAME" class="dinProStd  blackText">USER NAME</label>
                            <input type="text" id="userName" name="username" placeholder="Enter User Name" />
                        </div>
                        <div class="customerFormLabel2">
                            <label for="USER NAME" class="dinProStd  blackText">CUSTOMER NUMBER</label>
                            <input type="text" id="userNumber" class="number-only" name="customer_number"
                                placeholder="Enter Customer Number" />
                        </div>
                    </div>
                    <div class="customerFormLabel flex">
                        <div class="customerFormLabel3">
                            <label for="USER NAME" class="dinProStd  blackText">FIRST NAME</label>
                            <input type="text" id="firstName" name="first_name" placeholder="Enter First Name" />
                        </div>
                        <div class="customerFormLabel4">
                            <label for="USER NAME" class="dinProStd  blackText">LAST NAME</label>
                            <input type="text" id="lastName" name="last_name" placeholder="Enter Last Name" />
                        </div>
                    </div>
                    <div class="customerFormLabel">
                        <div class="customerFormLabel5">
                            <label for="USER NAME" class="dinProStd  blackText">EMAIL ADDRESS</label>
                            <input type="text" id="email" name="email" placeholder="Enter Email Address" />
                        </div>
                    </div>
                    <div class="customerFormCompany">
                        <h2 class="dinProStd greenText" class="dinProStd  blackText">Company Information</h2>
                        <div class="customerFormLabel">
                            <div class="customerFormLabel6">
                                <label for="USER NAME" class="dinProStd  blackText">COMPANY NAME</label>
                                <input type="text" id="companyName" name="company_name"
                                    placeholder="Enter Company Name" />
                            </div>
                        </div>
                        <div class="customerFormLabel flex">
                            <div class="customerFormLabel7">
                                <label for="USER NAME" class="dinProStd blackText">COMPANY ADDRESS</label>
                                <input type="text" id="companyAddress1" name="address"
                                    placeholder="Enter Company Address" />
                            </div>
                            <div class="customerFormLabel8">
                                <label for="USER NAME" class="dinProStd blackText">ADDRESS 2</label>
                                <input type="text" id="companyAddress2" name="address2" placeholder="Address 2" />
                            </div>
                        </div>
                        <div class="customerFormLabel flex">
                            <div class="customerFormLabel9">
                                <label for="USER NAME" class="dinProStd  blackText">CITY</label>
                                <input type="text" id="companyCity" name="city" placeholder="Enter Company Address" />
                            </div>
                            <div class="customerFormLabel10">
                                <label for="USER NAME" class="dinProStd  blackText">STATE/PROVINCE</label>
                                <input type="text" id="companyState" name="state" placeholder="Address 2" />
                            </div>
                        </div>
                        <div class="customerFormLabel flex">
                            <div class="customerFormLabel11">
                                <label for="USER NAME" class="dinProStd  blackText">ZIP/POSTAL CODE</label>
                                <input type="text" class="numbers-only" id="companyZip" name="zip"
                                    placeholder="Enter Company Address" />
                            </div>
                            <div class="customerFormLabel12">
                                <label for="USER NAME" class="dinProStd  blackText">COUNTRY</label>
                                <input type="text" id="companyCountry" name="country" placeholder="Country" />
                            </div>
                        </div>
                    </div>
                    <div class="customerFormProduct">
                        <h2 class="dinProStd greenText">Product Documentation</h2>
                        <div class="flex">
                            <div class="checkBox">
                                <input type="checkbox" id="lodging" name="doc[]" value="Lodging">
                                <label for="Lodging Management System">Lodging Management System</label>
                            </div>
                            <div class="checkBox">
                                <input type="checkbox" id="rGuestSeat" name="doc[]" value="rGuestSeat">
                                <label for="rGuest Seat">rGuest Seat</label>
                            </div>
                        </div>
                        <div class="flex">
                            <div class="checkBox">
                                <input type="checkbox" id="visual" name="doc[]" value="Visual">
                                <label for="Visual One">Visual One</label>
                            </div>
                            <div class="checkBox">
                                <input type="checkbox" id="rGuestPay" name="doc[]" value="rGuest Pay">
                                <label for="rGuest Pay">rGuest Pay</label>
                            </div>
                        </div>
                        <div class="flex">
                            <div class="checkBox">
                                <input type="checkbox" id="insight" name="doc[]" value="Insight">
                                <label for="Insight Mobile Manager">Insight Mobile Manager</label>
                            </div>
                            <div class="checkBox">
                                <input type="checkbox" id="rGuestAnalyze" name="doc[]" value="rGuest Analyze">
                                <label for="rGuest Analyze">rGuest Analyze</label>
                            </div>
                        </div>
                        <div class="flex">
                            <div class="checkBox">
                                <input type="checkbox" id="insight" name="doc[]" value="rGuest Stay">
                                <label for="rGuest Stay">rGuest Stay</label>
                            </div>
                            <div class="checkBox">
                                <input type="checkbox" id="eatec" name="doc[]" value="Eatec">
                                <label for="Eatec">Eatec</label>
                            </div>
                        </div>
                        <div class="flex">
                            <div class="checkBox">
                                <input type="checkbox" id="infoGenesis" name="doc[]" value="InfoGenesis">
                                <label for="InfoGenesis">InfoGenesis</label>
                            </div>
                            <div class="checkBox">
                                <input type="checkbox" id="stratton" name="doc[]" value="Stratton Warren System">
                                <label for="Stratton Warren System">Stratton Warren System</label>
                            </div>
                        </div>
                        <div class="flex">
                            <div class="checkBox">
                                <input type="checkbox" id="elevate" name="doc[]" value="Elevate">
                                <label for="Elevate">Elevate</label>
                            </div>
                            <div class="checkBox">
                                <input type="checkbox" id="dataMagine" name="doc[]" value="DataMagine">
                                <label for="DataMagine">DataMagine</label>
                            </div>
                        </div>
                        <div class="flex">
                            <div class="checkBox">
                                <input type="checkbox" id="rGuestBuy" name="doc[]" value="rGuest Buy">
                                <label for="rGuest Buy">rGuest Buy</label>
                            </div>
                        </div>

                        <label id="doc_error"></label>
                    </div>
                    <div class="customerFormSpecial">
                        <h2 class="dinProStd greenText">Special Offers &amp; Promotions</h2>
                        <p>Stay up to date</p>
                    </div>
                    <div class="customerFormButton">
                        <input class="" type="submit" id="supportRegPopupForm" value="Submit" />
                    </div>
                </form>-->
            </div>
        </div>
    </div>
</div>
<script>





jQuery(document).ready(function($) {


$.validator.addMethod("lettersonly", function(value, element) 
{
return this.optional(element) || /^[a-z," "]+$/i.test(value);
}, "Letters and spaces only please");   



    $("#frmRma").validate({
            onfocusout: false,

        rules: {

           FirstName:{
               required:true,
               lettersonly: true,        
           },
           LastName: {
               required:true,
               lettersonly: true,        
           },
           Company:{
               required:true,
               lettersonly: true,        
           },
           Phone: {
               
               required:true,
               number: true,
           },
           CompanyAddress:"required",
           Address2:"required",
           City:{
               required:true,
               lettersonly: true,        
           },
           Country:{
               required:true,
               lettersonly: true,        
           },
          Email: {
                required: true,
                email: true
            },
           
        },
        errorPlacement: function(error, element) {

           
                error.insertAfter(element);
           
        },

        messages: {
            
             FirstName:{
               required:"Please enter the First Name",
               lettersonly: "Please enter Letters only",        
           },
           LastName: {
               required:"Please enter the Last Name",
               lettersonly: "Please enter Letters only",       
           },
           Company:{
               required:"Please enter the Company Name",
               lettersonly: "Please enter Letters only",       
           },
           Phone: {
               required:"Please enter the Phone Number",
               number: "Please Enter Digits Only",    
           }, 
           CompanyAddress:"Please enter the Address",
           Address2:"Please enter the Address 2",
           City:{
               required:"Please enter the City",
               lettersonly: "Please enter Letters only",       
           },
           Country:{
               required:"Please enter the Country",
               lettersonly: "Please enter Letters only",       
           },
          Email: {
                required: "Please enter a valid Email Address",
                email: "Please enter a valid Email Address",
            },
        
       

        },


        submitHandler: function(form) {

          


            form.submit();



        }
    });
});

/*jQuery(document).ready(function($) {


    $("form[name='registration']").validate({

        rules: {

            username: "required",
            customer_number: "required",
            first_name: "required",
            last_name: "required",
            email: {
                required: true,
                email: true
            },
            company_name: "required",
            address: "required",
            address2: "required",
            city: "required",
            state: "required",
            zip: "required",
            country: "required",
            'doc[]': {
                required: true
            },

        },
        errorPlacement: function(error, element) {

            if (element.attr("name") == "doc[]") {
                error.appendTo($('#doc_error'));
            } else {
                error.insertAfter(element);
            }
        },

        messages: {
            username: "Please enter your username",
            customer_number: "Please enter your Customer Number",
            first_name: "Please enter your First Name",
            last_name: "Please enter your Last Name",
            email: "Please enter a valid email address",
            company_name: "Please enter your Company Name",
            address: "Please enter your Address",
            address2: "Please enter your Address2",
            city: "Please enter your City",
            state: "Please enter your State",
            zip: "Please enter your Zip",
            country: "Please enter your Country",
            'doc[]': {
                required: "Please enter Product Documentation",
            },
        },

        submitHandler: function(form) {



            form.submit();



        }
    });
});
*/
</script>


<?php
echo $args['after_widget'];
    }

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['title'] = strip_tags($new_instance['title']);
        $instance['description'] = strip_tags($new_instance['description']);
        $instance['second_desc'] = strip_tags($new_instance['second_desc']);
        $instance['signup_link'] = strip_tags($new_instance['signup_link']);
        $instance['forgot_password_link'] = strip_tags($new_instance['forgot_password_link']);
        $instance['support_image_uri0'] = (!empty($new_instance['support_image_uri0'])) ? strip_tags($new_instance['support_image_uri0']) : '';
//        $instance['support_image_uri'] = strip_tags($new_instance['support_image_uri']);
        $instance['support_icon_text'] = strip_tags($new_instance['support_icon_text']);
        $instance['support_icon_btn_url'] = strip_tags($new_instance['support_icon_btn_url']);

        $instance['signup_link_text'] = $new_instance['signup_link_text'];
        $instance['forgot_password_link_text'] = $new_instance['forgot_password_link_text'];
        $instance['online_support_text'] = $new_instance['online_support_text'];
        $instance['image_uri'] = $new_instance['image_uri'];
        $instance['image_uri_alt'] = $new_instance['image_uri_alt'];

        return $instance;
    }

    public function form($display_instance)
    {
        $widget_support_add_id = $this->get_field_id('') . "add";

        $title = ($display_instance['title']);
        $description = ($display_instance['description']);
        $second_desc = ($display_instance['second_desc']);
        $signup_link = ($display_instance['signup_link']);
        $support_icon_text = ($display_instance['support_icon_text']);

        $forgot_password_link = ($display_instance['forgot_password_link']);

        $support_icon_btn_url = ($display_instance['support_icon_btn_url']);

        $signup_link_text = $display_instance['signup_link_text'];
        $forgot_password_link_text = $display_instance['forgot_password_link_text'];
        $online_support_text = $display_instance['online_support_text'];

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('title') . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" type="text" value="' . $title . '" />';
        $rew_html .= '</p><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('description') . '"> ' . __('Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<textarea rows="4" cols="28"  id="' . $this->get_field_name('description') . '" name="' . $this->get_field_name('description') . '">' . $description . '</textarea>';
        $rew_html .= '</p><br><br><br><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('second_desc') . '"> ' . __('2nd Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<textarea rows="4" cols="28"  id="' . $this->get_field_name('second_desc') . '" name="' . $this->get_field_name('second_desc') . '">' . $second_desc . '</textarea>';
        $rew_html .= '</p><br><br><br><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('signup_link') . '"> ' . __('Signup Link', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('signup_link') . '" name="' . $this->get_field_name('signup_link') . '" type="text" value="' . $signup_link . '" />';
        $rew_html .= '</p><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('signup_link_text') . '"> ' . __('Signup Link Text', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('signup_link_text') . '" name="' . $this->get_field_name('signup_link_text') . '" type="text" value="' . $signup_link_text . '" />';
        $rew_html .= '</p><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('forgot_password_link') . '"> ' . __('Forgot Password Link', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('forgot_password_link') . '" name="' . $this->get_field_name('forgot_password_link') . '" type="text" value="' . $forgot_password_link . '" />';
        $rew_html .= '</p><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('forgot_password_link_text') . '"> ' . __('Forgot Password Link text', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('forgot_password_link_text') . '" name="' . $this->get_field_name('forgot_password_link_text') . '" type="text" value="' . $forgot_password_link_text . '" />';
        $rew_html .= '</p><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('online_support_text') . '"> ' . __('Online Support text', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('online_support_text') . '" name="' . $this->get_field_name('online_support_text') . '" type="text" value="' . $online_support_text . '" />';
        $rew_html .= '</p><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('support_icon_text') . '"> ' . __('Button Text', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('support_icon_text') . '" name="' . $this->get_field_name('support_icon_text') . '" type="text" value="' . $support_icon_text . '" />';
        $rew_html .= '</p><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('support_icon_btn_url') . '"> ' . __('Button URL', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('support_icon_btn_url') . '" name="' . $this->get_field_name('support_icon_btn_url') . '" type="text" value="' . $support_icon_btn_url . '" />';
        $rew_html .= '</p><br>';
        $i = 0;

        $rew_html .= '<div class="widg-img' . $i . '">';
        $show1 = (empty($display_instance['support_image_uri' . $i])) ? 'style="display:none;"' : '';
        $rew_html .= '<label id="support_image_uri_agilysys_support_widget"></label><br><img class="' . $this->get_field_id('support_image_uri' . $i) . '_media_image' . $i . ' custom_media_image' . $i . '" src="' . $display_instance['support_image_uri' . $i] . '" ' . $show1 . ' width=200" height="120"/>';
        $rew_html .= '<input type="hidden" class="' . $this->get_field_id('support_image_uri' . $i) . '_media_id' . $i . ' custom_media_id' . $i . '" name="' . $this->get_field_name('support_image_uri' . $i) . '" id="' . $this->get_field_id('support_image_uri' . $i) . '" value="' . $display_instance['support_image_uri' . $i] . '" />';
        $rew_html .= '<input type="hidden" class="' . $this->get_field_id('support_image_uri' . $i) . '_media_url' . $i . ' custom_media_url' . $i . '" name="' . $this->get_field_name('support_image_uri' . $i) . '" id="' . $this->get_field_id('support_image_uri' . $i) . '" value="' . $display_instance['support_image_uri' . $i] . '">';
        $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_upload' . $i . '" id="' . $this->get_field_id('support_image_uri' . $i) . '"/>';

        $rew_html .= '</div><br>';

        $rew_html .= '<label class="widg-label widg-img-label" for="' . $this->get_field_id('image_uri') . '">Icon Image</label>';
        $rew_html .= '<div class="widg-img" >';

        $rew_html .= '<label id="image_uri_agilysys_support_widget"></label><br><img class="' . $this->get_field_id('image_id') . '_media_image custom_media_image" src="' . $instance['image_uri'] . '" />';
        $rew_html .= '<input input type="hidden" type="text" class="' . $this->get_field_id('image_id') . '_media_id custom_media_id" name="' . $this->get_field_name('image_id') . '" id="' . $this->get_field_id('image_id') . '" value="' . $instance['image_id'] . '" />';
        $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id') . '_media_url custom_media_url" name="' . $this->get_field_name('image_uri') . '" id="' . $this->get_field_id('image_uri') . '" value="' . $instance['image_uri'] . '" >';
        $rew_html .= '<input type="button" value="Icon Image" class="button custom_media_upload" id="' . $this->get_field_id('image_id') . '"/>';
        $rew_html .= '</div><br><br><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt') . '"> ' . __('Image Alt', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt') . '" name="' . $this->get_field_name('image_uri_alt') . '" type="text" value="' . $display_instance['image_uri_alt'] . '">';
        $rew_html .= '</p><br>';

        ?>

<script>
jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    if (attachment.height == 1080 && attachment.width == 1920) {
                        jQuery('.' + button_id_s + '_media_id<?php echo $i; ?>').val(attachment.id);
                        jQuery('.' + button_id_s + '_media_url<?php echo $i; ?>').val(attachment
                            .url);
                        jQuery('.' + button_id_s + '_media_image<?php echo $i; ?>').attr('src',
                            attachment.url).css('display', 'block');

                        jQuery('#support_image_uri_agilysys_support_widget<?php echo $i; ?>')
                            .html("");
                    } else {
                        jQuery('#support_image_uri_agilysys_support_widget<?php echo $i; ?>')
                            .html("Please Enter the correct Dimensions 1920x1080").css('color',
                                'red');

                    }
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload<?php echo $i; ?>');

});


jQuery(document).ready(function() {
    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    if (attachment.height == 1080 && attachment.width == 1920) {
                        jQuery('.' + button_id_s + '_media_id').val(attachment.id);
                        jQuery('.' + button_id_s + '_media_url').val(attachment.url);
                        jQuery('.' + button_id_s + '_media_image').attr('src', attachment.url).css(
                            'display', 'block');

                        jQuery('#image_uri_agilysys_support_widget')
                            .html("");
                    } else {
                        jQuery('#image_uri_agilysys_support_widget')
                            .html("Please Enter the correct Dimensions 1920x1080").css('color',
                                'red');

                    }
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload');

});
</script>

<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_support_widget input,
select,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_support_widget label {
    width: 40%;
}

<?php echo '.'. $widget_support_add_id;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}


#entries_agilysys_support_widget {
    padding: 10px 0 0;
}

#entries_agilysys_support_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_support_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_support_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_support_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_support_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_support_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_support_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_support_widget #entries_agilysys_support_widget p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_support_widget">
    <?php echo $rew_html; ?>
</div>


<?php
}

}

?>
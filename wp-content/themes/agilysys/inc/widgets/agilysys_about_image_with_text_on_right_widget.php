<?php

function load_agilysys_about_image_with_text_on_right_widget()
{
    register_widget('agilysys_about_image_with_text_on_right_widget');
}

add_action('widgets_init', 'load_agilysys_about_image_with_text_on_right_widget');

class agilysys_about_image_with_text_on_right_widget extends WP_Widget
{

    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys About Image With Text On Right Widget', 'AGILYSYS_TEXT_DOMAIN'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {

        echo $args['before_widget'];

        $title = $instance['title'];
        $description = $instance['description'];
        $learn_more_text = $instance['learn_more_text'];

        $link_type = $instance['link_type'];
        if ($link_type == "link") {
            $link = $instance['link'];
        } elseif ($link_type == "page") {
            $post_id = $instance['page'];
            $post = get_post($post_id);
            $link = home_url($post->post_name) . "/";
        }
        $image_uri = $instance['image_uri'];
        $image_uri_alt = $instance['image_uri_alt'];
        ?>


<section class="aboutPartnership">
    <div class="row">
        <div class="aboutPartnershipImg col-12 col-sm-12 col-md-6 col-lg-6" data-aos="fade-right" data-aos-delay="300" data-aos-duration="400"
        data-aos-once="true" >
            <img class="img-fluid" src="<?php echo $image_uri; ?>" alt="<?php echo $image_uri_alt;?>" />
        </div>
        <div class="aboutPartnershipContent col-12 col-sm-12 col-md-6 col-lg-5" data-aos="fade-left" data-aos-delay="300" data-aos-duration="400"
        data-aos-once="true" >
            <h2 class="dinProStd greenText h2"><?php echo $title; ?></h2>
            <p><?php echo substr($description,0,650); ?></p>
            <a href="<?php echo $link; ?>" class="aboutButton violetText"> <?php echo $learn_more_text; ?> <i
                    class="fa fa-arrow-right" aria-hidden="true"></i></a>
        </div>
    </div>
</section>

<?php
echo $args['after_widget'];
    }

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['title'] = $new_instance['title'];
        $instance['description'] = $new_instance['description'];
        $instance['learn_more_text'] = $new_instance['learn_more_text'];
        $instance['image_uri'] = $new_instance['image_uri'];
        $instance['image_uri_alt'] = $new_instance['image_uri_alt'];
        $instance['link_type'] = $new_instance['link_type'];

        if ($new_instance['link_type'] == 'page') {
            $instance['page'] = $new_instance['page'];
            $instance['link'] = '';
        } elseif ($new_instance['link_type'] == 'link') {
            $instance['link'] = $new_instance['link'];
            $instance['page'] = '';

        }

        return $instance;
    }

    public function form($display_instance)
    {

        $widget_add_id_slider_client = $this->get_field_id('') . "add";

        $title = $display_instance['title'];
        $description = $display_instance['description'];
        $learn_more_text = $display_instance['learn_more_text'];
        $link = $display_instance['link'];
        $image_uri_alt = $display_instance['image_uri_alt'];

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('title') . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" type="text" value="' . $title . '" />';
        $rew_html .= '</p><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('description') . '"> ' . __('Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<textarea rows="6" cols="35" id="' . $this->get_field_id('description') . '" name="' . $this->get_field_name('description') . '" >' . $description . '</textarea>';
        $rew_html .= '</p><br><br><br>';

         $rew_html .= '<p>';
        // $show1 = (empty($display_instance['image_uri'])) ? 'style="display:none;"' : '';
        $rew_html .= '<label class="widg-label widg-img-label" for="' . $this->get_field_id('image_uri') . '">Image</label>';
        $rew_html .= '<div class="widg-img">';
        $rew_html .= '<label id="image_uri_agilysys_about_image_with_text_on_right_widget"></label><br><img width="200" height="120" class="' . $this->get_field_id('image_id') . '_media_image custom_media_image" src="' . $display_instance['image_uri'] . '" />';
        $rew_html .= '<input input type="hidden" type="text" class="' . $this->get_field_id('image_id') . '_media_id custom_media_id" name="' . $this->get_field_name('image_id') . '" id="' . $this->get_field_id('image_id') . '" value="' . $instance['image_id'] . '" />';
        $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id') . '_media_url custom_media_url" name="' . $this->get_field_name('image_uri') . '" id="' . $this->get_field_id('image_uri') . '" value="' . $instance['image_uri'] . '" >';
        $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_upload" id="' . $this->get_field_id('image_id') . '"/>';
        $rew_html .= '</div>';
     $rew_html .= '</p>';

        

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt') . '"> ' . __('Image Alt', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt') . '" name="' . $this->get_field_name('image_uri_alt') . '" type="text" value="' . $image_uri_alt . '" />';
        $rew_html .= '</p>';

        ?>

<script>
jQuery(document).ready(function() {
    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    if(attachment.height==1256 && attachment.width==1500)
                    {
                    jQuery('.' + button_id_s + '_media_id').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_url').val(attachment.url);
                    jQuery('.' + button_id_s + '_media_image').attr('src', attachment.url).css(
                        'display', 'block');
                        jQuery('#image_uri_agilysys_about_image_with_text_on_right_widget').html("");
                    }
                    else
                    {
                        jQuery('#image_uri_agilysys_about_image_with_text_on_right_widget').html("Please Enter the correct Dimensions 1500x1256").css('color','red');
                    }
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload');

});
</script>

<?php

   
     
        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('learn_more_text') . '"> ' . __('Learn More Text', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('learn_more_text') . '" name="' . $this->get_field_name('learn_more_text') . '" type="text" value="' . $learn_more_text . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('link_type') . '"> ' . __('Link', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<select id="' . $this->get_field_id('link_type') . '" name="' . $this->get_field_name('link_type') . '" onChange="show_hide_div_agilysys_about_image_with_text_on_right_widget(this.value);">';
        $rew_html .= '<option value="">Please Select</option>';

        $link_type = $display_instance['link_type'];

        if ($link_type == 'page') {
            $rew_html .= '<option value="page" selected="selected">Page</option>';
        } else {
            $rew_html .= '<option value="page">Page</option>';
        }

        if ($link_type == 'link') {
            $rew_html .= '<option value="link" selected="selected">Link</option>';
        } else {
            $rew_html .= '<option value="link">Link</option>';
        }

        $rew_html .= '</select>';
        $rew_html .= '</p>';

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block"';
            $show2 = 'style="display:none"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none"';
            $show2 = 'style="display:block"';

        } else {
            $show1 = 'style="display:none"';
            $show2 = 'style="display:none"';
        }
        $rew_html .= '<div id="page_div_agilysys_about_image_with_text_on_right_widget" ' . $show1 . '><p>';
        $rew_html .= '<label for="' . $this->get_field_id('page') . '"> ' . __('Page', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<select id="' . $this->get_field_id('page') . '" name="' . $this->get_field_name('page') . '">';
        $rew_html .= '<option value="">Please Select</option>';

        $page = $display_instance['page'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                $rew_html .= '<option value="' . $key->ID . '" selected="selected">' . $key->post_title . '</option>';
            } else {
                $rew_html .= '<option value="' . $key->ID . '">' . $key->post_title . '</option>';
            }

        }

        $rew_html .= '</select>';
        $rew_html .= '</p></div>';

        $rew_html .= '<div id="link_div_agilysys_about_image_with_text_on_right_widget" ' . $show2 . '><p>';
        $rew_html .= '<label for="' . $this->get_field_id('link') . '"> ' . __('Link', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('link') . '" name="' . $this->get_field_name('link') . '" type="text" value="' . $link . '" />';
        $rew_html .= '</p></div>';

        ?>
        <script>

function show_hide_div_agilysys_about_image_with_text_on_right_widget(val){
console.log(val);
    if(val=='page')
    {
         jQuery("#page_div_agilysys_about_image_with_text_on_right_widget").show();
         jQuery("#link_div_agilysys_about_image_with_text_on_right_widget").hide();
    }
    else if(val=='link')
    {
        jQuery("#page_div_agilysys_about_image_with_text_on_right_widget").hide();
         jQuery("#link_div_agilysys_about_image_with_text_on_right_widget").show();
    }

}

</script>


<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_about_image_with_text_on_right_widget  select{
     float: left;
    width: 60%;
    margin-top:20px !important;
    margin-bottom:10px !important;
}

#rew_container_agilysys_about_image_with_text_on_right_widget input,

textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_about_image_with_text_on_right_widget label {
    width: 40%;
     float: left;
}

#rew_container_agilysys_about_image_with_text_on_right_widget p {
    padding:10px !important;
}



<?php echo '.' . $widget_add_id_slider_client;

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}


#entries {
    padding: 10px 0 0;
}

#entries .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries .entrys:first-child {
    margin: 0;
}

#entries .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries .entry-title.active:after {
    content: '\f142';
}

#entries .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_about_image_with_text_on_right_widget #entries p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_about_image_with_text_on_right_widget">
    <?php echo $rew_html; ?>
</div>


<?php

    }

}
<?php

add_action('widgets_init', 'agilysys_solutions_testimonials_with_image_on_lefts');

function agilysys_solutions_testimonials_with_image_on_lefts()
{
    register_widget('agilysys_solutions_testimonials_with_image_on_left');
}

class agilysys_solutions_testimonials_with_image_on_left extends WP_Widget
{

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Solutions Testimonials Image on left', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
        add_action('load-widgets.php', array(&$this, 'my_custom_load'));
    }

    public function my_custom_load()
    {
        wp_enqueue_style('wp-color-picker');
        wp_enqueue_script('wp-color-picker');
    }
    public function widget($args, $instance)
    {

        echo $args['before_widget'];

        $background_color = !empty($instance['background_color']) ? $instance['background_color'] : '';
        $ag_solution_testimonial_img_lft_src = ($instance['ag_solution_testimonial_img_lft_src']);

        $image_uri_alt = $instance['ag_solution_testimonial_img_lft_src_alt'];
        $ag_solution_testimonial_img_lft_src_main = ($instance['ag_solution_testimonial_img_lft_src_main']);

        $image_uri_alt_main = $instance['ag_solution_testimonial_img_lft_src_main_alt'];
        $ag_solution_testimonial_img_lft_title = ($instance['ag_solution_testimonial_img_lft_title']);
        $ag_solution_testimonial_img_lft_desc = ($instance['ag_solution_testimonial_img_lft_desc']);
        $ag_solution_testimonial_img_lft_endorser = ($instance['ag_solution_testimonial_img_lft_endorser']);

        $ag_solution_testimonial_img_lft_title_desc = strip_tags($instance['ag_solution_testimonial_img_lft_title_desc']);
        ?>

<!--testimonial image on left -->
<section class="solutionTestimonialImageonLeft">

    <div class="solutionTestimonialTitle waterBG" style="background-color: <?php echo $background_color; ?>">
        <div class="row">
            <div class="solutionTestimonialsimageonleftTitle whiteText col-12 col-sm-12 col-md-10 col-lg-10">
                <h2 class="dinProStd "><?php echo $ag_solution_testimonial_img_lft_title; ?></h2>
                <p><?php echo $ag_solution_testimonial_img_lft_title_desc; ?></p>
            </div>
            <div class="solutionTestimonialImageonLeftTitleImg col-12 col-sm-12 col-md-2 col-lg-2">
                <img src="<?php echo $ag_solution_testimonial_img_lft_src; ?>" class="img-fluid" alt="<?php echo $image_uri_alt; ?>">
            </div>
        </div>
    </div>

    <div class="solutionTestimonialImageWithDesc">
        <div class="row">
            <div class="solutionTestimonialImage col-12 col-sm-12 col-md-6 col-lg-6">
                <img src="<?php echo $ag_solution_testimonial_img_lft_src_main; ?>" class="img-fluid"
                    alt="<?php echo $image_uri_alt_main; ?>">
            </div>
            <div class="solutionTestimonialDesc flex whiteText col-12 col-sm-12 col-md-6 col-lg-6">
                <div class="solutionTestimonialDoubleQuote">
                    <img src="<?php echo get_template_directory_uri() ?>/img/double-quote-top.png" class="img-fluid"
                        alt="double-quote">
                </div>
                <div class="solutionTestimonialDescHeading">
                    <h2 class="dinProStd"><?php echo $ag_solution_testimonial_img_lft_desc; ?></h2>
                </div>
                <div class="solutionTestimonialDescEndorser">
                    <p>-- <?php echo substr($ag_solution_testimonial_img_lft_endorser,0,168); ?></p>
                </div>
                <div class="solutionTestimonialDoubleQuoteBotom">
                    <img src="<?php echo get_template_directory_uri() ?>/img/double-quote-bottom.png" class="img-fluid"
                        alt="double-quote">
                </div>

            </div>
        </div>
    </div>

</section>

<?php
echo $args['after_widget'];
    }
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['ag_solution_testimonial_img_lft_src'] = strip_tags($new_instance['ag_solution_testimonial_img_lft_src']);
        $instance['ag_solution_testimonial_img_lft_src_main'] = strip_tags($new_instance['ag_solution_testimonial_img_lft_src_main']);

        $instance['ag_solution_testimonial_img_lft_src_alt'] = $new_instance['ag_solution_testimonial_img_lft_src_alt'];
        $instance['ag_solution_testimonial_img_lft_src_main_alt'] = $new_instance['ag_solution_testimonial_img_lft_src_main_alt'];

        $instance['ag_solution_testimonial_img_lft_title'] = strip_tags($new_instance['ag_solution_testimonial_img_lft_title']);
        $instance['ag_solution_testimonial_img_lft_desc'] = strip_tags($new_instance['ag_solution_testimonial_img_lft_desc']);
        $instance['ag_solution_testimonial_img_lft_endorser'] = strip_tags($new_instance['ag_solution_testimonial_img_lft_endorser']);
        $instance['ag_solution_testimonial_img_lft_title_desc'] = strip_tags($new_instance['ag_solution_testimonial_img_lft_title_desc']);
        $instance['background_color'] = $new_instance['background_color'];
        return $instance;
    }

    public function form($display_instance)
    {

        ?>

<script type='text/javascript'>
jQuery(document).ready(function($) {
    $('.my-color-picker').wpColorPicker();
});
</script>

<style>
    #entries label {
    width: 40%;
}

#entries input,
select,
textarea {
    float: right;
    width: 60%;
}

.wp-picker-container{
    margin-left:30%;
}
</style>

<?php
$ag_solution_testimonial_img_lft_src = ($display_instance['ag_solution_testimonial_img_lft_src']);
        $ag_solution_testimonial_img_lft_src_main = ($display_instance['ag_solution_testimonial_img_lft_src_main']);
        $ag_solution_testimonial_img_lft_title = ($display_instance['ag_solution_testimonial_img_lft_title']);
        $ag_solution_testimonial_img_lft_desc = ($display_instance['ag_solution_testimonial_img_lft_desc']);
        $ag_solution_testimonial_img_lft_endorser = ($display_instance['ag_solution_testimonial_img_lft_endorser']);
        $ag_solution_testimonial_img_lft_title_desc = ($display_instance['ag_solution_testimonial_img_lft_endorser']);

        $background_color = ($display_instance['background_color']);
        ?>
<div id="entries">
<p>
    <label
        for="<?php echo $this->get_field_id('background_color'); ?>"><?php _e('Border Color', $this->textdomain);?></label><br>
    <span><?php _e('The image border color', $this->textdomain);?></span>
    <input class="my-color-picker" type="text" id="<?php echo $this->get_field_id('background_color'); ?>"
        name="<?php echo $this->get_field_name('background_color'); ?>"
        value="<?php echo esc_attr($background_color); ?> " />
</p><br>
<p>
    <label
        for="<?php echo esc_attr($this->get_field_id('ag_solution_testimonial_img_lft_title')); ?>"><?php esc_attr_e('Title', 'vidget');?>:</label><br>
    <input class="widefat" id="<?php echo esc_attr($this->get_field_id('ag_solution_testimonial_img_lft_title')); ?>"
        name="<?php echo esc_attr($this->get_field_name('ag_solution_testimonial_img_lft_title')); ?>" type="text"
        value="<?php echo esc_attr($ag_solution_testimonial_img_lft_title); ?>">
</p><br>



<p>
    <label
        for="<?php echo esc_attr($this->get_field_id('ag_solution_testimonial_img_lft_title_desc')); ?>"><?php esc_attr_e('Description', 'vidget');?>:</label><br>
    <textarea 
        id="<?php echo esc_attr($this->get_field_id('ag_solution_testimonial_img_lft_title_desc')); ?>"
        name="<?php echo esc_attr($this->get_field_name('ag_solution_testimonial_img_lft_title_desc')); ?>"><?php echo esc_attr($ag_solution_testimonial_img_lft_title_desc); ?></textarea>
</p><br>

<p>
    <label class="widg-label widg-img-label"
        for="<?php echo $this->get_field_id('ag_solution_testimonial_img_lft_src'); ?>">Image(563x664)</label><br>
<div class="widg-img">
<label id="image_uri_agilysys_solutions_testimonials_with_image_on_left"></label><br><img class="<?php echo $this->get_field_id('image_id'); ?>_media_image custom_media_image"
        src="<?php if (!empty($display_instance['ag_solution_testimonial_img_lft_src'])) {echo $display_instance['ag_solution_testimonial_img_lft_src'];}?>"
        height="120" width="200" />
    <input input type="hidden" type="text"
        class="<?php echo $this->get_field_id('image_id'); ?>_media_id custom_media_id"
        name="<?php echo $this->get_field_name('image_id'); ?>" id="<?php echo $this->get_field_id('image_id'); ?>"
        value="<?php echo $display_instance['image_id']; ?>" />
    <input type="hidden" class="<?php echo $this->get_field_id('image_id'); ?>_media_url custom_media_url"
        name="<?php echo $this->get_field_name('ag_solution_testimonial_img_lft_src'); ?>"
        id="<?php echo $this->get_field_id('ag_solution_testimonial_img_lft_src'); ?>"
        value="<?php echo $display_instance['ag_solution_testimonial_img_lft_src']; ?>">
    <input type="button" value="Upload Image" class="button custom_media_upload"
        id="<?php echo $this->get_field_id('image_id'); ?>" />
</div>
</p><br>
<p>
    <label
        for="<?php echo esc_attr($this->get_field_id('ag_solution_testimonial_img_lft_src_alt')); ?>"><?php esc_attr_e('Image1 Alt', 'vidget');?>:</label><br>
    <input class="widefat" id="<?php echo esc_attr($this->get_field_id('ag_solution_testimonial_img_lft_src_alt')); ?>"
        name="<?php echo esc_attr($this->get_field_name('ag_solution_testimonial_img_lft_src_alt')); ?>" type="text"
        value="<?php echo esc_attr($ag_solution_testimonial_img_lft_src_alt); ?>">
</p><br>


<p>
    <label
        for="<?php echo esc_attr($this->get_field_id('ag_solution_testimonial_img_lft_desc')); ?>"><?php esc_attr_e('Description', 'vidget');?>:</label><br>
    <textarea  id="<?php echo esc_attr($this->get_field_id('ag_solution_testimonial_img_lft_desc')); ?>"
        name="<?php echo esc_attr($this->get_field_name('ag_solution_testimonial_img_lft_desc')); ?>"><?php echo esc_attr($ag_solution_testimonial_img_lft_desc); ?></textarea>
</p><br>

<p>
    <label
        for="<?php echo esc_attr($this->get_field_id('ag_solution_testimonial_img_lft_endorser')); ?>"><?php esc_attr_e('Endorser Name', 'vidget');?>:</label><br>
    <textarea
        id="<?php echo esc_attr($this->get_field_id('ag_solution_testimonial_img_lft_endorser')); ?>"
        name="<?php echo esc_attr($this->get_field_name('ag_solution_testimonial_img_lft_endorser')); ?>"><?php echo esc_attr($ag_solution_testimonial_img_lft_endorser); ?></textarea>
</p><br>


<p>
    <label class="widg-label widg-img-label"
        for="<?php echo $this->get_field_id('ag_solution_testimonial_img_lft_src_main'); ?>">Front Image (1200 x
        765)</label><br>
<div class="widg-img">
<label id="image_uri_agilysys_solutions_testimonials_with_image_on_left_main"></label><br><img class="<?php echo $this->get_field_id('image_id'); ?>_media_image1 custom_media_image1"
        src="<?php if (!empty($display_instance['ag_solution_testimonial_img_lft_src_main'])) {echo $display_instance['ag_solution_testimonial_img_lft_src_main'];}?>"
        height="120" width="200" />
    <input input type="hidden" type="text"
        class="<?php echo $this->get_field_id('image_id'); ?>_media_id1 custom_media_id1"
        name="<?php echo $this->get_field_name('image_id'); ?>" id="<?php echo $this->get_field_id('image_id'); ?>"
        value="<?php echo $display_instance['image_id']; ?>" />
    <input type="hidden" class="<?php echo $this->get_field_id('image_id'); ?>_media_url1 custom_media_url1"
        name="<?php echo $this->get_field_name('ag_solution_testimonial_img_lft_src_main'); ?>"
        id="<?php echo $this->get_field_id('ag_solution_testimonial_img_lft_src_main'); ?>"
        value="<?php echo $display_instance['ag_solution_testimonial_img_lft_src_main']; ?>">
    <input type="button" value="Upload Image" class="button custom_media_upload1"
        id="<?php echo $this->get_field_id('image_id'); ?>" />
</div>
</p><br>



<p>
    <label
        for="<?php echo esc_attr($this->get_field_id('ag_solution_testimonial_img_lft_src_main_alt')); ?>"><?php esc_attr_e('Image2 Alt', 'vidget');?>:</label><br>
    <input class="widefat" id="<?php echo esc_attr($this->get_field_id('ag_solution_testimonial_img_lft_src_main_alt')); ?>"
        name="<?php echo esc_attr($this->get_field_name('ag_solution_testimonial_img_lft_src_main_alt')); ?>" type="text"
        value="<?php echo esc_attr($ag_solution_testimonial_img_lft_src_main_alt); ?>">
</p><br>


</div>
<script>
jQuery(document).ready(function() {
    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    if (attachment.height == 1080 && attachment.width == 1920) {
                    jQuery('.' + button_id_s + '_media_id').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_url').val(attachment.url);
                    jQuery('.' + button_id_s + '_media_image').attr('src', attachment.url).css(
                        'display', 'block');
                        jQuery('#image_uri_agilysys_solutions_testimonials_with_image_on_left')
                            .html("");
                    } else {
                        jQuery('#image_uri_agilysys_solutions_testimonials_with_image_on_left')
                            .html("Please Enter the correct Dimensions 1920x1080").css('color',
                                'red');

                    }

                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload');


    function media_upload1(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload1', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    if (attachment.height == 1080 && attachment.width == 1920) {
                    jQuery('.' + button_id_s + '_media_id1').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_url1').val(attachment.url);
                    jQuery('.' + button_id_s + '_media_image1').attr('src', attachment.url).css(
                        'display', 'block');

                        jQuery('#image_uri_agilysys_solutions_testimonials_with_image_on_left_main')
                            .html("");
                    } else {
                        jQuery('#image_uri_agilysys_solutions_testimonials_with_image_on_left_main')
                            .html("Please Enter the correct Dimensions 1920x1080").css('color',
                                'red');

                    }   
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }

    media_upload1('.custom_media_upload1');
});
</script>

<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#entries_agilysys_solutions_testimonials_with_image_on_left select{
     float: left;
    width: 60%;
    margin-top:20px !important;
    margin-bottom:10px !important;
}

#entries_agilysys_solutions_testimonials_with_image_on_left input,
textarea {
    float: right;
    width: 60%;
}
 #entries_agilysys_solutions_testimonials_with_image_on_left label {
    width: 40%;
     float: left;
}

 #entries_agilysys_solutions_testimonials_with_image_on_left p{
    padding:10px !important;
}

<?php echo '.'. $widget_add_id_what_makes;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}

#entries_agilysys_solutions_testimonials_with_image_on_left #remove-img-bg {
    background: url('<?php echo ZWREW_URL; ?>assets/images/deleteimg.png') center center no-repeat;
    width: 20px;
    height: 22px;
    display: none;
}

#entries_agilysys_solutions_testimonials_with_image_on_left {
    padding: 10px 0 0;
}

#entries_agilysys_solutions_testimonials_with_image_on_left .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_solutions_testimonials_with_image_on_left .entrys:first-child {
    margin: 0;
}

#entries_agilysys_solutions_testimonials_with_image_on_left .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_solutions_testimonials_with_image_on_left .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_solutions_testimonials_with_image_on_left .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_solutions_testimonials_with_image_on_left .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_solutions_testimonials_with_image_on_left .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_solutions_testimonials_with_image_on_left #entries_agilysys_solutions_testimonials_with_image_on_left plast label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_solutions_testimonials_with_image_on_left">
    <?php echo $rew_html; ?>
</div>


<?php

       

    }
}
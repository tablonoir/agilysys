<?php

use PHPMailer\PHPMailer\PHPMailer;

require_once ABSPATH . WPINC . '/PHPMailer/PHPMailer.php';
require_once ABSPATH . WPINC . '/PHPMailer/Exception.php';

function load_myagilysys_registration_form_widget()
{
    register_widget('myagilysys_registration_form_widget');
}

add_action('widgets_init', 'load_myagilysys_registration_form_widget');

class myagilysys_registration_form_widget extends WP_Widget
{

    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('MyAgilysys Registration Form Widget', 'AGILYSYS_TEXT_DOMAIN'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {
 echo $args['before_widget'];
        $sub_title = $instance['sub_title'];
        $section_title = $instance['section_title'];
    
        
        $k = 0;

        ?>
       
        <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js">
        </script>
        <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/additional-methods.js">
        </script>
        
        <style id="compiled-css" type="text/css">
    
        
        .error {
            color: red;
        }
        
        #myform {
            margin: 10px;
        }
        
        #myform>div {
            margin-top: 10px;
        }
        
        /* EOS */
        </style>
        <?php


            
            

      

        ?>


<section class="rmaRequest center">
  
    <!--<h5 class="dinproBld blackText"><?php echo $sub_title; ?></h5>-->
    <!--<form class="rmaForm flex" id="myform1" method="post" action="">-->
    <!--    <input type="text" id="rma_Email" class="rma_Email" name="email" placeholder="Enter Email Address" />-->
    <!--    <input type="submit" id="rma_Submit" class="rma_Submit" name="rma_Submit" value="Submit" />-->
    <!--</form>-->
</section>




<section class="rmaRequest" id="frmRmaSection">
    
      <h2 class="dinProStd greenText center"><?php echo $section_title; ?></h2>
      
      <h3 class="dinProStd greenText center"><?php echo $sub_title;?></h3>
     
    <!--<button class="rma_Submit" onclick="document.location=document.location;">Start Over</button>-->
 <form method="post" class="" action="https://info.agilysys.com/l/76642/2020-12-28/5zcf8y" id="frmRma" style="padding:80px;">

<div class="row">
                <div class="col-md-6 ">
                    <div class="form-group ">
                        <label for="FirstName" class="blackText dinProStd font18">FIRST NAME</label>
                        <input name="FirstName" type="text" id="FirstName" class="form-control" placeholder="First Name"  tabindex="1"/>
                    </div>
                    <div class="form-group">
                        <label for="LastName" class="blackText dinProStd font18">LAST NAME</label>
                        <input name="LastName" type="text" id="LastName" class="form-control" placeholder="Last Name"  tabindex="3"/>
                        </div>
                    <div class="form-group">
                        <label for="Company" class="blackText dinProStd font18">COMPANY NAME</label>
                        <input name="Company" type="text" id="Company " class="form-control"  placeholder="Company" tabindex="5"/>
                    </div>
                      <div class="form-group">
                        <label for="Phone" class="blackText dinProStd font18">PHONE</label>
                        <input name="Phone" type="text" id="Phone" class="form-control" placeholder="Phone Number" tabindex="7" />
                    </div>
                </div>
             
                <div class="col-md-6">
                  
                    <div class="form-group ">
                        <label for="Email" class="blackText dinProStd font18">EMAIL</label>
                        <input name="Email" type="text" id="Email" class="form-control" placeholder="Email"  tabindex="2"/>
                    </div>
                    
                      <div class="form-group">
                        <label for="CompanyAddress" class="blackText dinProStd font18">ADDRESS 1</label>
                        <input name="CompanyAddress" type="text" id="CompanyAddress" class="form-control" placeholder="Address 1" tabindex="4"/>
                     </div>
                     
                      <div class="form-group">
                        <label for="Address2" class="blackText dinProStd font18">ADDRESS 2</label>
                        <input name="Address2" type="text" id="Address2" class="form-control" placeholder="Address 2" tabindex="6"/>
                     </div>
                     
                      <div class="form-group">
                        <label for="City" class="blackText dinProStd font18">CITY</label>
                        <input name="City" type="text" id="City" class="form-control" placeholder="City" tabindex="8"/>
                     </div>
                  </div>
            </div><!--row-->
            
           <div class="row">
                <div class="col-md-6 ">
                   
                   
                   
                     
                      <div class="form-group">
                        <label for="Country" class="blackText dinProStd font18">COUNTRY</label>
                        <input name="Country" type="text" id="Country" class="form-control" placeholder="Country" tabindex="9"/>
                     </div>
                     
                </div>
             
             
            </div><!--row--> 
            
           
                 
               <div class="col-md-12">
                <div id="frmSubmit" style="display:block;" class="text-center">
                    <p>
                       
                        <button id="submit" class="aboutButtonx"  type="submit">
                            Submit&nbsp;&nbsp;<i class="fa fa-arrow-right" aria-hidden="true"></i>
                        </button>
                    </p>
                </div>
                <div id="submitmsg">
                </div>
            </div>
</form>
</section>


<br><br><br>



<script>





jQuery(document).ready(function($) {


$.validator.addMethod("lettersonly", function(value, element) 
{
return this.optional(element) || /^[a-z," "]+$/i.test(value);
}, "Letters and spaces only please");   



    $("#frmRma").validate({
            onfocusout: false,

        rules: {

           FirstName:{
               required:true,
               lettersonly: true,        
           },
           LastName: {
               required:true,
               lettersonly: true,        
           },
           Company:{
               required:true,
               lettersonly: true,        
           },
           Phone: {
               
               required:true,
               number: true,
           },
           CompanyAddress:"required",
           Address2:"required",
           City:{
               required:true,
               lettersonly: true,        
           },
           Country:{
               required:true,
               lettersonly: true,        
           },
          Email: {
                required: true,
                email: true
            },
           
        },
        errorPlacement: function(error, element) {

           
                error.insertAfter(element);
           
        },

        messages: {
            
             FirstName:{
               required:"Please enter the First Name",
               lettersonly: "Please enter Letters only",        
           },
           LastName: {
               required:"Please enter the Last Name",
               lettersonly: "Please enter Letters only",       
           },
           Company:{
               required:"Please enter the Company Name",
               lettersonly: "Please enter Letters only",       
           },
           Phone: {
               required:"Please enter the Phone Number",
               number: "Please Enter Digits Only",    
           }, 
           CompanyAddress:"Please enter the Address",
           Address2:"Please enter the Address 2",
           City:{
               required:"Please enter the City",
               lettersonly: "Please enter Letters only",       
           },
           Country:{
               required:"Please enter the Country",
               lettersonly: "Please enter Letters only",       
           },
          Email: {
                required: "Please enter a valid Email Address",
                email: "Please enter a valid Email Address",
            },
        
       

        },


        submitHandler: function(form) {

          


            form.submit();



        }
    });
});
</script>


        <?php
echo $args['after_widget'];
    }

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['sub_title'] = strip_tags($new_instance['sub_title']);
        $instance['section_title'] = strip_tags($new_instance['section_title']);
        

        return $instance;
    }

    public function form($display_instance)
    {

        ?>
        <script type="text/javascript">
        jQuery(document).ready(function($) {
            jQuery('.color-picker').on('focus', function() {
                var parent = jQuery(this).parent();
                jQuery(this).wpColorPicker()
                parent.find('.wp-color-result').click();
            });
        });
        </script>


        <?php

        $section_title = $display_instance['section_title'];
        $sub_title = $display_instance['sub_title'];

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('section_title') . '"> ' . __('Section Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('section_title') . '" name="' . $this->get_field_name('section_title') . '" type="text" value="' . $section_title . '" />';
        $rew_html .= '</p><br><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('sub_title') . '"> ' . __('Sub Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('sub_title') . '" name="' . $this->get_field_name('sub_title') . '" type="text" value="' . $sub_title . '" />';
        $rew_html .= '</p><br><br>';

      

        ?>




<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container input,
select,
textarea {
    float: right;
    width: 60%;
}

#rew_container label {
    width: 40%;
}

<?php echo '.' . $widget_add_id_slider;

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}



#entries {
    padding: 10px 0 0;
}

#entries .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries .entrys:first-child {
    margin: 0;
}

#entries .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries .entry-title.active:after {
    content: '\f142';
}

#entries .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container #entries p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container">
    <?php echo $rew_html; ?>
</div>




<?php

    }

}

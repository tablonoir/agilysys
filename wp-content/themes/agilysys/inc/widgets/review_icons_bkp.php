<?php

function agilysys_reviews_icons_widget()
{
    register_widget('agilysys_reviews_icons_widgets');
}

add_action('widgets_init', 'agilysys_reviews_icons_widget');

class agilysys_reviews_icons_widgets extends WP_Widget
{
    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Review Icon Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {
        extract($args);
        $max_entries_row_icon = 5;

        ?>
<div class="homeLoaderIcon ">
     <div class="row">
    <?php for ($i = 0; $i < $max_entries_row_icon; $i++) {
            $block = $instance['block-' . $i];

            $review_icon_image = esc_url($instance['review_icon_image' . $i]);

            $link_type = $instance['link_type' . $i];
            if ($link_type == "link") {
                $review_icon_url = $instance['review_icon_url-' . $i];
            } elseif ($link_type == "page") {
                $post_id = $instance['page' . $i];
                $post = get_post($post_id);
                $review_icon_url = home_url($post->post_name) . "/";
            }
            // $review_icon_url = $instance['review_icon_url-' . $i];
            $review_icon_text = $instance['review_icon_text-' . $i];

            ?>
    <div class="col">
        <a href="<?php echo $review_icon_url; ?>" class="homeLoaderIconBox flex">
            <div class="homeLoaderImg">
                <img class="img-fluid" src="<?php echo $review_icon_image; ?>" alt="" />
            </div>

            <div class="homeLoaderText dinProStd whiteText">
                <h4><?php echo $review_icon_text; ?></h4>
            </div>
        </a>

    </div>
    <?php }?>
    </div>
</div>
<?php
echo $after_widget;
        ?>
<?php
}

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $max_entries_review_icon = 5;

        for ($i = 0; $i < $max_entries_review_icon; $i++) {
            $block = $new_instance['block-' . $i];
            $instance['review_icon_image' . $i] = strip_tags($new_instance['review_icon_image' . $i]);
            $instance['link_type' . $i] = $new_instance['link_type' . $i];
            if ($new_instance['link_type' . $i] == 'page') {

                $instance['page' . $i] = $new_instance['page' . $i];
                $instance['review_icon_url-' . $i] = '';
            } elseif ($new_instance['link_type' . $i] == 'link') {
                $instance['review_icon_url-' . $i] = $new_instance['review_icon_url-' . $i];
                $instance['page' . $i] = '';

            }
            $instance['review_icon_url-' . $i] = strip_tags($new_instance['review_icon_url-' . $i]);
            $instance['review_icon_text-' . $i] = strip_tags($new_instance['review_icon_text-' . $i]);
        }
        return $instance;
    }

    public function form($display_instance)
    {

        $max_entries_review_count = 5;

        $widget_add_id_review_ico = $this->get_field_id('') . "add";

        $rew_html .= '<div class="' . $widget_add_id_review_ico . '-input-containers"><div id="entries">';

        for ($i = 0; $i < 5; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Row', 'agilysys_text_domain') . ' </span>';
            $rew_html .= '<div class="entry-desc cf">';
            $rew_html .= '<input id="' . $this->get_field_id('block-' . $i) . '" name="' . $this->get_field_name('block-' . $i) . '" type="hidden" value="' . $display_instance['block-' . $i] . '">';
            /**
             * Block Caption
             */$display = (isset($display_instance['block-' . $i]) || ($display_instance['block-' . $i] == "")) ? 'style="display:block;"' : '';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('review_icon_text-' . $i) . '"> ' . __('Title', 'agilysys_text_domain') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('review_icon_text-' . $i) . '" name="' . $this->get_field_name('review_icon_text-' . $i) . '" type="text" value="' . $display_instance['review_icon_text-' . $i] . '">';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('link_type' . $i) . '"> ' . __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('link_type' . $i) . '" name="' . $this->get_field_name('link_type' . $i) . '" onChange="show_hide_div(this.value,' . $i . ');">';
            $rew_html .= '<option value="">Please Select</option>';

            $link_type = $display_instance['link_type' . $i];

            if ($link_type == 'page') {
                $rew_html .= '<option value="page" selected="selected">Internal Page Link</option>';
            } else {
                $rew_html .= '<option value="page">Internal Page Link</option>';
            }

            if ($link_type == 'link') {
                $rew_html .= '<option value="link" selected="selected">External Link</option>';
            } else {
                $rew_html .= '<option value="link">External Link</option>';
            }

            $rew_html .= '</select>';
            $rew_html .= '</p><br><br>';

            $args = array(
                'sort_order' => 'desc',
                'sort_column' => 'post_title',
                'hierarchical' => 1,
                'exclude' => '',
                'include' => '',
                'meta_key' => '',
                'meta_value' => '',
                'authors' => '',
                'child_of' => 0,
                'parent' => -1,
                'exclude_tree' => '',
                'number' => '',
                'offset' => 0,
                'post_type' => 'page',
                'post_status' => 'publish',
            );
            $pages = get_pages($args); // get all pages based on supplied args

            if ($link_type == 'page') {
                $show1 = 'style="display:block"';
                $show2 = 'style="display:none"';
            } elseif ($link_type == 'link') {
                $show1 = 'style="display:none"';
                $show2 = 'style="display:block"';

            } else {
                $show1 = 'style="display:none"';
                $show2 = 'style="display:none"';
            }
            $rew_html .= '<div id="page_div' . $i . '" ' . $show1 . '><p>';
            $rew_html .= '<label for="' . $this->get_field_id('page' . $i) . '"> ' . __('Page', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('page' . $i) . '" name="' . $this->get_field_name('page' . $i) . '">';
            $rew_html .= '<option value="">Please Select</option>';

            $page = $display_instance['page' . $i];

            foreach ($pages as $key) {

                if ($page == $key->ID) {
                    $rew_html .= '<option value="' . $key->ID . '" selected="selected">' . $key->post_title . '</option>';
                } else {
                    $rew_html .= '<option value="' . $key->ID . '">' . $key->post_title . '</option>';
                }

            }

            $rew_html .= '</select>';
            $rew_html .= '</p></div><br><br>';

            $rew_html .= '<div id="link_div' . $i . '" ' . $show2 . '><p>';
            $rew_html .= '<label for="' . $this->get_field_id('review_icon_url-' . $i) . '"> ' . __('URL', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('review_icon_url-' . $i) . '" name="' . $this->get_field_name('review_icon_url-' . $i) . '" type="text" value="' . $display_instance['review_icon_url-' . $i] . '" />';
            $rew_html .= '</p></div><br><br>';
            ?>
<script>
function show_hide_div(val, i) {

    if (val == 'page') {
        jQuery("#page_div" + i).show();
        jQuery("#link_div" + i).hide();
    } else if (val == 'link') {
        jQuery("#page_div" + i).hide();
        jQuery("#link_div" + i).show();
    }

}
</script>

<?php

            /* $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('review_icon_url-' . $i) . '"> ' . __('URL', 'agilysys_text_domain') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('review_icon_url-' . $i) . '" name="' . $this->get_field_name('review_icon_url-' . $i) . '" type="text" value="' . $display_instance['review_icon_url-' . $i] . '">';
            $rew_html .= '</p>'; */

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('review_icon_image' . $i) . '"> ' . __('Image', 'agilysys_text_domain') . ' :</label>';
            $rew_html .= '<div class="widg-img' . $i . '">';
            $show2 = (empty($display_instance['review_icon_image' . $i])) ? 'style="display:none;"' : '';
            $rew_html .= '<img class="' . $this->get_field_id('review_icon_image' . $i) . '_media_imageb' . $i . ' custom_media_imageb' . $i . '" src="' . $display_instance['review_icon_image' . $i] . '" ' . $show2 . ' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('review_icon_image' . $i) . '_media_idb' . $i . ' custom_media_idb' . $i . '" name="' . $this->get_field_name('review_icon_image' . $i) . '" id="' . $this->get_field_id('review_icon_image' . $i) . '" value="' . $display_instance['review_icon_image' . $i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('review_icon_image' . $i) . '_media_urlb' . $i . ' custom_media_urlb' . $i . '" name="' . $this->get_field_name('review_icon_image' . $i) . '" id="' . $this->get_field_id('review_icon_image' . $i) . '" value="' . $display_instance['review_icon_image' . $i] . '">';
            $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_uploadb' . $i . '" id="' . $this->get_field_id('review_icon_image' . $i) . '"/>';
            $rew_html .= '</div><br><br>';
            $rew_html .= '</p>';

            ?>

<script>
jQuery(document).ready(function() {


    function media_uploadb(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_uploadb<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {
                    jQuery('.' + button_id_s + '_media_idb<?php echo $i; ?>').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_urlb<?php echo $i; ?>').val(attachment.url);
                    jQuery('.' + button_id_s + '_media_imageb<?php echo $i; ?>').attr('src',
                        attachment.url).css('display', 'block');
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_uploadb('.custom_media_uploadb<?php echo $i; ?>');

});
</script> <?php

            $rew_html .= '</div></div>';

        }

        $rew_html .= '</div></div>';

        ?>
<script>
jQuery(document).ready(function(e) {
    jQuery.each(jQuery(".<?php echo $widget_add_id_review_ico; ?>-input-containers #entries").children(),
        function() {
            //                        alert(jQuery(this).find('input').val());
            if (jQuery(this).find('input').val() != '') {
                jQuery(this).show();
            }
        });
    jQuery(".<?php echo $widget_add_id_review_ico; ?>").bind('click', function(e) {
        var rows = 1;
        jQuery.each(jQuery(".<?php echo $widget_add_id_review_ico; ?>-input-containers #entries")
            .children(),
            function() {
                if (jQuery(this).find('input').val() == '') {
                    jQuery(this).find(".entry-title").addClass("active");
                    jQuery(this).find(".entry-desc").slideDown();
                    jQuery(this).find('input').first().val('0');
                    jQuery(this).show();
                    return false;
                } else {
                    rows++;
                    jQuery(this).show();
                    jQuery(this).find(".entry-title").removeClass("active");
                    jQuery(this).find(".entry-desc").slideUp();
                }
            });
        if (rows == '<?php echo $max_entries_review_count; ?>') {
            jQuery("#rew_container #message").show();
        }
    });
    jQuery(".delete-row").bind('click', function(e) {
        var count = 1;
        var current = jQuery(this).closest('.entrys').attr('id');
        jQuery.each(jQuery("#entries #" + current + " .entry-desc").children(), function() {
            jQuery(this).val('');
        });
        jQuery.each(jQuery("#entries #" + current + " .entry-desc p").children(), function() {
            jQuery(this).val('');
        });
        jQuery('#entries #' + current + " .entry-title").removeClass('active');
        jQuery('#entries #' + current + " .entry-desc").hide();
        jQuery('#entries #' + current).remove();
        jQuery.each(jQuery(".<?php echo $widget_add_id_review_ico; ?>-input-containers #entries")
            .children(),
            function() {
                if (jQuery(this).find('input').val() != '') {
                    jQuery(this).find('input').first().val(count);
                }
                count++;
            });
    });
});
</script>
<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container input,
select,
textarea {
    float: right;
    width: 60%;
}

#rew_container label {
    width: 40%;
}

<?php echo '.' . $widget_add_id_review_ico;

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}

#entries #remove-img-bg {
    background: url('<?php echo ZWREW_URL; ?>assets/images/deleteimg.png') center center no-repeat;
    width: 20px;
    height: 22px;
    display: none;
}

#entries {
    padding: 10px 0 0;
}

#entries .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries .entrys:first-child {
    margin: 0;
}

#entries .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries .entry-title.active:after {
    content: '\f142';
}

#entries .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container #entries plast label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container">
    <?php echo $rew_html; ?>
</div>
<?php
} //Function form ends here
}
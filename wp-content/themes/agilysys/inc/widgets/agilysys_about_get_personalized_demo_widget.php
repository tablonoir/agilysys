<?php
use PHPMailer\PHPMailer\PHPMailer;

require_once ABSPATH . WPINC . '/PHPMailer/PHPMailer.php';
require_once ABSPATH . WPINC . '/PHPMailer/Exception.php';

function load_agilysys_about_get_personalized_demo_widget()
{
    register_widget('agilysys_about_get_personalized_demo_widget');
}

add_action('widgets_init', 'load_agilysys_about_get_personalized_demo_widget');

class agilysys_about_get_personalized_demo_widget extends WP_Widget
{

    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys About Get Personalized Demo Widget', 'AGILYSYS_TEXT_DOMAIN'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {
echo $args['before_widget'];
        ?>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"
    type="text/javascript"></script>

        <?php

        /*if (isset($_POST) && !empty($_POST)) {

            $mail = new PHPMailer();

//$mail->isSMTP();

            $mail->SMTPDebug = false;

            $mail->Host = 'smtp.gmail.com';

            $mail->SMTPAuth = true;

            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = 587;

            $mail->Username = 'sau3334@gmail.com';

            $mail->Password = 'rwwtpnhahufyxqih';

            $mail->setFrom('testing@abiteoffrance.com', 'Testing');
            $mail->addReplyTo('sau3334@gmail.com', 'Saurav Daga');

            $mail->addAddress('saurav@tablonoir.com', 'Saurav Daga');

            $mail->Subject = 'New Enquiry';

            $email = sanitize_text_field($_POST['email']);
            $phoneNumber = sanitize_text_field($_POST['phoneNumber']);
            $firstName = sanitize_text_field($_POST['firstName']);
            $lastName = sanitize_text_field($_POST['lastName']);

            $jobTitle = sanitize_text_field($_POST['jobTitle']);
            $company = sanitize_text_field($_POST['company']);
            $industry = sanitize_text_field($_POST['industry']);

            $message = '';
            $message .= '<p>Email: ' . $email . '</p>';
            $message .= '<p>phoneNumber: ' . $phoneNumber . '</p>';
            $message .= '<p>firstName: ' . $firstName . '</p>';
            $message .= '<p>lastName: ' . $lastName . '</p>';
            $message .= '<p>jobTitle: ' . $jobTitle . '</p>';
            $message .= '<p>company: ' . $company . '</p>';
            $message .= '<p>industry: ' . $industry . '</p>';

            $mail->Body = $message;

            $mail->AltBody = 'New Enquiry';

            if (!$mail->send()) {
                echo 'Mailer Error: ' . $mail->ErrorInfo;
            } else {
                echo 'Message sent!';
            }

        }
        */

        $div_color = $instance['div_color'];
        $form_title = $instance['form_title'];
        $section_title = $instance['section_title'];

        ?>

<section class="aboutForm">
    <div class="aboutFormBox"  data-aos="fade-left" data-aos-delay="300" data-aos-duration="400" data-aos-once="true">
        <h3 class="whiteText greenBG" ><?php echo $form_title; ?></h3>
        <form class="" name="registration" action="http://info.agilysys.com/l/76642/2019-09-05/56y9pn" method="POST">
            <div class="lmsFormLabel flex">
                <div class="lmsLabelBox flex">
                    <label for="Email" class="dinProStd blackText">EMAIL ADDRESS</label>
                    <input type="text" id="about_email" name="email" placeholder="Enter Email Address" />
                </div>
                <div class="lmsLabelBox flex">
                    <label for="Phone Number" class="dinProStd blackText">PHONE NUMBER</label>
                    <input type="text" id="about_phoneNumber" name="phone_number" placeholder="Enter Phone Number" />
                </div>
            </div>
            <div class="lmsFormLabel flex">
                <div class="lmsLabelBox flex">
                    <label for="First Name" class="dinProStd blackText">FIRST NAME</label>
                    <input type="text" id="about_firstName" name="first_name" placeholder="Enter First Name" />
                </div>
                <div class="lmsLabelBox flex">
                    <label for="Last Name" class="dinProStd blackText">LAST NAME</label>
                    <input type="text" id="about_lastName" name="last_name" placeholder="Enter Last Name" />
                </div>
            </div>
            <div class="lmsFormLabel flex">
                <div class="lmsLabelBox flex">
                    <label for="Job Title" class="dinProStd blackText">Job Title</label>
                    <input type="text" id="about_jobTitle" name="jobTitle" placeholder="Enter Job Tile" />
                </div>
                <div class="lmsLabelBox flex">
                    <label for="Company" class="dinProStd blackText">Company</label>
                    <input type="text" id="about_company" name="company" placeholder="Enter Company Name" />
                </div>
            </div>
            <div class="lmsFormLabel flex">
                <div class="lmsLabelBox flex">
                    <label for="Industry" class="dinProStd blackText">INDUSTRY</label>
               
                    <select name="industry" id="about_insdustry">
                    <option value="">Please Select</option>    
                    <option value="Cruise">Cruise</option>
                    <option value="Education">Education</option>
                    <option value="Foodservice Management">Foodservice Management</option>
                    <option value="Gaming">Gaming</option>
                    <option value="Golf">Golf</option>
                    <option value="Spa">Spa</option>
                    <option value="Gaming - Tribal">Gaming - Tribal</option>
                    <option value="Healthcare">Healthcare</option>
                    <option value="Hotel/Resort">Hotel/Resort</option>
                    <option value="Restaurant">Restaurant</option>
                    <option value="Stadium">Stadium</option>
                    <option value="Other">Other</option>
                    </select>
                    
                    
                </div>
                <div class="lmsLabelBox flex">
                    <input type="submit" id="about_submit" value="Get My Demo" />
                </div>
            </div>
        </form>
    </div>
    <div class="aboutFormBG"  style="background:<?php echo $div_color; ?>">
        <h2 class="dinProStd whiteText" data-aos="fade-left" data-aos-delay="300" data-aos-duration="400" data-aos-once="true"><?php echo $section_title; ?></h2>
    </div>

</section>

<script>

jQuery(document).ready(function($) {


$("form[name='registration']").validate({

    rules: {

        
        email: {
            required: true,
            email: true
        },
        phone_number: "required",
        first_name:"required",
        last_name:"required",
        jobTitle:"required",
        company:"required",
        industry:"required"
    },

    messages: {
        email: "Please enter a valid email address",
        phone_number: "Please enter your Phone Number",
        first_name:"Please enter your First Name",
        last_name:"Please enter your Last Name",
        jobTitle:"Please enter your Job Title",
        company:"Please enter your Company Name",
        industry:"Please select Industry Name"
      
    },

    submitHandler: function(form) {


   
            form.submit();
       


    }
});
});


</script>


<?php

echo $args['after_widget'];
}

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['form_title'] = strip_tags($new_instance['form_title']);
        $instance['section_title'] = strip_tags($new_instance['section_title']);
        $instance['div_color'] = strip_tags($new_instance['div_color']);

        return $instance;
    }

    public function form($display_instance)
    {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                    jQuery('.color-picker').on('focus', function(){
                        var parent = jQuery(this).parent();
                        jQuery(this).wpColorPicker()
                        parent.find('.wp-color-result').click();
                    });
            });
        </script>


                <?php

        $section_title = $display_instance['section_title'];
        $form_title = $display_instance['form_title'];

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('section_title') . '"> ' . __('Section Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('section_title') . '" name="' . $this->get_field_name('section_title') . '" type="text" value="' . $section_title . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('div_color') . '">' . __('Div Color', 'AGILYSYS_TEXT_DOMAIN') . '</label>';
        $rew_html .= '<input class="widefat color-picker" id="' . $this->get_field_id('div_color') . '" name="' . $this->get_field_name('div_color') . '" type="text" value="' . esc_attr($div_color) . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('form_title') . '"> ' . __('Form Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('form_title') . '" name="' . $this->get_field_name('form_title') . '" type="text" value="' . $form_title . '" />';
        $rew_html .= '</p>';

        ?>


<style>

#rew_container p{
    padding:20px;
}

.wp-picker-container{
    margin-left:36% !important;
}
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container input,
select,
textarea {
    float: right;
    width: 60%;
}

#rew_container label {
    width: 40%;
}

<?php echo '.' . $widget_add_id_slider;

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}



#entries {
    padding: 10px 0 0;
}

#entries .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries .entrys:first-child {
    margin: 0;
}

#entries .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries .entry-title.active:after {
    content: '\f142';
}

#entries .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container #entries p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container">
    <?php echo $rew_html; ?>
</div>

<?php
}

}
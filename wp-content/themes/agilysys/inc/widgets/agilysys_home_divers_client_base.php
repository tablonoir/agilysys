<?php

function agilysys_leader_diverse_widget()
{
    register_widget('agilysys_home_divers_client_widget');
}
add_action('widgets_init', 'agilysys_leader_diverse_widget');

class agilysys_home_divers_client_widget extends WP_Widget
{
    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Our Diverse Client Base Widget', 'agilysys_text_domain'));

        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        ?>

<section class="homeOurDiverseSection">
    <div class="homeOurDiverseContent center">
        <h2 data-aos="fade-left" data-aos-delay="300" data-aos-duration="400" data-aos-once="true"
            class="greenText dinProStd">
            <?php echo $instance['diverse_title']; ?>
        </h2>
        <p class="dinproReg" data-aos="fade-right" data-aos-delay="300" data-aos-duration="400" data-aos-once="true">
            <?php echo substr($instance['diverse_desc'],0,120); ?>
        </p>

    </div>


    <div class="homeOurDiverseIcons flex">
        <?php
$count = count($instance['diverse_img_title']);

        $max = 15;
//                echo $diverse_count;
        for ($i = 0; $i < $count; $i++) {

            //Caption
            $caption = esc_attr($instance['diverse_img_title'][$i]);

            $link_type = $instance['link_type'][$i];
            if ($link_type == "link") {
                $diverse_img_url = $instance['diverse_url'][$i];
            } elseif ($link_type == "page") {
                $post_id = $instance['page'][$i];
                $post = get_post($post_id);
                $diverse_img_url = home_url($post->post_name) . "/";
            }

            //$diverse_img_url = esc_attr($instance['diverse_url-' . $i]);

            //Front Image
            $diverse_icon_image = esc_url($instance['diverse_icon_image'][$i]);
            $image_uri_alt = $instance['image_uri_alt'][$i];

            ?>
        <a href="<?php echo $diverse_img_url; ?>?q=<?php echo urlencode($caption); ?>" class="homeOurDiverseIconBox flex" data-aos="zoom-out"
            data-aos-delay="300" data-aos-duration="500" data-aos-once="true">
            <div class="homeOurImg">
                <img class="img-fluid colorImg" src="<?php echo $diverse_icon_image; ?>"
                    alt="<?php echo $image_uri_alt; ?>" />
            </div>
            <div class="homeOurtext">
                <h4 class="dinProStd"><?php echo $caption; ?></h4>
            </div>
        </a>

        <?php

        }
        ?>
    </div>


</section>


<?php
echo $args['after_widget'];
    }

    //Function form ends here

    public function update($new_instance, $old_instance)
    {

        $instance = array();
        $instance['diverse_title'] = strip_tags($new_instance['diverse_title']);
        $instance['diverse_desc'] = strip_tags($new_instance['diverse_desc']);
        $instance['what_makes_rows'] = strip_tags($new_instance['what_makes_rows']);
        $count = count($new_instance['diverse_img_title']);

        for ($i = 0; $i < $count; $i++) {

            $instance['diverse_img_title'][$i] = strip_tags($new_instance['diverse_img_title'][$i]);
            // $instance['diverse_url-' . $i] = strip_tags($new_instance['diverse_url-' . $i]);

            $instance['link_type'][$i] = $new_instance['link_type'][$i];
            if ($new_instance['link_type'][$i] == 'page') {

                $instance['page'][$i] = $new_instance['page'][$i];
                $instance['diverse_url'][$i] = '';
            } elseif ($new_instance['link_type'][$i] == 'link') {
                $instance['diverse_url'][$i] = $new_instance['diverse_url'][$i];
                $instance['page'][$i] = '';

            }
            $instance['diverse_icon_image'][$i] = strip_tags($new_instance['diverse_icon_image'][$i]);
            $instance['image_uri_alt'][$i] = strip_tags($new_instance['image_uri_alt'][$i]);

        }
        return $instance;
    }

    public function form($display_instance)
    {

        //$widget_add_id_diverse = $this->id . "-add";
        $widget_add_id_diverse = $this->get_field_id('') . "add";

        $diverse_title = ($display_instance['diverse_title']);
        $diverse_content = ($display_instance['diverse_desc']);
        if (!empty($display_instance['what_makes_rows'])) {
            $what_makes_rows = ($display_instance['what_makes_rows']);
        } else {
            $what_makes_rows = 0;
        }

        $max = 15;

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('diverse_title') . '"> ' . __('Title', 'agilysys_diverse') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('diverse_title') . '" name="' . $this->get_field_name('diverse_title') . '" type="text" value="' . $diverse_title . '" />';
        $rew_html .= '</p><br><br>';
        $rew_html .= '<label for="' . $this->get_field_id('diverse_desc') . '"> ' . __('Sub Title', 'agilysys_diverse') . ' :</label>';
        $rew_html .= '<textarea id="' . $this->get_field_id('diverse_desc') . '" name="' . $this->get_field_name('diverse_desc') . '" rows="3" cols="60" >' . $diverse_content . '</textarea>';
        $rew_html .= '</p><br><br><br>';

        $rew_html .= '<label for="' . $this->get_field_id('what_makes_rows') . '"> ' . __('No. of rows do you want to display*', 'agilysys_diverse') . ' :</label>';
        $rew_html .= '<input class="what_makes_rows" id="what_makes_rows" name="' . $this->get_field_name('what_makes_rows') . '" type="number" value="' . $what_makes_rows . '" />';
        $rew_html .= '</p><br>';

        $count = count($display_instance['diverse_img_title']);

        $rew_html .= '<div class="add_new_rowxx-input-containers"><div id="entries_agilysys_home_divers_client_widget">';

        $rew_html .= '<input class="cnt909" id="cnt909" name="cnt" type="hidden" value="' . $count . '">';

        for ($i = 0; $i < $count; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'agilysys_diverse') . ' </span>';

            $rew_html .= '<div class="entry-desc cf">';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('diverse_img_title-' . $i) . '"> ' . __('Icon Title', 'agilysys_diverse') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('diverse_img_title-' . $i) . '" name="' . $this->get_field_name('diverse_img_title[]') . '" type="text" value="' . $display_instance['diverse_img_title'][$i] . '">';
            $rew_html .= '</p>';

            $rew_html .= '<div class="widg-img' . $i . '">';
            $show1 = (empty($display_instance['diverse_icon_image'][$i])) ? 'style="display:none;"' : '';
            $rew_html .= '<label id="diverse_icon_image_agilysys_home_divers_client_widget'. $i . '"></label><br><img class="' . $this->get_field_id('image_id' . $i) . '_media_image' . $i . ' custom_media_image' . $i . '" src="' . $display_instance['diverse_icon_image'][$i] . '" ' . $show1 . ' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id' . $i) . '_media_id' . $i . ' custom_media_id' . $i . '" name="' . $this->get_field_name('image_id[]') . '" id="' . $this->get_field_id('image_id' . $i) . '" value="' . $display_instance['image_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id' . $i) . '_media_url' . $i . ' custom_media_url' . $i . '" name="' . $this->get_field_name('diverse_icon_image[]') . '" id="' . $this->get_field_id('diverse_icon_image-' . $i) . '" value="' . $display_instance['diverse_icon_image'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_upload' . $i . '" id="' . $this->get_field_id('image_id' . $i) . '"/>';

            $rew_html .= '</div>';

            ?>


<script>
jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    if (attachment.height == 390 && attachment.width == 443) {
                        jQuery('.' + button_id_s + '_media_id<?php echo $i; ?>').val(attachment.id);
                        jQuery('.' + button_id_s + '_media_url<?php echo $i; ?>').val(attachment
                            .url);
                        jQuery('.' + button_id_s + '_media_image<?php echo $i; ?>').attr('src',
                            attachment.url).css('display', 'block');

                        jQuery('#diverse_icon_image_agilysys_home_divers_client_widget<?php echo $i; ?>')
                            .html("");
                    } else {
                        jQuery('#diverse_icon_image_agilysys_home_divers_client_widget<?php echo $i; ?>')
                            .html("Please Enter the correct Dimensions 1920x1080").css(
                                'color', 'red');

                    }



                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload<?php echo $i; ?>');

});
</script>

<?php

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt' . $i) . '"> ' . __('Image Alt', 'agilysys_diverse') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt' . $i) . '" name="' . $this->get_field_name('image_uri_alt[]') . '" type="text" value="' . $display_instance['image_uri_alt'][$i] . '" />';
            $rew_html .= '</p>';

            $diverse_url = esc_attr($display_instance['diverse_url'][$i]);

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('link_type' . $i) . '"> ' . __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('link_type' . $i) . '" name="' . $this->get_field_name('link_type[]') . '" onChange="show_hide_div_agilysys_home_divers_client_widget(this.value,' . $i . ');">';
            $rew_html .= '<option value="">Please Select</option>';

            $link_type = $display_instance['link_type'][$i];

            if ($link_type == 'page') {
                $rew_html .= '<option value="page" selected="selected">Internal Page Link</option>';
            } else {
                $rew_html .= '<option value="page">Internal Page Link</option>';
            }

            if ($link_type == 'link') {
                $rew_html .= '<option value="link" selected="selected">External Link</option>';
            } else {
                $rew_html .= '<option value="link">External Link</option>';
            }

            $rew_html .= '</select>';
            $rew_html .= '</p><br><br>';

            $args = array(
                'sort_order' => 'desc',
                'sort_column' => 'post_title',
                'hierarchical' => 1,
                'exclude' => '',
                'include' => '',
                'meta_key' => '',
                'meta_value' => '',
                'authors' => '',
                'child_of' => 0,
                'parent' => -1,
                'exclude_tree' => '',
                'number' => '',
                'offset' => 0,
                'post_type' => 'page',
                'post_status' => 'publish',
            );
            $pages = get_pages($args); // get all pages based on supplied args

            if ($link_type == 'page') {
                $show1 = 'style="display:block"';
                $show2 = 'style="display:none"';
            } elseif ($link_type == 'link') {
                $show1 = 'style="display:none"';
                $show2 = 'style="display:block"';

            } else {
                $show1 = 'style="display:none"';
                $show2 = 'style="display:none"';
            }
            $rew_html .= '<div id="page_div_agilysys_home_divers_client_widget' . $i . '" ' . $show1 . '><p>';
            $rew_html .= '<label for="' . $this->get_field_id('page' . $i) . '"> ' . __('Page', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('page' . $i) . '" name="' . $this->get_field_name('page[]') . '">';
            $rew_html .= '<option value="">Please Select</option>';

            $page = $display_instance['page'][$i];

            foreach ($pages as $key) {

                if ($page == $key->ID) {
                    $rew_html .= '<option value="' . $key->ID . '" selected="selected">' . $key->post_title . '</option>';
                } else {
                    $rew_html .= '<option value="' . $key->ID . '">' . $key->post_title . '</option>';
                }

            }

            $rew_html .= '</select>';
            $rew_html .= '</p></div><br><br>';

            $rew_html .= '<div id="link_div_agilysys_home_divers_client_widget' . $i . '" ' . $show2 . '><p>';
            $rew_html .= '<label for="' . $this->get_field_id('diverse_url-' . $i) . '"> ' . __('URL', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('diverse_url-' . $i) . '" name="' . $this->get_field_name('diverse_url[]') . '" type="text" value="' . $display_instance['diverse_url'][$i] . '" />';
            $rew_html .= '</p></div><br><br>';
            ?>
<script>
function show_hide_div_agilysys_home_divers_client_widget(val, i) {
console.log(val);
    if (val == 'page') {
        jQuery("#page_div_agilysys_home_divers_client_widget" + i).show();
        jQuery("#link_div_agilysys_home_divers_client_widget" + i).hide();
    } else if (val == 'link') {
        jQuery("#page_div_agilysys_home_divers_client_widget" + i).hide();
        jQuery("#link_div_agilysys_home_divers_client_widget" + i).show();
    }

}
</script>

<?php

            /**
             * Read More Button
             */

            $k = $i + 1;
            $rew_html .= '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' . $k . ');">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';

        }
        $rew_html .= '</div></div>';

        $rew_html .= '<div class="' . $widget_add_id_diverse . '" style="margin-bottom: 36px;text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;" onClick="add_new_row();">' . __('ADD ROW', 'AGILYSYS_TEXT_DOMAIN') . '</div>';

        ?>
<script>
function add_new_row() {
    var cnt = '';

    jQuery.each(jQuery("#entries_agilysys_home_divers_client_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            cnt = jQuery(this).val();
        }
    });




    var what_makes_rows = jQuery('.what_makes_rows').val();


console.log(cnt);
    if (parseInt(cnt) < parseInt(what_makes_rows)) {

        cnt++;

        jQuery.each(jQuery("#entries_agilysys_home_divers_client_widget .cnt909"), function() {
            if (jQuery(this).val() != '') {
                jQuery(this).val(cnt);
            }
        });

        var new_row = '<div id="entry' + cnt +
            '"  class="entrys"><span class="entry-title" onclick = "slider(this);"><?php echo __('Add New Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span>';
        new_row += '<div class="entry-desc cf">';


        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Title', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('diverse_img_title[]')); ?>" type="text" value="">';
        new_row += '</p>';


        new_row += '<div class="widg-img' + cnt + '">';
        new_row += '<label id="diverse_icon_image_agilysys_home_divers_client_widget' + cnt +
            '"></label><br><img class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_image' + cnt +
            ' custom_media_image' + cnt + '" src="" style="display:none;" width=200" height="120"/>';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_id' +
            cnt + ' custom_media_id' + cnt + '" name="<?php echo $this->get_field_name('image_id[]'); ?>"  value="" />';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_url' +
            cnt + ' custom_media_url' + cnt +
            '" name="<?php echo $this->get_field_name('diverse_icon_image[]'); ?>" id="<?php echo $this->get_field_id('diverse_icon_image'); ?>' +
            cnt + '" value="">';
        new_row += '<input type="button" value="Upload Image" class="button custom_media_upload' + cnt +
            '" id="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '"/>';
        new_row += '</div><br><br>';


        jQuery(document).ready(function() {




            function media_upload(button_class) {
                var _custom_media = true,
                    _orig_send_attachment = wp.media.editor.send.attachment;
                jQuery('body').on('click', '.custom_media_upload' + cnt, function(e) {
                    var button_id = '#' + jQuery(this).attr('id');
                    var button_id_s = jQuery(this).attr('id');
                    console.log(button_id);
                    var self = jQuery(button_id);
                    var send_attachment_bkp = wp.media.editor.send.attachment;
                    var button = jQuery(button_id);
                    var id = button.attr('id').replace('_button', '');
                    _custom_media = true;

                    wp.media.editor.send.attachment = function(props, attachment) {
                        if (_custom_media) {

                            if (attachment.height == 390 && attachment.width == 443) {
                            jQuery('.' + button_id_s + '_media_id' + cnt).val(attachment.id);
                            jQuery('.' + button_id_s + '_media_url' + cnt).val(attachment.url);
                            jQuery('.' + button_id_s + '_media_image' + cnt).attr('src',
                                attachment.url).css('display', 'block');
                                jQuery('#diverse_icon_image_agilysys_home_divers_client_widget' + cnt)
                                    .html("");
                            }
                            else
                            {
                                jQuery('#diverse_icon_image_agilysys_home_divers_client_widget' + cnt)
                                    .html("Please Enter the correct Dimensions 1920x1080").css(
                                        'color', 'red');

                            }

                        } else {
                            return _orig_send_attachment.apply(button_id, [props, attachment]);
                        }
                    }
                    wp.media.editor.open(button);
                    return false;
                });
            }
            media_upload('.custom_media_upload' + cnt);

        });
        
         new_row += '<p>';
        new_row += '<label for=""><?php echo __('Image Alt', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('image_uri_alt[]')); ?>" type="text" value="">';
        new_row += '</p>';


        new_row += '<p>';
        new_row += '<label for="<?php echo $this->get_field_id('link_type'); ?>' + cnt +
            '"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
        new_row += '<select id="<?php echo $this->get_field_id('link_type'); ?>' + cnt +
            '" name="<?php echo $this->get_field_name('link_type[]'); ?>" onChange="show_hide_div_agilysys_home_divers_client_widget(this.value,' + cnt +
            ');">';
        new_row += '<option value="">Please Select</option>';
        new_row += '<option value="page">Internal Page Link</option>';
        new_row += '<option value="link">External Link</option>';
        new_row += '</select>';
        new_row += '</p><br><br>';


        <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        ?>






        new_row += '<div id="page_div_agilysys_home_divers_client_widget' + cnt + '" style="display:none;"><p>';
        new_row += '<label><?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';

        new_row += '<select name="<?php echo $this->get_field_name('page[]'); ?>">';
        new_row += '<option value="">Please Select</option>';

        <?php
foreach ($pages as $key) {
            ?>
        new_row += '<option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>';

        <?php
}
        ?>
        new_row += '</select>';
        new_row += '</p></div><br><br>';

        new_row += '<div id="link_div_agilysys_home_divers_client_widget' + cnt + '" style="display:none;"><p>';
        new_row += '<label><?php echo __('URL', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
        new_row += '<input  name="<?php echo $this->get_field_name('diverse_url[]'); ?>" type="text" value="" />';
        new_row += '</p></div><br><br>';


        var new_cnt = cnt;

        new_row += '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' + new_cnt +
            ');"><?php echo __('Delete Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span></a></p>';
        new_row += '</div></div>';

        jQuery('.add_new_rowxx-input-containers #entries_agilysys_home_divers_client_widget').append(new_row);

    }


}

function show_hide_div_agilysys_home_divers_client_widget(val, i) {
console.log(val);
    if (val == 'page') {
        jQuery("#page_div_agilysys_home_divers_client_widget" + i).show();
        jQuery("#link_div_agilysys_home_divers_client_widget" + i).hide();
    } else if (val == 'link') {
        jQuery("#page_div_agilysys_home_divers_client_widget" + i).hide();
        jQuery("#link_div_agilysys_home_divers_client_widget" + i).show();
    }

}

function delete_row(cnt) {
    jQuery.each(jQuery(".add_new_rowxx-input-containers #entries_agilysys_home_divers_client_widget"), function() {
        jQuery(' #entry' + cnt).remove();
    });
    var last_cnt = 0;

    jQuery.each(jQuery("#entries_agilysys_home_divers_client_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            last_cnt = jQuery(this).val();
        }
    });

    last_cnt--;
    jQuery.each(jQuery("#entries_agilysys_home_divers_client_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(last_cnt);
        }
    });
    jQuery(".what_makes_rows").val(last_cnt);
    jQuery('.what_makes_rows').trigger('change');

}
</script>
<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}


#rew_container_agilysys_home_divers_client_widget select {
    float: left;
    width: 60%;
    margin-top: 20px !important;
    margin-bottom: 10px !important;
}

#rew_container_agilysys_home_divers_client_widget input,

textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_home_divers_client_widget label {
    width: 40%;
    float: left;
}

#rew_container_agilysys_home_divers_client_widget p {
    padding:10px;
}

<?php echo '.' . $widget_add_id_diverse;

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}

#entries_agilysys_home_divers_client_widget #remove-img-bg {
    background: url('<?php echo ZWREW_URL; ?>assets/images/deleteimg.png') center center no-repeat;
    width: 20px;
    height: 22px;
    display: none;
}

#entries_agilysys_home_divers_client_widget {
    padding: 10px 0 0;
}

#entries_agilysys_home_divers_client_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_home_divers_client_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_home_divers_client_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_home_divers_client_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_home_divers_client_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_home_divers_client_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_home_divers_client_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_home_divers_client_widget #entries_agilysys_home_divers_client_widget plast label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_home_divers_client_widget">
    <?php echo $rew_html; ?>
</div>
<?php
} //Function form ends here
}
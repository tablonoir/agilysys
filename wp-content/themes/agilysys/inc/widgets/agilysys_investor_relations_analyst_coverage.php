<?php

add_action('widgets_init', 'agilysys_investor_relations_analyst_coverages');

function agilysys_investor_relations_analyst_coverages()
{
    register_widget('agilysys_investor_relations_analyst_coverage');
}

class agilysys_investor_relations_analyst_coverage extends WP_Widget
{

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Investor Relations Analyst Coverage', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');

        wp_enqueue_script('jquery-ui-datepicker');
        wp_register_style('jquery-ui', 'https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css');
        wp_enqueue_style('jquery-ui');

    }

    public function widget($args, $instance)
    {
       // echo $args['before_widget'];

        $section_title = $instance['section_title'];
        $what_makes_rows = $instance['what_makes_rows'];
        $max_entries_agilysys_investor_relations_analyst_coverage_slider_image = 15;
        /* echo $what_makes_rows; */
        ?>

<section class="investorInnerSection flex">
    <div class="investorInnerMainContent">
        <h2 class="dinProStd blackText h2"><?php echo $section_title; ?></h2>
        <?php

$count = count($instance['title_of_event']);
for ($i = 0; $i < $what_makes_rows; $i++) {
     

            $title_of_pdf = $instance['title_of_event'][$i];
            $sub_title = $instance['sub_title'][$i];
            $phone_number = $instance['phone_number'][$i];

            ?>
        <div class="investorInnerContent flex">
            <div class="investorInnerBox">
                <h5 class="greenText"><?php echo $title_of_pdf; ?></h5>
                <p class="blackText"><?php echo $sub_title; ?></p>
                <p>Phone:<a href="tel:<?php echo $phone_number; ?>"><?php echo $phone_number; ?></a></p>
            </div>
        </div>    
            <?php


        }
        ?>
        </div>


</section>

<?php
//echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $max_entries_agilysys_investor_relations_analyst_coverage_slider_image = 15;

        $instance['section_title'] = strip_tags($new_instance['section_title']);
        $instance['what_makes_rows'] = strip_tags($new_instance['what_makes_rows']);

        $count = count($new_instance['title_of_event']);

        for ($i = 0; $i < $count; $i++) {
          

                $instance['title_of_event'][$i] = $new_instance['title_of_event'][$i];
                $instance['sub_title'][$i] = strip_tags($new_instance['sub_title'][$i]);
                $instance['phone_number'][$i] = $new_instance['phone_number'][$i];
            
        }

        return $instance;

    }

    public function form($display_instance)
    {

        $max_entries_agilysys_investor_relations_analyst_coverage_slider_image = 15;

        $widget_add_id_slider = $this->get_field_id('') . "add_agilysys_news_events_blog_post_widget";
        $section_title = ($display_instance['section_title']);
        if (!empty($display_instance['what_makes_rows'])) {
            $what_makes_rows = ($display_instance['what_makes_rows']);
        } else {
            $what_makes_rows = 0;
        }

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('section_title') . '"> ' . __('Section Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('section_title') . '" name="' . $this->get_field_name('section_title') . '" type="text" value="' . $title . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('what_makes_rows') . '"> ' . __('No. of rows do you want to add*', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input class="what_makes_rows" id="' . $this->get_field_name('what_makes_rows') . '" name="' . $this->get_field_name('what_makes_rows') . '" type="number" value="' . $what_makes_rows . '" />';
        $rew_html .= '</p>';

        
        $count = count($display_instance['title_of_event']);

        $rew_html .= '<div class="add_new_rowxx-input-containers"><div id="entries_agilysys_investor_relations_analyst_coverage">';

        $rew_html .= '<input class="cnt909" id="cnt909" name="cnt" type="hidden" value="' . $count . '">';

        for ($i = 0; $i < $count; $i++) {

 
            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'AGILYSYS_TEXT_DOMAIN') . ' </span>';

            $rew_html .= '<div class="entry-desc cf">';
          

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('title_of_event' . $i) . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('title_of_event' . $i) . '" name="' . $this->get_field_name('title_of_event[]') . '" type="text" value="' . $display_instance['title_of_event'][$i] . '">';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('sub_title' . $i) . '"> ' . __('Sub Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('sub_title' . $i) . '" name="' . $this->get_field_name('sub_title[]') . '" type="text" value="' . $display_instance['sub_title'][$i] . '">';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('phone_number' . $i) . '"> ' . __('Phone no', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('phone_number' . $i) . '" name="' . $this->get_field_name('phone_number[]') . '" type="text" value="' . $display_instance['phone_number'][$i] . '">';
            $rew_html .= '</p>';

            $k = $i + 1;
            $rew_html .= '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' . $k . ');">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';

        }

        $rew_html .= '</div></div>';
        $rew_html .= '<div id="message">' . __('Sorry, you reached to the limit of', 'AGILYSYS_TEXT_DOMAIN') . ' "' . $what_makes_rows . '" ' . __('maximum entries_agilysys_investor_relations_analyst_coverage', 'AGILYSYS_TEXT_DOMAIN') . '.</div>';

        $rew_html .= '<div class="' . $widget_add_id_slider . '" style="text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;" onClick="add_new_row();">' . __('ADD ROW', 'AGILYSYS_TEXT_DOMAIN') . '</div>';
        ?>
<script>
jQuery(document).ready(function($) {
    $(".datepicker").datepicker({
        dateFormat: 'dd-mm-yy'
    });
});
</script>
<script>
function add_new_row() {
    var cnt = '';

    jQuery.each(jQuery("#entries_agilysys_investor_relations_analyst_coverage .cnt909"), function() {
        if (jQuery(this).val() != '') {
            cnt = jQuery(this).val();
        }
    });




    var what_makes_rows = jQuery('.what_makes_rows').val();

console.log(cnt);

   if (parseInt(cnt) < parseInt(what_makes_rows)) {   

        cnt++;

        jQuery.each(jQuery("#entries_agilysys_investor_relations_analyst_coverage .cnt909"), function() {
            if (jQuery(this).val() != '') {
                jQuery(this).val(cnt);
            }
        });

        var new_row = '<div id="entry' + cnt +
            '"  class="entrys"><span class="entry-title" onclick = "slider(this);"><?php echo __('Add New Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span>';
        new_row += '<div class="entry-desc cf">';


        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Title', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('title_of_event[]')); ?>" type="text" value="">';
        new_row += '</p>';

        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Sub Title', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('sub_title[]')); ?>" type="text" value="">';
        new_row += '</p>';
        

        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Phone no', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';

        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('phone_number[]')); ?>" type="text" value="">';
        new_row += '</p>';
       





        


     


        var new_cnt = cnt;

        new_row += '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' + new_cnt +
            ');"><?php echo __('Delete Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span></a></p>';
        new_row += '</div></div>';

        jQuery('.add_new_rowxx-input-containers #entries_agilysys_investor_relations_analyst_coverage').append(new_row);

    }


}



function delete_row(cnt) {
    jQuery.each(jQuery(".add_new_rowxx-input-containers #entries_agilysys_investor_relations_analyst_coverage"), function() {
        jQuery(' #entry' + cnt).remove();
    });
    var last_cnt = 0;

    jQuery.each(jQuery("#entries_agilysys_investor_relations_analyst_coverage .cnt909"), function() {
        if (jQuery(this).val() != '') {
            last_cnt = jQuery(this).val();
        }
    });

    last_cnt--;
    jQuery.each(jQuery("#entries_agilysys_investor_relations_analyst_coverage .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(last_cnt);
        }
    });
    jQuery(".what_makes_rows").val(last_cnt);
    jQuery('.what_makes_rows').trigger('change');

}

</script>
<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_investor_relations_analyst_coverage input,
select,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_investor_relations_analyst_coverage label {
    width: 40%;
}

<?php echo '.'. $widget_add_id_slider;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}



#entries_agilysys_investor_relations_analyst_coverage {
    padding: 10px 0 0;
}

#entries_agilysys_investor_relations_analyst_coverage .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_investor_relations_analyst_coverage .entrys:first-child {
    margin: 0;
}

#entries_agilysys_investor_relations_analyst_coverage .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_investor_relations_analyst_coverage .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_investor_relations_analyst_coverage .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_investor_relations_analyst_coverage .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_investor_relations_analyst_coverage .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_investor_relations_analyst_coverage p {
 padding:10px !important;
}

#rew_container_agilysys_investor_relations_analyst_coverage #entries_agilysys_investor_relations_analyst_coverage p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_investor_relations_analyst_coverage">
    <?php echo $rew_html; ?>
</div>

<?php
}

}
<?php

function agilysys_pos_3_column_sliders()
{
    register_widget('agilysys_pos_3_column_sliders');
}

add_action('widgets_init', 'agilysys_pos_3_column_sliders');

class agilysys_pos_3_column_sliders extends WP_Widget
{
    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys POS 3 columns slider', 'agilysys_text_domain'));

        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
        // add_action('load-widgets.php', array(&$this, 'my_custom_load'));
    }

    public function my_custom_load()
    {
        wp_enqueue_style('wp-color-picker');
        wp_enqueue_script('wp-color-picker');
    }
    /**
     * @see WP_Widget::widget -- do not rename this         * This is for front end
     */
    public function widget($args, $instance)
    {

        echo $args['before_widget'];
        extract($args);

        $bg_color = !empty($instance['background_color']) ? $instance['background_color'] : '';

        $text_color = !empty($instance['text_color']) ? $instance['text_color'] : '';
        $text_hover_color = !empty($instance['text_hover_color']) ? $instance['text_hover_color'] : '';

        $max_entries_agilysys_pos_3_column_sliders_solution_image_slider_count = 15;
        $solution_3_column_title = (isset($instance['solution_3_column_title'])) ? $instance['solution_3_column_title'] : "";
        $rand = rand(0, 99999);
        ?>

        <style>

            #posIconBoxText_<?php echo $rand;?> {
                color:<?php echo $text_color; ?> !important;
            }

            #posIconBoxText_<?php echo $rand;?>:hover{
                color:<?php echo $text_hover_color; ?> !important;
            }

        </style>

<section class="posIcon flex" style="background:<?php echo $bg_color; ?>;">
    <?php
$count = count($instance['image_uri']);
        for ($i = 0; $i < $count; $i++) {

            $solution_3_column_sub_title = $instance['solution_3_column_sub_title'][$i];
            $solution_3_column_slider_image = esc_attr($instance['image_uri'][$i]);
            $image_uri_alt = $instance['image_uri_alt'][$i];

            $solution_3_column_slider_image_hover = esc_attr($instance['image_urix'][$i]);

            $image_uri_alt1 = $instance['image_uri_alt1'][$i];
            ?>
    <div class="posIconBox flex">
        <div class="posIconBoxImg">
            <img class="img-fluid normalImg" src="<?php echo $solution_3_column_slider_image; ?>"
                alt="<?php echo $image_uri_alt; ?>" />
            <img class="img-fluid hoverImg" src="<?php echo $solution_3_column_slider_image_hover; ?>"
                alt="<?php echo $image_uri_alt1; ?>" />
        </div>
        <div class="posIconBoxText" id="posIconBoxText_<?php echo $rand;?>">
            <p><?php echo $solution_3_column_sub_title; ?></p>
        </div>
    </div>
    <?php
}
        ?>

</section>



<?php
echo $args['after_widget'];
    }
    //Function widget ends here

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['solution_3_column_title'] = strip_tags($new_instance['solution_3_column_title']);
        $instance['background_color'] = $new_instance['background_color'];
        $instance['text_color'] = $new_instance['text_color'];
        $instance['text_hover_color'] = $new_instance['text_hover_color'];

        $count = count($new_instance['image_uri']);
        for ($i = 0; $i < $count; $i++) {

            $instance['solution_3_column_sub_title'][$i] = strip_tags($new_instance['solution_3_column_sub_title'][$i]);
            $instance['image_uri'][$i] = strip_tags($new_instance['image_uri'][$i]);
            $instance['image_uri_alt'][$i] = strip_tags($new_instance['image_uri_alt'][$i]);

            $instance['image_urix'][$i] = strip_tags($new_instance['image_urix'][$i]);
            $instance['image_uri_alt1'][$i] = strip_tags($new_instance['image_uri_alt1'][$i]);

        }
        return $instance;
    }
    //Function update ends here

    /**
     * @see WP_Widget::form -- do not rename this
     */
    public function form($display_instance)
    {
        ?>
<script type='text/javascript'>
jQuery(document).ready(function($) {
    $('.my-color-picker').wpColorPicker();
});
</script>


<?php
$max_entries_agilysys_pos_3_column_sliders_solutions_slider = 15;

//                print_r($max_entries_agilysys_pos_3_column_sliders_solutions_slider);

        $rand = rand(0, 99999);

        $widget_add_id_solution_slider = $this->get_field_id('') . "add_agilysys_pos_3_column_sliders_" . $rand;
        $solution_3_column_title = ($display_instance['solution_3_column_title']);

        $background_color = ($display_instance['background_color']);
        $text_color = ($display_instance['text_color']);

        $text_hover_color = ($display_instance['text_hover_color']);

        $rew_html = '<p><label for="' . $this->get_field_id('background_color') . '"> ' . __('Background Color', 'agilysys_text_domain') . ' :</label>';
        $rew_html .= '<input  class="my-color-picker" id="' . $this->get_field_id('background_color') . '" name="' . $this->get_field_name('background_color') . '" type="text" value="' . $background_color . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p><label for="' . $this->get_field_id('text_color') . '" style="margin-right:35px !important;"> ' . __('Text Color', 'agilysys_text_domain') . ' :</label>';
        $rew_html .= '<input  class="my-color-picker" id="' . $this->get_field_id('text_color') . '" name="' . $this->get_field_name('text_color') . '" type="text" value="' . $text_color . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p><label for="' . $this->get_field_id('text_hover_color') . '"> ' . __('Text Hover Color', 'agilysys_text_domain') . ' :</label>';
        $rew_html .= '<input  class="my-color-picker" id="' . $this->get_field_id('text_hover_color') . '" name="' . $this->get_field_name('text_hover_color') . '" type="text" value="' . $text_hover_color . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('solution_3_column_title') . '"> ' . __('Title', 'agilysys_text_domain') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('solution_3_column_title') . '" name="' . $this->get_field_name('solution_3_column_title') . '" type="text" value="' . $solution_3_column_title . '" />';
        $rew_html .= '</p><br>';

        $count = count($display_instance['image_uri']);

        $rew_html .= '<div class="add_new_rowxx-input-containers"><div id="entries_agilysys_pos_3_column_sliders_'.$rand.'">';

        $rew_html .= '<input class="cnt909" id="cnt909" name="cnt" type="hidden" value="' . $count . '">';

        for ($i = 0; $i < $count; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'agilysys_text_domain') . ' </span>';
            $rew_html .= '<div class="entry-desc cf">';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('image_uri' . $i) . '">' . __(' Slider Image', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';

            $rew_html .= '<div class="widg-img' . $i . '">';
            $show1 = (!empty($display_instance['image_uri'][$i])) ? 'style="display:block;"' : '';
            $rew_html .= '<label id="image_uri_agilysys_pos_3_column_sliders' . $i . '"></label><br><img  class="' . $this->get_field_id('image_id' . $i) . '_media_image' . $i . ' custom_media_image' . $i . '" src="' . $display_instance['image_uri'][$i] . '" ' . $show1 . ' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id' . $i) . '_media_id' . $i . ' custom_media_id' . $i . '" name="' . $this->get_field_name('image_id[]') . '" id="' . $this->get_field_id('image_id' . $i) . '" value="' . $display_instance['image_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id' . $i) . '_media_url' . $i . ' custom_media_url' . $i . '" name="' . $this->get_field_name('image_uri[]') . '" id="' . $this->get_field_id('image_uri' . $i) . '" value="' . $display_instance['image_uri'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_upload' . $i . '" id="' . $this->get_field_id('image_id' . $i) . '"/>';

            $rew_html .= '</div>';
            $rew_html .= '</p><br><br>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt' . $i) . '"> ' . __('Image Alt', 'agilysys_text_domain') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt' . $i) . '" name="' . $this->get_field_name('image_uri_alt[]') . '" type="text" value="' . $display_instance['image_uri_alt'][$i] . '" />';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('image_urix' . $i) . '">' . __(' Hover Image', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';

            $rew_html .= '<div class="widg-imgx' . $i . '">';
            $show1 = (!empty($display_instance['image_urix'][$i])) ? 'style="display:block;"' : '';
            $rew_html .= '<label id="image_urix_agilysys_pos_3_column_sliders' . $i . '"></label><br><img class="' . $this->get_field_id('image_idx' . $i) . '_media_imagex' . $i . ' custom_media_imagex' . $i . '" src="' . $display_instance['image_urix'][$i] . '" ' . $show1 . ' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_idx' . $i) . '_media_idx' . $i . ' custom_media_idx' . $i . '" name="' . $this->get_field_name('image_idx[]') . '" id="' . $this->get_field_id('image_idx' . $i) . '" value="' . $display_instance['image_idx'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_idx' . $i) . '_media_urlx' . $i . ' custom_media_urlx' . $i . '" name="' . $this->get_field_name('image_urix[]') . '" id="' . $this->get_field_id('image_urix' . $i) . '" value="' . $display_instance['image_urix'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_uploadx' . $i . '" id="' . $this->get_field_id('image_idx' . $i) . '"/>';

            $rew_html .= '</div>';
            $rew_html .= '</p><br><br>';
            ?>


<script>
jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    //if (attachment.height == 150 && attachment.width == 150) {
                        jQuery('.' + button_id_s + '_media_id<?php echo $i; ?>').val(attachment.id);
                        jQuery('.' + button_id_s + '_media_url<?php echo $i; ?>').val(attachment
                            .url);
                        jQuery('.' + button_id_s + '_media_image<?php echo $i; ?>').attr('src',
                            attachment.url).css('display', 'block');

                        /*jQuery('#image_uri_agilysys_pos_3_column_sliders<?php echo $i; ?>')
                            .html("");
                    } else {
                        jQuery('#image_uri_agilysys_pos_3_column_sliders<?php echo $i; ?>')
                            .html("Please Enter the correct Dimensions 150x150").css(
                                'color', 'red');

                    }
                    */
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload<?php echo $i; ?>');



    function media_uploadx(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_uploadx<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    //if (attachment.height == 350 && attachment.width == 350) {
                        jQuery('.' + button_id_s + '_media_idx<?php echo $i; ?>').val(attachment
                            .id);
                        jQuery('.' + button_id_s + '_media_urlx<?php echo $i; ?>').val(attachment
                            .url);
                        jQuery('.' + button_id_s + '_media_imagex<?php echo $i; ?>').attr('src',
                            attachment.url).css('display', 'block');


                       /* jQuery('#image_urix_agilysys_pos_3_column_sliders<?php echo $i; ?>')
                            .html("");
                    } else {
                        jQuery('#image_urix_agilysys_pos_3_column_sliders<?php echo $i; ?>')
                            .html("Please Enter the correct Dimensions 350x350").css(
                                'color', 'red');

                    }
                    */
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_uploadx('.custom_media_uploadx<?php echo $i; ?>');

});
</script>

<?php

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt1' . $i) . '"> ' . __('Image Alt', 'agilysys_text_domain') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt1' . $i) . '" name="' . $this->get_field_name('image_uri_alt1[]') . '" type="text" value="' . $display_instance['image_uri_alt1'][$i] . '" />';
            $rew_html .= '</p><br>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('solution_3_column_sub_title' . $i) . '"> ' . __('Title', 'agilysys_text_domain') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('solution_3_column_sub_title' . $i) . '" name="' . $this->get_field_name('solution_3_column_sub_title[]') . '" type="text" value="' . $display_instance['solution_3_column_sub_title'][$i] . '">';
            $rew_html .= '</p>';

            $k = $i + 1;
            $rew_html .= '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' . $k . ');">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';

        }

        $rew_html .= '</div></div>';
        $rew_html .= '<div id="message">' . __('Sorry, you reached to the limit of', 'agilysys_text_domain') . ' "' . $max_entries_agilysys_pos_3_column_sliders_solutions_slider . '" ' . __('maximum entries_agilysys_pos_3_column_sliders', 'agilysys_text_domain') . '.</div>';

        $rew_html .= '<div class="' . $widget_add_id_solution_slider . '" style="margin-bottom: 36px;text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;" onClick="add_new_row();">' . __('ADD ROW', 'AGILYSYS_TEXT_DOMAIN') . '</div>';
        ?>
<script>
function add_new_row() {
    var cnt = '';

    jQuery.each(jQuery("#entries_agilysys_pos_3_column_sliders_<?php echo $rand;?> .cnt909"), function() {
        if (jQuery(this).val() != '') {
            cnt = jQuery(this).val();
        }
    });






    cnt++;

    jQuery.each(jQuery("#entries_agilysys_pos_3_column_sliders_<?php echo $rand;?> .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(cnt);
        }
    });
console.log(cnt);
    var new_row = '<div id="entry' + cnt +
        '"  class="entrys"><span class="entry-title" onclick = "slider(this);"><?php echo __('Add New Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span>';
    new_row += '<div class="entry-desc cf">';




    new_row += '<div class="widg-img' + cnt + '">';
    new_row += '<label id="image_uri_agilysys_pos_3_column_sliders' + cnt +
        '"></label><br><img class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_image' + cnt +
        ' custom_media_image' + cnt + '" src="" style="display:none;" width=200" height="120"/>';
    new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_id' +
        cnt + ' custom_media_id' + cnt + '" name="<?php echo $this->get_field_name('image_id[]'); ?>"  value="" />';
    new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_url' +
        cnt + ' custom_media_url' + cnt +
        '" name="<?php echo $this->get_field_name('image_uri[]'); ?>" id="<?php echo $this->get_field_id('image_uri'); ?>' +
        cnt + '" value="">';
    new_row += '<input type="button" value="Upload Image" class="button custom_media_upload' + cnt +
        '" id="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '"/>';
    new_row += '</div><br><br>';


    jQuery(document).ready(function() {




        function media_upload(button_class) {
            var _custom_media = true,
                _orig_send_attachment = wp.media.editor.send.attachment;
            jQuery('body').on('click', '.custom_media_upload' + cnt, function(e) {
                var button_id = '#' + jQuery(this).attr('id');
                var button_id_s = jQuery(this).attr('id');
                console.log(button_id);
                var self = jQuery(button_id);
                var send_attachment_bkp = wp.media.editor.send.attachment;
                var button = jQuery(button_id);
                var id = button.attr('id').replace('_button', '');
                _custom_media = true;

                wp.media.editor.send.attachment = function(props, attachment) {
                    if (_custom_media) {

                        //if (attachment.height == 150 && attachment.width == 150) {
                            jQuery('.' + button_id_s + '_media_id' + cnt).val(attachment.id);
                            jQuery('.' + button_id_s + '_media_url' + cnt).val(attachment.url);
                            jQuery('.' + button_id_s + '_media_image' + cnt).attr('src',
                                attachment.url).css('display', 'block');

                           /* jQuery('#image_uri_agilysys_pos_3_column_sliders' + cnt)
                                .html("");
                        } else {
                            jQuery('#image_uri_agilysys_pos_3_column_sliders' + cnt)
                                .html("Please Enter the correct Dimensions 150x150").css(
                                    'color', 'red');

                        }
                        */
                    } else {
                        return _orig_send_attachment.apply(button_id, [props, attachment]);
                    }
                }
                wp.media.editor.open(button);
                return false;
            });
        }
        media_upload('.custom_media_upload' + cnt);

    });


   new_row += '<p>';
    new_row += '<label><?php echo __('Image Alt', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
    new_row += '<input name="<?php echo $this->get_field_name('image_uri_alt[]'); ?>" type="text"  />';
    new_row += '</p><br>';



    new_row += '<div class="widg-img' + cnt + '">';
    new_row += '<label id="image_urix_agilysys_pos_3_column_sliders' + cnt +
        '"></label><br><img class="<?php echo $this->get_field_id('image_idx'); ?>' + cnt + '_media_imagex' + cnt +
        ' custom_media_imagex' + cnt + '" src="" style="display:none;" width="200" height="120"/>';
    new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_idx'); ?>' + cnt + '_media_idx' +
        cnt + ' custom_media_idx' + cnt +
        '" name="<?php echo $this->get_field_name('image_idx[]'); ?>"  value="" />';
    new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_idx'); ?>' + cnt + '_media_urlx' +
        cnt + ' custom_media_urlx' + cnt +
        '" name="<?php echo $this->get_field_name('image_urix[]'); ?>" id="<?php echo $this->get_field_id('image_urix'); ?>' +
        cnt + '" value="">';
    new_row += '<input type="button" value="Upload Image" class="button custom_media_uploadx' + cnt +
        '" id="<?php echo $this->get_field_id('image_idx'); ?>' + cnt + '"/>';
    new_row += '</div><br><br>';


    jQuery(document).ready(function() {




        function media_upload(button_class) {
            var _custom_media = true,
                _orig_send_attachment = wp.media.editor.send.attachment;
            jQuery('body').on('click', '.custom_media_uploadx' + cnt, function(e) {
                var button_id = '#' + jQuery(this).attr('id');
                var button_id_s = jQuery(this).attr('id');
                console.log(button_id);
                var self = jQuery(button_id);
                var send_attachment_bkp = wp.media.editor.send.attachment;
                var button = jQuery(button_id);
                var id = button.attr('id').replace('_button', '');
                _custom_media = true;

                wp.media.editor.send.attachment = function(props, attachment) {
                    if (_custom_media) {

                       // if (attachment.height == 350 && attachment.width == 350) {
                            jQuery('.' + button_id_s + '_media_idx' + cnt).val(attachment.id);
                            jQuery('.' + button_id_s + '_media_urlx' + cnt).val(attachment.url);
                            jQuery('.' + button_id_s + '_media_imagex' + cnt).attr('src',
                                attachment.url).css('display', 'block');


                           /* jQuery('#image_urix_agilysys_pos_3_column_sliders' + cnt)
                                .html("");
                        } else {
                            jQuery('#image_urix_agilysys_pos_3_column_sliders' + cnt)
                                .html("Please Enter the correct Dimensions 350x350").css(
                                    'color', 'red');

                        }
                        */
                    } else {
                        return _orig_send_attachment.apply(button_id, [props, attachment]);
                    }
                }
                wp.media.editor.open(button);
                return false;
            });
        }
        media_upload('.custom_media_uploadx' + cnt);

    });

       new_row += '<p>';
    new_row += '<label><?php echo __('Image Alt', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
    new_row += '<input name="<?php echo $this->get_field_name('image_uri_alt1[]'); ?>" type="text"  />';
    new_row += '</p><br>';



    new_row += '<p>';
    new_row += '<label><?php echo __('Title', 'agilysys_text_domain'); ?>:</label>';
    new_row +=
        '<input name="<?php echo $this->get_field_name('solution_3_column_sub_title[]'); ?>" type="text" value="">';
    new_row += '</p>';

    var new_cnt = cnt;

    new_row += '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' + new_cnt +
        ');"><?php echo __('Delete Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span></a></p>';
    new_row += '</div></div>';

    jQuery('.add_new_rowxx-input-containers #entries_agilysys_pos_3_column_sliders_<?php echo $rand;?>').append(new_row);



}



function delete_row(cnt) {
    jQuery.each(jQuery(".add_new_rowxx-input-containers #entries_agilysys_pos_3_column_sliders_<?php echo $rand;?>"), function() {
        jQuery(' #entry' + cnt).remove();
    });
    var last_cnt = 0;

    jQuery.each(jQuery("#entries_agilysys_pos_3_column_sliders_<?php echo $rand;?> .cnt909"), function() {
        if (jQuery(this).val() != '') {
            last_cnt = jQuery(this).val();
        }
    });

    last_cnt--;
    jQuery.each(jQuery("#entries_agilysys_pos_3_column_sliders_<?php echo $rand;?> .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(last_cnt);
        }
    });


}
</script>
<style>

.wp-picker-container{
    margin-left:34%;
}

#rew_container_agilysys_pos_3_column_sliders p {
    padding: 10px !important;
}



.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_pos_3_column_sliders input,
select,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_pos_3_column_sliders label {
    width: 40%;
}

<?php echo '.' . $widget_add_id_solution_slider;

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}

#entries_agilysys_pos_3_column_sliders_<?php echo $rand;?> #remove-img-bg {
    background: url('<?php echo ZWREW_URL; ?>assets/images/deleteimg.png') center center no-repeat;
    width: 20px;
    height: 22px;
    display: none;
}

#entries_agilysys_pos_3_column_sliders_<?php echo $rand;?> {
    padding: 10px 0 0;
}

#entries_agilysys_pos_3_column_sliders_<?php echo $rand;?> .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_pos_3_column_sliders_<?php echo $rand;?> .entrys:first-child {
    margin: 0;
}

#entries_agilysys_pos_3_column_sliders_<?php echo $rand;?> .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_pos_3_column_sliders_<?php echo $rand;?> .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_pos_3_column_sliders_<?php echo $rand;?> .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_pos_3_column_sliders_<?php echo $rand;?> .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_pos_3_column_sliders_<?php echo $rand;?> .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_pos_3_column_sliders #entries_agilysys_pos_3_column_sliders_<?php echo $rand;?> plast label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_pos_3_column_sliders">
    <?php echo $rew_html; ?>
</div>
<?php
} //Function form ends here
} // class ends here
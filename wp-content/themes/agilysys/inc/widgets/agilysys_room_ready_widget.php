<?php

add_action('widgets_init', 'agil_load_room_ready');

function agil_load_room_ready()
{
    register_widget('agilysys_room_ready_widget');
}

class agilysys_room_ready_widget extends WP_Widget
{

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Room Ready Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');

    }

    public function widget($args, $instance)
    {

        echo $args['before_widget'];

        $left_hand_side_title = $instance['left_hand_side_title'];
        $left_hand_side_desc = $instance['left_hand_side_desc'];
        $left_hand_side_bottom_title = $instance['left_hand_side_bottom_title'];

        $link_type = $instance['link_type'];
        if ($link_type == "link") {
            $left_hand_side_bottom_link = $instance['left_hand_side_bottom_link'];
        } elseif ($link_type == "page") {
            $post_id = $instance['page'];
            $post = get_post($post_id);
            $left_hand_side_bottom_link = home_url($post->post_name) . "/";
        }

        $image_uri = $instance['image_uri'];
        $image_uri_alt = $instance['image_uri_alt'];
        $right_hand_side_title = $instance['right_hand_side_title'];
        $right_hand_side_desc = $instance['right_hand_side_desc'];
        $right_hand_side_bottom_title = $instance['right_hand_side_bottom_title'];

        $link_type = $instance['link_typea'];
        if ($link_type == "link") {
            $right_hand_side_bottom_link = $instance['right_hand_side_bottom_link'];
        } elseif ($link_type == "page") {
            $post_id = $instance['pagea'];
            $post = get_post($post_id);
            $right_hand_side_bottom_link = home_url($post->post_name) . "/";
        }

        ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"
    type="text/javascript"></script>
<section class="rguestRoomSection">
    <div class="row">
        <div class="rguestRoomBox col-12 col-sm-12 col-md-12 col-lg-6" data-aos="fade-right" data-aos-delay="300" data-aos-duration="400" data-aos-once="true">
            <h2 class="dinProStd greenText"><?php echo $left_hand_side_title; ?></h2>
            <div class="rguestRoomTextBox greenBG">
                <div class="rguestRoomContent whiteText">
                    <p><?php echo substr($left_hand_side_desc,0,400); ?></p>

                    <a class="homeBannerButton whiteText"
                        href="<?php echo $left_hand_side_bottom_link; ?>"><?php echo $left_hand_side_bottom_title; ?> <i
                            class="fa fa-arrow-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
        <div class="rguestMobileImg" data-aos="zoom-out" data-aos-delay="300" data-aos-duration="500" data-aos-once="true">
            <img class="img-fluid" src="<?php echo $image_uri; ?>" alt="<?php echo $image_uri_alt; ?>" />
        </div>
        <div class="rguestMobileBox col-12 col-sm-12 col-md-12 col-lg-6" data-aos="fade-left" data-aos-delay="300" data-aos-duration="500" data-aos-once="true">
            <h2 class="dinProStd waterText"><?php echo $right_hand_side_title; ?></h2>
            <div class="rguestMobileContentBox waterBG">
                <div class="rguestMobileContent whiteText">
                    <p><?php echo substr($right_hand_side_desc,0,400); ?></p>

                    <a class="homeBannerButton whiteText" data-toggle="modal" data-target="#registerForm"><?php echo $right_hand_side_bottom_title; ?>
                        <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>

<style>
    
 
        #registerForm label {
   font-size:24px;
}

#registerForm .close {
    
    right: -0.5%;
}

 #registerForm  .error {
            color: red;
            font-size:16px;
        }
        
        
    
</style>


<!-- Modal -->
<div id="registerForm"  class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        
      </div>
      <div class="modal-body">
        <form name="registration" action="https://info.agilysys.com/l/76642/2020-11-12/5yq37l" id="frmRma" style="padding:10px;" method="POST">
                <div class="customerFormLabel flex">
                        <div class="customerFormLabel1">
                            <label for="FirstName" class="dinProStd  blackText">FIRST NAME</label>
                            <input type="text" id="FirstName" name="FirstName" placeholder="Enter First Name" />
                        </div>
                        <div class="customerFormLabel2">
                            <label for="LastName" class="dinProStd  blackText">LAST NAME</label>
                            <input type="text" id="LastName" class="number-only" name="LastName"
                                placeholder="Enter Last Name" />
                        </div>
                    </div>
                    
                       <div class="customerFormLabel flex">
                        <div class="customerFormLabel1">
                            <label for="FirstName" class="dinProStd  blackText">EMAIL</label>
                            <input type="text" id="email" name="email" placeholder="Enter Email" />
                        </div>
                        
                        <div class="customerFormLabel2">
                            <label for="Phone" class="dinProStd  blackText">COMPANY</label>
                            <input type="text" id="Company" class="number-only" name="Company"
                                placeholder="Enter Company" />
                        </div>
                        
                        
                        
                    </div>
                    
                     <div class="customerFormLabel flex">
                    <div class="customerFormLabel1">
                            <label for="Phone" class="dinProStd  blackText">PHONE</label>
                            <input type="text" id="Phone" class="number-only" name="Phone"
                                placeholder="Enter Phone" />
                        </div>
                        
                        <div class="customerFormLabel2">
                            <label for="CompanyAddress" class="dinProStd  blackText">ADDRESS 1</label>
                            <input type="text" id="CompanyAddress" name="CompanyAddress" placeholder="Enter Address 1" />
                        </div>
                      </div>
                    <div class="customerFormLabel flex">
                        
                        <div class="customerFormLabel1">
                            <label for="Address2" class="dinProStd  blackText">ADDRESS 2</label>
                            <input type="text" id="Address2" class="number-only" name="Address2"
                                placeholder="Enter Address 1" />
                        </div>
                        
                        <div class="customerFormLabel2">
                            <label for="City" class="dinProStd  blackText">CITY</label>
                            <input type="text" id="City" name="City" placeholder="Enter City" />
                        </div>
                    </div>
                    
                     <div class="customerFormLabel flex">
                        
                        <div class="customerFormLabel1">
                            <label for="State" class="dinProStd  blackText">STATE</label>
                            <input type="text" id="State" class="number-only" name="State"
                                placeholder="Enter State" />
                        </div>
                        
                        <div class="customerFormLabel2">
                            <label for="Zip" class="dinProStd  blackText">ZIP</label>
                            <input type="text" id="Zip" name="Zip" placeholder="Enter Zip" />
                        </div>
                    </div>
                    
                    <div class="customerFormLabel flex">
                        
                        <div class="customerFormLabel1">
                            <label for="Country" class="dinProStd  blackText">COUNTRY</label>
                            <input type="text" id="Country" class="number-only" name="Country"
                                placeholder="Enter Country" />
                        </div>
                    </div>
                     <div class="customerFormButton">
                        <button id="submit" class="aboutButtonx"  type="submit">
                            Submit&nbsp;&nbsp;<i class="fa fa-arrow-right" aria-hidden="true"></i>
                        </button>
                    </div>
        </form>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
      </div>
    </div>

  </div>
</div>


<script>
jQuery(document).ready(function($) {


    $("form[name='registration']").validate({

        rules: {

            FirstName: "required",
            LastName: "required",
          
            email: {
                required: true,
                email: true
            },
            Company: "required",
            Phone: "required",
            CompanyAddress: "required",
            Address2: "required",
            City: "required",
            State: "required",
            Zip: "required",
            Country: "required",

        },
        errorPlacement: function(error, element) {

           
                error.insertAfter(element);
            
        },

        messages: {
          
            FirstName: "Please enter your First Name",
            LastName: "Please enter your Last Name",
            email: "Please enter a valid email address",
            Company: "Please enter your Company Name",
            Phone: "Please enter your Phone Number",
            CompanyAddress: "Please enter your Address 1",
            Address2: "Please enter your Address 2",
            City: "Please enter your City",
            State: "Please enter your State",
            Zip: "Please enter your Zip",
            Country: "Please enter your Country",
        },

        submitHandler: function(form) {



            form.submit();



        }
    });
});
</script>

<?php

        echo $args['after_widget'];
    }
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['left_hand_side_title'] = strip_tags($new_instance['left_hand_side_title']);
        $instance['left_hand_side_desc'] = strip_tags($new_instance['left_hand_side_desc']);
        $instance['left_hand_side_bottom_title'] = strip_tags($new_instance['left_hand_side_bottom_title']);
        //$instance['left_hand_side_bottom_link'] = strip_tags($new_instance['left_hand_side_bottom_link']);

        $instance['link_type'] = $new_instance['link_type'];
        if ($new_instance['link_type'] == 'page') {
            $instance['page'] = $new_instance['page'];
            $instance['left_hand_side_bottom_link'] = '';
        } elseif ($new_instance['link_type'] == 'link') {
            $instance['left_hand_side_bottom_link'] = $new_instance['left_hand_side_bottom_link'];
            $instance['page'] = '';

        }

        $instance['image_uri'] = strip_tags($new_instance['image_uri']);
        $instance['image_uri_alt'] = strip_tags($new_instance['image_uri_alt']);

        $instance['right_hand_side_title'] = strip_tags($new_instance['right_hand_side_title']);
        $instance['right_hand_side_desc'] = strip_tags($new_instance['right_hand_side_desc']);
        $instance['right_hand_side_bottom_title'] = strip_tags($new_instance['right_hand_side_bottom_title']);

        $instance['link_typea'] = $new_instance['link_typea'];
        if ($new_instance['link_typea'] == 'page') {
            $instance['pagea'] = $new_instance['pagea'];
            $instance['right_hand_side_bottom_link'] = '';
        } elseif ($new_instance['link_typea'] == 'link') {
            $instance['right_hand_side_bottom_link'] = $new_instance['right_hand_side_bottom_link'];
            $instance['page'] = '';

        }

        //$instance['right_hand_side_bottom_link'] = strip_tags($new_instance['right_hand_side_bottom_link']);

        return $instance;
    }

    public function form($display_instance)
    {

        $left_hand_side_title = ($display_instance['left_hand_side_title']);
        $left_hand_side_desc = ($display_instance['left_hand_side_desc']);
        $left_hand_side_bottom_title = ($display_instance['left_hand_side_bottom_title']);
        $left_hand_side_bottom_link = ($display_instance['left_hand_side_bottom_link']);

        $center_image = ($display_instance['center_image']);

        $right_hand_side_title = ($display_instance['right_hand_side_title']);
        $right_hand_side_desc = ($display_instance['right_hand_side_desc']);
        $right_hand_side_bottom_title = ($display_instance['right_hand_side_bottom_title']);
        $right_hand_side_bottom_link = ($display_instance['right_hand_side_bottom_link']);

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('left_hand_side_title') . '"> ' . __('Left Hand Side Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('left_hand_side_title') . '" name="' . $this->get_field_name('left_hand_side_title') . '" type="text" value="' . $left_hand_side_title . '" />';
        $rew_html .= '</p><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('left_hand_side_desc') . '"> ' . __('Left Hand Side Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<textarea rows="6" cols="28" id="' . $this->get_field_id('left_hand_side_desc') . '" name="' . $this->get_field_name('left_hand_side_desc') . '" >' . $left_hand_side_desc . '</textarea>';
        $rew_html .= '</p><br><br><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('left_hand_side_bottom_title') . '"> ' . __('Left Hand Side Bottom Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('left_hand_side_bottom_title') . '" name="' . $this->get_field_name('left_hand_side_bottom_title') . '" type="text" value="' . $left_hand_side_bottom_title . '" />';
        $rew_html .= '</p><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('link_type') . '"> ' . __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN') . '</label>';
        $rew_html .= '<select id="' . $this->get_field_id('link_type') . '" name="' . $this->get_field_name('link_type') . '" onChange="show_hide_div_agilysys_room_ready_widget(this.value);">';
        $rew_html .= '<option value="">Please Select</option>';

        $link_type = $display_instance['link_type'];

        if ($link_type == 'page') {
            $rew_html .= '<option value="page" selected="selected">Internal Page Link</option>';
        } else {
            $rew_html .= '<option value="page">Internal Page Link</option>';
        }

        if ($link_type == 'link') {
            $rew_html .= '<option value="link" selected="selected">External Link</option>';
        } else {
            $rew_html .= '<option value="link">External Link</option>';
        }

        $rew_html .= '</select>';
        $rew_html .= '</p>';

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block"';
            $show2 = 'style="display:none"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none"';
            $show2 = 'style="display:block"';

        } else {
            $show1 = 'style="display:none"';
            $show2 = 'style="display:none"';
        }
        $rew_html .= '<div id="page_div_agilysys_room_ready_widget" ' . $show1 . '><p>';
        $rew_html .= '<label for="' . $this->get_field_id('page') . '"> ' . __('Page', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<select id="' . $this->get_field_id('page') . '" name="' . $this->get_field_name('page') . '">';
        $rew_html .= '<option value="">Please Select</option>';

        $page = $display_instance['page'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                $rew_html .= '<option value="' . $key->ID . '" selected="selected">' . $key->post_title . '</option>';
            } else {
                $rew_html .= '<option value="' . $key->ID . '">' . $key->post_title . '</option>';
            }

        }

        $rew_html .= '</select>';
        $rew_html .= '</p></div>';

        $rew_html .= '<div id="link_div_agilysys_room_ready_widget" ' . $show2 . '><p>';
        $rew_html .= '<label for="' . $this->get_field_id('left_hand_side_bottom_link') . '"> ' . __('Link', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('left_hand_side_bottom_link') . '" name="' . $this->get_field_name('left_hand_side_bottom_link') . '" type="text" value="' . $display_instance['left_hand_side_bottom_link'] . '" />';
        $rew_html .= '</p></div>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt') . '"> ' . __('Image Alt', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt') . '" name="' . $this->get_field_name('image_uri_alt') . '" type="text" value="' . $display_instance['image_uri_alt'] . '">';
        $rew_html .= '</p>';
        ?>
<script>
function show_hide_div_agilysys_room_ready_widget(val) {
console.log(val);
    if (val == 'page') {
        jQuery("#page_div_agilysys_room_ready_widget").show();
        jQuery("#link_div_agilysys_room_ready_widget").hide();
    } else if (val == 'link') {
        jQuery("#page_div_agilysys_room_ready_widget").hide();
        jQuery("#link_div_agilysys_room_ready_widget").show();
    }

}
</script>

<?php

        ?>

<div id="rew_container_agilysys_room_ready_widget">
    <p>
<label class="widg-label widg-img-label" for="<?php echo $this->get_field_id('image_uri'); ?>">Image</label>
<div class="widg-img">
    <img class="<?php echo $this->get_field_id('image_id'); ?>_media_image custom_media_image"
        src="<?php if (!empty($display_instance['image_uri'])) {echo $display_instance['image_uri'];}?>" height="120"
        width="200" />
    <input input type="hidden" type="text"
        class="<?php echo $this->get_field_id('image_id'); ?>_media_id custom_media_id"
        name="<?php echo $this->get_field_name('image_id'); ?>" id="<?php echo $this->get_field_id('image_id'); ?>"
        value="<?php echo $display_instance['image_id']; ?>" />
    <input type="hidden" class="<?php echo $this->get_field_id('image_id'); ?>_media_url custom_media_url"
        name="<?php echo $this->get_field_name('image_uri'); ?>" id="<?php echo $this->get_field_id('image_uri'); ?>"
        value="<?php echo $display_instance['image_uri']; ?>">
    <input type="button" value="Upload Image" class="button custom_media_upload"
        id="<?php echo $this->get_field_id('image_id'); ?>" />
</div></p>
</div>

<script>
jQuery(document).ready(function() {
    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    if (attachment.height == 1080 && attachment.width == 1920) {
                        jQuery('.' + button_id_s + '_media_id').val(attachment.id);
                        jQuery('.' + button_id_s + '_media_url').val(attachment.url);
                        jQuery('.' + button_id_s + '_media_image').attr('src', attachment.url).css(
                            'display', 'block');

                        jQuery('#image_uri_agilysys_room_ready_widget')
                            .html("");
                    } else {
                        jQuery('#image_uri_agilysys_room_ready_widget')
                            .html("Please Enter the correct Dimensions 1920x1080").css(
                                'color', 'red');

                    }
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload');

});
</script>

<?php

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('right_hand_side_title') . '"> ' . __('Right Hand Side Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('right_hand_side_title') . '" name="' . $this->get_field_name('right_hand_side_title') . '" type="text" value="' . $right_hand_side_title . '" />';
        $rew_html .= '</p><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('right_hand_side_desc') . '"> ' . __('Right Hand Side Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<textarea rows="6" cols="28" id="' . $this->get_field_id('right_hand_side_desc') . '" name="' . $this->get_field_name('right_hand_side_desc') . '" >' . $right_hand_side_desc . '</textarea>';
        $rew_html .= '</p><br><br><br><br><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('right_hand_side_bottom_title') . '"> ' . __('Right Hand Side Bottom Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('right_hand_side_bottom_title') . '" name="' . $this->get_field_name('right_hand_side_bottom_title') . '" type="text" value="' . $right_hand_side_bottom_title . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('link_typea') . '"> ' . __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<select id="' . $this->get_field_id('link_typea') . '" name="' . $this->get_field_name('link_typea') . '" onChange="show_hide_div_agilysys_room_ready_widget(this.value);">';
        $rew_html .= '<option value="">Please Select</option>';

        $link_type = $display_instance['link_typea'];

        if ($link_type == 'page') {
            $rew_html .= '<option value="page" selected="selected">Internal Page Link</option>';
        } else {
            $rew_html .= '<option value="page">Internal Page Link</option>';
        }

        if ($link_type == 'link') {
            $rew_html .= '<option value="link" selected="selected">External Link</option>';
        } else {
            $rew_html .= '<option value="link">External Link</option>';
        }

        $rew_html .= '</select>';
        $rew_html .= '</p><br><br>';

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block"';
            $show2 = 'style="display:none"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none"';
            $show2 = 'style="display:block"';

        } else {
            $show1 = 'style="display:none"';
            $show2 = 'style="display:none"';
        }
        $rew_html .= '<div id="page_div_agilysys_room_ready_widget" ' . $show1 . '><p>';
        $rew_html .= '<label for="' . $this->get_field_id('pagea') . '"> ' . __('Page', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<select id="' . $this->get_field_id('pagea') . '" name="' . $this->get_field_name('pagea') . '">';
        $rew_html .= '<option value="">Please Select</option>';

        $page = $display_instance['pagea'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                $rew_html .= '<option value="' . $key->ID . '" selected="selected">' . $key->post_title . '</option>';
            } else {
                $rew_html .= '<option value="' . $key->ID . '">' . $key->post_title . '</option>';
            }

        }

        $rew_html .= '</select>';
        $rew_html .= '</p></div><br><br>';

        $rew_html .= '<div id="link_div_agilysys_room_ready_widget" ' . $show2 . '><p>';
        $rew_html .= '<label for="' . $this->get_field_id('right_hand_side_bottom_link') . '"> ' . __('Link', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('right_hand_side_bottom_link') . '" name="' . $this->get_field_name('right_hand_side_bottom_link') . '" type="text" value="' . $display_instance['right_hand_side_bottom_link'] . '" />';
        $rew_html .= '</p></div><br>';
        ?>
<script>
function show_hide_div_agilysys_room_ready_widget(val) {
console.log(val);
    if (val == 'page') {
        jQuery("#page_div_agilysys_room_ready_widget").show();
        jQuery("#link_div_agilysys_room_ready_widget").hide();
    } else if (val == 'link') {
        jQuery("#page_div_agilysys_room_ready_widget").hide();
        jQuery("#link_div_agilysys_room_ready_widget").show();
    }

}
</script>

<?php
?>

<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}




#rew_container_agilysys_room_ready_widget select {
    float: left;
    width: 60%;
    margin-top: 20px !important;
    margin-bottom: 10px !important;
}

#rew_container_agilysys_room_ready_widget input,

textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_room_ready_widget label {
    width: 40%;
    float: left;
}

<?php echo '.' . $widget_add_id_review_ico;

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}

#entries_agilysys_room_ready_widget #remove-img-bg {
    background: url('<?php echo ZWREW_URL; ?>assets/images/deleteimg.png') center center no-repeat;
    width: 20px;
    height: 22px;
    display: none;
}

#entries_agilysys_room_ready_widget {
    padding: 10px 0 0;
}

#entries_agilysys_room_ready_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_room_ready_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_room_ready_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_room_ready_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_room_ready_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_room_ready_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_room_ready_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_room_ready_widget p {
    padding:20px !important;
}



#rew_container_agilysys_room_ready_widget #entries_agilysys_room_ready_widget plast label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_room_ready_widget">
    <?php echo $rew_html; ?>
</div>


<?php


    }
}
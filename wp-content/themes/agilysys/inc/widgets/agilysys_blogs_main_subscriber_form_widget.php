<?php
use PHPMailer\PHPMailer\PHPMailer;

require_once ABSPATH . WPINC . '/PHPMailer/PHPMailer.php';
require_once ABSPATH . WPINC . '/PHPMailer/Exception.php';
function load_agilysys_blogs_main_subscriber_form_widget()
{
    register_widget('agilysys_blogs_main_subscriber_form_widget');
}

add_action('widgets_init', 'load_agilysys_blogs_main_subscriber_form_widget');

class agilysys_blogs_main_subscriber_form_widget extends WP_Widget
{

    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Blogs Main Subscriber Form Widget', 'AGILYSYS_TEXT_DOMAIN'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        $title = $instance['title'];
        $description = $instance['description'];

        ?>

       <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js">
        </script>
        <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/additional-methods.js">
        </script>

        <style id="compiled-css" type="text/css">

        .error {
            color: red;
        }

        #myform2 {
            margin: 10px;
        }

        #myform2>div {
            margin-top: 10px;
        }

        /* EOS */
        </style>
        <?php

        if (isset($_POST) && !empty($_POST)) {

            $first_name = $_POST['first_name'];
            $last_name = $_POST['last_name'];
            $email = $_POST['email'];

            $message = '';
            $message .= '<p>First Name: ' . $first_name . '</p>';
            $message .= '<p>Last Name: ' . $last_name . '</p>';
            $message .= '<p>Email: ' . $email . '</p>';

            global $wpdb;

            $sql = $wpdb->prepare(
                "INSERT INTO `agsweb_blog_page_subscriber_form`
                   (`first_name`,`last_name`,`email`)
             values('$first_name', '$last_name', '$email')");

            $exe = $wpdb->query($sql);

            $subject = "New Enquiry";
            $mail = new PHPMailer();

//$mail->isSMTP();

            $mail->SMTPDebug = false;

            $mail->Host = 'smtp.gmail.com';

            $mail->SMTPAuth = true;

            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = 587;

            $mail->Username = 'sau3334@gmail.com';

            $mail->Password = 'rwwtpnhahufyxqih';

            $mail->setFrom('testing@abiteoffrance.com', 'Testing');
            $mail->addReplyTo('sau3334@gmail.com', 'Saurav Daga');

            $mail->addAddress('vandhiyadevan.jayasooriyan@agilysys.com', 'Deva');

            $mail->Subject = 'New Subscriber';

            $mail->Body = $message;

            $mail->AltBody = 'New Subscriber';

            if (!$mail->send()) {
                echo 'Mailer Error: ' . $mail->ErrorInfo;
            } else {
               ?>
               
               <script type="text/javascript">
    jQuery(window).on('load', function() {
        jQuery('#myModal').modal('show');
    });
</script>
               
               <?php
            }
        }

        ?>



<div class="blogSubscribeForm">
    <h3 class="greenText dinProStd"><?php echo $title; ?></h3>
    <p><?php echo $description; ?></p>
    <form class="" action="" id="myform2" method="POST">
        <input type="text" class="" name="first_name" id="fname" placeholder="First Name" />
        <input type="text" class="" name="last_name" id="lname" placeholder="Last Name" />
        <input type="text" class="" name="email" id="email" placeholder="Email" />
        <input type="submit" class="" name="subscribe" id="subscribe" value="Subscribe" />
    </form>
</div>


<script>
jQuery(document).ready(function($) {

$.validator.addMethod("lettersonly", function(value, element)
{
return this.optional(element) || /^[a-z," "]+$/i.test(value);
}, "Letters and spaces only please");




    $("#myform2").validate({
          onfocusout: false,
        rules: {

            first_name:{
               required:true,
               lettersonly: true,
           },
            last_name:{
               required:true,
               lettersonly: true,
           },

            email: {
                required: true,
                email: true
            },

        },
        errorPlacement: function(error, element) {


                error.insertAfter(element);

        },

        messages: {
            first_name: {
               required:"Please enter the First Name",
               lettersonly: "Please enter Letters only",
           },
            last_name: {
               required:"Please enter the Last Name",
               lettersonly: "Please enter Letters only",
           },
            email: "Please enter a valid Email Address",


        },


        submitHandler: function(form) {




            form.submit();



        }
    });
});
</script>

<?php
echo $args['after_widget'];
    }

    public function form($display_instance)
    {
        $widget_add_id_slider_client = $this->get_field_id('') . "add_agilysys_blogs_main_subscriber_form_widget";

        $title = $display_instance['title'];
        $description = $display_instance['description'];

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('title') . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input style="width:100%;" id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" type="text" value="' . $title . '" />';
        $rew_html .= '</p><br><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('description') . '"> ' . __('Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<textarea rows="6" cols="35" id="' . $this->get_field_id('description') . '" name="' . $this->get_field_name('description') . '" >' . $description . '</textarea>';
        $rew_html .= '</p><br><br><br>';
        ?>

<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container input,
select,
textarea {
    float: right;
    width: 60%;
}

#rew_container label {
    width: 40%;
}

<?php echo '.' . $widget_add_id_slider_client;

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}


#entries {
    padding: 10px 0 0;
}

#entries .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries .entrys:first-child {
    margin: 0;
}

#entries .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries .entry-title.active:after {
    content: '\f142';
}

#entries .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container #entries p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container">
    <?php echo $rew_html; ?>
</div>



<?php

    }
    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['title'] = $new_instance['title'];
        $instance['description'] = $new_instance['description'];

        return $instance;
    }

}
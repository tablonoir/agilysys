<?php

add_action('widgets_init', 'agil_load_agilysys_news_events_blog_post_widget');

function agil_load_agilysys_news_events_blog_post_widget()
{
    register_widget('agilysys_news_events_blog_post_widget');
}

class agilysys_news_events_blog_post_widget extends WP_Widget
{

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys News and Events Blog Post Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
        wp_enqueue_script('jquery');
        wp_enqueue_script('jquery-ui-datepicker');
        wp_register_style('jquery-ui', 'https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css');
        wp_enqueue_style('jquery-ui');

    }
    
      function addhttp($url) {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "https://" . $url;
        }
        return $url;
    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        $section_title = $instance['section_title'];
        $max_entries_agilysys_news_events_blog_post_widget_slider_image = 15;

        $count = count($instance['date_of_event']);

        ?>



<div class="upcomingEvents flex">
    <h2 class="dinProStd greenText"><?php echo $section_title; ?></h2>
    <?php
$cnt = 0;
        for ($i = 0; $i < $count; $i++) {

            if ($cnt < 4) {

                $date_of_event = $instance['date_of_event'][$i];

                $month = date("M", strtotime($date_of_event));
                $day = date("d", strtotime($date_of_event));

                $url_of_event = $instance['url_of_event'][$i];
                $title_of_event = $instance['title_of_event'][$i];

                ?>

      <a href="<?php echo $this->addhttp($url_of_event); ?>">
        <div class="upcomingEventsBox flex">
            <div class="update">
                <h2 class="dinProStd greenText"><?php echo $day; ?></h2>
                <h3 class="dinProStd"><?php echo $month; ?></h3>
            </div>
            <div class="upContent">
                <p><?php echo $title_of_event; ?></p>
            </div>
        </div>
    </a>

    <?php
}
            $cnt++;
        }

        ?>

    <a href="#" class="aboutButton violetText center">View All <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
</div>


<?php
echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $max_entries_agilysys_news_events_blog_post_widget_slider_image = 15;

        $instance['section_title'] = strip_tags($new_instance['section_title']);
        $instance['what_makes_rows'] = strip_tags($new_instance['what_makes_rows']);

        $count = count($new_instance['date_of_event']);

        for ($i = 0; $i < $count; $i++) {

            $instance['date_of_event'][$i] = strip_tags($new_instance['date_of_event'][$i]);
            $instance['title_of_event'][$i] = $new_instance['title_of_event'][$i];
            $instance['url_of_event'][$i] = $new_instance['url_of_event'][$i];

        }

        return $instance;

    }

    public function form($display_instance)
    {

        $max_entries_agilysys_news_events_blog_post_widget_slider_image = 15;

        $idxx = rand(10, 100);

        $widget_add_id_slider = $this->get_field_id('') . "add_agilysys_news_events_blog_post_widget" . $idxx;

        $section_title = ($display_instance['section_title']);

        if (!empty($display_instance['what_makes_rows'])) {
            $what_makes_rows = ($display_instance['what_makes_rows']);
        } else {
            $what_makes_rows = 0;
        }
        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('section_title') . '"> ' . __('Section Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('section_title') . '" name="' . $this->get_field_name('section_title') . '" type="text" value="' . $section_title . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('what_makes_rows') . '"> ' . __('No. of rows do you want to add*', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input class="test_post1" id="' . $this->get_field_name('what_makes_rows') . '" name="' . $this->get_field_name('what_makes_rows') . '" type="text" value="' . $what_makes_rows . '" />';
        $rew_html .= '</p>';

        $count = count($display_instance['date_of_event']);

        $rew_html .= '<div class="add_new_rowxx-input-containers"><div id="entries_agilysys_news_events_blog_post_widget">';

        $rew_html .= '<input class="cnt909" id="cnt909" name="cnt" type="hidden" value="' . $count . '">';

        for ($i = 0; $i < $count; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'AGILYSYS_TEXT_DOMAIN') . ' </span>';

            $rew_html .= '<div class="entry-desc cf">';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('date_of_event[]') . '"> ' . __('Date Of Event', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input class="datepicker"  name="' . $this->get_field_name('date_of_event[]') . '" type="text" value="' . $display_instance['date_of_event'][$i] . '">';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('title_of_event[]') . '"> ' . __('Title Of Event', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input name="' . $this->get_field_name('title_of_event[]') . '" type="text" value="' . $display_instance['title_of_event'][$i] . '">';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('url_of_event[]') . '"> ' . __('Url Of Event', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input  name="' . $this->get_field_name('url_of_event[]') . '" type="text" value="' . $display_instance['url_of_event'][$i] . '">';
            $rew_html .= '</p>';

            $k = $i + 1;
            $rew_html .= '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' . $k . ');">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';

        }

        $rew_html .= '</div></div>';
        $rew_html .= '<div id="message">' . __('Sorry, you reached to the limit of', 'AGILYSYS_TEXT_DOMAIN') . ' "' . $what_makes_rows . '" ' . __('maximum entries_agilysys_news_events_blog_post_widget', 'AGILYSYS_TEXT_DOMAIN') . '.</div>';

        $rew_html .= '<div class="add_new_row909" style="text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 1px;    font-weight: 800;" onClick="add_new_row();">' . __('ADD ROW', 'AGILYSYS_TEXT_DOMAIN') . '</div>';
        ?>


<script>
function add_new_row() {
    var cnt = '';

    jQuery.each(jQuery("#entries_agilysys_news_events_blog_post_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            cnt = jQuery(this).val();
        }
    });


    cnt++;

    jQuery.each(jQuery("#entries_agilysys_news_events_blog_post_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(cnt);
        }
    });

console.log(cnt);

    var new_row = '<div id="entry' + cnt +
        '"  class="entrys"><span class="entry-title" onclick = "slider(this);"><?php echo __('Add New Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span>';
    new_row += '<div class="entry-desc cf">';

    new_row += '<p>';
    new_row += '<label for=""><?php echo __('Date Of Event', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



    new_row +=
        '<input class="datepicker" name="<?php echo esc_attr($this->get_field_name('date_of_event')); ?>[]" type="text" value="">';
    new_row += '</p>';

    new_row += '<p>';
    new_row += '<label for=""><?php echo __('Title Of Event', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



    new_row +=
        '<input class="" name="<?php echo esc_attr($this->get_field_name('title_of_event')); ?>[]" type="text" value="">';
    new_row += '</p>';


    new_row += '<p>';
    new_row += '<label for=""><?php echo __('Url Of Event', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



    new_row +=
        '<input class="" name="<?php echo esc_attr($this->get_field_name('url_of_event')); ?>[]" type="text" value="">';
    new_row += '</p>';

    var new_cnt = cnt;

    new_row +=
        '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' + new_cnt +
        ');"><?php echo __('Delete Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span></a></p>';
    new_row += '</div></div>';

    jQuery('.add_new_rowxx-input-containers #entries_agilysys_news_events_blog_post_widget').append(new_row);

    jQuery(document).ready(function($) {
        $(".datepicker").datepicker({
            dateFormat: 'dd-mm-yy'
        });
    });


}

jQuery(document).ready(function($) {
    $(".datepicker").datepicker({
        dateFormat: 'dd-mm-yy'
    });
});

function delete_row(cnt) {
    jQuery.each(jQuery(".add_new_rowxx-input-containers #entries_agilysys_news_events_blog_post_widget"), function() {
    jQuery(' #entry' + cnt).remove();
    });
    var last_cnt = 0;

    jQuery.each(jQuery("#entries_agilysys_news_events_blog_post_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            last_cnt = jQuery(this).val();
        }
    });

    last_cnt--;
    jQuery.each(jQuery("#entries_agilysys_news_events_blog_post_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(last_cnt);
        }
    });
    jQuery(".test_post1").val(last_cnt);
    jQuery('.test_post1').trigger('change');

}
</script>
<style>

#rew_container_agilysys_news_events_blog_post_widget input{
    width:180px !important;
}
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_news_events_blog_post_widget input,
select,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_news_events_blog_post_widget label {
    width: 40%;
}

<?php echo '.add_new_row909';

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}



#entries_agilysys_news_events_blog_post_widget {
    padding: 10px 0 0;
}

#entries_agilysys_news_events_blog_post_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_news_events_blog_post_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_news_events_blog_post_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_news_events_blog_post_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_news_events_blog_post_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_news_events_blog_post_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_news_events_blog_post_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_news_events_blog_post_widget #entries_agilysys_news_events_blog_post_widget p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_news_events_blog_post_widget">
    <?php echo $rew_html; ?>
</div>

<?php
}

}
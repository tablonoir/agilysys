<?php

add_action('widgets_init', 'agil_load_agilysys_pos_testimonial_with_image_on_right_widget');

function agil_load_agilysys_pos_testimonial_with_image_on_right_widget()
{
    register_widget('agilysys_pos_testimonial_with_image_on_right_widget');
}

class agilysys_pos_testimonial_with_image_on_right_widget extends WP_Widget
{

    public function __construct()
    {
        parent::__construct(false, $name = __(' Agilysys Pos Testimonial With Image On Right Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');

    }

    public function my_custom_load()
    {
        wp_enqueue_style('wp-color-picker');
        wp_enqueue_script('wp-color-picker');
    }

    public function widget($args, $instance)
    {

        echo $args['before_widget'];

        $testimonial = $instance['testimonial'];
        $image_uri = $instance['image_uri'];
        $username = $instance['username'];
        $div_color = $instance['div_color'];
        $image_uri_alt = $instance['image_uri_alt'];
        ?>

<section class="posQuotesBox">
    <div class="row">
        <div class="posQuotesContent col-12 col-sm-12 col-md-6 col-lg-6" style="background: <?php echo $div_color; ?>" data-aos="fade-right" data-aos-delay="300" data-aos-duration="500" data-aos-once="true">
            <h2 class="dinProStd whiteText"><?php echo substr($testimonial,0,250); ?></h2>
            <p class="whiteText dinproMed">- -<?php echo $username; ?></p>
        </div>
        <div class="posQuotesImage col-12 col-sm-12 col-md-6 col-lg-6" data-aos="fade-left" data-aos-delay="300" data-aos-duration="500" data-aos-once="true">
            <img class="img-fluid" src="<?php echo $image_uri; ?>" alt="<?php echo $image_uri_alt; ?>" />
        </div>
    </div>
</section>

<?php
echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {

        $instance = array();

        $instance['testimonial'] = strip_tags($new_instance['testimonial']);
        $instance['image_uri'] = strip_tags($new_instance['image_uri']);
        $instance['image_uri_alt'] = strip_tags($new_instance['image_uri_alt']);

        $instance['username'] = strip_tags($new_instance['username']);
        $instance['div_color'] = strip_tags($new_instance['div_color']);
        return $instance;
    }

    public function form($display_instance)
    {
        ?>
<script type="text/javascript">
jQuery(document).ready(function($) {
    jQuery('.color-picker').on('focus', function() {
        var parent = jQuery(this).parent();
        jQuery(this).wpColorPicker()
        parent.find('.wp-color-result').click();
    });
});
</script>
<?php
$testimonial = ($display_instance['testimonial']);
        $image_uri = ($display_instance['image_uri']);
        $username = ($display_instance['username']);
        $div_color = ($display_instance['div_color']);
        $image_uri_alt = $display_instance['image_uri_alt'];

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt') . '"> ' . __('Image Alt', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt') . '" name="' . $this->get_field_name('image_uri_alt') . '" type="text" value="' . $image_uri_alt . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('testimonial') . '"> ' . __('Testimonial', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<textarea id="' . $this->get_field_id('testimonial') . '" name="' . $this->get_field_name('testimonial') . '">' . $testimonial . '</textarea>';
        $rew_html .= '</p><br><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('username') . '"> ' . __('Heading', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('username') . '" name="' . $this->get_field_name('username') . '" type="text" value="' . $username . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('div_color') . '">' . __('Div Color', 'AGILYSYS_TEXT_DOMAIN') . '</label>';
        $rew_html .= '<input class="color-picker" id="' . $this->get_field_id('div_color') . '" name="' . $this->get_field_name('div_color') . '" type="text" value="' . esc_attr($div_color) . '" />';
        $rew_html .= '</p>';


        if($image_uri=="")
        {

        $showxy = 'display:none !important;';
        
        }
        else
        {
            $showxy = '';
        }
        ?>


<div id="rew_container">

<div class="widg-img">
       <br>
<label class="widg-label widg-img-label"  for="<?php echo $this->get_field_id('image_uri'); ?>" style="margin-left:10px !important;float:left;">Image</label>

<div style="float:right;margin-right:53%;">
    <label id="image_uri_agilysys_pos_testimonial_with_image_on_right_widget"></label><img
        style="<?php echo $showxy;?>" height="120" width="200"
        class="<?php echo $this->get_field_id('image_id'); ?>_media_image custom_media_image"
        src="<?php if (!empty($display_instance['image_uri'])) {echo $display_instance['image_uri'];}?>" />
    <input type="hidden" type="text" class="<?php echo $this->get_field_id('image_id'); ?>_media_id custom_media_id"
        name="<?php echo $this->get_field_name('image_id'); ?>" id="<?php echo $this->get_field_id('image_id'); ?>"
        value="<?php echo $display_instance['image_id']; ?>" />
    <input type="hidden" class="<?php echo $this->get_field_id('image_id'); ?>_media_url custom_media_url"
        name="<?php echo $this->get_field_name('image_uri'); ?>" id="<?php echo $this->get_field_id('image_uri'); ?>"
        value="<?php echo $display_instance['image_uri']; ?>">
    <input type="button" value="Upload Image" class="button custom_media_upload"
        id="<?php echo $this->get_field_id('image_id'); ?>" />
        </div>
</div>
</div>
<br><br><br><br><br><br>
<script>
jQuery(document).ready(function() {
    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    //if (attachment.height == 1080 && attachment.width == 1920) {
                        jQuery('.' + button_id_s + '_media_id').val(attachment.id);
                        jQuery('.' + button_id_s + '_media_url').val(attachment.url);
                        jQuery('.' + button_id_s + '_media_image').attr('src', attachment.url).css(
                            'display', 'block');
                       /* jQuery('#image_uri_agilysys_pos_testimonial_with_image_on_right_widget')
                            .html("");
                    } else {
                        jQuery('#image_uri_agilysys_pos_testimonial_with_image_on_right_widget')
                            .html("Please Enter the correct Dimensions 1920x1080").css(
                                'color', 'red');

                    }
                    */
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload');

});
</script>

<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_pos_testimonial_with_image_on_right_widget p{
    padding:10px;
}

#rew_container_agilysys_pos_testimonial_with_image_on_right_widget input,
select,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_pos_testimonial_with_image_on_right_widget label {
    width: 40%;
}

<?php echo '.' . $widget_add_id_slider;

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}



#entries_agilysys_pos_testimonial_with_image_on_right_widget {
    padding: 10px 0 0;
}

#entries_agilysys_pos_testimonial_with_image_on_right_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_pos_testimonial_with_image_on_right_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_pos_testimonial_with_image_on_right_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_pos_testimonial_with_image_on_right_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_pos_testimonial_with_image_on_right_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_pos_testimonial_with_image_on_right_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_pos_testimonial_with_image_on_right_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_pos_testimonial_with_image_on_right_widget #entries_agilysys_pos_testimonial_with_image_on_right_widget p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_pos_testimonial_with_image_on_right_widget">
    <?php echo $rew_html; ?>
</div>

<?php

    }
}
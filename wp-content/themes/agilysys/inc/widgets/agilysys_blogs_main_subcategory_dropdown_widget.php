<?php

function load_agilysys_blogs_main_subcategory_dropdown_widget()
{
    register_widget('agilysys_blogs_main_subcategory_dropdown_widget');
}

add_action('widgets_init', 'load_agilysys_blogs_main_subcategory_dropdown_widget');

class agilysys_blogs_main_subcategory_dropdown_widget extends WP_Widget
{

    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Blogs Main Subcategory Dropdown Widget', 'AGILYSYS_TEXT_DOMAIN'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {
echo $args['before_widget'];
         
        $category_type = $instance['category_type'];
        $category_id = get_cat_ID($category_type);

        $args = array(
            "hide_empty" => 0,
            "type" => "post",
            "orderby" => "name",
            "order" => "ASC",
            "child_of" => $category_id,
        );
        $categories = get_categories($args);

      


        $title = $instance['title'];

        ?>
     


<div class="blogCategories">
    <h3 class="greenText dinProStd"><?php echo $title; ?></h3>
    <select id="display_category" name="display_category" class=""
        onChange="display_posts_based_on_subcat(this.value);">
        <option value="">Select Category</option>

        <?php
foreach ($categories as $key) {
            ?>
        <option value="<?php echo $key->slug; ?>"><?php echo $key->cat_name; ?></option>
        <?php
}
        ?>
    </select>
</div>


<script>
function display_posts_based_on_subcat(val) {

    var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

    var data = {

        action: "get-display-posts-based-on-subcat",
        category: '<?php echo $instance["category_type"];?>',
        subcat: val
    };


    // Send the data
    jQuery.post(ajaxurl, data, function(response) {

        document.location.href = response;


    });

}
</script>

<?php
echo $args['after_widget'];
?>

<h3 class="greenText dinProStd">Tag Cloud</h3>

<?php
    }

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['title'] = $new_instance['title'];
        $instance['category_type'] = $new_instance['category_type'];

        return $instance;
    }

    public function form($display_instance)
    {
        $widget_add_id_slider_client = $this->get_field_id('') . "add_agilysys_blogs_main_subcategory_dropdown_widget";

        $title = $display_instance['title'];

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('title') . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input style="width:100%;" id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" type="text" value="' . $title . '" />';
        $rew_html .= '</p><br><br>';

        $args = array(
            "hide_empty" => 0,
            "type" => "post",
            "orderby" => "name",
            "order" => "ASC",
        );
        $categories = get_categories($args);

        $category_type = $display_instance['category_type'];

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('category_type') . '"> ' . __('Main Category', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<select name="' . $this->get_field_name('category_type') . '" id="' . $this->get_field_id('category_type') . '">';

        foreach ($categories as $key) {

            if ($category_type == $key->slug) {
                $rew_html .= '<option value="' . $key->slug . '" selected="selected">' . $key->cat_name . '</option>';
            } else {
                $rew_html .= '<option value="' . $key->slug . '">' . $key->cat_name . '</option>';
            }

        }

        $rew_html .= '</select>';

        ?>

        <style>
        .cf:before,
        .cf:after {
            content: "";
            display: table;
        }

        .cf:after {
            clear: both;
        }

        .cf {
            zoom: 1;
        }

        .clear {
            clear: both;
        }

        .clearfix:after {
            content: ".";
            display: block;
            height: 0;
            clear: both;
            visibility: hidden;
        }

        .clearfix {
            display: inline-block;
        }

        * html .clearfix {
            height: 1%;
        }

        .clearfix {
            display: block;
        }

        #rew_container_agilysys_blogs_main_subcategory_dropdown_widget input,
        select,
        textarea {
            float: right;
            width: 60%;
        }

        #rew_container_agilysys_blogs_main_subcategory_dropdown_widget label {
            width: 40%;
        }

        <?php echo '.' . $widget_add_id_slider_client;

        ?> {
            background: #ccc none repeat scroll 0 0;
            font-weight: bold;
            margin: 20px 0px 9px;
            padding: 6px;
            text-align: center;
            display: block !important;
            cursor: pointer;
        }

        .block-image {
            width: 50px;
            height: 30px;
            float: right;
            display: none;
        }

        .desc {
            height: 55px;
        }


        #entries {
            padding: 10px 0 0;
        }

        #entries .entrys {
            padding: 0;
            border: 1px solid #e5e5e5;
            margin: 10px 0 0;
            clear: both;
        }

        #entries .entrys:first-child {
            margin: 0;
        }

        #entries .delete-row {
            margin-top: 20px;
            float: right;
            text-decoration: underline;
            color: red;
        }

        #entries .entry-title {
            display: block;
            font-size: 14px;
            line-height: 18px;
            font-weight: 600;
            background: #f1f1f1;
            padding: 7px 5px;
            position: relative;
        }

        #entries .entry-title:after {
            content: '\f140';
            font: 400 20px/1 dashicons;
            position: absolute;
            right: 10px;
            top: 6px;
            color: #a0a5aa;
        }

        #entries .entry-title.active:after {
            content: '\f142';
        }

        #entries .entry-desc {
            display: none;
            padding: 0 10px 10px;
            border-top: 1px solid #e5e5e5;
        }

        #rew_container_agilysys_blogs_main_subcategory_dropdown_widget #entries p.last label {
            white-space: pre-line;
            float: left;
            width: 39%;
        }

        #message {
            padding: 6px;
            display: none;
            color: red;
            font-weight: bold;
        }
        </style>
        <div id="rew_container_agilysys_blogs_main_subcategory_dropdown_widget">
            <?php echo $rew_html; ?>
        </div>
<?php
}

}
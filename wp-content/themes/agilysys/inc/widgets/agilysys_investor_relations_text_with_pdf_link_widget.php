<?php

add_action('widgets_init', 'load_agilysys_investor_relations_text_with_pdf_link_widget');

function load_agilysys_investor_relations_text_with_pdf_link_widget()
{
    register_widget('agilysys_investor_relations_text_with_pdf_link_widget');
}

class agilysys_investor_relations_text_with_pdf_link_widget extends WP_Widget
{

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Investor Relations Text With Pdf Link Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');

    }

    public function widget($args, $instance)
    {

        echo $args['before_widget'];

        $section_title = $instance['section_title'];
        $section_title2 = $instance['section_title2'];

        $fin = array();
        $events = array();

        $count = count($instance['title_of_section']);

        for ($i = 0; $i < $count; $i++) {

            $type = $instance['type'][$i];

            $title_of_section = $instance['title_of_section'][$i];

            $link_type = $instance['link_type'][$i];
            if ($link_type == "link") {
                $anchor_link = $instance['anchor_link'][$i];
            } elseif ($link_type == "page") {
                $post_id = $instance['page'][$i];
                $post = get_post($post_id);
                $anchor_link = home_url($post->post_name) . "/";
            }

            if ($type == 'financial') {

                $arr1 = array();
                $arr1['title_of_section'] = $title_of_section;
                $arr1['anchor_link'] = $anchor_link;
                array_push($fin, $arr1);

            } elseif ($type == 'events') {
                $arr1 = array();
                $arr1['title_of_section'] = $title_of_section;
                $arr1['anchor_link'] = $anchor_link;
                array_push($events, $arr1);
            }
        }

        ?>

<div class="investosAboutSideBar">
    <h2 class="blackText"><?php echo $section_title; ?></h2>
    <div class="investorResports">
        <ul>
            <?php
foreach ($fin as $key) {
            ?>
            <li><a href="<?php echo $key['anchor_link']; ?>"><?php echo $key['title_of_section']; ?></a></li>
            <?php
}
        ?>
        </ul>
    </div>
    <div class="investorResports">
        <h2 class="blackText"><?php echo $section_title2; ?></h2>
        <ul>
            <?php
foreach ($events as $key) {
            ?>
            <li><a href="<?php echo $key['anchor_link']; ?>"><?php echo $key['title_of_section']; ?></a></li>
            <?php
}
        ?>
        </ul>
    </div>
</div>




<?php
echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $max_entries_agilysys_investor_relations_text_with_pdf_link_widget_slider_image = 15;

        $instance['section_title'] = strip_tags($new_instance['section_title']);
        $instance['section_title2'] = strip_tags($new_instance['section_title2']);
        $instance['what_makes_rows'] = strip_tags($new_instance['what_makes_rows']);

        $count = count($new_instance['title_of_section']);

        for ($i = 0; $i < $count; $i++) {

            $instance['type'][$i] = $new_instance['type'][$i];
            $instance['title_of_section'][$i] = $new_instance['title_of_section'][$i];
            /*  $instance['anchor_link' . $i] = strip_tags($new_instance['anchor_link' . $i]); */

            $instance['link_type'][$i] = $new_instance['link_type'][$i];
            if ($new_instance['link_type'][$i] == 'page') {
                $instance['page'][$i] = $new_instance['page'][$i];
                $instance['anchor_link'][$i] = '';
            } elseif ($new_instance['link_type'][$i] == 'link') {
                $instance['anchor_link'][$i] = $new_instance['anchor_link'][$i];
                $instance['page'][$i] = '';

            }

        }

        return $instance;

    }

    public function form($display_instance)
    {

        $max_entries_agilysys_investor_relations_text_with_pdf_link_widget_slider_image = 15;

        $widget_add_id_slider = $this->get_field_id('') . "add_agilysys_news_events_blog_post_widget";

        $what_makes_rows = ($display_instance['what_makes_rows']);

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('what_makes_rows') . '"> ' . __('No. of rows do you want to add*', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input class="what_makes_rows" id="' . $this->get_field_name('what_makes_rows') . '" name="' . $this->get_field_name('what_makes_rows') . '" type="number" value="' . $what_makes_rows . '" />';
        $rew_html .= '</p><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('section_title') . '"> ' . __('Section Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('section_title') . '" name="' . $this->get_field_name('section_title') . '" type="text" value="' . $display_instance['section_title'] . '">';
        $rew_html .= '</p><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('section_title2') . '"> ' . __('Section Title2', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('section_title2') . '" name="' . $this->get_field_name('section_title2') . '" type="text" value="' . $display_instance['section_title2'] . '">';
        $rew_html .= '</p><br><br>';

        $count = count($display_instance['title_of_section']);

        $rew_html .= '<div class="add_new_rowxx-input-containers"><div id="entries_agilysys_investor_relations_text_with_pdf_link_widget">';

        $rew_html .= '<input class="cnt909" id="cnt909" name="cnt" type="hidden" value="' . $count . '">';

        for ($i = 0; $i < $count; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'AGILYSYS_TEXT_DOMAIN') . ' </span>';

            $rew_html .= '<div class="entry-desc cf">';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('title_of_section' . $i) . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('title_of_section' . $i) . '" name="' . $this->get_field_name('title_of_section[]') . '" type="text" value="' . $display_instance['title_of_section'][$i] . '">';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('link_type' . $i) . '"> ' . __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('link_type' . $i) . '" name="' . $this->get_field_name('link_type[]') . '" onChange="show_hide_div_agilysys_investor_relations_text_with_pdf_link_widget(this.value,' . $i . ');">';
            $rew_html .= '<option value="">Please Select</option>';

            $link_type = $display_instance['link_type'][$i];

            if ($link_type == 'page') {
                $rew_html .= '<option value="page" selected="selected">Internal Page Link</option>';
            } else {
                $rew_html .= '<option value="page">Internal Page Link</option>';
            }

            if ($link_type == 'link') {
                $rew_html .= '<option value="link" selected="selected">External Link</option>';
            } else {
                $rew_html .= '<option value="link">External Link</option>';
            }

            $rew_html .= '</select>';
            $rew_html .= '</p><br><br>';

            $args = array(
                'sort_order' => 'desc',
                'sort_column' => 'post_title',
                'hierarchical' => 1,
                'exclude' => '',
                'include' => '',
                'meta_key' => '',
                'meta_value' => '',
                'authors' => '',
                'child_of' => 0,
                'parent' => -1,
                'exclude_tree' => '',
                'number' => '',
                'offset' => 0,
                'post_type' => 'page',
                'post_status' => 'publish',
            );
            $pages = get_pages($args); // get all pages based on supplied args

            if ($link_type == 'page') {
                $show1 = 'style="display:block"';
                $show2 = 'style="display:none"';
            } elseif ($link_type == 'link') {
                $show1 = 'style="display:none"';
                $show2 = 'style="display:block"';

            } else {
                $show1 = 'style="display:none"';
                $show2 = 'style="display:none"';
            }
            $rew_html .= '<div id="page_div_agilysys_investor_relations_text_with_pdf_link_widget' . $i . '" ' . $show1 . '><p>';
            $rew_html .= '<label for="' . $this->get_field_id('page' . $i) . '"> ' . __('Page', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('page' . $i) . '" name="' . $this->get_field_name('page[]') . '">';
            $rew_html .= '<option value="">Please Select</option>';

            $page = $display_instance['page'][$i];

            foreach ($pages as $key) {

                if ($page == $key->ID) {
                    $rew_html .= '<option value="' . $key->ID . '" selected="selected">' . $key->post_title . '</option>';
                } else {
                    $rew_html .= '<option value="' . $key->ID . '">' . $key->post_title . '</option>';
                }

            }

            $rew_html .= '</select>';
            $rew_html .= '</p></div><br><br>';

            $rew_html .= '<div id="link_div_agilysys_investor_relations_text_with_pdf_link_widget' . $i . '" ' . $show2 . '><p>';
            $rew_html .= '<label for="' . $this->get_field_id('anchor_link' . $i) . '"> ' . __('Link', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('anchor_link' . $i) . '" name="' . $this->get_field_name('anchor_link[]') . '" type="text" value="' . $display_instance['anchor_link'][$i] . '" />';
            $rew_html .= '</p></div><br><br>';
            ?>
<script>
function show_hide_div_agilysys_investor_relations_text_with_pdf_link_widget(val, i) {
console.log(val);
    if (val == 'page') {
        jQuery("#page_div_agilysys_investor_relations_text_with_pdf_link_widget" + i).show();
        jQuery("#link_div_agilysys_investor_relations_text_with_pdf_link_widget" + i).hide();
    } else if (val == 'link') {
        jQuery("#page_div_agilysys_investor_relations_text_with_pdf_link_widget" + i).hide();
        jQuery("#link_div_agilysys_investor_relations_text_with_pdf_link_widget" + i).show();
    }

}
</script>

<?php

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('type' . $i) . '"> ' . __('Type', 'agilysys_text_domain') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('type' . $i) . '" name="' . $this->get_field_name('type[]') . '" onChange="show_hide_media_agilysys_investor_relations_text_with_pdf_link_widget(this.value,' . $i . ');">';
            $rew_html .= '<option value="">Please Select</option>';

            if ($display_instance['type'][$i] == "financial") {
                $rew_html .= '<option value="financial" selected="selected">Financial Reports</option>';
            } else {
                $rew_html .= '<option value="financial">Financial Reports</option>';
            }

            if ($display_instance['type'][$i] == "events") {
                $rew_html .= '<option value="events" selected="selected">Events & Presentations</option>';
            } else {
                $rew_html .= '<option value="events">Events & Presentations</option>';
            }

            $rew_html .= '</select>';
            $rew_html .= '</p>';

            $k = $i + 1;
            $rew_html .= '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' . $k . ');">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';

        }

        $rew_html .= '</div></div>';
        $rew_html .= '<div id="message">' . __('Sorry, you reached to the limit of', 'AGILYSYS_TEXT_DOMAIN') . ' "' . $what_makes_rows . '" ' . __('maximum entries_agilysys_investor_relations_text_with_pdf_link_widget', 'AGILYSYS_TEXT_DOMAIN') . '.</div>';

        $rew_html .= '<div class="' . $widget_add_id_slider . '" style="text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;" onClick="add_new_row();">' . __('ADD ROW', 'AGILYSYS_TEXT_DOMAIN') . '</div>';
        ?>

<script>
function add_new_row() {
    var cnt = '';

    jQuery.each(jQuery("#entries_agilysys_investor_relations_text_with_pdf_link_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            cnt = jQuery(this).val();
        }
    });




    var what_makes_rows = jQuery('.what_makes_rows').val();

console.log(cnt);

 if (parseInt(cnt) < parseInt(what_makes_rows)) {

        cnt++;

        jQuery.each(jQuery("#entries_agilysys_investor_relations_text_with_pdf_link_widget .cnt909"), function() {
            if (jQuery(this).val() != '') {
                jQuery(this).val(cnt);
            }
        });

        var new_row = '<div id="entry' + cnt +
            '"  class="entrys"><span class="entry-title" onclick = "slider(this);"><?php echo __('Add New Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span>';
        new_row += '<div class="entry-desc cf">';


        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Title', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('title_of_section[]')); ?>" type="text" value="">';
        new_row += '</p>';






        new_row += '<p>';
        new_row += '<label for="<?php echo $this->get_field_id('link_type'); ?>' + cnt +
            '"><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
        new_row += '<select id="<?php echo $this->get_field_id('link_type'); ?>' + cnt +
            '" name="<?php echo $this->get_field_name('link_type[]'); ?>" onChange="show_hide_div_agilysys_investor_relations_text_with_pdf_link_widget(this.value,' + cnt +
            ');">';
        new_row += '<option value="">Please Select</option>';
        new_row += '<option value="page">Internal Page Link</option>';
        new_row += '<option value="link">External Link</option>';
        new_row += '</select>';
        new_row += '</p><br><br>';


        <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        ?>






        new_row += '<div id="page_div_agilysys_investor_relations_text_with_pdf_link_widget' + cnt + '" style="display:none;"><p>';
        new_row += '<label><?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';

        new_row += '<select name="<?php echo $this->get_field_name('page[]'); ?>">';
        new_row += '<option value="">Please Select</option>';

        <?php
foreach ($pages as $key) {
            ?>
        new_row += '<option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>';

        <?php
}
        ?>
        new_row += '</select>';
        new_row += '</p></div><br><br>';

        new_row += '<div id="link_div_agilysys_investor_relations_text_with_pdf_link_widget' + cnt + '" style="display:none;"><p>';
        new_row += '<label><?php echo __('URL', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
        new_row += '<input  name="<?php echo $this->get_field_name('anchor_link[]'); ?>" type="text" value="" />';
        new_row += '</p></div><br><br>';



        new_row += '<p>';
        new_row += '<label><?php echo __('Type', 'agilysys_text_domain'); ?> :</label>';
        new_row += '<select name="<?php echo $this->get_field_name('type[]'); ?>"  onChange="show_hide_media_agilysys_investor_relations_text_with_pdf_link_widget(this.value,'+ cnt +');">';
        new_row += '<option value="">Please Select</option>';
        new_row += '<option value="financial">Financial Reports</option>';
        new_row += '<option value="events">Events & Presentations</option>';
        new_row += '</select>';
        new_row += '</p>';


        var new_cnt = cnt;

        new_row += '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' + new_cnt +
            ');"><?php echo __('Delete Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span></a></p>';
        new_row += '</div></div>';

        jQuery('.add_new_rowxx-input-containers #entries_agilysys_investor_relations_text_with_pdf_link_widget').append(new_row);

    }


}

function show_hide_div_agilysys_investor_relations_text_with_pdf_link_widget(val, i) {
console.log(val);
    if (val == 'page') {
        jQuery("#page_div_agilysys_investor_relations_text_with_pdf_link_widget" + i).show();
        jQuery("#link_div_agilysys_investor_relations_text_with_pdf_link_widget" + i).hide();
    } else if (val == 'link') {
        jQuery("#page_div_agilysys_investor_relations_text_with_pdf_link_widget" + i).hide();
        jQuery("#link_div_agilysys_investor_relations_text_with_pdf_link_widget" + i).show();
    }

}

function delete_row(cnt) {
    jQuery.each(jQuery(".add_new_rowxx-input-containers #entries_agilysys_investor_relations_text_with_pdf_link_widget"), function() {
        jQuery(' #entry' + cnt).remove();
    });
    var last_cnt = 0;

    jQuery.each(jQuery("#entries_agilysys_investor_relations_text_with_pdf_link_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            last_cnt = jQuery(this).val();
        }
    });

    last_cnt--;
    jQuery.each(jQuery("#entries_agilysys_investor_relations_text_with_pdf_link_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(last_cnt);
        }
    });
    jQuery(".what_makes_rows").val(last_cnt);
    jQuery('.what_makes_rows').trigger('change');

}
</script>
<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}



#rew_container_agilysys_investor_relations_text_with_pdf_link_widget select {
    float: left;
    width: 60%;
    margin-top: 20px !important;
    margin-bottom: 10px !important;
}

#rew_container_agilysys_investor_relations_text_with_pdf_link_widget input,

textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_investor_relations_text_with_pdf_link_widget label {
    width: 40%;
    float: left;
}


#rew_container_agilysys_investor_relations_text_with_pdf_link_widget p {
    padding:10px;
}

<?php echo '.' . $widget_add_id_slider;

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}



#entries_agilysys_investor_relations_text_with_pdf_link_widget {
    padding: 10px 0 0;
}

#entries_agilysys_investor_relations_text_with_pdf_link_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_investor_relations_text_with_pdf_link_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_investor_relations_text_with_pdf_link_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_investor_relations_text_with_pdf_link_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_investor_relations_text_with_pdf_link_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_investor_relations_text_with_pdf_link_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_investor_relations_text_with_pdf_link_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_investor_relations_text_with_pdf_link_widget #entries_agilysys_investor_relations_text_with_pdf_link_widget p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_investor_relations_text_with_pdf_link_widget">
    <?php echo $rew_html; ?>
</div>

<?php
}

}
<?php

function agilysys_our_customer_client_logo_widgets()
{
    register_widget('agilysys_our_customer_client_logo_widget');
}

add_action('widgets_init', 'agilysys_our_customer_client_logo_widgets');

class agilysys_our_customer_client_logo_widget extends WP_Widget
{
    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Our Customer Logo Widget', 'AGILYSYS_TEXT_DOMAIN'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {
        extract($args);
        $max_entries_agilysys_our_customer_client_logo_widget_slider_image = '25';
        echo $args['before_widget'];
        ?>



<section class="cusLogoSection flex">
    <?php
$count = count($instance['image_uri_slide_client']);
        for ($i = 0; $i < $max_entries_agilysys_our_customer_client_logo_widget_slider_image; $i++) {

            $image_url_slider_client = esc_url($instance['image_uri_slide_client'][$i]);
            $image_uri_alt = $instance['image_uri_alt'];
            ?>
    <div class="cusLogo">
        <img class="img-fluid" src="<?php echo $image_url_slider_client; ?>" alt="<?php echo $image_uri_alt; ?>" />
    </div>
    <?php }?>
</section>


<?php
echo $args['after_widget'];
    }

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $count = count($new_instance['image_uri_slide_client']);

        for ($i = 0; $i < $count; $i++) {

            $instance['image_uri_slide_client'][$i] = strip_tags($new_instance['image_uri_slide_client'][$i]);
            $instance['image_uri_alt'][$i] = strip_tags($new_instance['image_uri_alt'][$i]);

        }
        return $instance;
    }

    public function form($display_instance)
    {

        $widget_add_id_slider_client = $this->get_field_id('') . "add";

        $title = ($display_instance['slider_title']);

        if (!empty($display_instance['client_logo_rows'])) {
            $client_logo_rows = ($display_instance['client_logo_rows']);
        } else {
            $client_logo_rows = 0;
        }

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('client_logo_rows') . '"> ' . __('No. of rows do you want to add*', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input class="client_logo_rows" id="' . $this->get_field_name('client_logo_rows') . '" name="' . $this->get_field_name('client_logo_rows') . '" type="number" value="' . $client_logo_rows . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';

        $count = count($display_instance['image_uri_slide_client']);

        $rew_html .= '<div class="add_new_rowxx-input-containers"><div id="entries_agilysys_our_customer_client_logo_widget">';

        $rew_html .= '<input class="cnt909" id="cnt909" name="cnt" type="hidden" value="' . $count . '">';
        for ($i = 0; $i < $count; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'AGILYSYS_TEXT_DOMAIN') . ' </span>';

            $rew_html .= '<div class="entry-desc cf">';

            $rew_html .= '<div class="widg-img' . $i . '">';
            $show1 = (empty($display_instance['image_uri_slide_client'][$i])) ? 'style="display:none;"' : '';
            $rew_html .= '<label id="image_uri_agilysys_our_customer_client_logo_widget' . $i . '"></label><br><img class="' . $this->get_field_id('image_id' . $i) . '_media_image' . $i . ' custom_media_image' . $i . '" src="' . $display_instance['image_uri_slide_client'][$i] . '" ' . $show1 . ' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id' . $i) . '_media_id' . $i . ' custom_media_id' . $i . '" name="' . $this->get_field_name('image_id[]') . '" id="' . $this->get_field_id('image_id' . $i) . '" value="' . $display_instance['image_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id' . $i) . '_media_url' . $i . ' custom_media_url' . $i . '" name="' . $this->get_field_name('image_uri_slide_client[]') . '" id="' . $this->get_field_id('image_uri_slide_client-' . $i) . '" value="' . $display_instance['image_uri_slide_client'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_upload' . $i . '" id="' . $this->get_field_id('image_id' . $i) . '"/>';

            $rew_html .= '</div>';
            ?>


<script>
jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    if (attachment.height == 1080 && attachment.width == 1920) {
                        jQuery('.' + button_id_s + '_media_id<?php echo $i; ?>').val(attachment.id);
                        jQuery('.' + button_id_s + '_media_url<?php echo $i; ?>').val(attachment
                            .url);
                        jQuery('.' + button_id_s + '_media_image<?php echo $i; ?>').attr('src',
                            attachment.url).css('display', 'block');
                        jQuery(
                                '#image_uri_agilysys_our_customer_client_logo_widget<?php echo $i; ?>')
                            .html("");
                    } else {
                        jQuery(
                                '#image_uri_agilysys_our_customer_client_logo_widget<?php echo $i; ?>')
                            .html("Please Enter the correct Dimensions 1920x1080").css(
                                'color', 'red');

                    }



                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload<?php echo $i; ?>');

});
</script>


<?php

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt') . '"> ' . __('Image Alt', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input  id="' . $this->get_field_name('image_uri_alt') . '" name="' . $this->get_field_name('image_uri_alt[]') . '" type="number" value="' . $display_instance['image_uri_alt'] . '" />';
            $rew_html .= '</p>';
            $k = $i + 1;
            $rew_html .= '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' . $k . ');">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';

        }
        $rew_html .= '</div></div>';

        $rew_html .= '<div class="' . $widget_add_id_slider_client . '" style="margin-bottom: 36px;text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;" onClick="add_new_row();">' . __('ADD ROW', 'AGILYSYS_TEXT_DOMAIN') . '</div>';

        ?>
<script>
function add_new_row() {
    var cnt = '';

    jQuery.each(jQuery("#entries_agilysys_our_customer_client_logo_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            cnt = jQuery(this).val();
        }
    });




    var client_logo_rows = jQuery('.client_logo_rows').val();

console.log(cnt);

    if (parseInt(cnt) < parseInt(client_logo_rows)) {

        cnt++;

        jQuery.each(jQuery("#entries_agilysys_our_customer_client_logo_widget .cnt909"), function() {
            if (jQuery(this).val() != '') {
                jQuery(this).val(cnt);
            }
        });

        var new_row = '<div id="entry' + cnt +
            '"  class="entrys"><span class="entry-title" onclick = "slider(this);"><?php echo __('Add New Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span>';
        new_row += '<div class="entry-desc cf">';





        new_row += '<div class="widg-img' + cnt + '">';
        new_row += '<label id="image_uri_agilysys_our_customer_client_logo_widget' + cnt +
            '"></label><br><img class="<?php echo $this->get_field_id('image_uri_slide_client'); ?>' + cnt +
            '_media_image' +
            cnt +
            ' custom_media_image' + cnt + '" src="" style="display:none;" width=200" height="120"/>';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_uri_slide_client'); ?>' + cnt +
            '_media_id' +
            cnt + ' custom_media_id' + cnt +
            '" name="<?php echo $this->get_field_name('image_uri_slide_client[]'); ?>"  value="" />';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_uri_slide_client'); ?>' + cnt +
            '_media_url' +
            cnt + ' custom_media_url' + cnt +
            '" name="<?php echo $this->get_field_name('image_uri_slide_client[]'); ?>" id="<?php echo $this->get_field_id('image_uri_slide_client'); ?>' +
            cnt + '" value="">';
        new_row += '<input type="button" value="Upload Image" class="button custom_media_upload' + cnt +
            '" id="<?php echo $this->get_field_id('image_uri_slide_client'); ?>' + cnt + '"/>';
        new_row += '</div><br><br>';


        jQuery(document).ready(function() {




            function media_upload(button_class) {
                var _custom_media = true,
                    _orig_send_attachment = wp.media.editor.send.attachment;
                jQuery('body').on('click', '.custom_media_upload' + cnt, function(e) {
                    var button_id = '#' + jQuery(this).attr('id');
                    var button_id_s = jQuery(this).attr('id');
                    console.log(button_id);
                    var self = jQuery(button_id);
                    var send_attachment_bkp = wp.media.editor.send.attachment;
                    var button = jQuery(button_id);
                    var id = button.attr('id').replace('_button', '');
                    _custom_media = true;

                    wp.media.editor.send.attachment = function(props, attachment) {
                        if (_custom_media) {

                            if (attachment.height == 1080 && attachment.width == 1920) {
                                jQuery('.' + button_id_s + '_media_id' + cnt).val(attachment.id);
                                jQuery('.' + button_id_s + '_media_url' + cnt).val(attachment.url);
                                jQuery('.' + button_id_s + '_media_image' + cnt).attr('src',
                                    attachment.url).css('display', 'block');

                                jQuery('#image_uri_agilysys_our_customer_client_logo_widget' + cnt)
                                    .html("");
                            } else {
                                jQuery('#image_uri_agilysys_our_customer_client_logo_widget' + cnt)
                                    .html("Please Enter the correct Dimensions 1920x1080").css(
                                        'color', 'red');

                            }
                        } else {
                            return _orig_send_attachment.apply(button_id, [props, attachment]);
                        }
                    }
                    wp.media.editor.open(button);
                    return false;
                });
            }
            media_upload('.custom_media_upload' + cnt);

        });







   new_row += '<p>';
    new_row += '<label><?php echo __('Image Alt', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
    new_row += '<input name="<?php echo $this->get_field_name('image_uri_alt[]'); ?>" type="text"  />';
    new_row += '</p>';






        var new_cnt = cnt;

        new_row += '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' + new_cnt +
            ');"><?php echo __('Delete Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span></a></p>';
        new_row += '</div></div>';

        jQuery('.add_new_rowxx-input-containers #entries_agilysys_our_customer_client_logo_widget').append(new_row);

    }


}



function delete_row(cnt) {
    jQuery.each(jQuery(".add_new_rowxx-input-containers #entries_agilysys_our_customer_client_logo_widget"), function() {
        jQuery(' #entry' + cnt).remove();
    });
    var last_cnt = 0;

    jQuery.each(jQuery("#entries_agilysys_our_customer_client_logo_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            last_cnt = jQuery(this).val();
        }
    });

    last_cnt--;
    jQuery.each(jQuery("#entries_agilysys_our_customer_client_logo_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(last_cnt);
        }
    });
    jQuery(".client_logo_rows").val(last_cnt);
    jQuery('.client_logo_rows').trigger('change');

}
</script>
<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_our_customer_client_logo_widget input,
select,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_our_customer_client_logo_widget label {
    width: 40%;
}

<?php echo '.'. $widget_add_id_slider;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}


#entries_agilysys_our_customer_client_logo_widget {
    padding: 10px 0 0;
}

#entries_agilysys_our_customer_client_logo_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_our_customer_client_logo_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_our_customer_client_logo_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_our_customer_client_logo_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_our_customer_client_logo_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_our_customer_client_logo_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_our_customer_client_logo_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_our_customer_client_logo_widget #entries_agilysys_our_customer_client_logo_widget p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_our_customer_client_logo_widget">
    <?php echo $rew_html; ?>
</div>
<?php
} //Function form ends here

}
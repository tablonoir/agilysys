<?php

add_action('widgets_init', 'load_agilysys_resources_video_slider_widget');

function load_agilysys_resources_video_slider_widget()
{
    register_widget('agilysys_resources_video_slider_widget');
}

class agilysys_resources_video_slider_widget extends WP_Widget
{
    /**
     * constructor -- name this the same as the class above
     */

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Resources Video Slider Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');

    }

    public function widget($args, $instance)
    {

        echo $args['before_widget'];

        $count = count($instance['type']);

        ?>


<section class="videoSlider orangeBG">
    <div class="swiper-container">
        <div class="swiper-wrapper">


        <?php

        for ($i = 0; $i < $count; $i++) {

            $title = strip_tags($instance['title'][$i]);
            $title2 = strip_tags($instance['title2'][$i]);
            $desc = strip_tags($instance['desc'][$i]);
            $type = $instance['type'][$i];
            $image_uri = strip_tags($instance['image_uri'][$i]);

            $image_uri_alt = $instance['image_uri_alt'][$i];

            $video_uri = $instance['video_uri'][$i];
            $youtube_uri = $instance['youtube_uri'][$i];

            ?>
            <div class="swiper-slide">
                <div class="videoSliderSlide flex">
                    <div class="videoSliderSlideTitle">
                        <h2 class="h2 dinProStd whiteText"><?php echo $title; ?></h2>
                    </div>
                    <div class="videoSliderSlideImg">



                        <?php

            if ($type == "image") {
                ?>
    <img src="<?php echo $image_uri; ?>" class="img-fluid" alt="<?php echo $image_uri_alt; ?>">


    <?php
} elseif ($type == "video") {
                ?>
    <video width="100%" height="100%" autoplay muted>
        <source src="<?php echo $video_uri; ?>" type="video/mp4">
    </video>

    <?php
} elseif ($type == "youtube") {

                ?>

    <iframe src='<?php echo $youtube_uri; ?>?enablejsapi=1&version=3&playerapiid=ytplayer'
        id="youtube_<?php echo $i; ?>" class="yt_players" frameborder='0' width="669" height="382"
        allowscriptaccess="always"></iframe>


    <?php
}
            ?>
                    </div>
                    <div class="videoSliderSlideContent">
                        <h3 class="dinProStd whiteText"><?php echo $title2; ?></h3>
                        <p class="whiteText"><?php echo substr($desc, 0, 250); ?></p>

                    </div>
                </div>
            </div>

            <?php

        }

        ?>

        </div>
        <!-- Add Arrows -->
        <div class="videoSliderButton">
            <div class="swiper-button-next swiper-button-disabled" tabindex="0" role="button" aria-label="Next slide"
                aria-disabled="true">
                <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/right-arrow-white.png" alt="">
            </div>
            <div class="swiper-button-prev swiper-button-disabled" tabindex="0" role="button"
                aria-label="Previous slide" aria-disabled="true">
                <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/left-arrow-white.png" alt="">
            </div>
        </div>
    </div>
</section>

<script>
jQuery(document).ready(function(){

var swiper = new Swiper('.videoSlider .swiper-container', {
    slidesPerView: 1,
    pagination: {
        el: '.videoSlider .swiper-pagination',
        type: 'progressbar',
      },
      navigation: {
        nextEl: '.videoSlider .swiper-button-next',
        prevEl: '.videoSlider .swiper-button-prev',
      },
    });
});
</script>


<?php
echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['what_makes_rows'] = strip_tags($new_instance['what_makes_rows']);

        $count = count($new_instance['type']);

        for ($i = 0; $i < $count; $i++) {

            $instance['title'][$i] = strip_tags($new_instance['title'][$i]);
            $instance['title2'][$i] = strip_tags($new_instance['title2'][$i]);
            $instance['desc'][$i] = strip_tags($new_instance['desc'][$i]);

            $instance['type'][$i] = $new_instance['type'][$i];
            $instance['image_uri'][$i] = strip_tags($new_instance['image_uri'][$i]);
            $instance['image_uri_alt'][$i] = strip_tags($new_instance['image_uri_alt'][$i]);
            $instance['video_uri'][$i] = $new_instance['video_uri'][$i];
            $instance['youtube_uri'][$i] = $new_instance['youtube_uri'][$i];

        }
        return $instance;
    }

    public function form($display_instance)
    {

        $widget_add_id_webinars_info = $this->get_field_id('') . "add_agilysys_customer_stories_spotlight_widget";

        $section_title = $display_instance['section_title'];

        if (!empty($display_instance['what_makes_rows'])) {
            $what_makes_rows = ($display_instance['what_makes_rows']);
        } else {
            $what_makes_rows = 0;
        }

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('section_title') . '"> ' . __('Section Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('section_title') . '" name="' . $this->get_field_name('section_title') . '" type="text" value="' . $display_instance['section_title'] . '" />';
        $rew_html .= '</p><br><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('what_makes_rows') . '"> ' . __('No. of rows do you want to add*', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input class="what_makes_rows" id="' . $this->get_field_name('what_makes_rows') . '" name="' . $this->get_field_name('what_makes_rows') . '" type="number" value="' . $what_makes_rows . '" />';
        $rew_html .= '</p><br><br>';

        $count = count($display_instance['title']);

        $rew_html .= '<div class="add_new_rowxx-input-containers"><div id="entries_agilysys_resources_video_slider_widget">';

        $rew_html .= '<input class="cnt909" id="cnt909" name="cnt" type="hidden" value="' . $count . '">';

        for ($i = 0; $i < $count; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'AGILYSYS_TEXT_DOMAIN') . ' </span>';

            $rew_html .= '<div class="entry-desc cf">';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('title' . $i) . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('title' . $i) . '" name="' . $this->get_field_name('title[]') . '" type="text" value="' . $display_instance['title'][$i] . '" />';
            $rew_html .= '</p><br>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('title2' . $i) . '"> ' . __('Title 2', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('title2' . $i) . '" name="' . $this->get_field_name('title2[]') . '" type="text" value="' . $display_instance['title2'][$i] . '" />';
            $rew_html .= '</p><br>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('desc' . $i) . '"> ' . __('Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<textarea rows="6" cols="35" id="' . $this->get_field_id('desc' . $i) . '" name="' . $this->get_field_name('desc[]') . '" > ' . $display_instance['desc'][$i] . '</textarea>';
            $rew_html .= '</p><br><br><br><br><br><br><br>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('type' . $i) . '"> ' . __('Media Type', 'agilysys_text_domain') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('type' . $i) . '" name="' . $this->get_field_name('type[]') . '" onChange="show_hide_media_agilysys_resources_video_slider_widget(this.value,' . $i . ');">';
            $rew_html .= '<option value="">Please Select</option>';

            if ($display_instance['type'][$i] == "image") {
                $rew_html .= '<option value="image" selected="selected">Image</option>';
            } else {
                $rew_html .= '<option value="image">Image</option>';
            }

            if ($display_instance['type'][$i] == "video") {
                $rew_html .= '<option value="video" selected="selected">Video</option>';
            } else {
                $rew_html .= '<option value="video">Video</option>';
            }

            if ($display_instance['type'][$i] == "youtube") {
                $rew_html .= '<option value="youtube" selected="selected">Youtube url</option>';
            } else {
                $rew_html .= '<option value="youtube">Youtube url</option>';
            }

            $rew_html .= '</select>';
            $rew_html .= '</p>';
            $show1 = (!empty($display_instance['image_uri'][$i]) && $display_instance['type'][$i] == "image") ? '' : 'style="display:none;"';
            $rew_html .= '<div class="widg-img' . $i . '" ' . $show1 . '>';

            $show1 = (empty($display_instance['image_uri'][$i])) ? 'style="display:none;"' : '';
            $rew_html .= '<label id="image_uri_agilysys_resources_video_slider_widget' . $i . '"></label><br><img class="' . $this->get_field_id('image_uri' . $i) . '_media_imageyy' . $i . ' custom_media_imageyy' . $i . '" src="' . $display_instance['image_uri'][$i] . '" ' . $show1 . ' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_uri' . $i) . '_media_idyy' . $i . ' custom_media_idyy' . $i . '" name="' . $this->get_field_name('image_id[]') . '" id="' . $this->get_field_id('image_uri' . $i) . '" value="' . $display_instance['image_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_uri' . $i) . '_media_urlyy' . $i . ' custom_media_urlyy' . $i . '" name="' . $this->get_field_name('image_uri[]') . '" id="' . $this->get_field_id('image_uri' . $i) . '" value="' . $display_instance['image_uri'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload Logo" class="button custom_media_uploadyy' . $i . '" id="' . $this->get_field_id('image_uri' . $i) . '"/>';

            ?>

<script>

function show_hide_media_agilysys_resources_video_slider_widget(value, id) {

console.log(value);

    if (value == "image") {

        jQuery('.widg-img' + id).show();
        jQuery('.widg-youtube' + id).hide();
        jQuery('.widg-video' + id).hide();

    } else if (value == "youtube") {
        jQuery('.widg-img' + id).hide();
        jQuery('.widg-youtube' + id).show();
        jQuery('.widg-video' + id).hide();
    } else if (value == "video") {
        jQuery('.widg-img' + id).hide();
        jQuery('.widg-youtube' + id).hide();
        jQuery('.widg-video' + id).show();
    }

}

jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_uploadyy<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    //if (attachment.height == 1080 && attachment.width == 1920) {
                    jQuery('.' + button_id_s + '_media_idyy<?php echo $i; ?>').val(attachment
                        .id);
                    jQuery('.' + button_id_s + '_media_urlyy<?php echo $i; ?>').val(attachment
                        .url);
                    jQuery('.' + button_id_s + '_media_imageyy<?php echo $i; ?>').attr('src',
                        attachment.url).css('display', 'block');

                    jQuery('#image_uri_agilysys_resources_video_slider_widget<?php echo $i; ?>')
                        .html("");
                    /*} else {
                        jQuery('#image_uri_agilysys_resources_video_slider_widget<?php echo $i; ?>')
                            .html("Please Enter the correct Dimensions 1920x1080").css('color',
                                'red');

                    }
                    */
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_uploadyy<?php echo $i; ?>');

});
</script>

<?php

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt' . $i) . '"> ' . __('Image Alt', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt' . $i) . '" name="' . $this->get_field_name('image_uri_alt[]') . '" type="text" value="' . $display_instance['image_uri_alt'][$i] . '">';
            $rew_html .= '</p>';

            $rew_html .= '</div><br><br>';

            $show2 = (!empty($display_instance['video_uri'][$i]) && $display_instance['type'][$i] == "video") ? '' : 'style="display:none;"';
            $rew_html .= '<div class="widg-video' . $i . '" ' . $show2 . '>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('video_id' . $i) . '"> ' . __('Video', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<video class="' . $this->get_field_id('video_id' . $i) . '_media_videov' . $i . ' custom_media_videov' . $i . '" width="320" height="240" controls ' . $show2 . '><source src="' . $display_instance['video_uri'][$i] . '" type="video/mp4"></video>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('video_id' . $i) . '_media_idv' . $i . ' custom_media_idv' . $i . '" name="' . $this->get_field_name('video_id[]') . '" id="' . $this->get_field_id('video_id' . $i) . '" value="' . $display_instance['video_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('video_id' . $i) . '_media_urlv' . $i . ' custom_media_urlv' . $i . '" name="' . $this->get_field_name('video_uri[]') . '" id="' . $this->get_field_id('video_uri' . $i) . '" value="' . $display_instance['video_uri'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload Video" class="button custom_media_uploadv' . $i . '" id="' . $this->get_field_id('video_id' . $i) . '"/>';
            $rew_html .= '</p>';

            $rew_html .= '</div>';

            $show3 = (!empty($display_instance['youtube_uri'][$i]) && $display_instance['type'][$i] == "youtube") ? '' : 'style="display:none;"';

            $rew_html .= '<div class="widg-youtube' . $i . '" ' . $show3 . '>';
            $rew_html .= '<input type="text"  name="' . $this->get_field_name('youtube_uri[]') . '" id="' . $this->get_field_id('media_uri' . $i) . '" value="' . $display_instance['youtube_uri'][$i] . '" />';
            $rew_html .= '</p>';

            $rew_html .= '</div>';
            ?>
<script>
jQuery(document).ready(function() {




    function media_uploadv(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_uploadv<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {
                    jQuery('.' + button_id_s + '_media_idv<?php echo $i; ?>').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_urlv<?php echo $i; ?>').val(attachment.url);
                    jQuery('.' + button_id_s + '_media_videov<?php echo $i; ?>').attr('src',
                        attachment.url).css('display', 'block');
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_uploadv('.custom_media_uploadv<?php echo $i; ?>');


});
</script>

            <?php

            $k = $i + 1;
            $rew_html .= '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' . $k . ');">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';

        }

        $rew_html .= '</div></div>';

        $rew_html .= '<div class="' . $widget_add_id_webinars_info . '" style="margin-bottom: 36px;text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;" onClick="add_new_row();">' . __('ADD ROW', 'ZWREW_TEXT_DOMAIN') . '</div>';
        ?>


<script>
function show_hide_title_agilysys_resources_video_slider_widget(val) {
    console.log(val);

    if (val == 'yes') {
        jQuery("#link_type_id_p_agilysys_resources_video_slider_widget").show();

    } else if (val == 'no') {
        jQuery("#link_type_id_p_agilysys_resources_video_slider_widget").hide();

    }
}

function add_new_row() {
    var cnt = '';

    jQuery.each(jQuery("#entries_agilysys_resources_video_slider_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            cnt = jQuery(this).val();
        }
    });




    var what_makes_rows = jQuery('.what_makes_rows').val();

    console.log(cnt);

    if (parseInt(cnt) < parseInt(what_makes_rows)) {

        cnt++;

        jQuery.each(jQuery("#entries_agilysys_resources_video_slider_widget .cnt909"), function() {
            if (jQuery(this).val() != '') {
                jQuery(this).val(cnt);
            }
        });

        var new_row = '<div id="entry' + cnt +
            '"  class="entrys"><span class="entry-title" onclick = "slider(this);"><?php echo __('Add New Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span>';
        new_row += '<div class="entry-desc cf">';


        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Title', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('title[]')); ?>" type="text" value="">';
        new_row += '</p><br>';

        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Title 2', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('title2[]')); ?>" type="text" value="">';
        new_row += '</p><br>';


        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Description', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
        new_row +=
            '<textarea rows="6" cols="35" name="<?php echo esc_attr($this->get_field_name('desc[]')); ?>"></textarea>';
        new_row += '</p><br><br><br><br><br><br><br>';



        new_row += '<p>';
    new_row += '<label ><?php echo __('Media Type', 'agilysys_text_domain'); ?> :</label>';
    new_row +=
        '<select name="<?php echo $this->get_field_name('type[]'); ?>"  onChange="show_hide_media_agilysys_video_slider(this.value,' +
        cnt + ');">';
    new_row += '<option value="">Please Select</option>';
    new_row += '<option value="image">Image</option>';
    new_row += '<option value="video">Video</option>';
    new_row += '<option value="youtube">Youtube url</option>';
    new_row += '</select>';
    new_row += '</p>';


        new_row += '<div class="widg-img' + cnt + '" style="display:none;">';

        new_row += '<label id="image_uri_agilysys_resources_video_slider_widget' + cnt +
            '"></label><br><img class="<?php echo $this->get_field_id('image_uri'); ?>' + cnt + '_media_imageyy' + cnt +
            ' custom_media_imageyy' + cnt + '" src="" style="display:none;" width=200" height="120"/>';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_uri'); ?>' + cnt + '_media_idyy' +
            cnt + 'custom_media_idyy' + cnt + '" name="<?php echo $this->get_field_name('image_id[]'); ?>" />';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_uri'); ?>' + cnt +
            '_media_urlyy' +
            cnt + ' custom_media_urlyy' + cnt + '" name="<?php echo $this->get_field_name('image_uri[]'); ?>">';
        new_row += '<input type="button" value="Upload Logo" id="<?php echo $this->get_field_id('image_uri'); ?>' +
            cnt + '" class="button custom_media_uploadyy' + cnt + '"/>';

     


        jQuery(document).ready(function() {




            function media_uploadabc(button_class) {
                var _custom_media = true,
                    _orig_send_attachment = wp.media.editor.send.attachment;
                jQuery('body').on('click', '.custom_media_uploadyy' + cnt, function(e) {
                    var button_id = '#' + jQuery(this).attr('id');
                    var button_id_s = jQuery(this).attr('id');
                    console.log(button_id);
                    var self = jQuery(button_id);
                    var send_attachment_bkp = wp.media.editor.send.attachment;
                    var button = jQuery(button_id);
                    var id = button.attr('id').replace('_button', '');
                    _custom_media = true;

                    wp.media.editor.send.attachment = function(props, attachment) {
                        if (_custom_media) {

                            //if (attachment.height == 1080 && attachment.width == 1920) {
                            jQuery('.' + button_id_s + '_media_idyy' + cnt).val(attachment.id);
                            jQuery('.' + button_id_s + '_media_urlyy' + cnt).val(attachment
                                .url);
                            jQuery('.' + button_id_s + '_media_imageyy' + cnt).attr('src',
                                attachment.url).css('display', 'block');
                            jQuery('#image_uri_agilysys_resources_video_slider_widget' + cnt)
                                .html("");
                            /*} else {
                                jQuery('#image_uri_agilysys_resources_video_slider_widget' + cnt)
                                    .html("Please Enter the correct Dimensions 1920x1080").css(
                                        'color', 'red');

                            }
                            */
                        } else {
                            return _orig_send_attachment.apply(button_id, [props, attachment]);
                        }
                    }
                    wp.media.editor.open(button);
                    return false;
                });
            }
            media_uploadabc('.custom_media_uploadyy' + cnt);

        });

        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Image Alt', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';

        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('image_uri_alt[]')); ?>" type="text" value="">';
        new_row += '</p>';

        new_row += '</div><br><br>';

        new_row += '<div class="widg-video' + cnt + '" style="display:none;">';

        new_row += '<p>'; new_row +=
        '<label><?php echo __('Video', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>'; new_row +=
        '<video class="<?php echo $this->get_field_id('video_id'); ?>' + cnt + '_media_videov' + cnt +
        ' custom_media_videov' + cnt +
        '" width="320" height="240" controls style="display:none;"><source src="" type="video/mp4"></video>'; new_row +=
        '<input type="hidden" class="<?php echo $this->get_field_id('video_id'); ?>' + cnt + '_media_idv' +
        cnt + ' custom_media_idv' + cnt +
        '" name="<?php echo $this->get_field_name('video_id[]'); ?>"  />'; new_row +=
        '<input type="hidden" class="<?php echo $this->get_field_id('video_id'); ?>' + cnt + '_media_urlv' +
        cnt + ' custom_media_urlv' + cnt +
        '" name="<?php echo $this->get_field_name('video_uri[]'); ?>">'; new_row +=
        '<input type="button" value="Upload Video" class="button custom_media_uploadv' + cnt +
        '" id="<?php echo $this->get_field_id('video_id'); ?>' + cnt + '"/>'; new_row += '</p>'; new_row +=
        '<p>'; new_row += '<label><?php echo __('Title', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>'; new_row +=
        '<input  name="<?php echo $this->get_field_name('leadersDesc2[]'); ?>" type="text" value="" />'; new_row +=
        '</p>'; new_row += '</div>';


        jQuery(document).ready(function() {




            function media_uploadv(button_class) {
                var _custom_media = true,
                    _orig_send_attachment = wp.media.editor.send.attachment;
                jQuery('body').on('click', '.custom_media_uploadv' + cnt, function(e) {
                    var button_id = '#' + jQuery(this).attr('id');
                    var button_id_s = jQuery(this).attr('id');
                    console.log(button_id);
                    var self = jQuery(button_id);
                    var send_attachment_bkp = wp.media.editor.send.attachment;
                    var button = jQuery(button_id);
                    var id = button.attr('id').replace('_button', '');
                    _custom_media = true;

                    wp.media.editor.send.attachment = function(props, attachment) {
                        if (_custom_media) {
                            jQuery('.' + button_id_s + '_media_idv' + cnt).val(attachment.id);
                            jQuery('.' + button_id_s + '_media_urlv' + cnt).val(attachment.url);
                            jQuery('.' + button_id_s + '_media_videov' + cnt).attr('src',
                                attachment.url).css('display', 'block');
                        } else {
                            return _orig_send_attachment.apply(button_id, [props, attachment]);
                        }
                    }
                    wp.media.editor.open(button);
                    return false;
                });
            }
            media_uploadv('.custom_media_uploadv' + cnt);


        });



        new_row += '<div class="widg-youtube' + cnt + '" style="display:none;">'; new_row +=
        '<input type="text"  name="<?php echo $this->get_field_name('youtube_uri[]'); ?>" />'; new_row +=
        '</p>'; new_row += '<p>'; new_row +=
        '<label> <?php echo __('Title', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>'; new_row +=
        '<input  name="<?php echo $this->get_field_name('leadersDesc3[]'); ?>" type="text" />'; new_row +=
        '</p>'; new_row += '</div>';



        var new_cnt = cnt;

        new_row += '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' + new_cnt +
            ');"><?php echo __('Delete Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span></a></p>';
        new_row += '</div></div>';

        jQuery('.add_new_rowxx-input-containers #entries_agilysys_resources_video_slider_widget').append(new_row);

    }


}



function delete_row(cnt) {
    jQuery.each(jQuery(".add_new_rowxx-input-containers #entries_agilysys_resources_video_slider_widget"), function() {
        jQuery(' #entry' + cnt).remove();
    });
    var last_cnt = 0;

    jQuery.each(jQuery("#entries_agilysys_resources_video_slider_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            last_cnt = jQuery(this).val();
        }
    });

    last_cnt--;
    jQuery.each(jQuery("#entries_agilysys_resources_video_slider_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(last_cnt);
        }
    });
    jQuery(".what_makes_rows").val(last_cnt);
    jQuery('.what_makes_rows').trigger('change');

}
</script>
<style>
#rew_container_agilysys_resources_video_slider_widget p {
    padding: 10px !important;
}

.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_resources_video_slider_widget select {
    float: left;
    width: 60%;
    margin-top: 20px !important;
    margin-bottom: 10px !important;
}

#rew_container_agilysys_resources_video_slider_widget input,

textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_resources_video_slider_widget label {
    width: 40%;
    float: left;
}

<?php echo '.' . $widget_add_id_webinars_info;

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}

#entries_agilysys_resources_video_slider_widget #remove-img-bg {
    background: url('<?php echo ZWREW_URL; ?>assets/images/deleteimg.png') center center no-repeat;
    width: 20px;
    height: 22px;
    display: none;
}

#entries_agilysys_resources_video_slider_widget {
    padding: 10px 0 0;
}

#entries_agilysys_resources_video_slider_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_resources_video_slider_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_resources_video_slider_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_resources_video_slider_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_resources_video_slider_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_resources_video_slider_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_resources_video_slider_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_resources_video_slider_widget #entries_agilysys_resources_video_slider_widget plast label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_resources_video_slider_widget">
    <?php echo $rew_html; ?>
</div>

<?php

    }

}
<?php

add_action('widgets_init', 'agil_load_agilysys_news_events_blog_post_archive_widget');

function agil_load_agilysys_news_events_blog_post_archive_widget()
{
    register_widget('agilysys_news_events_blog_post_archive_widget');
}

class agilysys_news_events_blog_post_archive_widget extends WP_Widget
{

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys News and Events Blog Post Archive Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');

    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        ?>



<div class="archiveBox">
    <h2 class="dinProStd greenText">ARCHIVE</h2>
    <ul>
        <div class="show1" id="partial">
            <?php

        wp_get_archives('type=monthly&cat=9&limit=1');

        ?>
        </div>
        <div class="hide" id="archiveBoxList">
            <?php

        wp_get_archives('type=monthly&cat=9');

        ?>
        </div>
    </ul>
    <h3 class="dinProStd greenText" id="seeAllArchive">SEE FULL ARCHIVE</h3>
    <h3 class="dinProStd greenText hide" id="closeArchive">CLOSE ARCHIVE</h3>
</div>


<script>
//news events onclick function
jQuery("#seeAllArchive").click(function() {
    jQuery("#closeArchive").show();
    jQuery("#archiveBoxList").show();
    jQuery("#seeAllArchive").hide();
    jQuery('#partial').hide();
});

jQuery("#closeArchive").click(function() {
    jQuery("#seeAllArchive").show();
    jQuery("#closeArchive").hide();
    jQuery("#archiveBoxList").hide();
    jQuery('#partial').show();
});
</script>
<?php
echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {

    }

    public function form($display_instance)
    {

    }

}
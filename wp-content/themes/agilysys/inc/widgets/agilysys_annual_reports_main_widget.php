<?php

add_action('widgets_init', 'load_agilysys_annual_reports_main_widget');

function load_agilysys_annual_reports_main_widget()
{
    register_widget('agilysys_annual_reports_main_widget');
}

class agilysys_annual_reports_main_widget extends WP_Widget
{
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Annual Reports Main Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {
echo $args['before_widget'];
        $section_title = $instance['section_title'];

        $count = count($instance['title_of_pdf']);

        ?>
<section class="investorInnerSection annual flex">
    <div class="investorInnerMainContent">
        <h2 class="dinProStd blackText h2"><?php echo $section_title; ?></h2>
        <div class="investorInnerContent flex">

            <?php
for ($i = 0; $i < $count; $i++) {

            $title_of_pdf = $instance['title_of_pdf'][$i];
            $pdf_uri = $instance['pdf_uri'][$i];

            ?>
            <div class="investorInnerBox">
                <a target="_blank" href="<?php echo $pdf_uri; ?>"><img class="img-fluid1"
                        src="<?php echo get_template_directory_uri(); ?>/img/agilysys-pdf.png" alt="" />
                    <p class="blackText"><?php echo $title_of_pdf; ?></p>
                </a>
            </div>
            <?php

        }

        ?>

        </div>
    </div>
</section>


<?php
echo $args['after_widget'];
}

    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['section_title'] = strip_tags($new_instance['section_title']);
        $instance['what_makes_rows'] = strip_tags($new_instance['what_makes_rows']);

        $count = count($new_instance['title_of_pdf']);

        for ($i = 0; $i < $count; $i++) {

            $instance['title_of_pdf'][$i] = strip_tags($new_instance['title_of_pdf'][$i]);
            $instance['pdf_uri'][$i] = $new_instance['pdf_uri'][$i];

        }
        return $instance;
    }

    public function form($display_instance)
    {

        $max_entries_slider_image = 15;

        $widget_add_id_slider = $this->get_field_id('') . "add_agilysys_annual_reports_main_widget";
        $section_title = ($display_instance['section_title']);

        if (!empty($display_instance['what_makes_rows'])) {
            $what_makes_rows = ($display_instance['what_makes_rows']);
        } else {
            $what_makes_rows = 0;
        }

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('section_title') . '"> ' . __('Section Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('section_title') . '" name="' . $this->get_field_name('section_title') . '" type="text" value="' . $section_title . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('what_makes_rows') . '"> ' . __('No. of rows do you want to add*', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input class="what_makes_rows" id="' . $this->get_field_name('what_makes_rows') . '" name="' . $this->get_field_name('what_makes_rows') . '" type="number" value="' . $what_makes_rows . '" />';
        $rew_html .= '</p><br>';

        $count = count($display_instance['title_of_pdf']);

        $rew_html .= '<div class="add_new_rowxx-input-containers"><div id="entries_agilysys_annual_reports_main_widget">';

        $rew_html .= '<input class="cnt909" id="cnt909" name="cnt" type="hidden" value="' . $count . '">';

        for ($i = 0; $i < $count; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'AGILYSYS_TEXT_DOMAIN') . ' </span>';

            $rew_html .= '<div class="entry-desc cf">';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('title_of_pdf' . $i) . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('title_of_pdf' . $i) . '" name="' . $this->get_field_name('title_of_pdf[]') . '" type="text" value="' . $display_instance['title_of_pdf'][$i] . '">';
            $rew_html .= '</p><br><br>';

            $rew_html .= '<div class="widg-img' . $i . '">';
            $show1 = (empty($display_instance['pdf_uri'][$i])) ? 'style="display:none;"' : '';
            $rew_html .= '<label class="' . $this->get_field_id('pdf_id' . $i) . '_media_image' . $i . ' custom_media_image' . $i . '" value="' . $display_instance['pdf_uri'][$i] . '" ' . $show1 . ' width=200" height="120">' . $display_instance['pdf_uri'][$i] . ' </label>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('pdf_id' . $i) . '_media_id' . $i . ' custom_media_id' . $i . '" name="' . $this->get_field_name('pdf_id[]') . '" id="' . $this->get_field_id('pdf_id' . $i) . '" value="' . $display_instance['pdf_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('pdf_id' . $i) . '_media_url' . $i . ' custom_media_url' . $i . '" name="' . $this->get_field_name('pdf_uri[]') . '" id="' . $this->get_field_id('pdf_uri' . $i) . '" value="' . $display_instance['pdf_uri'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload PDF/Zip File" class="button custom_media_upload' . $i . '" id="' . $this->get_field_id('pdf_id' . $i) . '"/>';

            $rew_html .= '</div><br><br>';
            ?>

<script>
jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {
                    jQuery('.' + button_id_s + '_media_id<?php echo $i; ?>').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_url<?php echo $i; ?>').val(attachment.url);
                    jQuery('.' + button_id_s + '_media_image<?php echo $i; ?>').attr('src',
                        attachment.url).css('display', 'block');
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload<?php echo $i; ?>');

});
</script>


<?php

            $k = $i + 1;
            $rew_html .= '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' . $k . ');">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';

        }

        $rew_html .= '</div></div>';
        $rew_html .= '<div id="message">' . __('Sorry, you reached to the limit of', 'AGILYSYS_TEXT_DOMAIN') . ' "' . $what_makes_rows . '" ' . __('maximum entries', 'AGILYSYS_TEXT_DOMAIN') . '.</div>';

        $rew_html .= '<div class="' . $widget_add_id_slider . '" style="text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;" onClick="add_new_row();">' . __('ADD ROW', 'AGILYSYS_TEXT_DOMAIN') . '</div>';
        ?>

<script>
function add_new_row() {
    var cnt = '';

    jQuery.each(jQuery("#entries_agilysys_annual_reports_main_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            cnt = jQuery(this).val();
        }
    });




    var what_makes_rows = jQuery('.what_makes_rows').val();


 console.log(cnt);  
 if (parseInt(cnt) < parseInt(what_makes_rows)) {

        cnt++;

        jQuery.each(jQuery("#entries_agilysys_annual_reports_main_widget .cnt909"), function() {
            if (jQuery(this).val() != '') {
                jQuery(this).val(cnt);
            }
        });

        var new_row = '<div id="entry' + cnt +
            '"  class="entrys"><span class="entry-title" onclick = "slider(this);"><?php echo __('Add New Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span>';
        new_row += '<div class="entry-desc cf">';



        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Title', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('title_of_pdf[]')); ?>" type="text" value="">';
        new_row += '</p><br><br>';





        new_row += '<div class="widg-img' + cnt + '">';
        new_row += '<label class="<?php echo $this->get_field_id('pdf_id'); ?>' + cnt + '_media_image' + cnt +
            ' custom_media_image' + cnt + '" value="" width=200" height="120"></label>';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('pdf_id'); ?>'+cnt+'_media_url' +
            cnt + ' custom_media_url' + cnt +
            '" name="<?php echo $this->get_field_name('pdf_uri[]'); ?>" id="<?php echo $this->get_field_id('pdf_uri'); ?>' +
            cnt + '" value="">';
        new_row += '<input type="button" value="Upload PDF/Zip File" class="button custom_media_upload' + cnt +
            '" id="<?php echo $this->get_field_id('pdf_id'); ?>' + cnt + '"/>';
        new_row += '</div><br><br>';


        jQuery(document).ready(function() {




            function media_upload(button_class) {
                var _custom_media = true,
                    _orig_send_attachment = wp.media.editor.send.attachment;
                jQuery('body').on('click', '.custom_media_upload' + cnt, function(e) {
                    var button_id = '#' + jQuery(this).attr('id');
                    var button_id_s = jQuery(this).attr('id');
                    console.log(button_id);
                    var self = jQuery(button_id);
                    var send_attachment_bkp = wp.media.editor.send.attachment;
                    var button = jQuery(button_id);
                    var id = button.attr('id').replace('_button', '');
                    _custom_media = true;

                    wp.media.editor.send.attachment = function(props, attachment) {
                        if (_custom_media) {
                            jQuery('.' + button_id_s + '_media_id' + cnt).val(attachment.id);


                            jQuery('.' + button_id_s + '_media_url' + cnt).val(attachment.url);
                            jQuery('.' + button_id_s + '_media_image' + cnt).html(attachment.url).css('display', 'block');
                        } else {
                            return _orig_send_attachment.apply(button_id, [props, attachment]);
                        }
                    }
                    wp.media.editor.open(button);
                    return false;
                });
            }
            media_upload('.custom_media_upload' + cnt);

        });



        var new_cnt = cnt;

        new_row +=
            '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' + new_cnt +
            ');"><?php echo __('Delete Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span></a></p>';
        new_row += '</div></div>';

        jQuery('.add_new_rowxx-input-containers #entries_agilysys_annual_reports_main_widget').append(new_row);

    }


}



function delete_row(cnt) {
    jQuery.each(jQuery(".add_new_rowxx-input-containers #entries_agilysys_annual_reports_main_widget"), function() {
        jQuery(' #entry' + cnt).remove();
    });
    var last_cnt = 0;

    jQuery.each(jQuery("#entries_agilysys_annual_reports_main_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            last_cnt = jQuery(this).val();
        }
    });

    last_cnt--;
    jQuery.each(jQuery("#entries_agilysys_annual_reports_main_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(last_cnt);
        }
    });
    jQuery(".what_makes_rows").val(last_cnt);
    jQuery('.what_makes_rows').trigger('change');

}
</script>
<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_annual_reports_main_widget select {
    float: left;
    width: 60%;
    margin-top: 20px !important;
    margin-bottom: 10px !important;
}

#rew_container_agilysys_annual_reports_main_widget input,

textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_annual_reports_main_widget label {
    width: 40%;
    float: left;
}


<?php echo '.'. $widget_add_id_slider;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}



#entries_agilysys_annual_reports_main_widget {
    padding: 10px 0 0;
}

#entries_agilysys_annual_reports_main_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_annual_reports_main_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_annual_reports_main_widget .delete-row {
    margin-top: 40px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_annual_reports_main_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_annual_reports_main_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_annual_reports_main_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_annual_reports_main_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_annual_reports_main_widget #entries_agilysys_annual_reports_main_widget p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}


#rew_container_agilysys_annual_reports_main_widget p {
    padding: 20px !important;
}
</style>
<div id="rew_container_agilysys_annual_reports_main_widget">
    <?php echo $rew_html; ?>
</div>

<?php
}

}
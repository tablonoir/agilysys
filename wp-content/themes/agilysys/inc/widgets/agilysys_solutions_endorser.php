<?php

function agilysys_solutions_endorsers()
{
    register_widget('agilysys_solutions_endorser');
}

add_action('widgets_init', 'agilysys_solutions_endorsers');

class agilysys_solutions_endorser extends WP_Widget
{
    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys solutions endorser', 'agilysys_text_domain'));
        add_action('load-widgets.php', array(&$this, 'agilysys_color_picker_load'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');

    }

    public function agilysys_color_picker_load()
    {
        wp_enqueue_style('wp-color-picker');
        wp_enqueue_script('wp-color-picker');
    }

    /**
     * @see WP_Widget::widget -- do not rename this         * This is for front end
     */
    public function widget($args, $instance)
    {
        extract($args);
        
        echo $args['before_widget'];

        $bg_color = !empty($instance['background_color']) ? $instance['background_color'] : '';
        $agilysys_solutions_endorser_content = (isset($instance['agilysys_solutions_endorser_content'])) ? $instance['agilysys_solutions_endorser_content'] : "";
        $agilysys_solutions_endorser_name = (isset($instance['agilysys_solutions_endorser_name'])) ? $instance['agilysys_solutions_endorser_name'] : "";

        ?>

<section>
    <div class="d-block-quote ">
        <div class="solutionTestimonialDoubleQuote top-left">
            <img src="<?php echo get_template_directory_uri(); ?>/img/double-quote-top-grey.png" class="" alt="">
        </div>
        <div class="d-flex-quote pad-tb-80">
            <div class="solutionEndorsersFeedbackTitle">
                <h3 class=" dinProStd blackText">
                    <?php echo substr($agilysys_solutions_endorser_content,0,375); ?>
                </h3>
            </div>
            <div class="solutionEndorsersFeedbackEndorser">
                <p class="waterText" style="color:#49aa42">-- <?php echo $agilysys_solutions_endorser_name; ?></p>
            </div>
        </div>

        <div class="solutionTestimonialDoubleQuoteBotom bottom-right">
            <img src="<?php echo get_template_directory_uri(); ?>/img/double-quote-bottom-grey.png" class="" alt="">
        </div>
    </div>
</section>

<?php
                
                echo $args['after_widget'];
}
    //Function widget ends here
    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['agilysys_solutions_endorser_content'] = strip_tags($new_instance['agilysys_solutions_endorser_content']);
        $instance['agilysys_solutions_endorser_name'] = strip_tags($new_instance['agilysys_solutions_endorser_name']);
        $instance['background_color'] = $new_instance['background_color'];
        return $instance;
    }
    //Function update ends here

    /**
     * @see WP_Widget::form -- do not rename this
     */
    public function form($display_instance)
    {

        ?>
<script type='text/javascript'>
jQuery(document).ready(function($) {
    $('.my-color-picker').wpColorPicker();
});
</script>


<?php
$agilysys_solutions_endorser_content = ($display_instance['agilysys_solutions_endorser_content']);
        $agilysys_solutions_endorser_name = ($display_instance['agilysys_solutions_endorser_name']);
        $background_color = ($display_instance['background_color']);

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('agilysys_solutions_endorser_content') . '"> ' . __('Description', 'agilysys_text_domain') . ' :</label>';
        $rew_html .= '<textarea id="' . $this->get_field_id('agilysys_solutions_endorser_content') . '" name="' . $this->get_field_name('agilysys_solutions_endorser_content') . '" rows="3" cols="60" >' . $agilysys_solutions_endorser_content . '</textarea>';
        $rew_html .= '</p><br><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('agilysys_solutions_endorser_name') . '"> ' . __('Endorser Name', 'agilysys_text_domain') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('agilysys_solutions_endorser_name') . '" name="' . $this->get_field_name('agilysys_solutions_endorser_name') . '" type="text" value="' . $agilysys_solutions_endorser_name . '" />';
        $rew_html .= '</p>';
        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('background_color') . '"> ' . __('Background Color', 'agilysys_text_domain') . ' :</label>';
        $rew_html .= '<input  class="my-color-picker" id="' . $this->get_field_id('background_color') . '" name="' . $this->get_field_name('background_color') . '" type="text" value="' . $background_color . '" />';
        $rew_html .= '</p>';
      
        ?>

<style>
#rew_container_agilysys_solutions_endorser p {
    padding: 10px;
}

.wp-picker-container {
    margin-left: 32.75%;
}

.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_solutions_endorser input,
select,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_solutions_endorser label {
    width: 40%;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}

#entries_agilysys_solutions_endorser {
    padding: 10px 0 0;
}

#entries_agilysys_solutions_endorser .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_solutions_endorser .entrys:first-child {
    margin: 0;
}

#entries_agilysys_solutions_endorser .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_solutions_endorser .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_solutions_endorser .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_solutions_endorser .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_solutions_endorser .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_solutions_endorser #entries_agilysys_solutions_endorser plast label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_solutions_endorser">
    <?php echo $rew_html; ?>
</div>
<?php
} //Function form ends here
} // class ends here
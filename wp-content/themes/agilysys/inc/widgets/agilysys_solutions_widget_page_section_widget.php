<?php

add_action('widgets_init', 'load_agilysys_solutions_widget_page_section_widget');

function load_agilysys_solutions_widget_page_section_widget()
{
    register_widget('agilysys_solutions_widget_page_section_widget');
}

class agilysys_solutions_widget_page_section_widget extends WP_Widget
{

    /**
     * constructor -- name this the same as the class above
     */

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Solutions Widget Page Section Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');

    }

    public function clean($string)
    {
        $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public function widget($args, $instance)
    {



       


        echo $args['before_widget'];
        $type_arr = array();

        foreach ($instance['type'] as $key) {

            array_push($type_arr, $key);

        }

        $arr = array();

        for ($i = 0; $i < 150; $i++) {
            $block = $instance['block-' . $i];

            if (isset($block) && $block != "") {

                $type = strip_tags($instance['industry_type' . $i]);

                $section_title = strip_tags($instance['section_title' . $i]);
                $image_uri = $instance['image_uri' . $i];

                $arr1 = array();

                $arr1['section_title'] = $section_title;
                $arr1['image_uri'] = $image_uri;

                if (!empty($type)) {
                    $arr[$this->clean($type)][] = $arr1;
                }
            }
        }
        
     

        ?>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
jQuery(document).ready(function() {
    jQuery('#someTab').tab('show');
    jQuery('.solution_tab .nav-item').on('click', function() {
        jQuery(this).addClass('actives').siblings().removeClass('actives');
    });


    jQuery('.Checkin-links-btn').on('click', function() {
        jQuery(this).addClass('Checkin-links-btn-green').removeClass('Checkin-links-btn-green');
    });

    jQuery(".nav-item").click(function() {


        jQuery(".Checkin-links").slideDown('slow');
    });

});
</script>

<div class="d-flex solu-tab dinProStd box-shad w-100">
    <ul class="nav nav-tabs solution_tab w-100" id="myTab" role="tablist">

        <?php
$cnt = 0;
        foreach ($instance['type'] as $k => $v) {

            ?>

        <style>


@media (min-width: 768px) and (max-width: 1024px) and (orientation: landscape) {
    .solution_tab <?php echo '#nav-item'. $k;
        ?>:hover,
        .solution_tab <?php echo '#nav-item'. $k;?>.actives{
        /*background-size: 30px !important;*/
    }

}

@media (min-width: 481px) and (max-width: 767px) {
    .solution_tab <?php echo '#nav-item'. $k;
        ?>:hover,
        .solution_tab <?php echo '#nav-item'. $k;?>.actives{
        /*background-size: 70px !important;*/
    }

}

@media (min-width: 320px) and (max-width: 480px) {
    .solution_tab <?php echo '#nav-item'. $k;
        ?>:hover,
        .solution_tab <?php echo '#nav-item'. $k;?>.actives{
        /*background-size: 70px !important;*/
    }

} 

        .solution_tab <?php echo '#nav-item'. $k;

        ?> {
            background-image: url(<?php echo str_replace(' ', '', $instance['heading_image_uri'][$k]);
            ?>);
            background-position: 50% 25%;
            background-size: 80px;
            background-repeat: no-repeat;
        }

        .solution_tab <?php echo '#nav-item'. $k;
        ?>:hover,
        .solution_tab <?php echo '#nav-item'. $k;

        ?>.actives {
            background-image: url(<?php echo str_replace(' ', '', $instance['heading_image_uri_hover'][$k]);
            ?>);
            background-position: 50% 25%;
            background-size: 80px;
            background-repeat: no-repeat;
        }
        </style>
        <script>
        jQuery(document).ready(function() {

            jQuery("#nav-item<?php echo $k; ?>").click(function() {

                jQuery(".tab-pane").hide();

                jQuery("#tab_view_<?php echo $k; ?>").show();
            });

        });
        </script>
        <li class="nav-item  bor-right <?php if ($cnt == 0) {echo 'actives';};?>" id="nav-item<?php echo $k; ?>">
            <a class="nav-link <?php if ($cnt == 0) {echo 'active';};?>" id="<?php echo $v; ?>" data-toggle="tab"
                href="#<?php echo $v; ?>" role="tab" aria-controls="<?php echo $v; ?>"
                aria-selected="true"><?php echo $v; ?></a>
        </li>








        <?php
$cnt++;
        }

        ?>


    </ul>
</div>


<div class="tab-content p-70">


    <?php
$cnt = 0;
        foreach ($instance['type'] as $k => $v) {

            ?>

    <div class="tab-pane <?php if ($cnt == 0) {echo 'active';}?>" id="tab_view_<?php echo $k; ?>" role="tabpanel"
        aria-labelledby="<?php echo $v; ?>">
        <div class="d-flex w-100 Checkin-links ">
            <div class="d-flex w-100 justify-content-center">
                <ul class="d-flex w-100 blackText justify-content-start grid-wrapper">

                    <?php
$cnt = 0;
            foreach ($arr[$this->clean($v)] as $key) {

                ?>


                    <style>
                    <?php echo '.Checkin-links-btn'. $cnt;

                    ?> {
                     
                        background-image: url(<?php echo $key['image_uri']; ?>);
                        background-repeat: no-repeat;
                        background-position: 10% 50%;
                        background-size: 48px;
                      
                    }

                    <?php echo '.Checkin-links-btn-green'. $cnt;
                    ?>,
                    <?php echo '.Checkin-links-btn'. $cnt;

                    ?>:hover {
                       
                        background-image: url(<?php echo $key['image_uri']; ?>);
                        background-repeat: no-repeat;
                        background-position: 10% 50%;
                        background-size: 48px;
                    }
                    </style>
                    <li class="grid-item">
                        <span class="tabgenerealButton Checkin-links-btn<?php echo $cnt; ?> Checkin-links-btn-green<?php echo $cnt; ?>">
                            <?php echo $key['section_title']; ?>
                        </span>
                    </li>




                    <?php

                $cnt++;

            }

            ?>
                </ul>
            </div>
        </div>
    </div>



    <?php

        }

        ?>
</div>





<?php
echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['what_makes_rows'] = strip_tags($new_instance['what_makes_rows']);

        $instance['cnt'] = $new_instance['cnt'];

        for ($k = 0; $k < count($new_instance['type']); $k++) {

           
            $instance['type'][$k] = $new_instance['type'][$k];
            $instance['heading_image_uri'][$k] = $new_instance['heading_image_uri'][$k];
            $instance['heading_image_uri_hover'][$k] = $new_instance['heading_image_uri_hover'][$k];
            
        }

        for ($i = 0; $i < 150; $i++) {
            $block = $new_instance['block-' . $i];

            $instance['block-' . $i] = $new_instance['block-' . $i];

            $block = $new_instance['block-' . $i];

            if (isset($block) && $block != "") {

                $instance['industry_type' . $i] = strip_tags($new_instance['industry_type' . $i]);
                $instance['section_title' . $i] = strip_tags($new_instance['section_title' . $i]);
                $instance['image_uri' . $i] = strip_tags($new_instance['image_uri' . $i]);

            }

        }
        return $instance;
    }

    public function form($display_instance)
    {
        $max_entries_agilysys_solutions_widget_page_section_widget_slider_image = 150;

        $widget_add_id_slider = $this->get_field_id('') . "add_agilysys_solutions_widget_page_section_widget";

        $what_makes_rows = ($display_instance['what_makes_rows']);

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('what_makes_rows') . '"> ' . __('No. of rows do you want to add*', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input class="what_makes_rows" id="' . $this->get_field_name('what_makes_rows') . '" name="' . $this->get_field_name('what_makes_rows') . '" type="number" value="' . $what_makes_rows . '" />';
        $rew_html .= '</p><br>';

        if (isset($display_instance['cnt'])) {
            $cnt = $display_instance['cnt'];
        } else {
            $cnt = 0;
        }

        $type_arr = array();

        foreach ($display_instance['type'] as $key) {

            array_push($type_arr, $key);

        }

        $rew_html .= '<div id="div_type">';
        $rew_html .= '<input class="cnt" id="' . $this->get_field_id('cnt') . '" name="' . $this->get_field_name('cnt') . '" type="hidden" value="' . $cnt . '">';

        ?>
<script>
function add_type() {

    var cnt = jQuery('.cnt').val();
    cnt++;
    jQuery('.cnt').val(cnt);
    
      console.log(cnt);

    var html = '';
    html += '<div id="row_' + cnt + '"><br><br><p>';
    html +=
        '<label style="float:left;" for="<?php echo $this->get_field_id('type'); ?>"><?php echo __('Add Heading', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
    html +=
        '<input style="width:520px;float:left;" id="<?php echo $this->get_field_id('type'); ?>" name="<?php echo $this->get_field_name('type[]'); ?>" type="text"  />';

    html += '</p><br>';



    html += '<div class="widg-img' + cnt + '">';
    html += '<img class="<?php echo $this->get_field_id('heading_image_id' . $cnt); ?>_media_imagexx' + cnt +
        ' custom_media_imagexx' + cnt + '" src="" style="display:none;" width=200" height="120"/>';
    html += '<input type="hidden" class="<?php echo $this->get_field_id('heading_image_id'); ?>' + cnt +
        ' _media_idxx' + cnt + ' custom_media_idxx' + cnt +
        '" name="<?php echo $this->get_field_name('heading_image_id[]'); ?>" id="<?php echo $this->get_field_id('heading_image_id'); ?>' +
        cnt + '" value="" />';
    html += '<input type="hidden" class="<?php echo $this->get_field_id('heading_image_id'); ?>' + cnt +
        ' _media_urlxx' + cnt + ' custom_media_urlxx' + cnt +
        '" name="<?php echo $this->get_field_name('heading_image_uri[]'); ?>" id="<?php echo $this->get_field_id('heading_image_uri'); ?>' +
        cnt + '" value="">';
    html += '<input type="button" value="Upload Image" class="button custom_media_uploadxx' + cnt +
        '" id="<?php echo $this->get_field_id('heading_image_id'); ?>' + cnt + '"/>';
    html += '</div><br>';


    html += '<div class="widg-img' + cnt + '">';
    html += '<img class="<?php echo $this->get_field_id('heading_image_id_hover' . $cnt); ?>_media_imageyy' + cnt +
        ' custom_media_imageyy' + cnt + '" src="" style="display:none;" width=200" height="120"/>';
    html += '<input type="hidden" class="<?php echo $this->get_field_id('heading_image_id_hover'); ?>' + cnt +
        ' _media_idyy' + cnt + ' custom_media_idyy' + cnt +
        '" name="<?php echo $this->get_field_name('heading_image_id_hover[]'); ?>" id="<?php echo $this->get_field_id('heading_image_id_hover'); ?>' +
        cnt + '" value="" />';
    html += '<input type="hidden" class="<?php echo $this->get_field_id('heading_image_id_hover'); ?>' + cnt +
        ' _media_urlyy' + cnt + ' custom_media_urlyy' + cnt +
        '" name="<?php echo $this->get_field_name('heading_image_uri_hover[]'); ?>" id="<?php echo $this->get_field_id('heading_image_uri_hover'); ?>' +
        cnt + '" value="">';
    html += '<input type="button" value="Upload Hover Image" class="button custom_media_uploadyy' + cnt +
        '" id="<?php echo $this->get_field_id('heading_image_id_hover'); ?>' + cnt + '"/>';

    html += '<input type="button" style="width:80px;float:left;" value="Remove" onclick="remove_type(' + cnt + ')">';
    html += '</div><br>';


    jQuery('#div_type').append(html);

    jQuery(document).ready(function() {




        function media_upload(button_class) {
            var _custom_media = true,
                _orig_send_attachment = wp.media.editor.send.attachment;
            jQuery('body').on('click', '.custom_media_uploadxx' + cnt, function(e) {
                var button_id = '#' + jQuery(this).attr('id');
                var button_id_s = jQuery(this).attr('id');
                console.log(button_id);
                var self = jQuery(button_id);
                var send_attachment_bkp = wp.media.editor.send.attachment;
                var button = jQuery(button_id);
                var id = button.attr('id').replace('_button', '');
                _custom_media = true;

                wp.media.editor.send.attachment = function(props, attachment) {
                    if (_custom_media) {
                        jQuery('.' + button_id_s + '_media_idxx' + cnt).val(attachment.id);
                        jQuery('.' + button_id_s + '_media_urlxx' + cnt).val(attachment
                            .url);
                        jQuery('.' + button_id_s + '_media_imagexx' + cnt).attr('src',
                            attachment.url).css('display', 'block');
                    } else {
                        return _orig_send_attachment.apply(button_id, [props, attachment]);
                    }
                }
                wp.media.editor.open(button);
                return false;
            });
        }
        media_upload('.custom_media_uploadxx' + cnt);

    });




    jQuery(document).ready(function() {




        function media_upload(button_class) {
            var _custom_media = true,
                _orig_send_attachment = wp.media.editor.send.attachment;
            jQuery('body').on('click', '.custom_media_uploadyy' + cnt, function(e) {
                var button_id = '#' + jQuery(this).attr('id');
                var button_id_s = jQuery(this).attr('id');
                console.log(button_id);
                var self = jQuery(button_id);
                var send_attachment_bkp = wp.media.editor.send.attachment;
                var button = jQuery(button_id);
                var id = button.attr('id').replace('_button', '');
                _custom_media = true;

                wp.media.editor.send.attachment = function(props, attachment) {
                    if (_custom_media) {
                        jQuery('.' + button_id_s + '_media_idyy' + cnt).val(attachment.id);
                        jQuery('.' + button_id_s + '_media_urlyy' + cnt).val(attachment
                            .url);
                        jQuery('.' + button_id_s + '_media_imageyy' + cnt).attr('src',
                            attachment.url).css('display', 'block');
                    } else {
                        return _orig_send_attachment.apply(button_id, [props, attachment]);
                    }
                }
                wp.media.editor.open(button);
                return false;
            });
        }
        media_upload('.custom_media_uploadyy' + cnt);

    });

}

function remove_type(cnt) {

    jQuery('#row_' + cnt).remove();

}
</script>

<?php

        if (count($display_instance['type']) > 0) {

            $cnt = 0;
            foreach ($display_instance['type'] as $key) {

                $type = $key;
                $rew_html .= '<div id="row_' . $cnt . '"><br>';
                $rew_html .= '<p><label style="float:left;" for="' . $this->get_field_id('type') . '"> ' . __('Add Heading Type', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
                $rew_html .= '<input style="width:520px;float:left;" id="' . $this->get_field_id('type') . '" name="' . $this->get_field_name('type[]') . '" type="text" value="' . $type . '" /></p><br><br>';

                $rew_html .= '<p><div class="widg-img' . $cnt . '">';
                $show1 = (empty($display_instance['heading_image_uri'][$cnt])) ? 'style="display:none;"' : '';
                $rew_html .= '<img class="' . $this->get_field_id('heading_image_id' . $cnt) . '_media_imagexx' . $cnt . ' custom_media_imagexx' . $cnt . '" src="' . $display_instance['heading_image_uri'][$cnt] . '" ' . $show1 . ' width=200" height="120"/>';
                $rew_html .= '<input type="hidden" class="' . $this->get_field_id('heading_image_id' . $cnt) . '_media_idxx' . $i . ' custom_media_idxx' . $cnt . '" name="' . $this->get_field_name('heading_image_id[]') . '" id="' . $this->get_field_id('heading_image_id' . $cnt) . '" value="' . $display_instance['heading_image_id'][$cnt] . '" />';
                $rew_html .= '<input type="hidden" class="' . $this->get_field_id('heading_image_id' . $cnt) . '_media_urlxx' . $cnt . ' custom_media_urlxx' . $cnt . '" name="' . $this->get_field_name('heading_image_uri[]') . '" id="' . $this->get_field_id('heading_image_uri' . $cnt) . '" value="' . $display_instance['heading_image_uri'][ $cnt] . '">';
                $rew_html .= '<input type="button" value="Upload Image(200x200)" class="button custom_media_uploadxx' . $cnt . '" id="' . $this->get_field_id('heading_image_id' . $cnt) . '"/>';

                $rew_html .= '</div></p><br>';

                ?>
<script>
jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_uploadxx<?php echo $cnt; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {
                    jQuery('.' + button_id_s + '_media_idxx<?php echo $cnt; ?>').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_urlxx<?php echo $cnt; ?>').val(attachment
                        .url);
                    jQuery('.' + button_id_s + '_media_imagexx<?php echo $cnt; ?>').attr('src',
                        attachment.url).css('display', 'block');
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_uploadxx<?php echo $cnt; ?>');

});


jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_uploadyy<?php echo $cnt; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {
                    jQuery('.' + button_id_s + '_media_idyy<?php echo $cnt; ?>').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_urlyy<?php echo $cnt; ?>').val(attachment
                        .url);
                    jQuery('.' + button_id_s + '_media_imageyy<?php echo $cnt; ?>').attr('src',
                        attachment.url).css('display', 'block');
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_uploadyy<?php echo $cnt; ?>');

});
</script>



<?php

                $rew_html .= '<p><div class="widg-img' . $cnt . '">';
                $show1 = (empty($display_instance['heading_image_uri_hover'][$cnt])) ? 'style="display:none;"' : '';
                $rew_html .= '<img class="' . $this->get_field_id('heading_image_id_hover' . $cnt) . '_media_imageyy' . $cnt . ' custom_media_imageyy' . $cnt . '" src="' . $display_instance['heading_image_uri_hover'][$cnt] . '" ' . $show1 . ' width=200" height="120"/>';
                $rew_html .= '<input type="hidden" class="' . $this->get_field_id('heading_image_id_hover' . $cnt) . '_media_idyy' . $i . ' custom_media_idyy' . $cnt . '" name="' . $this->get_field_name('heading_image_id_hover[]') . '" id="' . $this->get_field_id('heading_image_id_hover' . $cnt) . '" value="' . $display_instance['heading_image_id_hover'][$cnt] . '" />';
                $rew_html .= '<input type="hidden" class="' . $this->get_field_id('heading_image_id_hover' . $cnt) . '_media_urlyy' . $cnt . ' custom_media_urlyy' . $cnt . '" name="' . $this->get_field_name('heading_image_uri_hover[]') . '" id="' . $this->get_field_id('heading_image_uri_hover' . $cnt) . '" value="' . $display_instance['heading_image_uri_hover'][$cnt] . '">';
                $rew_html .= '<input type="button" value="Upload Hover Image(200x200)" class="button custom_media_uploadyy' . $cnt . '" id="' . $this->get_field_id('heading_image_id_hover' . $cnt) . '"/>';

                $rew_html .= '</div></p><br>';

                if ($cnt == 0) {
                    $rew_html .= '<input type="button" style="width:80px;float:left;" value="Add" onclick="add_type()">';
                } else {
                    $rew_html .= '<input type="button" style="width:80px;float:left;" value="Remove" onclick="remove_type(' . $cnt . ')">';
                }

                $rew_html .= '<br><br></div>';
                $cnt++;
            }

        } else {
            $rew_html .= '<div id="row_1"><br><p>';
            $rew_html .= '<label style="float:left;" for="' . $this->get_field_id('type') . '"> ' . __('Add Heading Type', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input style="width:520px;float:left;" id="' . $this->get_field_id('type') . '" name="' . $this->get_field_name('type[]') . '" type="text" value="' . $type . '" />';
            $rew_html .= '<input type="button" style="width:80px;float:left;" value="Add" onclick="add_type()">';
            $rew_html .= '<br></p><br>';

            $cnt = 1;

            $rew_html .= '<p><div class="widg-img' . $cnt . '">';
            $show1 = (empty($display_instance['heading_image_uri'][$cnt])) ? 'style="display:none;"' : '';
            $rew_html .= '<img class="' . $this->get_field_id('heading_image_id' . $cnt) . '_media_imagexx' . $cnt . ' custom_media_imagexx' . $cnt . '" src="' . $display_instance['heading_image_uri'][$cnt] . '" ' . $show1 . ' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('heading_image_id' . $cnt) . '_media_idxx' . $i . ' custom_media_idxx' . $cnt . '" name="' . $this->get_field_name('heading_image_id[]') . '" id="' . $this->get_field_id('heading_image_id' . $cnt) . '" value="' . $display_instance['heading_image_id'][$cnt] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('heading_image_id' . $cnt) . '_media_urlxx' . $cnt . ' custom_media_urlxx' . $cnt . '" name="' . $this->get_field_name('heading_image_uri[]') . '" id="' . $this->get_field_id('heading_image_uri' . $cnt) . '" value="' . $display_instance['heading_image_uri'][$cnt] . '">';
            $rew_html .= '<input type="button" value="Upload Image(110x100)" class="button custom_media_uploadxx' . $cnt . '" id="' . $this->get_field_id('heading_image_id' . $cnt) . '"/>';

            $rew_html .= '<br></div>';

            $rew_html .= '<p><div class="widg-img' . $cnt . '">';
            $show1 = (empty($display_instance['heading_image_uri_hover'][$cnt])) ? 'style="display:none;"' : '';
            $rew_html .= '<img class="' . $this->get_field_id('heading_image_id_hover' . $cnt) . '_media_imageyy' . $cnt . ' custom_media_imageyy' . $cnt . '" src="' . $display_instance['heading_image_uri_hover'][$cnt] . '" ' . $show1 . ' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('heading_image_id_hover' . $cnt) . '_media_idyy' . $i . ' custom_media_idyy' . $cnt . '" name="' . $this->get_field_name('heading_image_id_hover[]') . '" id="' . $this->get_field_id('heading_image_id_hover' . $cnt) . '" value="' . $display_instance['heading_image_id_hover'][$cnt] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('heading_image_id_hover' . $cnt) . '_media_urlyy' . $cnt . ' custom_media_urlyy' . $cnt . '" name="' . $this->get_field_name('heading_image_uri_hover[]') . '" id="' . $this->get_field_id('heading_image_uri_hover' . $cnt) . '" value="' . $display_instance['heading_image_uri_hover'][$cnt] . '">';
            $rew_html .= '<input type="button" value="Upload Hover Image(200x200)" class="button custom_media_uploadyy' . $cnt . '" id="' . $this->get_field_id('heading_image_id_hover' . $cnt) . '"/>';

            $rew_html .= '</div></p><br>';

            ?>
<script>
jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_uploadxx<?php echo $cnt; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {
                    jQuery('.' + button_id_s + '_media_idxx<?php echo $cnt; ?>').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_urlxx<?php echo $cnt; ?>').val(attachment
                        .url);
                    jQuery('.' + button_id_s + '_media_imagexx<?php echo $cnt; ?>').attr('src',
                        attachment.url).css('display', 'block');
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_uploadxx<?php echo $cnt; ?>');

});


jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_uploadyy<?php echo $cnt; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {
                    jQuery('.' + button_id_s + '_media_idyy<?php echo $cnt; ?>').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_urlyy<?php echo $cnt; ?>').val(attachment
                        .url);
                    jQuery('.' + button_id_s + '_media_imageyy<?php echo $cnt; ?>').attr('src',
                        attachment.url).css('display', 'block');
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_uploadyy<?php echo $cnt; ?>');

});
</script>



<?php

        }

        $rew_html .= '</div></div><br>';

        $rew_html .= '<div class="' . $widget_add_id_slider . '-input-containers"><div id="entries_agilysys_solutions_widget_page_section_widget">';
        for ($i = 0; $i < $max_entries_agilysys_solutions_widget_page_section_widget_slider_image; $i++) {

            $display = (!isset($display_instance['block-' . $i]) || ($display_instance['block-' . $i] == "")) ? 'style="display:none;"' : '';

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'AGILYSYS_TEXT_DOMAIN') . ' </span>';

            $rew_html .= '<div class="entry-desc cf">';
            $rew_html .= '<input id="' . $this->get_field_id('block-' . $i) . '" name="' . $this->get_field_name('block-' . $i) . '" type="hidden" value="' . $display_instance['block-' . $i] . '">';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('section_title' . $i) . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('section_title' . $i) . '" name="' . $this->get_field_name('section_title' . $i) . '" type="text" value="' . $display_instance['section_title' . $i] . '" />';
            $rew_html .= '</p><br>';

            $rew_html .= '<p><div class="widg-img' . $i . '">';
            $show1 = (empty($display_instance['image_uri' . $i])) ? 'style="display:none;"' : '';
            $rew_html .= '<img class="' . $this->get_field_id('image_id' . $i) . '_media_image' . $i . ' custom_media_image' . $i . '" src="' . $display_instance['image_uri' . $i] . '" ' . $show1 . ' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id' . $i) . '_media_id' . $i . ' custom_media_id' . $i . '" name="' . $this->get_field_name('image_id' . $i) . '" id="' . $this->get_field_id('image_id' . $i) . '" value="' . $display_instance['image_id' . $i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id' . $i) . '_media_url' . $i . ' custom_media_url' . $i . '" name="' . $this->get_field_name('image_uri' . $i) . '" id="' . $this->get_field_id('image_uri' . $i) . '" value="' . $display_instance['image_uri' . $i] . '">';
            $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_upload' . $i . '" id="' . $this->get_field_id('image_id' . $i) . '"/>';

            $rew_html .= '</div></p><br>';

            ?>







<script>
jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {
                    jQuery('.' + button_id_s + '_media_id<?php echo $i; ?>').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_url<?php echo $i; ?>').val(attachment.url);
                    jQuery('.' + button_id_s + '_media_image<?php echo $i; ?>').attr('src',
                        attachment.url).css('display', 'block');
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload<?php echo $i; ?>');

});
</script>







<?php
$rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('industry_type' . $i) . '"> ' . __('Industry Type', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('industry_type' . $i) . '" name="' . $this->get_field_name('industry_type' . $i) . '">';

            $selected_industry = $display_instance['industry_type' . $i];
            $rew_html .= '<option value="">Please Select</option>';
            foreach ($type_arr as $key) {

                if ($this->clean($key) == $selected_industry) {
                    $rew_html .= '<option value="' . $this->clean($key) . '" selected="selected">' . $key . '</option>';
                } else {
                    $rew_html .= '<option value="' . $this->clean($key) . '">' . $key . '</option>';
                }
            }

            $rew_html .= '</select>';
            $rew_html .= '</p><br>';

            $rew_html .= '<p><a href="#delete"><span class="delete-row">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';

        }
        $rew_html .= '</div></div>';
        $rew_html .= '<div id="message">' . __('Sorry, you reached to the limit of', 'AGILYSYS_TEXT_DOMAIN') . ' "' . $what_makes_rows . '" ' . __('maximum entries_agilysys_solutions_widget_page_section_widget', 'AGILYSYS_TEXT_DOMAIN') . '.</div>';
        if (count($display_instance['type']) > 0) {
            $rew_html .= '<div class="' . $widget_add_id_slider . '" style="text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;">' . __('ADD ROW', 'AGILYSYS_TEXT_DOMAIN') . '</div>';
        }

        ?>


<script>
var last_cnt = 0;
jQuery(document).ready(function(e) {


    jQuery.each(jQuery(".<?php echo $widget_add_id_slider; ?>-input-containers #entries_agilysys_solutions_widget_page_section_widget").children(),
        function() {
            if (jQuery(this).find('input').val() != '') {
                jQuery(this).show();
                last_cnt++;
            }
        });
    jQuery(".<?php echo $widget_add_id_slider; ?>").bind('click', function(e) {


        var rows = 0;
        jQuery.each(jQuery(".<?php echo $widget_add_id_slider; ?>-input-containers #entries_agilysys_solutions_widget_page_section_widget")
            .children(),
            function() {

                if (last_cnt + 1 <= jQuery('.what_makes_rows').val()) {
                    if (jQuery(this).find('input').val() == '') {
                        last_cnt++;
                        jQuery(this).find(".entry-title").addClass("active");
                        jQuery(this).find(".entry-desc").slideDown();
                        jQuery(this).find('input').first().val('0');
                        jQuery(this).show();
                        return false;
                    } else {
                        rows++;

                        jQuery(this).show();
                        jQuery(this).find(".entry-title").removeClass("active");
                        jQuery(this).find(".entry-desc").slideUp();
                    }
                }

            });
        if (rows == '<?php echo $max_entries_agilysys_solutions_widget_page_section_widget_slider_image; ?>') {
            jQuery("#rew_container #message").show();
        }
    });
    jQuery(".delete-row").bind('click', function(e) {
        var count = 1;
        var current = jQuery(this).closest('.entrys').attr('id');
        jQuery.each(jQuery("#entries_agilysys_solutions_widget_page_section_widget #" + current + " .entry-desc").children(), function() {
            jQuery(this).val('');
        });
        jQuery.each(jQuery("#entries_agilysys_solutions_widget_page_section_widget #" + current + " .entry-desc p").children(), function() {
            jQuery(this).val('');
        });
        jQuery('#entries_agilysys_solutions_widget_page_section_widget #' + current + " .entry-title").removeClass('active');
        jQuery('#entries_agilysys_solutions_widget_page_section_widget #' + current + " .entry-desc").hide();
        jQuery('#entries_agilysys_solutions_widget_page_section_widget #' + current).remove();

        var cnt = 0;
        jQuery.each(jQuery(".<?php echo $widget_add_id_slider; ?>-input-containers #entries_agilysys_solutions_widget_page_section_widget")
            .children(),
            function() {
                if (jQuery(this).find('input').val() != '') {
                    jQuery(this).find('input').first().val(count);
                    cnt++;

                }
                count++;
            });
        jQuery('.what_makes_rows').val(cnt);
        last_cnt--;
    });
});
</script>
<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_solutions_widget_page_section_widget input,
select,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_solutions_widget_page_section_widget label {
    width: 40%;
}

#entries_agilysys_solutions_widget_page_section_widget label {
    width: 40%;
}

#entries_agilysys_solutions_widget_page_section_widget input,
select,
textarea {
    float: right;
    width: 60%;
}


<?php echo '.'. $widget_add_id_slider;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}



#entries_agilysys_solutions_widget_page_section_widget {
    padding: 10px 0 0;
}

#entries_agilysys_solutions_widget_page_section_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_solutions_widget_page_section_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_solutions_widget_page_section_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_solutions_widget_page_section_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_solutions_widget_page_section_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_solutions_widget_page_section_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_solutions_widget_page_section_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_solutions_widget_page_section_widget #entries_agilysys_solutions_widget_page_section_widget p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_solutions_widget_page_section_widget">
    <?php echo $rew_html; ?>
</div>
<?php
}

}
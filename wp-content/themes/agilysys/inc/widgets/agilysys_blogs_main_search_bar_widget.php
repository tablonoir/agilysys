<?php

function load_agilysys_blogs_main_search_bar_widget()
{
    register_widget('agilysys_blogs_main_search_bar_widget');
}

add_action('widgets_init', 'load_agilysys_blogs_main_search_bar_widget');

class agilysys_blogs_main_search_bar_widget extends WP_Widget
{

    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Blogs Main Search Bar Widget', 'AGILYSYS_TEXT_DOMAIN'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {
        
        echo $args['before_widget'];
        $category_id = get_cat_ID('blogs');

        $args = array(
            "hide_empty" => 0,
            "type" => "post",
            "orderby" => "name",
            "order" => "ASC",
            "child_of" => $category_id,
        );
        $categories = get_categories($args);



        ?>
        
        <style>

 
  .loader_agilysys_blogs_main_search_bar_widget {
    width: 80px;
    height: 80px;
    background: #fff;
    border: 2px solid #f3f3f3;
    border-top: 3px solid #008000;
    border-radius: 100%;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 45%;
    margin: auto;
    animation: spin 1s infinite;
    display:none;

}

@keyframes spin {
    from {
        transform: rotate(0deg);
    }

    to {
        transform: rotate(360deg);
    }
}

</style>

 <div id="overlay_agilysys_blogs_main_search_bar_widget">
        <div class="loader_agilysys_blogs_main_search_bar_widget"></div>
    </div>
    <div class="searchForm">

        <input type="text" class="" name="s" id="s" placeholder="Search this blog..."
            onKeyup="search_this(this.value);" />
        <div id="search_results_div"></div>
    </div>
   <!--  <div class="blogSubscribeForm">
        <h3 class="greenText dinProStd">Subscribe to Post Updates</h3>
        <p>Don't miss a post! Subscribe to receive our semi-monthly newsletter.</p>
        <form class="" action="" id="" method="post">
            <input type="text" class="" name="first_name" id="fname" placeholder="First Name" />
            <input type="text" class="" name="last_name" id="lname" placeholder="Last Name" />
            <input type="email" class="" name="eamil" id="email" placeholder="Email" />
            <input type="submit" class="" name="submit" id="submit" value="Subscribe" />
        </form>
    </div>
    <div class="blogCategories">
        <h3 class="greenText dinProStd">Categories</h3>
        <select id="display_category" name="display_category" class=""
            onChange="display_posts_based_on_subcat(this.value);">
            <option value="">Select Category</option>

            <?php
//foreach ($categories as $key) {
            ?>
            <option value="<?php //echo $key->slug; ?>"><?php //echo $key->cat_name; ?></option>
            <?php
//}
        ?>
        </select>
    </div>
    -->

    <script>
    function display_posts_based_on_subcat(val) {

        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

        var data = {

            action: "get-display-posts-based-on-subcat",
            category: 'blogpost',
            subcat: val
        };


        // Send the data
        jQuery.post(ajaxurl, data, function(response) {

            document.location.href = response;


        });

    }

    function search_this(val) {

        if (val != '') {
             jQuery(".loader_agilysys_blogs_main_search_bar_widget").fadeIn("slow");
            var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

            var data = {

                action: "demo-pagination-search-this",
                s: val
            };


            // Send the data
            jQuery.post(ajaxurl, data, function(response) {

                jQuery('#search_results_div').html(response);
 jQuery(".loader_agilysys_blogs_main_search_bar_widget").fadeOut("slow");

            });
        } else {
            jQuery('#search_results_div').html('');
        }
    }
    </script>

    <?php
echo $args['after_widget'];
    }

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {

    }

    public function form($display_instance)
    {

    }

}
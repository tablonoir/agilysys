<?php

add_action('widgets_init', 'agil_load_hospital');

function agil_load_hospital()
{
    register_widget('agilysys_hospital_widget');
}

class agilysys_hospital_widget extends WP_Widget
{

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Hospital Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');

    }

    public function widget($args, $instance)
    {

        echo $args['before_widget'];

        $image_uri1 = $instance['image_uri1'];

        $image[0] = preg_replace('/\s+/', '', $image_uri1);
        $image_uri2 = $instance['image_uri2'];
        $image_uri2_alt = $instance['image_uri2_alt'];

        $desc1 = $instance['desc1'];
        $desc2 = $instance['desc2'];
        $link_type = $instance['link_type'];
        if ($link_type == "link") {
            $link = $instance['link'];
        } elseif ($link_type == "page") {
            $post_id = $instance['page'];
            $post = get_post($post_id);
            $link = home_url($post->post_name) . "/";
        }

        $button_text = $instance['button_text'];
        $button_required = $instance['button_required'];
        ?>

<section class="rguestTabSection"
    style="background: url(<?php echo $image[0]; ?>) no-repeat;background-size:cover;background-position: center;" ;>
    <div class="row">
        <div class="rguestTabImg col-12 col-sm-12 col-md-6 col-lg-6">
            <img class="img-fluid" src="<?php echo $image_uri2; ?>" alt="<?php echo $image_uri2_alt; ?>" />
        </div>
        <div class="rguestTabContent col-12 col-sm-12 col-md-6 col-lg-6">
            <h2 class="dinProStd whiteText"><?php echo substr($desc1,0,250); ?></h2>
         
            <h2 class="dinProStd whiteText"><?php echo substr($desc2,0,250); ?></h2>
        

             <?php
             if($button_required=="yes")
             { 
             ?>
            <a class="homeBannerButton whiteText" href="<?php echo $link; ?>">
                    <?php echo $button_text; ?> <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                    <?php
             } 
                    ?>
        </div>
    </div>
</section>
<?php

        echo $args['after_widget'];
    }
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['image_uri1'] = strip_tags($new_instance['image_uri1']);
        $instance['image_uri2'] = strip_tags($new_instance['image_uri2']);
        $instance['image_uri2_alt'] = strip_tags($new_instance['image_uri2_alt']);

        $instance['desc1'] = strip_tags($new_instance['desc1']);
        $instance['desc2'] = strip_tags($new_instance['desc2']);

        $instance['button_required'] = strip_tags($new_instance['button_required']);
        $instance['button_text'] = strip_tags($new_instance['button_text']);

        $instance['link_type'] = $new_instance['link_type'];

        if ($new_instance['link_type'] == 'page') {
            $instance['page'] = $new_instance['page'];
            $instance['link'] = '';
        } elseif ($new_instance['link_type'] == 'link') {
            $instance['link'] = $new_instance['link'];
            $instance['page'] = '';

        }

        return $instance;
    }

    public function form($display_instance)
    {

        $image_uri1 = ($display_instance['image_uri1']);
        $image_uri2 = ($display_instance['image_uri2']);
        $desc1 = ($display_instance['desc1']);
        $desc2 = ($display_instance['desc2']);
        $button_required = $display_instance['button_required'];

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('desc1') . '"> ' . __('Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<textarea rows="6" cols="35" id="' . $this->get_field_id('desc1') . '" name="' . $this->get_field_name('desc1') . '" >' . $desc1 . '</textarea>';
        $rew_html .= '</p><br><br><br><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('desc2') . '"> ' . __('Description 2', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<textarea rows="6" cols="35" id="' . $this->get_field_id('desc2') . '" name="' . $this->get_field_name('desc2') . '" >' . $desc2 . '</textarea>';
        $rew_html .= '</p><br><br><br><br>';

        $rew_html .= '<label id="image_uri1_agilysys_hospital_widget_error"></label><br><label class="widg-label widg-img-label" for=' . $this->get_field_id('image_uri1') . '">Background Image (1920 x 1080)</label>';
        $rew_html .= '<div class="widg-img">';
        $rew_html .= '<img class="' . $this->get_field_id('image_id') . '_media_image custom_media_image" src="' . $display_instance['image_uri1'] . '" height="120" width="200" />';
        $rew_html .= '<input type="hidden" type="text" class="' . $this->get_field_id('image_id') . '_media_id custom_media_id" name="' . $this->get_field_name('image_id') . '" id="' . $this->get_field_id('image_id') . '" value="' . $display_instance['image_id'] . '" />';
        $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id') . '_media_url custom_media_url" name="' . $this->get_field_name('image_uri1') . '" id="' . $this->get_field_id('image_uri1') . '" value="' . $display_instance['image_uri1'] . '">';
        $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_upload" id="' . $this->get_field_id('image_id') . '" />';
        $rew_html .= '</div>';

        $rew_html .= '<label id="image_uri2_agilysys_hospital_widget_error"></label><br><label class="widg-label widg-img-label" for="' . $this->get_field_id('image_uri2') . '">Front Image (1000 x 755)</label>';
        $rew_html .= '<div class="widg-img">';
        $rew_html .= '<img class="' . $this->get_field_id('image_id') . '_media_image1 custom_media_image1" src="' . $display_instance['image_uri2'] . '" height="120" width="200" />';
        $rew_html .= '<input input type="hidden" type="text" class="' . $this->get_field_id('image_id') . '_media_id1 custom_media_id1" name="' . $this->get_field_name('image_id') . '" id="' . $this->get_field_id('image_id') . '" value="' . $display_instance['image_id'] . '" />';
        $rew_html .= ' <input type="hidden" class="' . $this->get_field_id('image_id') . '_media_url1 custom_media_url1" name="' . $this->get_field_name('image_uri2') . '" id="' . $this->get_field_id('image_uri2') . '" value="' . $display_instance['image_uri2'] . '">';
        $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_upload1" id="' . $this->get_field_id('image_id') . '" />';
        $rew_html .= '</div>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('image_uri2_alt') . '"> ' . __('Image Alt', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('image_uri2_alt') . '" name="' . $this->get_field_name('image_uri2_alt') . '" type="text" value="' . $display_instance['image_uri2_alt'] . '" />';
        $rew_html .= '</p><br>';

        if ($button_required == "yes") {
            $checked1 = 'checked="checked"';
            $checked2 = '';
        } elseif ($button_required == "no") {
            $checked1 = '';
            $checked2 = 'checked="checked"';
        } else {
            $checked1 = '';
            $checked2 = '';
        }

        $rew_html .= '<p><label style="float:left;">Button Required</label>';


        $rew_html .= '<span style="float:left;">Yes</span><input style="width:1% !important;float:left;" type="radio" id="' . $this->get_field_id('button_required') . '" name="' . $this->get_field_name('button_required') . '" value="yes" ' . $checked1 . ' onClick="show_hide_link_type_id_p_agilysys_hospital_widget(this.value);">';
        $rew_html .= '<span style="float:left;">No</span><input style="width:1% !important;float:left;" type="radio" id="' . $this->get_field_id('button_required') . '" name="' . $this->get_field_name('button_required') . '" value="no" ' . $checked2 . ' onClick="show_hide_link_type_id_p_agilysys_hospital_widget(this.value);"></p><br>';
    

        $rew_html .= '<div style="display:none;" id="link_type_id_p_agilysys_hospital_widget">';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('button_text') . '"> ' . __('Button Text', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('button_text') . '" name="' . $this->get_field_name('button_text') . '" type="text" value="' . $display_instance['button_text'] . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('link_type') . '"> ' . __('Link', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<select id="' . $this->get_field_id('link_type') . '" name="' . $this->get_field_name('link_type') . '" onChange="show_hide_div_agilysys_hospital_widget(this.value);">';
        $rew_html .= '<option value="">Please Select</option>';

        $link_type = $display_instance['link_type'];

        if ($link_type == 'page') {
            $rew_html .= '<option value="page" selected="selected">Page</option>';
        } else {
            $rew_html .= '<option value="page">Page</option>';
        }

        if ($link_type == 'link') {
            $rew_html .= '<option value="link" selected="selected">Link</option>';
        } else {
            $rew_html .= '<option value="link">Link</option>';
        }

        $rew_html .= '</select>';
        $rew_html .= '</p></div>';

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block"';
            $show2 = 'style="display:none"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none"';
            $show2 = 'style="display:block"';

        } else {
            $show1 = 'style="display:none"';
            $show2 = 'style="display:none"';
        }
        $rew_html .= '<div id="page_div_agilysys_hospital_widget" ' . $show1 . '><p>';
        $rew_html .= '<label for="' . $this->get_field_id('page') . '"> ' . __('Page', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<select id="' . $this->get_field_id('page') . '" name="' . $this->get_field_name('page') . '">';
        $rew_html .= '<option value="">Please Select</option>';

        $page = $display_instance['page'];

        foreach ($pages as $key) {

            if ($page == $key->ID) {
                $rew_html .= '<option value="' . $key->ID . '" selected="selected">' . $key->post_title . '</option>';
            } else {
                $rew_html .= '<option value="' . $key->ID . '">' . $key->post_title . '</option>';
            }

        }

        $rew_html .= '</select>';
        $rew_html .= '</p></div><br><br>';

        $rew_html .= '<div id="link_div_agilysys_hospital_widget" ' . $show2 . '><p>';
        $rew_html .= '<label for="' . $this->get_field_id('link') . '"> ' . __('Link', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('link') . '" name="' . $this->get_field_name('link') . '" type="text" value="' . $link . '" />';
        $rew_html .= '</p></div><br><br>';

        ?>

<script>

function show_hide_link_type_id_p_agilysys_hospital_widget(val){
    console.log(val);
    if(val=='yes')
    {
         jQuery("#link_type_id_p_agilysys_hospital_widget").show();

    }
    else if(val=='no')
    {
        jQuery("#link_type_id_p_agilysys_hospital_widget").hide();

    }
}

function show_hide_div_agilysys_hospital_widget(val){
console.log(val);
    if(val=='page')
    {
         jQuery("#page_div_agilysys_hospital_widget").show();
         jQuery("#link_div_agilysys_hospital_widget").hide();
    }
    else if(val=='link')
    {
        jQuery("#page_div_agilysys_hospital_widget").hide();
         jQuery("#link_div_agilysys_hospital_widget").show();
    }

}

</script>





<script>
jQuery(document).ready(function() {



    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {


                if (_custom_media) {


                    if(attachment.height==1080 && attachment.width==1920)
                    {
                    jQuery('.' + button_id_s + '_media_id').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_url').val(attachment.url);
                    jQuery('.' + button_id_s + '_media_image').attr('src', attachment.url).css(
                        'display', 'block');
                        jQuery('#image_uri1_agilysys_hospital_widget_error').html("");
                    }
                    else
                    {



                        jQuery('#image_uri1_agilysys_hospital_widget_error').html("Please Enter the correct Dimensions 1920x1080").css('color','red');

                    }
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });



    }
    media_upload('.custom_media_upload');


    function media_upload1(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload1', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    if(attachment.height==755 && attachment.width==1000)
                    {
                    jQuery('.' + button_id_s + '_media_id1').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_url1').val(attachment.url);
                    jQuery('.' + button_id_s + '_media_image1').attr('src', attachment.url).css(
                        'display', 'block');
                        jQuery('#image_uri2_agilysys_hospital_widget_error').html("");
                    }
                    else
                    {
                        jQuery('#image_uri2_agilysys_hospital_widget_error').html("Please Enter the correct Dimensions 1000x755").css('color','red');
                    }

                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }

    media_upload1('.custom_media_upload1');
});
</script>

<style>

#rew_container_agilysys_hospital_widget p {
   padding:10px !important;
}

.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_hospital_widget input,
select,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_hospital_widget label {
    width: 40%;
}

<?php echo '.' . $widget_add_id_slider;

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}



#entries_agilysys_hospital_widget {
    padding: 10px 0 0;
}

#entries_agilysys_hospital_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_hospital_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_hospital_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_hospital_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_hospital_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_hospital_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_hospital_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_hospital_widget #entries_agilysys_hospital_widget p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>

<div id="rew_container_agilysys_hospital_widget">
    <?php echo $rew_html; ?>
</div>

<?php

    }
}
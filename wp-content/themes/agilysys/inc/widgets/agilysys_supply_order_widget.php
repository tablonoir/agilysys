<?php

use PHPMailer\PHPMailer\PHPMailer;

require_once ABSPATH . WPINC . '/PHPMailer/PHPMailer.php';
require_once ABSPATH . WPINC . '/PHPMailer/Exception.php';

function load_agilysys_supply_order_widget()
{
    register_widget('agilysys_supply_order_widget');
}

add_action('widgets_init', 'load_agilysys_supply_order_widget');

class agilysys_supply_order_widget extends WP_Widget
{
/**
 * constructor -- name this the same as the class above
 */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Supply Order Widget', 'AGILYSYS_TEXT_DOMAIN'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');

    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"
    type="text/javascript"></script>
<script type="text/javascript" src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/additional-methods.js">
</script>

<?php

        if (isset($_POST['sup_CheckOut'])) {

            $attention = $_POST['attention'];
            $company = $_POST['company'];
            $order = $_POST['order'];
            $phone = $_POST['phone'];

            $email = $_POST['email'];
            $fax = $_POST['fax'];
            $billing_address1 = $_POST['billing_address1'];
            $billing_address2 = $_POST['billing_address2'];

            $billing_city = $_POST['billing_city'];
            $billing_state = $_POST['billing_state'];
            $billing_zip = $_POST['billing_zip'];
            $unit = $_POST['unit'];

            $shipping_address1 = $_POST['shipping_address1'];
            $shipping_address2 = $_POST['shipping_address2'];
            $shipping_city = $_POST['shipping_city'];
            $shipping_state = $_POST['shipping_state'];

            $shipping_zip = $_POST['shipping_zip'];

            $qty = $_POST['qty'];
            $partnumber = $_POST['partnumber'];
            $productDescription = $_POST['productDescription'];
            $cost = $_POST['cost'];
            $order_total = $_POST['order_total'];

            global $wpdb;

            $sql = $wpdb->prepare(
                "INSERT INTO `wp_supply_order_form_details`
                   (`attention`,`company`,`order_number`,`phone`,`email`,`fax`,`billing_address1`,`billing_address2`,`billing_city`,`billing_state`,`billing_zip`,`unit`,`shipping_address1`,`shipping_address2`,`shipping_city`,`shipping_state`,`shipping_zip`,`order_total`)
             values ('$attention', '$company', '$order', '$phone', '$email', '$fax', '$billing_address1','$billing_address2','$billing_city','$billing_state','$billing_zip','$unit','$shipping_address1','$shipping_address2','$shipping_city','$shipping_state','$shipping_zip','$order_total')");

            $exe = $wpdb->query($sql);

            $order_id = $wpdb->insert_id;

            $message = '';
            $message .= '<p>Attention: ' . $attention . '</p>';
            $message .= '<p>Company: ' . $company . '</p>';
            $message .= '<p>Order: ' . $order . '</p>';
            $message .= '<p>Phone: ' . $phone . '</p>';
            $message .= '<p>Email: ' . $email . '</p>';
            $message .= '<p>Fax: ' . $fax . '</p>';
            $message .= '<p>Billing Address 1: ' . $billing_address1 . '</p>';
            $message .= '<p>Billing Address 2: ' . $billing_address2 . '</p>';

            $message .= '<p>Billing City: ' . $billing_city . '</p>';
            $message .= '<p>Billing State: ' . $billing_state . '</p>';
            $message .= '<p>Billing Zip: ' . $billing_zip . '</p>';
            $message .= '<p>Unit: ' . $unit . '</p>';
            $message .= '<p>Shipping Address 1: ' . $shipping_address1 . '</p>';
            $message .= '<p>Shipping Address 2: ' . $shipping_address2 . '</p>';
            $message .= '<p>Shipping City: ' . $shipping_city . '</p>';
            $message .= '<p>Shipping State: ' . $shipping_state . '</p>';
            $message .= '<p>Shipping Zip: ' . $shipping_zip . '</p>';

            $message .= '<p>PartNumber Qty ProductDescription Cost </p>';
            foreach ($qty as $key => $val) {
                if ($val != 0) {
                    $message .= '<p>' . $partnumber[$key] . ' ' . $qty[$key] . ' ' . strip_tags($productDescription[$key]) . ' ' . $cost[$key] . '</p>';

                    $sql = $wpdb->prepare(
                        "INSERT INTO `wp_supply_order_form_order_items`
                           (`order_id`,`partnumber`,`qty`,`productDescription`,`cost`)
                     values ('$order_id', '$partnumber[$key]', '$qty[$key]', '$productDescription[$key]', '$cost[$key]')");

                    $exe = $wpdb->query($sql);
                }
            }
            $message .= '<p>Total Cost: ' . $order_total . '</p>';

            $mail = new PHPMailer();

//$mail->isSMTP();

            $mail->SMTPDebug = false;

            $mail->Host = 'smtp.gmail.com';

            $mail->SMTPAuth = true;

            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port = 587;

            $mail->Username = 'sau3334@gmail.com';

            $mail->Password = 'rwwtpnhahufyxqih';

            $mail->setFrom('testing@abiteoffrance.com', 'Testing');
            $mail->addReplyTo('sau3334@gmail.com', 'Saurav Daga');
$mail->addAddress('sau3334@gmail.com', 'Saurav');
            //$mail->addAddress('vandhiyadevan.jayasooriyan@agilysys.com', 'Deva');

            $mail->Subject = 'Supply Order Form';

            $mail->Body = $message;

            $mail->AltBody = 'Supply Order Form';

            if (!$mail->send()) {
                echo 'Mailer Error: ' . $mail->ErrorInfo;
            } else {
                
                ?>
                
              <script type="text/javascript">
    jQuery(window).on('load', function() {
        jQuery('#myModal').modal('show');
    });
</script>     
                
                <?php
               
            }

        }
        ?>


<div class="" style="margin-top:50px !important;">
    <section class="supplyRequest lmsForm">
        <h2 class="dinProStd greenText center">Supply Order Request Form</h2>

        <form class="supplySearch center" action="" method="post">
            <input type="text" id="sup_Serach" name="sup_Serach" placeholder="Search Orders" />
            <input type="hidden" id="sup_search_submit" />
        </form>

        <div class="supplySearchTitle">
            <h2 class="dinProStd greenText">Company, Billing and Shipping Information </h2>
            <p>Please fill out the following contact, billing and shipping information. <span>* denotes a required
                    field.</span></p>
        </div>

        <form class="supplyOrderForm" action="" method="post" name="registration">
            <div class="supplyOrderbox">
                <div class="lmsFormLabel flex">
                    <div class="lmsLabelBox flex">
                        <label for="First Name" class="dinProStd blackText">* Attention</label>
                        <input type="text" id="supply_atten" name="attention" />
                    </div>
                    <div class="lmsLabelBox flex">
                        <label for="Last Name" class="dinProStd blackText">* Company</label>
                        <input type="text" id="supply_Company" name="company" />
                    </div>
                </div>
                <div class="lmsFormLabel flex">
                    <div class="lmsLabelBox flex">
                        <label for="Company" class="dinProStd blackText">* Purchase Order #:</label>
                        <input type="text" id="supply_Order" name="order" />
                    </div>
                    <div class="lmsLabelBox flex">
                        <label for="Job Title" class="dinProStd blackText">* Phone:</label>
                        <input type="text" id="supply_Phone" name="phone" />
                    </div>
                </div>
                <div class="lmsFormLabel flex">
                    <div class="lmsLabelBox flex">
                        <label for="Phone" class="dinProStd blackText">* Emai:</label>
                        <input class="number-only" type="text" id="supply_Email" name="email" />
                    </div>
                    <div class="lmsLabelBox flex">
                        <label for="Email" class="dinProStd blackText">Fax:</label>
                        <input type="text" id="supply_Fax" name="fax" />
                    </div>
                </div>
                <div class="lmsFormLabel flex">
                    <div class="lmsLabelBox flex">
                        <label for="Country" class="dinProStd blackText">* Billing Address:</label>
                        <input type="text" id="supply_Billing_add1" name="billing_address1" />
                    </div>
                    <div class="lmsLabelBox flex">
                        <label for="Address" class="dinProStd blackText">Billing Address2:</label>
                        <input type="text" id="supply_Billing_address2" name="billing_address2" />
                    </div>
                </div>
                <div class="lmsFormLabel flex">
                    <div class="lmsLabelBox flex">
                        <label for="City" class="dinProStd blackText">* Billing City:</label>
                        <input type="text" id="supply_Billing_City" name="billing_city" />
                    </div>
                    <div class="lmsLabelBox flex">
                        <label for="City" class="dinProStd blackText">* Billing State:</label>
                        <input type="text" id="supply_Billing_State" name="billing_state" />
                    </div>
                </div>
                <div class="lmsFormLabel flex">
                    <div class="lmsLabelBox flex">
                        <label for="City" class="dinProStd blackText">* Billing Zip:</label>
                        <input type="text" id="supply_Billing_zip" name="billing_zip" />
                    </div>
                    <div class="lmsLabelBox flex">
                        <label for="City" class="dinProStd blackText">Unit #:</label>
                        <input type="text" id="supply_unit" name="unit" />
                    </div>
                </div>

                <div class="lmsCheckBox flex">
                    <input type="checkbox" class="customCheckBox" id="test1" name="" value=""
                        onClick="set_shipping_addr();">
                    <label for="test1">
                        <h3 class="dinProStd blackText">Shipping Address is the Same as Billing Address:</h3>
                    </label>
                </div>

                <div class="lmsFormLabel flex">
                    <div class="lmsLabelBox flex">
                        <label for="City" class="dinProStd blackText">* Shipping Address:</label>
                        <input type="text" id="supply_Add" name="shipping_address1" />
                    </div>
                    <div class="lmsLabelBox flex">
                        <label for="City" class="dinProStd blackText">Shipping Address2:</label>
                        <input type="text" id="supply_Add2" name="shipping_address2" />
                    </div>
                </div>
                <div class="lmsFormLabel flex">
                    <div class="lmsLabelBox flex">
                        <label for="City" class="dinProStd blackText">* Shipping City:</label>
                        <input type="text" id="supply_Ship_City" name="shipping_city" />
                    </div>
                    <div class="lmsLabelBox flex">
                        <label for="City" class="dinProStd blackText">* Shipping State:</label>
                        <input type="text" id="supply_Ship_State" name="shipping_state" />
                    </div>
                </div>

                <div class="lmsFormLabel flex">
                    <div class="lmsLabelBox flex">
                        <label for="City" class="dinProStd blackText">* Shipping Zip:</label>
                        <input type="text" id="supply_Ship_Zip" name="shipping_zip" />
                    </div>
                </div>

                <div class="supplyItems">
                    <h2 class="dinProStd greenText">Your Items</h2>
                    <p>Choose the items that you would like to order and enter the quantity of that part in the quantity
                        fields below</p>

                    <table>
                        <thead>
                            <tr class="productsectiontitle">
                                <th class="productPartNumber dinProStd blackText">Part Number</th>
                                <th class="productDescription dinProStd blackText">Description</th>
                                <th class="productQuantity dinProStd blackText">Quantity</th>
                                <th class="productCost dinProStd blackText">Cost</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="productsectiontitle">
                                <td class="category" colspan="4">
                                    <center><b class="dinProStd blackText">Accessories </b></center>
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    ACC-FP2504-SA

                                    <input type="hidden" name="partnumber[]" id="partnumber_1" value="ACC-FP2504-SA">
                                </td>
                                <td class="productDescription">
                                    <b> Stylus for iSC250/iSC480 </b>
                                    &nbsp;<br>Right Angle Stylus for iSC250/iSC480


                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_1"><b>Stylus for iSC250/iSC480</b>&nbsp;<br>Right Angle Stylus for iSC250/iSC480</textarea>

                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_1" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(1);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_1" name="cost[]" size="5"
                                        type="text" value="0">

                                    <input type="hidden" name="unitcost[]" id="unitCost_1" value="30">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    23665-01-R
                                    <input type="hidden" name="partnumber[]" id="partnumber_2" value="23665-01-R">
                                </td>
                                <td class="productDescription">
                                    <b> Stylus: MX870, Thick Stylus </b>
                                    &nbsp;<br>Stylus Assembly for Verifone MX800 Series



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_2"><b> Stylus: MX870, Thick Stylus  </b>&nbsp;<br>Stylus Assembly for Verifone MX800 Series</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_2" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(2);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_2" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_2" value="20">

                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    27555-01-R
                                    <input type="hidden" name="partnumber[]" id="partnumber_3" value="27555-01-R">
                                </td>
                                <td class="productDescription">
                                    <b> Stylus: MX870, Thin Stylus </b>
                                    &nbsp;<br>Thin Stylus for Verifone MX Series



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_3"><b> Stylus: MX870, Thin Stylus  </b>&nbsp;<br>Thin Stylus for Verifone MX Series</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_3" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(3);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_3" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_3" value="35">
                                </td>
                            </tr>
                            <tr class="productsectiontitle">
                                <td class="category" colspan="4">
                                    <center><b class="dinProStd blackText">Add Ons </b></center>
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    JRS-ART02142
                                    <input type="hidden" name="partnumber[]" id="partnumber_4" value="JRS-ART02142">
                                </td>
                                <td class="productDescription">
                                    <b> 16 x 16 Cash Drawer </b>
                                    &nbsp;<br>16 x 16 Cash Drawer



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_4"><b> 16 x 16 Cash Drawer</b>&nbsp;<br>16 x 16 Cash Drawer</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_4" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(4);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_4" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_4" value="90">

                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    APG-VB554-BL1616
                                    <input type="hidden" name="partnumber[]" id="partnumber_5" value="APG-VB554-BL1616">
                                </td>
                                <td class="productDescription">
                                    <b> APG Model 554A USB ProII Black Cash Drawer </b>
                                    &nbsp;<br>APG Model 554A USB ProII Black Cash Drawer-2 media slots, USB connected
                                    drawer
                                    for IG &amp; IG Flex POS Terminals




                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_5"><b> APG Model 554A USB ProII Black Cash Drawer  </b>
                                    &nbsp;<br>APG Model 554A USB ProII Black Cash Drawer-2 media slots, USB connected drawer for IG &amp; IG Flex POS Terminals</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_5" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(5);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_5" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_5" value="330">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    M2354-0001
                                    <input type="hidden" name="partnumber[]" id="partnumber_6" value="M2354-0001">
                                </td>
                                <td class="productDescription">
                                    <b> Cash Drawer 16" w/ Cable &amp; Insert - PAR/APG </b>
                                    &nbsp;<br>PAR APG Cash Drawer 16" with Cable and Insert



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_6"><b> Cash Drawer 16" w/ Cable &amp; Insert - PAR/APG   </b>
                                    &nbsp;<br>PAR APG Cash Drawer 16" with Cable and Insert</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_6" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(6);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_6" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_6" value="230">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    F1501
                                    <input type="hidden" name="partnumber[]" id="partnumber_7" value="F1501">
                                </td>
                                <td class="productDescription">
                                    <b> Cash Drawer Lock &amp; Key Assembly - PAR M2352A </b>
                                    &nbsp;<br>Par M2352A Cash Drawer Lock &amp; Key Assembly



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_7"><b> Cash Drawer Lock &amp; Key Assembly - PAR M2352A   </b>
                                    &nbsp;<br>Par M2352A Cash Drawer Lock &amp; Key Assembly</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_7" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(7);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_7" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_7" value="20">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    TGC-00EP034
                                    <input type="hidden" name="partnumber[]" id="partnumber_8" value="TGC-00EP034">
                                </td>
                                <td class="productDescription">
                                    <b> Cash Drawer Lock Insert - Toshiba </b>
                                    &nbsp;<br>Cash Drawer Lock Insert - Toshiba



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_8"><b> Cash Drawer Lock Insert - Toshiba  </b>
                                    &nbsp;<br>Cash Drawer Lock Insert - Toshiba</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_8" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(8);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_8" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_8" value="10">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">

                                    JRS-3S430TIL
                                    <input type="hidden" name="partnumber[]" id="partnumber_9" value="JRS-3S430TIL">
                                </td>
                                <td class="productDescription">
                                    <b> Cash Drawer Till insert - J2 </b>
                                    &nbsp;<br>Till insert for J2 cash drawer




                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_9"><b> Cash Drawer Till insert - J2  </b>
                                    &nbsp;<br>Till insert for J2 cash drawer</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_9" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(9);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_9" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_9" value="25">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    K2331
                                    <input type="hidden" name="partnumber[]" id="partnumber_10" value="K2331">
                                </td>
                                <td class="productDescription">
                                    <b> Cash Drawer Till Insert - PAR M2352A </b>
                                    &nbsp; <br>Till Insert for Par Cash Drawer M2352A-0001



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_10"><b> Cash Drawer Till Insert - PAR M2352A  </b>
                                    &nbsp; <br>Till Insert for Par Cash Drawer M2352A-0001</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_10" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(10);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_10" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_10" value="40">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    K8570
                                    <input type="hidden" name="partnumber[]" id="partnumber_11" value="K8570">
                                </td>
                                <td class="productDescription">
                                    <b> Cash Drawer Till Insert - PAR M8570 </b>
                                    &nbsp;<br>Par Till Insert for M8570 Cash Drawer



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_11"><b> Cash Drawer Till Insert - PAR M8570  </b>
                                    &nbsp;<br>Par Till Insert for M8570 Cash Drawer</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_11" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(11);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_11" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_11" value="40">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    RG-PAY-HW-ING-SEN351014
                                    <input type="hidden" name="partnumber[]" id="partnumber_12"
                                        value="RG-PAY-HW-ING-SEN351014">
                                </td>
                                <td class="productDescription">
                                    <b> Device Stand for Ingenico iPP3xx with 0-90° tilt with plate &amp; cradle </b>
                                    &nbsp;<br>Device Stand for Ingenico iPP3xx with 0-90° tilt with plate &amp; cradle


                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_12"><b> Device Stand for Ingenico iPP3xx with 0-90° tilt with plate &amp; cradle  </b>
                                    &nbsp;<br>Device Stand for Ingenico iPP3xx with 0-90° tilt with plate &amp; cradle</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_12" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(12);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_12" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_12" value="70">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    RG-PAY-HW-ING-SEN351013
                                    <input type="hidden" name="partnumber[]" id="partnumber_13"
                                        value="RG-PAY-HW-ING-SEN351013">
                                </td>
                                <td class="productDescription">
                                    <b> Device Stand for Ingenico iPP3xx with 65° tilt with plate &amp; cradle </b>
                                    &nbsp;<br>Device Stand for Ingenico iPP3xx with 65° tilt with plate &amp; cradle



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_13"><b> Device Stand for Ingenico iPP3xx with 65°  tilt with plate &amp; cradle  </b>
                                    &nbsp;<br>Device Stand for Ingenico iPP3xx with 65°  tilt with plate &amp; cradle</textarea>

                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_13" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(13);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_13" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_13" value="160">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    ING-BOX350769
                                    <input type="hidden" name="partnumber[]" id="partnumber_14" value="ING-BOX350769">
                                </td>
                                <td class="productDescription">
                                    <b> Glue Pad for Stands </b>
                                    &nbsp;<br> Glue Pad for Stands - To adhere stands to surface without screws.




                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_14"><b> Glue Pad for Stands  </b>
                                    &nbsp;<br> Glue Pad for Stands - To adhere stands to surface without screws.</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_14" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(14);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_14" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_14" value="25">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    JRS-RDVFDFAGY
                                    <input type="hidden" name="partnumber[]" id="partnumber_15" value="JRS-RDVFDFAGY">
                                </td>
                                <td class="productDescription">
                                    <b> J2 Pole Display w/ Serial Cable &amp; Power Supply </b>
                                    &nbsp; <br>J2 Pole Display-2-line X 20-character Stand Alone - Serial 8" Screen,
                                    With
                                    Hardwired Serial Cable &amp; Power Supply



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_15"><b> J2 Pole Display w/ Serial Cable &amp; Power Supply </b>
                                &nbsp; <br>J2 Pole Display-2-line X 20-character Stand Alone - Serial 8" Screen, With
                                Hardwired Serial Cable &amp; Power Supply</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_15" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(15);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_15" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_15" value="144">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">

                                    JRS-RD225AGY
                                    <input type="hidden" name="partnumber[]" id="partnumber_16" value="JRS-RD225AGY">
                                </td>
                                <td class="productDescription">
                                    <b> J2 Rear Display for J2 225 - 6" Screen </b>
                                    &nbsp; <br>J2 Rear Display for J2 225 with 2 line x 20 Character - 6" Screen




                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_16"> <b> J2 Rear Display for J2 225 - 6" Screen </b>
                                &nbsp; <br>J2 Rear Display for J2 225 with 2 line x 20 Character - 6" Screen</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_16" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(16);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_16" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_16" value="80">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    RG-PAY-HW-ING-SEN350819
                                    <input type="hidden" name="partnumber[]" id="partnumber_17"
                                        value="RG-PAY-HW-ING-SEN350819">
                                </td>
                                <td class="productDescription">
                                    <b> Locking Device Stand for Ingenico iSC250 with 0-65° tilt </b> &nbsp; <br>Locking
                                    Device Stand for Ingenico iSC250 with 0-65° tilt




                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_17"><b> Locking Device Stand for Ingenico iSC250 with 0-65° tilt </b> &nbsp; <br>Locking
                                Device Stand for Ingenico iSC250 with 0-65° tilt</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_17" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(17);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_17" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_17" value="195">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    RG-PAY-HW-ING-SEN350818
                                    <input type="hidden" name="partnumber[]" id="partnumber_18"
                                        value="RG-PAY-HW-ING-SEN350818">
                                </td>
                                <td class="productDescription">
                                    <b> Locking Device Stand for Ingenico iSC250 with 0-90° tilt </b> &nbsp; <br>Locking
                                    Device Stand for Ingenico iSC250 with 0-90° tilt



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_18"><b> Locking Device Stand for Ingenico iSC250 with 0-90° tilt </b> &nbsp; <br>Locking
                                Device Stand for Ingenico iSC250 with 0-90° tilt</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_18" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(18);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_18" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_18" value="200">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    LOG-LD9000XG
                                    <input type="hidden" name="partnumber[]" id="partnumber_19" value="LOG-LD9000XG">
                                </td>
                                <td class="productDescription">
                                    <b> Logic Controls Double-Sided&nbsp;Pole Display </b>
                                    &nbsp;<br>Logic Controls Double-Sided&nbsp;Pole Display,&nbsp; 2x20 char, Serial
                                    DB9F
                                    connect,Power supply incl.



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_19"><b> Logic Controls Double-Sided&nbsp;Pole Display </b>
                                &nbsp;<br>Logic Controls Double-Sided&nbsp;Pole Display,&nbsp; 2x20 char, Serial DB9F
                                connect,Power supply incl.</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_19" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(19);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_19" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_19" value="576">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    ING-SEN350765
                                    <input type="hidden" name="partnumber[]" id="partnumber_20" value="ING-SEN350765">
                                </td>
                                <td class="productDescription">
                                    <b> Stand Assembly for iSC480, 0-90° tilt </b>
                                    &nbsp;<br>Stand Assembly for iSC480, 0-90° tilt (Heavy Duty, Swivel)



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_20"><b> Stand Assembly for iSC480, 0-90° tilt </b>
                                &nbsp;<br>Stand Assembly for iSC480, 0-90° tilt (Heavy Duty, Swivel)</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_20" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(20);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_20" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_20" value="80">
                                </td>
                            </tr>
                            <tr class="productsectiontitle">
                                <td class="category" colspan="4">
                                    <center><b class="dinProStd blackText">Cables </b></center>
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    CTG-03019
                                    <input type="hidden" name="partnumber[]" id="partnumber_21" value="CTG-03019">
                                </td>
                                <td class="productDescription">
                                    <b> Cable: 6' DB25M to DB9F Null Modem Cable, Beige </b>
                                    &nbsp;<br>6' DB25M to DB9F Null Modem Cable, Beige



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_21"><b> Cable: 6' DB25M to DB9F Null Modem Cable, Beige </b>
                                &nbsp;<br>6' DB25M to DB9F Null Modem Cable, Beige</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_21" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(21);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_21" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_21" value="9">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    15196
                                    <input type="hidden" name="partnumber[]" id="partnumber_22" value="15196">
                                </td>
                                <td class="productDescription">
                                    <b> Cable: 7ft SE 350 MHz Snagless patch cable - black </b>
                                    &nbsp;<br>7ft SE 350 MHz Snagless patch cable - black



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_22"> <b> Cable: 7ft SE 350 MHz Snagless patch cable - black </b>
                                &nbsp;<br>7ft SE 350 MHz Snagless patch cable - black</textarea>


                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_22" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(22);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_22" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_22" value="5">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">

                                    64066733
                                    <input type="hidden" name="partnumber[]" id="partnumber_23" value="64066733">
                                </td>
                                <td class="productDescription">
                                    <b> Cable: Ariva Scale Power Supply </b>
                                    &nbsp;<br> KOP Power Supply w/ Adaptors for ARIVA scale



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_23"><b> Cable: Ariva Scale Power Supply </b>
                                &nbsp;<br> KOP Power Supply w/ Adaptors for ARIVA scale</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_23" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(23);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_23" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_23" value="70">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    7003468
                                    <input type="hidden" name="partnumber[]" id="partnumber_24" value="7003468">
                                </td>
                                <td class="productDescription">

                                    <b> Cable: DB25F/DB9M to RJ45 Adapter </b>
                                    &nbsp;<br>DB25F and DB9M serial ports to RJ45 connectors


                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_24"><b> Cable: DB25F/DB9M to RJ45 Adapter </b>
                                &nbsp;<br>DB25F and DB9M serial ports to RJ45 connectors</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_24" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(24);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_24" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_24" value="20">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    7003486
                                    <input type="hidden" name="partnumber[]" id="partnumber_25" value="7003486">
                                </td>
                                <td class="productDescription">
                                    <b> Cable: DB9F/DB9F Null Modem PC to Systech Device </b>
                                    &nbsp;<br>DB9 F/F 5ft Custom Cable


                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_25"><b> Cable: DB9F/DB9F Null Modem PC to Systech Device </b>
                                &nbsp;<br>DB9 F/F 5ft Custom Cable</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_25" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(25);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_25" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_25" value="15">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    7003553
                                    <input type="hidden" name="partnumber[]" id="partnumber_26" value="7003553">
                                </td>
                                <td class="productDescription">
                                    <b> Cable: DB9F/RJ45 Modular Adapter </b>
                                    &nbsp;<br>DB9m/RJ45 Modular



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_26"><b> Cable: DB9F/RJ45 Modular Adapter </b>
                                &nbsp;<br>DB9m/RJ45 Modular</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_26" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(26);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_26" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_26" value="40">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    7005662
                                    <input type="hidden" name="partnumber[]" id="partnumber_27" value="7005662">
                                </td>
                                <td class="productDescription">
                                    <b> Cable: DB9M/DB9F </b>
                                    &nbsp;<br>DB9M / DB9F Cable - 8C, 6ft



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_27"><b> Cable: DB9M/DB9F </b>
                                &nbsp;<br>DB9M / DB9F Cable - 8C, 6ft</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_27" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(27);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_27" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_27" value="35">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    PWR-WUA5V4W0US

                                    <input type="hidden" name="partnumber[]" id="partnumber_28" value="PWR-WUA5V4W0US">
                                </td>
                                <td class="productDescription">
                                    <b> Cable: DS9208 Scanner Power Supply </b>
                                    &nbsp;<br>DS9208 Power Supply AC Input: 100-240V, DC Output: 5.2V - Includes line
                                    cord




                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_28"><b> Cable: DS9208 Scanner Power Supply </b>
                                &nbsp;<br>DS9208 Power Supply AC Input: 100-240V, DC Output: 5.2V - Includes line cord</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_28" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(28);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_28" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_28" value="15">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    C2355

                                    <input type="hidden" name="partnumber[]" id="partnumber_29" value="C2355">
                                </td>
                                <td class="productDescription">
                                    <b> Cable: Dual Cash Drawer Adapter Cable - PAR </b>
                                    &nbsp;<br>PAR POS Dual Cash Drawer Adapter Cable



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_29"><b> Cable: Dual Cash Drawer Adapter Cable - PAR </b>
                                &nbsp;<br>PAR POS Dual Cash Drawer Adapter Cable</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_29" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(29);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_29" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_29" value="10">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    CEPS-003
                                    <input type="hidden" name="partnumber[]" id="partnumber_30" value="CEPS-003">
                                </td>
                                <td class="productDescription">
                                    <b> Cable: Epson 6ft DB9F/DB25M Cable </b>
                                    &nbsp;<br>Epson 6ft DB9 Female DB25 Male Cable




                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_30"><b> Cable: Epson 6ft DB9F/DB25M Cable  </b>
                                    &nbsp;<br>Epson 6ft DB9 Female DB25 Male Cable</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_30" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(30);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_30" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_30" value="35">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    EPS-PS180
                                    <input type="hidden" name="partnumber[]" id="partnumber_31" value="EPS-PS180">
                                </td>
                                <td class="productDescription">
                                    <b> Cable: Epson Power Cable </b>
                                    &nbsp;<br>Epson 110/220 Volt AC Power Supply With AC Cord

                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_31"><b> Cable: Epson Power Cable </b>
                                &nbsp;<br>Epson 110/220 Volt AC Power Supply With AC Cord</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_31" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(31);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_31" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_31" value="30">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    RG-PAY-HW-ING-296111170AC
                                    <input type="hidden" name="partnumber[]" id="partnumber_32"
                                        value="RG-PAY-HW-ING-296111170AC">
                                </td>
                                <td class="productDescription">
                                    <b> Cable: Ingenico iSC250/iSC480 Interface Cable - USB </b>
                                    &nbsp;<br>Ingenico iSC250/iSC480 Device Interface Cable - USB. Not powered. A
                                    separate
                                    power supply is required.



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_32"><b> Cable: Ingenico iSC250/iSC480 Interface Cable - USB </b>
                                &nbsp;<br>Ingenico iSC250/iSC480 Device Interface Cable - USB. Not powered. A separate
                                power supply is required.</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_32" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(32);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_32" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_32" value="20">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    ARTUS-00044
                                    <input type="hidden" name="partnumber[]" id="partnumber_33" value="ARTUS-00044">
                                </td>
                                <td class="productDescription">
                                    <b> Cable: J2 Adapter RJ45M/DB9M </b>
                                    &nbsp;<br>Pole Display Cable Adapter (RJ45 to DB9 male)



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_33"><b> Cable: J2 Adapter RJ45M/DB9M </b>
                                &nbsp;<br>Pole Display Cable Adapter (RJ45 to DB9 male)</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_33" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(33);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_33" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_33" value="40">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    JRS-ARTUS00045
                                    <input type="hidden" name="partnumber[]" id="partnumber_34" value="JRS-ARTUS00045">
                                </td>
                                <td class="productDescription">
                                    <b> Cable: J2 RJ45F/DB9F </b>
                                    &nbsp;<br>J2 Cable Adaptor RJ45/DB9 Female


                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_34"><b> Cable: J2 RJ45F/DB9F </b>
                                &nbsp;<br>J2 Cable Adaptor RJ45/DB9 Female</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_34" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(34);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_34" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_34" value="14">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    ARTUS-00046
                                    <input type="hidden" name="partnumber[]" id="partnumber_35" value="ARTUS-00046">
                                </td>
                                <td class="productDescription">
                                    <b> Cable: J2 Y Cable for 2 Cash Drawers </b>
                                    &nbsp;<br>J2 225 Splitter Cable for 2 Cash Drawers



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_35"><b> Cable: J2 Y Cable for 2 Cash Drawers </b>
                                &nbsp;<br>J2 225 Splitter Cable for 2 Cash Drawers</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_35" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(35);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_35" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_35" value="195">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    C2350A

                                    <input type="hidden" name="partnumber[]" id="partnumber_36" value="C2350A">
                                </td>
                                <td class="productDescription">
                                    <b> Cable: PAR Gemini Cash Drawer Cable </b>
                                    &nbsp;<br>PAR POS Dual Cash Drawer Adapter Cable



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_36"><b> Cable: PAR Gemini Cash Drawer Cable </b>
                                &nbsp;<br>PAR POS Dual Cash Drawer Adapter Cable</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_36" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(36);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_36" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_36" value="35">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    363015001
                                    <input type="hidden" name="partnumber[]" id="partnumber_37" value="363015001">
                                </td>
                                <td class="productDescription">
                                    <b> Cable: PAR Gemini Power Supply </b>
                                    &nbsp;<br>PAR Gemini Power Supply

                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_37"><b> Cable: PAR Gemini Power Supply </b>
                                &nbsp;<br>PAR Gemini Power Supply</textarea>

                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_37" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(37);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_37" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_37" value="8.5">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    296196003
                                    <input type="hidden" name="partnumber[]" id="partnumber_38" value="296196003">
                                </td>
                                <td class="productDescription">
                                    <b> Cable: Power Supply for the iSC250 </b>
                                    &nbsp;<br>Power Supply for the iSC250 Used with rGuest Pay



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_38"><b> Cable: Power Supply for the iSC250 </b>
                                &nbsp;<br>Power Supply for the iSC250 Used with rGuest Pay</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_38" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(38);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_38" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_38" value="15">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    CTG-15199
                                    <input type="hidden" name="partnumber[]" id="partnumber_39" value="CTG-15199">
                                </td>
                                <td class="productDescription">
                                    <b> Cable: Printer &amp; Terminal Patch Cable </b>
                                    &nbsp;<br>Cables To Go 10ft CAT 5E 350MHz Snagless Patch Cable, Grey



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_39"><b> Cable: Printer &amp; Terminal Patch Cable </b>
                                &nbsp;<br>Cables To Go 10ft CAT 5E 350MHz Snagless Patch Cable, Grey</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_39" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(39);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_39" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_39" value="9">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    CTG-7082609-INV
                                    <input type="hidden" name="partnumber[]" id="partnumber_40" value="CTG-7082609-INV">
                                </td>
                                <td class="productDescription">
                                    <b> Cable: Serial Cable for Ariva-S Scale </b>
                                    &nbsp;<br>Custom Serial Cable for Mettler Toledo Ariva-S scale



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_40"><b> Cable: Serial Cable for Ariva-S Scale </b>
                                &nbsp;<br>Custom Serial Cable for Mettler Toledo Ariva-S scale</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_40" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(40);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_40" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_40" value="20">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    28102
                                    <input type="hidden" name="partnumber[]" id="partnumber_41" value="28102">
                                </td>
                                <td class="productDescription">
                                    <b> Cable: USB Cable 2M/6.5ft, Black </b>
                                    &nbsp;<br>USB Cable 2M/6.5ft, Black




                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_41"><b> Cable: USB Cable 2M/6.5ft, Black </b>
                                &nbsp;<br>USB Cable 2M/6.5ft, Black</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_41" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(41);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_41" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_41" value="10">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    RG-PAY-HW-ING-296100039AB
                                    <input type="hidden" name="partnumber[]" id="partnumber_42"
                                        value="RG-PAY-HW-ING-296100039AB">
                                </td>
                                <td class="productDescription">
                                    <b> Cable: USB Powered 5V Cable for iPP320/iPP350 </b>
                                    &nbsp;<br>USB powered 5V cable for the iPP320 &amp; iPP350




                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_42"><b> Cable: USB Powered 5V Cable for iPP320/iPP350 </b>
                                &nbsp;<br>USB powered 5V cable for the iPP320 &amp; iPP350</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_42" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(42);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_42" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_42" value="1.5">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    F2350
                                    <input type="hidden" name="partnumber[]" id="partnumber_43" value="F2350">
                                </td>
                                <td class="productDescription">
                                    <b> Power Cord for PAR Docking Station K8983A-01 </b>
                                    &nbsp;<br>Power Cord for PAR Docking Station K8983A-01



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_43"><b> Power Cord for PAR Docking Station K8983A-01 </b>
                                &nbsp;<br>Power Cord for PAR Docking Station K8983A-01</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_43" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(43);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_43" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_43a" value="150">
                                    <input type="hidden" name="unitcost[]" id="unitCost_43b" value="100">

                                </td>
                            </tr>
                            <tr class="productsectiontitle">
                                <td class="category" colspan="4">
                                    <center><b class="dinProStd blackText">Misc </b></center>
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    0400210
                                    <input type="hidden" name="partnumber[]" id="partnumber_44" value="0400210">

                                </td>
                                <td class="productDescription">
                                    <b> Card Reader Cleaning Card </b>
                                    &nbsp;<br>MSR Card Reader Cleaning Card




                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_44"><b> Card Reader Cleaning Card </b>
                                &nbsp;<br>MSR Card Reader Cleaning Card</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_44" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(44);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_44" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_44" value="55">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    9001369
                                    <input type="hidden" name="partnumber[]" id="partnumber_45" value="9001369">
                                </td>
                                <td class="productDescription">
                                    <b> Magnetic Stripe Cards (Non-Customized) 100 Pack </b>
                                    &nbsp;
                                    <br>
                                    <div class="tableDiv">
                                        Volume
                                    </div>
                                    <div class="tableDiv">
                                        Price Per Unit
                                    </div>
                                    <div class="breakclear"></div>
                                    <div class="tableDiv">
                                        1 - 4 cases
                                    </div>
                                    <div class="tableDiv">
                                        $150.00 per case
                                    </div>
                                    <div class="breakclear"></div>
                                    <div class="tableDiv">
                                        5 - 10 cases
                                    </div>
                                    <div class="tableDiv">
                                        $100.00 per case
                                    </div>
                                    <div class="breakclear"></div>
                                    <div class="tableDiv">
                                        11 + cases
                                    </div>
                                    <div class="tableDiv">
                                        $100.00 per case
                                    </div>
                                    <div class="breakclear"></div>




                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_45"><b> Magnetic Stripe Cards (Non-Customized) 100 Pack  </b>&nbsp;<br><div class="
                                    tableDiv">Volume
            </div>
            <div class="tableDiv">Price Per Unit</div>
            <div class="breakclear"></div>
            <div class="tableDiv">1 - 4 cases</div>
            <div class="tableDiv">$150.00 per case</div>
            <div class="breakclear"></div>
            <div class="tableDiv">5 - 10 cases</div>
            <div class="tableDiv">$100.00 per case</div>
            <div class="breakclear"></div>
            <div class="tableDiv">11 + cases</div>
            <div class="tableDiv">$100.00 per case</div>
            <div class="breakclear"></div></textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_45" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(45);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_45" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_45" value="115">
                                </td>
                            </tr>
                            <tr class="productsectiontitle">
                                <td class="category" colspan="4">
                                    <center><b class="dinProStd blackText">Pape</b></center>
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    10R3269

                                    <input type="hidden" name="partnumber[]" id="partnumber_46" value="10R3269">
                                </td>
                                <td class="productDescription">



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_46"><b> Paper: Cash Register/Adding Machine Rolls - Bond 3" x 165" - 1 Ply</b>&nbsp;<br>
                    <div class="tableDiv">Volume</div>
                    <div class="tableDiv">Price Per Unit</div>
                    <div class="breakclear"></div>
                    <div class="tableDiv">1 - 24 cases</div>
                    <div sclass="tableDiv">$42.00 per case</div>
                    <div class="breakclear"></div>
                    <div class="tableDiv">25 - 49 cases</div>
                    <div class="tableDiv">$34.00 per case</div>
                    <div class="breakclear"></div>
                    <div class="tableDiv">50 - 99 cases</div>
                    <div class="tableDiv">$33.00 per case</div>
                    <div class="breakclear"></div>
                    <div class="tableDiv">100 - 500 cases</div>
                    <div class="tableDiv">$32.00 per case</div><div class="breakclear">
        </div></textarea>


                                    <b> Paper: Cash Register/Adding Machine Rolls - Bond 3" x 165' - 1 Ply </b>
                                    &nbsp;<br>
                                    <div class="tableDiv">
                                        Volume
                                    </div>
                                    <div class="tableDiv">
                                        Price Per Unit
                                    </div>
                                    <div class="breakclear"></div>
                                    <div class="tableDiv">
                                        1 - 24 cases
                                    </div>
                                    <div sclass="tableDiv">
                                        $42.00 per case
                                    </div>
                                    <div class="breakclear"></div>
                                    <div class="tableDiv">
                                        25 - 49 cases
                                    </div>
                                    <div class="tableDiv">
                                        $34.00 per case
                                    </div>
                                    <div class="breakclear"></div>
                                    <div class="tableDiv">
                                        50 - 99 cases
                                    </div>
                                    <div class="tableDiv">
                                        $33.00 per case
                                    </div>
                                    <div class="breakclear"></div>
                                    <div class="tableDiv">
                                        100 - 500 cases
                                    </div>
                                    <div class="tableDiv">
                                        $32.00 per case
                                    </div>
                                    <div class="breakclear"></div>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_46" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(46);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_46" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_46" value="70">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    SPE-4012

                                    <input type="hidden" name="partnumber[]" id="partnumber_47" value="SPE-4012">
                                </td>
                                <td class="productDescription">
                                    <b> Paper: Epson 3"x 3"x 67', 3 Ply </b>
                                    &nbsp;<br>Epson 3" 3 Ply Printer Paper 50 Rolls Per Case




                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_47"><b> Paper: Epson 3"x 3"x 67', 3 Ply </b>
                &nbsp;<br>Epson 3" 3 Ply Printer Paper 50 Rolls Per Case</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_47" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(47);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_47" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_47" value="72">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    7379
                                    <input type="hidden" name="partnumber[]" id="partnumber_48" value="7379">
                                </td>
                                <td class="productDescription">
                                    <b> Paper: Receipt Printer Paper 3"x90', White/Canary </b>
                                    &nbsp;<br>3" x 90' 7/16 2 Ply CB/CF (White/Canary) Case of 50




                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_48"><b> Paper: Receipt Printer Paper 3"x90', White/Canary </b>
                &nbsp;<br>3" x 90' 7/16 2 Ply CB/CF (White/Canary) Case of 50</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_48" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(48);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_48" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_48" value="90">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    10011043
                                    <input type="hidden" name="partnumber[]" id="partnumber_49" value="10011043">
                                </td>
                                <td class="productDescription">
                                    <b> Paper: Receipt Printer Paper, Thermal, 2" x 55' </b>
                                    &nbsp;<br>Zebra Z-Select 4000D 3.2 mil Receipt Paper - Bright White - 36 Rolls (2 in
                                    x
                                    55 ft)




                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_49"> <b> Paper: Receipt Printer Paper, Thermal, 2" x 55' </b>
                &nbsp;<br>Zebra Z-Select 4000D 3.2 mil Receipt Paper - Bright White - 36 Rolls (2 in x 55 ft)</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_49" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(49);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_49" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_49" value="40">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    SPE-1213
                                    <input type="hidden" name="partnumber[]" id="partnumber_50" value="SPE-1213">
                                </td>
                                <td class="productDescription">






                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_50"><b> Paper: Receipt Printer Paper, Thermal, 3 1/8" x 220', 1 Ply, 50 Rolls Per Case </b>
                                &nbsp;
                                <br>
                                <div class="tableDiv">
                                    Volume
                                </div>
                                <div class="tableDiv">
                                    Price Per Unit
                                </div>
                                <div class="breakclear"></div>
                                <div class="tableDiv">
                                    1 - 24 cases
                                </div>
                                <div class="tableDiv">
                                    $77.00 per case
                                </div>
                                <div class="breakclear"></div>
                                <div class="tableDiv">
                                    25 - 49 cases
                                </div>
                                <div class="tableDiv">
                                    $71.00 per case
                                </div>
                                <div class="breakclear"></div>
                                <div class="tableDiv">
                                    50 - 99 cases
                                </div>
                                <div class="tableDiv">
                                    $68.00 per case
                                </div>
                                <div class="breakclear"></div>
                                <div class="tableDiv">
                                    100 + cases
                                </div>
                                <div class="tableDiv">
                                    $66.00 per case
                                </div>
                                <div class="breakclear"></div> </textarea>



                                    <b> Paper: Receipt Printer Paper, Thermal, 3 1/8" x 220', 1 Ply, 50 Rolls Per Case
                                    </b>
                                    &nbsp;
                                    <br>
                                    <div class="tableDiv">
                                        Volume
                                    </div>
                                    <div class="tableDiv">
                                        Price Per Unit
                                    </div>
                                    <div class="breakclear"></div>
                                    <div class="tableDiv">
                                        1 - 24 cases
                                    </div>
                                    <div class="tableDiv">
                                        $77.00 per case
                                    </div>
                                    <div class="breakclear"></div>
                                    <div class="tableDiv">
                                        25 - 49 cases
                                    </div>
                                    <div class="tableDiv">
                                        $71.00 per case
                                    </div>
                                    <div class="breakclear"></div>
                                    <div class="tableDiv">
                                        50 - 99 cases
                                    </div>
                                    <div class="tableDiv">
                                        $68.00 per case
                                    </div>
                                    <div class="breakclear"></div>
                                    <div class="tableDiv">
                                        100 + cases
                                    </div>
                                    <div class="tableDiv">
                                        $66.00 per case
                                    </div>
                                    <div class="breakclear"></div>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_50" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(50);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_50" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_50" value="135">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    SPE-120601
                                    <input type="hidden" name="partnumber[]" id="partnumber_51" value="SPE-120601">
                                </td>
                                <td class="productDescription">
                                    <b> Paper: Thermal Printer Paper, 2 1/4" x 80' </b>
                                    &nbsp;<br> 2 1/4" x 80' Thermal Paper, 48 Rolls Per Case



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_51"><b> Paper: Thermal Printer Paper, 2 1/4" x 80' </b>
                                &nbsp;<br> 2 1/4" x 80' Thermal Paper, 48 Rolls Per Case</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_51" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(51);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_51" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_51" value="91">
                                </td>
                            </tr>
                            <tr class="productsectiontitle">
                                <td class="category" colspan="4">
                                    <center><b class="dinProStd blackText">Power </b></center>
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    BK350
                                    <input type="hidden" name="partnumber[]" id="partnumber_52" value="BK350">
                                </td>
                                <td class="productDescription">
                                    <b> BK350 APC Back-UPS </b>
                                    &nbsp;<br>APC Back-UPS CS 350 - UPS - AC 120 V - 210 Watt - 350 VA - 6 output
                                    connector(s)



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_52">  <b> BK350 APC Back-UPS </b>
                                &nbsp;<br>APC Back-UPS CS 350 - UPS - AC 120 V - 210 Watt - 350 VA - 6 output
                                connector(s)</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_52" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(52);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_52" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_52" value="65">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    IN02040008
                                    <input type="hidden" name="partnumber[]" id="partnumber_53" value="IN02040008">
                                </td>
                                <td class="productDescription">
                                    <b> Series 3 Tablet Battery 10.1" Spare battery </b>
                                    &nbsp;<br>Series 3 Tablet Battery 10.1" Spare




                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_53"><b> Series 3 Tablet Battery 10.1" Spare battery </b>
                                &nbsp;<br>Series 3 Tablet Battery 10.1" Spare</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_53" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(53);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_53" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_53" value="10">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    IN02040007
                                    <input type="hidden" name="partnumber[]" id="partnumber_54" value="IN02040007">
                                </td>
                                <td class="productDescription">
                                    <b> Series 3 Tablet Battery 8.3" Spare battery </b>
                                    &nbsp;<br>Series 3 Tablet Battery 8.3" Spare battery



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_54"><b> Series 3 Tablet Battery 8.3" Spare battery </b>
                                &nbsp;<br>Series 3 Tablet Battery 8.3" Spare battery</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_54" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(54);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_54" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_54" value="10">
                                </td>
                            </tr>
                            <tr class="productsectiontitle">
                                <td class="category" colspan="4">
                                    <center><b class="dinProStd blackText">Ribbon </b></center>
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    4017502
                                    <input type="hidden" name="partnumber[]" id="partnumber_55" value="4017502">
                                </td>
                                <td class="productDescription">
                                    <b> Printer Ribbon: Black </b>
                                    &nbsp;<br>Ribbon TM-200/300 Black, 6 Per Pack
                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_55"> <b> Printer Ribbon: Black </b>
                                &nbsp;<br>Ribbon TM-200/300 Black, 6 Per Pack</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_55" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(55);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_55" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_55" value="50">
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    4017521
                                    <input type="hidden" name="partnumber[]" id="partnumber_56" value="4017521">
                                </td>
                                <td class="productDescription">
                                    <b> Printer Ribbon: Red/Black </b>
                                    &nbsp;<br>Nylon Printer Ribbon Cartridge - Red/Black - 6 Per Pack



                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_56"><b> Printer Ribbon: Red/Black </b>
                                &nbsp;<br>Nylon Printer Ribbon Cartridge - Red/Black - 6 Per Pack</textarea>
                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_56" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(56);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" disabled="" id="productCost_56" name="cost[]"
                                        size="5" type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_56" value="20">
                                </td>
                            </tr>
                            <tr class="productsectiontitle">
                                <td class="category" colspan="4">
                                    <center><b class="dinProStd blackText">Uncategorized </b></center>
                                </td>
                            </tr>
                            <tr class="productsection">
                                <td class="productPartNumber">
                                    ACC-CTG8000
                                    <input type="hidden" name="partnumber[]" id="partnumber_57" value="ACC-CTG8000">
                                </td>
                                <td class="productDescription">
                                    <b> C2G 7-Port USB 2.0 Aluminum Hub </b>
                                    &nbsp;<br>C2G 7-Port Powered USB Hub for POS terminals rovides additional USB ports
                                    for
                                    peripherals




                                    <textarea style="display:none;" name="productDescription[]"
                                        id="productDescription_57"><b> C2G 7-Port USB 2.0 Aluminum Hub </b>
                                &nbsp;<br>C2G 7-Port Powered USB Hub for POS terminals rovides additional USB ports for
                                peripherals</textarea>

                                </td>
                                <td class="productQuantity" nowrap="1">
                                    #<input class="productQuantityInput" id="productQuantity_57" name="qty[]" size="5"
                                        type="text" value="0" onkeyup="calculate_costs(57);">
                                </td>
                                <td class="productCost" nowrap="1">
                                    $<input class="productCostInput" id="productCost_57" name="cost[]" size="5"
                                        type="text" value="0">
                                    <input type="hidden" name="unitcost[]" id="unitCost_57" value="20">
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="supplyOrder center">
                        <h2 class="greenText dinProStd">Supply Order Request Form</h2>
                        <p class="blackText"><b>Order Total</b></p>
                        $<input class="supplyOrderInput" id="order_total" name="order_total" type="text" value="0">
                    </div>
                </div>
            </div>

            <div class="supplyBG orangeBG">
                <p class="whiteText center">Continue to Checkout to confirm your order and select shipping method.</p>
                <div class="lmsFormLabel flex">
                    <div class="lmsLabelBox flex">
                        <input type="submit" id="sup_Clear" name="" value="Clear" />
                    </div>
                    <div class="lmsLabelBox flex">
                        <input type="submit" id="sup_CheckOut" name="sup_CheckOut" value="Checkout" />
                    </div>
                </div>
            </div>
        </form>
    </section>
</div>
<script>
function calculate_costs(val) {


    var totalcost = 0;
    for (i = 1; i <= 57; i++) {


        if (i == 43) {

            var qty = jQuery('#productQuantity_' + i).val();


            if (qty > 0 && qty < 11) {
                var unitcost = jQuery('#unitCost_' + i + 'a').val();
            } else {
                var unitcost = jQuery('#unitCost_' + i + 'b').val();
            }

            var price = parseFloat(unitcost) * qty;

            jQuery('#productCost_' + i).val(price);

            if (!isNaN(price)) {
                totalcost = parseFloat(totalcost) + parseFloat(price);
            }
        } else {
            var unitcost = jQuery('#unitCost_' + i).val();
            var qty = jQuery('#productQuantity_' + i).val();

            var price = parseFloat(unitcost) * qty;

            jQuery('#productCost_' + i).val(price);

            if (!isNaN(price)) {
                totalcost = parseFloat(totalcost) + parseFloat(price);
            }
        }

    }

    jQuery('#order_total').val(totalcost);
}




jQuery(document).ready(function($) {


    $.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z," "]+$/i.test(value);
    }, "Letters and spaces only please");


    $("form[name='registration']").validate({
        onfocusout: false,

        rules: {

            attention: {
                required: true,
                lettersonly: true,
            },
            company: {
                required: true,
                lettersonly: true,
            },
            order: {
                required: true,
                number: true,
            },
            phone: {
                required: true,
                number: true,
            },
            email: {
                required: true,
                email: true
            },
            fax: {
                required: true,
                number: true,
            },
            billing_address1: "required",
            billing_address2: "required",
            billing_city: {
                required: true,
                lettersonly: true,
            },
            billing_state: {
                required: true,
                lettersonly: true,
            },
            billing_zip: {
                required: true,
                number: true,
            },
            unit: "required",
            shipping_address1: "required",
            shipping_address2: "required",
            shipping_city: {
                required: true,
                lettersonly: true,
            },
            shipping_state: {
                required: true,
                lettersonly: true,
            },
            shipping_zip: {
                required: true,
                number: true,
            }

        },

        messages: {
            attention: {
                required: "Please enter Your Attention",
                lettersonly: "Please enter Letters only",
            },
            company: {
                required: "Please enter the Company Name",
                lettersonly: "Please enter Letters only",
            },
            order: {
                required: "Please enter the Order Number",
                number: "Please enter Numbers only",
            },
            phone: {
                required: "Please enter the Phone Number",
                number: "Please enter Numbers only",
            },
            email: "Please enter a valid email address",
            fax: {
                required: "Please enter the Fax Number",
                number: "Please enter Numbers only",
            },
            billing_address1: "Please enter  your Billing Address1",
            billing_address2: "Please enter  your Billing Address2",
            billing_city: {
                required: "Please enter the Billing City",
                lettersonly: "Please enter Letters only",
            },
            billing_state: {
                required: "Please enter the Billing State",
                lettersonly: "Please enter Letters only",
            },
            billing_zip: {
                required: "Please enter the Billing Zip",
                number: "Please enter Number only",
            },
            unit: "Please enter a Unit",
            shipping_address1: "Please enter a Shipping Address1",
            shipping_address2: "Please enter a Shipping Address2",
            shipping_city: {
                required: "Please enter the Shipping City",
                lettersonly: "Please enter Letters only",
            },
            shipping_state: {
                required: "Please enter the Shipping State",
                lettersonly: "Please enter Letters only",
            },
            shipping_zip: {
                required: "Please enter the Shipping Zip",
                number: "Please enter Letters only",
            },

        },

        submitHandler: function(form) {
            

            var cnt = 0;

            $(".productQuantityInput").each(function() {

                if ($(this).val() != 0) {
                    cnt++;
                }
                console.log($(this).val());
            });

            if (cnt != 0) {
                form.submit();
            } else {
                alert("Please add atleast one product");
            }

            return false;
        }

        
    });
});

function set_shipping_addr() {

    var supply_Billing_add1 = jQuery('#supply_Billing_add1').val();
    var supply_Billing_address2 = jQuery('#supply_Billing_address2').val();
    var supply_Billing_City = jQuery('#supply_Billing_City').val();
    var supply_Billing_State = jQuery('#supply_Billing_State').val();
    var supply_Billing_zip = jQuery('#supply_Billing_zip').val();

    jQuery('#supply_Add').val(supply_Billing_add1);
    jQuery('#supply_Add2').val(supply_Billing_address2);
    jQuery('#supply_Ship_City').val(supply_Billing_City);
    jQuery('#supply_Ship_State').val(supply_Billing_State);
    jQuery('#supply_Ship_Zip').val(supply_Billing_zip);


}


jQuery(document).ready(function() {

    jQuery('.productQuantityInput').keyup(function() {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    });
    
   
    
    
    jQuery('#sup_CheckOut').click(function(){
    // alert('Does this work?');
     jQuery('input').removeClass('error');
        });
});
</script>


<?php
echo $args['after_widget'];
    }

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {

    }

    public function form($display_instance)
    {

    }

}
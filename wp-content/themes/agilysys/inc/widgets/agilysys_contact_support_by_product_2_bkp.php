<?php

function agilysys_contact_support_by_product_2s()
{
    register_widget('agilysys_contact_support_by_product_2');
}
add_action('widgets_init', 'agilysys_contact_support_by_product_2s');

class agilysys_contact_support_by_product_2 extends WP_Widget
{

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Contact Support By Product Widget 2', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {

        $section_title = $instance['section_title'];
        $max_entries_agilysys_contact_support_by_product_2_slider_image = 15;

        ?>


<section class="contactSupportMap">
    <div class="contactMap flex">
        <div class="contactMapBox">
            <h4 class="dinProStd greenText"><?php echo $section_title; ?>™</h4>


            <?php

        $count = count($instance['title']);
        for ($i = 0; $i < $count; $i++) {

            //Caption
            $title = esc_attr($instance['title'][$i]);

            //Slider Image
            $mobile_number = $instance['mobile_number'][$i];
            ?>
            <h5 class="dinproBld blackText"><?php echo $title; ?></h5>
            <a href="mailto:<?php echo $mobile_number; ?>"><?php echo $mobile_number; ?></a>

            <?php

        }
        ?>

        </div>
    </div>

</section>


<?php

    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['section_title'] = strip_tags($new_instance['section_title']);
        $instance['what_makes_rows'] = $new_instance['what_makes_rows'];

        $max_entries_agilysys_contact_support_by_product_2_slider_image = 15;

        $count = count($new_instance['title']);

        for ($i = 0; $i < $count; $i++) {

            $instance['title'][$i] = strip_tags($new_instance['title'][$i]);
            $instance['mobile_number'][$i] = $new_instance['mobile_number'][$i];

        }

        return $instance;
    }

    public function form($display_instance)
    {

        $max_entries_agilysys_contact_support_by_product_2_slider_image = 15;

        $widget_add_id_slider = $this->get_field_id('') . "add_agilysys_contact_support_by_product_2";

        $section_title = ($display_instance['section_title']);

        $what_makes_rows = ($display_instance['what_makes_rows']);
        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('section_title') . '"> ' . __('Main Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('section_title') . '" name="' . $this->get_field_name('section_title') . '" type="text" value="' . $section_title . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('what_makes_rows') . '"> ' . __('No. of rows do you want to add*', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input class="what_makes_rows" id="' . $this->get_field_name('what_makes_rows') . '" name="' . $this->get_field_name('what_makes_rows') . '" type="number" value="' . $what_makes_rows . '" />';
        $rew_html .= '</p>';

        $count = count($display_instance['title']);

        $rew_html .= '<div class="add_new_rowxx-input-containers"><div id="entries_agilysys_contact_support_by_product_2">';

        $rew_html .= '<input class="cnt909" id="cnt909" name="cnt" type="hidden" value="' . $count . '">';

        for ($i = 0; $i < $count; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'AGILYSYS_TEXT_DOMAIN') . ' </span>';

            $rew_html .= '<div class="entry-desc cf">';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('title' . $i) . '"> ' . __('Country Name', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('title' . $i) . '" name="' . $this->get_field_name('title[]') . '" type="text" value="' . $display_instance['title'][$i] . '">';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('mobile_number' . $i) . '"> ' . __('Mobile Number', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('mobile_number' . $i) . '" name="' . $this->get_field_name('mobile_number[]') . '" type="text" value="' . $display_instance['mobile_number'][$i] . '">';
            $rew_html .= '</p>';

            $rew_html .= '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' . $k . ');">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';

        }
        $rew_html .= '</div></div>';
        $rew_html .= '<div id="message">' . __('Sorry, you reached to the limit of', 'AGILYSYS_TEXT_DOMAIN') . ' "' . $what_makes_rows . '" ' . __('maximum entries_agilysys_contact_support_by_product_2', 'AGILYSYS_TEXT_DOMAIN') . '.</div>';

        $rew_html .= '<div class="' . $widget_add_id_slider . '" style="text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;" onClick="add_new_row();">' . __('ADD ROW', 'AGILYSYS_TEXT_DOMAIN') . '</div>';
        ?>


<script>
function add_new_row() {
    var cnt = '';

    jQuery.each(jQuery("#entries_agilysys_contact_support_by_product_2 .cnt909"), function() {
        if (jQuery(this).val() != '') {
            cnt = jQuery(this).val();
        }
    });




    var what_makes_rows = jQuery('.what_makes_rows').val();


console.log(cnt);


    if (parseInt(cnt) < parseInt(what_makes_rows)) {


        cnt++;

        jQuery.each(jQuery("#entries_agilysys_contact_support_by_product_2 .cnt909"), function() {
            if (jQuery(this).val() != '') {
                jQuery(this).val(cnt);
            }
        });

        var new_row = '<div id="entry' + cnt +
            '"  class="entrys"><span class="entry-title" onclick = "slider(this);"><?php echo __('Add New Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span>';
        new_row += '<div class="entry-desc cf">';


        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Title', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('title[]')); ?>" type="text" value="">';
        new_row += '</p>';


        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Mobile Number', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';



        new_row +=
            '<input class="" name="<?php echo esc_attr($this->get_field_name('mobile_number[]')); ?>" type="text" value="">';
        new_row += '</p>';



        var new_cnt = cnt;

        new_row +=
            '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' + new_cnt +
            ');"><?php echo __('Delete Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span></a></p>';
        new_row += '</div></div>';

        jQuery('.add_new_rowxx-input-containers #entries_agilysys_contact_support_by_product_2').append(new_row);

    }

}


function delete_row(cnt) {

    jQuery.each(jQuery(".add_new_rowxx-input-containers #entries_agilysys_contact_support_by_product_2"), function() {
        jQuery(' #entry' + cnt).remove();
    });
    var last_cnt = 0;

    jQuery.each(jQuery("#entries_agilysys_contact_support_by_product_2 .cnt909"), function() {
        if (jQuery(this).val() != '') {
            last_cnt = jQuery(this).val();
        }
    });

    last_cnt--;
    jQuery.each(jQuery("#entries_agilysys_contact_support_by_product_2 .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(last_cnt);
        }
    });
    jQuery(".what_makes_rows").val(last_cnt);
    jQuery('.what_makes_rows').trigger('change');


}
</script>
<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_contact_support_by_product_2 p {
  padding:10px !important;
}

#rew_container_agilysys_contact_support_by_product_2  select{
     float: left;
    width: 60%;
    margin-top:20px !important;
    margin-bottom:10px !important;
}

#rew_container_agilysys_contact_support_by_product_2 input,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_contact_support_by_product_2 label {
    width: 40%;
     float: left;
}

<?php echo '.'. $widget_add_id_slider;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}



#entries_agilysys_contact_support_by_product_2 {
    padding: 10px 0 0;
}

#entries_agilysys_contact_support_by_product_2 .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_contact_support_by_product_2 .entrys:first-child {
    margin: 0;
}

#entries_agilysys_contact_support_by_product_2 .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_contact_support_by_product_2 .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_contact_support_by_product_2 .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_contact_support_by_product_2 .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_contact_support_by_product_2 .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_contact_support_by_product_2 #entries_agilysys_contact_support_by_product_2 p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_contact_support_by_product_2">
    <?php echo $rew_html; ?>
</div>




<?php

    }

}
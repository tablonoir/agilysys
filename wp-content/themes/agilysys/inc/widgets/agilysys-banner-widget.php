<?php

defined('ABSPATH') or die('No script kiddies please!');

if (!defined('ABSPATH')) {
    exit;
}

function agilysys_banner_widget()
{
    register_widget('agilysys_banner_with_socl_widget');
}
add_action('widgets_init', 'agilysys_banner_widget');

class agilysys_banner_with_socl_widget extends WP_Widget
{

    /**
     * Register widget with WordPress.
     */
    public function __construct()
    {
        parent::__construct(
            'agilysys_banner_with_socl_widget', // Base ID
            __('Agilysys Banner Widget', 'agividwid'), // Name
            array('description' => __('Agilysys Banner Widget', 'agividwid')) // Args
        );

        wp_register_script('add-sd-jsd', get_template_directory_uri() . '/inc/widgets/repeater-entries-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-jsd');
        wp_enqueue_media();
    }

    public function sisw_image_slider_enqueue_script()
    { //Enqueue sscript on widget page
        global $pagenow;
        if ($pagenow == 'widgets.php' || $pagenow == 'customize.php') {
            wp_enqueue_media();

            wp_enqueue_script('jquery-ui-core');

            wp_register_script('sisw-admin-script', get_template_directory_uri('/', __FILE__) . '/inc/widgets/repeater-entries-widget.js', array("jquery"));
            wp_localize_script('sisw-admin-script', 'sisw_admindata', $sisw_translation_array);
            wp_enqueue_script('sisw-admin-script');
        }
    }
    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget($args, $instance)
    {


        
        $nav_menu = !empty($instance['nav_menu']) ? wp_get_nav_menu_object($instance['nav_menu']) : false;

        echo $args['before_widget'];
        if (!empty($instance['title'])) {
            echo $args['before_title'] . apply_filters('widget_title', $instance['title']) . $args['after_title'];
        }
        extract($instance);
        $extra_attr = '';
        $shortcode = '';
        $poster = empty($poster) ? '' : $poster;
        $autoplay = $autoplay;
        $loop = ($loop == 1) ? 'on' : 'off';
        $preload = ($preload == 1) ? 'auto' : 'metadata';
        $height = empty($height) ? '' : $height;
        $width = empty($width) ? '' : $width;

        extract($instance);

        $count_images = 0;

        if (!empty($image_url)) {
            foreach ($image_url as $available_image_url) {
                if (!empty($available_image_url)) {
                    $count_images++;
                }
            }
        }

        $targetval = (isset($instance['target_tab'])) ? $instance['target_tab'] : "";

        $linkval = (isset($instance['image_link'])) ? $instance['image_link'] : "";

        $menu_title = (isset($instance['menu_title'])) ? $instance['menu_title'] : "";

        $banner_title = (isset($instance['banner_title'])) ? $instance['banner_title'] : "";


       
        

        $banner_desc = (isset($instance['banner_desc'])) ? $instance['banner_desc'] : "";

       

        //$banner_url = (isset($instance['banner_url'])) ? $instance['banner_url'] : "";

        $banner_url_title = (isset($instance['banner_url_title'])) ? $instance['banner_url_title'] : "";

        $count = 0;
        if ($count_images > 1) {

        }

        $link_type = $instance['link_type'];
        if ($link_type == "link") {
            $link = $instance['link'];
        } elseif ($link_type == "page") {
            $post_id = $instance['page'];
            $post = get_post($post_id);
            $link = home_url($post->post_name) . "/";
        }

        ?>

<section class="agilysysBanner" id="agilysysBanner1">

    <div class="row agilysysBannerRow">
        <div class="agilysysBannerSocialMedia <?php echo $args['widget_id']; ?> ">
            <div class="social-menu-icons flex">
                <a href="<?php echo get_theme_mod('facebook_link'); ?>"><img
                        src="<?php echo get_theme_mod('facebook_image_settings'); ?>" class="img-fluid" alt="fb"></a>
                <a href="<?php echo get_theme_mod('twitter_link'); ?>"><img
                        src="<?php echo get_theme_mod('twitter_image_settings'); ?>" class="img-fluid"
                        alt="twitter"></a>
                <a href="<?php echo get_theme_mod('linkedin_link'); ?>"><img
                        src="<?php echo get_theme_mod('linkedin_image_settings'); ?>" class="img-fluid"
                        alt="linked"></a>
                <a href="<?php echo get_theme_mod('instagram_link'); ?>"><img
                        src="<?php echo get_theme_mod('instagram_image_settings'); ?>" class="img-fluid"
                        alt="instagram"></a>
                <a href="<?php echo get_theme_mod('pinterest_link'); ?>"><img
                        src="<?php echo get_theme_mod('pinterest_image_settings'); ?>" class="img-fluid"
                        alt="volly"></a>

                <div class="bannerIconText">
                    <p><?php echo get_theme_mod('follow_us_text'); ?></p>
                </div>
            </div>

        </div>


        <?php

        if (!$nav_menu) {
            return;
        }
        if ($loop == 'on') {
            $extra_attr .= ' loop="on" ';
        }
        if ($autoplay == 1) {
            $extra_attr .= ' autoplay="1" ';
        }
        if ($preload == 'metadata') {
            $extra_attr .= ' preload="metadata" ';
        }

        $format = current_theme_supports('html5', 'navigation-widgets') ? 'html5' : 'xhtml';

        $format = apply_filters('navigation_widgets_format', $format);

        if ('html5' === $format) {
            // The title may be filtered: Strip out HTML and make sure the aria-label is never empty.
            $title = trim(strip_tags($title));
            $aria_label = $title ? $title : $default_title;

            $nav_menu_args = array(
                'fallback_cb' => '',
                'menu' => $nav_menu,
                'container' => 'nav',
                'container_aria_label' => $aria_label,
                'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            );
        } else {
            $nav_menu_args = array(
                'fallback_cb' => '',
                'menu' => $nav_menu,
            );
        }

        ?>
        <div class="homeBannerMenu">
            <h4 class="dinProStd whiteText greenBG" id="menu_title"><?php echo $menu_title; ?></h4>
            <ul class="dinproBld blackText">
                <?php
wp_nav_menu(apply_filters('widget_nav_menu_args', $nav_menu_args, $nav_menu, $args, $instance));
        ?>
            </ul>
        </div>
        
               <script>

jQuery(document).ready(function(){

    setTimeout(function(){
jQuery("#menu-industries-select option").each(function () {
   
    if (jQuery(this).val() === window.location.toString()) {
        jQuery(this).prop('selected', true);
        jQuery('#menu_title').html(jQuery(this).text());
    }
});
}, 100);
});
</script>

        <div class="agilysysBannerNavMenu">
            <div class="mobileMenu panel-widget-style panel-widget-style-for-gb1223-5f83f4838d88e-0-1-0" id="menuTitle">
                <h3 class="widget-title"><?php echo $menu_title; ?></h3>
                <ul class="menu-industries-container">
                    <ul id="menu-industries" class="menu">

                        <?php
wp_nav_menu(apply_filters('widget_nav_menu_args', $nav_menu_args, $nav_menu, $args, $instance));
        ?>
                </ul></ul>
            </div>
            <section class="homeContact">
                <div class="homeContactText">
                  
                    <h4 class="dinProStd greenText"><?php echo get_theme_mod('contact_us_text'); ?></h4>
                    <p class="blackText"><img class="img-fluid headerIcoimg" src="<?php echo get_template_directory_uri(); ?>/img/phone-icon-1.png"><span class="homeContactText"><?php echo get_theme_mod('mobile_number'); ?></span></p>
                </div>
                <div class="homeGetDemo dinproMed">
                    <a class="greenText"
                        href="<?php echo get_theme_mod('get_a_demo_link'); ?>"><?php echo get_theme_mod('get_a_demo_text'); ?>
                        <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                </div>
            </section>
        </div>
        <?php if ($type == 'image') {
            ?>
        <div class="homeBannerVideo"
            style="background:url(<?php echo clean_url_agilysys($src); ?>);background-repeat:no-repeat; background-size: cover;   background-position: center;">
            <div class="homeBannerText whiteText agilysysBannerImg">
                <?php if ($banner_desc != "") {?>
                <h2 class=" dinProStd "> <?php echo $banner_title; ?></h2>
                <?php } else {?>
                <h2 class="dinProStd bannerTitleonly titleWithouLink "> <?php echo $banner_title; ?></h2>
                <?php }
            ?>
                <?php if ($banner_desc != "") {?>
                <p class="dinproMed"><?php echo substr($banner_desc,0,250); ?></p>
                <?php }?>
                <?php if ($link_type != "") {?>
                <a class="homeBannerButton whiteText" href="<?php echo $link; ?>">
                    <?php echo $banner_url_title; ?> <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                <?php }?>
            </div>
            <?php
}?>
            <?php
if ($type == 'video') {
            ?>
            <div class=" whiteText agilysysBannerImgVid">
                <div class="overlay"></div>
                <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
                    <source src="<?php echo clean_url_agilysys($src); ?>" type="video/mp4">
                </video>
                <div class="container h-100">
                    <div class="d-flex  align-items-center">
                        <div class=" homeBannerText text-white">
                            <h2 class="dinProStd"> <?php echo $banner_title; ?></h2>

                            <?php if ($banner_desc != "" ) {?>
                            <p class="dinproMed"><?php echo substr($banner_desc,0,250); ?></p>
                            <?php }?>
                            <?php if ($link_type != "") {?>
                            <a class="homeBannerButton whiteText" href="<?php echo $link; ?>">
                                <?php echo $banner_url_title; ?> <i class="fa fa-arrow-right"
                                    aria-hidden="true"></i></a>
                            <?php }?>
                        </div>
                    </div>
                </div>
                <?php
}
        ?>


                <?php
if ($type == 'youtube') {
            ?>
                <div class=" whiteText agilysysBannerImgVid">
                    <div class="overlay"></div>


                    <iframe width="100%" height="664" src="<?php echo $src; ?>?autoplay=1&mute=1" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                    <div class="container h-100">
                        <div class="d-flex  align-items-center"
                            style="position: absolute !important;z-index: 2 !important;left: 5% !important;top: 0 !important;">
                            <div class=" homeBannerText text-white">
                                <h2 class="dinProStd"> <?php echo $banner_title; ?></h2>

                                <?php if ($banner_desc != "") {?>
                                <p class="dinproMed"><?php echo $banner_desc; ?></p>
                                <?php }?>
                                <?php if ($link_type != "") {?>
                                <a class="homeBannerButton whiteText" href="<?php echo $link; ?>">
                                    <?php echo $banner_url_title; ?> <i class="fa fa-arrow-right"
                                        aria-hidden="true"></i></a>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                    <?php
}
        ?>
                </div>
            </div>
        </div>
</section>
<?php
echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form($instance)
    {
        global $wp_customize;
        $title = !empty($instance['title']) ? $instance['title'] : __('Featured Video ', 'vidget');
        $src = !empty($instance['src']) ? $instance['src'] : '';
//                print_r($src);
        $type = !empty($instance['type']) ? $instance['type'] : 'video';
        $poster = !empty($instance['poster']) ? $instance['poster'] : '';
        $loop = !empty($instance['loop']) ? $instance['loop'] : 'off'; // on - off
        $preload = !empty($instance['preload']) ? $instance['preload'] : '0'; // 1 - 0
        $autoplay = !empty($instance['autoplay']) ? $instance['autoplay'] : 0; //0 - 1

        $nav_menu = isset($instance['nav_menu']) ? $instance['nav_menu'] : '';

        $menu_title = !empty($instance['menu_title']) ? $instance['menu_title'] : '';

        $banner_title = !empty($instance['banner_title']) ? $instance['banner_title'] : '';

        $banner_desc = !empty($instance['banner_desc']) ? $instance['banner_desc'] : '';

        $banner_url = !empty($instance['banner_url']) ? $instance['banner_url'] : '';

        $banner_url_title = !empty($instance['banner_url_title']) ? $instance['banner_url_title'] : '';

        $icon_title = !empty($instance['icon_title']) ? $instance['icon_title'] : '';

     


        wp_enqueue_media();

        // Get menus.
        $menus = wp_get_nav_menus();

        $empty_menus_style = '';
        $not_empty_menus_style = '';
        if (empty($menus)) {
            $empty_menus_style = ' style="display:none" ';
        } else {
            $not_empty_menus_style = ' style="display:none" ';
        }

        $nav_menu_style = '';
        if (!$nav_menu) {
            $nav_menu_style = 'display: none;';
        }

        $defaults = array(
            'image_url' => array(),
            'slider_title' => 'Slider',
            'nav_option' => 'true',
            'autoslide_option' => 'true',
            'slider_pause_option' => '5',
            'target_tab' => array(''),
            'image_link' => array(''),

        );
        if ($instance) {
            extract($instance);
        } else {
            extract($defaults);
        }

        ?>
<style type="text/css">
.vidget_option_toggle_controller {
    cursor: pointer;
    display: block;
    text-align: center;
    margin: 10px !important;
}

.vidget_option_toggle {
    display: block;
    border: 1px solid #eee;
    padding: 10px;
    background: #fefefe;
}

.vidget_widget_wrapper label {
    min-width: 110px;
    display: inline-block;
}

.img-fluid {
    width: 100%;
    height: auto;
}

.flex {
    display: flex;
}

.video-widget-dropdown {}

.widget-content a.add_field_upload_media_widget {
    font-size: 20px;
    text-decoration: none;
    margin-left: 5px;
    background: #0073aa;
    color: #fff;
    padding: 0px 5px;
    line-height: 20px;
    vertical-align: sub;
}

.widget-content a.sisw_upload_image_media {
    font-size: 12px;
    text-decoration: none;
    margin-left: 5px;
    background: #0073aa;
    color: #fff;
    padding: 5px 9px;
    line-height: 20px;
    vertical-align: initial;
    font-weight: 600;
    text-transform: uppercase;
}

h4.sisw_heading-section {
    text-align: center;
    margin: 10px 0 15px;
    background: rgb(245, 245, 245);
    color: #0073aa;
    padding: 5px 0;
    font-size: 14px;
    font-weight: 500;
}

.sisw_admin_image_preview {
    /*display: none;*/
    width: 25px;
    height: 25px;
    vertical-align: top;
}

.sisw_admin_image_preview.active {
    display: inline-block;
}

.sisw_individual_image_section input.sisw_image_input_field {
    width: 100%;
}

span.sisw_drag_Section {
    cursor: move;
}

.widget-content a.sisw_remove_field_upload_media_widget {
    font-size: 20px;
    text-decoration: none;
    margin-left: 5px;
    background: rgb(255, 0, 0);
    color: #fff;
    padding: 0px 5px;
    line-height: 20px;
    vertical-align: sub;
}

table.sisw_multi_image_slider_table_wrapper {
    padding-bottom: 6px;
    border-bottom: 1px solid #ccc;
    margin-bottom: 9px;
    margin-top: 9px;
    width: 100%;

}

table.sisw_multi_image_slider_table_wrapper tr {
    width: 100%;
}

.sisw_slider_options_section {
    display: block;
    width: 100%;
    padding: 10px 0 0;
}

.sisw_slider_options_section label {
    font-size: 13px;
    text-transform: capitalize;
    display: inline-block;
    min-width: 69px;
    font-weight: 500;
}

.sisw_slider_options_section input[type="radio"] {
    vertical-align: middle;
    display: inline-block;
    margin: 0 5px;
}

.sisw_slider_options_section select {
    width: 100%;
    margin-top: 6px;
}

.sisw_multi_image_slider_setting {
    background: #f4f4f4;
    padding: 15px;
    margin: 15px 0;
}

.sisw_multi_image_slider_setting h4 {
    text-align: center;
    text-transform: uppercase;
    margin: 0 !important;
}

.sisw_multi_image_slider_table_wrapper .sisw_individual_image_section span.drag_Section {
    vertical-align: initial;
    padding-right: 6px;
    cursor: pointer;
}

html .widget-content .sisw_remove_field_upload_media_widget {
    margin: 0;
    display: block;
    text-align: center;
    padding: 17px 5px;
}

.sisw_individual_image_section {
    background-color: #eee;
}


div#sisw_single_image_wrapper {
    width: 100%;
    display: block;
    text-align: center;
}

div#sisw_single_image_wrapper img {
    width: 100%;
}

.clearfix:after {
    visibility: hidden;
    display: block;
    font-size: 0;
    content: " ";
    clear: both;
    height: 0;
}

* html .clearfix {
    zoom: 1;
}

/* IE6 */
*:first-child+html .clearfix {
    zoom: 1;
}

/* IE7 */
/*Slider css*/
#jssor_1 {
    position: relative;
    margin: 0 auto;
    top: 0px;
    left: 0px;
    width: 980px;
    height: 380px;
    overflow: hidden;
    visibility: hidden;
}

#jssor_1 .jssor_1_wrapper {
    cursor: default;
    position: relative;
    top: 0px;
    left: 0px;
    width: 980px;
    height: 380px;
    overflow: hidden;
}

h1.slider_widget_title {
    font-size: 24px;
    font-weight: 600;
    text-transform: capitalize;
    text-align: center;
}

.owl-item {
    display: inline-block;
}

.sisw_multi_image_slider_wrapper {
    overflow: hidden;
    text-align: center;
    width: 100%;
    position: relative;
}

.sisw_multi_image_slider_wrapper .owl-item,
.sisw_multi_image_slider_wrapper .item {
    margin: 0;
    padding: 0;
    text-align: center;
}

.sisw_multi_image_slider_wrapper .item {
    display: none;
}

.sisw_multi_image_slider_wrapper .item:first-child {
    display: block;
}

.sisw_multi_image_slider_wrapper a,
.sisw_multi_image_slider_wrapper a:focus {
    display: block;
    margin: 0;
    padding: 0;
    outline: 0;
}

.sisw_multi_image_slider_wrapper a img {
    display: inline-block;
    margin: 0;
    padding: 0;
}

.sisw_multi_image_slider_wrapper .owl-nav {
    position: absolute;
    top: 50%;
    width: 100%;
    margin-top: -23px !important;
}

.sisw_multi_image_slider_wrapper .owl-nav button.owl-next {
    position: absolute;
    right: 0;
    background: rgba(0, 0, 0, 0.4);
    font-size: 32px;
    line-height: 32px;
    padding: 0;
    outline: 0;
    height: 38px;
    width: 28px;
}

.sisw_multi_image_slider_wrapper .owl-nav button.owl-next:hover {
    background: rgba(0, 0, 0, 0.4);
}

.sisw_multi_image_slider_wrapper .owl-nav button span {
    font-size: 32px;
    font-weight: 400;
    line-height: 32px;
    text-align: center;
    display: block;
    width: 30px;
    height: 100%;
}

.sisw_multi_image_slider_wrapper .owl-nav button.owl-prev {
    position: absolute;
    left: 0;
    background: rgba(0, 0, 0, 0.4);
    font-size: 32px;
    line-height: 32px;
    padding: 0;
    outline: 0;
    height: 38px;
    width: 28px;
}

.sisw_multi_image_slider_wrapper .owl-nav button.owl-prev:hover {
    background: rgba(0, 0, 0, 0.4);
}

.sisw_multi_image_slider_wrapper .owl-nav.disabled {
    display: none;
}

.urlOptions select {
    width: 100px
}

.agilysys-banner-widget-title .widget-title-lable {
    float: left;
    width: 40%;

}

.agilysys-banner-widget-title .widget-title-input-text {

    float: right;
    width: 60%;
}

.menu-select-nav{
    float:left !important;
    
}

.menu-select-nav select{
    width:100% !important;
}

.vidget_upload_btn{
    margin-top:10px !important;
}

</style>
<script type="text/javascript">

function isImage(file) {
    return (file['type'].split('/')[0] == 'image'); //returns true or false
}

jQuery(document).ready(function($) {
    $('.vidget_option_toggle_controller').on('click', function() {
        var toToggle = $(this).siblings('.vidget_option_toggle').eq(0);
        toToggle.fadeIn();
    });
    if ($('.vidget_upload_btn').length > 0) {
        if (typeof wp !== 'undefined' && wp.media && wp.media.editor) {
            $(document).on('click', '.vidget_upload_btn', function(e) {
                e.preventDefault();
                var button = $(this);
                var id = button.prev();
                wp.media.editor.send.attachment = function(props, attachment) {

                    if (isImage(attachment)) {
                        if (attachment.height == 750 && attachment.width == 1920) {
                            id.val(attachment.url);
                            jQuery('#image_uri_agilysys_banner_with_socl_widget').html("");
                        } else {
                            jQuery('#image_uri_agilysys_banner_with_socl_widget').html(
                                "Please Enter the correct Dimensions 1920x750").css('color',
                                'red');
                        }

                    } else {
                        id.val(attachment.url);
                    }

                    //                                            alert(id.val(attachment.url));
                };
                wp.media.editor.open(button);
                //                                        console.log(id.val(wp.media.editor.open(button)));
                return false;
            });
        }
    }


});
</script>
<div class="vidget_widget_wrapper" id="divcontainer">

    <p class="nav-menu-widget-no-menus-message" <?php echo $not_empty_menus_style; ?>>
        <?php
if ($wp_customize instanceof WP_Customize_Manager) {
            $url = 'javascript: wp.customize.panel( "nav_menus" ).focus();';
        } else {
            $url = admin_url('nav-menus.php');
        }

        /* translators: %s: URL to create a new menu. */
        printf(__('No menus have been created yet. <a href="%s">Create some</a>.'), esc_attr($url));
        ?>
    </p>

    <p>
    <div class="nav-menu-widget-form-controls" <?php echo $empty_menus_style; ?>>
        <div class="flex menu-select-class agilysys-banner-widget-title">
            <div class="menu-select-class-nav widget-title-lable">
                <label for="<?php echo $this->get_field_id('nav_menu'); ?>"><?php _e('Select Menu:');?></label>
            </div>

            <div class="menu-select-nav widget-title-input-text">
                <select id="<?php echo $this->get_field_id('nav_menu'); ?>"
                    name="<?php echo $this->get_field_name('nav_menu'); ?>">
                    <option value="0"><?php _e('&mdash; Select &mdash;');?></option>
                    <?php foreach ($menus as $menu): ?>
                    <option value="<?php echo esc_attr($menu->term_id); ?>"
                        <?php selected($nav_menu, $menu->term_id);?>>
                        <?php echo esc_html($menu->name); ?>
                    </option>
                    <?php endforeach;?>
                </select>
            </div>
        </div>
    </div>
    </p><br>

    <p>
    <div class="flex agilysys-banner-widget-title">
        <div class="widget-title-lable">
            <label
                for="<?php echo esc_attr($this->get_field_id('menu_title')); ?>"><?php esc_attr_e('Menu Title', 'agilysys');?>:</label>
        </div>

        <div class="widget-title-input-text">
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('menu_title')); ?>"
                name="<?php echo esc_attr($this->get_field_name('menu_title')); ?>" type="text"
                value="<?php echo esc_attr($menu_title); ?>">
        </div>

    </div>
    </p><br>

    <p>
    <div class="flex agilysys-banner-widget-title">
        <div class="widget-title-lable">
            <label
                for="<?php echo esc_attr($this->get_field_id('banner_title')); ?>"><?php esc_attr_e('Banner Title', 'agilysys');?>:</label>
        </div>

        <div class="widget-title-input-text">
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('banner_title')); ?>"
                name="<?php echo esc_attr($this->get_field_name('banner_title')); ?>" type="text"
                value="<?php echo esc_attr($banner_title); ?>">
        </div>

    </div>
    </p><br>




    <div class="flex agilysys-banner-widget-title" id="link_type_id_textarea">
        <div class="widget-title-lable">
            <label
                for="<?php echo esc_attr($this->get_field_id('banner_desc')); ?>"><?php esc_attr_e('Description', 'vidget');?>:</label>
        </div>

        <div class="widget-title-input-text">
            <textarea rows="6" cols="35" class="widefat"
                id="<?php echo esc_attr($this->get_field_id('banner_desc')); ?>"
                name="<?php echo esc_attr($this->get_field_name('banner_desc')); ?>">
                <?php echo trim($banner_desc); ?></textarea>
        </div>
<br><br><br><br><br><br><br><br>
    </div>











    <div id="link_type_id_p_agilysys_banner_widget">
        <p>
        <div class="flex agilysys-banner-widget-title">
            <div class="widget-title-lable">
                <label
                    for="<?php echo esc_attr($this->get_field_id('banner_url_title')); ?>"><?php esc_attr_e('Button Title', 'vidget');?>:</label>
            </div>

            <div class="widget-title-input-text">
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('banner_url_title')); ?>"
                    name="<?php echo esc_attr($this->get_field_name('banner_url_title')); ?>" type="text"
                    value="<?php echo esc_attr($banner_url_title); ?>">
            </div>

        </div>
        </p><br>




        <p>

        <div class="flex agilysys-banner-widget-title">
            <div class="widget-title-lable">
                <label
                    for="<?php echo $this->get_field_id('link_type');?>"><?php echo __('Link Type', 'AGILYSYS_TEXT_DOMAIN');?>
                    :</label>
            </div>
            <div class="widget-title-input-text">
                <select class="widefat" id="<?php echo $this->get_field_id('link_type');?>"
                    name="<?php echo $this->get_field_name('link_type');?>"
                    onChange="show_hide_div_agilysys_banner_widget(this.value);">
                    <option value="">Please Select</option>


                    <?php
$link_type = $display_instance['link_type'];
?>

                    <?php
if ($link_type == 'page') {
    ?>

                    <option value="page" selected="selected">Page</option>
                    <?php
        } else {
            ?>
                    <option value="page">Page</option>
                    <?php
        }
       
        if ($link_type == 'link') {
            ?>
                    <option value="link" selected="selected">Link</option>
                    <?php
        } else {
            ?>
                    <option value="link">Link</option>

                    <?php
        }
        ?>
                </select>
            </div>
        </div>

        </p>




        <?php
        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        if ($link_type == 'page') {
            $show1 = 'style="display:block"';
            $show2 = 'style="display:none"';
        } elseif ($link_type == 'link') {
            $show1 = 'style="display:none"';
            $show2 = 'style="display:block"';

        } else {
            $show1 = 'style="display:none"';
            $show2 = 'style="display:none"';
        }

?>
        <div id="page_div_agilysys_banner_widget" <?php echo $show1;?>>
            

            <div class="flex agilysys-banner-widget-title">

                <div class="widget-title-lable">
                    <label for="<?php echo $this->get_field_id('page');?>">
                        <?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN');?> :</label>
                </div>
                <div class="widget-title-input-text">
                    <select class="widefat" id="<?php echo $this->get_field_id('page');?>"
                        name="<?php echo $this->get_field_name('page');?>">
                        <option value="">Please Select</option>


                        <?php
        $page = $display_instance['page'];

       

        foreach ($pages as $key) {
          
            if ($page == $key->ID) {
?>
                        <option value="<?php echo $key->ID;?>" selected="selected"><?php echo $key->post_title;?>
                        </option>
                        <?php
            } else {
                ?>
                        <option value="<?php echo $key->ID;?>"><?php echo $key->post_title;?></option>

                        <?php
            }

        }
        ?>
                    </select>
                </div>
            </div>
            
        </div>

        <div id="link_div_agilysys_banner_widget" <?php echo $show2;?>>
            

            <div class="flex agilysys-banner-widget-title">
                <div class="widget-title-lable">
                    <label
                        for="<?php echo $this->get_field_id('link');?>"><?php echo __('Link', 'AGILYSYS_TEXT_DOMAIN');?>
                        :</label>
                </div>
                <div class="widget-title-input-text">
                    <input class="widefat" id="<?php echo $this->get_field_id('link');?>"
                        name="<?php echo $this->get_field_name('link');?>" type="text" value="<?php echo $link ;?>" />
                </div>

            </div>
            
        </div>

    </div>




<script>

function show_hide_div_agilysys_banner_widget(val) {

    if (val == 'page') {
        jQuery("#page_div_agilysys_banner_widget").show();
        jQuery("#link_div_agilysys_banner_widget").hide();
    } else if (val == 'link') {
        jQuery("#page_div_agilysys_banner_widget").hide();
        jQuery("#link_div_agilysys_banner_widget").show();
    }

}
</script>

<br>
<p>
<div class="urlOptions flex agilysys-banner-widget-title">
    <div class="widget-title-lable">

        <label
            for="<?php echo esc_attr($this->get_field_id('type')); ?>"><?php esc_attr_e('Select Banner Image/Video', 'vidget');?>:</label>

    </div>
    <div class="widget-title-input-text">
        <select style="width:100%;" id="<?php echo esc_attr($this->get_field_id('type')); ?>"
            name="<?php echo esc_attr($this->get_field_name('type')); ?>">
            <option <?php selected($type, 'video', true);?> value="video"><?php _e('Video', 'vidget');?></option>
            <option <?php selected($type, 'image', true);?> value="image"><?php _e('Image', 'image');?></option>
            <option <?php selected($type, 'youtube', true);?> value="youtube"><?php _e('Youtube Url', 'youtube');?>
            </option>
        </select>

    </div>
</div>

<div class="agilysys-banner-widget-title">
    <p>
    <div class="widget-title-lable">
        <label
            for="<?php echo esc_attr($this->get_field_id('src')); ?>"><?php esc_attr_e('Upload Image/VideoURL/Youtube Url', 'vidget');?>:</label>
    </div>
    <div class="widget-title-input-text">
        <label id="image_uri_agilysys_banner_with_socl_widget"></label><br><input class="widefat" id="<?php echo esc_attr($this->get_field_id('src')); ?>"
            name="<?php echo esc_attr($this->get_field_name('src')); ?>" type="text"
            value="<?php echo esc_attr($src); ?>">
        <button class="vidget_upload_btn button" style="margin-top:20px !important;margin-bottom:20px;"><?php _e('Upload', 'vidget');?></button>
    </div>
</div>
<div class="vidget_option_toggle" style="display:none;">

    <p>
    <h4><?php _e(' video options', 'vidget');?></h4>
    <hr>
    <p>
        <label
            for="<?php echo esc_attr($this->get_field_id('poster')); ?>"><?php esc_attr_e('poster', 'vidget');?>:</label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('poster')); ?>"
            name="<?php echo esc_attr($this->get_field_name('poster')); ?>" type="text"
            value="<?php echo esc_attr($poster); ?>">
    </p>
    <p>
        <label
            for="<?php echo esc_attr($this->get_field_id('width')); ?>"><?php esc_attr_e('width', 'vidget');?>(px):</label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('width')); ?>"
            name="<?php echo esc_attr($this->get_field_name('width')); ?>" type="text"
            value="<?php echo esc_attr($poster); ?>">
    </p>
    <p>
        <label
            for="<?php echo esc_attr($this->get_field_id('height')); ?>"><?php esc_attr_e('height', 'vidget');?>(px):</label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('height')); ?>"
            name="<?php echo esc_attr($this->get_field_name('height')); ?>" type="text"
            value="<?php echo esc_attr($poster); ?>">
    </p>
    </p>
    </p>
</div>
</p>


<table class="sisw_multi_image_slider_table_wrapper">
    <tbody>
        <?php
$targetval = (isset($instance['target_tab'])) ? $instance['target_tab'] : "";
        $linkval = (isset($instance['image_link'])) ? $instance['image_link'] : "";

        if (isset($image_url)) {
            $imagelength = array();
            $count = 0;
            foreach ($image_url as $url) {
                if (!empty($url)) {$imgurl = wp_get_attachment_image_src($url, 'full');
                    ?>
        <tr class="sisw_individual_image_section">
            <td width="50px;">
                <span class="sisw_drag_Section">&#9776;</span>


                <a href="<?php if (!empty($imgurl[0])) {echo $imgurl[0];}?>" target="_blank"><img
                        class="sisw_admin_image_preview active"
                        src="<?php if (!empty($imgurl[0])) {echo $imgurl[0];}?>"></a>
            </td>
            <td>
                <input class="" name="<?php echo esc_attr($this->get_field_name('image_url')); ?>[]" type="hidden"
                    value="<?php echo $url; ?>" />
                <input class="sisw_image_input_field"
                    name="<?php echo esc_attr($this->get_field_name('image_link')); ?>[]" type="text"
                    value="<?php echo esc_url($linkval[$count]); ?>" placeholder="Link (optional)" />

                <?php _e("Open link in a new tab", "sisw_gwl");?> <select
                    name="<?php echo esc_attr($this->get_field_name('target_tab')); ?>[]" class="sisw_opentab"
                    style="display:none;">
                    <option <?php selected("", $targetval[$count]);?> value=""><?php _e("Select", "sisw_gwl");?>
                    </option>
                    <option <?php selected("newtab", $targetval[$count]);?> value="newtab">
                        <?php _e("New Window", "sisw_gwl");?></option>
                </select>
                <input type="checkbox" name="ssiw_checkurl" <?php checked("newtab", $targetval[$count]);?>
                    value="newtab" class="ssiw_checkurl">
            </td>
            <td>
                <a class="sisw_remove_field_upload_media_widget" title="Delete" href="javascript:void(0)">&times;</a>
            </td>
        </tr>
        <?php
}
                $imagelength[] = $count;
                $count++;
            }
        }
        ?>
        <!-- <tr class="sisw_no_images" <?php //if (count($image_url) >= 1) {?> style="display:none;" <?php //}?>>
                <td colspan="3">
                    <?php //_e("No images selected");?>
                </td>
            </tr> -->
    </tbody>
</table>

<script>
jQuery(document).ready(function() {
    jQuery(".sisw_multi_image_slider_table_wrapper tbody").sortable({
        update: function() {
            jQuery(this).parents('.widget').find('input[type="submit"]').removeAttr('disabled');
            jQuery(this).parents('.widget').find('.sisw_temp_text_val').trigger("change");
        }

    });
});
</script>

<input type="hidden" class="sisw_temp_image_name"
    value="<?php echo esc_attr($this->get_field_name('image_url')); ?>[]" />
<input type="hidden" class="sisw_temp_image_link"
    value="<?php echo esc_attr($this->get_field_name('image_link')); ?>[]" />
<input type="hidden" class="sisw_temp_image_tab"
    value="<?php echo esc_attr($this->get_field_name('target_tab')); ?>[]" />
<input type="hidden" class="sisw_temp_text_val" value="" />

</div> <!-- wrapper -->
<!--<button class="appendbtn" type="button">Click to append</button>-->

<script>
jQuery(".appendbtn").click(function() {
    jQuery("#divcontainer").append('<div class="appendme">The div that should be appended</div>');
});
</script>
<?php

    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        $instance['banner_title'] = (!empty($new_instance['banner_title'])) ? strip_tags($new_instance['banner_title']) : '';

        $instance['src'] = (!empty($new_instance['src'])) ? strip_tags($new_instance['src']) : '';
        $instance['type'] = (!empty($new_instance['type'])) ? strip_tags($new_instance['type']) : 'video';
        $instance['poster'] = (!empty($new_instance['poster'])) ? strip_tags($new_instance['poster']) : '';
        $instance['loop'] = (!empty($new_instance['loop'])) ? strip_tags($new_instance['loop']) : 'off';
        $instance['prel oad'] = (!empty($new_instance['preload'])) ? strip_tags($new_instance['preload']) : 0;
        $instance['autoplay'] = (!empty($new_instance['autoplay'])) ? strip_tags($new_instance['autoplay']) : 'off';
        $instance['menu_title'] = (!empty($new_instance['menu_title'])) ? strip_tags($new_instance['menu_title']) : '';
        $instance['banner_desc'] = (!empty($new_instance['banner_desc'])) ? strip_tags($new_instance['banner_desc']) : '';

      

        //$instance['banner_url'] = (!empty($new_instance['banner_url'])) ? strip_tags($new_instance['banner_url']) : '';

        $instance['banner_url_title'] = (!empty($new_instance['banner_url_title'])) ? strip_tags($new_instance['banner_url_title']) : '';
        $instance['icon_title'] = (!empty($new_instance['icon_title'])) ? strip_tags($new_instance['icon_title']) : '';

        if (!empty($new_instance['nav_menu'])) {
            $instance['nav_menu'] = (int) $new_instance['nav_menu'];
        }

        $instance['image_url'] = $new_instance['image_url'];
        $instance['image_link'] = $new_instance['image_link'];
        $instance['slider_title'] = $new_instance['slider_title'];
        $instance['nav_option'] = $new_instance['nav_option'];
        $instance['autoslide_option'] = $new_instance['autoslide_option'];
        $instance['slider_pause_option'] = $new_instance['slider_pause_option'];
        $instance['target_tab'] = $new_instance['target_tab'];

      
        $instance['link_type'] = $new_instance['link_type'];

        if ($new_instance['link_type'] == 'page') {
            $instance['page'] = $new_instance['page'];
            $instance['link'] = '';
        } elseif ($new_instance['link_type'] == 'link') {
            $instance['link'] = $new_instance['link'];
            $instance['page'] = '';

        }

        return $instance;
    }

}
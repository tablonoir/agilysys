<?php

add_action('widgets_init', 'agil_load_agilysys_articles_blog_posts_widget');

function agil_load_agilysys_articles_blog_posts_widget()
{
    register_widget('agilysys_articles_blog_posts_widget');
}

class agilysys_articles_blog_posts_widget extends WP_Widget
{

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Articles Blog Posts Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');

        wp_enqueue_script('jquery-ui-datepicker');

        // You need styling for the datepicker. For simplicity I've linked to the jQuery UI CSS on a CDN.
        wp_register_style('jquery-ui', 'https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css');
        wp_enqueue_style('jquery-ui');

    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        $no_of_posts = $instance['no_of_posts'];
        $per_page = $instance['per_page'];
        $from_date = $instance['from_date'];
        $to_date = $instance['to_date'];

        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

        $category = ($instance['category']);

        ?>

<style>
.cvf_pag_loading {
    padding: 20px;
}

.cvf-universal-pagination ul {
    margin: 0;
    padding: 0;
}

.cvf-universal-pagination ul li {
    display: inline;
    margin: 3px;
    padding: 4px 8px;
    background: #FFF;
    color: black;
}

.cvf-universal-pagination ul li.active:hover {
    cursor: pointer;
    background: #1E8CBE;
    color: white;
}

.cvf-universal-pagination ul li.inactive {
    background: #7E7E7E;
}

.cvf-universal-pagination ul li.selected {
    background: #1E8CBE;
    color: white;
}


.loader {
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3498db;
    width: 120px;
    height: 120px;
    -webkit-animation: spin 2s linear infinite;
    /* Safari */
    animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
    0% {
        -webkit-transform: rotate(0deg);
    }

    100% {
        -webkit-transform: rotate(360deg);
    }
}

@keyframes spin {
    0% {
        transform: rotate(0deg);
    }

    100% {
        transform: rotate(360deg);
    }
}

#pagination_agilysys_articles_blog_posts_widget {
    float: right;
    margin-right: 0;
    width: 100%;
}

#pagination_agilysys_articles_blog_posts_widget .cvf-universal-pagination ul{
    
     
     width: 56%;

     margin-left: auto;
     margin-right: auto;
     padding-top:10px;
     
}


</style>

 <div id="overlay_agilysys_articles_blog_posts_widget">
        <div class="loader_agilysys_articles_blog_posts_widget"></div>
    </div>
<section class="artTitle center">
    <h2 class="dinProStd greenText h2">Hospitality professionals are expanding their use of technology to augment guest
        service</h2>
</section>




<div class="">

<section class="articlePostSection flex" id="articleList">
    
</section>



</div>




<section class="articleBG orangeBG">
<div id="pagination_agilysys_articles_blog_posts_widget">


</div>
</section>


<style>


  .loader_agilysys_articles_blog_posts_widget {
    width: 80px;
    height: 80px;
    background: #fff;
    border: 2px solid #f3f3f3;
    border-top: 3px solid #008000;
    border-radius: 100%;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 45%;
    margin: auto;
    animation: spin 1s infinite;

}

@keyframes spin {
    from {
        transform: rotate(0deg);
    }

    to {
        transform: rotate(360deg);
    }
}

</style>


<script type="text/javascript">
jQuery(document).ready(function($) {
    // This is required for AJAX to work on our page
    var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

    function cvf_load_all_posts(page) {
    jQuery(".loader_agilysys_articles_blog_posts_widget").fadeIn("slow");

        // Data to receive from our server
        // the value in 'action' is the key that will be identified by the 'wp_ajax_' hook
        var data = {
            page: page,
            action: "demo-pagination-load-posts",
            per_page: '<?php echo $per_page; ?>',
            category: '<?php echo $category; ?>',
            from_date: '<?php echo $from_date; ?>',
            to_date: '<?php echo $to_date; ?>'
        };


        // Send the data
        jQuery.post(ajaxurl, data, function(response) {


            var dataxx = JSON.parse(response);

            jQuery("#articleList").html(dataxx.msg);

            jQuery("#pagination_agilysys_articles_blog_posts_widget").html(dataxx.pag_container);


            jQuery(".loader_agilysys_articles_blog_posts_widget").fadeOut("slow");

        });
    }

    // Load page 1 as the default
    cvf_load_all_posts(1);




    jQuery(document).on('click', '#pagination_agilysys_articles_blog_posts_widget .cvf-universal-pagination li.active', function() {

        var page = jQuery(this).attr('p');
        cvf_load_all_posts(page);
    });
});
</script>



</div>
</div>

<?php
echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['section_title'] = $new_instance['section_title'];
        $instance['no_of_posts'] = $new_instance['no_of_posts'];
        $instance['per_page'] = $new_instance['per_page'];
        $instance['category'] = strip_tags($new_instance['category']);

        $from_date = explode("-", $new_instance['from_date']);
        $instance['from_date'] = $from_date[2] . "-" . $from_date[1] . "-" . $from_date[0];

        $to_date = explode("-", $new_instance['to_date']);
        $instance['to_date'] = $to_date[2] . "-" . $to_date[1] . "-" . $to_date[0];

        return $instance;
    }

    public function form($display_instance)
    {

        $max_entries_hospitality_software = 25;
        $widget_add_id_hosipitality_software = $this->get_field_id('') . "add";

        $section_title = $display_instance['section_title'];
        $no_of_posts = $display_instance['no_of_posts'];
        $per_page = $display_instance['per_page'];
        $selected_category = $display_instance['category'];

        $from_datexx = explode("-", $display_instance['from_date']);
        $from_date = $from_datexx[2] . "-" . $from_datexx[1] . "-" . $from_datexx[0];

        $to_datexx = explode("-", $display_instance['to_date']);
        $to_date = $to_datexx[2] . "-" . $to_datexx[1] . "-" . $to_datexx[0];

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('from_date') . '"> ' . __('From Date', 'agilysys_text_domain') . ' :</label>';
        $rew_html .= '<input id="from_date" name="' . $this->get_field_name('from_date') . '" type="text" value="' . $from_date . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('from_date') . '"> ' . __('To Date', 'agilysys_text_domain') . ' :</label>';
        $rew_html .= '<input id="to_date" name="' . $this->get_field_name('to_date') . '" type="text" value="' . $to_date . '" />';
        $rew_html .= '</p>';
        ?>

<script>
jQuery(document).ready(function($) {
    $("#from_date").datepicker({
        dateFormat: 'dd-mm-yy'
    });
    $("#to_date").datepicker({
        dateFormat: 'dd-mm-yy'
    });
});
</script>

<?php

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('section_title') . '"> ' . __('Title', 'agilysys_text_domain') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('section_title') . '" name="' . $this->get_field_name('section_title') . '" type="text" value="' . $section_title . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('no_of_posts') . '"> ' . __('No of Posts', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<select id="' . $this->get_field_id('no_of_posts') . '" name="' . $this->get_field_name('no_of_posts') . '">';
        $rew_html .= '<option value="">Please Select</option>';

        if ($no_of_posts == "5") {
            $rew_html .= '<option value="5" selected="selected">5 Posts</option>';
        } else {
            $rew_html .= '<option value="5">5 Posts</option>';
        }

        if ($no_of_posts == "10") {
            $rew_html .= '<option value="10" selected="selected">10 Posts</option>';
        } else {
            $rew_html .= '<option value="10">10 Posts</option>';
        }

        if ($no_of_posts == "15") {
            $rew_html .= '<option value="15" selected="selected">15 Posts</option>';
        } else {
            $rew_html .= '<option value="15">15 Posts</option>';
        }

        if ($no_of_posts == "20") {
            $rew_html .= '<option value="20" selected="selected">20 Posts</option>';
        } else {
            $rew_html .= '<option value="20">20 Posts Per Page</option>';
        }

        if ($no_of_posts == "25") {
            $rew_html .= '<option value="25" selected="selected">25 Posts</option>';
        } else {
            $rew_html .= '<option value="25">25 Posts</option>';
        }

        $rew_html .= '</select>';
        $rew_html .= '</p>';

        $args = array("hide_empty" => 0,
            "type" => "post",
            "orderby" => "name",
            "order" => "ASC");
        $categories = get_categories($args);

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('category') . '"> ' . __('Select Blog Category', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<select id="' . $this->get_field_id('category') . '" name="' . $this->get_field_name('category') . '">';
        $rew_html .= '<option value="">Please Select</option>';

        foreach ($categories as $key) {
            if ($key->name != 'Uncategorized') {

                if ($key->name == $selected_category) {
                    $rew_html .= '<option value="' . $key->name . '" selected="selected">' . $key->name . '</option>';
                } else {
                    $rew_html .= '<option value="' . $key->name . '">' . $key->name . '</option>';
                }
            }

        }

        $rew_html .= '</select>';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('per_page') . '"> ' . __('Per Page', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<select id="' . $this->get_field_id('per_page') . '" name="' . $this->get_field_name('per_page') . '">';
        $rew_html .= '<option value="">Please Select</option>';

        if ($per_page == "3") {
            $rew_html .= '<option value="3" selected="selected">3 Posts Per Page</option>';
        } else {
            $rew_html .= '<option value="3">3 Posts Per Page</option>';
        }

        if ($per_page == "6") {
            $rew_html .= '<option value="6" selected="selected">6 Posts Per Page</option>';
        } else {
            $rew_html .= '<option value="6">6 Posts Per Page</option>';
        }

        if ($per_page == "9") {
            $rew_html .= '<option value="9" selected="selected">9 Posts Per Page</option>';
        } else {
            $rew_html .= '<option value="9">9 Posts Per Page</option>';
        }

        if ($per_page == "12") {
            $rew_html .= '<option value="12" selected="selected">12 Posts Per Page</option>';
        } else {
            $rew_html .= '<option value="12">12 Posts Per Page</option>';
        }

        $rew_html .= '</select>';
        $rew_html .= '</p><br>';
        ?>


<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_articles_blog_posts_widget  select{
     float: left;
    width: 60%;
    margin-top:20px !important;
    margin-bottom:10px !important;
}

#rew_container_agilysys_articles_blog_posts_widget input,

textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_articles_blog_posts_widget label {
    width: 40%;
     float: left;
}

#rew_container_agilysys_articles_blog_posts_widget p {
   padding:10px !important;
}

<?php echo '.' . $widget_add_id_slider_client;

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}


#entries {
    padding: 10px 0 0;
}

#entries .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries .entrys:first-child {
    margin: 0;
}

#entries .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries .entry-title.active:after {
    content: '\f142';
}

#entries .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_articles_blog_posts_widget p {
   padding:10px !important;
}

#rew_container_agilysys_articles_blog_posts_widget #entries p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_articles_blog_posts_widget">
    <?php echo $rew_html; ?>
</div>




<?php

    }
}

?>
<?php

function load_agilysys_footer_icons_widget()
{
    register_widget('agilysys_footer_icons_widget');
}
add_action('widgets_init', 'load_agilysys_footer_icons_widget');

class agilysys_footer_icons_widget extends WP_Widget
{
/**
 * constructor -- name this the same as the class above
 */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Footer Icons Widget', 'agilysys_text_domain'));

        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }
    
    public function addhttp($url) {
    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "https://" . $url;
    }
    return $url;
}

    public function widget($args, $instance)
    {

        $title = $instance['title'];
        ?>

<div class="flex footerSocialInner">
    <div class="footerSocialText">
        <p><?php echo $title; ?></p>
    </div>
    <div class="footerSocialIcons">
        <ul class="footerSocialIconsUL">
            <li><a href="<?php echo $this->addhttp(get_theme_mod('footer_facebook_link')); ?>"><img
                        src="<?php echo get_theme_mod('footer_facebook_image_settings'); ?>" class="img-fluid footerimg"
                        alt="fb" onmouseover="show_image_fb();" id="facebook_img" onmouseout="hide_image_fb();"></a></li>
            <li><a href="<?php echo $this->addhttp(get_theme_mod('footer_twitter_link')); ?>"><img
                        src="<?php echo get_theme_mod('footer_twitter_image_settings'); ?>" class="img-fluid"
                        alt="twitter" onmouseover="show_image_twitter();" id="twitter_img" onmouseout="hide_image_twitter();"></a></li>
            <li><a href="<?php echo $this->addhttp(get_theme_mod('footer_linkedin_link')); ?>"><img
                        src="<?php echo get_theme_mod('footer_linkedin_image_settings'); ?>" class="img-fluid"
                        alt="linked" onmouseover="show_image_linkedin();" id="linkedin_img" onmouseout="hide_image_linkedin();"></a></li>
            <li><a href="<?php echo $this->addhttp(get_theme_mod('footer_instagram_link')); ?>"><img
                        src="<?php echo get_theme_mod('footer_instagram_image_settings'); ?>" class="img-fluid"
                        alt="instagram" onmouseover="show_image_instagram();" id="instagram_img" onmouseout="hide_image_instagram();"></a></li>
            <li><a href="<?php echo $this->addhttp(get_theme_mod('footer_pinterest_link')); ?>"><img
                        src="<?php echo get_theme_mod('footer_pinterest_image_settings'); ?>" class="img-fluid"
                        alt="volly" onmouseover="show_image_pinterest();" id="pinterest_img" onmouseout="hide_image_pinterest();"></a></li>
        </ul>
    </div>
</div>

<script>
 function show_image_fb(){
     jQuery("#facebook_img").attr('src',"<?php echo get_theme_mod('footer_facebook_image_hover_settings'); ?>");
 }
 
 function hide_image_fb(){
     
    jQuery("#facebook_img").attr('src',"<?php echo get_theme_mod('footer_facebook_image_settings'); ?>"); 
 }
 
  function show_image_twitter(){
     jQuery("#twitter_img").attr('src',"<?php echo get_theme_mod('footer_twitter_image_hover_settings'); ?>");
 }
 
 function hide_image_twitter(){
     
    jQuery("#twitter_img").attr('src',"<?php echo get_theme_mod('footer_twitter_image_settings'); ?>"); 
 }
 
 
  function show_image_linkedin(){
     jQuery("#linkedin_img").attr('src',"<?php echo get_theme_mod('footer_linkedin_image_hover_settings'); ?>");
 }
 
 function hide_image_linkedin(){
     
    jQuery("#linkedin_img").attr('src',"<?php echo get_theme_mod('footer_linkedin_image_settings'); ?>"); 
 }
 
 
  function show_image_instagram(){
     jQuery("#instagram_img").attr('src',"<?php echo get_theme_mod('footer_instagram_image_hover_settings'); ?>");
 }
 
 function hide_image_instagram(){
     
    jQuery("#instagram_img").attr('src',"<?php echo get_theme_mod('footer_instagram_image_settings'); ?>"); 
 }
 
 
 
  function show_image_pinterest(){
     jQuery("#pinterest_img").attr('src',"<?php echo get_theme_mod('footer_pinterest_image_hover_settings'); ?>");
 }
 
 function hide_image_pinterest(){
     
    jQuery("#pinterest_img").attr('src',"<?php echo get_theme_mod('footer_pinterest_image_settings'); ?>"); 
 }
 
 
</script>

<?php

    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['title'] = $new_instance['title'];

        return $instance;
    }

    public function form($display_instance)
    {
        $widget_add_id_slider_client = $this->get_field_id('') . "add_agilysys_footer_icons_widget";

        $title = ($display_instance['title']);
        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('title') . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input style="width:100%;" id="' . $this->get_field_name('title') . '" name="' . $this->get_field_name('title') . '" type="text" value="' . $title . '" />';
        $rew_html .= '</p>';
        ?>

<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_footer_icons_widget input,
select,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_footer_icons_widget label {
    width: 40%;
}

<?php echo '.'. $widget_add_id_slider;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}


#entries {
    padding: 10px 0 0;
}

#entries .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries .entrys:first-child {
    margin: 0;
}

#entries .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries .entry-title.active:after {
    content: '\f142';
}

#entries .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_footer_icons_widget #entries p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_footer_icons_widget">
    <?php echo $rew_html; ?>
</div>
<?php

    }

}
<?php

function load_agilysys_resources_with_blog_post_widget()
{
    register_widget('agilysys_resources_with_blog_post_widget');
}

add_action('widgets_init', 'load_agilysys_resources_with_blog_post_widget');

class agilysys_resources_with_blog_post_widget extends WP_Widget
{

/**
 *  constructor -- name this the same as the class above
 */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Resources With Blog Post Widget', 'AGILYSYS_TEXT_DOMAIN'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];

        if (!empty($instance['no_of_posts'])) {
            $posts_per_page = $instance['no_of_posts'];
        } else {
            $posts_per_page = 3;
        }

        $selected_category = ($instance['sub_category']);

        $from_date = date("F d,Y", strtotime($instance['from_date']));
        $to_date = date("F d,Y", strtotime($instance['to_date']));

        $the_query = new WP_Query(array(
            'category_name' => $selected_category,
            'post_status' => 'publish',
            'date_query' => array(
                array(
                    'after' => $from_date,
                    'before' => $to_date,
                    'inclusive' => true,
                ),
            ),
            'posts_per_page' => $posts_per_page,
        ));

        $title = $instance['title'];
        $image_uri = $instance['image_uri'];
        $image_uri_alt = $instance['image_uri_alt'];
        ?>


<section class="resWhatSection">
    <div class="resWhatImg center" data-aos="fade-down" data-aos-delay="300" data-aos-duration="400" data-aos-once="true">
        <img class="img-fluid" src="<?php echo $image_uri; ?>" alt="<?php echo $image_uri_alt; ?>" />
    </div>
    <div class="resWhatBG"></div>
    <h2 class="dinProStd whiteText h2 center"
    data-aos="fade-right" data-aos-delay="300" data-aos-duration="400" data-aos-once="true">What's Happening Hospitality?</h2>


    <div class="homePosts homePosts1" data-aos="fade-left" data-aos-delay="300" data-aos-duration="400" data-aos-once="true">
        <div class="swiper-container">
            <!-- Additional required wrapper -->
            <div class="swiper-wrapper">
                <?php if ($the_query->have_posts()): ?>
                <?php while ($the_query->have_posts()): $the_query->the_post();?>
                <div class="swiper-slide">
                    <div class=" homePostsInner">


                        <div class="homePostsCard homePostsCard1">

                            <a class="homePostGrid" href="<?php echo get_permalink(get_option('page_for_posts')); ?>">

                                <div class="homePostsCardImg">
                                    <?php $img_attribs = wp_get_attachment_image_src(get_post_thumbnail_id(), array(420, 110)); // returns an array
            if ($img_attribs) {
                ?>
                                    <img alt="posts" class="img-fluid" src="<?php echo $img_attribs[0]; ?>">
                                    <?php }?>
                                </div>

                                <div class="homePostsCardContent">
                                    <div class="homePostsCardSMDate">
                                        <p><time datetime="<?php echo get_the_date('c'); ?>"
                                                itemprop="datePublished"><?php echo get_the_date(); ?></time></p>
                                    </div>
                                    <div class="homePostsCardContent1">
                                        <h4 class="blackText"><?php echo wp_trim_words(get_the_title(), 6, '...'); ?>
                                        </h4>
                                        <p><?php echo wp_trim_words(get_the_content(), 12); ?></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <?php endwhile;?>
                <?php wp_reset_postdata();?>

                <?php else: ?>
                <p><?php __('No News');?></p>
                <?php endif;?>
            </div>

            <!-- If we need navigation buttons -->
            <div class="swiper-button-next">
                <img src="<?php echo get_template_directory_uri(); ?>/img/arrow-right-white.png" class="img-fluid"
                    alt="arrow-left">
            </div>

            <div class="swiper-button-prev">
                <img src="<?php echo get_template_directory_uri(); ?>/img/arrow-left-white.png" class="img-fluid"
                    alt="arrow-right">
            </div>
        </div>

        <div class="flex homePostEnd">
            <div class="homePostEnd2">
                <a href="" class="homebuttonRounded">View All Posts
                    <i class="fa fa-arrow-right" aria-hidden="true"></i>
                </a>
            </div>
        </div>

    </div>


</section>


<?php
echo $args['after_widget'];
    }

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['title'] = strip_tags($new_instance['title']);
        $instance['no_of_posts'] = strip_tags($new_instance['no_of_posts']);

        $instance['image_uri'] = strip_tags($new_instance['image_uri']);
        $instance['image_uri_alt'] = strip_tags($new_instance['image_uri_alt']);

        $instance['category'] = $new_instance['category'];
        $instance['sub_category'] = $new_instance['sub_category'];

        $from_date = explode("-", $new_instance['from_date']);
        $instance['from_date'] = $from_date[2] . "-" . $from_date[1] . "-" . $from_date[0];

        $to_date = explode("-", $new_instance['to_date']);
        $instance['to_date'] = $to_date[2] . "-" . $to_date[1] . "-" . $to_date[0];

        return $instance;
    }

    public function form($display_instance)
    {
        $widget_add_id_slider_client = $this->get_field_id('') . "add";

        $title = ($display_instance['title']);

        $no_of_posts = ($display_instance['no_of_posts']);

        $selected_category = ($display_instance['category']);

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('title') . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" type="text" value="' . $title . '" />';
        $rew_html .= '</p><br><br><br><br>';

        $from_datexx = explode("-", $display_instance['from_date']);
        $from_date = $from_datexx[2] . "-" . $from_datexx[1] . "-" . $from_datexx[0];

        $to_datexx = explode("-", $display_instance['to_date']);
        $to_date = $to_datexx[2] . "-" . $to_datexx[1] . "-" . $to_datexx[0];

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('from_date') . '"> ' . __('From Date', 'agilysys_text_domain') . ' :</label>';
        $rew_html .= '<input id="from_date" name="' . $this->get_field_name('from_date') . '" type="text" value="' . $from_date . '" />';
        $rew_html .= '</p><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('from_date') . '"> ' . __('To Date', 'agilysys_text_domain') . ' :</label>';
        $rew_html .= '<input id="to_date" name="' . $this->get_field_name('to_date') . '" type="text" value="' . $to_date . '" />';
        $rew_html .= '</p><br>';
        ?>

<script>
jQuery(document).ready(function($) {
    $("#from_date").datepicker({
        dateFormat: 'dd-mm-yy'
    });
    $("#to_date").datepicker({
        dateFormat: 'dd-mm-yy'
    });
});
</script>

<?php

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('no_of_posts') . '"> ' . __('No of Posts', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<select id="' . $this->get_field_id('no_of_posts') . '" name="' . $this->get_field_name('no_of_posts') . '">';
        $rew_html .= '<option value="">Please Select</option>';

        if ($no_of_posts == "5") {
            $rew_html .= '<option value="5" selected="selected">5 Posts Per Page</option>';
        } else {
            $rew_html .= '<option value="5">5 Posts Per Page</option>';
        }

        if ($no_of_posts == "10") {
            $rew_html .= '<option value="10" selected="selected">10 Posts Per Page</option>';
        } else {
            $rew_html .= '<option value="10">10 Posts Per Page</option>';
        }

        $rew_html .= '</select>';
        $rew_html .= '</p><br>';

        $args = array("hide_empty" => 0,

            "parent" => 0,
            "type" => "post",
            "orderby" => "name",
            "order" => "ASC");
        $parent_categories = get_categories($args);

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('category') . '"> ' . __('Select Blog Category', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<select id="' . $this->get_field_id('category') . '" name="' . $this->get_field_name('category') . '" onChange="fetch_subcat(this.value);">';
        $rew_html .= '<option value="">Please Select</option>';

        foreach ($parent_categories as $key) {

            if ($key->name != 'Uncategorized') {

                if ($key->term_id == $display_instance['category']) {
                    $rew_html .= '<option value="' . $key->term_id . '" selected="selected">' . $key->name . '</option>';
                } else {
                    $rew_html .= '<option value="' . $key->term_id . '">' . $key->name . '</option>';
                }

            }

        }

        $rew_html .= '</select>';
        $rew_html .= '</p><br>';

        if ($display_instance['category'] != "") {

            $args = array("hide_empty" => 0,

                "parent" => $display_instance['category'],
                "type" => "post",
                "orderby" => "name",
                "order" => "ASC");
            $child_cat = get_categories($args);

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('sub_category') . '"> ' . __('Select Blog SubCategory', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="sub_category_agilysys_blog_posts_widget" name="' . $this->get_field_name('sub_category') . '">';
            $rew_html .= '<option value="">Please Select</option>';
     
            foreach ($child_cat as $key) {

                if ($key->name == $display_instance['sub_category']) {
                    $rew_html .= '<option value="' . $key->name . '" selected="selected">' . $key->name . '</option>';
                } else {
                    $rew_html .= '<option value="' . $key->name . '">' . $key->name . '</option>';
                }

            }
            $rew_html .= '</select>';
            $rew_html .= '</p><br>';

        }
        else
        {
            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('sub_category') . '"> ' . __('Select Blog SubCategory', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="sub_category_agilysys_blog_posts_widget" name="' . $this->get_field_name('sub_category') . '">';
            $rew_html .= '<option value="">Please Select</option>';
         
            $rew_html .= '</select>';
            $rew_html .= '</p><br>';
        }

        ?>

<script>
function fetch_subcat(value) {
 // This is required for AJAX to work on our page
 var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

 jQuery("#sub_category_agilysys_blog_posts_widget").html('<option value="">Please Select</option>');


// Data to receive from our server
// the value in 'action' is the key that will be identified by the 'wp_ajax_' hook
var data = {

    action: "fetch-all-subcategories-based-on-category",
    value: value,

};


// Send the data
jQuery.post(ajaxurl, data, function(response) {


    var dataxx = JSON.parse(response);

    jQuery("#sub_category_agilysys_blog_posts_widget").append(dataxx.msg);






});
}
</script>


        <?php


        $rew_html .= '<br><br><div class="widg-img">';
        $show1 = (empty($display_instance['image_uri'])) ? 'style="display:none;"' : '';
        $rew_html .= '<img class="' . $this->get_field_id('image_id') . '_media_image custom_media_image" src="' . $display_instance['image_uri'] . '" ' . $show1 . ' width=200" height="120"/>';
        $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id') . '_media_id custom_media_id" name="' . $this->get_field_name('image_id') . '" id="' . $this->get_field_id('image_id') . '" value="' . $display_instance['image_id'] . '" />';
        $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id') . '_media_url custom_media_url" name="' . $this->get_field_name('image_uri') . '" id="' . $this->get_field_id('image_uri') . '" value="' . $display_instance['image_uri'] . '">';
        $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_upload" id="' . $this->get_field_id('image_id') . '"/>';

        $rew_html .= '</div><br><br><br><br>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt') . '"> ' . __('Image Alt', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt') . '" name="' . $this->get_field_name('image_uri_alt') . '" type="text" value="' . $display_instance['image_uri_alt'] . '" />';
        $rew_html .= '</p><br><br><br><br>';
        ?>

<script>
jQuery(document).ready(function() {
    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {
                    jQuery('.' + button_id_s + '_media_id').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_url').val(attachment.url);
                    jQuery('.' + button_id_s + '_media_image').attr('src', attachment.url).css(
                        'display', 'block');
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload');

});
</script>

<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_resources_with_blog_post_widget  select{
     float: left;
    width: 60%;
    margin-top:20px !important;
    margin-bottom:10px !important;
}

#rew_container_agilysys_resources_with_blog_post_widget input,

textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_resources_with_blog_post_widget label {
    width: 40%;
     float: left;
}

#rew_container_agilysys_resources_with_blog_post_widget p {
   padding:10px;
}

<?php echo '.'. $widget_add_id_slider_client;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}


#entries_agilysys_resources_with_blog_post_widget {
    padding: 10px 0 0;
}

#entries_agilysys_resources_with_blog_post_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_resources_with_blog_post_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_resources_with_blog_post_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_resources_with_blog_post_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_resources_with_blog_post_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_resources_with_blog_post_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_resources_with_blog_post_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_resources_with_blog_post_widget #entries_agilysys_resources_with_blog_post_widget p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_resources_with_blog_post_widget">
    <?php echo $rew_html; ?>
</div>
<?php
}

}
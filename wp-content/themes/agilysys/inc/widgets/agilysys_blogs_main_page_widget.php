<?php

function load_agilysys_blogs_main_page_widget()
{
    register_widget('agilysys_blogs_main_page_widget');
}

add_action('widgets_init', 'load_agilysys_blogs_main_page_widget');

class agilysys_blogs_main_page_widget extends WP_Widget
{

    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Blogs Main Page Widget', 'AGILYSYS_TEXT_DOMAIN'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {
        
        echo $args['before_widget'];
        $category_id = get_cat_ID('blogpost');

        $args = array(
            "hide_empty" => 0,
            "type" => "post",
            "orderby" => "name",
            "order" => "ASC",
            "child_of" => $category_id,
        );
        $categories = get_categories($args);

        ?>

<div class="blogSideBar">
    <div class="searchForm">

        <input type="text" class="" name="s" id="s" placeholder="Search this blog..."
            onKeyup="search_this(this.value);" />
        <div id="search_results_div"></div>
    </div>
    <div class="blogSubscribeForm">
        <h3 class="greenText dinProStd">Subscribe to Post Updates</h3>
        <p>Don't miss a post! Subscribe to receive our semi-monthly newsletter.</p>
        <form class="" action="" id="" method="post">
            <input type="text" class="" name="first_name" id="fname" placeholder="First Name" />
            <input type="text" class="" name="last_name" id="lname" placeholder="Last Name" />
            <input type="email" class="" name="eamil" id="email" placeholder="Email" />
            <input type="submit" class="" name="submit" id="submit" value="Subscribe" />
        </form>
    </div>
    <div class="blogCategories">
        <h3 class="greenText dinProStd">Categories</h3>
        <select id="" class="">
            <option value="">Select Category</option>

            <?php
foreach ($categories as $key) {
            ?>
            <option value="<?php echo $key->term_id; ?>"><?php echo $key->cat_name; ?></option>
            <?php
}
        ?>
        </select>
    </div>
    <div class="blogTag">
        <h3 class="greenText dinProStd">Tag Cloud</h3>
        <div class="blogTagLinks ">
            <a href="#">Analytics</a>
            <a href="#">Casinos </a>
            <a href="#">coronavirus</a>
            <a href="#">Cruise </a>
            <a href="#">data security </a>
            <a href="#">digital ordering </a>
            <a href="#">digital room keys </a>
            <a href="#">foodservice operations </a>
            <a href="#">G2E 2019 </a>
            <a href="#">gaming </a>
            <a href="#">guest engagement </a>
            <a href="#">guest service</a>
            <a href="#">HITEC19 </a>
            <a href="#">HITEC2020 </a>
            <a href="#">hospitality </a>
            <a href="#">hospitality marketing </a>
            <a href="#">hotel management software </a>
            <a href="#">Hotels </a>
            <a href="#">Hotels and Resorts </a>
            <a href="#">Housekeeping</a>
            <a href="#">IoT </a>
            <a href="#">mobile check-in </a>
            <a href="#">mobile ordering </a>
            <a href="#">nutrition software </a>
            <a href="#">online booking </a>
            <a href="#">online </a>
            <a href="#">ordering </a>
            <a href="#">payments </a>
            <a href="#">Point Of Sale Solution </a>
            <a href="#">Property Management Solution </a>
            <a href="#">restaurant inventory </a>
            <a href="#">Restaurants </a>
            <a href="#">Self-Service Kiosks </a>
            <a href="#">stadium foodservice </a>
            <a href="#">table management </a>
            <a href="#">web booking</a>
        </div>
    </div>

    <script>
    function search_this(val) {

        if(val!='')
        {
        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

        var data = {

            action: "demo-pagination-search-this",
            s: val
        };


        // Send the data
        jQuery.post(ajaxurl, data, function(response) {

            jQuery('#search_results_div').html(response);


        });
    }
    else
    {
        jQuery('#search_results_div').html('');
    }
    }
    </script>

    <?php
echo $args['after_widget'];
    }

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {

    }

    public function form($display_instance)
    {

    }

}
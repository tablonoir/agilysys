<?php

function agilysys_what_makes_Widgets()
{
    register_widget('agilysys_what_makes');
}

add_action('widgets_init', 'agilysys_what_makes_Widgets');

class agilysys_what_makes extends WP_Widget
{
    /**
     * constructor -- name this the same as the class above
     */

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys What Makes Different Widget', 'AGILYSYS_TEXT_DOMAIN '));

        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    /**
     * @see WP_Widget::widget -- do not rename this
     * This is for front end
     */

    public function widget($args, $instance)
    {

        extract($args);

        echo $args['before_widget'];

        ?>


<div class="homeOurDiverseContent center">
    <h2 data-aos="fade-left" data-aos-delay="300" data-aos-duration="400" data-aos-once="true"
        class="greenText dinProStd"><?php echo $instance['what_makes_title']; ?></h2>
    <p class="dinproReg" data-aos="fade-right" data-aos-delay="300" data-aos-duration="400" data-aos-once="true">
        <?php echo substr($instance['what_makes_content'],0,65); ?></p>
</div>

<section class="homeFlippingCards">
    <div class="row">
        <?php

        for ($i = 0; $i < 3; $i++) {

            //Caption
            $whatmakes_title = esc_attr($instance['what_makes_front_title-' . $i]);
            $what_makes_back_title = esc_attr($instance['what_makes_back_title-' . $i]);

            //Front Image
            $image_front = esc_url($instance['what_makes_front_image' . $i]);
            //back Image
            $image_back = esc_url($instance['what_makes_back_image' . $i]);
            $image_uri_alt = $instance['image_uri_alt' . $i];

            //Description

            $full_description = apply_filters('the_content', $instance['what_full_desc-' . $i]);

            $link_type = $instance['link_type' . $i];
            if ($link_type == "link") {
                $what_makes_url = $instance['what_makes_url' . $i];
            } elseif ($link_type == "page") {
                $post_id = $instance['page' . $i];
                $post = get_post($post_id);
                $what_makes_url = home_url($post->post_name) . "/";
            }

            $what_makes_url_title = esc_attr($instance['what_makes_url_title-' . $i]);

            ?>



        <div class="flip fliping1 col-12 col-sm-12 col-md-12 col-lg-4" data-aos="fade-up" data-aos-delay="300"
            data-aos-duration="400" data-aos-once="true">
            <div class="front flip1 flex" style="background-image: url(<?php echo $image_front; ?>);">
                <h2 class="text-shadow dinProStd white">
                    <?php echo $whatmakes_title; ?>
                </h2>
            </div>
            <div class="back flex">
                <div class="backImg">

                    <img src="<?php echo $image_back; ?>" class="img-fluid" alt="<?php echo $image_uri_alt; ?>">
                </div>
                <div class="backContent">
                    <h2 class="dinProStd white"><?php echo $what_makes_back_title; ?></h2>
                    <p class="white"><?php echo $full_description; ?></p>
                    <a href="<?php echo $what_makes_url; ?>" class="homebuttonFlip"><?php echo $what_makes_url_title; ?>
                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                    </a>
                </div>
            </div>

        </div>

        <?php

        }

        ?>
    </div>
</section>
<?php

        echo $args['after_widget'];
    }
    //Function widget ends here

    /**
     * @see WP_Widget::update -- do not rename this
     */

    //Function update ends here

    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['what_makes_title'] = strip_tags($new_instance['what_makes_title']);
        $instance['what_makes_content'] = strip_tags($new_instance['what_makes_content']);

        for ($i = 0; $i < 3; $i++) {
            $block = $new_instance['block-' . $i];

            $instance['block-' . $i] = $new_instance['block-' . $i];
            $instance['what_makes_front_title-' . $i] = strip_tags($new_instance['what_makes_front_title-' . $i]);
            $instance['what_makes_back_title-' . $i] = strip_tags($new_instance['what_makes_back_title-' . $i]);
            $instance['what_makes_front_image' . $i] = strip_tags($new_instance['what_makes_front_image' . $i]);
            $instance['what_makes_back_image' . $i] = strip_tags($new_instance['what_makes_back_image' . $i]);

            $instance['image_uri_alt' . $i] = $new_instance['image_uri_alt' . $i];

            $instance['what_full_desc-' . $i] = strip_tags($new_instance['what_full_desc-' . $i]);
            $instance['what_makes_url_title-' . $i] = strip_tags($new_instance['what_makes_url_title-' . $i]);

            $instance['link_type' . $i] = $new_instance['link_type' . $i];
            if ($new_instance['link_type' . $i] == 'page') {
                $instance['page' . $i] = $new_instance['page' . $i];
                $instance['what_makes_url' . $i] = '';
            } elseif ($new_instance['link_type' . $i] == 'link') {
                $instance['what_makes_url' . $i] = $new_instance['what_makes_url-' . $i];
                $instance['page' . $i] = '';

            }

//                $instance['what_makes_url_title-' . $i] = strip_tags($new_instance['what_makes_url_title-' . $i]);

        }
        return $instance;
    }
    /**
     * @see WP_Widget::form -- do not rename this
     */
    public function form($display_instance)
    {

        //$widget_add_id_what_makes = $this->id . "-add";
        $widget_add_id_what_makes = $this->get_field_id('') . "add";
        $what_makes_title = ($display_instance['what_makes_title']);
        $what_makes_content = esc_attr($display_instance['what_makes_content']);

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('what_makes_title') . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('what_makes_title') . '" name="' . $this->get_field_name('what_makes_title') . '" type="text" value="' . $what_makes_title . '" />';
        $rew_html .= '</p><br><br>';
        $rew_html .= '<label for="' . $this->get_field_id('what_makes_content') . '"> ' . __('Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<textarea id="' . $this->get_field_id('what_makes_content') . '" name="' . $this->get_field_name('what_makes_content') . '">' . $what_makes_content . '</textarea>';
        $rew_html .= '</p><br><br>';

        /*         $rew_html .= '<label for="'.$this->get_field_id('what_makes_rows').'"> '. __( 'No. of rows do you want to add*', 'AGILYSYS_TEXT_DOMAIN' ) .' :</label>';
        $rew_html .= '<input class="what_makes_rows" id="'.$this->get_field_name('what_makes_rows').'" name="'.$this->get_field_name('what_makes_rows').'" type="number" value="'.$what_makes_rows.'" />';
        $rew_html .='</p>';   */

        $rew_html .= '<div class="' . $widget_add_id_what_makes . '-input-containers"><div id="entries_agilysys_what_makes">';

        for ($i = 0; $i < 3; $i++) {
            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'AGILYSYS_TEXT_DOMAIN') . ' </span>';

            $display = (!isset($display_instance['block-' . $i]) || ($display_instance['block-' . $i] == "")) ? 'style="display:block;"' : '';

            $rew_html .= '<div class="entry-desc cf">';
            $rew_html .= '<input id="' . $this->get_field_id('block-' . $i) . '" name="' . $this->get_field_name('block-' . $i) . '" type="hidden" value="' . $display_instance['block-' . $i] . '">';
            /**
             * Block Caption
             */

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('what_makes_front_title-' . $i) . '"> ' . __('Front Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('what_makes_front_title-' . $i) . '" name="' . $this->get_field_name('what_makes_front_title-' . $i) . '" type="text" value="' . $display_instance['what_makes_front_title-' . $i] . '">';
            $rew_html .= '</p><br>';

            $rew_html .= '<div class="widg-img' . $i . '">';
            $show1 = (empty($display_instance['what_makes_front_image' . $i])) ? 'style="display:none;"' : '';
            $rew_html .= '<label id="image_uri_agilysys_what_makes_front' . $i . '"></label><br><img class="' . $this->get_field_id('what_makes_front_image' . $i) . '_media_image' . $i . ' custom_media_image' . $i . '" src="' . $display_instance['what_makes_front_image' . $i] . '" ' . $show1 . ' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('what_makes_front_image' . $i) . '_media_id' . $i . ' custom_media_id' . $i . '" name="' . $this->get_field_name('what_makes_front_image' . $i) . '" id="' . $this->get_field_id('what_makes_front_image' . $i) . '" value="' . $display_instance['what_makes_front_image' . $i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('what_makes_front_image' . $i) . '_media_url' . $i . ' custom_media_url' . $i . '" name="' . $this->get_field_name('what_makes_front_image' . $i) . '" id="' . $this->get_field_id('what_makes_front_image' . $i) . '" value="' . $display_instance['what_makes_front_image' . $i] . '">';
            $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_upload' . $i . '" id="' . $this->get_field_id('what_makes_front_image' . $i) . '"/>';

            $rew_html .= '</div><br><br>';
            ?>

<script>
jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    if (attachment.height == 412 && attachment.width == 400 ) {
                        jQuery('.' + button_id_s + '_media_id<?php echo $i; ?>').val(attachment.id);
                        jQuery('.' + button_id_s + '_media_url<?php echo $i; ?>').val(attachment
                            .url);
                        jQuery('.' + button_id_s + '_media_image<?php echo $i; ?>').attr('src',
                            attachment.url).css('display', 'block');



                        jQuery('#image_uri_agilysys_what_makes_front<?php echo $i; ?>')
                            .html("");
                    } else {
                        jQuery('#image_uri_agilysys_what_makes_front<?php echo $i; ?>')
                            .html("Please Enter the correct Dimensions 400x412").css('color',
                                'red');

                    }
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload<?php echo $i; ?>');

});
</script>


<?php

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('what_makes_back_title-' . $i) . '"> ' . __('Back Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('what_makes_back_title-' . $i) . '" name="' . $this->get_field_name('what_makes_back_title-' . $i) . '" type="text" value="' . $display_instance['what_makes_back_title-' . $i] . '">';
            $rew_html .= '</p><br>';

            $rew_html .= '<div class="widg-img' . $i . '">';
            $show1 = (empty($display_instance['what_makes_back_image' . $i])) ? 'style="display:none;"' : '';
            $rew_html .= '<label id="image_uri_agilysys_what_makes_back' . $i . '"></label><br><img class="' . $this->get_field_id('what_makes_back_image' . $i) . '_media_imageb' . $i . ' custom_media_imageb' . $i . '" src="' . $display_instance['what_makes_back_image' . $i] . '" ' . $show1 . ' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('what_makes_back_image' . $i) . '_media_idb' . $i . ' custom_media_idb' . $i . '" name="' . $this->get_field_name('what_makes_back_image' . $i) . '" id="' . $this->get_field_id('what_makes_back_image' . $i) . '" value="' . $display_instance['what_makes_back_image' . $i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('what_makes_back_image' . $i) . '_media_urlb' . $i . ' custom_media_urlb' . $i . '" name="' . $this->get_field_name('what_makes_back_image' . $i) . '" id="' . $this->get_field_id('what_makes_back_image' . $i) . '" value="' . $display_instance['what_makes_back_image' . $i] . '">';
            $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_uploadb' . $i . '" id="' . $this->get_field_id('what_makes_back_image' . $i) . '"/>';

            $rew_html .= '</div><br><br>';
            ?>

<script>
jQuery(document).ready(function() {




    function media_uploadb(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_uploadb<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    if (attachment.height == 148 && attachment.width == 150 ) {
                        jQuery('.' + button_id_s + '_media_idb<?php echo $i; ?>').val(attachment
                            .id);
                        jQuery('.' + button_id_s + '_media_urlb<?php echo $i; ?>').val(attachment
                            .url);
                        jQuery('.' + button_id_s + '_media_imageb<?php echo $i; ?>').attr('src',
                            attachment.url).css('display', 'block');

                        jQuery('#image_uri_agilysys_what_makes_back<?php echo $i; ?>')
                            .html("");
                    } else {
                        jQuery('#image_uri_agilysys_what_makes_back<?php echo $i; ?>')
                            .html("Please Enter the correct Dimensions 400x412").css('color',
                                'red');

                    }
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_uploadb('.custom_media_uploadb<?php echo $i; ?>');

});
</script>



<?php

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt' . $i) . '"> ' . __('Image Alt', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt' . $i) . '" name="' . $this->get_field_name('image_uri_alt'.$i) . '" type="text" value="' . $display_instance['image_uri_alt'.$i] . '">';
            $rew_html .= '</p><br>';

            /**
             * Description
             */
            $what_full_desc = esc_attr($display_instance['what_full_desc-' . $i]);
            $what_makes_url_title = esc_attr($display_instance['what_makes_url_title-' . $i]);
            $what_makes_url = esc_attr($display_instance['what_makes_url-' . $i]);

            $rew_html .= '<p class="last desc">';
            $rew_html .= '<label for="' . $this->get_field_id('what_full_desc-' . $i) . '"> ' . __('Full Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<textarea id="' . $this->get_field_id('what_full_desc-' . $i) . '" name="' . $this->get_field_name('what_full_desc-' . $i) . '">' . $what_full_desc . '</textarea>';
            $rew_html .= '</p>';

            /**
             * Read More Button
             */$rew_html .= '<p class="last">';
            $rew_html .= '<label for="' . $this->get_field_id('what_makes_url_title-' . $i) . '"> ' . __('Button Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('what_makes_url_title-' . $i) . '" name="' . $this->get_field_name('what_makes_url_title-' . $i) . '" type="text" value="' . $what_makes_url_title . '" />';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('link_type' . $i) . '"> ' . __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN') . ' </label>';
            $rew_html .= '<select id="' . $this->get_field_id('link_type' . $i) . '" name="' . $this->get_field_name('link_type' . $i) . '" onChange="show_hide_div_agilysys_what_makes(this.value,' . $i . ');">';
            $rew_html .= '<option value="">Please Select</option>';

            $link_type = $display_instance['link_type' . $i];

            if ($link_type == 'page') {
                $rew_html .= '<option value="page" selected="selected">Internal Page Link</option>';
            } else {
                $rew_html .= '<option value="page">Internal Page Link</option>';
            }

            if ($link_type == 'link') {
                $rew_html .= '<option value="link" selected="selected">External Link</option>';
            } else {
                $rew_html .= '<option value="link">External Link</option>';
            }

            $rew_html .= '</select>';
            $rew_html .= '</p><br><br>';

            $args = array(
                'sort_order' => 'desc',
                'sort_column' => 'post_title',
                'hierarchical' => 1,
                'exclude' => '',
                'include' => '',
                'meta_key' => '',
                'meta_value' => '',
                'authors' => '',
                'child_of' => 0,
                'parent' => -1,
                'exclude_tree' => '',
                'number' => '',
                'offset' => 0,
                'post_type' => 'page',
                'post_status' => 'publish',
            );
            $pages = get_pages($args); // get all pages based on supplied args

            if ($link_type == 'page') {
                $show1 = 'style="display:block"';
                $show2 = 'style="display:none"';
            } elseif ($link_type == 'link') {
                $show1 = 'style="display:none"';
                $show2 = 'style="display:block"';

            } else {
                $show1 = 'style="display:none"';
                $show2 = 'style="display:none"';
            }
            $rew_html .= '<div id="page_div' . $i . '" ' . $show1 . '><p>';
            $rew_html .= '<label for="' . $this->get_field_id('page' . $i) . '"> ' . __('Page', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('page' . $i) . '" name="' . $this->get_field_name('page' . $i) . '">';
            $rew_html .= '<option value="">Please Select</option>';

            $page = $display_instance['page' . $i];

            foreach ($pages as $key) {

                if ($page == $key->ID) {
                    $rew_html .= '<option value="' . $key->ID . '" selected="selected">' . $key->post_title . '</option>';
                } else {
                    $rew_html .= '<option value="' . $key->ID . '">' . $key->post_title . '</option>';
                }

            }

            $rew_html .= '</select>';
            $rew_html .= '</p></div><br><br>';

            $rew_html .= '<div id="link_div' . $i . '" ' . $show2 . '><p>';
            $rew_html .= '<label for="' . $this->get_field_id('what_makes_url-' . $i) . '"> ' . __('Link', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('what_makes_url-' . $i) . '" name="' . $this->get_field_name('what_makes_url-' . $i) . '" type="text" value="' . $display_instance['what_makes_url-' . $i] . '" />';
            $rew_html .= '</p></div><br><br>';
            ?>
<script>
function show_hide_div_agilysys_what_makes(val, i) {
  console.log(val);
    if (val == 'page') {
        jQuery("#page_div" + i).show();
        jQuery("#link_div" + i).hide();
    } else if (val == 'link') {
        jQuery("#page_div" + i).hide();
        jQuery("#link_div" + i).show();
    }

}
</script>

<?php

            $rew_html .= '</p>';
            //$rew_html .= '<p><a href="#delete"><span class="delete-row">'. __( 'Delete Row', 'AGILYSYS_TEXT_DOMAIN' ) .'</span></a></p>';
            $rew_html .= '</div></div>';

        }
        $rew_html .= '</div></div>';

        //$rew_html .= '<div class="'.$widget_add_id_what_makes.'" style="margin-bottom: 36px;text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;">' . __( 'ADD ROW', 'ZWREW_TEXT_DOMAIN' ) . '</div>';

        ?>
<script>
jQuery(document).ready(function(e) {

    jQuery.each(jQuery(".<?php echo $widget_add_id_what_makes; ?>-input-containers #entries_agilysys_what_makes").children(),
        function() {
            if (jQuery(this).find('input').val() != '') {
                jQuery(this).show();
            }
        });
    jQuery(".<?php echo $widget_add_id_what_makes; ?>").bind('click', function(e) {
        var rows = 0;

        jQuery.each(jQuery(".<?php echo $widget_add_id_what_makes; ?>-input-containers #entries_agilysys_what_makes")
            .children(),
            function() {


                if (jQuery(this).find('input').val() == '') {
                    jQuery(this).find(".entry-title").addClass("active");
                    jQuery(this).find(".entry-desc").slideDown();
                    jQuery(this).find('input').first().val('0');
                    jQuery(this).show();
                    return false;
                } else {
                    rows++;
                    jQuery(this).show();
                    jQuery(this).find(".entry-title").removeClass("active");
                    jQuery(this).find(".entry-desc").slideUp();
                }


            });
        if (rows == 4) {
            jQuery("#rew_container #message").show();
        }
    });
    jQuery(".delete-row").bind('click', function(e) {
        var count = 1;
        var current = jQuery(this).closest('.entrys').attr('id');
        jQuery.each(jQuery("#entries_agilysys_what_makes #" + current + " .entry-desc").children(), function() {
            jQuery(this).val('');
        });
        jQuery.each(jQuery("#entries_agilysys_what_makes #" + current + " .entry-desc p").children(), function() {
            jQuery(this).val('');
        });
        jQuery('#entries_agilysys_what_makes #' + current + " .entry-title").removeClass('active');
        jQuery('#entries_agilysys_what_makes #' + current + " .entry-desc").hide();
        jQuery('#entries_agilysys_what_makes #' + current).remove();
        jQuery.each(jQuery(".<?php echo $widget_add_id_what_makes; ?>-input-containers #entries_agilysys_what_makes")
            .children(),
            function() {
                if (jQuery(this).find('input').val() != '') {
                    jQuery(this).find('input').first().val(count);
                }
                count++;
            });
    });
});
</script>
<style>

.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}



#rew_container_agilysys_what_makes select {
    float: left;
    width: 60%;
    margin-top: 20px !important;
    margin-bottom: 10px !important;
}

#rew_container_agilysys_what_makes input,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_what_makes label {
    width: 40%;
    float: left;
}

#rew_container_agilysys_what_makes p {
    padding:20px;
}

<?php echo '.' . $widget_add_id_what_makes;

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}

#entries_agilysys_what_makes #remove-img-bg {
    background: url('<?php echo ZWREW_URL; ?>assets/images/deleteimg.png') center center no-repeat;
    width: 20px;
    height: 22px;
    display: none;
}

#entries_agilysys_what_makes {
    padding: 10px 0 0;
}

#entries_agilysys_what_makes .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_what_makes .entrys:first-child {
    margin: 0;
}

#entries_agilysys_what_makes .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_what_makes .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_what_makes .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_what_makes .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_what_makes .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_what_makes #entries_agilysys_what_makes plast label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_what_makes">
    <?php echo $rew_html; ?>
</div>
<?php
} //Function form ends here
} //ZWREW_Plugin class ends here
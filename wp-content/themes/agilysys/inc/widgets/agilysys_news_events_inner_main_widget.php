<?php

add_action('widgets_init', 'agil_load_agilysys_news_events_inner_main_widget');

function agil_load_agilysys_news_events_inner_main_widget()
{
    register_widget('agilysys_news_events_inner_main_widget');
}

class agilysys_news_events_inner_main_widget extends WP_Widget
{

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys News and Events Inner Main Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');

    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        $args = array(
            'posts_per_page' => -1,
            'post_type' => 'post',
        );
        $all_blog_posts = new WP_Query($args);

        $post_id_arr = array();

        $post_slug_arr = array();

        foreach ($all_blog_posts->posts as $key => $post):

            $postyy = get_post($post->ID);
            $slug = $postyy->post_name;

            array_push($post_slug_arr, $slug);
            array_push($post_id_arr, $post->ID);
        endforeach;

        $id = $_COOKIE['postID'];

        $postxx = get_post($id);

        $key = array_search($id, $post_id_arr);

        $len = count($post_id_arr);

        if ($key != 0 && $key + 1 != $len) {
            $prev = $post_slug_arr[$key + 1];
            $next = $post_slug_arr[$key - 1];
        }
        if ($key == 0) {
            $prev = $post_slug_arr[$key + 1];

        }
        if ($key + 1 == $len) {
            $next = $post_slug_arr[$key - 1];

        }

        ?>



<section class="newsSection flex">
    <div class="newsLatest">
        <div class="newsLatestTitle flex">
            <h4 class="dinProStd greenText"><?php echo date('F d, Y', $postxx->post_date); ?></h4>
            <a href="#" onclick="back_to_all();" class="violetText">Back to all <i
                    class="fa fa-arrow-right" aria-hidden="true"></i></a>
        </div>
        <div class="newsContent">
            <?php

        echo html_entity_decode($postxx->post_content);

        ?>
        </div>

        <div class="newsPrevLink flex">

            <?php
if ($key != 0) {
            ?>
            <a href="#" onClick="navigate_to_page('<?php echo $next; ?>');" class="aboutButton violetText center"><i
                    class="fa fa-arrow-left" aria-hidden="true"></i> Newer
                Posts </a>
            <?php
}
        ?>

            <?php
if ($key + 1 != $len) {
            ?>
            <a href="#" onClick="navigate_to_page('<?php echo $prev; ?>');" class="aboutButton violetText center">Older
                Posts <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
            <?php
}
        ?>
        </div>

    </div>
</section>
<script>
function back_to_all(){
    document.location.href = 'test-news-and-events/';
}


function navigate_to_page(slug) {

    document.location.href = slug+'/';
}
</script>

<?php
echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {

    }

    public function form($display_instance)
    {

    }

}
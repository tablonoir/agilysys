<?php

function agilysys_leader_video_slider()
{
    register_widget('agilysys_video_slider');
}

add_action('widgets_init', 'agilysys_leader_video_slider');

class agilysys_video_slider extends WP_Widget
{
    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {

        parent::__construct(false, $name = __('Agilysys Leaders in hospitality software', 'agilysys_text_domain'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');

        /* wp_register_script('youtube-js', 'http://www.youtube.com/iframe_api', array('jquery'), 'null', true);
        wp_enqueue_script('youtube-js'); */
        add_action('load-widgets.php', array(&$this, 'agilysys_color_picker_load'));

    }

    public function agilysys_color_picker_load()
    {
        wp_enqueue_style('wp-color-picker');
        wp_enqueue_script('wp-color-picker');
    }

    /**
     * @see WP_Widget::widget -- do not rename this         * This is for front end
     */
    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        extract($args);
        $max_entries_agilysys_video_slider_hospitality_software1 = esc_attr(get_option('rew_max'));

        $link_target = get_option('rew_link_target');
        $alignment = esc_attr(get_option('content_align'));
        $max_entries_agilysys_video_slider_hospitality_software = 100;

        $bg_color = !empty($instance['background_color']) ? $instance['background_color'] : '';

        ?>
<!-- <script src="https://www.youtube.com/iframe_api"></script> -->


<!-- partial:index.partial.html -->
<!-- Slider main container -->
<section class="leaderSolutionsWidget">


    <section class="homeLeaderSection center">
        <h2 class="greenText dinProStd aos-init aos-animate reviewsCaseHeader dinProStd whiteText">
            <?php echo $instance['review_title']; ?></h2>
        <p class="blackText dinproMed"><?php echo substr($instance['content'],0,305); ?></p>
    </section>
    <div class="homeLeaderBg" style="background: <?php echo $bg_color; ?>"></div>
    <div class="swiper-container">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">

            <?php

        $count = count($instance['type']);
        for ($i = 0; $i < $count; $i++) {

            $type = $instance['type'][$i];
            $image_uri = $instance['image_uri'][$i];

            $leadersDesc = $instance['leadersDesc'][$i];
            $leadersDesc2 = $instance['leadersDesc2'][$i];
            $leadersDesc3 = $instance['leadersDesc3'][$i];

            $video_uri = $instance['video_uri'][$i];
            $youtube_uri = $instance['youtube_uri'][$i];
            $image_uri_alt = $instance['image_uri_alt'][$i];
            ?>
            <!-- Slides -->
            <div class="swiper-slide">

                <?php

            if ($type == "image") {
                ?>
                <img src="<?php echo $image_uri; ?>" class="img-fluid" alt="<?php echo $image_uri_alt; ?>">
                <div class="leadersolutionContent dinProStd"><?php echo $leadersDesc; ?> </div>

                <?php
} elseif ($type == "video") {
                ?>
                <video width="100%" height="100%" autoplay muted>
                    <source src="<?php echo $video_uri; ?>" type="video/mp4">
                </video>
                <div class="leadersolutionContent dinProStd"><?php echo $leadersDesc2; ?> </div>
                <?php
} elseif ($type == "youtube") {

                ?>







                <iframe src='<?php echo $youtube_uri; ?>?enablejsapi=1&version=3&playerapiid=ytplayer'
                    id="youtube_<?php echo $i; ?>" class="yt_players" frameborder='0' width="669" height="382"
                    allowscriptaccess="always"></iframe>

                <!-- <div class="leadersolutionContent dinProStd"><?php //echo $leadersDesc3; ?> </div> -->
                <?php
}
            ?>
            </div>
            <?php

        }
        ?>

        </div>
        <!-- If we need navigation buttons -->
        <div class="swiper-button-next" style="background: <?php echo $bg_color; ?>">
            <img src="<?php echo get_template_directory_uri(); ?>/img/arrow-right-white.png" class="img-fluid"
                alt="arrow-left">
        </div>

        <div class="swiper-button-prev" style="background: <?php echo $bg_color; ?>">
            <img src="<?php echo get_template_directory_uri(); ?>/img/arrow-left-white.png" class="img-fluid"
                alt="arrow-right">
        </div>

    </div>
</section>






<?php
echo $args['after_widget'];

    }
    //Function widget ends here

    /**
     * @see WP_Widget::update -- do not rename this
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['review_title'] = strip_tags($new_instance['review_title']);
        $instance['content'] = strip_tags($new_instance['content']);
        $instance['background_color'] = $new_instance['background_color'];

        $count = count($new_instance['type']);

        for ($i = 0; $i < $count; $i++) {

            $instance['type'][$i] = $new_instance['type'][$i];
            $instance['leadersDesc'][$i] = $new_instance['leadersDesc'][$i];
            $instance['leadersDesc2'][$i] = $new_instance['leadersDesc2'][$i];
            $instance['leadersDesc3'][$i] = $new_instance['leadersDesc3'][$i];
            $instance['image_uri'][$i] = $new_instance['image_uri'][$i];
            $instance['image_uri_alt'][$i] = $new_instance['image_uri_alt'][$i];

            $instance['video_uri'][$i] = $new_instance['video_uri'][$i];
            $instance['youtube_uri'][$i] = $new_instance['youtube_uri'][$i];

        }
        return $instance;
    }
    //Function update ends here

    /**
     * @see WP_Widget::form -- do not rename this
     */
    public function form($display_instance)
    {

        ?>
<script>
jQuery(document).ready(function($) {
    $('.my-color-picker').wpColorPicker();
});
</script>
<?php

        $max_entries_agilysys_video_slider_hospitality_software = 100;

        $widget_add_id_hosipitality_software = $this->get_field_id('') . "add";
        $review_title = ($display_instance['review_title']);
        $contentDesc = ($display_instance['content']);
        $background_color = ($display_instance['background_color']);

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('review_title') . '"> ' . __('Title', 'agilysys_text_domain') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('review_title') . '" name="' . $this->get_field_name('review_title') . '" type="text" value="' . $review_title . '" />';
        $rew_html .= '</p><br><br><br>';
        $rew_html .= '<label for="' . $this->get_field_id('content') . '"> ' . __('Sub Title', 'agilysys_text_domain') . ' :</label>';
        $rew_html .= '<textarea rows="6" cols="35" id="' . $this->get_field_id('content') . '" name="' . $this->get_field_name('content') . '" >' . $contentDesc . '" </textarea>';
        $rew_html .= '</p><br><br><br>';

        $rew_html .= '<p><label for="' . $this->get_field_id('background_color') . '"> ' . __('Background Color', 'agilysys_text_domain') . ' :</label>';
        $rew_html .= '<input  class="my-color-picker" id="' . $this->get_field_id('background_color') . '" name="' . $this->get_field_name('background_color') . '" type="text" value="' . $background_color . '" />';
        $rew_html .= '</p>';

        $count = count($display_instance['type']);

        $rew_html .= '<div class="add_new_rowxx-input-containers"><div id="entries_agilysys_video_slider">';

        $rew_html .= '<input class="cnt909" id="cnt909" name="cnt" type="hidden" value="' . $count . '">';

        for ($i = 0; $i < $count; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'agilysys_text_domain') . ' </span>';
            $rew_html .= '<div class="entry-desc cf">';

            /**
             * Block Caption
             */

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('type' . $i) . '"> ' . __('Media Type', 'agilysys_text_domain') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_name('type' . $i) . '" name="' . $this->get_field_id('type[]') . '" onChange="show_hide_media_agilysys_video_slider(this.value,' . $i . ');">';
            $rew_html .= '<option value="">Please Select</option>';

            if ($display_instance['type'][$i] == "image") {
                $rew_html .= '<option value="image" selected="selected">Image</option>';
            } else {
                $rew_html .= '<option value="image">Image</option>';
            }

            if ($display_instance['type'][$i] == "video") {
                $rew_html .= '<option value="video" selected="selected">Video</option>';
            } else {
                $rew_html .= '<option value="video">Video</option>';
            }

            if ($display_instance['type'][$i] == "youtube") {
                $rew_html .= '<option value="youtube" selected="selected">Youtube url</option>';
            } else {
                $rew_html .= '<option value="youtube">Youtube url</option>';
            }

            $rew_html .= '</select>';
            $rew_html .= '</p>';

            $show1 = (!empty($display_instance['image_uri'][$i]) && $display_instance['type'][$i] == "image") ? '' : 'style="display:none;"';
            $rew_html .= '<div class="widg-img' . $i . '" ' . $show1 . '>';
            $rew_html .= '<p>';
            $rew_html .= '<label id="image_uri_agilysys_video_slider' . $i . '"></label><br><label for="' . $this->get_field_id('image_id' . $i) . '"> ' . __('Image', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<img class="' . $this->get_field_id('image_id' . $i) . '_media_image' . $i . ' custom_media_image' . $i . '" src="' . $display_instance['image_uri'][$i] . '" ' . $show1 . ' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id' . $i) . '_media_id' . $i . ' custom_media_id' . $i . '" name="' . $this->get_field_name('image_id[]') . '" id="' . $this->get_field_id('image_id' . $i) . '" value="' . $display_instance['image_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id' . $i) . '_media_url' . $i . ' custom_media_url' . $i . '" name="' . $this->get_field_name('image_uri[]') . '" id="' . $this->get_field_id('image_uri-' . $i) . '" value="' . $display_instance['image_uri'][$i] . '" />';
            $rew_html .= '<input type="button" value="Upload Image" class="button1 custom_media_upload' . $i . '" id="' . $this->get_field_id('image_id' . $i) . '"/>';
            $rew_html .= '</p>';
            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt' . $i) . '"> ' . __('Image Alt', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt' . $i) . '" name="' . $this->get_field_name('image_uri_alt[]') . '" type="text" value="' . $display_instance['image_uri_alt'][$i] . '" />';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('leadersDesc' . $i) . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('leadersDesc' . $i) . '" name="' . $this->get_field_name('leadersDesc[]') . '" type="text" value="' . $display_instance['leadersDesc'][$i] . '" />';
            $rew_html .= '</p>';

            $rew_html .= '</div>';

            ?>

<script>
function show_hide_media_agilysys_video_slider(value, id) {

console.log(value);

    if (value == "image") {

        jQuery('.widg-img' + id).show();
        jQuery('.widg-youtube' + id).hide();
        jQuery('.widg-video' + id).hide();

    } else if (value == "youtube") {
        jQuery('.widg-img' + id).hide();
        jQuery('.widg-youtube' + id).show();
        jQuery('.widg-video' + id).hide();
    } else if (value == "video") {
        jQuery('.widg-img' + id).hide();
        jQuery('.widg-youtube' + id).hide();
        jQuery('.widg-video' + id).show();
    }

}

jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    if (attachment.height == 698 && attachment.width == 1226) {
                        jQuery('.' + button_id_s + '_media_id<?php echo $i; ?>').val(attachment.id);
                        jQuery('.' + button_id_s + '_media_url<?php echo $i; ?>').val(attachment
                            .url);
                        jQuery('.' + button_id_s + '_media_image<?php echo $i; ?>').attr('src',
                            attachment.url).css('display', 'block');

                        jQuery('#image_uri_agilysys_video_slider<?php echo $i; ?>')
                            .html("");
                    } else {
                        jQuery('#image_uri_agilysys_video_slider<?php echo $i; ?>')
                            .html("Please Enter the correct Dimensions 1226x698").css(
                                'color', 'red');

                    }


                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload<?php echo $i; ?>');

});
</script>

<?php

            $show2 = (!empty($display_instance['video_uri'][$i]) && $display_instance['type'][$i] == "video") ? '' : 'style="display:none;"';
            $rew_html .= '<div class="widg-video' . $i . '" ' . $show2 . '>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('video_id' . $i) . '"> ' . __('Video', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<video class="' . $this->get_field_id('video_id' . $i) . '_media_videov' . $i . ' custom_media_videov' . $i . '" width="320" height="240" controls ' . $show2 . '><source src="' . $display_instance['video_uri'][$i] . '" type="video/mp4"></video>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('video_id' . $i) . '_media_idv' . $i . ' custom_media_idv' . $i . '" name="' . $this->get_field_name('video_id[]') . '" id="' . $this->get_field_id('video_id' . $i) . '" value="' . $display_instance['video_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('video_id' . $i) . '_media_urlv' . $i . ' custom_media_urlv' . $i . '" name="' . $this->get_field_name('video_uri[]') . '" id="' . $this->get_field_id('video_uri' . $i) . '" value="' . $display_instance['video_uri'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload Video" class="button custom_media_uploadv' . $i . '" id="' . $this->get_field_id('video_id' . $i) . '"/>';
            $rew_html .= '</p>';
            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('leadersDesc2' . $i) . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('leadersDesc2' . $i) . '" name="' . $this->get_field_name('leadersDesc2[]') . '" type="text" value="' . $display_instance['leadersDesc2'][$i] . '" />';
            $rew_html .= '</p>';
            $rew_html .= '</div>';

            $show3 = (!empty($display_instance['youtube_uri'][$i]) && $display_instance['type'][$i] == "youtube") ? '' : 'style="display:none;"';

            $rew_html .= '<div class="widg-youtube' . $i . '" ' . $show3 . '>';
            $rew_html .= '<input type="text"  name="' . $this->get_field_name('youtube_uri[]') . '" id="' . $this->get_field_id('media_uri' . $i) . '" value="' . $display_instance['youtube_uri'][$i] . '" />';
            $rew_html .= '</p>';
            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('leadersDesc3' . $i) . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('leadersDesc3' . $i) . '" name="' . $this->get_field_name('leadersDesc3[]') . '" type="text" value="' . $display_instance['leadersDesc3'][$i] . '" />';
            $rew_html .= '</p>';
            $rew_html .= '</div>';
            ?>

<script>
jQuery(document).ready(function() {




    function media_uploadv(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_uploadv<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {
                    jQuery('.' + button_id_s + '_media_idv<?php echo $i; ?>').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_urlv<?php echo $i; ?>').val(attachment.url);
                    jQuery('.' + button_id_s + '_media_videov<?php echo $i; ?>').attr('src',
                        attachment.url).css('display', 'block');
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_uploadv('.custom_media_uploadv<?php echo $i; ?>');


});
</script>

<?php

            $k = $i + 1;
            $rew_html .= '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' . $k . ');">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';

        }

        $rew_html .= '</div></div>';

        $rew_html .= '<div class="' . $widget_add_id_hosipitality_software . '" style="margin-bottom: 36px;text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;" onClick="add_new_row();">' . __('ADD ROW', 'AGILYSYS_TEXT_DOMAIN') . '</div>';
        ?>
<script>
function add_new_row() {
    var cnt = '';

    jQuery.each(jQuery("#entries_agilysys_video_slider .cnt909"), function() {
        if (jQuery(this).val() != '') {
            cnt = jQuery(this).val();
        }
    });






    cnt++;

    jQuery.each(jQuery("#entries_agilysys_video_slider .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(cnt);
        }
    });
    
    console.log(cnt);

    var new_row = '<div id="entry' + cnt +
        '"  class="entrys"><span class="entry-title" onclick = "slider(this);"><?php echo __('Add New Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span>';
    new_row += '<div class="entry-desc cf">';


    new_row += '<p>';
    new_row += '<label ><?php echo __('Media Type', 'agilysys_text_domain'); ?> :</label>';
    new_row +=
        '<select name="<?php echo $this->get_field_name('type[]'); ?>"  onChange="show_hide_media_agilysys_video_slider(this.value,' +
        cnt + ');">';
    new_row += '<option value="">Please Select</option>';
    new_row += '<option value="image">Image</option>';
    new_row += '<option value="video">Video</option>';
    new_row += '<option value="youtube">Youtube url</option>';
    new_row += '</select>';
    new_row += '</p>';




    new_row += '<div class="widg-img' + cnt + '" style="display:none;">';
    new_row += '<p>';
    new_row += '<label id="image_uri_agilysys_video_slider' + cnt +
        '"></label><br><label><?php echo __('Image', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
    new_row += '<img class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_image' + cnt +
        ' custom_media_image' + cnt + '" src="" style="display:none;" width=200" height="120"/>';
    new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_id' +
        cnt + ' custom_media_id' + cnt + '" name="<?php echo $this->get_field_name('image_id[]'); ?>"  />';
    new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_url' +
        cnt + ' custom_media_url' + cnt + '" name="<?php echo $this->get_field_name('image_uri[]'); ?>"  />';
    new_row += '<input type="button" value="Upload Image" class="button custom_media_upload' + cnt +
        '" id="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '"/>';
    new_row += '</p>';

    new_row += '<p>';
    new_row += '<label><?php echo __('Image Alt', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
    new_row += '<input name="<?php echo $this->get_field_name('image_uri_alt[]'); ?>" type="text"  />';
    new_row += '</p>';




    new_row += '<p>';
    new_row += '<label><?php echo __('Title', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
    new_row += '<input name="<?php echo $this->get_field_name('leadersDesc[]'); ?>" type="text"  />';
    new_row += '</p>';

    new_row += '</div>';


    jQuery(document).ready(function() {




            function media_upload(button_class) {
                var _custom_media = true,
                    _orig_send_attachment = wp.media.editor.send.attachment;
                jQuery('body').on('click', '.custom_media_upload' + cnt, function(e) {
                        var button_id = '#' + jQuery(this).attr('id');
                        var button_id_s = jQuery(this).attr('id');
                        console.log(button_id);
                        var self = jQuery(button_id);
                        var send_attachment_bkp = wp.media.editor.send.attachment;
                        var button = jQuery(button_id);
                        var id = button.attr('id').replace('_button', '');
                        _custom_media = true;

                        wp.media.editor.send.attachment = function(props, attachment) {
                            if (_custom_media) {

                                if (attachment.height == 698 && attachment.width == 1226) {
                                    jQuery('.' + button_id_s + '_media_id' + cnt).val(attachment.id);
                                    jQuery('.' + button_id_s + '_media_url' + cnt).val(attachment.url);
                                    jQuery('.' + button_id_s + '_media_image' + cnt).attr('src', attachment
                                        .url).css('display', 'block');

                                    jQuery('#image_uri_agilysys_video_slider' + cnt)
                                        .html("");
                                } else {
                                    jQuery('#image_uri_agilysys_video_slider' + cnt)
                                        .html("Please Enter the correct Dimensions 1226x698").css(
                                            'color', 'red');
                                }

                                } else {
                                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                                }
                            }
                            wp.media.editor.open(button);
                            return false;
                        });
                }
                media_upload('.custom_media_upload' + cnt);

            });






        new_row += '<div class="widg-video' + cnt + '" style="display:none;">';

        new_row += '<p>'; new_row +=
        '<label><?php echo __('Video', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>'; new_row +=
        '<video class="<?php echo $this->get_field_id('video_id'); ?>' + cnt + '_media_videov' + cnt +
        ' custom_media_videov' + cnt +
        '" width="320" height="240" controls style="display:none;"><source src="" type="video/mp4"></video>'; new_row +=
        '<input type="hidden" class="<?php echo $this->get_field_id('video_id'); ?>' + cnt + '_media_idv' +
        cnt + ' custom_media_idv' + cnt +
        '" name="<?php echo $this->get_field_name('video_id[]'); ?>"  />'; new_row +=
        '<input type="hidden" class="<?php echo $this->get_field_id('video_id'); ?>' + cnt + '_media_urlv' +
        cnt + ' custom_media_urlv' + cnt +
        '" name="<?php echo $this->get_field_name('video_uri[]'); ?>">'; new_row +=
        '<input type="button" value="Upload Video" class="button custom_media_uploadv' + cnt +
        '" id="<?php echo $this->get_field_id('video_id'); ?>' + cnt + '"/>'; new_row += '</p>'; new_row +=
        '<p>'; new_row += '<label><?php echo __('Title', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>'; new_row +=
        '<input  name="<?php echo $this->get_field_name('leadersDesc2[]'); ?>" type="text" value="" />'; new_row +=
        '</p>'; new_row += '</div>';


        jQuery(document).ready(function() {




            function media_uploadv(button_class) {
                var _custom_media = true,
                    _orig_send_attachment = wp.media.editor.send.attachment;
                jQuery('body').on('click', '.custom_media_uploadv' + cnt, function(e) {
                    var button_id = '#' + jQuery(this).attr('id');
                    var button_id_s = jQuery(this).attr('id');
                    console.log(button_id);
                    var self = jQuery(button_id);
                    var send_attachment_bkp = wp.media.editor.send.attachment;
                    var button = jQuery(button_id);
                    var id = button.attr('id').replace('_button', '');
                    _custom_media = true;

                    wp.media.editor.send.attachment = function(props, attachment) {
                        if (_custom_media) {
                            jQuery('.' + button_id_s + '_media_idv' + cnt).val(attachment.id);
                            jQuery('.' + button_id_s + '_media_urlv' + cnt).val(attachment.url);
                            jQuery('.' + button_id_s + '_media_videov' + cnt).attr('src',
                                attachment.url).css('display', 'block');
                        } else {
                            return _orig_send_attachment.apply(button_id, [props, attachment]);
                        }
                    }
                    wp.media.editor.open(button);
                    return false;
                });
            }
            media_uploadv('.custom_media_uploadv' + cnt);


        });



        new_row += '<div class="widg-youtube' + cnt + '" style="display:none;">'; new_row +=
        '<input type="text"  name="<?php echo $this->get_field_name('youtube_uri[]'); ?>" />'; new_row +=
        '</p>'; new_row += '<p>'; new_row +=
        '<label> <?php echo __('Title', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>'; new_row +=
        '<input  name="<?php echo $this->get_field_name('leadersDesc3[]'); ?>" type="text" />'; new_row +=
        '</p>'; new_row += '</div>';


        var new_cnt = cnt;

        new_row += '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' + new_cnt +
        ');"><?php echo __('Delete Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span></a></p>'; new_row += '</div></div>';

        jQuery('.add_new_rowxx-input-containers #entries_agilysys_video_slider').append(new_row);




    }


    function show_hide_media_agilysys_video_slider(value, id) {

console.log(value);

        if (value == "image") {

            jQuery('.widg-img' + id).show();
            jQuery('.widg-youtube' + id).hide();
            jQuery('.widg-video' + id).hide();

        } else if (value == "youtube") {
            jQuery('.widg-img' + id).hide();
            jQuery('.widg-youtube' + id).show();
            jQuery('.widg-video' + id).hide();
        } else if (value == "video") {
            jQuery('.widg-img' + id).hide();
            jQuery('.widg-youtube' + id).hide();
            jQuery('.widg-video' + id).show();
        }

    }



    function delete_row(cnt) {
        jQuery.each(jQuery(".add_new_rowxx-input-containers #entries_agilysys_video_slider"), function() {
            jQuery(' #entry' + cnt).remove();
        });
        var last_cnt = 0;

        jQuery.each(jQuery("#entries_agilysys_video_slider .cnt909"), function() {
            if (jQuery(this).val() != '') {
                last_cnt = jQuery(this).val();
            }
        });

        last_cnt--;
        jQuery.each(jQuery("#entries_agilysys_video_slider .cnt909"), function() {
            if (jQuery(this).val() != '') {
                jQuery(this).val(last_cnt);
            }
        });
        jQuery(".what_makes_rows").val(last_cnt);
        jQuery('.what_makes_rows').trigger('change');

    }
</script>
<style>
#rew_container_agilysys_video_slider .button1 {
    color: #0071a1;
    border-color: #0071a1;
    background: #f3f5f6;
    vertical-align: top;
    display: inline-block;
    text-decoration: none;
    font-size: 13px;
    line-height: 2.15384615;
    min-height: 30px;
    margin: 10px;
    padding: 0 10px;
    cursor: pointer;
    border-width: 1px;
    border-style: solid;
    -webkit-appearance: none;
    border-radius: 3px;
    white-space: nowrap;
    box-sizing: border-box;
    margin-bottom: 40px !important;
}


#rew_container_agilysys_video_slider .wp-picker-container {

    margin-top: 20px !important;
    margin-bottom: 10px !important;
}

#rew_container_agilysys_video_slider p {

    padding: 20px;
}


.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

<?php echo '.'. $widget_add_id_hosipitality_software;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}

#entries_agilysys_video_slider #remove-img-bg {
    background: url('<?php echo ZWREW_URL; ?>assets/images/deleteimg.png') center center no-repeat;
    width: 20px;
    height: 22px;
    display: none;
}

#entries_agilysys_video_slider {
    padding: 10px 0 0;
}

#entries_agilysys_video_slider .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_video_slider .entrys:first-child {
    margin: 0;
}

#entries_agilysys_video_slider .delete-row {
    margin-top: 30px !important;
    float: right !important;
    text-decoration: underline;
    color: red;
    margin-left:50%;
}

#entries_agilysys_video_slider .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_video_slider .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_video_slider .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_video_slider .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_video_slider #entries_agilysys_video_slider plast label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}

#rew_container_agilysys_video_slider select {
    float: left;
    width: 60%;
    margin-top: 20px !important;
    margin-bottom: 10px !important;
}

#rew_container_agilysys_video_slider img {

    margin-top: 40px !important;
    float: left;
    margin-bottom: 40px !important;

}

#rew_container_agilysys_video_slider input,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_video_slider label {
    width: 40%;
    float: left;
}
</style>
<div id="rew_container_agilysys_video_slider">
    <?php echo $rew_html; ?>
</div>
<?php
} //Function form ends here
} // class ends here
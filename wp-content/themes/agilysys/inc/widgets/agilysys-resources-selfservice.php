<?php

function agilysys_solution_widget()
{
    register_widget('agilysys_solution_orange_widget');
}
add_action('widgets_init', 'agilysys_solution_widget');

class agilysys_solution_orange_widget extends WP_Widget
{
    /**
     * constructor -- name this the same as the class above
     */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Solution Orange Widget', 'agilysys_text_domain'));
    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        ?>


<section class="rguestSelfSection">

    <div class="row">
        <div class="rguestSelfTitle col-12 col-sm-12 col-md-3 col-lg-3" data-aos="fade-right" data-aos-delay="300" data-aos-duration="400" data-aos-once="true">
            <h2 class="dinProStd greenText"><?php echo $instance['solution_title']; ?></h2>
        </div>

        <div class="rguestSelfIcon orangeBG col-12 col-sm-12 col-md-9 col-lg-9">
            <div class="row">

                <?php
$soultion_count = $instance['solution_rows'];
//                echo $diverse_count;

        $count = count($instance['image_uri']);
        for ($i = 0; $i < $count; $i++) {

            //Content
            //$caption = esc_attr( $instance['solution_img_title-' . $i] );
            $solution_img_content = esc_attr($instance['solution_img_content'][$i]);

            //Front Image
            $image_uri = esc_url($instance['image_uri'][$i]);

            $image_uri_alt = $instance['image_uri_alt'][$i];

            ?>

                <div class="rguestSelfIconBox flex center col-12 col-sm-4 col-md-4 col-lg-4" data-aos="zoom-out" data-aos-delay="300" data-aos-duration="500" data-aos-once="true">
                    <div class="rguestSelfIconImg">
                        <img class="img-fluid" src="<?php echo $image_uri; ?>" alt="<?php echo $image_uri_alt; ?>" />
                    </div>
                    <div class="rguestSelfIconText">
                        <h4 class="dinProStd whiteText"><?php echo $solution_img_content; ?> </h4>
                    </div>
                </div>

                <?php

        }
        ?>
            </div>
        </div>
    </div>
</section>




<?php
echo $args['after_widget'];
    }

    //Function form ends here

    public function update($new_instance, $old_instance)
    {

        $instance = array();
        $instance['solution_title'] = strip_tags($new_instance['solution_title']);
        $instance['solution_rows'] = strip_tags($new_instance['solution_rows']);
        $instance['solution_icon_image'] = strip_tags($new_instance['solution_icon_image']);
        $instance['solution_img_content'] = strip_tags($new_instance['solution_img_content']);

        $count = count($new_instance['image_uri']);

        for ($i = 0; $i < $count; $i++) {

            $instance['image_uri'][$i] = strip_tags($new_instance['image_uri'][$i]);
            $instance['image_uri_alt'][$i] = strip_tags($new_instance['image_uri_alt'][$i]);

            $instance['solution_img_content'][$i] = strip_tags($new_instance['solution_img_content'][$i]);
        }
        return $instance;
    }

    public function form($display_instance)
    {

        //$widget_add_id_diverse = $this->id . "-add";
        $widget_add_id_solution_orange = $this->get_field_id('') . "add_agilysys_solution_orange_widget";

        $solution_title = ($display_instance['solution_title']);
        $solution_rows = ($display_instance['solution_rows']);
        $solution_icon_image = ($display_instance['solution_icon_image']);
        $solution_img_content = ($display_instance['solution_img_content']);

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('solution_title') . '"> ' . __('Title', 'agilysys_solution') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('solution_title') . '" name="' . $this->get_field_name('solution_title') . '" type="text" value="' . $solution_title . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<label for="' . $this->get_field_id('solution_rows') . '"> ' . __('No. of rows do you want to display*', 'agilysys_solution') . ' :</label>';
        $rew_html .= '<input class="solution_rows" id="' . $this->get_field_name('solution_rows') . '" name="' . $this->get_field_name('solution_rows') . '" type="number" value="' . $solution_rows . '" />';
        $rew_html .= '</p>';

        $count = count($display_instance['image_uri']);

        $rew_html .= '<div class="add_new_rowxx-input-containers"><div id="entries_agilysys_solution_orange_widget">';

        $rew_html .= '<input class="cnt909" id="cnt909" name="cnt" type="hidden" value="' . $count . '">';

        for ($i = 0; $i < $count; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'agilysys_solution') . ' </span>';

            $rew_html .= '<div class="entry-desc cf">';

            $rew_html .= '<div class="widg-img' . $i . '">';
            $show1 = (empty($display_instance['image_uri'][$i])) ? 'style="display:none;"' : '';
            $rew_html .= '<label id="image_uri_agilysys_solution_orange_widget'.$i.'"></label><br><img class="' . $this->get_field_id('image_id' . $i) . '_media_image' . $i . ' custom_media_image' . $i . '" src="' . $display_instance['image_uri'][$i] . '" '.$show1.' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id' . $i) . '_media_id' . $i . ' custom_media_id' . $i . '" name="' . $this->get_field_name('image_id[]') . '" id="' . $this->get_field_id('image_id' . $i) . '" value="' . $display_instance['image_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id' . $i) . '_media_url' . $i . ' custom_media_url' . $i . '" name="' . $this->get_field_name('image_uri[]' . $i) . '" id="' . $this->get_field_id('image_uri' . $i) . '" value="' . $display_instance['image_uri'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_upload' . $i . '" id="' . $this->get_field_id('image_id' . $i) . '"/>';

            $rew_html .= '</div><br><br>';

            ?>
<script>
jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    if (attachment.height == 1080 && attachment.width == 1920) {
                        jQuery('.' + button_id_s + '_media_id<?php echo $i; ?>').val(attachment.id);
                        jQuery('.' + button_id_s + '_media_url<?php echo $i; ?>').val(attachment
                            .url);
                        jQuery('.' + button_id_s + '_media_image<?php echo $i; ?>').attr('src',
                            attachment.url).css('display', 'block');

                        jQuery('#image_uri_agilysys_solution_orange_widget<?php echo $i; ?>')
                            .html("");
                    } else {
                        jQuery('#image_uri_agilysys_solution_orange_widget<?php echo $i; ?>')
                            .html("Please Enter the correct Dimensions 1920x1080").css(
                                'color', 'red');

                    }
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload<?php echo $i; ?>');

});
</script>


<?php

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt' . $i) . '"> ' . __('Image Alt', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt' . $i) . '" name="' . $this->get_field_name('image_uri_alt[]') . '" type="text" value="' . $display_instance['image_uri_alt'][$i] . '">';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('solution_img_content' . $i) . '"> ' . __('Image Content', 'agilysys_solution') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('solution_img_content' . $i) . '" name="' . $this->get_field_name('solution_img_content[]') . '" type="text" value="' . $display_instance['solution_img_content'][$i] . '">';
            $rew_html .= '</p>';

            $k = $i + 1;
            $rew_html .= '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' . $k . ');">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';

        }
        $rew_html .= '</div></div>';

        $rew_html .= '<div class="' . $widget_add_id_solution_orange . '" style="margin-bottom: 36px;text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;"  onClick="add_new_row();">' . __('ADD ROW', 'AGILYSYS_TEXT_DOMAIN') . '</div>';

        ?>
<script>
function add_new_row() {
    var cnt = '';

    jQuery.each(jQuery("#entries_agilysys_solution_orange_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            cnt = jQuery(this).val();
        }
    });




    var solution_rows = jQuery('.solution_rows').val();



console.log(cnt);

    if (parseInt(cnt) < parseInt(solution_rows)) {

        cnt++;

        jQuery.each(jQuery("#entries_agilysys_solution_orange_widget .cnt909"), function() {
            if (jQuery(this).val() != '') {
                jQuery(this).val(cnt);
            }
        });

        var new_row = '<div id="entry' + cnt +
            '"  class="entrys"><span class="entry-title" onclick = "slider(this);"><?php echo __('Add New Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span>';
        new_row += '<div class="entry-desc cf">';


        new_row += '<div class="widg-img' + cnt + '">';

        new_row += '<label id="image_uri_agilysys_solution_orange_widget' + cnt +
            '"></label><br><img class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_image' + cnt +
            ' custom_media_image' + cnt + '" style="display:none;" width=200" height="120"/>';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_id' +
            cnt + ' custom_media_id' + cnt + '" name="<?php echo $this->get_field_name('image_id[]'); ?>" />';
        new_row += '<input type="hidden" class="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '_media_url' +
            cnt + ' custom_media_url' + cnt + '" name="<?php echo $this->get_field_name('image_uri[]'); ?>">';
        new_row += '<input type="button" value="Upload Image" class="button custom_media_upload' + cnt +
            '" id="<?php echo $this->get_field_id('image_id'); ?>' + cnt + '"/>';

        new_row += '</div><br><br>';


        jQuery(document).ready(function() {




            function media_upload(button_class) {
                var _custom_media = true,
                    _orig_send_attachment = wp.media.editor.send.attachment;
                jQuery('body').on('click', '.custom_media_upload' + cnt, function(e) {
                    var button_id = '#' + jQuery(this).attr('id');
                    var button_id_s = jQuery(this).attr('id');
                    console.log(button_id);
                    var self = jQuery(button_id);
                    var send_attachment_bkp = wp.media.editor.send.attachment;
                    var button = jQuery(button_id);
                    var id = button.attr('id').replace('_button', '');
                    _custom_media = true;

                    wp.media.editor.send.attachment = function(props, attachment) {
                        if (_custom_media) {

                            //if (attachment.height == 1080 && attachment.width == 1920) {
                                jQuery('.' + button_id_s + '_media_id' + cnt).val(attachment.id);
                                jQuery('.' + button_id_s + '_media_url' + cnt).val(attachment.url);
                                jQuery('.' + button_id_s + '_media_image' + cnt).attr('src',
                                    attachment.url).css('display', 'block');

                                /*jQuery('#image_uri_agilysys_solution_orange_widget' + cnt)
                                    .html("");
                            } else {
                                jQuery('#image_uri_agilysys_solution_orange_widget' + cnt)
                                    .html("Please Enter the correct Dimensions 1920x1080").css(
                                        'color', 'red');

                            }
                            */
                        } else {
                            return _orig_send_attachment.apply(button_id, [props, attachment]);
                        }
                    }
                    wp.media.editor.open(button);
                    return false;
                });
            }
            media_upload('.custom_media_upload' + cnt);

        });



        new_row += '<p>';
        new_row += '<label><?php echo __('Image Content', 'agilysys_solution'); ?>:</label>';
        new_row += '<input  name="<?php echo $this->get_field_name('solution_img_content[]'); ?>" type="text">';
        new_row += '</p>';

        new_row += '<p>';
        new_row += '<label for=""><?php echo __('Image Alt', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
        new_row += '<input class="" name="<?php echo esc_attr($this->get_field_name('image_uri_alt[]')); ?>" type="text" value="">';
        new_row += '</p>';


        var new_cnt = cnt;

        new_row += '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' + new_cnt +
            ');"><?php echo __('Delete Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span></a></p>';
        new_row += '</div></div>';

        jQuery('.add_new_rowxx-input-containers #entries_agilysys_solution_orange_widget').append(new_row);

    }


}

function show_hide_div(val, i) {
console.log(val);
    if (val == 'page') {
        jQuery("#page_div" + i).show();
        jQuery("#link_div" + i).hide();
    } else if (val == 'link') {
        jQuery("#page_div" + i).hide();
        jQuery("#link_div" + i).show();
    }

}

function delete_row(cnt) {
    jQuery.each(jQuery(".add_new_rowxx-input-containers #entries_agilysys_solution_orange_widget"), function() {
        jQuery(' #entry' + cnt).remove();
    });
    var last_cnt = 0;

    jQuery.each(jQuery("#entries_agilysys_solution_orange_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            last_cnt = jQuery(this).val();
        }
    });

    last_cnt--;
    jQuery.each(jQuery("#entries_agilysys_solution_orange_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(last_cnt);
        }
    });
    jQuery(".solution_rows").val(last_cnt);
    jQuery('.solution_rows').trigger('change');

}
</script>
<style>
#rew_container_agilysys_solution_orange_widget p{
    padding:10px;
}
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_solution_orange_widget input {
    width: 60%;
}

#rew_container_agilysys_solution_orange_widget input,
select,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_solution_orange_widget label {
    width: 40%;
}

<?php echo '.'. $widget_add_id_diverse;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}

#entries_agilysys_solution_orange_widget #remove-img-bg {
    background: url('<?php echo ZWREW_URL; ?>assets/images/deleteimg.png') center center no-repeat;
    width: 20px;
    height: 22px;
    display: none;
}

#entries_agilysys_solution_orange_widget {
    padding: 10px 0 0;
}

#entries_agilysys_solution_orange_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_solution_orange_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_solution_orange_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_solution_orange_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_solution_orange_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_solution_orange_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_solution_orange_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_solution_orange_widget #entries_agilysys_solution_orange_widget plast label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_solution_orange_widget">
    <?php echo $rew_html; ?>
</div>
<?php
} //Function form ends here
}
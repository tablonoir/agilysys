<?php

add_action('widgets_init', 'load_agilysys_footer_background_image_widget');

function load_agilysys_footer_background_image_widget()
{
    register_widget('agilysys_footer_background_image_widget');
}

class agilysys_footer_background_image_widget extends WP_Widget
{
/**
 * constructor -- name this the same as the class above
 */

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Footer Background Image Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');

    }

public function widget($args, $instance)
    {
          echo $args['before_widget'];
        ?>

<section class="homeFooterImg" style="background: url(<?php  echo $instance['image_uri']; ?>); background-repeat:no-repeat;background-size: cover; ">
        <!--<div class="homeFooterImgTopBG"></div>-->
    <div class="homeFooterImgBG"></div>

</section>


<?php
echo $args['after_widget'];
}

    public function update($new_instance, $old_instance)
    {
        $instance = array();
      
        $instance['image_uri'] = strip_tags($new_instance['image_uri']);
       
        return $instance;
    }

    public function form($display_instance)
    {
        ?>
<div id="agilysys_footer_background_image_widget">
<label class="widg-label widg-img-label" for="<?php echo $this->get_field_id( 'image_uri' ); ?>">Image</label>
<div class="widg-img">
    <img class="<?php echo $this->get_field_id( 'image_id' ); ?>_media_image custom_media_image" src="<?php if( !empty( $display_instance['image_uri'] ) ){echo $display_instance['image_uri'];} ?>" height="120" width="320"/>
    <input input type="hidden" type="text" class="<?php echo $this->get_field_id( 'image_id' ); ?>_media_id custom_media_id" name="<?php echo $this->get_field_name( 'image_id' ); ?>" id="<?php echo $this->get_field_id( 'image_id' ); ?>" value="<?php echo $instance['image_id']; ?>" />
    <input type="hidden" class="<?php echo $this->get_field_id( 'image_id' ); ?>_media_url custom_media_url" name="<?php echo $this->get_field_name( 'image_uri' ); ?>" id="<?php echo $this->get_field_id( 'image_uri' ); ?>" value="<?php echo $instance['image_uri']; ?>" >
    <input type="button" value="Upload Image" class="button custom_media_upload" id="<?php echo $this->get_field_id( 'image_id' ); ?>"/>
</div>
</div>

<style>
  #agilysys_footer_background_image_widget  .widg-label{
        margin-top:20px;
        margin-bottom:40px;
    }
    
  #agilysys_footer_background_image_widget  .widg-img img {
              margin-left:-100px !important; 
              margin-top:10px !important; 
    }
    
   #agilysys_footer_background_image_widget .button{
        margin-top:150px !important;
        float:left;
       
    }
</style>

<script>

jQuery(document ).ready( function(){
    function media_upload( button_class ) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
         jQuery('body').on('click','.custom_media_upload',function(e) {
            var button_id ='#'+jQuery(this).attr( 'id' );
            var button_id_s = jQuery(this).attr( 'id' ); 
            console.log(button_id); 
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr( 'id' ).replace( '_button', '' );
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment ){
                if ( _custom_media ) {
                    jQuery( '.' + button_id_s + '_media_id' ).val(attachment.id); 
                    jQuery( '.' + button_id_s + '_media_url' ).val(attachment.url);
                    jQuery( '.' + button_id_s + '_media_image' ).attr( 'src',attachment.url).css( 'display','block' ); 
                } else {
                    return _orig_send_attachment.apply( button_id, [props, attachment] );
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload( '.custom_media_upload' );

});

</script>



        <?php

    }
}
<?php

function load_agilysys_product_resource_point_of_sales_guides_widget()
{
    register_widget('agilysys_product_resource_point_of_sales_guides_widget');
}

add_action('widgets_init', 'load_agilysys_product_resource_point_of_sales_guides_widget');

class agilysys_product_resource_point_of_sales_guides_widget extends WP_Widget
{
/**
 * constructor -- name this the same as the class above
 */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Product Resource Point Of Sales Guides Widget', 'AGILYSYS_TEXT_DOMAIN'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {

       echo $args['before_widget'];
        
        $section_title = strip_tags($instance['section_title']);
        $sub_title = strip_tags($instance['sub_title']);
        $image_uri = strip_tags($instance['image_uri']);

        $image_uri = preg_replace("/\s+/", "", $image_uri);

        $description = strip_tags($instance['description']);
        $button_text = strip_tags($instance['button_text']);
        $pdf_uri = strip_tags($instance['pdf_uri']);

        ?>


<section class="propetySaleGuidesSection whiteText" style="background:url(<?php echo $image_uri; ?>)">
    <div class="propetySaleGuidesContent center">
        <h2 class="dinProStd h2"><?php echo $section_title; ?></h2>
        <h5 class="dinProStd"><?php echo $sub_title; ?></h5>
        <p><?php echo $description; ?></p>
        <a href="<?php echo $pdf_uri; ?>" class="homeBannerButton whiteText"> <?php echo $button_text; ?> <i class="fa fa-arrow-right"
                aria-hidden="true"></i></a>
    </div>
</section>


<?php
echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['section_title'] = strip_tags($new_instance['section_title']);
        $instance['sub_title'] = strip_tags($new_instance['sub_title']);
        $instance['image_uri'] = strip_tags($new_instance['image_uri']);

        $instance['description'] = strip_tags($new_instance['description']);
        $instance['button_text'] = strip_tags($new_instance['button_text']);
        $instance['pdf_uri'] = $new_instance['pdf_uri'];
        return $instance;
    }

    public function form($display_instance)
    {
        $max_entries_agilysys_product_resource_point_of_sales_guides_widget_slider_image = 15;

        $widget_add_id_slider = $this->get_field_id('') . "add_agilysys_product_resource_point_of_sales_guides_widget";

        $section_title = ($display_instance['section_title']);
        $sub_title = ($display_instance['sub_title']);
        $button_text = $display_instance['button_text'];
        $url = $display_instance['url'];
        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('section_title') . '"> ' . __('Section Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('section_title') . '" name="' . $this->get_field_name('section_title') . '" type="text" value="' . $section_title . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('sub_title') . '"> ' . __('Sub Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input class="sub_title" id="' . $this->get_field_name('sub_title') . '" name="' . $this->get_field_name('sub_title') . '" type="text" value="' . $sub_title . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('description') . '"> ' . __('Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input class="description" id="' . $this->get_field_name('description') . '" name="' . $this->get_field_name('description') . '" type="text" value="' . $sub_title . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('button_text') . '"> ' . __('Button Text', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input class="button_text" id="' . $this->get_field_name('button_text') . '" name="' . $this->get_field_name('button_text') . '" type="text" value="' . $button_text . '" />';
        $rew_html .= '</p>';

        $rew_html .= '<div class="widg-img">';
        $show1 = (empty($display_instance['pdf_uri'])) ? 'style="display:none;"' : '';
        $rew_html .= '<span style="color:green;display:none;" class="' . $this->get_field_id('pdf_id') . '_media_imagexx">File Uploaded Successfully</span>';
        $rew_html .= '<span style="color:green;display:none;" class="' . $this->get_field_id('pdf_id') . '_media_image123xx">File Uploaded Successfully</span>';
        $rew_html .= '<input type="hidden" class="' . $this->get_field_id('pdf_id') . '_media_idxx custom_media_idxx" name="' . $this->get_field_name('pdf_id') . '" id="' . $this->get_field_id('pdf_id') . '" value="' . $display_instance['pdf_id'] . '" />';
        $rew_html .= '<input type="hidden" class="' . $this->get_field_id('pdf_id') . '_media_urlxx custom_media_urlxx" name="' . $this->get_field_name('pdf_uri') . '" id="' . $this->get_field_id('pdf_uri') . '" value="' . $display_instance['pdf_uri'] . '">';
        $rew_html .= '<input type="button" value="Upload PDF or ZIP File" class="button custom_media_uploadxx" id="' . $this->get_field_id('pdf_id') . '"/>';

        $rew_html .= '</div>';

        ?>
<script>
jQuery(document).ready(function() {
    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_uploadxx', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {
                    jQuery('.' + button_id_s + '_media_idxx').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_urlxx').val(attachment.url);
                    jQuery('.' + button_id_s + '_media_imagexx').css(
                        'display', 'block');
                jQuery('.' + button_id_s + '_media_image123xx').css(
                        'display', 'block').html(attachment.url);

                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_uploadxx');

});
</script>


<?php

        $rew_html .= '<div class="widg-img">';
        $show1 = (empty($display_instance['image_uri'])) ? 'style="display:none;"' : '';
        $rew_html .= '<img class="' . $this->get_field_id('image_id') . '_media_image custom_media_image" src="' . $display_instance['image_uri'] . '" ' . $show1 . ' width=200" height="120"/>';
        $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id') . '_media_id custom_media_id" name="' . $this->get_field_name('image_id') . '" id="' . $this->get_field_id('image_id') . '" value="' . $display_instance['image_id'] . '" />';
        $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id') . '_media_url custom_media_url" name="' . $this->get_field_name('image_uri') . '" id="' . $this->get_field_id('image_uri') . '" value="' . $display_instance['image_uri'] . '">';
        $rew_html .= '<input type="button" value="Upload Background Image" class="button custom_media_upload" id="' . $this->get_field_id('image_id') . '"/>';

        $rew_html .= '</div>';

        ?>

<script>
jQuery(document).ready(function() {
    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {
                    jQuery('.' + button_id_s + '_media_id').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_url').val(attachment.url);
                    jQuery('.' + button_id_s + '_media_image').attr('src', attachment.url).css(
                        'display', 'block');
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload');

});
</script>

<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_product_resource_point_of_sales_guides_widget input,
select,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_product_resource_point_of_sales_guides_widget label {
    width: 40%;
}

#rew_container_agilysys_product_resource_point_of_sales_guides_widget p {
    padding:20px;
}


<?php echo '.' . $widget_add_id_slider;

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}



#entries_agilysys_product_resource_point_of_sales_guides_widget {
    padding: 10px 0 0;
}

#entries_agilysys_product_resource_point_of_sales_guides_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_product_resource_point_of_sales_guides_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_product_resource_point_of_sales_guides_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_product_resource_point_of_sales_guides_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_product_resource_point_of_sales_guides_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_product_resource_point_of_sales_guides_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_product_resource_point_of_sales_guides_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_product_resource_point_of_sales_guides_widget #entries_agilysys_product_resource_point_of_sales_guides_widget p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_product_resource_point_of_sales_guides_widget">
    <?php echo $rew_html; ?>
</div>
<?php
}

}
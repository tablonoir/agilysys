<?php

function load_agilysys_webinars_info_widget()
{
    register_widget('agilysys_webinars_info_widget');
}
add_action('widgets_init', 'load_agilysys_webinars_info_widget');

class agilysys_webinars_info_widget extends WP_Widget
{

    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Webinars Info Widget', 'AGILYSYS_TEXT_DOMAIN'));

        wp_enqueue_media();
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        $webinars_info_title = ($instance['webinars_info_title']);

        $industries_arr = array(
            1 => 'Hotels & Resorts',
            2 => 'Casino Resorts',
            3 => 'Tribal Gaming',
            4 => 'Cruise Lines',
            5 => 'Foodservice Management',
            6 => 'Sports & Entertainment',
            7 => 'Restaurants',
            8 => 'Spa',
            9 => 'Golf',
        );

        $products_arr = array(
            'PROPERTY MANAGEMENT' => array(
                1 => 'Agilysys Stay',
                2 => 'Agilysys LMS',
                3 => 'Agilysys Visual One PMS',
                4 => 'Agilysys Sales and Catering',
                5 => 'rGuest® Book',
                6 => 'rGuest® Express Kiosk',
                7 => 'rGuest® Express Mobile',
                8 => 'rGuest® Service',
                9 => 'b4checkin',
            ),

            'POINT OF SALE' => array(
                10 => 'Agilysys InfoGenesis',
                11 => 'IG Flex',
                12 => 'IG Buy',
                13 => 'IG OnDemand',
            ),
            'PAYMENT SOLUTION' => array(
                14 => 'Agilysys Pay',

            ),
            'ANALYTICS & MARKETING LOYALTY' => array(
                15 => 'Agilysys Analyze',
            ),
            'INVENTORY & PROCUREMENT' => array(
                16 => 'Agilysys Eatec',
                17 => 'Agilysys SWS',

            ),
            'RESERVATIONS AND TABLE MANAGEMENT' => array(
                18 => 'Agilysys Seat',
            ),
            'ACTIVITY SCHEDULING' => array(
                19 => 'Agilysys Golf',
                20 => 'Agilysys Spa',
            ),
            'DOCUMENT MANAGEMENT' => array(
                21 => 'Agilysys DataMagine',
            ),
            'SERVICES' => array(
                22 => 'Professional Services',
            ),

        );
        ?>

<style>
.loader_agilysys_webinars_info_widget {
    width: 80px;
    height: 80px;
    background: #fff;
    border: 2px solid #f3f3f3;
    border-top: 3px solid #008000;
    border-radius: 100%;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 45%;
    margin: auto;
    animation: spin 1s infinite;

}

@keyframes spin {
    from {
        transform: rotate(0deg);
    }

    to {
        transform: rotate(360deg);
    }
}
</style>

<div id="overlay_agilysys_webinars_info_widget">
    <div class="loader_agilysys_webinars_info_widget"></div>
</div>
<section class="webInnarSection orangeBG">
    <h2 class="h2 whiteText dinProStd center"><?php echo $webinars_info_title; ?></h2>

    <div class="videoFilter flex whiteText">
        <div class="videoFilterBox">
            <p>FILTER BY :</p>
        </div>
        <div class="videoFilterBox">
            <select class="solPartnerCat filter1" name="industries" id="industries_agilysys_webinars_info_widget"
                onChange="fetch_data_agilysys_webinars_info_widget(1);">
                <option value="">Select Category</option>
                <?php

        foreach ($industries_arr as $key => $val) {

            ?>
                <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                <?php
}
        ?>
            </select>
        </div>
        <div class="videoFilterBox">
            <select class="solPartnerCat filter1" name="products" id="products_agilysys_webinars_info_widget"
                onChange="fetch_data_agilysys_webinars_info_widget(1);">
                <option value="">Select Products</option>
                <?php

        foreach ($products_arr as $key => $val) {

            if (is_array($val)) {
                ?>
                <optgroup label="<?php echo $key; ?>">
                    <?php
foreach ($val as $k => $v) {
                    ?>
                    <option value="<?php echo $k; ?>"><?php echo $v; ?></option>
                    <?php
}
                ?>
                </optgroup>
                <?php
}

        }
        ?>
            </select>
        </div>
        <!--        <div class="videoFilterBox">
            <select class="filterThree">
                <option value="0">Case Studies</option>
                <option value="cat-red">Red</option>
                <option value="cat-blue">Blue</option>
                <option value="cat-green">Green</option>
            </select>
        </div>-->
        <div class="videoFilterBox videoClear">
            <p>CLEAR FILTERS</p>
        </div>
        <!--        <div class="videoSearch">
            <i class="fa fa-search" aria-hidden="true"></i>
        </div>-->
    </div>

    <div class="webInnarVideo flex" id="articleList_agilysys_webinars_info_widget">



    </div>


    <div id="pagination_agilysys_webinars_info_widget">
    </div>

    <!-- Modal -->
    <section class="webinarVideoWidget">
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <video controls id="videoxx">
                        <source src="movie.mp4" type="video/mp4">
                    </video>
                    <iframe src="" id="youtubexx">
                    </iframe>
                </div>
            </div>
        </div>
    </section>

</section>


<style>
.spinner2 {
    width: 40px;
    height: 40px;
    background-color: #000;

    margin: -200px auto;
    z-index: 99999;
    -webkit-animation: sk-rotateplane 1.2s infinite ease-in-out;
    animation: sk-rotateplane 1.2s infinite ease-in-out;
}

@-webkit-keyframes sk-rotateplane {
    0% {
        -webkit-transform: perspective(120px)
    }

    50% {
        -webkit-transform: perspective(120px) rotateY(180deg)
    }

    100% {
        -webkit-transform: perspective(120px) rotateY(180deg) rotateX(180deg)
    }
}

@keyframes sk-rotateplane {
    0% {
        transform: perspective(120px) rotateX(0deg) rotateY(0deg);
        -webkit-transform: perspective(120px) rotateX(0deg) rotateY(0deg)
    }

    50% {
        transform: perspective(120px) rotateX(-180.1deg) rotateY(0deg);
        -webkit-transform: perspective(120px) rotateX(-180.1deg) rotateY(0deg)
    }

    100% {
        transform: perspective(120px) rotateX(-180deg) rotateY(-179.9deg);
        -webkit-transform: perspective(120px) rotateX(-180deg) rotateY(-179.9deg);
    }
}
</style>

<script>
jQuery(document).on('click', '.videoClear', function() {
    //    $('input[data-type="search"]').val('');
    jQuery('body select').val("");
    fetch_data_agilysys_webinars_info_widget(1);
    //    $('input[data-type="search"]').trigger("keyup");
});

jQuery(document).on("click", ".aboutButton", function() {
    var id = jQuery(this).data('id');
    var video_type = jQuery('#video_type_' + id).val();
    var video_uri = jQuery('#video_uri_' + id).val();
    var youtube_url = jQuery('#youtube_url_' + id).val();

    if (video_type == "video") {
        jQuery('#videoxx').attr('src', video_uri);
        jQuery('#videoxx').show();
        jQuery('#youtubexx').hide();
    }

    if (video_type == "youtube") {
        jQuery('#youtubexx').attr('src', youtube_url);
        jQuery('#videoxx').hide();
        jQuery('#youtubexx').show();
    }
});
</script>

<div class="spinner2"></div>
<style>
.cvf_pag_loading {
    padding: 20px;
}

.cvf-universal-pagination ul {
    margin: 0;
    padding: 0;
}

.cvf-universal-pagination ul li {
    display: inline;
    margin: 3px;
    padding: 4px 8px;
    background: #FFF;
    color: black;
}

.cvf-universal-pagination ul li.active:hover {
    cursor: pointer;
    background: #1E8CBE;
    color: white;
}

.cvf-universal-pagination ul li.inactive {
    background: #7E7E7E;
}

.cvf-universal-pagination ul li.selected {
    background: #1E8CBE;
    color: white;
}

#pagination_agilysys_webinars_info_widget {
    /* float: right; */
    /*margin-right: 15%;*/
    text-align: center;
 
}
</style>

<script>
jQuery(document).ready(function($) {
    fetch_data_agilysys_webinars_info_widget(1);

    jQuery(document).on('click', '#pagination_agilysys_webinars_info_widget .cvf-universal-pagination li.active', function() {

        var page = jQuery(this).attr('p');
        fetch_data_agilysys_webinars_info_widget(page);
    });

});



function fetch_data_agilysys_webinars_info_widget(page) {

    jQuery(".loader_agilysys_webinars_info_widget").fadeIn("slow");
    var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

    // Data to receive from our server
    // the value in 'action' is the key that will be identified by the 'wp_ajax_' hook
    var data = {

        action: "fetch-data-agilysys-webinars-widget",
        industries: jQuery('#industries_agilysys_webinars_info_widget').val(),
        products: jQuery('#products_agilysys_webinars_info_widget').val(),
        data: '<?php echo json_encode($instance); ?>',
        page: page,
    };


    // Send the data
    jQuery.post(ajaxurl, data, function(response) {


        var dataxx = JSON.parse(response);


        jQuery("#articleList_agilysys_webinars_info_widget").html(dataxx.msg);

        jQuery("#pagination_agilysys_webinars_info_widget").html(dataxx.pag_container);

        jQuery(".loader_agilysys_webinars_info_widget").fadeOut("slow");


    });
}
</script>




<?php
echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['webinars_info_title'] = strip_tags($new_instance['webinars_info_title']);
        $count = count($new_instance['section_title']);
        for ($i = 0; $i < $count; $i++) {

            $instance['section_title'][$i] = strip_tags($new_instance['section_title'][$i]);
            $instance['webinars_info_front_image'][$i] = strip_tags($new_instance['webinars_info_front_image'][$i]);
            $instance['image_uri_alt'][$i] = strip_tags($new_instance['image_uri_alt'][$i]);

            $instance['webinars_info_desc'][$i] = strip_tags($new_instance['webinars_info_desc'][$i]);
            $instance['webinars_info_url_title'][$i] = strip_tags($new_instance['webinars_info_url_title'][$i]);
            $instance['industries'][$i] = strip_tags($new_instance['industries'][$i]);
            $instance['products'][$i] = strip_tags($new_instance['products'][$i]);

            $instance['link_type'][$i] = $new_instance['link_type'][$i];
            if ($new_instance['link_type'][$i] == 'page') {
                $instance['page'][$i] = $new_instance['page'][$i];
                $instance['url_link'][$i] = '';
            } elseif ($new_instance['link_type'][$i] == 'link') {
                $instance['url_link'][$i] = $new_instance['url_link'][$i];
                $instance['page'][$i] = '';

            }

        }
        return $instance;
    }

    /**
     * @see WP_Widget::form -- do not rename this
     */
    public function form($display_instance)
    {

        //$widget_add_id_webinars_info = $this->id . "-add";
        $widget_add_id_webinars_info = $this->get_field_id('') . "add";
        $webinars_info_title = ($display_instance['webinars_info_title']);

        $industries_arr = array(
            1 => 'Hotels & Resorts',
            2 => 'Casino Resorts',
            3 => 'Tribal Gaming',
            4 => 'Cruise Lines',
            5 => 'Foodservice Management',
            6 => 'Sports & Entertainment',
            7 => 'Restaurants',
            8 => 'Spa',
            9 => 'Golf',
        );

        $products_arr = array(
            'PROPERTY MANAGEMENT' => array(
                1 => 'Agilysys Stay',
                2 => 'Agilysys LMS',
                3 => 'Agilysys Visual One PMS',
                4 => 'Agilysys Sales and Catering',
                5 => 'rGuest® Book',
                6 => 'rGuest® Express Kiosk',
                7 => 'rGuest® Express Mobile',
                8 => 'rGuest® Service',
                9 => 'b4checkin',
            ),

            'POINT OF SALE' => array(
                10 => 'Agilysys InfoGenesis',
                11 => 'IG Flex',
                12 => 'IG Buy',
                13 => 'IG OnDemand',
            ),
            'PAYMENT SOLUTION' => array(
                14 => 'Agilysys Pay',

            ),
            'ANALYTICS & MARKETING LOYALTY' => array(
                15 => 'Agilysys Analyze',
            ),
            'INVENTORY & PROCUREMENT' => array(
                16 => 'Agilysys Eatec',
                17 => 'Agilysys SWS',

            ),
            'RESERVATIONS AND TABLE MANAGEMENT' => array(
                18 => 'Agilysys Seat',
            ),
            'ACTIVITY SCHEDULING' => array(
                19 => 'Agilysys Golf',
                20 => 'Agilysys Spa',
            ),
            'DOCUMENT MANAGEMENT' => array(
                21 => 'Agilysys DataMagine',
            ),
            'SERVICES' => array(
                22 => 'Professional Services',
            ),

        );

        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('webinars_info_title') . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('webinars_info_title') . '" name="' . $this->get_field_name('webinars_info_title') . '" type="text" value="' . $webinars_info_title . '" />';
        $rew_html .= '</p><br>';

        $count = count($display_instance['section_title']);

        $rew_html .= '<div class="add_new_rowxx-input-containers"><div id="entries_agilysys_webinars_info_widget">';

        $rew_html .= '<input class="cnt909" id="cnt909" name="cnt" type="hidden" value="' . $count . '">';

        for ($i = 0; $i < $count; $i++) {

            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'AGILYSYS_TEXT_DOMAIN') . ' </span>';

            $rew_html .= '<div class="entry-desc cf">';

            $rew_html .= '<p class="last">';
            $rew_html .= '<label for="' . $this->get_field_id('section_title' . $i) . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('section_title' . $i) . '" name="' . $this->get_field_name('section_title[]') . '" type="text" value="' . $display_instance['section_title'][$i] . '" />';
            $rew_html .= '</p>';

            $rew_html .= '<br><br><div class="widg-img' . $i . '">';
            $show1 = (empty($display_instance['webinars_info_front_image'][$i])) ? 'style="display:none;"' : '';
            $rew_html .= '<label id="image_uri_agilysys_webinars_info_widget' . $i . '"></label><br><img class="' . $this->get_field_id('webinars_info_front_image' . $i) . '_media_image' . $i . ' custom_media_image' . $i . '" src="' . $display_instance['webinars_info_front_image'][$i] . '" ' . $show1 . ' width=200" height="120"/>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('webinars_info_front_image' . $i) . '_media_id' . $i . ' custom_media_id' . $i . '" name="' . $this->get_field_name('webinars_info_front_image_id[]') . '" id="' . $this->get_field_id('webinars_info_front_image' . $i) . '" value="' . $display_instance['webinars_info_front_image_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('webinars_info_front_image' . $i) . '_media_url' . $i . ' custom_media_url' . $i . '" name="' . $this->get_field_name('webinars_info_front_image[]') . '" id="' . $this->get_field_id('webinars_info_front_image' . $i) . '" value="' . $display_instance['webinars_info_front_image'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload Image" class="button custom_media_upload' . $i . '" id="' . $this->get_field_id('webinars_info_front_image' . $i) . '"/>';

            $rew_html .= '</div><br><br>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('image_uri_alt' . $i) . '"> ' . __('Image Alt', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('image_uri_alt' . $i) . '" name="' . $this->get_field_name('image_uri_alt' . $i) . '" type="text" value="' . $display_instance['image_uri_alt' . $i] . '">';
            $rew_html .= '</p>';
            ?>

<script>
jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    if (attachment.height == 98 && attachment.width == 175) {
                        jQuery('.' + button_id_s + '_media_id<?php echo $i; ?>').val(attachment.id);
                        jQuery('.' + button_id_s + '_media_url<?php echo $i; ?>').val(attachment
                            .url);
                        jQuery('.' + button_id_s + '_media_image<?php echo $i; ?>').attr('src',
                            attachment.url).css('display', 'block');
                        jQuery('#image_uri_agilysys_webinars_info_widget<?php echo $i; ?>')
                            .html("");
                    } else {
                        jQuery('#image_uri_agilysys_webinars_info_widget<?php echo $i; ?>')
                            .html("Please Enter the correct Dimensions 175x98").css('color',
                                'red');

                    }
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload<?php echo $i; ?>');

});
</script>






<?php

            /**
             * Description
             */
            $webinars_info_desc = esc_attr($display_instance['webinars_info_desc'][$i]);
            $webinars_info_url_title = esc_attr($display_instance['webinars_info_url_title'][$i]);

            $rew_html .= '<br><p class="last desc">';
            $rew_html .= '<label for="' . $this->get_field_id('webinars_info_desc' . $i) . '"> ' . __('Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<textarea id="' . $this->get_field_id('webinars_info_desc' . $i) . '" name="' . $this->get_field_name('webinars_info_desc[]') . '">' . $webinars_info_desc . '</textarea>';
            $rew_html .= '</p>';

            $rew_html .= '<p class="last">';
            $rew_html .= '<label for="' . $this->get_field_id('webinars_info_url_title-' . $i) . '"> ' . __('Learn More Text', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('webinars_info_url_title-' . $i) . '" name="' . $this->get_field_name('webinars_info_url_title[]') . '" type="text" value="' . $webinars_info_url_title . '" />';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('industries' . $i) . '"> ' . __('Industries', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('industries' . $i) . '" name="' . $this->get_field_name('industries[]') . '">';

            $selected_industry = $display_instance['industries'][$i];

            foreach ($industries_arr as $key => $val) {

                if ($key == $selected_industry) {
                    $rew_html .= '<option value="' . $key . '" selected="selected">' . $val . '</option>';
                } else {
                    $rew_html .= '<option value="' . $key . '">' . $val . '</option>';
                }
            }

            $rew_html .= '</select>';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('products' . $i) . '"> ' . __('Products', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('products' . $i) . '" name="' . $this->get_field_name('products[]') . '">';

            $selected_products = $display_instance['products'][$i];

            foreach ($products_arr as $key => $val) {

                if (is_array($val)) {

                    $rew_html .= '<optgroup label="' . $key . '">';

                    foreach ($val as $k => $v) {
                        if ($k == $selected_products) {
                            $rew_html .= '<option value="' . $k . '" selected="selected">' . $v . '</option>';
                        } else {
                            $rew_html .= '<option value="' . $k . '">' . $v . '</option>';
                        }
                    }

                    $rew_html .= '</optgroup>';

                }
            }

            $rew_html .= '</select>';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('link_type' . $i) . '"> ' . __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('link_type' . $i) . '" name="' . $this->get_field_name('link_type[]') . '" onChange="show_hide_div_agilysys_webinars_info_widget(this.value,' . $i . ');">';
            $rew_html .= '<option value="">Please Select</option>';

            $link_type = $display_instance['link_type'][$i];

            if ($link_type == 'page') {
                $rew_html .= '<option value="page" selected="selected">Internal Page Link</option>';
            } else {
                $rew_html .= '<option value="page">Internal Page Link</option>';
            }

            if ($link_type == 'link') {
                $rew_html .= '<option value="link" selected="selected">External Link</option>';
            } else {
                $rew_html .= '<option value="link">External Link</option>';
            }

            $rew_html .= '</select>';
            $rew_html .= '</p><br><br>';

            $args = array(
                'sort_order' => 'desc',
                'sort_column' => 'post_title',
                'hierarchical' => 1,
                'exclude' => '',
                'include' => '',
                'meta_key' => '',
                'meta_value' => '',
                'authors' => '',
                'child_of' => 0,
                'parent' => -1,
                'exclude_tree' => '',
                'number' => '',
                'offset' => 0,
                'post_type' => 'page',
                'post_status' => 'publish',
            );
            $pages = get_pages($args); // get all pages based on supplied args

            if ($link_type == 'page') {
                $show1 = 'style="display:block"';
                $show2 = 'style="display:none"';
            } elseif ($link_type == 'link') {
                $show1 = 'style="display:none"';
                $show2 = 'style="display:block"';

            } else {
                $show1 = 'style="display:none"';
                $show2 = 'style="display:none"';
            }
            $rew_html .= '<div id="page_div_agilysys_webinars_info_widget' . $i . '" ' . $show1 . '><p>';
            $rew_html .= '<label for="' . $this->get_field_id('page' . $i) . '"> ' . __('Page', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<select id="' . $this->get_field_id('page' . $i) . '" name="' . $this->get_field_name('page[]') . '">';
            $rew_html .= '<option value="">Please Select</option>';

            $page = $display_instance['page'][$i];

            foreach ($pages as $key) {

                if ($page == $key->ID) {
                    $rew_html .= '<option value="' . $key->ID . '" selected="selected">' . $key->post_title . '</option>';
                } else {
                    $rew_html .= '<option value="' . $key->ID . '">' . $key->post_title . '</option>';
                }

            }

            $rew_html .= '</select>';
            $rew_html .= '</p></div><br><br>';

            $rew_html .= '<div id="link_div_agilysys_webinars_info_widget' . $i . '" ' . $show2 . '><p>';
            $rew_html .= '<label for="' . $this->get_field_id('url_link' . $i) . '"> ' . __('Link', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('url_link' . $i) . '" name="' . $this->get_field_name('url_link[]') . '" type="text" value="' . $display_instance['url_link'][$i] . '" />';
            $rew_html .= '</p></div><br><br>';

            ?>
<script>
function show_hide_div_agilysys_webinars_info_widget(val, i) {
    console.log(val);
    if (val == 'page') {
        jQuery("#page_div_agilysys_webinars_info_widget" + i).show();
        jQuery("#link_div_agilysys_webinars_info_widget" + i).hide();
    } else if (val == 'link') {
        jQuery("#page_div_agilysys_webinars_info_widget" + i).hide();
        jQuery("#link_div_agilysys_webinars_info_widget" + i).show();
    }

}
</script>

<?php
    
            ?>






<?php

            $k = $i + 1;
            $rew_html .= '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' . $k . ');">' . __('Delete Row', 'AGILYSYS_TEXT_DOMAIN') . '</span></a></p>';
            $rew_html .= '</div></div>';

        }
        $rew_html .= '</div></div>';

        $rew_html .= '<div class="' . $widget_add_id_webinars_info . '" style="margin-bottom: 36px;text-align: center;    padding: 1%;    background: #ccc;    text-transform: uppercase;    cursor: pointer;    position: relative;    top: 16px;    font-weight: 800;" onClick="add_new_row();">' . __('ADD ROW', 'ZWREW_TEXT_DOMAIN') . '</div>';

        ?>
<script>


function add_new_row() {
    var cnt = '';

    jQuery.each(jQuery("#entries_agilysys_webinars_info_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            cnt = jQuery(this).val();
        }
    });





    cnt++;

    jQuery.each(jQuery("#entries_agilysys_webinars_info_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(cnt);
        }
    });

    console.log(cnt);

    var new_row = '<div id="entry' + cnt +
        '"  class="entrys"><span class="entry-title" onclick = "slider(this);"><?php echo __('Add New Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span>';
    new_row += '<div class="entry-desc cf">';


    new_row += '<p class="last">';
    new_row += '<label><?php echo __('Title', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
    new_row += '<input name="<?php echo $this->get_field_name('section_title[]'); ?>" type="text" value="" />';
    new_row += '</p>';

    new_row += '<br><br><div class="widg-img' + cnt + '">';

    new_row += '<label id="image_uri_agilysys_webinars_info_widget' + cnt +
        '"></label><br><img class="<?php echo $this->get_field_id('webinars_info_front_image'); ?>' + cnt +
        '_media_image' +
        cnt + ' custom_media_image' + cnt + '" src="" style="display:none;" width=200" height="120"/>';
    new_row += '<input type="hidden" class="<?php echo $this->get_field_id('webinars_info_front_image'); ?>' + cnt +
        '_media_id' + cnt + ' custom_media_id' + cnt +
        '" name="<?php echo $this->get_field_name('webinars_info_front_image_id[]'); ?>"  />';
    new_row += '<input type="hidden" class="<?php echo $this->get_field_id('webinars_info_front_image'); ?>' + cnt +
        '_media_url' + cnt + ' custom_media_url' + cnt +
        '" name="<?php echo $this->get_field_name('webinars_info_front_image[]'); ?>">';
    new_row += '<input type="button" value="Upload Image" class="button custom_media_upload' + cnt +
        '" id="<?php echo $this->get_field_id('webinars_info_front_image'); ?>' + cnt + '"/>';

    new_row += '</div><br><br>';

    jQuery(document).ready(function() {




        function media_upload(button_class) {
            var _custom_media = true,
                _orig_send_attachment = wp.media.editor.send.attachment;
            jQuery('body').on('click', '.custom_media_upload' + cnt, function(e) {
                var button_id = '#' + jQuery(this).attr('id');
                var button_id_s = jQuery(this).attr('id');
                console.log(button_id);
                var self = jQuery(button_id);
                var send_attachment_bkp = wp.media.editor.send.attachment;
                var button = jQuery(button_id);
                var id = button.attr('id').replace('_button', '');
                _custom_media = true;

                wp.media.editor.send.attachment = function(props, attachment) {
                    if (_custom_media) {

                        if (attachment.height == 98 && attachment.width == 175) {
                            jQuery('.' + button_id_s + '_media_id' + cnt).val(attachment.id);
                            jQuery('.' + button_id_s + '_media_url' + cnt).val(attachment.url);
                            jQuery('.' + button_id_s + '_media_image' + cnt).attr('src',
                                attachment.url).css('display', 'block');

                            jQuery('#image_uri_agilysys_webinars_info_widget' + cnt)
                                .html("");
                        } else {
                            jQuery('#image_uri_agilysys_webinars_info_widget' + cnt)
                                .html("Please Enter the correct Dimensions 175x98").css(
                                    'color', 'red');

                        }
                    } else {
                        return _orig_send_attachment.apply(button_id, [props, attachment]);
                    }
                }
                wp.media.editor.open(button);
                return false;
            });
        }
        media_upload('.custom_media_upload' + cnt);

    });


    new_row += '<p>';
    new_row += '<label for=""><?php echo __('Image Alt', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';

    new_row +=
        '<input class="" name="<?php echo esc_attr($this->get_field_name('image_uri_alt[]')); ?>" type="text" value="">';
    new_row += '</p>';



    new_row += '<br><p class="last desc">';
    new_row += '<label><?php echo __('Description', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
    new_row += '<textarea name="<?php echo $this->get_field_name('webinars_info_desc[]'); ?>"></textarea>';
    new_row += '</p>';

    new_row += '<p class="last">';
    new_row += '<label><?php echo __('Learn More Text', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
    new_row += '<input name="<?php echo $this->get_field_name('webinars_info_url_title[]'); ?>" type="text" />';
    new_row += '</p>';

    new_row += '<p>';
    new_row += '<label> <?php echo __('Industries', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
    new_row += '<select name="<?php echo $this->get_field_name('industries[]'); ?>">';

    <?php

        foreach ($industries_arr as $key => $val) {

            ?>

    new_row += '<option value="<?php echo $key; ?>"><?php echo $val; ?></option>';


    <?php

        }

        ?>

    new_row += '</select>';
    new_row += '</p>';

    new_row += '<p>';
    new_row += '<label> <?php echo __('Products', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
    new_row += '<select name="<?php echo $this->get_field_name('products[]'); ?>">';

    <?php

        foreach ($products_arr as $key => $val) {

            if (is_array($val)) {
                ?>
    new_row += '<optgroup label="<?php echo $key; ?>">';
    <?php
foreach ($val as $k => $v) {

                    ?>

    new_row += '<option value="<?php echo $k; ?>"><?php echo $v; ?></option>';
    <?php
}
                ?>
    new_row += '</optgroup>';
    <?php
}
        }
        ?>
    new_row += '</select>';
    new_row += '</p>';

    new_row += '<p>';
    new_row += '<label><?php echo __('Select Link type url: ', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
    new_row +=
        '<select name="<?php echo $this->get_field_name('link_type[]'); ?>" onChange="show_hide_div_agilysys_webinars_info_widget(this.value,' +
        cnt + ');">';
    new_row += '<option value="">Please Select</option>';

    new_row += '<option value="page">Internal Page Link</option>';

    new_row += '<option value="link">External Link</option>';

    new_row += '</select>';
    new_row += '</p><br><br>';


    <?php

        $args = array(
            'sort_order' => 'desc',
            'sort_column' => 'post_title',
            'hierarchical' => 1,
            'exclude' => '',
            'include' => '',
            'meta_key' => '',
            'meta_value' => '',
            'authors' => '',
            'child_of' => 0,
            'parent' => -1,
            'exclude_tree' => '',
            'number' => '',
            'offset' => 0,
            'post_type' => 'page',
            'post_status' => 'publish',
        );
        $pages = get_pages($args); // get all pages based on supplied args

        ?>


    new_row += '<div id="page_div_agilysys_webinars_info_widget' + cnt + '" style="display:none;"><p>';
    new_row += '<label><?php echo __('Page', 'AGILYSYS_TEXT_DOMAIN'); ?>:</label>';
    new_row += '<select  name="<?php echo $this->get_field_name('page[]'); ?>">';
    new_row += '<option value="">Please Select</option>';


    <?php
foreach ($pages as $key) {
            ?>



    new_row += '<option value="<?php echo $key->ID; ?>"><?php echo $key->post_title; ?></option>';


    <?php
}
        ?>

    new_row += '</select>';
    new_row += '</p></div><br><br>';

    new_row += '<div id="link_div_agilysys_webinars_info_widget' + cnt + '" style="display:none;"><p>';
    new_row += '<label> <?php echo __('Link', 'AGILYSYS_TEXT_DOMAIN'); ?> :</label>';
    new_row += '<input name="<?php echo $this->get_field_name('url_link[]'); ?>" type="text"  />';
    new_row += '</p></div><br><br>';



    var new_cnt = cnt;

    new_row += '<p><a href="#delete"><span class="delete-row" onClick="delete_row(' + new_cnt +
        ');"><?php echo __('Delete Row', 'AGILYSYS_TEXT_DOMAIN'); ?></span></a></p>';
    new_row += '</div></div>';

    jQuery('.add_new_rowxx-input-containers #entries_agilysys_webinars_info_widget').append(new_row);




}


function show_hide_div_agilysys_webinars_info_widget(val, i) {
    console.log(val);
    if (val == 'page') {
        jQuery("#page_div_agilysys_webinars_info_widget" + i).show();
        jQuery("#link_div_agilysys_webinars_info_widget" + i).hide();
    } else if (val == 'link') {
        jQuery("#page_div_agilysys_webinars_info_widget" + i).hide();
        jQuery("#link_div_agilysys_webinars_info_widget" + i).show();
    }

}



function delete_row(cnt) {
    jQuery.each(jQuery(".add_new_rowxx-input-containers #entries_agilysys_webinars_info_widget"), function() {
        jQuery(' #entry' + cnt).remove();
    });
    var last_cnt = 0;

    jQuery.each(jQuery("#entries_agilysys_webinars_info_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            last_cnt = jQuery(this).val();
        }
    });

    last_cnt--;
    jQuery.each(jQuery("#entries_agilysys_webinars_info_widget .cnt909"), function() {
        if (jQuery(this).val() != '') {
            jQuery(this).val(last_cnt);
        }
    });


}
</script>
<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_webinars_info_widget select {
    float: left;
    width: 60%;
    margin-top: 20px !important;
    margin-bottom: 10px !important;
}

#rew_container_agilysys_webinars_info_widget input,
textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_webinars_info_widget label {
    width: 40%;
    float: left;
}

#rew_container_agilysys_webinars_info_widget p {
    padding:20px;
}

<?php echo '.'. $widget_add_id_webinars_info;

?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}

#entries_agilysys_webinars_info_widget #remove-img-bg {
    background: url('<?php echo ZWREW_URL; ?>assets/images/deleteimg.png') center center no-repeat;
    width: 20px;
    height: 22px;
    display: none;
}

#entries_agilysys_webinars_info_widget {
    padding: 10px 0 0;
}

#entries_agilysys_webinars_info_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_webinars_info_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_webinars_info_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_webinars_info_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_webinars_info_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_webinars_info_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_webinars_info_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_webinars_info_widget #entries_agilysys_webinars_info_widget plast label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_webinars_info_widget">
    <?php echo $rew_html; ?>
</div>
<?php
}
}
<?php

function load_agilysys_product_resource_resort_activities_suite_widget()
{
    register_widget('agilysys_product_resource_resort_activities_suite_widget');
}

add_action('widgets_init', 'load_agilysys_product_resource_resort_activities_suite_widget');

class agilysys_product_resource_resort_activities_suite_widget extends WP_Widget
{
/**
 * constructor -- name this the same as the class above
 */
    public function __construct()
    {
        parent::__construct(false, $name = __('Agilysys Product Resource Resort Activities Suite Widget', 'AGILYSYS_TEXT_DOMAIN'));
        wp_register_script('add-sd-js', get_template_directory_uri() . '/inc/widgets/agilysys-widget.js', array('jquery'), 'null', true);
        wp_enqueue_script('add-sd-js');
    }

    public function widget($args, $instance)
    {

        echo $args['before_widget'];
        $section_title = strip_tags($instance['section_title']);

        $image_uri = strip_tags($instance['image_uri']);
    

        $image_uri = preg_replace("/\s+/", "", $image_uri);
        
        
        

        ?>


<section class="propetyResortSection whiteText" style="background:url(<?php echo $image_uri; ?>);    background-position: top;
    background-size: cover;">
    <div class="propetyResortTitle">
        <h2 class="dinProStd h2 center"><?php echo $section_title; ?></h2>
    </div>
    <div class="propetyResortBoxContainer greenBG">
        <div class="row">


            <?php

        for ($i = 0; $i < 3; $i++) {
            $block = $instance['block-' . $i];

            $title = strip_tags($instance['title' . $i]);
            $description = strip_tags($instance['description' . $i]);
            $button_text = strip_tags($instance['button_text' . $i]);

            $pdf_uri = $instance['pdf_uri'][$i];

            ?>
            <div class="propetyResortBox col-12 col-sm-12 col-md-4 collg-4">
                <h4 class="dinProStd"><?php echo $title; ?></h4>
                <p><?php echo $description; ?></p>
                <a href="<?php echo $pdf_uri; ?>" class="homeBannerButton whiteText"><?php echo $button_text; ?> <i
                        class="fa fa-arrow-right" aria-hidden="true"></i></a>
            </div>
            <?php

        }

        ?>
        </div>
    </div>
</section>
<br>


<?php
echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();

        $instance['section_title'] = strip_tags($new_instance['section_title']);

        $instance['image_uri'] = strip_tags($new_instance['image_uri']);
      

        for ($i = 0; $i < 3; $i++) {
            $block = $new_instance['block-' . $i];

            $instance['block-' . $i] = $new_instance['block-' . $i];
            $instance['title' . $i] = strip_tags($new_instance['title' . $i]);
            $instance['description' . $i] = strip_tags($new_instance['description' . $i]);
            $instance['button_text' . $i] = strip_tags($new_instance['button_text' . $i]);

            $instance['pdf_uri'][$i] = $new_instance['pdf_uri'][$i];

        }
        return $instance;

      
    }

    public function form($display_instance)
    {
        $max_entries_agilysys_product_resource_resort_activities_suite_widget_slider_image = 15;

        $widget_add_id_slider = $this->get_field_id('') . "add_agilysys_product_resource_point_of_sales_guides_widget";

        $section_title = ($display_instance['section_title']);
        $sub_title = ($display_instance['sub_title']);
        $button_text = $display_instance['button_text'];
        $url = $display_instance['url'];
        $rew_html = '<p>';
        $rew_html .= '<label for="' . $this->get_field_id('section_title') . '"> ' . __('Section Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
        $rew_html .= '<input id="' . $this->get_field_id('section_title') . '" name="' . $this->get_field_name('section_title') . '" type="text" value="' . $section_title . '" />';
        $rew_html .= '</p><br><br>';

        $rew_html .= '<div class="widg-img">';
        $show1 = (empty($display_instance['image_uri'])) ? 'style="display:none;"' : '';
        $rew_html .= '<label id="image_uri_agilysys_product_resource_resort_activities_suite_widget"></label><br><img class="' . $this->get_field_id('image_id') . '_media_image custom_media_image" src="' . $display_instance['image_uri'] . '" ' . $show1 . ' width=200" height="120"/>';
        $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id') . '_media_id custom_media_id" name="' . $this->get_field_name('image_id') . '" id="' . $this->get_field_id('image_id') . '" value="' . $display_instance['image_id'] . '" />';
        $rew_html .= '<input type="hidden" class="' . $this->get_field_id('image_id') . '_media_url custom_media_url" name="' . $this->get_field_name('image_uri') . '" id="' . $this->get_field_id('image_uri') . '" value="' . $display_instance['image_uri'] . '">';
        $rew_html .= '<input type="button" value="Upload Background Image" class="button custom_media_upload" id="' . $this->get_field_id('image_id') . '"/>';

        $rew_html .= '</div><br><br><br><br>';

        ?>

<script>
jQuery(document).ready(function() {
    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {

                    //if (attachment.height == 1080 && attachment.width == 1920) {
                        jQuery('.' + button_id_s + '_media_id').val(attachment.id);
                        jQuery('.' + button_id_s + '_media_url').val(attachment.url);
                        jQuery('.' + button_id_s + '_media_image').attr('src', attachment.url).css(
                            'display', 'block');
                        /*jQuery('#image_uri_agilysys_product_resource_resort_activities_suite_widget')
                            .html("");
                    } else {
                        jQuery('#image_uri_agilysys_product_resource_resort_activities_suite_widget')
                            .html("Please Enter the correct Dimensions 1920x1080").css(
                                'color', 'red');

                    }
                    */
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload');

});
</script>

<?php

        $rew_html .= '<div class="' . $widget_add_id_what_makes . '-input-containers"><div id="entries_agilysys_product_resource_resort_activities_suite_widget">';

        for ($i = 0; $i < 3; $i++) {
            $rew_html .= '<div id="entry' . ($i + 1) . '" ' . $display . ' class="entrys"><span class="entry-title" onclick = "slider(this);"> ' . __('Add New Row', 'AGILYSYS_TEXT_DOMAIN') . ' </span>';

            $display = (!isset($display_instance['block-' . $i]) || ($display_instance['block-' . $i] == "")) ? 'style="display:block;"' : '';

            $rew_html .= '<div class="entry-desc cf">';
            $rew_html .= '<input id="' . $this->get_field_id('block-' . $i) . '" name="' . $this->get_field_name('block-' . $i) . '" type="hidden" value="' . $display_instance['block-' . $i] . '">';
            /**
             * Block Caption
             */

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('title' . $i) . '"> ' . __('Title', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('title' . $i) . '" name="' . $this->get_field_name('title' . $i) . '" type="text" value="' . $display_instance['title' . $i] . '">';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('description' . $i) . '"> ' . __('Description', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('description' . $i) . '" name="' . $this->get_field_name('description' . $i) . '" type="text" value="' . $display_instance['description' . $i] . '">';
            $rew_html .= '</p>';

            $rew_html .= '<p>';
            $rew_html .= '<label for="' . $this->get_field_id('button_text' . $i) . '"> ' . __('Button Text', 'AGILYSYS_TEXT_DOMAIN') . ' :</label>';
            $rew_html .= '<input id="' . $this->get_field_id('button_text' . $i) . '" name="' . $this->get_field_name('button_text' . $i) . '" type="text" value="' . $display_instance['button_text' . $i] . '">';
            $rew_html .= '</p><br><br><br>';

            
            $rew_html .= '<div class="widg-img' . $i . '">';
            $show1 = (empty($display_instance['pdf_uri'][$i])) ? 'style="display:none;"' : '';
            $rew_html .= '<label class="' . $this->get_field_id('pdf_id' . $i) . '_media_image' . $i . ' custom_media_image' . $i . '" value="' . $display_instance['pdf_uri'][$i] . '" ' . $show1 . ' width=200" height="120">' . $display_instance['pdf_uri'][$i] . ' </label>';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('pdf_id' . $i) . '_media_id' . $i . ' custom_media_id' . $i . '" name="' . $this->get_field_name('pdf_id[]') . '" id="' . $this->get_field_id('pdf_id' . $i) . '" value="' . $display_instance['pdf_id'][$i] . '" />';
            $rew_html .= '<input type="hidden" class="' . $this->get_field_id('pdf_id' . $i) . '_media_url' . $i . ' custom_media_url' . $i . '" name="' . $this->get_field_name('pdf_uri[]') . '" id="' . $this->get_field_id('pdf_uri' . $i) . '" value="' . $display_instance['pdf_uri'][$i] . '">';
            $rew_html .= '<input type="button" value="Upload PDF/Zip File" class="button custom_media_upload' . $i . '" id="' . $this->get_field_id('pdf_id' . $i) . '"/>';

            $rew_html .= '</div>';
            ?>

<script>
jQuery(document).ready(function() {




    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;
        jQuery('body').on('click', '.custom_media_upload<?php echo $i; ?>', function(e) {
            var button_id = '#' + jQuery(this).attr('id');
            var button_id_s = jQuery(this).attr('id');
            console.log(button_id);
            var self = jQuery(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = jQuery(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment) {
                if (_custom_media) {
                    jQuery('.' + button_id_s + '_media_id<?php echo $i; ?>').val(attachment.id);
                    jQuery('.' + button_id_s + '_media_url<?php echo $i; ?>').val(attachment.url);
                    jQuery('.' + button_id_s + '_media_image<?php echo $i; ?>').attr('src',
                        attachment.url).css('display', 'block');
                } else {
                    return _orig_send_attachment.apply(button_id, [props, attachment]);
                }
            }
            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_upload<?php echo $i; ?>');

});
</script>


<?php
            ?>


<?php

            $rew_html .= '</div></div>';
        }
        $rew_html .= '</div></div>';
        ?>



<style>
.cf:before,
.cf:after {
    content: "";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    zoom: 1;
}

.clear {
    clear: both;
}

.clearfix:after {
    content: ".";
    display: block;
    height: 0;
    clear: both;
    visibility: hidden;
}

.clearfix {
    display: inline-block;
}

* html .clearfix {
    height: 1%;
}

.clearfix {
    display: block;
}

#rew_container_agilysys_product_resource_resort_activities_suite_widget  select{
     float: left;
    width: 60%;
    margin-top:20px !important;
    margin-bottom:10px !important;
}

#rew_container_agilysys_product_resource_resort_activities_suite_widget input,

textarea {
    float: right;
    width: 60%;
}

#rew_container_agilysys_product_resource_resort_activities_suite_widget label {
    width: 40%;
     float: left;
}


#rew_container_agilysys_product_resource_resort_activities_suite_widget p {
    padding:20px !important;
}

<?php echo '.' . $widget_add_id_slider;

        ?> {
    background: #ccc none repeat scroll 0 0;
    font-weight: bold;
    margin: 20px 0px 9px;
    padding: 6px;
    text-align: center;
    display: block !important;
    cursor: pointer;
}

.block-image {
    width: 50px;
    height: 30px;
    float: right;
    display: none;
}

.desc {
    height: 55px;
}



#entries_agilysys_product_resource_resort_activities_suite_widget {
    padding: 10px 0 0;
}

#entries_agilysys_product_resource_resort_activities_suite_widget .entrys {
    padding: 0;
    border: 1px solid #e5e5e5;
    margin: 10px 0 0;
    clear: both;
}

#entries_agilysys_product_resource_resort_activities_suite_widget .entrys:first-child {
    margin: 0;
}

#entries_agilysys_product_resource_resort_activities_suite_widget .delete-row {
    margin-top: 20px;
    float: right;
    text-decoration: underline;
    color: red;
}

#entries_agilysys_product_resource_resort_activities_suite_widget .entry-title {
    display: block;
    font-size: 14px;
    line-height: 18px;
    font-weight: 600;
    background: #f1f1f1;
    padding: 7px 5px;
    position: relative;
}

#entries_agilysys_product_resource_resort_activities_suite_widget .entry-title:after {
    content: '\f140';
    font: 400 20px/1 dashicons;
    position: absolute;
    right: 10px;
    top: 6px;
    color: #a0a5aa;
}

#entries_agilysys_product_resource_resort_activities_suite_widget .entry-title.active:after {
    content: '\f142';
}

#entries_agilysys_product_resource_resort_activities_suite_widget .entry-desc {
    display: none;
    padding: 0 10px 10px;
    border-top: 1px solid #e5e5e5;
}

#rew_container_agilysys_product_resource_resort_activities_suite_widget #entries_agilysys_product_resource_resort_activities_suite_widget p.last label {
    white-space: pre-line;
    float: left;
    width: 39%;
}

#message {
    padding: 6px;
    display: none;
    color: red;
    font-weight: bold;
}
</style>
<div id="rew_container_agilysys_product_resource_resort_activities_suite_widget">
    <?php echo $rew_html; ?>
</div>
<?php
}

}
<?php

function agylisis_script_enqueue() {

    //CSS
    wp_enqueue_style('bootstrapcss', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '1.0.0', 'all');
    wp_enqueue_style('fontstyle', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '1.0.0', 'all');	
    wp_enqueue_style('swipermincss', get_template_directory_uri() . '/css/swiper.min.css', array(), '1.0.0', 'all');   
      wp_enqueue_style('customstyle', get_template_directory_uri() . '/css/style.css', array(), '1.0.0', 'all');	
 
  	

     
 

    wp_enqueue_script('jquerycustom', get_template_directory_uri() . '/js/jquery-3.4.1.js', array(), '3.4.1', true);    
    wp_enqueue_script('bootstrapjs', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '4.0.0', true);
    wp_enqueue_script('swiperminjs', get_template_directory_uri() . '/js/swiper.min.js', array(), '2.0.1', true);
   // wp_enqueue_script('customizerjs', get_template_directory_uri() . '/js/customizer.js', array(), '2.0.1', true);
  
	
    wp_enqueue_script('customjs', get_template_directory_uri() . '/js/main.js', array(), '1.0.0', true);
    
      wp_enqueue_script('rguest', get_template_directory_uri() . '/js/rguest.js', array(), '1.0.0', true);
   wp_enqueue_script('home', get_template_directory_uri() . '/js/home-animations.js', array(), '1.0.0', true);
   
   
}
add_action( 'wp_enqueue_scripts', 'agylisis_script_enqueue');

function agylisis_theme_setup() {
	
	add_theme_support('menus');

	register_nav_menu('primary', 'Primary Header Navigation');
	register_nav_menu('secondary', 'Footer Navigation');
}
add_action('init', 'agylisis_theme_setup');


/*
change date format
 */
function dateCustom($date) {
	$result = date("M d, Y", strtotime($date));
	return $result;
}
<?php
$activePage = basename($_SERVER['PHP_SELF'], ".php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--	<title><?php //echo $title; ?></title>-->
    <title>Agilysys</title>
    <meta name="charset" content="UTF-8">
    <meta name="theme-color" content="#fff">
    <!--	<meta name="description" content="<?php //echo $description ?>">-->
    <!--	<meta name="keywords" content="<?php //echo $keywords ?>">-->
    <meta name="copyright" content="Copyright � <?php echo date('Y'); ?> <?php //echo $title; ?> ">
    <!--	<link rel="canonical" href="<?php //echo $canonical ?>"/>-->
    <!--	<meta name="author" content="<?php //echo $title; ?> ">-->
    <!--	<meta name="designer" content="<?php// echo $title; ?>">-->
    <meta name="robots" content="index, follow">
    <meta name="googlebot" content="index, follow">
    <meta http-equiv="cache-control" content="no-cache" />
    <link rel="icon" href="<?php echo get_theme_mod( 'site_icon'); ?>" type="image/vnd.microsoft.icon">


</head>


<body>



    <?php
        wp_head();
    ?>

    <div id="overlay">
        <div class="spinner"></div>
    </div>
    <header class="header">
        <div class="hamburgerMenu" id="hamburgerAnimation">
            <div class="bar1"></div>
            <div class="bar2"></div>
            <div class="bar3"></div>
        </div>
        <div class="mainLogo">
            <a href="index.php">
                <img class="img-fluid" src="<?php echo get_theme_mod( 'logo_settings'); ?>" alt="Agilysys Logo">
            </a>
        </div>

        <?php
        $menu_name = 'header-menu';
        $locations = get_nav_menu_locations();
        $menu = wp_get_nav_menu_object($locations[$menu_name]);
        $menuitems = wp_get_nav_menu_items($menu->term_id, array('order' => 'DESC'));

        $top_level = array();
        $top_level_id = array();
        $children = array();
        $children_id = array();
        $sub_level_children = array();

        foreach ($menuitems as $item) {

            if (!$item->menu_item_parent) {
                array_push($top_level, $item);
                array_push($top_level_id, $item->ID);
            }

        }

foreach ($menuitems as $item) {

    if (in_array($item->menu_item_parent, $top_level_id)) {
        $children[$item->menu_item_parent][] = $item;
        array_push($children_id, $item->ID);

    }

}

foreach ($menuitems as $item) {

    if (in_array($item->menu_item_parent, $children_id)) {
        $sub_level_children[$item->menu_item_parent][] = $item;
    }

}

?>


        <nav class="headerNav">
            <ul class="menu">
                <?php
foreach ($top_level as $item) {

    $title = $item->title;
    $link = $item->url;
    $ID = $item->ID;

    ?>
                <li>

                <?php

if (!empty($children[$item->ID])) {

?>
                    <a href="<?php echo $link; ?>" class="ffBold downArrow hoverEffect"
                        id="<?php echo $ID; ?>SubMenu"><?php echo $title; ?></a>

                        <?php

}
else
{
?>

<a href="<?php echo $link; ?>" class="ffBold hoverEffect"
                        id="<?php echo $ID; ?>SubMenu"><?php echo $title; ?></a>

<?php

}

?>

                        <?php

if (!empty($children[$item->ID])) {

?>
                    <div class="dropMenu greenBG" id="<?php echo $ID; ?>SubMenuShow">


                        <script>
                        jQuery(".headerNav ul li a#<?php echo $ID; ?>SubMenu").hover(function() {



                            jQuery("#<?php echo $ID; ?>SubMenuShow").show();

                            jQuery('.dropMenuLink').removeClass('active');

                            jQuery('#child_<?php echo $ID; ?>').addClass('active');
                            jQuery('#childxx_<?php echo $ID; ?>').addClass('active');

                            

                        });



                        jQuery(".headerNav").mouseleave(function() {
                            jQuery(".dropMenu").css('display','none');
                        });

                        jQuery(".headerNav").click(function(e) {
                            e.stopPropagation();
                        });
                        </script>

                        <style>
                        .headerNav ul li .dropMenu .dropMenuList li .panelLeft {
                            width: 50%;
                            padding: 10px;
                        }
                        </style>



<ul class="dropMenuList">
                            <?php

    $cnt = 0;
    foreach ($children[$item->ID] as $child) {

        $title1 = $child->title;
        $link1 = $child->url;
        $description1 = $child->description;

        if ($cnt == 0) {
            ?>

                            <li>
                                <a class="dropMenuLink active" id="child_<?php echo $ID; ?>"><?php echo $title1; ?></a>
                                <ul class="dropMenuPanel active" id="childxx_<?php echo $ID; ?>">
                                    <li class="panelLeft">
                                        <h3 class="greenText dinProStd"><?php echo $title1; ?></h3>
                                        <p><?php echo $description1; ?></p>
                                        <a href="<?php echo $link1; ?>" class="aboutButton">Read More <i
                                                class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                    </li>
                                    <li class="panelRight">

                                        <?php

            if (!empty($sub_level_children[$child->ID])) {
                ?>

                                        <ul>

                                            <?php

                foreach ($sub_level_children[$child->ID] as $subchild) {

                    $title2 = $subchild->title;
                    $link2 = $subchild->url;
                    ?>
                                            <li><a href="<?php echo $link2; ?>"><?php echo $title2; ?></a></li>

                                            <?php

                }
                ?>

                                        </ul>
                                        <?php

            }
            ?>
                                    </li>
                                </ul>
                            </li>

                            <?php
} else {
            ?>


                            <li>
                                <a class="dropMenuLink"><?php echo $title1; ?></a>
                                <ul class="dropMenuPanel hide">
                                    <li class="panelLeft">
                                        <h3 class="greenText dinProStd"><?php echo $title1; ?></h3>
                                        <p><?php echo $description1; ?></p>
                                        <a href="<?php echo $link1; ?>" class="aboutButton">Read More <i
                                                class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                    </li>
                                    <li class="panelRight">
                                        <?php

            if (!empty($sub_level_children[$child->ID])) {
                ?>

                                        <ul>

                                            <?php

                foreach ($sub_level_children[$child->ID] as $subchild) {

                    $title2 = $subchild->title;
                    $link2 = $subchild->url;
                    ?>
                                            <li><a href="<?php echo $link2; ?>"><?php echo $title2; ?></a></li>

                                            <?php

                }
                ?>

                                        </ul>
                                        <?php

            }
            ?>
                                    </li>
                                </ul>
                            </li>




                            <?php

        }

        ?>

                            <?php
$cnt++;
    }

    ?>

</ul>


                    </div>
                </li>

                <?php

}
?>
                <?php
}
?>




                <li class="singleLink"><a href="#" class="ffBold hoverEffect">My Agilysys</a></li>
                <li class="singleLink">
                    <div class="headerLang">
                        <select class="">
                            <option>EN</option>
                            <option>CH</option>
                            <option>JP</option>
                        </select>
                    </div>
                </li>
                <li class="singleLink">
                    <div class="headerNumber">
                        <a href="tel:877 369 6208">877 369 6208</a>
                    </div>
                </li>
                <li class="singleLink">
                    <div class="headerSeach">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </div>
                </li>
            </ul>

        </nav>

        <!-- menu mobile display -->
        <div id="mobileNav" class="sidenav">
            <div class="activePage center">
                <?php echo $activePage; ?>
            </div>
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

            <ul class="mobileMainMenu">
                <li>
                    <div data-widget="showhide" data-type="panel" class="agMobileMenu">
                        <h2 id="showDiv1">
                            <a href="javascript:void();" class="ffBold">Industries <span class="menuIcon"></span></a>
                        </h2>
                        <ul class="subMenu" id="div1">
                            <li><a href=#>Industries</a></li>
                            <li><a href=#>Hotels &amp; Resorts</a></li>
                            <li><a href=#>Casino Resorts</a></li>
                            <li><a href=#>Tribal Gaming</a></li>
                            <li><a href=#>Cruise Lines</a></li>
                            <li><a href=#>Corp Cafeteria</a></li>
                            <li><a href=#>Healthcare</a></li>
                            <li><a href=#>Universities</a></li>
                            <li><a href=#>Sports &amp; Entertainment</a></li>
                            <li><a href=#>Restaurants</a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <div data-widget="showhide" data-type="panel" class="agMobileMenu">
                        <h2 id="showDiv2">
                            <a href="javascript:void();">Solutions <span class="menuIcon"></span></a>
                        </h2>

                        <ul class="subMenu" id="div2">
                            <li><a href="#">Solutions</a>
                            <li><a href="javascript:void();">Property Management</a>
                                <ul class="childSubMenu">
                                    <li><a href=#>Agilysys Stay</a></li>
                                    <li><a href=#>Agilysys LMS</a></li>
                                    <li><a href=#>Agilysys Visual One PMS</a></li>
                                    <li><a href=#>Agilysys Sales and Catering</a></li>
                                    <li><a href=#>rGuest� Book</a></li>
                                    <li><a href=#>rGuest� Express Kiosk </a></li>
                                    <li><a href=#>rGuest� Express Mobile</a></li>
                                    <li><a href=#>rGuest� Service</a></li>
                                    <li><a href=#>b4</a></li>
                                </ul>
                            </li>
                            <li><a href=javascript:void();>Point of Sale</a>
                                <ul class="childSubMenu">
                                    <li><a href=#>Agilysys InfoGenesis</a></li>
                                    <li><a href=#>IG Flex</a></li>
                                    <li><a href=#>IG Buy</a></li>
                                    <li><a href=#>IG OnDemand</a></li>
                                </ul>
                            </li>
                            <li><a href=#>Payment Solution</a>
                                <ul class="childSubMenu">
                                    <li><a href="">Agilysys Pay</a></li>
                                </ul>
                            </li>
                            <li><a href=#>Analytics &amp; Marketing Loyalty</a>
                                <ul class="childSubMenu">
                                    <li><a href=#>Agilysys Analyze</a></li>
                                </ul>
                            </li>
                            <li><a href=#>Inventory &amp; Procurement</a>
                                <ul class="childSubMenu">
                                    <li><a href="#">Agilysys Eatec</a></li>
                                    <li><a href="#">Agilysys SWS</a></li>
                                </ul>
                            </li>
                            <li><a href=#>Reservations and Table Management</a>
                                <ul class="childSubMenu">
                                    <li><a href="#">Agilysys Seat</a></li>
                                </ul>
                            </li>
                            <li><a href=#>Activity Scheduling</a>
                                <ul class="childSubMenu">
                                    <li><a href="#">Agilysys Golf</a></li>
                                    <li><a href="#">Agilysys Spa</a></li>
                                </ul>
                            </li>
                            <li><a href=#>Document Management</a>
                                <ul class="childSubMenu">
                                    <li><a href="#">Agilysys DataMagine</a></li>
                                </ul>
                            </li>
                            <li><a href=#>Services</a>
                                <ul class="childSubMenu">
                                    <li><a href="#">Professional Services</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <div data-widget="showhide" data-type="panel" class="agMobileMenu">
                        <h2 id="showDiv3">
                            <a href="javascript:void();" class="ffBold">Resources <span class="menuIcon"></span></a>
                        </h2>
                        <ul class="subMenu" id="div3">
                            <li><a href="#">Resources</a></li>
                            <li><a href="#">Knowledge Center</a></li>
                            <li><a href="#">Articles</a></li>
                            <li><a href="#">Product Resources</a></li>
                            <li><a href="#">Product Showcase</a></li>
                            <li><a href="#">Customer Stories</a></li>
                            <li><a href="#">Videos</a></li>
                            <li><a href="#">Webinars</a></li>
                            <li><a href="#">Industry Reports</a></li>
                            <li><a href="#">Blog</a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <div data-widget="showhide" data-type="panel" class="agMobileMenu">
                        <h2 id="showDiv4">
                            <a href="javascript:void();" class="ffBold">Support <span class="menuIcon"></span></a>
                        </h2>
                        <ul class="subMenu" id="div4">
                            <li><a href="#">Support</a></li>
                            <li><a href="#">Payment Center</a></li>
                            <li><a href="#">Contact Support</a></li>
                            <li><a href="#">Mircrosoft Patches</a></li>
                            <li><a href="#">RMA Requests</a></li>
                            <li><a href="#">Supply Orders</a></li>
                            <li><a href="#">LMS/SWS Licensed Upgrade Request</a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <div data-widget="showhide" data-type="panel" class="agMobileMenu">
                        <h2 id="showDiv5">
                            <a href="javascript:void();" class="ffBold"> About <span class="menuIcon"></span></a>
                        </h2>
                        <ul class="subMenu" id="div5">
                            <li><a href="#">About</a></li>
                            <li><a href="#">Careers</a></li>
                            <li><a href="#">Contact Us</a></li>
                            <li><a href="#">Investor Relations</a></li>
                            <li><a href="#">Leadership BOD</a></li>
                            <li><a href="#">News &amp; Events</a></li>
                            <li><a href="#">Our Customers</a></li>
                            <li><a href="#">Solution Partners</a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <h2>
                        <a href="my-agilysys.php"
                            class="<?php if ($activePage == "my-agilysys") {echo "activeNav";}?>  ffBold"> My
                            Agilysys</a>
                    </h2>
                </li>
            </ul>
        </div>

        <span class="openNav" onclick="openNav()">&#9776;</span>
        <!-- menu mobile display end-->

        <div class="hamMegaMenu whiteBG" id="hamMegaMenu">
            <div class="hamMegaMenuOption flex">
                <div class="hamMegaMenuList greyBG">
                    <ul>
                        <li role="presentation" class="blackText"><strong>Explore</strong></li>
                        <li role="presentation" class="blackText active" id="industriesShow"><a
                                class="blackText">Industries</a></li>
                        <li role="presentation" class="blackText" id="solutionsShow"><a class="blackText">Solutions</a>
                        </li>
                        <li role="presentation" class="blackText" id="resourcesShow"><a class="blackText">Resources</a>
                        </li>
                        <li role="presentation" class="blackText" id="supportShow"><a class="blackText">Support</a></li>
                        <li role="presentation" class="blackText" id="aboutShow"><a class="blackText">About</a></li>
                        <li role="presentation" class="blackText" id="myAgilysysShow"><a class="blackText">My
                                Agilysys</a></li>
                    </ul>
                </div>
                <div class="hamMegaMenuContanier">
                    <div class="hamMenuListRow" id="industries">
                        <div class="hamMenuListColumn flex">
                            <div class="hamMenuLinks">
                                <h2 class="dinProStd h2"><a class="greenText" href="#">Industries</a></h2>
                                <ul>
                                    <li><a class="showIndustries blackText active" id="firstLink" href="#"
                                            target="1">Hotels &amp; Resorts</a></li>
                                    <li><a class="showIndustries blackText" href="#" target="2">Casino Resorts</a></li>
                                    <li><a class="showIndustries blackText" href="#" target="3">Tribal Gaming</a></li>
                                    <li><a class="showIndustries blackText" href="#" target="4">Cruise Lines</a></li>
                                    <li><a class="blackText" href="#" target="5">Corp Cafeteria</a></li>
                                    <li><a class="showIndustries blackText" href="#" target="6">Healthcare</a></li>
                                    <li><a class="showIndustries blackText" href="#" target="7">Universities</a></li>
                                    <li><a class="showIndustries blackText" href="#" target="8">Sports &amp;
                                            Entertainment</a></li>
                                    <li><a class="showIndustries blackText" href="#" target="9">Restaurants</a></li>
                                </ul>
                            </div>
                            <div class="hamMenuImgSection active" id="indust1">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Hotels &amp; Resorts</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="indust2">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Casino Resorts</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="indust3">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Tribal Gaming</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="indust4">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Cruise Lines</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="indust5">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Corp Cafeteria</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="indust6">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Healthcare</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="indust7">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Universities</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="indust8">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Sports &amp; Entertainment</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="indust9">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Restaurants</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                        </div>
                    </div>
                    <div class="hamMenuListRow hide" id="solutions">
                        <div class="hamMenuListColumn flex">
                            <div class="hamMenuLinks">
                                <h2 class="dinProStd h2"><a class="greenText" href="#">Solutions</a></h2>
                                <ul>
                                    <li><a class="showSolutions blackText active" id="firstLink" href="#"
                                            target="1">Property Management</a></li>
                                    <li><a class="showSolutions blackText" href="#" target="2">Point of Sale</a></li>
                                    <li><a class="showSolutions blackText" href="#" target="3">Payment Solution</a></li>
                                    <li><a class="showSolutions blackText" href="#" target="4">Analytics &amp; Marketing
                                            Loyalty</a></li>
                                    <li><a class="showSolutions blackText" href="#" target="5">Inventory &amp;
                                            Procurement</a></li>
                                    <li><a class="showSolutions blackText" href="#" target="6">Reservations and Table
                                            Management</a></li>
                                    <li><a class="showSolutions blackText" href="#" target="7">Activity Scheduling</a>
                                    </li>
                                    <li><a class="showSolutions blackText" href="#" target="8">Document Management</a>
                                    </li>
                                    <li><a class="showSolutions blackText" href="#" target="9">Services</a></li>
                                </ul>
                            </div>
                            <div class="hamMenuImgSection active" id="sol1">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Property Management</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="sol2">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Point of Sale</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="sol3">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Payment Solution</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="sol4">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Analytics &amp; Marketing Loyalty</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="sol5">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Inventory &amp; Procurement</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="sol6">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Reservations and Table Management</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="sol7">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Activity Scheduling</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="sol8">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Document Management</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="sol9">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Services</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                        </div>
                    </div>
                    <div class="hamMenuListRow hide" id="resources">
                        <div class="hamMenuListColumn flex">
                            <div class="hamMenuLinks">
                                <h2 class="dinProStd h2"><a class="greenText" href="#">Resources</a></h2>
                                <ul>
                                    <li><a class="showResources blackText active" id="firstLink" href="#"
                                            target="1">Knowledge Center</a></li>
                                    <li><a class="showResources blackText" href="#" target="2">Articles</a></li>
                                    <li><a class="showResources blackText" href="#" target="3">Product Resources</a>
                                    </li>
                                    <li><a class="showResources blackText" href="#" target="4">Product Showcase</a></li>
                                    <li><a class="showResources blackText" href="#" target="5">Customer Stories</a></li>
                                    <li><a class="showResources blackText" href="#" target="6">Videos</a></li>
                                    <li><a class="showResources blackText" href="#" target="7">Webinars</a></li>
                                    <li><a class="showResources blackText" href="#" target="8">Industry Reports</a></li>
                                    <li><a class="showResources blackText" href="#" target="9">Blog</a></li>
                                </ul>
                            </div>
                            <div class="hamMenuImgSection active" id="res1">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Knowledge Center</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="res2">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Articles</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="res3">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Product Resources</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="res4">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Product Showcase</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="res5">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Customer Stories</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="res6">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Videos</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="res7">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Webinars</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="res8">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Industry Reports</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="res9">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Blog</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                        </div>
                    </div>
                    <div class="hamMenuListRow hide" id="support">
                        <div class="hamMenuListColumn flex">

                            <div class="hamMenuLinks">
                                <h2 class="dinProStd h2"><a href="#" class="greenText">Support</a></h2>
                                <ul>
                                    <li><a class="showSupport blackText active" id="firstLink" href="#"
                                            target="1">Payment Center</a></li>
                                    <li><a class="showSupport blackText" href="#" target="2">Contact Support</a></li>
                                    <li><a class="showSupport blackText" href="#" target="3">Mircrosoft Patches</a></li>
                                    <li><a class="showSupport blackText" href="#" target="4">RMA Requests</a></li>
                                    <li><a class="showSupport blackText" href="#" target="5">Supply Orders</a></li>
                                    <li><a class="showSupport blackText" href="#" target="6">LMS/SWS Licensed Upgrade
                                            Request</a></li>
                                </ul>
                            </div>
                            <div class="hamMenuImgSection active" id="sup1">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Payment Center</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="sup2">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Contact Support</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="sup3">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Mircrosoft Patches</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="sup4">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">RMA Requests</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="sup5">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Supply Orders</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="sup6">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">LMS/SWS Licensed Upgrade Request</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                        </div>
                    </div>
                    <div class="hamMenuListRow hide" id="about">
                        <div class="hamMenuListColumn flex">
                            <div class="hamMenuLinks">
                                <h2 class="dinProStd h2"><a href="#" class="greenText">About</a></h2>
                                <ul>
                                    <li><a class="showAbout blackText active" id="firstLink" href="#"
                                            target="1">Careers</a></li>
                                    <li><a class="showAbout blackText" href="#" target="2">Contact Us</a></li>
                                    <li><a class="showAbout blackText" href="#" target="3">Investor Relations</a></li>
                                    <li><a class="showAbout blackText" href="#" target="4">Leadership BOD</a></li>
                                    <li><a class="showAbout blackText" href="#" target="5">News &amp; Events</a></li>
                                    <li><a class="showAbout blackText" href="#" target="6">Our Customers</a></li>
                                    <li><a class="showAbout blackText" href="#" target="7">Solution Partners</a></li>
                                </ul>
                            </div>
                            <div class="hamMenuImgSection active" id="about1">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Careers</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="about2">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Contact Us</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="about3">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Investor Relations</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="about4">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Leadership BOD</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="about5">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">News &amp; Events</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="about6">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Our Customers</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                            <div class="hamMenuImgSection hide" id="about7">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">Solution Partners</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                        </div>
                    </div>
                    <div class="hamMenuListRow hide" id="myAgilysys">
                        <div class="hamMenuListColumn flex">
                            <div class="hamMenuLinks">
                                <h2 class="dinProStd h2"><a href="#" class="greenText">My Agilysys</a></h2>
                                <ul>
                                    <li><a class="blackText active" id="firstLink" href="#">My Agilysys</a></li>
                                </ul>
                            </div>
                            <div class="hamMenuImgSection">
                                <img class="img-fluid" src="img/menu-img.jpg" alt="" />
                                <h4 class="blackText">My Agilysys</h4>
                                <p>From the front desk to the back office, and everywhere in between, Agilysys solutions
                                    are designed to streamline operations, so you can focus on accelerating business
                                    growth and improving guest satisfaction.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
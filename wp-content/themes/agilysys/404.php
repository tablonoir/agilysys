<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Agilysys
 * 
 */
get_header(); 

?>

<section class="fourNotFourImg col-12 col-sm-12 col-md-12 col-lg-12">
    <img class="img-fluid" src="<?php echo get_template_directory_uri();?>/img/404-img.png" alt="" />
</section>

<section class="fourNotFourContetSection orangeBG whiteText center col-12 col-sm-12 col-md-12 col-lg-12">
    <h3 class="dinProStd">Sorry for the inconvenience but the page you are looking for is no longer available.  Please tr y one of our sections listed below. If you require support assistance, please contact our support line at <a class="whiteText" href="tel:800 327 7088">800 327 7088</a> and someone will assist you.</h3>
    <a class="homeBannerButton whiteText" href="<?php echo site_url();?>/">Go Back to Home Page <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
</section>

<section class="homeFooterImg">
    <img class="img-fluid" src="<?php echo get_template_directory_uri();?>/img/home-footer-img.jpg" alt="" />
</section>

<?php get_footer(); ?>
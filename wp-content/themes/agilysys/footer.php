<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Agilysys
 */

?>


<footer class="footerMain">   
    
        <section id="footer" class="jr-site-footer">
        <div class="container-fluid footer-container">
                            
                <div class="row footerSignup">
                    <?php if ( is_active_sidebar( 'agilysys-footer-1' ) ) : ?>
                    <?php get_template_part( 'template-parts/footer1' , 'sidebar' ); ?>
                <?php endif; ?>
                </div>
                <div class="row footerSocial">
                    <?php if ( is_active_sidebar( 'agilysys-footer-2' ) ) : ?>
                    <?php get_template_part( 'template-parts/footer2' , 'sidebar' ); ?>
                <?php endif; ?>
                </div>                

        </div>

        
    </section>
    
    </footer>
    
    <!--Ends-->
      
<?php wp_footer(); ?>




</body>
</html>

<style>

body { 
    
    padding-right: 0 !important;
    padding-left: 0 !important;
    
}

    .modal {
 overflow-y: auto;
}

.modal-open {
 overflow: auto;
}
</style>


<div class="modal in" id="myModal" style="height:180px;width:650px;background:#FFF;color:#000;margin-top:auto;margin-bottom:auto;margin-left:auto;margin-right:auto;border-radius:10px;">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>
      
    </div>
    <div class="modal-body">
        <h5 class="text-center">Mail Sent Successfully</h5>
    </div>
    <div class="modal-footer">
   
    </div>
</div>
<?php

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

require get_template_directory() ."/inc/theme-support.php";

add_theme_support( 'post-thumbnails' );
    

function load_scripts() {
    global $post;

//    wp_enqueue_style('agi-widgets', get_template_directory_uri() . '/inc/widgets/style.css', array('jquery'), '', false);

  
  
// wp_register_script('add-sd-custom', get_template_directory_uri() . '/form-slider/formslider.js', array('jquery'),'null', true );
// wp_register_style('add-sd-css',get_stylesheet_directory_uri() . '/form-slider/styles.css', '','','screen' );

 
    if( is_page() || is_single() )
    {
        switch($post->post_name) // post_name is the post slug which is more consistent for matching to here
        {
            case 'home':
                wp_enqueue_script('home', get_template_directory_uri() . '/js/home-animations.js', array('jquery'), '', false);
                break;
            case 'rguest-express-mobile':
                wp_enqueue_script('rguest-express-mobile', get_template_directory_uri() . '/js/rguest.js', array('jquery'), '', true);
                break;
        }
    } 
}
  
add_action('wp_enqueue_scripts', 'load_scripts');


//create custom widget area
function smallenvelop_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Home page', 'homepageenv' ),
        'id' => 'homepage-section',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h1>',
        'after_title' => '</h1>',
    ) );
}
add_action( 'widgets_init', 'smallenvelop_widgets_init' );

include get_template_directory() . '/inc/widgets/agilysys_contactless_guest_engament.php';

include get_template_directory() . '/inc/widgets/agilysys-banner-widget.php';
include get_template_directory() . '/inc/widgets/agilysys_leaders_in_hopitaliy_soltuion.php';
include get_template_directory() . '/inc/widgets/agilysys_home_divers_client_base.php';
include get_template_directory() . '/inc/widgets/agilysys_leaders_in_hospitality_software.php';
include get_template_directory() . '/inc/widgets/agilysys_blog_post_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_reviews_icons_widgets.php';

include get_template_directory() . '/inc/widgets/agilysys_what_makes.php';

include get_template_directory() . '/inc/widgets/agilysys_reviews_case_study.php';
include get_template_directory() . '/inc/widgets/agilysys_client_logo_widget.php';
include get_template_directory() . '/inc/widgets/agilysys-solutions-3-column-slider.php';
include get_template_directory() . '/inc/widgets/agilysys_solutions_endorser.php';

include get_template_directory() . '/inc/widgets/agilysys_room_ready_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_man_section_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_hospital_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_guest_service_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_solutions_testimonial_with_image_on_left.php';

include get_template_directory() . '/inc/widgets/agilysys_pos_product_info_with_image_on_left_with_additional_text_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_pos_product_info_with_image_on_right_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_pos_testimonial_with_image_on_right_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_pos_titlebox_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_contact_support_by_product_widget.php';
include get_template_directory() . '/inc/widgets/agiliysys_ms_patches_notes_widget.php';
include get_template_directory() . '/inc/widgets/agiliysys_ms_patches_exceptions_widget.php';

include get_template_directory() . '/inc/widgets/agilysys_resources_text_with_image_on_right_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_resources_with_blog_post_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_articles_blog_posts_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_support_widgets.php';
include get_template_directory() . '/inc/widgets/agilysys_webinars_info_widget.php';
include get_template_directory() . '/inc/widgets/agilysys-home-bottom-strip.php';

include get_template_directory() . '/inc/widgets/agilysys_about_image_with_text_on_right_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_about_market_leadership_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_about_trusted_and_loved_by_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_about_when_you_work_widget.php';

include get_template_directory() . '/inc/widgets/agilysys_solutions_leaders_slider.php';

include get_template_directory() . '/inc/widgets/agilysys_customer_slider_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_our_customer_client_logo_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_customer_success_stories_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_pos_3_column_sliders.php';


include get_template_directory() . '/inc/widgets/agilysys_solutions_partners_category_logo_widget.php';
include get_template_directory() . '/inc/widgets/agilysys_contact_support_by_product_2.php';
include get_template_directory() . '/inc/widgets/agilysys_guest_express_widget.php';
include get_template_directory() . '/inc/widgets/agilysys-resources-selfservice.php';   

include get_template_directory() . '/inc/widgets/agilysys_news_events_blog_post_widget.php';   
include get_template_directory() . '/inc/widgets/agilysys_news_events_blog_post_archive_widget.php';   
include get_template_directory() . '/inc/widgets/agilysys_news_events_blog_post_default_widget.php';   

include get_template_directory() . '/inc/widgets/agilysys_resources_video_widget.php';   
include get_template_directory() . '/inc/widgets/agilysys_news_events_inner_main_widget.php';   

include get_template_directory() . '/inc/widgets/agilysys_investor_relations_analyst_coverage.php';   
include get_template_directory() . '/inc/widgets/agilysys_investor_relations_sidebar.php';   

include get_template_directory() . '/inc/widgets/agilysys_investor_relations_presentation.php';   

include get_template_directory() . '/inc/widgets/agilysys_blogs_main_page_widget.php';   

        
add_action( 'load-widgets.php', 'my_custom_load' );


function my_custom_load() {    
    wp_enqueue_style( 'wp-color-picker' );        
    wp_enqueue_script( 'wp-color-picker' );    
}

function zwrew_admin_enqueue()
{
	wp_enqueue_style('thickbox');
	wp_enqueue_script('media-upload');
	wp_enqueue_script('thickbox');
	//
}
add_filter( 'post_thumbnail_html', 'zwrew_remove_thumbnail_dimensions', 10 );
add_filter( 'image_send_to_editor', 'zwrew_remove_thumbnail_dimensions', 10 );
function zwrew_remove_thumbnail_dimensions( $html ) {
	$html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
	return $html;
}


/**
* Function to get attachment id based on url
*/
function zwrew_get_attachment_id1( $attachment_url = '' ) {

	global $wpdb;
	$attachment_id_front = false;
	if ( '' == $attachment_url )
	return;

	$upload_dir_paths = wp_upload_dir();

	if ( false !== strpos( $attachment_url, $upload_dir_paths['baseurl'] ) )
	{
		$attachment_url = preg_replace( '/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $attachment_url );
		$attachment_url = str_replace( $upload_dir_paths['baseurl'] . '/', '', $attachment_url );
		$attachment_id_front = $wpdb->get_var( $wpdb->prepare( "SELECT wposts.ID FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta WHERE wposts.ID = wpostmeta.post_id AND wpostmeta.meta_key = '_wp_attached_file' AND wpostmeta.meta_value = '%s' AND wposts.post_type = 'attachment'", $attachment_url ) );
	}
	return $attachment_id_front;
}

add_action('admin_enqueue_scripts', 'zwrew_admin_enqueue');

add_action('wp_enqueue_scripts', 'include_thickbox_scripts');

//footer files 
require get_template_directory() . '/inc/template-functions.php';
require get_template_directory() . '/inc/customizer.php';

//header menu function
function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      'extra-menu' => __( 'Extra Menu' )
     )
   );
 }
 add_action( 'init', 'register_my_menus' );
 
//search bar
 function agilysys_setup() {
	
		add_theme_support( 'html5', array(
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );
		
	}
add_action( 'after_setup_theme', 'agilysys_setup' );

//header menu
class Walker_Nav_Pointers extends Walker_Nav_Menu
{
    function start_lvl( &$output, $depth = 0, $args = array() )
    {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"sub-menu\">\n";
        $output .= "\n<div class=\"pointer\">\n";
    }
    function end_lvl( &$output, $depth = 0, $args = array() )
    {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul>\n".($depth ? "$indent</div>\n" : "");
    }
}


class Menu_With_Description extends Walker_Nav_Menu {
    function start_el(&$output, $item, $depth, $args) {
        global $wp_query;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
         
        $class_names = $value = '';
 
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
 
        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        $class_names = ' class="' . esc_attr( $class_names ) . '"';
 
        $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .' >';
 
        $attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) .'"' : '';
        $attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) .'"' : '';
        $attributes .= ! empty( $item->url ) ? ' href="#"' : '';
 
        $item_output = $args->before;
        $item_output .= '<a'. $attributes .' onmouseover="show_desc('.$item->ID.')">';
      
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
          $item_output .= '</a>';
          
           
     
        if($item->menu_item_parent==1405 || $item->menu_item_parent==1408)
        {
        $item_output .= '<div style="position:relative;"><div style="position:absolute;margin-left:200px;color:white;"><span class="sub" id="desc_'.
                $item->ID . '">' .$item->title ."<br/>". $item->description . '<a href="'.esc_attr( $item->url ).'">Read More</a></span></div></div>';
        }
        $item_output .= $args->after;
 
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}



//add paginations for articles page

function pagination($pages = '', $range = 4)
{
    $showitems = ($range * 2)+1;
 
    global $paged;
    if(empty($paged)) $paged = 1;
 
    if($pages == '')
    {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages)
        {
            $pages = 1;
        }
    }
 
    if(1 != $pages)
    {
        echo "<div class=\"pagination\"><span>Page ".$paged." of ".$pages."</span>";
        if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
        if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";
 
        for ($i=1; $i <= $pages; $i++)
        {
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
            {
                echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
            }
        }
 
        if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>";
        if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
        echo "</div>\n";
    }
}

//ajax preload


add_action( 'wp_ajax_demo-pagination-load-posts', 'cvf_demo_pagination_load_posts' );

add_action( 'wp_ajax_nopriv_demo-pagination-load-posts', 'cvf_demo_pagination_load_posts' ); 

function cvf_demo_pagination_load_posts() {

    global $wpdb;
    // Set default variables
    $msg = '';

    if(isset($_POST['page'])){
        // Sanitize the received page   
        $page = sanitize_text_field($_POST['page']);
        $cur_page = $page;
        $page -= 1;
        // Set the number of results to display
        $per_page = $_POST['per_page'];
        $previous_btn = true;
        $next_btn = true;
        $first_btn = true;
        $last_btn = true;
        $start = $page * $per_page;

        
        $selected_category = $_POST['category'];    
       

      

       
    $from_date = date("F d,Y",strtotime($_POST['from_date']));
        $to_date = date("F d,Y",strtotime($_POST['to_date']));
       
        $all_blog_posts = new WP_Query(
            array(
                'post_type'         => 'post',
                'post_status '      => 'publish',
                 'date_query' => array(
                  array(
                       'after'     => $from_date,
                       'before'    => $to_date,
                       'inclusive' => true,
                ),
             ),
                'category_name' => $selected_category,
                'orderby'           => 'post_date',
                'order'             => 'DESC',
                'posts_per_page'    => $per_page,
                'offset'            => $start
            )
                
        );

        $count = new WP_Query(
            array(
                'post_type'         => 'post',
                'post_status '      => 'publish',
                 'date_query' => array(
                  array(
                       'after'     => $from_date,
                       'before'    => $to_date,
                       'inclusive' => true,
                ),
             ),
                'category_name' => $selected_category,
                'posts_per_page'    => -1
            )
        );
        
       // echo '<pre>';
        //print_r($all_blog_posts);
  

        // Loop into all the posts
        foreach($all_blog_posts->posts as $key => $post): 
            
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
            
           

            // Set the desired output into a variable
            $msg .= '
            
             <div class="articleBox flex">
        <div class="artImg">
            <img class="img-fluid" src="'.$image[0].'" alt="" />
        </div>
        <div class="articleBoxTitle">
            <h4 class="dinProStd greenText"> ' . $post->post_title . '</h4>
            <h5 class="dinProStd">Featured in "The Overview"</h5>
            <a class="aboutButton violetText">Read the full story <i class="fa fa-arrow-right" aria-hidden="true"></i></a> 
        </div>      
    </div>


';

        endforeach;

      
        
        $countxx = count($count->posts);

        // This is where the magic happens
        $no_of_paginations = ceil($countxx / $per_page);

        if ($cur_page >= 7) {
            $start_loop = $cur_page - 3;
            if ($no_of_paginations > $cur_page + 3)
                $end_loop = $cur_page + 3;
            else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
                $start_loop = $no_of_paginations - 6;
                $end_loop = $no_of_paginations;
            } else {
                $end_loop = $no_of_paginations;
            }
        } else {
            $start_loop = 1;
            if ($no_of_paginations > 7)
                $end_loop = 7;
            else
                $end_loop = $no_of_paginations;
        }

        // Pagination Buttons logic     
        $pag_container .= "
        <div class='cvf-universal-pagination'>
            <ul>";

       if ($first_btn && $cur_page > 1) {
            $pag_container .= "<li p='1' class='active'>First</li>";
        } else if ($first_btn) {
            $pag_container .= "<li p='1' class='inactive'>First</li>";
        }

        if ($previous_btn && $cur_page > 1) {
            $pre = $cur_page - 1;
            $pag_container .= "<li p='$pre' class='active'>Previous</li>";
        } else if ($previous_btn) {
            $pag_container .= "<li class='inactive'>Previous</li>";
        }
         
         
        for ($i = $start_loop; $i <= $end_loop; $i++) {

            if ($cur_page == $i)
                $pag_container .= "<li p='$i' class = 'selected' >{$i}</li>";
            else
                $pag_container .= "<li p='$i' class='active'>{$i}</li>";
        }

        if ($next_btn && $cur_page < $no_of_paginations) {
            $nex = $cur_page + 1;
            $pag_container .= "<li p='$nex' class='active'>Next</li>";
        } else if ($next_btn) {
            $pag_container .= "<li class='inactive'>Next</li>";
        }

        if ($last_btn && $cur_page < $no_of_paginations) {
            $pag_container .= "<li p='$no_of_paginations' class='active'>Last</li>";
        } else if ($last_btn) {
            $pag_container .= "<li p='$no_of_paginations' class='inactive'>Last</li>";
        }

        $pag_container = $pag_container . "
            </ul>
        </div>";
        
        
        $arr = array();
        
        $arr['msg'] = $msg;
        
        $arr['pag_container'] = '<div class = "cvf-pagination-nav">' . $pag_container . '</div>';

        echo json_encode($arr);

    }
    // Always exit to avoid further execution
    exit();
    
   }
   
   
   add_action( 'wp_ajax_fetch-data-agilysys-solutions-partners-category-logo-widget', 'fetch_data_solutions_partners' );

add_action( 'wp_ajax_nopriv_fetch-data-agilysys-solutions-partners-category-logo-widget', 'fetch_data_solutions_partners' );
   
function fetch_data_solutions_partners() {

    
        
   
    $json = stripslashes($_POST['data']);
     $data=preg_replace('/\s+/', '',$json);
  $data = json_decode($data, TRUE);

 

 $cat = $_POST['category'];
 $pro = $_POST['products'];
  
  
    $length = $_POST['length'];
    
    $arr = array();
    
    
    if($_POST['category']!="" && empty($_POST['products']))
    {
        
       
    
    for($i=0;$i<$length;$i++)
    {
        $block = $data['block-'.$i];
        
    if(isset($block) && $block!="")
        {
            
            $category = $data['category'.$i];
            $products = $data['products'.$i];
            
            
            
            
            if($cat==$category)
            {
               array_push($arr,$data['image_uri'.$i]);
            }
        }
        
    }
    
     foreach($arr as $key)
   {
       
       echo '<div class="solPartnerLogoImg"><img class="img-fluid" src="'.$key.'" alt="" /></div>';

   }
   // Always exit to avoid further execution
    exit();
    
    }
    elseif($_POST['products']!="" && empty($_POST['category'])){
        
         for($i=0;$i<$length;$i++)
    {
        $block = $data['block-'.$i];
        
        if(isset($block) && $block!="")
        {
            
            $category = $data['category'.$i];
            $products = $data['products'.$i];
            
            if($pro==$products)
            {
               array_push($arr,$data['image_uri'.$i]);
            }
        }
    }
     foreach($arr as $key)
   {
       
       echo '<div class="solPartnerLogoImg"><img class="img-fluid" src="'.$key.'" alt="" /></div>';

   }
   // Always exit to avoid further execution
    exit();
        
    }
    elseif($_POST['products']!="" && $_POST['category']!="")
    {
          for($i=0;$i<$length;$i++)
    {
        $block = $data['block-'.$i];
        
        if(isset($block) && $block!="")
        {
            
            $category = $data['category'.$i];
            $products = $data['products'.$i];
            
            if($pro==$products && $cat==$category)
            {
               array_push($arr,$data['image_uri'.$i]);
            }
        }
    }
     foreach($arr as $key)
   {
       
       echo '<div class="solPartnerLogoImg"><img class="img-fluid" src="'.$key.'" alt="" /></div>';

   }
   // Always exit to avoid further execution
    exit();
    }
     elseif($_POST['products']=="" && $_POST['category']=="")
    {
          for($i=0;$i<$length;$i++)
    {
        $block = $data['block-'.$i];
        
        if(isset($block) && $block!="")
        {
            
           
               array_push($arr,$data['image_uri'.$i]);
            
        }
    }
     foreach($arr as $key)
   {
       
       echo '<div class="solPartnerLogoImg"><img class="img-fluid" src="'.$key.'" alt="" /></div>';

   }
   // Always exit to avoid further execution
    exit();
    }
   
    

    

}



add_action( 'wp_ajax_fetch-data-agilysys-solutions-partners-category-logo-widget-view-all', 'fetch_data_solutions_partners_view_all' );

add_action( 'wp_ajax_nopriv_fetch-data-agilysys-solutions-partners-category-logo-widget-view-all', 'fetch_data_solutions_partners_view_all' );


function fetch_data_solutions_partners_view_all(){
     $json = stripslashes($_POST['data']);
     $data=preg_replace('/\s+/', '',$json);
  $data = json_decode($data, TRUE);
  
  $length = $_POST['length'];
  
  $arr = array();
  
       for($i=0;$i<$length;$i++)
    {
        $block = $data['block-'.$i];
        
        if(isset($block) && $block!="")
        {
            
            
               array_push($arr,$data['image_uri'.$i]);
            
        }
    }
     foreach($arr as $key)
   {
       
       echo '<div class="solPartnerLogoImg"><img class="img-fluid" src="'.$key.'" alt="" /></div>';

   }
   // Always exit to avoid further execution
    exit();
}


add_action('wp_ajax_fetch-data-agilysys-webinars-widget', 'fetch_data_agilysys_webinars_widget');

add_action('wp_ajax_nopriv_fetch-data-agilysys-webinars-widget', 'fetch_data_agilysys_webinars_widget');

function fetch_data_agilysys_webinars_widget()
{

    $json = stripslashes($_POST['data']);
    $data = preg_replace('/\s+/', ' ', $json);
    $data = json_decode($data, true);
    $html = '';
    $per_page = 3;
    $page = $_POST['page'];
    $pag_container = '';
    $cur_page = $page;
    $pro = $_POST['products'];
    $ind = $_POST['industries'];

    $arr = array();

    if ($_POST['products'] != "" && $_POST['industries'] != "") {

        for ($i = 0; $i < 150; $i++) {
            $block = $data['block-' . $i];
            if (isset($block) && $block != "") {

                $industries = $data['industries' . $i];
                $products = $data['products' . $i];

                $arr1 = array();
                $arr1['block'] = $data['block-' . $i];
                $arr1['section_title'] = $data['section_title-' . $i];
                $arr1['webinars_info_front_image'] = $data['webinars_info_front_image' . $i];
                $arr1['webinars_info_desc'] = $data['webinars_info_desc-' . $i];
                $arr1['webinars_info_url_title'] = $data['webinars_info_url_title-' . $i];
                $arr1['video_type'] = $data['video_type' . $i];
                $arr1['video_uri'] = $data['video_uri' . $i];
                $arr1['youtube_url'] = $data['youtube_url' . $i];

                if ($products == $pro && $industries == $ind) {
                    array_push($arr, $arr1);
                }
            }

        }
    } elseif ($_POST['products'] == "" && $_POST['industries'] != "") {

        for ($i = 0; $i < 150; $i++) {
            $block = $data['block-' . $i];
            if (isset($block) && $block != "") {

                $industries = $data['industries' . $i];
                $products = $data['products' . $i];

                $arr1 = array();
                $arr1['block'] = $data['block-' . $i];
                $arr1['section_title'] = $data['section_title-' . $i];
                $arr1['webinars_info_front_image'] = $data['webinars_info_front_image' . $i];
                $arr1['webinars_info_desc'] = $data['webinars_info_desc-' . $i];
                $arr1['webinars_info_url_title'] = $data['webinars_info_url_title-' . $i];
                $arr1['video_type'] = $data['video_type' . $i];
                $arr1['video_uri'] = $data['video_uri' . $i];
                $arr1['youtube_url'] = $data['youtube_url' . $i];
                if ($industries == $ind) {
                    array_push($arr, $arr1);
                }
            }

        }

    } elseif ($_POST['products'] != "" && $_POST['industries'] == "") {

        for ($i = 0; $i < 150; $i++) {
            $block = $data['block-' . $i];
            if (isset($block) && $block != "") {

                $industries = $data['industries' . $i];
                $products = $data['products' . $i];

                $arr1 = array();
                $arr1['block'] = $data['block-' . $i];
                $arr1['section_title'] = $data['section_title-' . $i];
                $arr1['webinars_info_front_image'] = $data['webinars_info_front_image' . $i];
                $arr1['webinars_info_desc'] = $data['webinars_info_desc-' . $i];
                $arr1['webinars_info_url_title'] = $data['webinars_info_url_title-' . $i];
                $arr1['video_type'] = $data['video_type' . $i];
                $arr1['video_uri'] = $data['video_uri' . $i];
                $arr1['youtube_url'] = $data['youtube_url' . $i];
                if ($products == $pro) {
                    array_push($arr, $arr1);
                }
            }

        }
    } elseif ($_POST['products'] == "" && $_POST['industries'] == "") {

        for ($i = 0; $i < 150; $i++) {
            $block = $data['block-' . $i];
            if (isset($block) && $block != "") {

                $industries = $data['industries' . $i];
                $products = $data['products' . $i];

                $arr1 = array();
                $arr1['block'] = $data['block-' . $i];
                $arr1['section_title'] = $data['section_title-' . $i];
                $arr1['webinars_info_front_image'] = $data['webinars_info_front_image' . $i];
                $arr1['webinars_info_desc'] = $data['webinars_info_desc-' . $i];
                $arr1['webinars_info_url_title'] = $data['webinars_info_url_title-' . $i];
                $arr1['video_type'] = $data['video_type' . $i];
                $arr1['video_uri'] = $data['video_uri' . $i];
                $arr1['youtube_url'] = $data['youtube_url' . $i];

                array_push($arr, $arr1);

            }

        }
    }

    $dataxx = array();

    $start = ($page - 1) * $per_page;

    for ($i = $start; $i < $start + $per_page; $i++) {

        array_push($dataxx, $arr[$i]);
    }

    $cnt = 0;
    foreach ($dataxx as $key) {
        if (empty($key)) {
            unset($dataxx[$cnt]);
        }
        $cnt++;
    }

    $cnt = 0;
    foreach ($dataxx as $key) {

        $section_title = strip_tags($key['section_title']);
        $webinars_info_front_image = strip_tags($key['webinars_info_front_image']);
        $webinars_info_desc = strip_tags($key['webinars_info_desc']);
        $webinars_info_url_title = strip_tags($key['webinars_info_url_title']);

        $video_uri = '';
        $youtube_url = '';

        $video_type = $key['video_type'];

        if ($video_type == "video") {
            $video_uri = $key['video_uri'];

        } elseif ($video_type == "youtube") {
            $youtube_url = $key['youtube_url'];
        }

        $html .= '<div class="webInnarBox flex"><div class="webInnarImg"><img class="img-fluid" src="' . $webinars_info_front_image . '" alt="" /></div><div class="webInnarContent"><h4 class="dinProStd greenText">' . $section_title . '</h4><p>' . $webinars_info_desc . '</p><a class="aboutButton violetText" data-toggle="modal" data-target="#myModal" data-id="' . $cnt . '">' . $webinars_info_url_title . '<i class="fa fa-arrow-right" aria-hidden="true"></i></a></div></div><input type="hidden" name="video_type_' . $cnt . '" id="video_type_' . $cnt . '" value="' . $video_type . '"><input type="hidden" name="video_uri_' . $cnt . '" id="video_uri_' . $cnt . '" value="' . $video_uri . '"><input type="hidden" name="youtube_url_' . $cnt . '" id="youtube_url_' . $cnt . '" value="' . $youtube_url . '">';

        $cnt++;

    }

    $countxx = count($arr);

    // This is where the magic happens
    $no_of_paginations = ceil($countxx / $per_page);

    if ($cur_page >= 7) {
        $start_loop = $cur_page - 3;
        if ($no_of_paginations > $cur_page + 3) {
            $end_loop = $cur_page + 3;
        } else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
            $start_loop = $no_of_paginations - 6;
            $end_loop = $no_of_paginations;
        } else {
            $end_loop = $no_of_paginations;
        }
    } else {
        $start_loop = 1;
        if ($no_of_paginations > 7) {
            $end_loop = 7;
        } else {
            $end_loop = $no_of_paginations;
        }

    }

    // Pagination Buttons logic
    $pag_container .= "
     <div class='cvf-universal-pagination'>
         <ul>";

    for ($i = $start_loop; $i <= $end_loop; $i++) {

        if ($cur_page == $i) {
            $pag_container .= "<li p='$i' class = 'selected' >{$i}</li>";
        } else {
            $pag_container .= "<li p='$i' class='active'>{$i}</li>";
        }

    }

    $pag_container = $pag_container . "
         </ul>
     </div>";

    $arr2 = array();

    $arr2['msg'] = $html;

    $arr2['pag_container'] = '<div class = "cvf-pagination-nav">' . $pag_container . '</div>';

    echo json_encode($arr2);
    exit();

}


add_action('wp_ajax_demo-pagination-load-posts-agilysys-news-events-blog-post-default-widget', 'agilysys_demo_pagination_load_posts_news_events_default');

add_action('wp_ajax_nopriv_demo-pagination-load-posts-agilysys-news-events-blog-post-default-widget', 'agilysys_demo_pagination_load_posts_news_events_default');

function agilysys_demo_pagination_load_posts_news_events_default()
{

    global $wpdb;
    // Set default variables
    $msg = '';

    if (isset($_POST['page'])) {
        // Sanitize the received page
        $page = sanitize_text_field($_POST['page']);
        $cur_page = $page;
        $page -= 1;
        // Set the number of results to display
        $per_page = $_POST['per_page'];

        $start = $page * $per_page;

        $sub_cat = $_POST['sub_cat'];

        $sort_by = $_POST['sort_by'];
        $sort_order = $_POST['sort_order'];

        $all_blog_posts = new WP_Query(
            array(
                'post_type' => 'post',
                'post_status ' => 'publish',
                'category__in' => array($sub_cat),
                'orderby' => $sort_by,
                'order' => $sort_order,
                'posts_per_page' => $per_page,
                'offset' => $start,
            )

        );

        $count = new WP_Query(
            array(
                'post_type' => 'post',
                'post_status ' => 'publish',

                'category__in' => array($sub_cat),
                'posts_per_page' => -1,
            )
        );

        // echo '<pre>';
        //print_r($all_blog_posts);

        // Loop into all the posts
        foreach ($all_blog_posts->posts as $key => $post):

            $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail');
            $postyy = get_post($post->ID);

            $slug = "'".$postyy->post_name."'";



            // Set the desired output into a variable
            $msg .= '<div class="newsBox flex">
		                                <div class="newsBoxImg">
		                                    <img class="img-fluid" src="' . $image[0] . '" alt="" />
		                                </div>
		                                <div class="newsBoxContent">
		                                    <div class="newsDate greenText">AUG 18, 2020</div>
		                                    <div class="catList violetText">Press Release</div>
		                                    <div class="newsBoxTitle">
		                                        <h3 class="dinProStd greenText"> ' . $post->post_title . '</h3>
		                                        <a onClick="navigate_to_page('.trim($slug).');"  class="aboutButton violetText center">Read More <i class="fa fa-arrow-right"
		                                                aria-hidden="true"></i></a>
		                                    </div>
		                                </div>
		                            </div>';

        endforeach;

        $countxx = count($count->posts);

        // This is where the magic happens
        $no_of_paginations = ceil($countxx / $per_page);

        if ($cur_page >= 7) {
            $start_loop = $cur_page - 3;
            if ($no_of_paginations > $cur_page + 3) {
                $end_loop = $cur_page + 3;
            } else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
                $start_loop = $no_of_paginations - 6;
                $end_loop = $no_of_paginations;
            } else {
                $end_loop = $no_of_paginations;
            }
        } else {
            $start_loop = 1;
            if ($no_of_paginations > 7) {
                $end_loop = 7;
            } else {
                $end_loop = $no_of_paginations;
            }

        }

        // Pagination Buttons logic
        $pag_container .= "
        <div class='cvf-universal-pagination'>
            <ul>";

        if ($first_btn && $cur_page > 1) {
            $pag_container .= "<li p='1' class='active'>First</li>";
        } else if ($first_btn) {
            $pag_container .= "<li p='1' class='inactive'>First</li>";
        }

        if ($previous_btn && $cur_page > 1) {
            $pre = $cur_page - 1;
            $pag_container .= "<li p='$pre' class='active'>Previous</li>";
        } else if ($previous_btn) {
            $pag_container .= "<li class='inactive'>Previous</li>";
        }

        for ($i = $start_loop; $i <= $end_loop; $i++) {

            if ($cur_page == $i) {
                $pag_container .= "<li p='$i' class = 'selected' >{$i}</li>";
            } else {
                $pag_container .= "<li p='$i' class='active'>{$i}</li>";
            }

        }

        if ($next_btn && $cur_page < $no_of_paginations) {
            $nex = $cur_page + 1;
            $pag_container .= "<li p='$nex' class='active'>Next</li>";
        } else if ($next_btn) {
            $pag_container .= "<li class='inactive'>Next</li>";
        }

        if ($last_btn && $cur_page < $no_of_paginations) {
            $pag_container .= "<li p='$no_of_paginations' class='active'>Last</li>";
        } else if ($last_btn) {
            $pag_container .= "<li p='$no_of_paginations' class='inactive'>Last</li>";
        }

        $pag_container = $pag_container . "
            </ul>
        </div>";

        $arr = array();

        $arr['msg'] = $msg;

        $arr['pag_container'] = '<div class = "cvf-pagination-nav">' . $pag_container . '</div>';

        echo json_encode($arr);

    }
    // Always exit to avoid further execution
    exit();
}


add_action('wp_ajax_fetch-data-resources-videos-widget', 'fetch_data_agilysys_videos_widget');

add_action('wp_ajax_nopriv_fetch-data-resources-videos-widget', 'fetch_data_agilysys_videos_widget');

function fetch_data_agilysys_videos_widget(){

    $json = stripslashes($_POST['data']);
    $data = preg_replace('/\s+/', ' ', $json);
    $data = json_decode($data, true);
    $html = '';
    $per_page = 3;
    $page = $_POST['page'];
    $pag_container = '';
    $cur_page = $page;
    $pro = $_POST['products'];
    $ind = $_POST['industries'];

    $arr = array();

    if ($_POST['products'] != "" && $_POST['industries'] != "") {

        for ($i = 0; $i < 150; $i++) {
            $block = $data['block-' . $i];
            if (isset($block) && $block != "") {

                $industries = $data['industries' . $i];
                $products = $data['products' . $i];

                $arr1 = array();
                $arr1['block'] = $data['block-' . $i];
                $arr1['section_title'] = $data['section_title-' . $i];
                $arr1['webinars_info_front_image'] = $data['webinars_info_front_image' . $i];
                $arr1['webinars_info_desc'] = $data['webinars_info_desc-' . $i];
                $arr1['webinars_info_url_title'] = $data['webinars_info_url_title-' . $i];
                $arr1['video_type'] = $data['video_type' . $i];
                $arr1['video_uri'] = $data['video_uri' . $i];
                $arr1['youtube_url'] = $data['youtube_url' . $i];

                if ($products == $pro && $industries == $ind) {
                    array_push($arr, $arr1);
                }
            }

        }
    } elseif ($_POST['products'] == "" && $_POST['industries'] != "") {

        for ($i = 0; $i < 150; $i++) {
            $block = $data['block-' . $i];
            if (isset($block) && $block != "") {

                $industries = $data['industries' . $i];
                $products = $data['products' . $i];

                $arr1 = array();
                $arr1['block'] = $data['block-' . $i];
                $arr1['section_title'] = $data['section_title-' . $i];
                $arr1['webinars_info_front_image'] = $data['webinars_info_front_image' . $i];
                $arr1['webinars_info_desc'] = $data['webinars_info_desc-' . $i];
                $arr1['webinars_info_url_title'] = $data['webinars_info_url_title-' . $i];
                $arr1['video_type'] = $data['video_type' . $i];
                $arr1['video_uri'] = $data['video_uri' . $i];
                $arr1['youtube_url'] = $data['youtube_url' . $i];
                if ($industries == $ind) {
                    array_push($arr, $arr1);
                }
            }

        }

    } elseif ($_POST['products'] != "" && $_POST['industries'] == "") {

        for ($i = 0; $i < 150; $i++) {
            $block = $data['block-' . $i];
            if (isset($block) && $block != "") {

                $industries = $data['industries' . $i];
                $products = $data['products' . $i];

                $arr1 = array();
                $arr1['block'] = $data['block-' . $i];
                $arr1['section_title'] = $data['section_title-' . $i];
                $arr1['webinars_info_front_image'] = $data['webinars_info_front_image' . $i];
                $arr1['webinars_info_desc'] = $data['webinars_info_desc-' . $i];
                $arr1['webinars_info_url_title'] = $data['webinars_info_url_title-' . $i];
                $arr1['video_type'] = $data['video_type' . $i];
                $arr1['video_uri'] = $data['video_uri' . $i];
                $arr1['youtube_url'] = $data['youtube_url' . $i];
                if ($products == $pro) {
                    array_push($arr, $arr1);
                }
            }

        }
    } elseif ($_POST['products'] == "" && $_POST['industries'] == "") {

        for ($i = 0; $i < 150; $i++) {
            $block = $data['block-' . $i];
            if (isset($block) && $block != "") {

                $industries = $data['industries' . $i];
                $products = $data['products' . $i];

                $arr1 = array();
                $arr1['block'] = $data['block-' . $i];
                $arr1['section_title'] = $data['section_title-' . $i];
                $arr1['webinars_info_front_image'] = $data['webinars_info_front_image' . $i];
                $arr1['webinars_info_desc'] = $data['webinars_info_desc-' . $i];
                $arr1['webinars_info_url_title'] = $data['webinars_info_url_title-' . $i];
                $arr1['video_type'] = $data['video_type' . $i];
                $arr1['video_uri'] = $data['video_uri' . $i];
                $arr1['youtube_url'] = $data['youtube_url' . $i];

                array_push($arr, $arr1);

            }

        }
    }

    $dataxx = array();

    $start = ($page - 1) * $per_page;

    for ($i = $start; $i < $start + $per_page; $i++) {

        array_push($dataxx, $arr[$i]);
    }

    $cnt = 0;
    foreach ($dataxx as $key) {
        if (empty($key)) {
            unset($dataxx[$cnt]);
        }
        $cnt++;
    }

    $cnt = 0;
    foreach ($dataxx as $key) {

        $section_title = strip_tags($key['section_title']);
        $webinars_info_front_image = strip_tags($key['webinars_info_front_image']);
        $webinars_info_desc = strip_tags($key['webinars_info_desc']);
        $webinars_info_url_title = strip_tags($key['webinars_info_url_title']);

        $video_uri = '';
        $youtube_url = '';

        $video_type = $key['video_type'];

        if ($video_type == "video") {
            $video_uri = $key['video_uri'];

        } elseif ($video_type == "youtube") {
            $youtube_url = $key['youtube_url'];
        }

       $html .= '<div class="videoBoxSection flex">
       <div class="videoBox flex">';

       if ($video_type == "youtube") {
       $html.='<div class="videoBoxVideo">
               <iframe width="250" height="150" src="'.$youtube_url.'">
               </iframe>
           </div>';
       }
       else if($video_type == "video"){
        $html.='<video width="320" height="240" controls>
        <source src="'.$video_uri.'" type="video/mp4">
          </video>';
       }


       $html.='<div class="videoBoxContent">
               <h3 class="dinProStd greenText">'.$section_title.'</h3>
               <p>'.$webinars_info_desc.'</p>
               <div class="videoLink flex">
                   <a class="aboutButton violetText center" id="videoButton" data-id="' . $cnt . '" data-toggle="modal" data-target="#myModal">Watch the video <i class="fa fa-arrow-right" aria-hidden="true"></i></a> 
                   <a class="aboutButton violetText center">Get a Demo <i class="fa fa-arrow-right" aria-hidden="true"></i></a> 
               </div>
           </div>
       </div>
   </div><input type="hidden" name="video_type_' . $cnt . '" id="video_type_' . $cnt . '" value="' . $video_type . '"><input type="hidden" name="video_uri_' . $cnt . '" id="video_uri_' . $cnt . '" value="' . $video_uri . '"><input type="hidden" name="youtube_url_' . $cnt . '" id="youtube_url_' . $cnt . '" value="' . $youtube_url . '">  ';

        $cnt++;

    }

    $countxx = count($arr);

    // This is where the magic happens
    $no_of_paginations = ceil($countxx / $per_page);

    if ($cur_page >= 7) {
        $start_loop = $cur_page - 3;
        if ($no_of_paginations > $cur_page + 3) {
            $end_loop = $cur_page + 3;
        } else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
            $start_loop = $no_of_paginations - 6;
            $end_loop = $no_of_paginations;
        } else {
            $end_loop = $no_of_paginations;
        }
    } else {
        $start_loop = 1;
        if ($no_of_paginations > 7) {
            $end_loop = 7;
        } else {
            $end_loop = $no_of_paginations;
        }

    }

    // Pagination Buttons logic
    $pag_container .= "
     <div class='cvf-universal-pagination'>
         <ul>";

    for ($i = $start_loop; $i <= $end_loop; $i++) {

        if ($cur_page == $i) {
            $pag_container .= "<li p='$i' class = 'selected' >{$i}</li>";
        } else {
            $pag_container .= "<li p='$i' class='active'>{$i}</li>";
        }

    }

    $pag_container = $pag_container . "
         </ul>
     </div>";

    $arr2 = array();

    $arr2['msg'] = $html;

    $arr2['pag_container'] = '<div class = "cvf-pagination-nav">' . $pag_container . '</div>';

    echo json_encode($arr2);
    exit();

}

//create custom widget area
function smallenvelop_widgets_inits()
{
    register_sidebar(array(
        'name' => __('News & Events', 'homepageenv'),
        'id' => 'news-events',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h1>',
        'after_title' => '</h1>',
    ));
}
add_action('widgets_init', 'smallenvelop_widgets_inits');


//create custom widget area
function investor_relations_sidebar()
{
    register_sidebar(array(
        'name' => __('Investor Relations Sidebar', 'homepageenv'),
        'id' => 'investor-relations',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h1>',
        'after_title' => '</h1>',
    ));
}
add_action('widgets_init', 'investor_relations_sidebar');


// blogs

add_action('wp_ajax_demo-pagination-load-posts-page-blogs-main-page', 'agilysys_demo_pagination_load_posts_page_blogs_main_page');

add_action('wp_ajax_nopriv_demo-pagination-load-posts-page-blogs-main-page', 'agilysys_demo_pagination_load_posts_page_blogs_main_page');

function agilysys_demo_pagination_load_posts_page_blogs_main_page()
{
    
    if (isset($_POST['page'])) {
        // Sanitize the received page
        $page = sanitize_text_field($_POST['page']);
        $cur_page = $page;
        $page -= 1;
        // Set the number of results to display
        $per_page = $_POST['per_page'];

        $start = $page * $per_page;

        $idObj = get_category_by_slug('blogpost');
        $id = $idObj->term_id;

// the query
        $all_blog_posts = new WP_Query(array(
            'post_type' => 'post',
            'category__in' => array($id),
            'post_status' => 'publish',
            'orderby' => 'post_date',
            'order' => 'DESC',
            'posts_per_page' => $per_page,
            'offset' => $start,
        ));

        $count = new WP_Query(
            array(
                'post_type' => 'post',
                'post_status ' => 'publish',

                'category__in' => array($id),
                'posts_per_page' => -1,
            )
        );

         $msg = '';

        foreach ($all_blog_posts->posts as $key) {

            $image = wp_get_attachment_image_src(get_post_thumbnail_id($key->ID), 'single-post-thumbnail');

            $image[0] = preg_replace('/\s+/', '', $image[0]);

            $post_title = html_entity_decode($key->post_title);

            $post_content = html_entity_decode($key->post_content);

            $post_date = date("F d,Y", strtotime($key->post_date));
            $postyy = get_post($key->ID);
            $slug = $postyy->post_name;


            $facebook_link = get_post_meta( $key->ID, 'facebook_link' );

    

            $twitter_link = get_post_meta( $key->ID, 'twitter_link' );
            $linkedin_link = get_post_meta( $key->ID, 'linkedin_link' );

            $msg .= '<div class="blogBox flex"><div class="blogBoxImg"><img class="img-fluid" src="'.$image[0].'" alt=""/></div><div class="blogBoxcontent"><p class="blackText">'.$post_date.'</p><h3 class="dinProStd greenText">'.$post_title.'</h3><p>'.$post_content.'</p><div class="blogSliderContentLink flex"><a href="'.home_url($slug).'/" class="aboutButton violetText" > Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a><div class="blogSliderContentsocial">

            <a href="'.$facebook_link[0].'"><i class="fa fa-facebook" aria-hidden="true" ></i></a>
            <a href="'.$twitter_link[0].'"><i class="fa fa-twitter" aria-hidden="true"></i></a>
            <a href="'.$linkedin_link[0].'"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            <i class="fa fa-print" aria-hidden="true"></i>
            
            </div></div></div></div>';




    }

    $countxx = count($count->posts);

        // This is where the magic happens
        $no_of_paginations = ceil($countxx / $per_page);

        if ($cur_page >= 7) {
            $start_loop = $cur_page - 3;
            if ($no_of_paginations > $cur_page + 3) {
                $end_loop = $cur_page + 3;
            } else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
                $start_loop = $no_of_paginations - 6;
                $end_loop = $no_of_paginations;
            } else {
                $end_loop = $no_of_paginations;
            }
        } else {
            $start_loop = 1;
            if ($no_of_paginations > 7) {
                $end_loop = 7;
            } else {
                $end_loop = $no_of_paginations;
            }

        }

        // Pagination Buttons logic
        $pag_container .= "
        <div class='cvf-universal-pagination'>
            <ul>";

        

        if ($cur_page > 1) {
            $pre = $cur_page - 1;
            $pag_container .= "<li p='$pre' class='active'>Previous Posts</li>";
        } else if ($previous_btn) {
            $pag_container .= "<li class='inactive'>Previous Posts</li>";
        }

        

        if ($cur_page < $no_of_paginations) {
            $nex = $cur_page + 1;
            $pag_container .= "<li p='$nex' class='active'>Next Posts</li>";
        } else if ($next_btn) {
            $pag_container .= "<li class='inactive'>Next Posts</li>";
        }

      

        $pag_container = $pag_container . "
            </ul>
        </div>";

        $arr = array();

        $arr['msg'] = $msg;

        $arr['pag_container'] = '<div class = "cvf-pagination-nav">' . $pag_container . '</div>';

        echo json_encode($arr);

    ?>
        
  <?php

    }
    exit();
}





//create custom widget area
function blogs_main_page_sidebar()
{
    register_sidebar(array(
        'name' => __('Blogs Main Page Sidebar', 'homepageenv'),
        'id' => 'blogs-page',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h1>',
        'after_title' => '</h1>',
    ));
}
add_action('widgets_init', 'blogs_main_page_sidebar');

add_action('wp_ajax_demo-pagination-search-this', 'agilysys_demo_pagination_search_this');

add_action('wp_ajax_nopriv_demo-pagination-search-this', 'agilysys_demo_pagination_search_this');

function agilysys_demo_pagination_search_this(){
    $s = $_POST['s'];

    $query = new WP_Query( 's='.$s );



    $posts = $query->posts;

    $html = '<ul>';

    foreach($posts as $key)
    {

        $postyy = get_post($key->ID);
        $slug = $postyy->post_name;
        $html .= '<li><a href="'.home_url($slug).'/">'.$key->post_title.'</a></li>';
    }

    $html .= '</ul>';

    echo $html;
}


?>
<?php
$activePage = basename($_SERVER['PHP_SELF'], ".php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php

        $site_icon_id = get_theme_mod('agilysys_favicon');

?>

    <link rel="icon" href="<?php echo clean_url_agilysys(esc_url($site_icon_id)); ?>">
    <title><?php wp_title('|',true,'right'); ?> <?php bloginfo('name'); ?></title>

    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="profile" href="https://gmpg.org/xfn/11">


</head>

<body class="">



    <?php
wp_head();
?>




    <div id="overlay">
        <div class="spinner"></div>
    </div>
    <header class="header">
        <div class="hamburgerMenu" id="hamburgerAnimation">
            <div class="bar1"></div>
            <div class="bar2"></div>
            <div class="bar3"></div>
        </div>
        <div class="mainLogo">
            <a href="<?php echo get_option("siteurl"); ?>">
                <?php

$custom_logo_id = get_theme_mod('custom_logo');
$logo = wp_get_attachment_image_src($custom_logo_id, 'full');

echo '<img class="img-fluid" src="' . clean_url_agilysys(esc_url($logo[0])) . '" alt="' . get_bloginfo('name') . '">';

?>



            </a>
        </div>

        <?php
$menu_name = 'header-menu';
$locations = get_nav_menu_locations();
$menu = wp_get_nav_menu_object($locations[$menu_name]);
$menuitems = wp_get_nav_menu_items($menu->term_id, array('order' => 'DESC'));

$top_level = array();
$top_level_id = array();
$children = array();
$children_id = array();
$sub_level_children = array();

$cnt = 0;
foreach ($menuitems as $item) {

    if (!$item->menu_item_parent && $cnt<7) {
        array_push($top_level, $item);
        array_push($top_level_id, $item->ID);
        $cnt++;
    }

}

foreach ($menuitems as $item) {

    if (in_array($item->menu_item_parent, $top_level_id)) {
        $children[$item->menu_item_parent][] = $item;
        array_push($children_id, $item->ID);

    }

}

foreach ($menuitems as $item) {

    if (in_array($item->menu_item_parent, $children_id)) {
        $sub_level_children[$item->menu_item_parent][] = $item;
    }

}

?>


        <nav class="headerNav">
            <ul class="menu">
                <?php
foreach ($top_level as $item) {

    $title = $item->title;
    $link = $item->url;
    $ID = $item->ID;

    ?>
                <li>

                    <?php

    if (!empty($children[$item->ID])) {

        ?>
                    <a href="<?php echo $link; ?>" class="downArrow hoverEffect"
                        id="<?php echo $ID; ?>SubMenu"><?php echo $title; ?></a>

                    <?php

    } else {
        ?>

                    <a href="<?php echo $link; ?>" class="hoverEffect"
                        id="<?php echo $ID; ?>SubMenu"><?php echo $title; ?></a>

                    <?php

    }

    ?>

                    <?php

    if (empty($children[$item->ID])) {

        ?>

                    <script>
                    jQuery(document).ready(function() {
                        jQuery(".headerNav ul li a#<?php echo $ID; ?>SubMenu").hover(function() {
                            jQuery(".dropMenu").hide();

                        });

                    });
                    </script>

                    <?php

    }

        ?>



                    <?php

    if (!empty($children[$item->ID])) {

        ?>
                    <div class="dropMenu greenBG" id="<?php echo $ID; ?>SubMenuShow">


                        <script>
                        jQuery(document).ready(function() {
                            jQuery(".headerNav ul li a#<?php echo $ID; ?>SubMenu").hover(function() {



                                jQuery(".dropMenu").hide();


                                jQuery("#<?php echo $ID; ?>SubMenuShow").show();

                                jQuery('.dropMenuLink').removeClass('active');

                                jQuery('#child_<?php echo $ID; ?>').addClass('active');
                                jQuery('#childxx_<?php echo $ID; ?>').addClass('active');



                            });












                            jQuery(".dropMenu").mouseleave(function() {
                                jQuery(".dropMenu").css('display', 'none');

                            });


                            jQuery(".headerNav").click(function(e) {
                                e.stopPropagation();
                            });

                        });
                        </script>





                        <ul class="dropMenuList">
                            <?php

        $cnt = 0;
        foreach ($children[$item->ID] as $child) {

            $title1 = $child->title;
            $link1 = $child->url;
            $description1 = $child->description;

            if ($cnt == 0) {
                ?>

                            <li>
                                <a class="dinConBold dropmenuAct  dropMenuLink active"
                                    id="child_<?php echo $ID; ?>"><?php echo $title1; ?></a>
                                <ul class="dropMenuPanel active" id="childxx_<?php echo $ID; ?>">
                                    <li class="panelLeft">
                                        <h3 class="greenText dinConBold"><?php echo $title1; ?></h3>
                                        <p class="dinproMed"><?php echo substr($description1,0,550); ?></p>
                                        <a href="<?php echo $link1; ?>" class="aboutButton">Read More <i
                                                class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                    </li>
                                    <li class="panelRight">

                                        <?php

                if (!empty($sub_level_children[$child->ID])) {
                    ?>

                                        <ul>

                                            <?php
    $cnt2 = 0;  
                    foreach ($sub_level_children[$child->ID] as $subchild) {
                        if($cnt2<=8)
                        {
                        $title2 = $subchild->title;
                        $link2 = $subchild->url;
                        ?>
                                            <li><a href="<?php echo $link2; ?>"><?php echo $title2; ?></a></li>

                                            <?php
                        }
                        $cnt2++;
                    }
                    ?>

                                        </ul>
                                        <?php

                }
                ?>
                                    </li>
                                </ul>
                            </li>

                            <?php
} else {
                ?>


                            <li>
                                <a class="dropMenuLink"><?php echo $title1; ?></a>
                                <ul class="dropMenuPanel hide">
                                    <li class="panelLeft">
                                        <h3 class="greenText dinProStd"><?php echo $title1; ?></h3>

                                        <?php
                                        
                                        if(isset($description1) && !empty($description1))
                                        {
                                        
                                        ?>
                                        <p><?php echo substr($description1,0,550); ?></p>
                                        <?php
                                        }
                                        
                                        ?>

                                        <a href="<?php echo $link1; ?>" class="aboutButton">Read More <i
                                                class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                    </li>
                                    <li class="panelRight">
                                        <?php

                if (!empty($sub_level_children[$child->ID])) {
                    ?>

                                        <ul>

                                            <?php
                    $cnt2 = 0;  
                    foreach ($sub_level_children[$child->ID] as $subchild) {

                        if($cnt2<=8)
                        {
                        $title2 = $subchild->title;
                        $link2 = $subchild->url;
                        ?>
                                            <li><a href="<?php echo $link2; ?>"><?php echo $title2; ?></a></li>

                          <?php
                        }

                                            $cnt2++;
                      
                    }
                    ?>

                                        </ul>
                                        <?php

                }
                ?>
                                    </li>
                                </ul>
                            </li>




                            <?php

            }

            ?>

                            <?php
$cnt++;
        }

        ?>

                        </ul>


                    </div>
                </li>

                <?php

    }
    ?>
                <?php
}
?>


                <li class="singleLink">
                    <div class="headerLang">
                        <select class="">
                            <option>EN</option>
                            <option>CH</option>
                            <option>JP</option>
                        </select>
                    </div>
                </li>
                <li class="singleLink">
                    <div class="headerNumber">
                        <a href="tel:877 369 6208">877 369 6208</a>
                    </div>
                </li>
                <li class="singleLink">
                    <div class="headerSeach">
                        <a href="#" onClick="show_custom_modal();"><i class="fa fa-search" aria-hidden="true"></i></a>
                    </div>
                    <script>
                    function show_custom_modal() {
                        jQuery('#myModal-custom').modal('show');
                    }

                    jQuery(document).ready(function() {
                        jQuery('#search-box').keypress(function(e) {
                            if (e.keyCode == 13) {
                                var s = jQuery('#search-box').val();
                                window.location.href = '<?php echo home_url(); ?>?s=' + s +
                                    '#searchboxHeading';
                            }
                        });

                    });
                    </script>
                    <!-- Modal -->
                    <div id="myModal-custom" class="modal fade" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button class="close" type="button" data-dismiss="modal">×</button>

                                </div>
                                <div class="modal-body">


                                    <input type="text" id="search-box" class="form-control" name="s"
                                        placeholder="Search..."><br><br>



                                    <!--  <input type="text" name="s" id="search-box" class="form-control search-autocomplete"
                                        placeholder="Search"> -->



                                </div>

                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                </li>
            </ul>

        </nav>


        <style>
        .ui-autocomplete {
            z-index: 9999 !important;
        }
        </style>


        <?php
$menu_name = 'mobile-menu';
$locations = get_nav_menu_locations();
$menu = wp_get_nav_menu_object($locations[$menu_name]);
$menuitems = wp_get_nav_menu_items($menu->term_id, array('order' => 'DESC'));

$top_level = array();
$top_level_id = array();
$children = array();
$children_id = array();
$sub_level_children = array();

foreach ($menuitems as $item) {

    if (!$item->menu_item_parent) {
        array_push($top_level, $item);
        array_push($top_level_id, $item->ID);
    }

}

foreach ($menuitems as $item) {

    if (in_array($item->menu_item_parent, $top_level_id)) {
        $children[$item->menu_item_parent][] = $item;
        array_push($children_id, $item->ID);

    }

}

foreach ($menuitems as $item) {

    if (in_array($item->menu_item_parent, $children_id)) {
        $sub_level_children[$item->menu_item_parent][] = $item;
    }

}

?>


        <!-- menu mobile display -->
        <div id="mobileNav" class="sidenav">
            <div class="activePage center" id="activePage">
                <?php
$post = $wp_query->get_queried_object();

$pagename = $post->post_title;
echo $pagename;
?>
            </div>
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

            <div class="backButton">
                <a href="javascript:void(0)" onclick="prevNav()"></a>
            </div>

            <ul class="mobileMainMenu">


                <?php
foreach ($top_level as $item) {

    $title = $item->title;
    $link = $item->url;
    $ID = $item->ID;

    ?>


                <?php

    if (empty($children[$item->ID])) {

        ?>
                <li>
                    <div data-widget="showhide" data-type="panel" class="agMobileMenu">
                        <h2>
                            <a href="<?php echo $link; ?>"><?php echo $title; ?></a>
                        </h2>
                        <?php

    }

    ?>

                        <?php

    if (!empty($children[$item->ID])) {

        ?>

                <li>
                    <div data-widget="showhide" data-type="panel" class="agMobileMenu">
                        <h2 id="<?php echo $ID; ?>">
                            <a href="<?php echo $link; ?>"><?php echo $title; ?></a> <span class="menuIcon"></span>
                        </h2>

                        <script>
                        jQuery(document).ready(function() {

                            jQuery('#<?php echo $ID; ?>').click(function() {
                                //jQuery('div[id^=div]').hide();
                                jQuery('#div<?php echo $ID; ?>').addClass("active");
                                jQuery('.backButton').addClass("active");
                                jQuery('.backButton').click(function() {
                                    jQuery('#div<?php echo $ID; ?>').removeClass("active");
                                    jQuery(this).removeClass("active");
                                });
                            });
                        });
                        </script>

                        <ul class="subMenu" id="div<?php echo $ID; ?>">
                            <?php

        foreach ($children[$item->ID] as $child) {

            $title1 = $child->title;
            $link1 = $child->url;
            $ID1 = $child->ID;

            ?>


                            <?php

            if (empty($sub_level_children[$child->ID])) {
                ?>

                            <li><a href="<?php echo $link1; ?>"><?php echo $title1; ?></a>
                                <?php

            }

            ?>
                                <?php

            if (!empty($sub_level_children[$child->ID])) {
                ?>





                            <li><a href="<?php echo $link1; ?>"><?php echo $title1; ?></a><span class="menuIcon"
                                    id="showSub<?php echo $child->ID; ?>"></span>
                                <ul class="childSubMenu" id="child<?php echo $child->ID; ?>">

                                    <?php

                foreach ($sub_level_children[$child->ID] as $sub_child) {

                    $title2 = $sub_child->title;
                    $link2 = $sub_child->url;

                    ?>
                                    <li><a href="<?php echo $link2; ?>"><?php echo $title2; ?></a></li>
                                    <?php

                }

                ?>
                                </ul>
                            </li>

                            <?php

            }

            ?>



                            <script>
                            jQuery(document).ready(function() {
                                jQuery('#showSub<?php echo $child->ID; ?>').click(function() {
                                    //jQuery('child[id^=child]').hide();


                                    jQuery('#child<?php echo $child->ID; ?>').addClass("active");
                                    jQuery('.backButton').addClass("active");

                                    jQuery('.backButton').click(function() {
                                        jQuery('#child<?php echo $child->ID; ?>').removeClass(
                                            "active");
                                        jQuery('#div<?php echo $ID; ?>').addClass("active");
                                        jQuery(this).addClass("active");

                                        jQuery('.backButton').click(function() {
                                            jQuery('#div<?php echo $ID; ?>')
                                                .removeClass("active");
                                            jQuery(this).removeClass("active");
                                        });
                                    });
                                });
                            });
                            </script>

                            <?php

        }

        ?>

                        </ul>
                        <?php

    }

    ?>

                    </div>
                </li>
                <?php

}

?>



            </ul>
        </div>


        <span class="openNav" onclick="openNav()">&#9776;</span>
        <!-- menu mobile display end-->


        <?php
$menu_name = 'hamburger-menu';
$locations = get_nav_menu_locations();
$menu = wp_get_nav_menu_object($locations[$menu_name]);
$menuitems = wp_get_nav_menu_items($menu->term_id, array('order' => 'DESC'));

$top_level = array();
$top_level_id = array();
$children = array();
$children_id = array();
$sub_level_children = array();

foreach ($menuitems as $item) {

    if (!$item->menu_item_parent) {
        array_push($top_level, $item);
        array_push($top_level_id, $item->ID);
    }

}

foreach ($menuitems as $item) {

    if (in_array($item->menu_item_parent, $top_level_id)) {
        $children[$item->menu_item_parent][] = $item;
        array_push($children_id, $item->ID);

    }

}

foreach ($menuitems as $item) {

    if (in_array($item->menu_item_parent, $children_id)) {
        $sub_level_children[$item->menu_item_parent][] = $item;
    }

}

?>




        <div class="hamMegaMenu whiteBG" id="hamMegaMenu">
            <div class="hamMegaMenuOption flex">
                <div class="hamMegaMenuList greyBG">
                    <ul>
                        <li role="presentation" class="blackText"><strong>Explore</strong></li>
                        <?php
$cnt = 0;
foreach ($top_level as $item) {

    $title = $item->title;
    $link = $item->url;
    $ID = $item->ID;

    if ($cnt == 0) {

        ?>

                        <li role="presentation" class="blackText active" id="<?php echo $ID; ?>Show"><a
                                class="blackText"><?php echo $title; ?></a></li>

                        <script>
                        jQuery(".hamMegaMenuList ul li").hover(
                            function() {
                                jQuery(this).addClass("active");
                            },
                            function() {
                                jQuery(this).removeClass("active");
                                jQuery(".hamMegaMenuList ul li#<?php echo $ID; ?>Show").removeClass("active");
                            }
                        );

                        jQuery(".hamMegaMenuList ul li#<?php echo $ID; ?>Show").hover(function() {

                            jQuery(".hamMenuListRow").hide();
                            jQuery("#<?php echo $ID; ?>").show();

                            jQuery(".hamMegaMenuContanier").mouseover(function() {
                                jQuery(".hamMegaMenuList ul li#<?php echo $ID; ?>Show a").addClass(
                                    "menuActive");
                                /*   jQuery(".hamMegaMenuList ul li#solutionsShow a").removeClass("menuActive"); */

                            });
                            jQuery(".hamMegaMenuContanier").mouseout(function() {
                                jQuery(".hamMegaMenuList ul li#<?php echo $ID; ?>Show a").removeClass(
                                    "menuActive");

                            });
                        });
                        </script>


                        <?php
} else {
        ?>

                        <li role="presentation" class="blackText" id="<?php echo $ID; ?>Show"><a
                                class="blackText"><?php echo $title; ?></a></li>
                        <script>
                        jQuery(".hamMegaMenuList ul li").hover(
                            function() {
                                jQuery(this).addClass("active");
                            },
                            function() {
                                jQuery(this).removeClass("active");
                                jQuery(".hamMegaMenuList ul li#<?php echo $ID; ?>Show").removeClass("active");
                            }
                        );

                        jQuery(".hamMegaMenuList ul li#<?php echo $ID; ?>Show").hover(function() {

                            jQuery(".hamMenuListRow").hide();
                            jQuery("#<?php echo $ID; ?>").show();

                            jQuery(".hamMegaMenuContanier").mouseover(function() {
                                jQuery(".hamMegaMenuList ul li#<?php echo $ID; ?>Show a").addClass(
                                    "menuActive");
                                /*   jQuery(".hamMegaMenuList ul li#solutionsShow a").removeClass("menuActive"); */

                            });
                            jQuery(".hamMegaMenuContanier").mouseout(function() {
                                jQuery(".hamMegaMenuList ul li#<?php echo $ID; ?>Show a").removeClass(
                                    "menuActive");

                            });
                        });
                        </script>

                        <?php

    }

    $cnt++;

}
?>
                    </ul>
                </div>
                <div class="hamMegaMenuContanier">
                    <?php
$cnt = 0;
foreach ($top_level as $item) {
    $title = $item->title;
    $link = $item->url;
    $ID = $item->ID;
    ?>



                    <div class="hamMenuListRow" id="<?php echo $ID; ?>">
                        <div class="hamMenuListColumn flex">
                            <div class="hamMenuLinks">


                                <h2 class="dinProStd h2"><a class="greenText" href="#"><?php echo $title; ?></a></h2>


                                <script>
                                jQuery(document).ready(function() {



                                    <?php
if ($cnt == 0) {

        ?>

                                    jQuery('.hamMenuListRow').hide();
                                    jQuery('#<?php echo $ID; ?>').show();


                                    <?php
} else {
        ?>

                                    jQuery('#<?php echo $ID; ?>').hide();
                                    <?php
}
    ?>


                                    jQuery('#<?php echo $item->ID; ?> .show<?php echo $ID; ?>').hover(
                                function() {
                                        jQuery('#<?php echo $item->ID; ?> .hamMenuImgSection').hide();
                                        jQuery('#indust' + $(this).attr('target')).show();
                                    });

                                });
                                </script>

                                <?php

    if (!empty($children[$item->ID])) {

        ?>
                                <ul>



                                    <?php
foreach ($children[$item->ID] as $child) {
            ?>

                                    <li><a class="show<?php echo $ID; ?> blackText" href="#"
                                            target="<?php echo $child->ID; ?>"><?php echo $child->title; ?></a></li>



                                    <?php
}
        ?>
                                </ul>
                                <?php
}

    ?>


                            </div>

                            <?php

                            
                            $k = 0;
foreach ($children[$item->ID] as $child) {

    if($k==0)
    {

        
        ?>

                            <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $child->object_id ), 'full' ); ?>

                            <div class="hamMenuImgSection active" id="indust<?php echo $child->ID; ?>">
                                <img class="img-fluid" src="<?php echo $image[0];?>" alt="" height="150" width="330" />
                                <h4 class="blackText"><?php echo $child->title; ?></h4>

                                <?php
                                if(isset($child->description) && !empty($child->description))
                                {
                                
                                ?>
                                <p><?php echo $child->description; ?></p>
                                <?php
                                }
                                
                                ?>

                            </div>
                            <?php
    }
    else
    {
?>

                            <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $child->object_id ), 'full' ); ?>
                            <div class="hamMenuImgSection hide" id="indust<?php echo $child->ID; ?>">
                                <img class="img-fluid" src="<?php echo $image[0];?>" alt="" height="150" width="330" />
                                <h4 class="blackText"><?php echo $child->title; ?></h4>


                                <?php
                                if(isset($child->description) && !empty($child->description))
                                {
                                
                                ?>
                                <p><?php echo $child->description; ?></p>
                                <?php
                                }
                                
                                ?>
                            </div>


                            <?php
    }  
?>

                            <?php
                            $k++;
}
    ?>

                        </div>
                    </div>

                    <?php
$cnt++;
}
?>

                </div>
            </div>
        </div>
    </header>